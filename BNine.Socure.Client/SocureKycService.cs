﻿namespace BNine.Socure.Client;

using System.Net;
using Application.Aggregates.Evaluation.Commands;
using Application.Aggregates.Evaluation.Models;
using Application.Interfaces.Socure;
using Common;
using Enums;
using ICSharpCode.SharpZipLib.Zip;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Settings;

public class SocureKycService : BaseSocureService, ISocureKycService
{
    private readonly ILogger<SocureKycService> _logger;

    public SocureKycService(
        IOptions<SocureSettings> options,
        IHttpClientFactory clientFactory,
        ILogger<SocureKycService> logger)
        : base(options, clientFactory)
    {
        _logger = logger;
    }

    public async Task<SocureTransactionResponse> PerformKycTransaction(SocureTransactionRequest request,
        CancellationToken token)
    {
        var client = GetAuthorizedClient();
        var uri = "api/3.0/EmailAuthScore";
        var content = SerializationHelper.GetRequestContent(request);
        var response = await client.PostAsync(uri, content, token);

        await HandleErrors(response);

        var data = await SerializationHelper.GetResponseContent<SocureTransactionResponse>(response);
        data.RawResult = await response.Content.ReadAsStringAsync(token);

        return data;
    }

    public async Task<Dictionary<SocureClientDocumentModel, byte[]>> DownloadAllDocumentSides(Guid scanId,
        CancellationToken token)
    {
        var client = GetAuthorizedDocumentsClient();
        var uri = $"api/3.0/documents/{scanId}";
        var response = await client.GetAsync(uri, token);

        if (response.StatusCode == HttpStatusCode.BadRequest
            && (await response.Content.ReadAsStringAsync(token)).Contains("No Documents found"))
        {
            return null!;
        }

        await HandleErrors(response);

        var bytes = await response.Content.ReadAsByteArrayAsync(token);

        return await ExtractAllImages(bytes);
    }

    private async Task HandleErrors(HttpResponseMessage response)
    {
        if (!response.IsSuccessStatusCode)
        {
            var result = await response.Content.ReadAsStringAsync();
            _logger.LogError("Socure error response: {result}", result);

            if (response.StatusCode == HttpStatusCode.Unauthorized)
            {
                throw new UnauthorizedAccessException(result);
            }

            if (response.StatusCode == HttpStatusCode.BadRequest)
            {
                throw new FormatException(result);
            }

            if (response.StatusCode == HttpStatusCode.NotFound)
            {
                throw new ArgumentException(result, nameof(SocureSettings.ApiUrl));
            }

            if (response.StatusCode == HttpStatusCode.NotAcceptable)
            {
                throw new AccessViolationException("Make sure your IP is whitelisted or try using a VPN.");
            }

            throw new Exception(result);
        }
    }

    private async Task<Dictionary<SocureClientDocumentModel, byte[]>> ExtractAllImages(byte[] bytes)
    {
        await using var memory = new MemoryStream(bytes);
        await using var zipStream = new ZipInputStream(memory);

        ZipEntry lastEntryInArchive = zipStream.GetNextEntry();
        if (lastEntryInArchive == null)
        {
            throw new NullReferenceException("The zip archive does not contain any files.");
        }

        var files = new Dictionary<SocureClientDocumentModel, byte[]>();

        while (lastEntryInArchive != null)
        {
            var fileName = Path.GetFileName(lastEntryInArchive.Name);

            if (fileName == string.Empty)
            {
                return null!;
            }

            await using var outMemory = new MemoryStream(0);
            await using BinaryWriter streamWriter = new BinaryWriter(outMemory);
            var size = 2048;
            var data = new byte[2048];
            while (true)
            {
                size = zipStream.Read(data, 0, data.Length);
                if (size > 0)
                {
                    streamWriter.Write(data, 0, size);
                }
                else
                {
                    break;
                }
            }

            files.Add(
                new SocureClientDocumentModel()
                {
                    FileName = fileName,
                    DocumentSide = GetDocumentSideFromName(fileName)
                }, outMemory.ToArray());

            lastEntryInArchive = zipStream.GetNextEntry();
        }

        return files;
    }

    private DocumentSide GetDocumentSideFromName(string fileName)
    {
        if (fileName.Contains("documentBack"))
        {
            return DocumentSide.BackSide;
        }
        else if (fileName.Contains("documentFront"))
        {
            return DocumentSide.FrontSide;
        }

        return DocumentSide.Default;
    }
}
