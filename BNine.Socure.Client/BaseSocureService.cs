﻿namespace BNine.Socure.Client;

using System.Net.Http.Headers;
using Constants;
using Microsoft.Extensions.Options;
using Settings;

public class BaseSocureService
{
    private readonly IHttpClientFactory _clientFactory;
    private readonly IOptions<SocureSettings> _options;

    public BaseSocureService(
        IOptions<SocureSettings> options,
        IHttpClientFactory clientFactory)
    {
        _clientFactory = clientFactory;
        _options = options;
    }

    protected HttpClient GetAuthorizedClient()
    {
        var client = _clientFactory.CreateClient(HttpClientName.Socure);
        client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(
            "SocureApiKey", _options.Value.ApiKey);
        return client;
    }

    protected HttpClient GetAuthorizedDocumentsClient()
    {
        var client = _clientFactory.CreateClient(HttpClientName.SocureDocuments);
        client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(
            "SocureApiKey", _options.Value.ApiKey);
        return client;
    }
}
