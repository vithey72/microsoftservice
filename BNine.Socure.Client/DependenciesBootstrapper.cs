﻿namespace BNine.Socure.Client;

using System;
using Application.Interfaces.Socure;
using Constants;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

public static class DependenciesBootstrapper
{
    public static IServiceCollection AddSocureClient(this IServiceCollection services, IConfiguration configuration)
    {
        var apiAddress = configuration["SocureSettings:ApiUrl"];
        var documentsAddress = configuration["SocureSettings:DocumentsApiUrl"];
        services.AddTransient<ISocureKycService, SocureKycService>();
        services.AddHttpClient(HttpClientName.Socure, client =>
        {
            client.BaseAddress = new Uri(apiAddress);
        });
        services.AddHttpClient(HttpClientName.SocureDocuments, client =>
        {
            client.BaseAddress = new Uri(documentsAddress);
        });

        return services;
    }
}
