﻿namespace BNine.Domain.Events.AdvanceTaken
{
    using System;
    using BNine.Domain.Infrastructure;

    public class AdvanceTakenEvent : DomainEvent
    {
        public Guid UserId
        {
            get; set;
        }

        public AdvanceTakenEvent(Guid userId)
        {
            UserId = userId;
        }
    }
}
