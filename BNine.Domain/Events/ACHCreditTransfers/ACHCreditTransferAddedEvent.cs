﻿namespace BNine.Domain.Events.ACHCreditTransfers
{
    using BNine.Domain.Entities.BankAccount;
    using BNine.Domain.Infrastructure;

    public class ACHCreditTransferAddedEvent : DomainEvent
    {
        public ACHCreditTransfer Transfer
        {
            get; set;
        }

        public ACHCreditTransferAddedEvent(ACHCreditTransfer transfer)
        {
            Transfer = transfer;
        }
    }
}
