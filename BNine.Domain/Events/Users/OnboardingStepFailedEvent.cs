﻿namespace BNine.Domain.Events.Users;

using System.Collections.Generic;
using BNine.Domain.Entities.User;
using BNine.Enums;

public class OnboardingStepFailedEvent
    : OnboardingStepEventBase
{
    public IEnumerable<string> Errors
    {
        get; set;
    }

    public OnboardingStepFailedEvent(User user, UserStatus status, UserStatusDetailed statusDetailed, IEnumerable<string> errors,
        EventSourcePlatform sourcePlatform = EventSourcePlatform.Backend)
    {
        User = user;
        Status = status;
        StatusDetailed = statusDetailed;
        Errors = errors;
        SourcePlatform = sourcePlatform;
    }
}
