﻿namespace BNine.Domain.Events
{
    using BNine.Domain.Entities.User;
    using BNine.Domain.Infrastructure;

    public class UserBlockedEvent
        : DomainEvent
    {
        public User User
        {
            get;
        }

        public UserBlockedEvent(User user)
        {
            User = user;
        }
    }
}
