﻿namespace BNine.Domain.Events
{
    using BNine.Domain.Entities.User;
    using BNine.Domain.Infrastructure;

    public class UserUnblockedEvent
        : DomainEvent
    {
        public User User
        {
            get;
        }

        public UserUnblockedEvent(User user)
        {
            User = user;
        }
    }
}
