﻿namespace BNine.Domain.Events.Users;

using BNine.Domain.Entities.User;
using BNine.Domain.Infrastructure;
using BNine.Enums;

public class OnboardingStepEventBase
    : DomainEvent
{
    public User User
    {
        get; set;
    }

    public UserStatus Status
    {

        get; set;
    }

    public UserStatusDetailed StatusDetailed
    {

        get; set;
    }

    public EventSourcePlatform SourcePlatform
    {
        get; set;
    }
}
