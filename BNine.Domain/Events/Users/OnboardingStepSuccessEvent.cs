﻿namespace BNine.Domain.Events.Users;

using BNine.Domain.Entities.User;
using BNine.Enums;

public class OnboardingStepSuccessEvent
    : OnboardingStepEventBase
{
    public OnboardingStepSuccessEvent(User user, UserStatus status, UserStatusDetailed statusDetailed,
        EventSourcePlatform sourcePlatform = EventSourcePlatform.Backend)
    {
        User = user;
        Status = status;
        StatusDetailed = statusDetailed;
        SourcePlatform = sourcePlatform;
    }
}
