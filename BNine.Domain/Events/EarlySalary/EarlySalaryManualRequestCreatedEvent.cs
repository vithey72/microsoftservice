﻿namespace BNine.Domain.Events.EarlySalary
{
    using BNine.Domain.Entities.User.EarlySalary;
    using BNine.Domain.Infrastructure;

    public class EarlySalaryManualRequestCreatedEvent : DomainEvent
    {
        public EarlySalaryManualRequest EarlySalaryManualRequest
        {
            get; set;
        }

        public EarlySalaryManualRequestCreatedEvent(EarlySalaryManualRequest earlySalaryManualRequest)
        {
            EarlySalaryManualRequest = earlySalaryManualRequest;
        }
    }
}
