﻿namespace BNine.Domain.Infrastructure
{
    using System;

    public abstract class DomainEvent
    {
        protected DomainEvent()
        {
            DateOccurred = DateTimeOffset.UtcNow;
        }

        public DateTimeOffset DateOccurred { get; protected set; } = DateTime.UtcNow;

        public bool IsPublished
        {
            get; set;
        }
    }
}
