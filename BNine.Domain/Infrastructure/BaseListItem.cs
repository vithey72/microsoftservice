﻿using BNine.Domain.Interfaces;

namespace BNine.Domain.Infrastructure
{
    using Entities.Common;

    public class BaseListItem : IBaseListItem
    {
        public string Key
        {
            get; set;
        }

        public ICollection<LocalizedText> Value
        {
            get; set;
        }

        public int Order
        {
            get; set;
        }
    }
}
