﻿namespace BNine.Domain.Entities
{
    using System;
    using BNine.Domain.Interfaces;

    public class LoanCalculatedLimit : IBaseEntity
    {
        public LoanCalculatedLimit()
        {
            CreatedAt = DateTime.UtcNow;
        }

        public Guid Id
        {
            get; set;
        }

        public DateTime CreatedAt
        {
            get; set;
        }

        public DateTime? DeletedAt
        {
            get; set;
        }

        public decimal Amount
        {
            get; set;
        }

        public User.User User
        {
            get; set;
        }

        public Guid UserId
        {
            get; set;
        }
    }
}
