﻿namespace BNine.Domain.Entities.TransfersSecurityOTPs;

using BNine.Domain.Interfaces;
using Enums.Transfers;
using User;

public class TransfersSecurityOTP : IBaseEntity
{
    public Guid Id
    {
        get;
        set;
    }

    public string Value
    {
        get;
        set;
    }

    public bool IsValid
    {
        get;
        set;
    }

    public DateTime CreatedDate
    {
        get;
        set;
    }

    public Guid UserId
    {
        get;
        set;
    }

    public string DeviceId
    {
        get;
        set;
    }

    public TransferType OperationType
    {
        get;
        set;
    }

    public string OperationPayload
    {
        get;
        set;
    }

    public User User
    {
        get;
        set;
    }

    public string PhoneNumber
    {
        get;
        set;
    }

    public void Terminate(string payload)
    {
        IsValid = false;
        OperationPayload = payload;
    }
}

