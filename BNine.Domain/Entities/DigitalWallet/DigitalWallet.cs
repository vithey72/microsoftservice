﻿namespace BNine.Domain.Entities.DigitalWallet
{
    using System;
    using Interfaces;
    using Enums;

    public abstract class DigitalWallet : IBaseEntity
    {
        public DigitalWallet()
        {
            CreatedAt = DateTime.UtcNow;
            UpdatedAt = DateTime.UtcNow;
        }

        public Guid Id
        {
            get; set;
        }

        public DigitalWalletType Type
        {
            get; set;
        }

        public DateTime CreatedAt
        {
            get; set;
        }

        public DateTime UpdatedAt
        {
            get; set;
        }

        public DateTime? DeletedAt
        {
            get; set;
        }

        public DebitCard DebitCard
        {
            get; set;
        }

        public Guid DebitCardId
        {
            get; set;
        }
    }
}
