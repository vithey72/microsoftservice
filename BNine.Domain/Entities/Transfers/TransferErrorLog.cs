﻿namespace BNine.Domain.Entities.Transfers;

using Enums.Transfers;
using Interfaces;
using User;

public class TransferErrorLog : IBaseEntity
{
    public Guid Id
    {
        get;
        set;
    }

    public Guid UserId
    {
        get;
        set;
    }

    public TransferDirection TransferDirection
    {
        get; set;
    }

    public TransferType TransferNetworkType
    {
        get;
        set;
    }

    public string ExceptionHeader
    {
        get;
        set;
    }

    public string ExceptionBody
    {
        get;
        set;
    }

    public string MbanqRawErrorPayload
    {
        get;
        set;
    }

    public DateTime Timestamp
    {
        get;
        set;
    }

    public User User
    {
        get;
        set;
    }
}
