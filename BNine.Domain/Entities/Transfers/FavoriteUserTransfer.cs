﻿namespace BNine.Domain.Entities.Transfers
{
    using BNine.Domain.Interfaces;

    public class FavoriteUserTransfer : IBaseEntity
    {
        public Guid Id
        {
            get; set;
        }

        public string Name
        {
            get; set;
        }

        public string Description
        {
            get; set;
        }

        public DateTime CreatedAt
        {
            get; set;
        }

        public DateTime UpdatedAt
        {
            get; set;
        }

        public int TransferId
        {
            get; set;
        }

        public string CreditorIdentifier
        {
            get; set;
        }

        public Guid UserId
        {
            get; set;
        }

        public User.User User
        {
            get; set;
        }
    }
}
