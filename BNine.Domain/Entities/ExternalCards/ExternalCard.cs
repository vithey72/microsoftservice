﻿namespace BNine.Domain.Entities.ExternalCards
{
    using System;
    using BNine.Domain.Interfaces;

    public class ExternalCard : IBaseEntity
    {
        public ExternalCard()
        {
            CreatedAt = DateTime.UtcNow;
            UpdatedAt = DateTime.UtcNow;
        }

        public Guid Id
        {
            get; set;
        }

        public long ExternalId
        {
            get; set;
        }

        public string Name
        {
            get; set;
        }

        public DateTime CreatedAt
        {
            get; set;
        }

        public DateTime UpdatedAt
        {
            get; set;
        }

        public DateTime? DeletedAt
        {
            get; set;
        }

        public User.User User
        {
            get; set;
        }

        public Guid UserId
        {
            get; set;
        }

        public void Delete()
        {
            DeletedAt = DateTime.Now;
        }
    }
}
