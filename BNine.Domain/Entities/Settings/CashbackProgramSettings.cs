﻿namespace BNine.Domain.Entities.Settings
{
    using System;
    using BNine.Domain.Interfaces;

    public class CashbackProgramSettings : IBaseEntity
    {
        public Guid Id
        {
            get; set;
        }

        /// <summary>
        /// Amount of $ user has to spend on categories of cashback to actually receive cashback at the end of month.
        /// </summary>
        public decimal MinimumSpentAmount
        {
            get; set;
        }

        /// <summary>
        /// Json of ViewModel that the screens are rendered off of.
        /// </summary>
        public string SlideshowScreens
        {
            get; set;
        }

        /// <summary>
        /// Number of categories user can pick at min.
        /// </summary>
        public int MinCashbackCategories
        {
            get; set;
        }

        /// <summary>
        /// Number of categories user can pick at max.
        /// </summary>
        public int MaxCashbackCategories
        {
            get; set;
        }

        public DateTime UpdatedAt
        {
            get; set;
        }
    }
}
