﻿namespace BNine.Domain.Entities.Settings
{
    using System;
    using System.Collections.Immutable;
    using BNine.Domain.Interfaces;

    public class LoanSettings : IBaseEntity
    {
        public LoanSettings()
        {
            CreatedAt = DateTime.UtcNow;
            UpdatedAt = DateTime.UtcNow;
        }

        public Guid Id
        {
            get; set;
        }

        public ImmutableSortedSet<string> ACHPrincipalKeywords
        {
            get; set;
        }

        public decimal Limit
        {
            get; set;
        }

        public int BoostExpressFeeChargeTypeId
        {
            get; set;
        }

        public decimal RequiredMinimalDeposit
        {
            get; set;
        }

        public DateTime CreatedAt
        {
            get; set;
        }

        public DateTime? UpdatedAt
        {
            get; set;
        }
    }
}
