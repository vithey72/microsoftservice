﻿namespace BNine.Domain.Entities.Settings;

public class MerchantCategoryCodeIconSettings
{
    public int Code
    {
        get; set;
    }

    public string Description
    {
        get; set;
    }

    public string IconUrl
    {
        get; set;
    }
}
