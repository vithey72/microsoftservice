﻿namespace BNine.Domain.Entities.Settings
{
    using System;
    using System.Collections.Immutable;
    using BNine.Domain.Interfaces;

    public class TransferMerchantsSettings : IBaseEntity
    {
        public Guid Id
        {
            get; set;
        }

        /// <summary>
        /// Determines order of search for a match. The higher the priority, the earlier this instance among others is scanned for a match.
        /// </summary>
        public int Priority
        {
            get;
            set;
        }

        public string MerchantName
        {
            get; set;
        }

        public string MerchantDisplayName
        {
            get; set;
        }

        public ImmutableSortedSet<string> MerchantDescriptionKeywords
        {
            get; set;
        }

        public DateTime CreatedAt
        {
            get; set;
        }

        public DateTime? UpdatedAt
        {
            get; set;
        }

        public string PictureUrl
        {
            get; set;
        }
    }
}
