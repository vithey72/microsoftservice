﻿namespace BNine.Domain.Entities.Settings;

using BNine.Domain.Interfaces;

public class CommonAccountSettings : IBaseEntity
{
    public Guid Id
    {
        get; set;
    }

    public int VoluntaryClosureDelayInDays
    {
        get; set;
    }

    public int PendingClosureCheckGapInHours
    {
        get; set;
    }

    public int BlockClosureOffsetFromActivationInDays
    {
        get; set;
    }

    public int TruvDistributionShareInPercents
    {
        get; set;
    }
}
