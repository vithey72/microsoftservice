﻿namespace BNine.Domain.Entities.Settings
{
    using System;
    using Interfaces;

    public class StaticDocument : IBaseEntity
    {
        public StaticDocument()
        {
            Id = Guid.NewGuid();
            CreatedAt = DateTime.UtcNow;
            UpdatedAt = DateTime.UtcNow;
        }

        public Guid Id
        {
            get;
            set;
        }

        /// <summary>
        /// <see cref="StaticDocumentKeys"/>
        /// </summary>
        public string Key
        {
            get;
            set;
        }

        public string Title
        {
            get;
            set;
        }

        public int Version
        {
            get;
            set;
        }

        public DateTime CreatedAt
        {
            get;
            set;
        }

        public DateTime UpdatedAt
        {
            get;
            set;
        }

        /// <summary>
        /// Dynamic storage path that contains timestamp at the end in the file name
        /// Used from B9 product app directly
        /// </summary>
        public string StoragePath
        {
            get;
            set;
        }

        /// <summary>
        /// Used from Zendesk support
        /// </summary>
        public string StaticStoragePath
        {
            get;
            set;
        }

        public bool IsAgreementRequired
        {
            get;
            set;
        }
    }
}
