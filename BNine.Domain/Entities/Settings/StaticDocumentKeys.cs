﻿namespace BNine.Domain.Entities.Settings;

public static class StaticDocumentKeys
{
    public const string TermsOfServiceKey = "terms-of-service";
    public const string PrivacyPolicy = "privacy-policy";
    public const string BNineCashBackRewardsDisclosure = "b9-cashback-rewards-disclosure";
    public const string ExternalCardTermsAndConditions = "external-card-terms-and-conditions";
    public const string ElectronicCommunicationConsent = "electronic-communication-consent";
    public const string DigitalWalletTermsAndConditions = "digital-wallet-terms-and-conditions";
    public const string CustomerAccountAndCardholderAgreement = "customer-account-and-cardholder-agreement";
    public const string BNineDisclosure = "b9-disclosure";
    public const string SmsCampaignTermsOfServicesMarketing = "sms-campaign-terms-of-services-marketing";
    public const string SmsCampaignTermsOfServicesOtp = "sms-campaign-terms-of-services-otp";

}


