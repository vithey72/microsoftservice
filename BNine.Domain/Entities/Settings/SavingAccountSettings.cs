﻿namespace BNine.Domain.Entities.Settings
{
    using System;
    using BNine.Domain.Interfaces;

    public class SavingAccountSettings : IBaseEntity
    {
        public SavingAccountSettings()
        {
            CreatedAt = DateTime.UtcNow;
            UpdatedAt = DateTime.UtcNow;
        }

        public Guid Id
        {
            get; set;
        }

        public bool IsPullFromExternalCardEnabled
        {
            get; set;
        }

        public DateTime CreatedAt
        {
            get; set;
        }

        public DateTime? UpdatedAt
        {
            get; set;
        }

        public decimal PullFromExternalCardMinAmount
        {
            get; set;
        }

        public decimal PullFromExternalCardMaxAmount
        {
            get; set;
        }

        public decimal PushToExternalCardMinAmount
        {
            get; set;
        }

        public decimal PushToExternalCardMaxAmount
        {
            get; set;
        }
    }
}
