﻿namespace BNine.Domain.Entities.Settings;

using BNine.Domain.Interfaces;

public class WalletBlockCollectionSettings : IBaseEntity
{
    public Guid Id
    {
        get; set;
    }

    /// <summary>
    /// Text description of what this wallet block collection is.
    /// </summary>
    public string TextDescription
    {
        get; set;
    }
}
