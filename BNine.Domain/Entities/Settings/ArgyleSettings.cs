﻿namespace BNine.Domain.Entities.Settings
{
    using System;
    using BNine.Domain.Interfaces;

    public class ArgyleSettings : IBaseEntity
    {
        public ArgyleSettings()
        {
            CreatedAt = DateTime.UtcNow;
            UpdatedAt = DateTime.UtcNow;
        }

        public Guid Id
        {
            get; set;
        }

        public decimal DefaultAmountAllocation
        {
            get; set;
        }

        public decimal MinAmountAllocation
        {
            get; set;
        }

        public decimal MaxAmountAllocation
        {
            get; set;
        }

        public decimal DefaultPercentAllocation
        {
            get; set;
        }

        public decimal MinPercentAllocation
        {
            get; set;
        }

        public decimal MaxPercentAllocation
        {
            get; set;
        }

        public DateTime CreatedAt
        {
            get; set;
        }

        public DateTime? UpdatedAt
        {
            get; set;
        }
    }
}
