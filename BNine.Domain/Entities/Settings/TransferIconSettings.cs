﻿namespace BNine.Domain.Entities.Settings
{
    using BNine.Enums.Transfers.Controller;

    public class TransferIconSettings
    {
        public BankTransferIconOption Id
        {
            get; set;
        }

        public string IconUrl
        {
            get; set;
        }
    }
}
