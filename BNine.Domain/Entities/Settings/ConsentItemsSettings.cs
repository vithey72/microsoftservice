﻿namespace BNine.Domain.Entities.Settings;

public static class ConsentItemsSettings
{
    public const string SmsMarketingConsent = "sms-marketing-consent";

    public static readonly Dictionary<string, ConsentDetails> ConsentData = new Dictionary<string, ConsentDetails>()
    {
        { SmsMarketingConsent, new ConsentDetails { IsRequired = false } }
    };
}

public class ConsentDetails
{
    public bool IsRequired
    {
        get;
        set;
    }
}
