﻿namespace BNine.Domain.Entities
{
    using System;
    using DigitalWallet;
    using BNine.Enums.DebitCard;
    using Interfaces;

    public class DebitCard : IBaseEntity
    {
        public DebitCard()
        {
            CreatedAt = DateTime.UtcNow;
            IsPinSet = false;
        }

        public Guid Id
        {
            get;
            set;
        }

        public int ExternalId
        {
            get;
            set;
        }

        public CardStatus Status
        {
            get;
            set;
        }

        public CardShippingStatus ShippingStatus
        {
            get;
            set;
        }

        public DateTime? ActivationDate
        {
            get;
            set;
        }

        public DateTime? CreatedAt
        {
            get;
            set;
        }

        public DateTime? EmbossedAt
        {
            get;
            set;
        }

        public DateTime? SuspendedAt
        {
            get;
            set;
        }

        public DateTime? TerminatedAt
        {
            get;
            set;
        }

        public Guid UserId
        {
            get;
            set;
        }

        public User.User User
        {
            get;
            set;
        }

        public AppleWallet AppleWallet
        {
            get; set;
        }

        public GoogleWallet GoogleWallet
        {
            get; set;
        }

        public SamsungWallet SamsungWallet
        {
            get; set;
        }

        public bool? ReplacedByUserRequest
        {
            get;
            set;
        }

        public CardType Type
        {
            get; set;
        }

        public bool IsPinSet
        {
            get;
            set;
        }

        public void Terminate()
        {
            if (Status == CardStatus.TERMINATED)
            {
                return;
            }

            Status = CardStatus.TERMINATED;
            TerminatedAt = DateTime.Now;
        }
    }
}
