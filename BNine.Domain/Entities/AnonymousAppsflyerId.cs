﻿namespace BNine.Domain.Entities
{
    using System;
    using Interfaces;

    public class AnonymousAppsflyerId : IBaseEntity
    {
        public AnonymousAppsflyerId()
        {
            CreatedAt = DateTime.UtcNow;
        }

        public Guid Id
        {
            get;
            set;
        }

        public string Phone
        {
            get;
            set;
        }

        public string AppsflyerId
        {
            get;
            set;
        }

        public DateTime CreatedAt
        {
            get;
            set;
        }
    }
}
