﻿namespace BNine.Domain.Entities.Banners;

using Enums;
using Interfaces;

public class BaseMarketingBanner : IBaseEntity
{
    public Guid Id
    {
        get;
        set;
    }

    /// <summary>
    /// Unique name of this type of banner.
    /// </summary>
    public string Name
    {
        get;
        set;
    }

    public bool IsEnabled
    {
        get;
        set;
    }

    public string Title
    {
        get;
        set;
    }

    public string Subtitle
    {
        get;
        set;
    }

    /// <summary>
    /// Higher value means banner will be shown before others.
    /// </summary>
    public int Priority
    {
        get;
        set;
    }

    public string ButtonText
    {
        get;
        set;
    }

    public string ButtonDeeplink
    {
        get;
        set;
    }

    public object ButtonDeeplinkArguments
    {
        get;
        set;
    }

    public string ExternalLink
    {
        get;
        set;
    }

    public LinkFollowMode ExternalLinkType
    {
        get;
        set;
    }

    /// <summary>
    /// At what moment in time this banner should become visible.
    /// </summary>
    public DateTime? ShowFrom
    {
        get;
        set;
    }

    /// <summary>
    /// At what moment in time this banner should be disabled.
    /// </summary>
    public DateTime? ShowTil
    {
        get;
        set;
    }

    /// <summary>
    /// How much time should pass until this banner is shown again.
    /// Null means infinity.
    /// </summary>
    public TimeSpan? ShowCooldown
    {
        get;
        set;
    }

    /// <summary>
    /// Array of cooldowns, increasing with every banner closing.
    /// </summary>
    public TimeSpan[] ShowCooldownProgressive
    {
        get;
        set;
    }

    /// <summary>
    /// Minimum registration date of the user, so that only new users could see it.
    /// Null means no restriction.
    /// </summary>
    public DateTime? MinUserRegistrationDate
    {
        get;
        set;
    }

    public List<SeenMarketingBanner> SeenBannerInstances
    {
        get;
        set;
    }

    /// <summary>
    /// Ids of feature.Groups entities that user should have at least one of to see the banner.
    /// </summary>
    public Guid[] Groups
    {
        get;
        set;
    }

    public DateTime CreatedAt
    {
        get;
        set;
    }

    public DateTime? UpdatedAt
    {
        get;
        set;
    }
}
