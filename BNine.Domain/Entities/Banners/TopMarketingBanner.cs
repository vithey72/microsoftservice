﻿namespace BNine.Domain.Entities.Banners;

[Obsolete]
public class TopMarketingBanner : BaseMarketingBanner
{
    public bool HasCloseButton
    {
        get;
        set;
    }
}
