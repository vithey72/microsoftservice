﻿namespace BNine.Domain.Entities.Banners;

using System;
using Interfaces;
using User;

public class SeenMarketingBanner : IBaseEntity
{
    public Guid Id
    {
        get;
        set;
    }

    public Guid UserId
    {
        get;
        set;
    }

    public User User
    {
        get;
        set;
    }

    public Guid BannerId
    {
        get;
        set;
    }

    public BaseMarketingBanner Banner
    {
        get;
        set;
    }

    /// <summary>
    /// Just for sake or readability.
    /// </summary>
    public string BannerName
    {
        get;
        set;
    }

    public DateTime? NextDisplayTime
    {
        get;
        set;
    }

    public int SeenTimes
    {
        get;
        set;
    }
}
