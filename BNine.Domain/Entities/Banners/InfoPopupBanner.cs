﻿namespace BNine.Domain.Entities.Banners;

using Enums;

public class InfoPopupBanner : BaseMarketingBanner
{
    public string PictureUrl
    {
        get;
        set;
    }

    public AppScreens AppScreen
    {
        get;
        set;
    }

    public bool CannotBeClosed
    {
        get;
        set;
    }
}
