﻿namespace BNine.Domain.Entities.BonusCodes
{
    using System;
    using BNine.Domain.Interfaces;

    /// <summary>
    /// An entity that ties together every individual use of <see cref="Entities.BonusCode"/> with the code itself.
    /// </summary>
    public class BonusCodeActivation : IBaseEntity
    {
        public BonusCodeActivation()
        {
            UpdatedAt = DateTime.UtcNow;
        }

        public Guid Id
        {
            get;
            set;
        }

        public Guid BonusCodeId
        {
            get;
            set;
        }

        public BonusCode BonusCode
        {
            get;
            set;
        }

        public Guid ReceiverId
        {
            get;
            set;
        }

        public User.User Receiver
        {
            get;
            set;
        }

        public DateTime CreatedAt
        {
            get;
            set;
        }

        public DateTime? UpdatedAt
        {
            get;
            set;
        }

        public bool ReceiverTransferSuccess
        {
            get;
            set;
        }

        public bool SenderTransferSuccess
        {
            get;
            set;
        }
    }
}
