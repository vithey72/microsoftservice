﻿namespace BNine.Domain.Entities
{
    using System;
    using System.Collections.Generic;
    using BNine.Domain.Entities.BonusCodes;
    using BNine.Domain.Interfaces;
    using Newtonsoft.Json;

    public class BonusCode : IBaseEntity
    {
        public BonusCode()
        {
            CreationDate = DateTime.UtcNow;
            UpdatedAt = DateTime.UtcNow;
        }

        public Guid Id
        {
            get; set;
        }

        public string Code
        {
            get; set;
        }

        public DateTime CreationDate
        {
            get; set;
        }

        public Guid SenderId
        {
            get; set;
        }

        [JsonIgnore]
        public User.User Sender
        {
            get;
            set;
        }

        [Obsolete]
        public Guid? ReceiverId
        {
            get; set;
        }

        [Obsolete]
        [JsonIgnore]
        public User.User Receiver
        {
            get; set;
        }

        [Obsolete]
        public bool IsAlreadyUsed
        {
            get; set;
        }

        public DateTime? UpdatedAt
        {
            get; set;
        }

        public DateTime? DeletedAt
        {
            get; set;
        }

        [JsonIgnore]
        /// <summary>
        /// Collection of activations of this code. Empty if the code hasn't been activated.
        /// </summary>
        public ICollection<BonusCodeActivation> Activations
        {
            get; set;
        } = new List<BonusCodeActivation>();
    }
}
