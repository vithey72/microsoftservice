﻿namespace BNine.Domain.Entities.Administrator
{
    using System;

    public class ExternalIdentity
    {
        public ExternalIdentity()
        {
            CreatedAt = DateTime.UtcNow;
            UpdatedAt = DateTime.UtcNow;
        }

        public string Provider
        {
            get; set;
        }

        public string Id
        {
            get; set;
        }

        public Guid AdministratorId
        {
            get; set;
        }

        public Administrator Administrator
        {
            get; set;
        }

        public DateTime? CreatedAt
        {
            get; set;
        }

        public DateTime? UpdatedAt
        {
            get; set;
        }

        public DateTime? DeletedAt
        {
            get; set;
        }
    }
}
