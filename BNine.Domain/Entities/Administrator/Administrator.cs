﻿namespace BNine.Domain.Entities.Administrator
{
    using System;
    using System.Collections.Generic;
    using BNine.Domain.Interfaces;

    public class Administrator : IBaseEntity
    {
        public Administrator()
        {
            CreatedAt = DateTime.UtcNow;
            UpdatedAt = DateTime.UtcNow;
        }

        public Guid Id
        {
            get; set;
        }

        public string Name
        {
            get; set;
        }

        public DateTime? CreatedAt
        {
            get; set;
        }

        public DateTime? UpdatedAt
        {
            get; set;
        }

        public DateTime? DeletedAt
        {
            get; set;
        }

        public IEnumerable<ExternalIdentity> ExternalIdentities
        {
            get; set;
        } = new List<ExternalIdentity>();
    }
}
