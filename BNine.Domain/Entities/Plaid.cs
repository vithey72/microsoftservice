﻿namespace BNine.Domain.Entities
{
    using System;
    using System.Collections.Generic;
    using Interfaces;

    public class Plaid : IBaseEntity
    {
        public Guid Id
        {
            get;
            set;
        }

        public DateTime CreatedAt
        {
            get;
            set;
        }

        public DateTime UpdatedAt
        {
            get;
            set;
        }

        public Guid BankAccountId
        {
            get;
            set;
        }

        public ExternalBankAccount BankAccount
        {
            get;
            set;
        }

        public ICollection<string> Transactions
        {
            get;
            set;
        } = new List<string>();
    }
}
