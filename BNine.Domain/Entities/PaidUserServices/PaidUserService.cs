﻿namespace BNine.Domain.Entities.PaidUserServices;

using System;
using Interfaces;

public class PaidUserService : IBaseEntity
{
    public Guid Id
    {
        get;
        set;
    }

    /// <summary>
    /// Id of charge type entity in MBanq.
    /// </summary>
    public int ChargeTypeId
    {
        get;
        set;
    }

    public string Name
    {
        get;
        set;
    }

    /// <summary>
    /// Determines the subject of bus messages of user buying this service.
    /// </summary>
    public string ServiceBusName
    {
        get;
        set;
    }

    public string Description
    {
        get;
        set;
    }

    /// <summary>
    /// How long a service is supposed to be available once purchased in days.
    /// </summary>
    public int DurationInDays
    {
        get;
        set;
    }

    public DateTime CreatedAt
    {
        get;
        set;
    }

    public DateTime? ObsoletedAt
    {
        get;
        set;
    }

    public ICollection<UsedPaidUserService> UsedServices
    {
        get;
        set;
    } = new List<UsedPaidUserService>();
}
