﻿namespace BNine.Domain.Entities.PaidUserServices;

using System;
using Interfaces;
using User;

public class UsedPaidUserService : IBaseEntity
{
    public Guid Id
    {
        get;
        set;
    }

    public Guid UserId
    {
        get;
        set;
    }

    public User User
    {
        get;
        set;
    }

    public Guid ServiceId
    {
        get;
        set;
    }

    public PaidUserService Service
    {
        get;
        set;
    }

    /// <summary>
    /// Id of a corresponding Charge entity in MBanq.
    /// </summary>
    public int ChargeExternalId
    {
        get;
        set;
    }

    public DateTime CreatedAt
    {
        get;
        set;
    }

    public DateTime ValidTo
    {
        get;
        set;
    }

    /// <summary>
    /// Flag that part of the charge was completed because user didn't have full sum.
    /// </summary>
    public bool IsOverdue
    {
        get;
        set;
    }
}
