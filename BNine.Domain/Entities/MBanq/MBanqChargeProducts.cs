﻿namespace BNine.Domain.Entities.MBanq
{
    using System;
    using BNine.Domain.Interfaces;

    public class MBanqChargeProduct : IBaseEntity
    {
        public Guid Id
        {
            get; set;
        }

        public ChargeType ChargeType
        {
            get; set;
        }

        public long ExternalId
        {
            get; set;
        }
    }

    public enum ChargeType
    {
        PullFromCardFee,
        PushToCardFee
    }
}
