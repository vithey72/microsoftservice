﻿namespace BNine.Domain.Entities.CashbackProgram;

using System;
using Interfaces;

public class CashbackCategory : IBaseEntity
{
    public Guid Id
    {
        get;
        set;
    }

    /// <summary>
    /// Name denoting cashback category. May not be unique in the table, but among instances with IsDisabled = false
    /// it should be unique.
    /// </summary>
    public string Name
    {
        get;
        set;
    }

    public string ReadableName
    {
        get;
        set;
    }

    public string CategoryDescription
    {
        get;
        set;
    }

    public int[] MccCodes
    {
        get;
        set;
    }

    public string IconUrl
    {
        get;
        set;
    }

    /// <summary>
    /// If true, this cashback category is unavailable, yet or anymore.
    /// </summary>
    public bool IsDisabled
    {
        get;
        set;
    }

    public DateTime CreatedAt
    {
        get;
        set;
    }

    public DateTime? UpdatedAt
    {
        get;
        set;
    }

    /// <summary>
    /// For example 0.015 means 1.5% of cashback.
    /// </summary>
    public decimal Percentage
    {
        get;
        set;
    }
}
