﻿namespace BNine.Domain.Entities.CashbackProgram;

using Infrastructure;

public class UserCashbackStatistic : ValueObject
{
    public Guid UserId
    {
        get; set;
    }

    public decimal TotalEarnedAmount
    {
        get;
        set;
    }

    public DateTime EarnedAmountUpdatedAt
    {
        get;
        set;
    }

    protected override IEnumerable<object> GetAtomicValues()
    {
        yield return TotalEarnedAmount;
        yield return EarnedAmountUpdatedAt;
    }
}
