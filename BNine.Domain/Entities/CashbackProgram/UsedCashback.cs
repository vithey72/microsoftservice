﻿namespace BNine.Domain.Entities.CashbackProgram;

using Interfaces;

public class UsedCashback : IBaseEntity
{
    public Guid Id
    {
        get;
        set;
    }

    public Guid UserId
    {
        get;
        set;
    }

    public User.User User
    {
        get;
        set;
    }

    public int Year
    {
        get;
        set;
    }

    /// <summary>
    /// Numeric representation of month for which cashback was enabled.
    /// </summary>
    public byte Month
    {
        get;
        set;
    }

    /// <summary>
    /// Date of activation.
    /// </summary>
    public DateTime EnabledAt
    {
        get;
        set;
    }

    /// <summary>
    /// Date of end of period, or null if not yet known.
    /// </summary>
    public DateTime? ExpiredAt
    {
        get;
        set;
    }

    /// <summary>
    /// Ids of <see cref="CashbackCategory"/> entities.
    /// </summary>
    public Guid[] CashbackCategoriesIds
    {
        get;
        set;
    }

    public string[] CashbackCategoriesNames
    {
        get;
        set;
    }

    /// <summary>
    /// Cashback earned in this month.
    /// </summary>
    public decimal EarnedAmount
    {
        get;
        set;
    }

    /// <summary>
    /// Total sum of amounts of relevant transfers. Should be over minimum for user to be eligible for receiving cashback.
    /// </summary>
    public decimal SpentAmount
    {
        get;
        set;
    }

    /// <summary>
    /// Timestamp when above values were last updated.
    /// </summary>
    public DateTime EarnedAmountUpdatedAt
    {
        get;
        set;
    }

    /// <summary>
    /// Rating that user gave to selected categories out of 5. Null means no rating was given, or it's a legacy record.
    /// </summary>
    public int? UsersEvaluation
    {
        get;
        set;
    }
}
