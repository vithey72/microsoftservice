﻿namespace BNine.Domain.Entities;

using Enums;
using Interfaces;
using Newtonsoft.Json;

public class AdminApiUser: IBaseEntity
{
    public Guid Id
    {
        get; set;
    }

    public DateTime CreatedAt
    {
        get; set;
    }

    public string Email
    {
        get; set;
    }

    [JsonIgnore]
    public string Password
    {
        get; set;
    }

    public string FirstName
    {
        get; set;
    }

    public string LastName
    {
        get; set;
    }

    public AdminRole RoleId
    {
        get;
        set;
    }
}
