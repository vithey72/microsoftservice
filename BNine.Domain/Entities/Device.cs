﻿namespace BNine.Domain.Entities
{
    using System;
    using BNine.Domain.Interfaces;
    using BNine.Enums;

    public class Device : IBaseEntity
    {
        public Device()
        {
            CreatedAt = DateTime.UtcNow;
            UpdatedAt = DateTime.UtcNow;
        }

        public Guid Id
        {
            get; set;
        }

        public string ExternalDeviceId
        {
            get; set;
        }

        public string RegistrationId
        {
            get; set;
        }

        public MobilePlatform Type
        {
            get; set;
        }

        public Guid UserId
        {
            get; set;
        }

        public User.User User
        {
            get; set;
        }

        public DateTime? CreatedAt
        {
            get; set;
        }

        public DateTime? UpdatedAt
        {
            get; set;
        }

        public DateTime? DeletedAt
        {
            get; set;
        }
    }
}
