﻿namespace BNine.Domain.Entities;

using System;
using Interfaces;

public class UserPlaceholderText : IBaseEntity
{
    public Guid Id
    {
        get;
        set;
    }

    public Guid UserId
    {
        get;
        set;
    }

    public User.User User
    {
        get;
        set;
    }

    public string Key
    {
        get;
        set;
    }

    public string Value
    {
        get;
        set;
    }

    public DateTime CreatedAt
    {
        get;
        set;
    }

    public DateTime? UpdatedAt
    {
        get;
        set;
    }
}
