﻿namespace BNine.Domain.Entities.CheckCashing
{
    using System;
    using Enums.CheckCashing;
    using Interfaces;

    public class CheckCashingTransaction : IBaseEntity
    {
        public Guid Id
        {
            get; set;
        }

        public Guid UserId
        {
            get; set;
        }

        public BNine.Domain.Entities.User.User User
        {
            get; set;
        }

        public string AlltrustTransactionId
        {
            get; set;
        }

        public int AmountRequested
        {
            get; set;
        }

        public int AmountTotal
        {
            get; set;
        }

        /// <summary>
        /// Status of money transfer. Reflective on MBanq status.
        /// </summary>
        public CheckCashingPaymentStatus PaymentStatus
        {
            get; set;
        }

        /// <summary>
        /// Internal status of transaction inside our workflow.
        /// </summary>
        public CheckCashingTransactionStatus Status
        {
            get; set;
        }

        public DateTime CreatedDate
        {
            get; set;
        }

        public DateTime StatusDate
        {
            get; set;
        }

        /// <summary>
        /// Reference to maker that user set as his employer.
        /// Null if user chose not to set employer, not applicable if check is not payroll type.
        /// </summary>
        public Guid? CheckMakerId
        {
            get; set;
        }

        public CheckMaker CheckMaker
        {
            get; set;
        }
    }
}
