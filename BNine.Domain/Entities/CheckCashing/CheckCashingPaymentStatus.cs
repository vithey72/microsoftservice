﻿namespace BNine.Domain.Entities.CheckCashing
{
    public enum CheckCashingPaymentStatus
    {
        Unknown = 0,

        /// <summary>
        /// No funds were transferred.
        /// </summary>
        NotPaid = 1,

        /// <summary>
        /// Maximum allowed amount was transferred before start of clearing process.
        /// </summary>
        InstantFundsPaid = 2,

        /// <summary>
        /// Full amount was transferred instantly before start of clearing process.
        /// </summary>
        InstantPaidInFull = 3,

        /// <summary>
        /// Full amount was transferred after successful clearing.
        /// </summary>
        PaidInFull = 4,

        /// <summary>
        /// Rest of the funds were transferred after <see cref="InstantFundsPaid"/>.
        /// </summary>
        PaidRestOfFunds = 5,

        /// <summary>
        /// <see cref="InstantFundsPaid"/> was reverted as a result of failure to clear the check.
        /// </summary>
        InstantPaymentReverted = 6,
    }
}
