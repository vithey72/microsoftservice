﻿namespace BNine.Domain.Entities.CheckCashing
{
    using System;
    using Interfaces;

    /// <summary>
    /// Entity of an institution that pays people in checks.
    /// </summary>
    public class CheckMaker : IBaseEntity
    {
        public Guid Id
        {
            get; set;
        }

        /// <summary>
        /// aka ABA, id of the bank.
        /// </summary>
        public string MakerRoutingNumber
        {
            get; set;
        }

        /// <summary>
        /// Account number of the maker inside the bank.
        /// </summary>
        public string MakerAccountNumber
        {
            get; set;
        }

        public string MakerName
        {
            get; set;
        }
    }
}
