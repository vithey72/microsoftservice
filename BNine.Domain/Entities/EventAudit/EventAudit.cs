﻿namespace BNine.Domain.Entities.EventAudit;

using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

public class EventAudit
{

    public int Id
    {
        get;
        set;
    }

    public Guid? UserId
    {
        get;
        set;
    }

    public Guid EventId
    {
        get;
        set;
    }

    public string EventName
    {
        get;
        set;
    }

    public string EntityTypeName
    {
        get;
        set;
    }

    public DateTime EventTime
    {
        get;
        set;
    }

    public string RawEventModel
    {
        get;
        set;
    }

    public string ParsedEventModel
    {
        get;
        set;
    }

    public DbAuditEntry DbAuditEntry
    {
        get;
        set;
    }


}

public class DbAuditEntry
{
    public string AuditMessage
    {
        get;
        set;
    }

    public ErrorDetails ErrorDetails
    {
        get;
        set;
    }

    [JsonConverter(typeof(StringEnumConverter))]
    public EntityState EntityState
    {
        get;
        set;
    }

    public bool? IsSuccessful
    {
        get;
        set;
    }

    public DateTime CreatedAt
    {
        get;
        set;
    }

    public DateTime? ProcessedAt
    {
        get;
        set;
    }
}

public class ErrorDetails
{
    public string ErrorMessage
    {
        get;
        set;
    }

    public string StackTrace
    {
        get;
        set;
    }
}
