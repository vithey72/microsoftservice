﻿namespace BNine.Domain.Entities.Notification
{
    using System;
    using BNine.Domain.Interfaces;
    using BNine.Enums;

    public class Notification : IBaseEntity
    {
        public Notification()
        {
            CreatedAt = DateTime.UtcNow;
        }

        public Guid Id
        {
            get; set;
        }

        public DateTime CreatedAt
        {
            get; set;
        }

        public string Title
        {
            get; set;
        }

        public string Category
        {
            get; set;
        }

        public string Text
        {
            get; set;
        }

        public bool IsRead
        {
            get; set;
        }

        public Guid UserId
        {
            get; set;
        }

        public User.User User
        {
            get; set;
        }

        public string Deeplink
        {
            get; set;
        }

        public string Sub1
        {
            get; set;
        }

        public bool IsImportant
        {
            get; set;
        }

        public NotificationTypeEnum Type
        {
            get; set;
        }

        public NotificationType NotificationType
        {
            get; set;
        }

        public Guid? HookId
        {
            get; set;
        }
    }
}
