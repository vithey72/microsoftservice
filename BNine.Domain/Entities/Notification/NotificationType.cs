﻿namespace BNine.Domain.Entities.Notification
{
    using BNine.Enums;

    public class NotificationType
    {
        public NotificationTypeEnum Id
        {
            get; set;
        }

        public string Name
        {
            get; set;
        }

        public string Icon
        {
            get; set;
        }

        public List<Notification> Notifications
        {
            get; set;
        }
    }
}
