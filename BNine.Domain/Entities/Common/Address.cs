﻿namespace BNine.Domain.Entities.Common
{
    using System;
    using System.Collections.Generic;
    using BNine.Domain.Interfaces;
    using Infrastructure;

    public class Address : ValueObject, IHistoryEntry
    {
        public Guid UserId
        {
            get; set;
        }

        public DateTime SysStartTime
        {
            get;
            set;
        }

        public string ZipCode
        {
            get; set;
        }

        public string City
        {
            get; set;
        }

        public string State
        {
            get; set;
        }

        public string AddressLine
        {
            get; set;
        }

        public string Unit
        {
            get; set;
        }

        public int? ExternalId
        {
            get; set;
        }

        protected override IEnumerable<object> GetAtomicValues()
        {
            yield return ZipCode;
            yield return City;
            yield return State;
            yield return AddressLine;
            yield return Unit;
            yield return ExternalId;
        }
    }
}
