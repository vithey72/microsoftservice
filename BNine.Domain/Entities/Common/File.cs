﻿namespace BNine.Domain.Entities.Common
{
    using System;
    using Interfaces;

    public class File : IBaseEntity
    {
        public Guid Id
        {
            get;
            set;
        }

        public string Url
        {
            get;
            set;
        }

        public string FileName
        {
            get;
            set;
        }

        public string ContentType
        {
            get;
            set;
        }
    }
}
