﻿namespace BNine.Domain.Entities.Common
{
    using System.Collections.Generic;
    using Enums;
    using Infrastructure;

    public class LocalizedText : ValueObject
    {
        public Language Language
        {
            get; set;
        }

        public string Text
        {
            get; set;
        }

        protected override IEnumerable<object> GetAtomicValues()
        {
            yield return Language;
            yield return Text;
        }
    }
}
