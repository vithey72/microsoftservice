﻿namespace BNine.Domain.Entities
{
    using System;
    using Enums;
    using Interfaces;

    public class LoanAccount : IBaseEntity
    {
        public LoanAccount()
        {
            CreatedAt = DateTime.UtcNow;
            UpdatedAt = DateTime.UtcNow;
        }

        public Guid Id
        {
            get;
            set;
        }

        public int ExternalId
        {
            get;
            set;
        }

        public int ProductId
        {
            get;
            set;
        }

        public LoanAccountStatus Status
        {
            get;
            set;
        }

        public Guid UserId
        {
            get;
            set;
        }

        public User.User User
        {
            get;
            set;
        }

        public DateTime? CreatedAt
        {
            get; set;
        }

        public DateTime? UpdatedAt
        {
            get; set;
        }

        public DateTime? DeletedAt
        {
            get; set;
        }
    }
}
