﻿namespace BNine.Domain.Entities.Features
{
    using System;
    using BNine.Domain.Interfaces;
    using Enums;

    public class FeatureGroup : IBaseEntity
    {
        public FeatureGroup()
        {
            CreatedAt = DateTime.Now;
            UpdatedAt = DateTime.Now;
        }

        #region BaseEntity
        public Guid Id
        {
            get;
            set;
        }

        public DateTime? CreatedAt
        {
            get; set;
        }

        public DateTime? UpdatedAt
        {
            get; set;
        }

        public DateTime? DeletedAt
        {
            get; set;
        }
        #endregion

        public Guid GroupId
        {
            get;
            set;
        }

        public Group Group
        {
            get;
            set;
        }

        public Guid FeatureId
        {
            get;
            set;
        }

        public Feature Feature
        {
            get;
            set;
        }

        public bool? IsEnabled
        {
            get; set;
        }

        public MobilePlatform? MobilePlatform
        {
            get;
            set;
        }

        public Version MinAppVersion
        {
            get;
            set;
        }

        public string Settings
        {
            get;
            set;
        }

        public DateTime? MinRegistrationDate
        {
            get;
            set;
        }

        public int? MinAccountAgeHours
        {
            get;
            set;
        }

        public int? MaxAccountAgeHours
        {
            get;
            set;
        }
    }
}
