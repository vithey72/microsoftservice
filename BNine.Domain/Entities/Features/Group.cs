﻿namespace BNine.Domain.Entities.Features
{
    using System;
    using System.Collections.Generic;
    using BNine.Domain.Interfaces;

    public abstract class Group : IBaseEntity
    {
        public Group()
        {
            CreatedAt = DateTime.Now;
            UpdatedAt = DateTime.Now;
        }

        #region BaseEntity
        public Guid Id
        {
            get;
            set;
        }

        public DateTime? CreatedAt
        {
            get; set;
        }

        public DateTime? UpdatedAt
        {
            get; set;
        }

        public DateTime? DeletedAt
        {
            get; set;
        }
        #endregion

        public string Name
        {
            get;
            set;
        }

        public GroupKind Kind
        {
            get;
            set;
        }

        public List<FeatureGroup> FeatureGroups
        {
            get;
            set;
        } = new List<FeatureGroup>();

        public List<UserGroup> UserGroups
        {
            get;
            set;
        } = new List<UserGroup>();
    }

    public enum GroupKind
    {
        Default = 0,
        Sample = 10,
        UserList = 20,
        Personal = 50,
    }
}
