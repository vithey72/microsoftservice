﻿namespace BNine.Domain.Entities.Features
{
    using System;
    using System.Collections.Generic;
    using BNine.Domain.Interfaces;

    public class Feature : IBaseEntity
    {
        public Feature()
        {
            CreatedAt = DateTime.Now;
            UpdatedAt = DateTime.Now;
        }

        #region BaseEntity
        public Guid Id
        {
            get;
            set;
        }

        public DateTime? CreatedAt
        {
            get; set;
        }

        public DateTime? UpdatedAt
        {
            get; set;
        }

        public DateTime? DeletedAt
        {
            get; set;
        }
        #endregion

        public string Name
        {
            get;
            set;
        }

        public List<FeatureGroup> FeatureGroups
        {
            get;
            set;
        }

        public string SettingsSchema
        {
            get;
            set;
        }
    }
}
