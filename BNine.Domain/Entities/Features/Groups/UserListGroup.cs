﻿namespace BNine.Domain.Entities.Features.Groups
{
    using System.Collections.Generic;

    public class UserListGroup : Group
    {
        public List<UserGroup> UserGroups
        {
            get;
            set;
        }
    }
}
