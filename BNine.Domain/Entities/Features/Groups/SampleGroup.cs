﻿namespace BNine.Domain.Entities.Features.Groups
{
    public class SampleGroup : Group
    {
        public int Seed
        {
            get;
            set;
        } = 0;

        /// <summary>
        /// Part of an auditory, [0; 1]
        /// </summary>
        public double Rollout
        {
            get;
            set;
        } = 1;

        /// <summary>
        /// Start of a subrange of an auditory, [0; 1] * Rollout
        /// </summary>
        public double SubRangeStart
        {
            get;
            set;
        } = 0;

        /// <summary>
        /// End of a subrange of an auditory, [0; 1] * Rollout
        /// </summary>
        public double SubRangeEnd
        {
            get;
            set;
        } = 1;
    }
}
