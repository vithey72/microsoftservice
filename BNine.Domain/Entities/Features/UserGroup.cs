﻿namespace BNine.Domain.Entities.Features
{
    using System;
    using BNine.Domain.Interfaces;
    using BNine.Domain.Entities.User;

    public class UserGroup : IBaseEntity
    {
        public UserGroup()
        {
            CreatedAt = DateTime.Now;
            UpdatedAt = DateTime.Now;
        }

        #region BaseEntity
        public Guid Id
        {
            get;
            set;
        }

        public DateTime? CreatedAt
        {
            get; set;
        }

        public DateTime? UpdatedAt
        {
            get; set;
        }

        public DateTime? DeletedAt
        {
            get; set;
        }
        #endregion

        public Guid UserId
        {
            get;
            set;
        }

        public User User
        {
            get;
            set;
        }

        public Guid GroupId
        {
            get; set;
        }

        public Group Group
        {
            get;
            set;
        }
    }
}
