﻿namespace BNine.Domain.Entities
{
    using System;
    using BNine.Domain.Interfaces;

    public class LoanSpecialOffer : IBaseEntity, ISoftDeletable
    {
        public LoanSpecialOffer()
        {
            CreatedAt = DateTime.UtcNow;
            UpdatedAt = DateTime.UtcNow;
        }

        public Guid Id
        {
            get; set;
        }

        public Guid UserId
        {
            get; set;
        }

        public User.User User
        {
            get; set;
        }

        public decimal Value
        {
            get; set;
        }

        public DateTime? CreatedAt
        {
            get; set;
        }

        public DateTime? UpdatedAt
        {
            get; set;
        }

        public DateTime? DeletedAt
        {
            get; set;
        }

        public bool IsDeleted
        {
            get; set;
        }
    }
}
