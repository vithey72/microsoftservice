﻿namespace BNine.Domain.Entities.UserQuestionnaire
{
    using System;
    using System.Collections.Generic;
    using Enums;
    using Interfaces;

    /// <summary>
    /// Entity of a question inside user questionnaire. Can be either open or closed type.
    /// If it's a close type, then entity references list of possible responses.
    /// </summary>
    public class Question : IBaseEntity
    {
        public Question()
        {
        }

        /// <summary>
        /// Create an open type question.
        /// </summary>
        public Question(Guid id, string title, int order, int characterLimit, string tag = null, string header = null,
            string mainButtonText = null, string altButtonText = null)
            : this(id, title, order, tag, header, mainButtonText, altButtonText)
        {
            QuestionType = UserQuestionType.OpenEnded;
            OpenResponseCharacterLimit = characterLimit;
        }

        /// <summary>
        /// Create a closed type question.
        /// </summary>
        public Question(Guid id, string title, int order, QuestionResponse[] responses, bool multipleResponses, string tag = null,
            string header = null, string mainButtonText = null, string altButtonText = null)
            : this(id, title, order, tag, header, mainButtonText, altButtonText)
        {
            QuestionType = multipleResponses
                ? UserQuestionType.ClosedWithMultipleChoices
                : UserQuestionType.ClosedWithSingleChoice;
            ClosedResponses = responses;
        }

        private Question(Guid id, string title, int order, string tag, string header, string mainButtonText = null,
            string altButtonText = null)
        {
            Id = id;
            Title = title;
            Tag = tag;
            Order = order;
            Header = header;
            MainButtonText = mainButtonText;
            AltButtonText = altButtonText;
        }

        public Guid Id
        {
            get;
            set;
        }

        public string Title
        {
            get;
            set;
        }

        /// <summary>
        /// String without spaces that can be used to mark question in telemetry data.
        /// </summary>
        public string Tag
        {
            get;
            set;
        }

        public UserQuestionType QuestionType
        {
            get;
            set;
        }

        public int Order
        {
            get;
            set;
        }

        public ICollection<QuestionResponse> ClosedResponses
        {
            get; set;
        } = new List<QuestionResponse>();


        public int OpenResponseCharacterLimit
        {
            get;
            set;
        }

        // TODO: regex for open response

        public DateTime? DeletedAt
        {
            get;
            set;
        }

        [Obsolete]
        public string Header
        {
            get;
            set;
        }

        [Obsolete]
        public string AltButtonText
        {
            get;
            set;
        }

        [Obsolete]
        public string MainButtonText
        {
            get;
            set;
        }

        public ICollection<QuestionQuestionSet> QuestionSets
        {
            get;
            set;
        } = new List<QuestionQuestionSet>();
    }
}
