﻿namespace BNine.Domain.Entities.UserQuestionnaire
{
    using System;

    /// <summary>
    /// Entity of a M-M relationship between submitted and possible responses to a question.
    /// </summary>
    public class SubmittedQuestionResponseQuestionResponse
    {
        public Guid SubmittedQuestionResponseId
        {
            get;
            set;
        }

        public SubmittedQuestionResponse SubmittedQuestionResponse
        {
            get;
            set;
        }

        public Guid QuestionResponseId
        {
            get;
            set;
        }
        public QuestionResponse QuestionResponse
        {
            get;
            set;
        }

        public DateTime CreatedAt
        {
            get;
            set;
        }
    }
}
