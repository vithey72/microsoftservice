﻿namespace BNine.Domain.Entities.UserQuestionnaire
{
    using System;
    using System.Collections.Generic;
    using Interfaces;

    /// <summary>
    /// Entity of a questionnaire that has been filled and submitted by the user.
    /// </summary>
    public class SubmittedUserQuestionnaire : IBaseEntity
    {
        public Guid Id
        {
            get;
            set;
        }

        public Guid UserId
        {
            get;
            set;
        }

        public Guid QuestionSetId
        {
            get;
            set;
        }

        public QuestionSet QuestionSet
        {
            get;
            set;
        }

        public DateTime CompletionDate
        {
            get;
            set;
        }

        public ICollection<SubmittedQuestionResponse> Responses

        {
            get;
            set;
        }
    }
}
