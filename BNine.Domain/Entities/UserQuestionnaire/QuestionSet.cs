﻿namespace BNine.Domain.Entities.UserQuestionnaire
{
    using System;
    using System.Collections.Generic;
    using Interfaces;

    /// <summary>
    /// Entity of a questionnaire. Contains a set of questions.
    /// </summary>
    public class QuestionSet : IBaseEntity
    {
        public Guid Id
        {
            get;
            set;
        }

        /// <summary>
        /// Unique name to identify a questionnaire.
        /// </summary>
        public string Name
        {
            get;
            set;
        }

        public string Header
        {
            get;
            set;
        }

        public string HeaderSubtitle
        {
            get;
            set;
        }

        public string MainButtonText
        {
            get;
            set;
        }

        public string AltButtonText
        {
            get;
            set;
        }

        public ICollection<QuestionQuestionSet> Questions
        {
            get;
            set;
        } = new List<QuestionQuestionSet>();
    }
}
