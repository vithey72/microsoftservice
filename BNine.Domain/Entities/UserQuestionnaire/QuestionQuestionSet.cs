﻿namespace BNine.Domain.Entities.UserQuestionnaire
{
    using System;

    /// <summary>
    /// Entity allowing for M-M relationship between questions and questionnaires.
    /// </summary>
    public class QuestionQuestionSet
    {
        public Guid QuestionId
        {
            get;
            set;
        }

        public Question Question
        {
            get;
            set;
        }

        public Guid QuestionSetId
        {
            get;
            set;
        }

        public QuestionSet QuestionSet
        {
            get;
            set;
        }
    }
}
