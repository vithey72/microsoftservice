﻿namespace BNine.Domain.Entities.UserQuestionnaire
{
    using System;
    using System.Collections.Generic;
    using Interfaces;

    /// <summary>
    /// Entity of a submitted response to a question inside a completed questionnaire.
    /// In a M-M relationship with possible response entities.
    /// </summary>
    public class SubmittedQuestionResponse : IBaseEntity
    {
        public Guid Id
        {
            get;
            set;
        }
        
        public DateTime CreatedAt
        {
            get;
            set;
        }

        public Guid CompletedUserQuestionnaireId
        {
            get;
            set;
        }

        public SubmittedUserQuestionnaire CompletedUserQuestionnaire
        {
            get;
            set;
        }

        public Guid QuestionId
        {
            get;
            set;
        }

        public Question Question
        {
            get;
            set;
        }

        public ICollection<SubmittedQuestionResponseQuestionResponse> Responses
        {
            get;
            set;
        } = new List<SubmittedQuestionResponseQuestionResponse>();

        public string OpenQuestionResponse
        {
            get;
            set;
        }
    }
}
