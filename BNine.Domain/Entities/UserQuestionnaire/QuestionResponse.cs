﻿namespace BNine.Domain.Entities.UserQuestionnaire
{
    using System;
    using System.Collections.Generic;
    using Interfaces;

    /// <summary>
    /// Possible response to a closed-type question.
    /// </summary>
    public class QuestionResponse : IBaseEntity
    {
        public QuestionResponse()
        {
        }

        public QuestionResponse(Guid id, string text, int order)
        {
            Id = id;
            Text = text;
            Order = order;
        }

        public Guid Id
        {
            get;
            set;
        }

        public Guid QuestionId
        {
            get;
            set;
        }

        public Question Question
        {
            get;
            set;
        }

        public string Text
        {
            get;
            set;
        }

        public int Order
        {
            get;
            set;
        }

        public DateTime? DeletedAt
        {
            get;
            set;
        }

        public ICollection<SubmittedQuestionResponseQuestionResponse> SubmittedQuestionResponses
        {
            get;
            set;
        }
    }
}
