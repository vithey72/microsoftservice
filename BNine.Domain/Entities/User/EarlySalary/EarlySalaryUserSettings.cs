﻿namespace BNine.Domain.Entities.User.UserDetails.Settings
{
    using System;
    using System.Collections.Generic;
    using BNine.Domain.Interfaces;
    using Infrastructure;

    public class EarlySalaryUserSettings : ValueObject, IHistoryEntry
    {
        public Guid UserId
        {
            get; set;
        }

        public DateTime SysStartTime
        {
            get;
            set;
        }

        public bool IsEarlySalaryEnabled
        {
            get; set;
        }

        public DateTime EarlySalarySubscriptionDate
        {
            get; set;
        }

        protected override IEnumerable<object> GetAtomicValues()
        {
            yield return EarlySalarySubscriptionDate;
            yield return IsEarlySalaryEnabled;
        }
    }
}
