﻿namespace BNine.Domain.Entities.User.EarlySalary
{
    using System;
    using System.Collections.Generic;
    using BNine.Domain.Infrastructure;
    using BNine.Domain.Interfaces;

    public class EarlySalaryManualRequest : IBaseEntity, IHaveDomainEvents
    {
        public EarlySalaryManualRequest()
        {
            CreatedAt = DateTime.UtcNow;
        }

        public Guid Id
        {
            get; set;
        }

        public DateTime CreatedAt
        {
            get; set;
        }

        public User User
        {
            get; set;
        }

        public Guid UserId
        {
            get; set;
        }

        public decimal? AmountAllocation
        {
            get; set;
        }

        public decimal? PercentAllocation
        {
            get; set;
        }

        public string Email
        {
            get; set;
        }

        public string EmployerName
        {
            get; set;
        }

        public string EmployerContactPerson
        {
            get; set;
        }

        public string EmployerEmail
        {
            get; set;
        }

        public List<DomainEvent> DomainEvents
        {
            get; set;
        } = new List<DomainEvent>();
    }
}
