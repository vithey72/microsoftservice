﻿namespace BNine.Domain.Entities.User.EarlySalary
{
    using System;
    using Entities.User;

    public class EarlySalaryWidgetConfiguration
    {
        public EarlySalaryWidgetConfiguration()
        {
            CreatedAt = DateTime.UtcNow;
            UpdatedAt = DateTime.UtcNow;
        }

        public Guid WidgetUserId
        {
            get;
            set;
        }

        [Obsolete]
        public DateTime? TokenValidTo
        {
            get;
            set;
        }

        public string Token
        {
            get;
            set;
        }

        public Guid UserId
        {
            get;
            set;
        }

        public User User
        {
            get;
            set;
        }

        public DateTime CreatedAt
        {
            get; set;
        }

        public DateTime? UpdatedAt
        {
            get; set;
        }

        public DateTime? DeletedAt
        {
            get; set;
        }
    }
}
