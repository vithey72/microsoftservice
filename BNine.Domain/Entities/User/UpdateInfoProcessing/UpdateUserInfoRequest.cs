﻿namespace BNine.Domain.Entities.User.UpdateInfoProcessing
{
    using System;
    using BNine.Domain.Interfaces;
    using BNine.Enums;

    public class UpdateUserInfoRequest : IBaseEntity
    {
        public Guid Id
        {
            get; set;
        }

        public Guid UserId
        {
            get; set;
        }

        public UserInfoField UpdatedFieldType
        {
            get; set;
        }

        public string UpdatedFieldValue
        {
            get; set;
        }

        public DateTime RequestDate
        {
            get; set;
        }

        public string ConfirmationCode
        {
            get; set;
        }

        public RequestProcessingStatus Status
        {
            get; set;
        }
    }
}
