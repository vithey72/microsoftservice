﻿namespace BNine.Domain.Entities.User
{
    using System;
    using System.Collections.Generic;
    using BNine.Domain.Infrastructure;
    using CheckCashing;

    public class AlltrustCustomerSettings : ValueObject
    {
        public Guid UserId
        {
            get; set;
        }

        /// <summary>
        /// CustomerId in Alltrust system that is connected with this user.
        /// </summary>
        public string AlltrustCustomerId
        {
            get; set;
        }

        /// <summary>
        /// Id of the maker of the check that user chose as their employer.
        /// </summary>
        public Guid? CurrentEmployerId
        {
            get; set;
        }

        public CheckMaker CurrentEmployer
        {
            get; set;
        }

        public DateTime? CreatedAt
        {
            get; set;
        }

        public DateTime? UpdatedAt
        {
            get; set;
        }

        protected override IEnumerable<object> GetAtomicValues()
        {
            yield return AlltrustCustomerId;
            yield return CurrentEmployerId;
            yield return CreatedAt;
            yield return UpdatedAt;
        }
    }
}
