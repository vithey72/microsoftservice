﻿namespace BNine.Domain.Entities.User;

using System;
using System.Collections.Generic;
using Banners;
using BNine.Domain.Entities.BankAccount;
using BNine.Domain.Entities.ExternalCards;
using BNine.Domain.Entities.Features;
using BNine.Domain.Entities.Notification;
using BNine.Domain.Entities.TariffPlan;
using BNine.Domain.Entities.Transfers;
using BNine.Domain.Infrastructure;
using CashbackProgram;
using Common;
using EarlySalary;
using Enums;
using Interfaces;
using Newtonsoft.Json;
using PaidUserServices;
using TransfersSecurityOTPs;
using UserDetails;
using UserDetails.Settings;

public class User
    : IBaseEntity
    , IHaveDomainEvents
    , IHistoryEntry
{
    private UserStatus _status;

    public Guid Id
    {
        get; set;
    }

    public DateTime CreatedAt
    {
        get; set;
    }

    public string Phone
    {
        get; set;
    }

    public string Email
    {
        get; set;
    }

    [JsonIgnore]
    public string Password
    {
        get; set;
    }

    public DateTime? DateOfBirth
    {
        get; set;
    }

    public string FirstName
    {
        get; set;
    }

    public string LastName
    {
        get; set;
    }

    public UserStatus Status
    {
        get => _status;
        set
        {
            _status = value;
            // fill this status with a fallback value, unless it was previously explicitly set as a non-round step.
            if ((int)StatusDetailed % 10 == 0)
            {
                StatusDetailed = (UserStatusDetailed)((int)value * 10);
            }
        }
    }

    public UserStatusDetailed StatusDetailed
    {
        get; set;
    }

    public bool IsBlocked
    {
        get; set;
    }

    public Guid? UserBlockReasonId
    {
        get; set;
    }

    public BlockUserReason UserBlockReason
    {
        get; set;
    }

    public Guid? UserCloseReasonId
    {
        get; set;
    }

    public BlockUserReason? UserCloseReason
    {
        get; set;
    }
    public DateTime? ClosedAt
    {
        get;
        set;
    }

    public string ProfilePhotoUrl
    {
        get; set;
    }

    public Guid AppsflyerId
    {
        get; set;
    }

    public Address Address
    {
        get; set;
    }

    public ToSAcceptance ToSAcceptance
    {
        get; set;
    }

    public EarlySalaryUserSettings EarlySalarySettings
    {
        get; set;
    }

    public EarlySalaryWidgetConfiguration EarlySalaryWidgetConfiguration
    {
        get; set;
    }

    public TruvAccount TruvAccount
    {
        get; set;
    }

    public int ExternalId
    {
        get; set;
    }

    public int? ExternalClientId
    {
        get; set;
    }

    public Identity Identity
    {
        get; set;
    }

    public Document Document
    {
        get; set;
    }

    [JsonIgnore]
    public CurrentAccount CurrentAccount
    {
        get; set;
    }

    [JsonIgnore]
    public ICollection<CurrentAdditionalAccount> CurrentAdditionalAccounts
    {
        get;
        set;
    } = new List<CurrentAdditionalAccount>();

    public DateTime? ActivatedAt
    {
        get; set;
    }

    [Obsolete]
    [JsonIgnore]
    public ClientRating ClientRating
    {
        get; set;
    }

    [Obsolete]
    [JsonIgnore]
    public ClientLoanSettings ClientLoanSettings
    {
        get; set;
    }

    public UserSettings Settings
    {
        get; set;
    }

    /// <summary>
    /// Entity that encapsulates Alltrust customer properties.
    /// Null if customer is not yet enrolled in Alltrust.
    /// </summary>
    [JsonIgnore]
    public AlltrustCustomerSettings AlltrustCustomerSettings
    {
        get; set;
    }

    public ArrayCustomerSettings ArrayCustomerSettings
    {
        get; set;
    }

    [JsonIgnore]
    public ICollection<ExternalBankAccount> BankAccounts
    {
        get; set;
    } = new List<ExternalBankAccount>();

    [JsonIgnore]
    public ICollection<Device> Devices
    {
        get; set;
    } = new List<Device>();

    [JsonIgnore]
    public ICollection<Notification> Notifications
    {
        get; set;
    } = new List<Notification>();

    [JsonIgnore]
    public ICollection<DebitCard> DebitCards
    {
        get; set;
    } = new List<DebitCard>();

    [JsonIgnore]
    public ICollection<LoanAccount> LoanAccounts
    {
        get; set;
    } = new List<LoanAccount>();

    [JsonIgnore]
    public ICollection<ExternalCard> ExternalCards
    {
        get; set;
    } = new List<ExternalCard>();

    [JsonIgnore]
    public ICollection<KYCVerificationResult> KycVerificationResults
    {
        get; set;
    } = new List<KYCVerificationResult>();

    [JsonIgnore]
    public IList<LoanSpecialOffer> LoanSpecialOffers
    {
        get; set;
    } = new List<LoanSpecialOffer>();

    [JsonIgnore]
    public IList<LoanCalculatedLimit> LoanCalculatedLimits
    {
        get; set;
    } = new List<LoanCalculatedLimit>();

    [JsonIgnore]
    public IList<OnboardingEvent> OnboardingErrors
    {
        get; set;
    } = new List<OnboardingEvent>();

    [JsonIgnore]
    public List<DomainEvent> DomainEvents
    {
        get; set;
    } = new();

    [JsonIgnore]
    public List<UserGroup> UserGroups
    {
        get; set;
    } = new();

    public DateTime SysStartTime
    {
        get; set;
    }

    public string Groups
    {
        get; set;
    }

    [JsonIgnore]
    public List<UserTariffPlan> UserTariffPlans
    {
        get; set;
    }

    [JsonIgnore]
    public List<UsedCashback> UsedCashbacks
    {
        get; set;
    }

    [JsonIgnore]
    public UserCashbackStatistic CashbackStatistic
    {
        get; set;
    }

    [JsonIgnore]
    public AdvanceStats AdvanceStats
    {
        get; set;
    }

    [JsonIgnore]
    public UserSyncedStats UserSyncedStats
    {
        get; set;
    }

    [JsonIgnore]
    public List<FavoriteUserTransfer> FavoriteUserTransfer
    {
        get; set;
    }

    [JsonIgnore]
    public List<SeenMarketingBanner> MarketingBannersSeen
    {
        get;
        set;
    }

    [JsonIgnore]
    public List<UsedPaidUserService> UsedPaidUserServices
    {
        get;
        set;
    }

    [JsonIgnore]
    public List<TransfersSecurityOTP> TransfersSecurityOTPs
    {
        get;
        set;
    }

    [JsonIgnore]
    public List<PotentialAchIncomeTransfer> PotentialAchIncomeTransfers
    {
        get;
        set;
    } = new();


    [JsonIgnore]
    public List<UserPlaceholderText> UserPlaceholderTexts
    {
        get;
        set;
    }

    [JsonIgnore]
    public List<TransferErrorLog> TransferErrorLogs
    {
        get;
        set;
    }

    [JsonIgnore]
    public List<UserActivityCooldown> UserActivityCooldowns
    {
        get;
        set;
    }

    [JsonIgnore]
    public AccountClosureSchedule AccountClosureSchedule
    {
        get; set;
    }

    public void CloseClient(Guid blockReasonId)
    {
        Status = UserStatus.Closed;
        UserCloseReasonId = blockReasonId;
        ClosedAt = DateTime.Now;
    }
}
