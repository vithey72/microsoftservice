﻿namespace BNine.Domain.Entities.User
{
    using System;
    using System.Collections.Generic;
    using Infrastructure;
    using Interfaces.EventAudit;

    public class AdvanceStats : ValueObject, IDbEventAuditableEntity
    {
        public Guid UserId
        {
            get; set;
        }

        public Guid? EventId
        {
            get; set;
        }

        public string OfferId
        {
            get;
            set;
        }

        public decimal AvailableLimit
        {
            get;
            set;
        }

        public decimal ExtensionPrice
        {
            get;
            set;
        }

        public decimal ExtensionLimit
        {
            get;
            set;
        }

        /// <summary>
        /// UTC date of when the Extension offer expires.
        /// </summary>
        public DateTime? ExtensionExpiresAt
        {
            get;
            set;
        }

        public DelayedBoostSettings DelayedBoostSettings
        {
            get;
            set;
        }

        public bool HasBoostLimit
        {
            get;
            set;
        }

        public decimal BoostLimit1
        {
            get;
            set;
        }

        public decimal BoostLimit1Price
        {
            get;
            set;
        }

        public int BoostLimit1HoursToDelay
        {
            get;
            set;
        }

        public decimal BoostLimit2
        {
            get;
            set;
        }

        public decimal BoostLimit2Price
        {
            get;
            set;
        }

        public int BoostLimit2HoursToDelay
        {
            get;
            set;
        }

        public decimal BoostLimit3
        {
            get;
            set;
        }

        public decimal BoostLimit3Price
        {
            get;
            set;
        }

        public int BoostLimit3HoursToDelay
        {
            get;
            set;
        }

        /// <summary>
        /// Date when message that projects into this entity was created.
        /// </summary>
        public DateTime UpdatedAt
        {
            get;
            set;
        }

        public decimal Rating
        {
            get;
            set;
        }

        public decimal Slider1
        {
            get;
            set;
        }
        public decimal Slider2
        {
            get;
            set;
        }
        public decimal Slider3
        {
            get;
            set;
        }
        public decimal Slider4
        {
            get;
            set;
        }


        public bool IsEligibleForExtension() => ExtensionExpiresAt != null && ExtensionExpiresAt.Value.Date >= DateTime.Now.Date && ExtensionLimit != 0;

        protected override IEnumerable<object> GetAtomicValues()
        {
            yield return AvailableLimit;
            yield return ExtensionPrice;
            yield return ExtensionLimit;
            yield return ExtensionExpiresAt;
            yield return Rating;
            yield return Slider1;
            yield return Slider2;
            yield return Slider3;
            yield return Slider4;
            yield return BoostLimit1HoursToDelay;
            yield return BoostLimit2HoursToDelay;
            yield return BoostLimit3HoursToDelay;
        }
    }

    public class DelayedBoostSettings
    {
        public int DelayedBoostId
        {
            get;
            set;
        }

        public bool ReadyToDisburse
        {
            get;
            set;
        }

        public DateTime ActivationDate
        {
            get;
            set;
        }

        public bool SyncedWithData
        {
            get;
            set;
        }
    }
}
