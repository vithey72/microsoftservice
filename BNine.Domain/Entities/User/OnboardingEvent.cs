﻿namespace BNine.Domain.Entities
{
    using System;
    using BNine.Domain.Interfaces;
    using BNine.Enums;

    public class OnboardingEvent
        : IBaseEntity
    {
        public OnboardingEvent()
        {
            CreatedAt = DateTime.UtcNow;
        }

        public Guid Id
        {
            get; set;
        }

        public DateTime CreatedAt
        {
            get; set;
        }

        public UserStatus Step
        {
            get; set;
        }

        public UserStatusDetailed StepDetailed
        {
            get; set;
        }

        public string Message
        {
            get; set;
        }

        public User.User User
        {
            get; set;
        }

        public Guid UserId
        {
            get; set;
        }

        public bool Succeeded
        {
            get; set;
        }

        public EventSourcePlatform SourcePlatform
        {
            get;
            set;
        }
    }
}
