﻿namespace BNine.Domain.Entities.User
{
    using System;
    using BNine.Domain.Interfaces;

    [Obsolete]
    public class ClientRating : IHistoryEntry
    {
        public DateTime SysStartTime
        {
            get;
            set;
        }

        public Guid UserId
        {
            get; set;
        }

        public decimal CreditRating
        {
            get; set;
        }

        public decimal CurrentPaycheckDeposits
        {
            get; set;
        }

        public decimal PaycheckDepositHistory
        {
            get; set;
        }

        public decimal CardTransactions
        {
            get; set;
        }

        public decimal OnTimeRepaymentsOfAdvances
        {
            get; set;
        }
    }
}
