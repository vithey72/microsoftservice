﻿namespace BNine.Domain.Entities.User
{
    using System;
    using Enums;
    using Interfaces;
    using Newtonsoft.Json;

    public class KYCVerificationResult : IBaseEntity
    {
        public KYCVerificationResult()
        {
            CreatedAt = DateTime.UtcNow;
        }

        public Guid Id
        {
            get; set;
        }

        public string EntityToken
        {
            get; set;
        }

        /// <summary>
        /// Guid of transaction in Socure, if this KYC provider was used.
        /// </summary>
        public Guid? EvaluationExternalId
        {
            get; set;
        }

        public string EvaluationToken
        {
            get; set;
        }

        [Obsolete]
        public string Status // TODO: 2- change to enum
        {
            get; set;
        }

        /// <summary>
        /// Alloy or Socure.
        /// </summary>
        public EvaluationProviderEnum EvaluationProvider
        {
            get; set;
        }

        /// <summary>
        /// Decision about this user.
        /// </summary>
        public KycStatusEnum StatusEnum
        {
            get; set;
        }

        public DateTime CreatedAt
        {
            get; set;
        }

        public string RawEvaluation
        {
            get; set;
        }

        public Guid UserId
        {
            get; set;
        }

        [JsonIgnore]
        public User User
        {
            get; set;
        }

        public string OverridenByRule
        {
            get; set;
        }
    }
}
