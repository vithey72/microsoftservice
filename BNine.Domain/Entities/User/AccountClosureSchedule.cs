﻿namespace BNine.Domain.Entities.User;

using BNine.Domain.Interfaces;

public class AccountClosureSchedule : IBaseEntity
{
    public Guid Id
    {
        get; set;
    }

    public DateTime CreatedAt
    {
        get; set;
    }

    public Guid UserId
    {
        get; set;
    }

    public User User
    {
        get; set;
    }
}
