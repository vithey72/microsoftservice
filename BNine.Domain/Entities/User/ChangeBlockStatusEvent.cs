﻿namespace BNine.Domain.Entities.User
{
    using System;
    using BNine.Domain.Interfaces;
    using BNine.Enums;

    public class ChangeBlockStatusEvent : IBaseEntity
    {
        public ChangeBlockStatusEvent()
        {
            CreatedAt = DateTime.UtcNow;
        }

        public Guid Id
        {
            get; set;
        }

        public UserBlockAction Action
        {
            get; set;
        }

        public Guid? UserBlockReasonId
        {
            get; set;
        }

        public BlockUserReason UserBlockReason
        {
            get; set;
        }

        public User User
        {
            get; set;
        }

        public Guid UserId
        {
            get; set;
        }

        public DateTime CreatedAt
        {
            get; set;
        }
    }
}
