﻿namespace BNine.Domain.Entities.User
{
    using System;
    using System.Collections.Generic;
    using Infrastructure;

    public class ArrayCustomerSettings : ValueObject
    {
        public Guid UserId
        {
            get; set;
        }

        /// <summary>
        /// Id of user entity in Array which is simultaneously a key/password to access Array.
        /// </summary>
        public Guid ArrayUserId
        {
            get;
            set;
        }

        /// <summary>
        /// Technical date of when user was created.
        /// </summary>
        public DateTime? CreatedAt
        {
            get; set;
        }

        /// <summary>
        /// Technical date of when user was changed.
        /// </summary>
        public DateTime? UpdatedAt
        {
            get; set;
        }

        /// <summary>
        /// Timestamp when user was successfully authorized (passed KBA).
        /// </summary>
        public DateTime? ActivatedAt
        {
            get;
            set;
        }

        /// <summary>
        /// Timestamp when user's enrollment was cancelled.
        /// </summary>
        public DateTime? DeactivatedAt
        {
            get;
            set;
        }

        protected override IEnumerable<object> GetAtomicValues()
        {
            yield return ArrayUserId;
            yield return ActivatedAt;
            yield return DeactivatedAt;
            yield return CreatedAt;
            yield return UpdatedAt;
        }
    }
}
