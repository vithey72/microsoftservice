﻿namespace BNine.Domain.Entities.User.UserDetails.Settings;

using System.Runtime.Serialization;

public enum PaycheckSwitchProvider
{
    [EnumMember(Value = "argyle")]
    Argyle = 1,
    [EnumMember(Value = "truv")]
    Truv = 2,
}
