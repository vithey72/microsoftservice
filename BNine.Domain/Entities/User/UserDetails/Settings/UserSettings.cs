﻿namespace BNine.Domain.Entities.User.UserDetails.Settings
{
    using System;
    using System.Collections.Generic;
    using BNine.Domain.Interfaces;
    using Infrastructure;

    public class UserSettings : ValueObject, IHistoryEntry
    {
        public Guid UserId
        {
            get; set;
        }

        public DateTime SysStartTime
        {
            get; set;
        }

        public bool IsReceivedBonusBlockEnabled
        {
            get; set;
        }

        public bool IsResetPasswordAllowed
        {
            get; set;
        }

        public bool IsNotificationsEnabled
        {
            get; set;
        }

        public bool IsActivationEligibilityBlockEnabled
        {
            get; set;
        }

        public bool IsPayAllocationEnabled
        {
            get; set;
        }

        public bool IsSelfieScanned
        {
            get; set;
        }

        public List<string> ConfirmedAgreementItems
        {
            get; set;
        }

        public int? LastCheckMatchesFromContactListCount
        {
            get; set;
        }

        public PaycheckSwitchProvider PaycheckSwitchProvider
        {
            get; set;
        }

        public bool? InitialTariffIsPurchased
        {
            get; set;
        }

        protected override IEnumerable<object> GetAtomicValues()
        {
            yield return IsNotificationsEnabled;
            yield return IsResetPasswordAllowed;
            yield return IsReceivedBonusBlockEnabled;
        }
    }
}
