﻿namespace BNine.Domain.Entities.User.UserDetails
{
    using System;
    using System.Collections.Generic;
    using Infrastructure;

    public class ToSAcceptance : ValueObject
    {
        public Guid UserId
        {
            get; set;
        }

        public DateTime Time
        {
            get; set;
        }

        public string Ip
        {
            get; set;
        }

        protected override IEnumerable<object> GetAtomicValues()
        {
            yield return Ip;
            yield return Time;
        }
    }
}
