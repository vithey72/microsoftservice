﻿namespace BNine.Domain.Entities.User;

using System;
using System.Collections.Generic;
using Infrastructure;

public class UserSyncedStats : ValueObject
{
    public Guid UserId
    {
        get; set;
    }

    public bool IsWaitingForArgyle
    {
        get;
        set;
    }

    public bool HasPayAllocation
    {
        get;
        set;
    }

    public int AdvancesTakenCount
    {
        get;
        set;
    }

    public decimal PayrollSumFor14Days
    {
        get;
        set;
    }

    public decimal PayrollSumFor30Days
    {
        get;
        set;
    }

    /// <summary>
    /// 0 means advance was never taken.
    /// </summary>
    public decimal LastAdvanceTakenAmount
    {
        get;
        set;
    }

    public int PayrollsTotalCount
    {
        get;
        set;
    }

    public int ActivePayAllocationCount
    {
        get;
        set;
    }

    public int ActiveExternalPayAllocationCount
    {
        get;
        set;
    }

    public string ActiveInternalPayAllocationIds
    {
        get;
        set;
    }

    /// <summary>
    /// Technical field to prevent updates with stale data.
    /// </summary>
    public DateTime UpdatedAt
    {
        get;
        set;
    } = DateTime.UtcNow;

    public decimal MaximumBasicAdvanceAmount
    {
        get;
        set;
    }

    public decimal CurrentBasicAdvanceAmount
    {
        get;
        set;
    }

    public decimal CurrentPremiumAdvanceAmount
    {
        get;
        set;
    }

    public decimal NextBasicAdvanceAmount
    {
        get;
        set;
    }

    public decimal NextPremiumAdvanceAmount
    {
        get;
        set;
    }

    protected override IEnumerable<object> GetAtomicValues()
    {
        yield return IsWaitingForArgyle;
        yield return HasPayAllocation;
        yield return AdvancesTakenCount;
        yield return PayrollSumFor14Days;
        yield return LastAdvanceTakenAmount;
        yield return PayrollsTotalCount;
        yield return ActivePayAllocationCount;
        yield return ActiveExternalPayAllocationCount;
        yield return UpdatedAt;
        yield return MaximumBasicAdvanceAmount;
        yield return NextBasicAdvanceAmount;
        yield return NextPremiumAdvanceAmount;
    }
}
