﻿namespace BNine.Domain.Entities.User
{
    using System;
    using System.Collections.Generic;
    using BNine.Domain.Infrastructure;
    using BNine.Domain.Interfaces;
    using BNine.Enums;

    public class Identity : ValueObject, IHistoryEntry
    {
        public Guid UserId
        {
            get; set;
        }

        public DateTime SysStartTime
        {
            get;
            set;
        }

        public string Value
        {
            get; set;
        }

        public IdentityType Type
        {
            get; set;
        }

        public int? ExternalId
        {
            get; set;
        }

        protected override IEnumerable<object> GetAtomicValues()
        {
            yield return Type;
            yield return Value;
            yield return ExternalId;
        }
    }
}
