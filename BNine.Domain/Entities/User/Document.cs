﻿namespace BNine.Domain.Entities.User
{
    using System;
    using System.Collections.Generic;
    using BNine.Domain.Interfaces;
    using Enums;
    using Infrastructure;
    using DocumentType = ListItems.DocumentType;

    public class Document : ValueObject, IHistoryEntry
    {
        public Guid UserId
        {
            get; set;
        }

        public DateTime SysStartTime
        {
            get;
            set;
        }

        public string VouchedJobId
        {
            get; set;
        }

        /// <summary>
        /// Id of a zip file containing scanning results.
        /// </summary>
        public Guid? DocumentScansExternalId
        {
            get; set;
        }

        public EvaluationProviderEnum OcrProvider
        {
            get; set;
        }

        public string Number
        {
            get;
            set;
        }

        public string IssuingState
        {
            get;
            set;
        }

        public string IssuingCountry
        {
            get;
            set;
        }

        public string TypeKey
        {
            get;
            set;
        }

        public DocumentType Type
        {
            get;
            set;
        }

        public int? ExternalId
        {
            get; set;
        }

        /// <summary>
        /// Indicates document uploaded into B9 Azure Blob Storage
        /// </summary>
        public bool IsUploaded
        {
            get;
            set;
        }

        protected override IEnumerable<object> GetAtomicValues()
        {
            yield return Number;
            yield return Type;
            yield return IssuingCountry;
            yield return IssuingState;
            yield return TypeKey;
            yield return VouchedJobId;
        }
    }
}
