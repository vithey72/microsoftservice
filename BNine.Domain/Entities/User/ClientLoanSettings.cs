﻿namespace BNine.Domain.Entities.User
{
    using System;
    using BNine.Domain.Interfaces;

    [Obsolete]
    public class ClientLoanSettings : IHistoryEntry
    {
        public DateTime SysStartTime
        {
            get;
            set;
        }

        public Guid UserId
        {
            get; set;
        }

        public decimal LoanLimit
        {
            get; set;
        }
    }
}
