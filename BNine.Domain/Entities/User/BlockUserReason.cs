﻿namespace BNine.Domain.Entities.User
{
    using System;
    using BNine.Domain.Interfaces;

    public class BlockUserReason : IBaseEntity
    {
        public Guid Id
        {
            get; set;
        }

        public string Value
        {
            get; set;
        }

        public int Order
        {
            get; set;
        }
    }
}
