﻿namespace BNine.Domain.Entities
{
    using System;
    using BNine.Domain.Interfaces;
    using BNine.Enums;

    public class OTP : IBaseEntity
    {
        public Guid Id
        {
            get; set;
        }

        public string Value
        {
            get; set;
        }

        public bool IsValid
        {
            get; set;
        }

        public DateTime Date
        {
            get; set;
        }

        public string PhoneNumber
        {
            get; set;
        }

        public OTPAction Action
        {
            get; set;
        }

        public bool? IsFromVoiceRequest
        {
            get; set;
        }

        public string IpAddress
        {
            get; set;
        }
    }
}
