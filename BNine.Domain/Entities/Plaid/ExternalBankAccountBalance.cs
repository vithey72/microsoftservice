﻿namespace BNine.Domain.Entities
{
    using System;
    using BNine.Domain.Interfaces;

    public class ExternalBankAccountBalance : IBaseEntity
    {
        public ExternalBankAccountBalance()
        {
            CreatedAt = DateTime.UtcNow;
        }

        public Guid Id
        {
            get; set;
        }

        public string Currency
        {
            get; set;
        }

        public decimal? AvailableAmount
        {
            get; set;
        }

        public decimal? CurrentAmount
        {
            get; set;
        }

        public ExternalBankAccount ExternalBankAccount
        {
            get; set;
        }

        public Guid ExternalBankAccountId
        {
            get; set;
        }

        public DateTime CreatedAt
        {
            get; set;
        }
    }
}
