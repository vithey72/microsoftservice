﻿namespace BNine.Domain.Entities
{
    using System;
    using System.Collections.Generic;
    using BNine.Domain.Interfaces;

    public class ExternalBankAccount : IBaseEntity, ISoftDelete
    {
        public Guid Id
        {
            get; set;
        }

        public DateTime BindingDate
        {
            get; set;
        }

        public DateTime? UpdatedAt
        {
            get; set;
        }

        public string AccountNumber
        {
            get; set;
        }

        public string RoutingNumber
        {
            get; set;
        }

        public Guid UserId
        {
            get; set;
        }

        public User.User User
        {
            get; set;
        }

        public Plaid PlaidData
        {
            get; set;
        }

        public string ExternalAccountId
        {
            get; set;
        }

        public string Currency
        {
            get; set;
        }

        public decimal? InitialAvailableAmount
        {
            get; set;
        }

        public IEnumerable<ExternalBankAccountBalance> ExternalBankAccountBalances
        {
            get; set;
        } = new List<ExternalBankAccountBalance>();

        public IEnumerable<ExternalBankAccountTransfer> ExternalBankAccountTransfers
        {
            get; set;
        } = new List<ExternalBankAccountTransfer>();

        public string AccessToken
        {
            get; set;
        }

        public DateTime? DeletedAt
        {
            get; set;
        }

        public string DeletedBy
        {
            get; set;
        }
    }
}
