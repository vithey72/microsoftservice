﻿namespace BNine.Domain.Entities
{
    using System;
    using BNine.Domain.Interfaces;

    public class ExternalBankAccountTransfer : IBaseEntity
    {
        public Guid Id
        {
            get; set;
        }

        public int ExternalTransferId
        {
            get; set;
        }

        public ExternalBankAccount ExternalBankAccount
        {
            get; set;
        }

        public Guid ExternalBankAccountId
        {
            get; set;
        }

        public DateTime CreatedAt
        {
            get; set;
        }
    }
}
