﻿namespace BNine.Domain.Entities;

using System;
using BNine.Domain.Interfaces;
using Newtonsoft.Json;

public class PotentialAchIncomeTransfer : IBaseEntity
{
    public PotentialAchIncomeTransfer()
    {
        CreatedAt = DateTime.UtcNow;
    }

    public Guid Id
    {
        get; set;
    }

    public DateTime CreatedAt
    {
        get; set;
    }

    public int TransferExternalId
    {
        get; set;
    }

    public string Reference
    {
        get;
        set;
    } = string.Empty;

    [JsonIgnore]
    public User.User User
    {
        get; set;
    }

    public Guid UserId
    {
        get; set;
    }
}
