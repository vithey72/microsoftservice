﻿namespace BNine.Domain.Entities.BankAccount;

using System;
using Interfaces;
using Enums.DebitCard;

public class CardProduct : IBaseEntity
{
    public Guid Id
    {
        get; set;
    }

    public int ExternalId
    {
        get;
        set;
    }

    public string Name
    {
        get;
        set;
    }

    public CardType AssociatedType
    {
        get;
        set;
    }
}
