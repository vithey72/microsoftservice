﻿namespace BNine.Domain.Entities.BankAccount;

using Enums;

public class CurrentAdditionalAccount : CurrentAccountBase
{
    public CurrentAdditionalAccountEnum AccountType
    {
        get;
        set;
    }
}
