﻿namespace BNine.Domain.Entities.BankAccount
{
    using System;
    using BNine.Domain.Interfaces;
    using BNine.Domain.Entities.User;
    using System.Collections.Generic;
    using BNine.Domain.Infrastructure;

    public class ACHCreditTransfer : IBaseEntity, IHaveDomainEvents
    {
        public Guid Id
        {
            get; set;
        }

        public int ExternalId
        {
            get; set;
        }

        public string Reference
        {
            get; set;
        }

        public decimal Amount
        {
            get; set;
        }

        public User User
        {
            get; set;
        }

        public Guid UserId
        {
            get; set;
        }

        /// <summary>
        /// Timestamp projected from transfer properties.
        /// </summary>
        public DateTime CreatedAt
        {
            get; set;
        }

        /// <summary>
        /// Timestamp of saving this entity to database.
        /// </summary>
        public DateTime UpdatedAt
        {
            get; set;
        }

        /// <summary>
        /// Transaction id that can be associated with mbanq graphql SavingsAccountTransactions.id
        /// </summary>
        public int SavingsAccountTransferExternalId
        {
            get;
            set;
        }

        public List<DomainEvent> DomainEvents
        {
            get; set;
        } = new List<DomainEvent>();
    }
}
