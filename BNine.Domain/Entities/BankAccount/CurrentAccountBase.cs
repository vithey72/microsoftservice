﻿namespace BNine.Domain.Entities.BankAccount;

using Interfaces;

public class CurrentAccountBase : IBaseEntity
{
    public Guid Id
    {
        get; set;
    }

    public DateTime CreatedAt
    {
        get; set;
    }

    public int ExternalId
    {
        get; set;
    }

    public int ProductId
    {
        get; set;
    }

    public Guid UserId
    {
        get; set;
    }

    public User.User User
    {
        get; set;
    }
}
