﻿namespace BNine.Domain.Entities.TariffPlan
{
    using System;
    using BNine.Domain.Interfaces;
    using BNine.Enums.TariffPlan;

    public class TariffPlan : IBaseEntity
    {
        public Guid Id
        {
            get; set;
        }

        public string Name
        {
            get; set;
        }

        /// <summary>
        /// Name to display in Wallet screen.
        /// </summary>
        public string ShortName
        {
            get; set;
        }

        /// <summary>
        /// Basic or premium plan family.
        /// </summary>
        public TariffPlanFamily Type
        {
            get; set;
        }

        /// <summary>
        /// A disabled plan is impossible to switch to normally.
        /// </summary>
        public bool IsDisabled
        {
            get; set;
        }

        public decimal MonthlyFee
        {
            get; set;
        }

        public DateTime CreatedAt
        {
            get; set;
        }

        public int ExternalClassificationId
        {
            get; set;
        }

        public int ExternalChargeId
        {
            get; set;
        }


        /// <summary>
        /// Use <see cref="TariffFamilyImageUrl"/> + <see cref="ActiveImageSubtitleObject"/>.
        /// </summary>
        [Obsolete("Left for backwards compatibility.")]
        public string PictureUrl
        {
            get; set;
        }

        /// <summary>
        /// Use <see cref="TariffFamilyImageUrl"/> + <see cref="AdvertisementImageSubtitleObject"/>.
        /// </summary>
        [Obsolete("Left for backwards compatibility.")]
        public string AdvertisementPictureUrl
        {
            get; set;
        }

        public string WelcomeScreenPictureUrl
        {
            get; set;
        }

        public string ActiveImageSubtitleObject
        {
            get; set;
        }

        public string AdvertisementImageSubtitleObject
        {
            get; set;
        }

        public string IconUrl
        {
            get; set;
        }

        public string TariffFamilyImageUrl
        {
            get; set;
        }

        /// <summary>
        /// Text description of what this plan offers.
        /// </summary>
        public string Features
        {
            get; set;
        }

        /// <summary>
        /// List of groups members of which should be able to see this plan. Null means everyone sees it.
        /// </summary>
        public Guid[] GroupIds
        {
            get; set;
        }

        public int MonthsDuration
        {
            get; set;
        }

        public bool IsParent
        {
            get; set;
        }

        public decimal DiscountPercentage
        {
            get; set;
        }

        public List<UserTariffPlan> UserTariffPlans
        {
            get; set;
        }

        public decimal TotalPrice
        {
            get; set;
        }

        public decimal PreDiscountPrice
        {
            get; set;
        }
    }
}
