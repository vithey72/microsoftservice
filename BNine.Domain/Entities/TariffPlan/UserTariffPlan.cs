﻿namespace BNine.Domain.Entities.TariffPlan
{
    using System;
    using BNine.Domain.Interfaces;
    using BNine.Enums.TariffPlan;
    using BNine.Domain.Entities.User;

    public class UserTariffPlan : IBaseEntity, IHistoryEntry
    {
        public Guid Id
        {
            get; set;
        }

        public Guid TariffPlanId
        {
            get; set;
        }

        public TariffPlan TariffPlan
        {
            get; set;
        }

        public Guid UserId
        {
            get; set;
        }

        public User User
        {
            get; set;
        }

        public DateTime CreatedAt
        {
            get; set;
        }

        public DateTime UpdatedAt
        {
            get; set;
        }

        public DateTime NextPaymentDate
        {
            get; set;
        }

        public TariffPlanStatus Status
        {
            get; set;
        }

        public DateTime SysStartTime
        {
            get;
            set;
        }
    }
}
