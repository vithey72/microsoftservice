﻿namespace BNine.Domain.Entities;

using System.ComponentModel.DataAnnotations.Schema;
using Enums.Transfers;
using Newtonsoft.Json;

public class TransactionAnalyticsCategory
{
    public TransactionAnalyticsCategory()
    {
        ChildCategories = new List<TransactionAnalyticsCategory>();
    }

    /// <summary>
    /// Url to colorless icon of category
    /// </summary>
    public string IconUrl
    {
        get;
        set;
    }

    /// <summary>
    /// Icon and category item color, ex: D31C04
    /// </summary>
    public string Color
    {
        get;
        set;
    }

    /// <summary>
    /// Unique hardcoded key
    /// </summary>
    public string Key
    {
        get;
        set;
    }

    public string Title
    {
        get;
        set;
    }

    public string ParentId
    {
        get;
        set;
    }

    /// <summary>
    /// Parent object that current category belongs to
    /// </summary>
    public TransactionAnalyticsCategory ParentCategory
    {
        get;
        set;
    }

    public List<TransactionAnalyticsCategory> ChildCategories
    {
        get;
        set;
    }

    /// <summary>
    /// Merchant category code range
    /// </summary>
    public int? MccRangeStart
    {
        get;
        set;
    }

    /// <summary>
    /// Merchant category code range
    /// </summary>
    public int? MccRangeEnd
    {
        get;
        set;
    }

    /// <summary>
    /// Merchant category codes
    /// Json array
    /// </summary>
    public string MccCodesJsonArrayString
    {
        get;
        private set;
    }

    /// <summary>
    /// Merchant category codes
    /// Json array
    /// </summary>
    [NotMapped]
    public List<int> MccCodes
    {
        get
        {
            var mccCodesFromJsonArray = new List<int>();
            if (!string.IsNullOrEmpty(MccCodesJsonArrayString))
            {
                mccCodesFromJsonArray = JsonConvert.DeserializeObject<List<int>>(MccCodesJsonArrayString);
            }

            var mccCodesArrayFromRange = new List<int>();
            if (this.MccRangeStart.HasValue && this.MccRangeEnd.HasValue)
            {
                mccCodesArrayFromRange = Enumerable.Range(this.MccRangeStart.Value, this.MccRangeEnd.Value - this.MccRangeStart.Value + 1).ToList();
            }

            return mccCodesFromJsonArray.Concat(mccCodesArrayFromRange).ToList();
        }
        set
        {
            MccCodesJsonArrayString = JsonConvert.SerializeObject(value);
        }
    }

    /// <summary>
    /// Transfer Payment Types
    /// ex: ACH, INTERNAL etc
    /// Json array
    /// </summary>
    public string TransferDisplayTypeIdsJsonArrayString
    {
        get;
        private set;
    }

    /// <summary>
    /// Transfer Payment Types
    /// </summary>
    [NotMapped]
    public List<TransferDisplayType> TransferDisplayTypes
    {
        get
        {
            if (string.IsNullOrEmpty(TransferDisplayTypeIdsJsonArrayString))
            {
                return new List<TransferDisplayType>();
            }

            return JsonConvert.DeserializeObject<List<TransferDisplayType>>(TransferDisplayTypeIdsJsonArrayString);
        }
        set
        {
            TransferDisplayTypeIdsJsonArrayString = JsonConvert.SerializeObject(value);
        }
    }

    public string HintTitle
    {
        get;
        set;
    }

    public string HintIconUrl
    {
        get;
        set;
    }

    public string HintSubTitle
    {
        get;
        set;
    }

    public string HintButtonText
    {
        get;
        set;
    }
}
