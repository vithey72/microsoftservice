﻿namespace BNine.Domain.Entities;

using System;
using BNine.Domain.Interfaces;

public class UserActivityCooldown : IBaseEntity
{
    public Guid Id
    {
        get;
        set;
    }

    public Guid UserId
    {
        get;
        set;
    }

    public User.User User
    {
        get;
        set;
    }

    /// <summary>
    /// Unique name of the activity. See <see cref="UserActivityCooldown"/> for list of potential values.
    /// </summary>
    public string ActivityKey
    {
        get;
        set;
    }

    /// <summary>
    /// UTC date of when the entry was created.
    /// </summary>
    public DateTime CreatedAt
    {
        get;
        set;
    }

    /// <summary>
    /// UTC date of when cooldown date was updated, if any.
    /// </summary>
    public DateTime? UpdatedAt
    {
        get;
        set;
    }

    /// <summary>
    /// UTC date of when the cooldown should pass.
    /// </summary>
    public DateTime ExpiresAt
    {
        get;
        set;
    }
}
