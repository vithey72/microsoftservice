﻿namespace BNine.Domain.Constants;

public static class CooldownActivitiesNames
{
    public static class Menu
    {
        public static string RedDotForArgyleTile = "menu-red-dot-for-argyle-tile";
    }

    public static class Marketing
    {
        public static string SwitchToPremiumPitch = "switch-to-premium-pitch";
    }
}
