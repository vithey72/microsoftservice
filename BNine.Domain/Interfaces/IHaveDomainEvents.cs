﻿namespace BNine.Domain.Interfaces
{
    using System.Collections.Generic;
    using BNine.Domain.Infrastructure;

    public interface IHaveDomainEvents
    {
        List<DomainEvent> DomainEvents
        {
            get; set;
        }
    }
}
