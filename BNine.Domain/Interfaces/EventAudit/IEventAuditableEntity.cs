﻿namespace BNine.Domain.Interfaces.EventAudit
{
    public interface IEventAuditableEntity
    {
        public Guid? EventId
        {
            get; set;
        }
    }
}
