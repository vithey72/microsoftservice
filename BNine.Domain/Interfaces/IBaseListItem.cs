﻿namespace BNine.Domain.Interfaces
{
    using Entities.Common;

    public interface IBaseListItem
    {
        string Key
        {
            get; set;
        }

        ICollection<LocalizedText> Value
        {
            get; set;
        }

        int Order
        {
            get; set;
        }
    }
}
