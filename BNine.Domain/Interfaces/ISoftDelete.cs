﻿namespace BNine.Domain.Interfaces
{
    using System;

    public interface ISoftDelete
    {
        public DateTime? DeletedAt
        {
            get; set;
        }

        public string DeletedBy
        {
            get; set;
        }
    }
}
