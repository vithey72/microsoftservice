namespace BNine.Domain.Interfaces
{
    using System;

    public interface IHistoryEntry
    {
        public DateTime SysStartTime
        {
            get; set;
        }
    }
}
