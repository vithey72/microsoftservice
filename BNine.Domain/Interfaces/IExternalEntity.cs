﻿namespace BNine.Domain.Interfaces
{
    using System;
    using Entities.User;

    public interface IExternalEntity
    {
        int ExternalId { get; set; }

        Guid UserId { get; set; }

        User User { get; set; }
    }
}
