﻿namespace BNine.Domain.ListItems
{
    using BNine.Domain.Infrastructure;

    public class DocumentType : BaseListItem
    {
        public string ExternalName
        {
            get; set;
        }
    }
}
