﻿namespace BNine.Firebase
{
    using Application.Interfaces.Firebase;
    using Microsoft.Extensions.DependencyInjection;
    using Services;

    public static class DependenciesBootstrapper
    {
        public static IServiceCollection AddFirebase(this IServiceCollection services)
        {
            services.AddTransient<IDynamicLinkService, DynamicLinkService>();

            return services;
        }
    }
}
