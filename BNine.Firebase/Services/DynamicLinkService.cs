﻿namespace BNine.Firebase.Services
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Net.Http;
    using System.Net.Http.Headers;
    using Application.Interfaces.Firebase;
    using Common;
    using Google.Apis.Auth.OAuth2;
    using Google.Apis.FirebaseDynamicLinks.v1.Data;
    using Microsoft.Extensions.Options;
    using Models.Responses;
    using Newtonsoft.Json;
    using Settings.Firebase;

    public class DynamicLinkService : IDynamicLinkService
    {
        private readonly FirebaseSettings _firebaseSettings;

        public DynamicLinkService(IOptions<FirebaseSettings> options)
        {
            ArgumentNullException.ThrowIfNull(options?.Value);

            _firebaseSettings = options.Value;
        }

        public string GetDynamicShortLink(object obj, string linkSuffix)
        {
            var credential = GoogleCredential
                    .FromJson(JsonConvert.SerializeObject(_firebaseSettings.PrivateKey))
                    .CreateScoped("https://www.googleapis.com/auth/firebase")
                as ITokenAccess;

            var accessToken = credential.GetAccessTokenForRequestAsync().Result;

            var client = new HttpClient();
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", accessToken);

            var body = new
            {
                dynamicLinkInfo = new DynamicLinkInfo
                {
                    DomainUriPrefix = _firebaseSettings.DynamicLink.DomainUriPrefix,
                    Link = GetLongLink(obj, linkSuffix),
                    IosInfo = new IosInfo { IosBundleId = _firebaseSettings.DynamicLink.IosBundleId },
                    AndroidInfo = new AndroidInfo { AndroidPackageName = _firebaseSettings.DynamicLink.AndroidPackageName }
                }
            };

            var content = SerializationHelper.GetIgnoredEmptyPropertiesRequestContent(body);

            var result = client
                .PostAsync($"https://firebasedynamiclinks.googleapis.com/v1/shortLinks?key={_firebaseSettings.ApiKey}", content)
                .Result;

            var deepLink = JsonConvert.DeserializeObject<DynamicLink>(result.Content.ReadAsStringAsync().Result);

            return deepLink.ShortLink;
        }

        private string GetLongLink(object obj, string linkSuffix)
        {
            var link = _firebaseSettings.DynamicLink.Link + linkSuffix;

            var type = obj.GetType();

            var dictionary = type.GetProperties()
                .Where(item => item.GetValue(obj) != null)
                .ToDictionary(item => item.Name, item => item.GetValue(obj)!.ToString());

            if (dictionary.Count > 0)
            {
                link = ConstructLink(link, dictionary);
            }

            return link;
        }

        private string ConstructLink(string baseLink, Dictionary<string, string> dictionary)
        {
            var link = baseLink + "?";

            foreach (var (key, value) in dictionary)
            {
                var propName = key;

                if (!key.Equals(string.Empty) && char.IsUpper(key[0]))
                {
                    propName = char.ToLower(propName[0]) + propName[1..];
                }

                link += $"{propName}={value}&";
            }

            link = link.Remove(link.Length - 1);

            return link;
        }
    }
}
