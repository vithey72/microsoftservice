﻿namespace BNine.Firebase.Models.Responses
{
    using Newtonsoft.Json;

    public class Warning
    {
        [JsonProperty("warningCode")]
        public string WarningCode
        {
            get; set;
        }

        [JsonProperty("warningMessage")]
        public string WarningMessage
        {
            get; set;
        }
    }
}
