﻿namespace BNine.Firebase.Models.Responses
{
    using Newtonsoft.Json;

    public class DynamicLink
    {
        [JsonProperty("shortLink")]
        public string ShortLink
        {
            get; set;
        }

        [JsonProperty("previewLink")]
        public string PreviewLink
        {
            get; set;
        }

        [JsonProperty("warning")]
        public Warning[] Warning
        {
            get; set;
        }
    }
}
