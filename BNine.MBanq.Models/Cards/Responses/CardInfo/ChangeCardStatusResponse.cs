﻿namespace BNine.MBanq.Models.Cards.Responses.CardInfo
{
    using Newtonsoft.Json;

    public class ChangeCardStatusResponse
    {
        [JsonProperty("handleCardEvent")]
        public HandleCardEvent HandleCardEvent
        {
            get; set;
        }
    }

    public class HandleCardEvent
    {
        [JsonProperty("id")]
        public string Id
        {
            get; set;
        }

        [JsonProperty("status")]
        public string Status
        {
            get; set;
        }
    }
}
