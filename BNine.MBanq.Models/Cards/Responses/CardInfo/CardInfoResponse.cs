﻿namespace BNine.MBanq.Models.Cards.Responses.CardInfo
{
    using System.Collections.Generic;
    using Newtonsoft.Json;

    public class CardInfoResponse
    {
        [JsonProperty("Cards")]
        public CardsResponse Cards
        {
            get; set;
        }
    }

    public class CardsResponse
    {
        [JsonProperty("select")]
        public List<SelectedCards> SelectedCards
        {
            get; set;
        }
    }

    public class SelectedCards
    {
        [JsonProperty("id")]
        public int Id
        {
            get; set;
        }

        [JsonProperty("status")]
        public string Status
        {
            get; set;
        }

        [JsonProperty("onlinePaymentEnabled")]
        public bool OnlinePaymentEnabled
        {
            get;
            set;
        }

        [JsonProperty("physicalCardActivated")]
        public bool PhysicalCardActivated
        {
            get;
            set;
        }

        [JsonProperty("primaryAccountNumber")]
        public string PrimaryAccountNumber
        {
            get;
            set;
        }

        [JsonProperty("replacedDamageCount")]
        public int ReplacedDamageCount
        {
            get;
            set;
        }

        [JsonProperty("product")]
        public Product CardProduct
        {
            get;
            set;
        }
    }

    public class Product
    {
        [JsonProperty("id")]
        public int Id
        {
            get;
            set;
        }
    }
}
