﻿namespace BNine.API.Infrastructure.Requirements
{
    using Microsoft.AspNetCore.Authorization;

    public class NotBlockedRequirement : IAuthorizationRequirement
    {
        public NotBlockedRequirement()
        {
        }
    }
}
