﻿namespace BNine.API.Infrastructure.Requirements;

public class BlockedUserPolicyWaiver : IBlockedUserPolicyWaiver
{
    private static readonly HashSet<EndPointWhiteListEntry> _endpointWhiteList = new()
        {
            new EndPointWhiteListEntry("GET", "/profile"),
            new EndPointWhiteListEntry("PUT", "/devices"),
            new EndPointWhiteListEntry("POST", "/devices/details"),
            new EndPointWhiteListEntry("GET", "/marketing/info-popup"),
            new EndPointWhiteListEntry("GET", "/configuration/menu"),
        };

    private readonly IHttpContextAccessor _httpContextAccessor;

    public BlockedUserPolicyWaiver(IHttpContextAccessor httpContextAccessor)
    {
        _httpContextAccessor = httpContextAccessor;
    }

    public bool CanProceed()
    {
        var request = _httpContextAccessor.HttpContext.Request;

        if (!request.Path.HasValue)
        {
            return false;
        }

        var endpointEntry = new EndPointWhiteListEntry(request.Method, request.Path.Value);

        return _endpointWhiteList.Contains(endpointEntry);
    }

    private record EndPointWhiteListEntry(string method, string path);
}
