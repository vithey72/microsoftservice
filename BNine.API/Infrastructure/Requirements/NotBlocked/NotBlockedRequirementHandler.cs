﻿namespace BNine.API.Infrastructure.Requirements
{
    using System.Linq;
    using System.Threading.Tasks;
    using BNine.Application.Extensions;
    using BNine.Enums;
    using Microsoft.AspNetCore.Authorization;

    public class NotBlockedRequirementHandler : AuthorizationHandler<NotBlockedRequirement>
    {
        private readonly IBlockedUserPolicyWaiver _blockedUserPolicyWaiver;

        public NotBlockedRequirementHandler(IBlockedUserPolicyWaiver blockedUserPolicyWaiver)
        {
            _blockedUserPolicyWaiver = blockedUserPolicyWaiver;
        }

        protected override Task HandleRequirementAsync(AuthorizationHandlerContext context, NotBlockedRequirement requirement)
        {
            var statusClaim = context.User.Claims.FirstOrDefault(c => c.Type == "status");
            if (statusClaim == null)
            {
                context.Succeed(requirement);

                return Task.CompletedTask;
            }

            if (statusClaim.Value == UserStatus.Blocked.ToLowerCamelCase())
            {
                if (!_blockedUserPolicyWaiver.CanProceed())
                {
                    context.Fail();

                    return Task.CompletedTask;
                }
            }

            context.Succeed(requirement);

            return Task.CompletedTask;
        }
    }
}
