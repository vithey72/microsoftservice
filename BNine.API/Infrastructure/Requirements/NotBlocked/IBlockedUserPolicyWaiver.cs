﻿namespace BNine.API.Infrastructure.Requirements;

public interface IBlockedUserPolicyWaiver
{
    bool CanProceed();
}
