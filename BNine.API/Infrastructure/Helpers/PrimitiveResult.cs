﻿namespace BNine.API.Infrastructure.Helpers;

using Newtonsoft.Json;

/// <summary>
/// Small class for returning json object instead of raw value from controller methods.
/// </summary>
/// <typeparam name="T">Type that this json object encapsulates.</typeparam>
public class PrimitiveResult<T>
{
#pragma warning disable CS1591
    public PrimitiveResult(T result)
    {
        Result = result;
    }

    [JsonProperty("result")]
    public T Result
    {
        get; set;
    }
}
