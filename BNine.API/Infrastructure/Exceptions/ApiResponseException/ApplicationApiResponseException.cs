﻿namespace BNine.API.Infrastructure.Exceptions.ApiResponseException
{
    using Constants;
    using Newtonsoft.Json;

    /// <summary>
    /// Standardized projection of an exception that frontend clients are supposed to know how to handle.
    /// Visually results in a popup with error details in most cases.
    /// </summary>
    public class ApplicationApiResponseException
    {
        /// <summary>
        /// One of <see cref="ApiResponseErrorCodes"/>.
        /// </summary>
        [JsonProperty("code")]
        public string Code
        {
            get; set;
        }

        /// <summary>
        /// Subject.
        /// </summary>
        [JsonProperty("title")]
        public string Title
        {
            get; set;
        }

        /// <summary>
        /// Details.
        /// </summary>
        [JsonProperty("message")]
        public string Message
        {
            get; set;
        }

        /// <summary>
        /// Unused field. Supposedly a trace id.
        /// </summary>
        [JsonProperty("operationId")]
        public string OperationId
        {
            get; set;
        }

        /// <summary>
        /// Initialize all fields.
        /// </summary>
        public ApplicationApiResponseException(string code, string message, string title)
        {
            Code = code;
            Message = message;
            Title = title;
        }
    }
}
