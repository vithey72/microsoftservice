﻿namespace BNine.API.Infrastructure.Extensions
{
    using System;
    using System.Security.Claims;
    using BNine.API.Infrastructure.Requirements;
    using BNine.Settings;
    using BNine.Settings.DwhIntegration;
    using Microsoft.AspNetCore.Authentication.JwtBearer;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.Extensions.Configuration;
    using Microsoft.Extensions.DependencyInjection;
    using Microsoft.IdentityModel.Tokens;
    using Settings.Blob;
    using Settings.Firebase;

    internal static class ServiceCollectionExtensions
    {
        internal static IServiceCollection AddJWTAuthentication(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
                .AddJwtBearer(JwtBearerDefaults.AuthenticationScheme, opts =>
                {
                    opts.Authority = configuration["Authority"];
                    opts.RequireHttpsMetadata = true;

                    opts.TokenValidationParameters = new TokenValidationParameters
                    {
                        NameClaimType = "name",
                        RoleClaimType = ClaimTypes.Role,
                        ValidateAudience = false,
                        ClockSkew = new TimeSpan(0, 0, 0)
                    };
                });

            services.AddAuthorization(options =>
            {
                options.DefaultPolicy = new AuthorizationPolicyBuilder()
                    .RequireAuthenticatedUser()
                    .AddRequirements(new NotBlockedRequirement())
                    .Build();

                options.AddPolicy("EmailsSender", policy =>
                    policy.RequireClaim("client_id", configuration["AzureFunctionsClientId"]));
            });

            services.AddSingleton<IAuthorizationHandler, NotBlockedRequirementHandler>();
            services.AddTransient<IBlockedUserPolicyWaiver, BlockedUserPolicyWaiver>();

            return services;
        }

        internal static IServiceCollection AddSettings(this IServiceCollection services, IConfiguration configuration)
        {
            services.ConfigureAndValidateApplicationSettings<MBanqSettings>(configuration);
            services.ConfigureAndValidateApplicationSettings<TwilioSettings>(configuration);
            services.ConfigureAndValidateApplicationSettings<DocsAlloySettings>(configuration);
            services.ConfigureAndValidateApplicationSettings<BlobStorageSettings>(configuration);
            services.ConfigureAndValidateApplicationSettings<SendGridSettings>(configuration);
            services.ConfigureAndValidateApplicationSettings<PlaidSettings>(configuration);
            services.ConfigureAndValidateApplicationSettings<AzureNotificationHubSettings>(configuration);
            services.ConfigureAndValidateApplicationSettings<BNineBonusAccountSettings>(configuration);
            services.ConfigureAndValidateApplicationSettings<ArgyleSettings>(configuration);
            services.ConfigureAndValidateApplicationSettings<TruvSettings>(configuration);
            services.ConfigureAndValidateApplicationSettings<FirebaseSettings>(configuration);
            services.ConfigureAndValidateApplicationSettings<EmailTemplatesSettings>(configuration);
            services.ConfigureAndValidateApplicationSettings<DynamicShortLinksSettings>(configuration);
            services.ConfigureAndValidateApplicationSettings<AlwaysEncryptedSettings>(configuration);

            services.Configure<AzureServiceBusSettings>(configuration.GetSection(nameof(AzureServiceBusSettings)));
            services.ConfigureAndValidateApplicationSettings<AlltrustSettings>(configuration);

            services.ConfigureAndValidateApplicationSettings<ArraySettings>(configuration);
            services.ConfigureAndValidateApplicationSettings<SocureSettings>(configuration);
            services.ConfigureAndValidateApplicationSettings<DwhIntegrationSettings>(configuration);
            services.ConfigureAndValidateApplicationSettings<AprilTaxServiceSettings>(configuration);
            services.ConfigureAndValidateApplicationSettings<PartnerSettings>(configuration);
            services.ConfigureAndValidateApplicationSettings<AuditEventSettings>(configuration);
            services.ConfigureAndValidateApplicationSettings<CurrentAdditionalAccountsSettings>(configuration);

            return services;
        }
    }
}
