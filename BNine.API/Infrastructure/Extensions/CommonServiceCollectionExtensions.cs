﻿namespace BNine.API.Infrastructure.Extensions;

using Settings;

public static class CommonServiceCollectionExtensions
{
    public static IServiceCollection ConfigureAndValidateApplicationSettings<T>(
        this IServiceCollection @this,
        IConfiguration configuration) where T : class, IApplicationSettings
        => @this.Configure<T>(configuration.GetSection(typeof(T).Name))
            .PostConfigure<T>(settings =>
            {
                var configurationErrors = settings.ValidateApplicationSettings().ToArray();
                if (configurationErrors.Any())
                {
                    var aggregatedErrors = string.Join("\n", configurationErrors);
                    throw new ApplicationException(
                        $"Found {configurationErrors.Length} configuration error(s) in {typeof(T).Name}:\n{aggregatedErrors}");
                }
            });
}
