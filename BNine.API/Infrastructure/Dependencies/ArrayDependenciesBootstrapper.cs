﻿namespace BNine.API.Infrastructure.Dependencies
{
    using Array.Interfaces;
    using Array.Services;
    using Constants;
    using Polly;

    public static class ArrayDependenciesBootstrapper
    {
        public static IServiceCollection AddArray(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddTransient<IArrayUserRegistrationService, ArrayUserAuthorizationService>();

            services.AddHttpClient(HttpClientName.Array, client =>
                {
                    client.BaseAddress = new Uri(configuration["ArraySettings:ApiUrl"]);
                })
                .AddTransientHttpErrorPolicy(p => p.WaitAndRetryAsync(3, (x) => TimeSpan.FromSeconds(x)));

            return services;
        }
    }
}
