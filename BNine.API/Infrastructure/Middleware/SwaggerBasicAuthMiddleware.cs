﻿namespace BNine.API.Infrastructure.Middleware;

using System.Net;
using System.Net.Http.Headers;
using System.Text;

/// <summary>
/// Ensures that swagger UI cannot be viewed without proper authorization.
/// </summary>
public class SwaggerBasicAuthMiddleware
{
    private readonly RequestDelegate _next;

    /// <summary>
    /// Middleware ctor.
    /// </summary>
    public SwaggerBasicAuthMiddleware(RequestDelegate next)
    {
        _next = next;
    }

    /// <summary>
    /// Invoked on every request.
    /// </summary>
    public async Task InvokeAsync(HttpContext context, IConfiguration configuration)
    {
        if (context.Request.Path.StartsWithSegments("/swagger"))
        {
            string authHeader = context.Request.Headers["Authorization"];
            if (authHeader != null && authHeader.StartsWith("Basic "))
            {
                var (_, password) = ExtractCredentials(authHeader);
                var correctPassword = configuration["APIKey"];
                if (password.Equals(correctPassword))
                {
                    await _next.Invoke(context).ConfigureAwait(false);
                    return;
                }
            }
            context.Response.Headers["WWW-Authenticate"] = "Basic";
            context.Response.StatusCode = (int)HttpStatusCode.Unauthorized;
        }
        else
        {
            await _next.Invoke(context).ConfigureAwait(false);
        }
    }

    private (string Username, string Password) ExtractCredentials(string authHeader)
    {
        var header = AuthenticationHeaderValue.Parse(authHeader);
        var inBytes = Convert.FromBase64String(header.Parameter);
        var credentials = Encoding.UTF8.GetString(inBytes).Split(':');
        return (credentials[0], credentials[1]);
    }
}
