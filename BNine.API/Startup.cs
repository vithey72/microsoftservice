﻿namespace BNine.API
{
    using System.Collections.Generic;
    using System.Globalization;
    using System.Linq;
    using Application.Aggregates.CheckingAccounts.Queries.GetFilteredTransfers;
    using Application.Aggregates.CheckingAccounts.Queries.GetFilteredTransfers.Services;
    using Application.Aggregates.CheckingAccounts.Queries.TransactionAnalytics;
    using Application.Aggregates.CreditLines.Queries.AdvanceSimulation.Mappers;
    using Application.Interfaces.Bank.Administration;
    using Application.Interfaces.TransactionHistory;
    using Application.Services.ClientDocument;
    using Application.Services.DelayedBoostDateTimeCalculator;
    using April.Client;
    using April.Client.Services;
    using BNine.Alltrust.Client;
    using BNine.API.Filters;
    using BNine.API.Infrastructure.Extensions;
    using BNine.API.Services;
    using BNine.Application;
    using BNine.Application.Aggregates.CreditScore.Models;
    using BNine.Application.Aggregates.PaycheckSwitch;
    using BNine.Application.Aggregates.Payroll;
    using BNine.Application.Aggregates.Users.AccountStateMachine;
    using BNine.Application.Aggregates.Users.Closure;
    using BNine.Application.Aggregates.Users.Queries.CloseAccount.GetAccountClosureInitDialog;
    using BNine.Application.Interfaces;
    using BNine.Application.Services;
    using BNine.Application.Services.AccountClosure;
    using BNine.Application.Services.AdminAPI;
    using BNine.Argyle;
    using BNine.BlobStorage;
    using BNine.DocsAlloy;
    using BNine.DwhIntegration;
    using BNine.MBanq.Administration.Services.Classifications;
    using BNine.MBanq.Client;
    using BNine.MBanq.Infrastructure;
    using BNine.NotificationHub;
    using BNine.Persistence;
    using BNine.Persistence.Services;
    using BNine.Plaid;
    using BNine.SendGrid;
    using BNine.ServiceBus;
    using BNine.Truv;
    using BNine.Twilio;
    using BNine.Zendesk;
    using Firebase;
    using FluentValidation.AspNetCore;
    using Infrastructure.Dependencies;
    using Infrastructure.Middleware;
    using MBanq.Administration;
    using MBanq.Administration.Services.Cards;
    using Microsoft.AspNetCore.Builder;
    using Microsoft.AspNetCore.Hosting;
    using Microsoft.AspNetCore.HttpOverrides;
    using Microsoft.AspNetCore.Localization;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.Extensions.Configuration;
    using Microsoft.Extensions.DependencyInjection;
    using Microsoft.Extensions.Hosting;
    using Microsoft.Extensions.Options;
    using Microsoft.IdentityModel.Logging;
    using Newtonsoft.Json.Converters;
    using Newtonsoft.Json.Serialization;
    using NSwag;
    using NSwag.Generation.Processors.Security;
    using Socure.Client;

#pragma warning disable CS1591

    public class Startup
    {
        public IConfiguration Configuration
        {
            get;
        }

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        /// <summary>
        /// This method gets called by the runtime. Use this method to add services to the container.
        /// For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        /// </summary>
        /// <param name="services"></param>
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddLocalization(options =>
            {
                options.ResourcesPath = "Resources";
            });

            services.AddApplication();

            services.AddPersistence(Configuration);

            services.AddDatabaseDeveloperPageExceptionFilter();

            services.AddCors();

            services.AddHttpContextAccessor();

            services.AddJWTAuthentication(Configuration);

            services.AddSettings(Configuration);

            IdentityModelEventSource.ShowPII = true;

            services.Configure<ForwardedHeadersOptions>(options =>
            {
                options.ForwardedHeaders = ForwardedHeaders.XForwardedFor | ForwardedHeaders.XForwardedProto;
                options.KnownNetworks.Clear();
                options.KnownProxies.Clear();
            });

            services.Configure<RequestLocalizationOptions>(
                options =>
                {
                    options.SupportedCultures = new List<CultureInfo>
                    {
                        new CultureInfo("en"),
                        new CultureInfo("es")
                    };

                    options.DefaultRequestCulture = new RequestCulture(culture: "en", uiCulture: "en");
                    options.SupportedUICultures = options.SupportedCultures;

                    options.RequestCultureProviders = new List<IRequestCultureProvider>
                    {
                       new AcceptLanguageHeaderRequestCultureProvider()
                    };
                });

            services.AddScoped<ICurrentUserService, CurrentUserService>();
            services.AddScoped<ICurrentUserIpAddressService, CurrentUserService>();
            services.AddScoped<ICurrentAppVersionService, CurrentUserService>();
            services.AddScoped<IAdminApiCurrentUserService, AdminApiCurrentUserService>();

            services.AddHealthChecks()
                .AddDbContextCheck<BNineDbContext>();

            services
                .AddControllers(options => options.Filters.Add(new ApiExceptionFilterAttribute()))
                .AddFluentValidation()
                .AddNewtonsoftJson(options =>
                {
                    options.SerializerSettings.ContractResolver = new DefaultContractResolver
                    {
                        NamingStrategy = new CamelCaseNamingStrategy
                        {
                            ProcessDictionaryKeys = true,
                        }
                    };
                    options.SerializerSettings.Converters.Add(new StringEnumConverter());

                    options.SerializerSettings.DateFormatString = "yyyy'-'MM'-'dd'T'HH':'mm':'ss.fffffff";
                });

            services.Configure<ApiBehaviorOptions>(options =>
            {
                options.SuppressModelStateInvalidFilter = true;
            });

            services.AddOpenApiDocument(configure =>
            {
                configure.Title = "BNine API";
                configure.AllowReferencesWithProperties = true;
                configure.AddSecurity("JWT", Enumerable.Empty<string>(), new OpenApiSecurityScheme
                {
                    Type = OpenApiSecuritySchemeType.ApiKey,
                    Name = "Authorization",
                    In = OpenApiSecurityApiKeyLocation.Header,
                    Description = "Type into the textbox: Bearer {your JWT token}."
                });

                configure.OperationProcessors.Add(new AspNetCoreOperationSecurityScopeProcessor("JWT"));
            });

            services.AddCors((options) =>
            {
                options.AddPolicy("BnineCorsPolicy",
                    builder => builder
                    .SetIsOriginAllowedToAllowWildcardSubdomains()
                    .WithOrigins("https://*.bnine.com")
                    .AllowAnyMethod()
                    .AllowCredentials()
                    .AllowAnyHeader()
                    .Build());
            });

            services.AddTransient<IActivateClientsService, ActivateClientsService>();

            services.AddTransient<INotificationUsersService, NotificationUsersService>();
            services.AddScoped<IDomainEventsService, DomainEventsService>();

            services.AddTransient<ICreditProductService, CreditProductService>();
            services.AddTransient<IFeatureConfigurationService, FeatureConfigurationService>();
            services.AddTransient<ITariffPlanService, TariffPlanService>();
            services.AddTransient<IPaidUserServicesService, PaidUserServicesService>();
            services.AddTransient<ICheckCreditScorePaidService, CheckCreditScorePaidService>();
            services.AddTransient<ITransferIconsAndNamesProviderService, TransferIconsAndNamesProviderService>();
            services.AddSingleton<IDateTimeProviderService, DateTimeProviderService>();
            services.AddSingleton<IDateTimeUtcProviderService, DateTimeProviderService>();
            services.AddTransient<IBankCreditCardsService, MBanqCreditCardsService>();
            services.AddTransient<IPushPullFromExternalCardErrorHandlingService, PushPullFromExternalCardErrorHandlingService>();
            services.AddTransient<IMarketingBannersService, MarketingBannersService>();
            services.AddScoped<IMarketingBannersAdminService, MarketingBannersAdminService>();
            services.AddTransient<IClientDocumentsOriginalsPersistenceService, ClientDocumentsOriginalsPersistenceService>();
            services.AddTransient<ITransactionAnalyticsBuilderResolver, TransactionAnalyticsBuilderResolver>();
            services.AddTransient<IAprilTaxService, AprilTaxService>();
            services.AddTransient<ITransfersMappingService, TransfersMappingService>();
            services.AddTransient<IAdvanceSimulationViewModelMapper, AdvanceSimulationViewModelMapper>();
            services.AddTransient<ICardsAndAccountsDigitsProviderService, CardsAndAccountsDigitsProviderService>();
            services.AddScoped<IPartnerProviderService, PartnerProviderService>();
            services.AddTransient<IDateTimeCalculationService, DelayedBoostDateTimeCalculationService>();
            services.AddTransient<IBankClassificationsService, MBanqClassificationsService>();
            services.AddScoped<IUserActivitiesCooldownService, UserActivitiesCooldownService>();
            services.AddTransient<IAdvanceWidgetStateProvider, AdvanceWidgetStateProvider>();
            services.AddTransient<IAdvancesCompletedCheckupService, AdvancesCompletedCheckupService>();
            services.AddTransient<ILoanAccountProvider, LoanAccountProvider>();
            services.AddTransient<ILoanSettingsProvider, LoanSettingsProvider>();
            services.AddTransient<IClientAccountSettingsProvider, ClientAccountSettingsProvider>();
            services.AddTransient<IAccountClosureScheduler, AccountClosureScheduler>();
            services.AddTransient<ICreditScoreBasicOfferInfoScreenViewModelFactory, CreditScoreBasicOfferInfoScreenViewModelFactory>();
            services.AddTransient<IAccountClosurePrerequisitesProvider, AccountClosurePrerequisitesProvider>();
            services.AddTransient<IAccountClosureDialogViewModelFactory, AccountClosureDialogViewModelFactory>();
            services.AddTransient<IClientAccountBlockedStateProvider, ClientAccountBlockedStateProvider>();
            services.AddTransient<IAccountStateMachineFactory, AccountStateMachineFactory>();
            services.AddTransient<IClientAccountBlockedStateFactory, ClientAccountBlockedStateFactory>();
            services.AddTransient<IAccountClosureProcessor, AccountClosureProcessor>();
            services.AddTransient<IPayrollConnectionsProvider, PayrollConnectionsProvider>();
            services.AddPaycheckSwitch();

            services.AddHostedService<AccountClosureScheduleMonitor>();

            services.AddMBanqAdministration();
            services.AddMBanqClient();
            services.AddMBanqInfrastructure(Configuration);
            services.AddDwhIntegration();

            services.AddTwilio();

            services.AddDocsAlloy(Configuration);

            services.AddBlobStorage();

            services.AddSendGrid();

            services.AddPlaid(Configuration);

            services.AddNotificationHub();

            services.AddArgyle(Configuration);
            services.AddTruv(Configuration);
            services.AddZendesk(Configuration);
            services.AddFirebase();
            services.AddMemoryCache();

            services.AddAzureServiceBus(Configuration);

            services.AddAlltrustCheckCashingClient(Configuration);
            services.AddAppsflyer(Configuration);

            services.AddArray(Configuration);

            services.AddSocureClient(Configuration);

            services.AddAprilTaxServiceClient(Configuration);
        }

        /// <summary>
        /// This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        /// </summary>
        /// <param name="app"></param>
        /// <param name="env"></param>
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            app.UseForwardedHeaders();

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseMigrationsEndPoint();
            }
            else
            {
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
                app.UseMiddleware<SwaggerBasicAuthMiddleware>();
            }

            app.UseHealthChecks("/health");

            app.UseRequestLocalization(
                app.ApplicationServices.GetService<IOptions<RequestLocalizationOptions>>().Value);

            app.UseHttpsRedirection();

            app.UseStaticFiles(new StaticFileOptions
            {
                ServeUnknownFileTypes = true,
                DefaultContentType = "application/json"
            });
            app.UseOpenApi();
            app.UseSwaggerUi3();

            app.UseCors("BnineCorsPolicy");

            app.UseRouting();

            app.UseAuthentication();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{controller}/{action=Index}/{id?}");
            });
        }
    }
}
