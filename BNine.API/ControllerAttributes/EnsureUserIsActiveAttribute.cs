﻿namespace BNine.API.ControllerAttributes
{
    using System.Security.Authentication;
    using Enums;
    using Microsoft.AspNetCore.Mvc.Filters;
    using Persistence;

    /// <summary>
    /// Controller/controller method attribute that checks that user has completed registration successfully
    /// by the time they call the method. Otherwise throws an <see cref="AuthenticationException"/>.
    /// </summary>
    public class EnsureUserIsActiveAttribute : Attribute, IAuthorizationFilter
    {
        /// <inheritdoc />
        public void OnAuthorization(AuthorizationFilterContext context)
        {
            var dbContext = context.HttpContext.RequestServices.GetRequiredService<BNineDbContext>();
            var userId = context.HttpContext.User.Claims
                .FirstOrDefault(x =>
                    x.Type == "http://schemas.xmlsoap.org/ws/2005/05/identity/claims/nameidentifier")?.Value;

            if (Guid.TryParse(userId, out var userGuid))
            {
                var user = dbContext.Users.FirstOrDefault(x => x.Id == userGuid);
                if (user?.Status == UserStatus.Active)
                {
                    return;
                }
            }
            throw new AuthenticationException("User is not supposed to call this method before completing registration.");
        }
    }
}
