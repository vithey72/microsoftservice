﻿namespace BNine.API.Services;

using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using Application.Interfaces;
using Enums;

public class CurrentUserService
    : ICurrentUserService, ICurrentUserIpAddressService, ICurrentAppVersionService
{
    private readonly IHttpContextAccessor _httpContextAccessor;

    public CurrentUserService(IHttpContextAccessor httpContextAccessor)
    {
        _httpContextAccessor = httpContextAccessor;
    }

    public Guid? UserId
    {
        get
        {
            var stringId = _httpContextAccessor.HttpContext?.User?.FindFirstValue(ClaimTypes.NameIdentifier);

            return Guid.TryParse(stringId, out var guid) ? guid : null;
        }
    }

    public string? UserMbanqToken
    {
        get
        {
            var authHeader = _httpContextAccessor
                .HttpContext!
                .Request
                .Headers
                .FirstOrDefault(x => x.Key == "Authorization")
                .Value;

            if (string.IsNullOrEmpty(authHeader))
            {
                return null;
            }

            var testToken = authHeader.ToString().Split(" ")[1];

            var handler = new JwtSecurityTokenHandler();

            var jwtToken = handler.ReadToken(testToken) as JwtSecurityToken;

            var mbanqToken = jwtToken.Claims.First(claim => claim.Type == "mbanq_access_token").Value;

            return mbanqToken != null ? mbanqToken : null;
        }
    }

    public Guid[] GroupIds
    {
        get
        {
            var bearerJwtToken = _httpContextAccessor.HttpContext.Request.Headers.First(x => x.Key == "Authorization")
                .Value.ToString().Split(" ")[1];

            var handler = new JwtSecurityTokenHandler();

            var jwtToken = handler.ReadToken(bearerJwtToken) as JwtSecurityToken;

            var groups = jwtToken.Claims.Where(claim => claim.Type == "groups").Select(claim => Guid.Parse(claim.Value))
                .ToArray();

            return groups;
        }
    }

    public JwtSecurityToken AccessToken
    {
        get
        {
            if (!_httpContextAccessor.HttpContext.Request.Headers.Any(x => x.Key == "Authorization"))
            {
                return null;
            }

            var bearerJwtToken = _httpContextAccessor.HttpContext.Request.Headers.First(x => x.Key == "Authorization")
                .Value.ToString().Split(" ")[1];

            var handler = new JwtSecurityTokenHandler();

            return handler.ReadToken(bearerJwtToken) as JwtSecurityToken;
        }
    }

    /// <summary>
    /// OS of the phone of the user.
    /// </summary>
    public MobilePlatform Platform
    {
        get
        {
            var key = "User-Agent";
            if (_httpContextAccessor.HttpContext!.Request.Headers.All(x => x.Key != key))
            {
                return MobilePlatform.iOs;
            }

            var clientHttpLibrary = _httpContextAccessor.HttpContext.Request.Headers.First(x => x.Key == key)
                .Value.ToString();

            if (clientHttpLibrary.StartsWith("okhttp", StringComparison.InvariantCultureIgnoreCase))
            {
                return MobilePlatform.Android;
            }

            return MobilePlatform.iOs;
        }
    }

    /// <summary>
    /// IP address of the user.
    /// </summary>
    public string CurrentIp
    {
        get => _httpContextAccessor.HttpContext!.Connection.RemoteIpAddress!.ToString();

    }

    public Version AppVersion
    {
        get
        {
            var version = _httpContextAccessor.HttpContext!.Request.Headers["B9App-Version"].ToString().Split("-")[0];
            if (string.IsNullOrEmpty(version))
            {
                return null;
            }
            var isSuccess = Version.TryParse(version, out var parsedVersion);
            return !isSuccess ? null : parsedVersion;
        }



    }
}
