﻿namespace BNine.API.Controllers;

using Application.Aggregates.AgreementDocuments.AgreementScreen;
using Application.Aggregates.AgreementDocuments.AgreementScreen.Models;
using Application.Aggregates.AgreementDocuments.SaveAgreementDocuments;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

/// <summary>
/// Controller to handle agreements to legal documents.
/// </summary>
///
[Authorize]
[Route("agreement-documents")]
public class AgreementItemsController : ApiController
{
    private readonly ILogger<AgreementItemsController> _logger;

    /// <inheritdoc />
    public AgreementItemsController(ILogger<AgreementItemsController> logger) : base(logger)
    {
        _logger = logger;
    }

    /// <summary>
    /// Validates that user agreed to all required documents.
    /// And saves the list of agreed documents to user settings.
    /// </summary>
    /// <param name="command"></param>
    /// <returns></returns>
    [HttpPost]
    [Route("user-accepted")]
    public async Task<IActionResult> UploadUserAcceptedDocuments(SaveUserAgreementItemsCommand command)
    {

        await Mediator.Send(
            new ValidateSaveUserAgreementItemsCommand(
                command.UserAgreedDocumentsKeys,
                command.ScreenType));

        await Mediator.Send(command);
        return Ok();
    }

    /// <summary>
    /// Provides a view model for Agreement Screen
    /// </summary>
    /// <returns></returns>
    [HttpGet]
    [Route("agreement-screen")]
    [ProducesResponseType(typeof(AgreementScreenViewModel), Status200OK)]
    public async Task<IActionResult> GetAgreementScreen()
    {
        var result = await Mediator.Send(
            new GetAgreementScreenViewModelQuery());
        return Ok(result);
    }

    /// <summary>
    /// Endpoint to get viewmodel for the SMS agreement screens
    /// </summary>
    /// <returns></returns>
    [HttpGet]
    [Route("sms-agreement-screen")]
    [AllowAnonymous]
    [ProducesResponseType(typeof(AgreementScreenViewModelBase), Status200OK)]
    public async Task<IActionResult> GetSmsAgreementScreen()
    {
        var result = await Mediator.Send(
            new GetSmsAgreementScreenViewModelQuery());
        return Ok(result);
    }
}
