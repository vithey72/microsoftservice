﻿namespace BNine.API.Controllers
{
    using System.Net;
    using System.Threading.Tasks;
    using Application.Aggregates.UserQuestionnaire.Commands;
    using Application.Aggregates.UserQuestionnaire.Models;
    using Application.Aggregates.UserQuestionnaire.Queries;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.Extensions.Logging;
    using NSwag.Annotations;

    /// <summary>
    /// Controller with methods that return questionnaires viewmodels and accept users' responses to those.
    /// </summary>
    [OpenApiTag("User Questionnaire")]
    [Route("user-questionnaire")]
    [ApiController]
    [Authorize]
    public class UserQuestionnaireController : ApiController
    {
        /// <inheritdoc />
        public UserQuestionnaireController(ILogger<UserQuestionnaireController> logger)
            : base(logger)
        {
        }

        /// <summary>
        /// Get user questionnaire.
        /// </summary>
        /// <param name="questionSet">Name of questionnaire. If empty then default set is queried. If unknown name then empty response.</param>
        [HttpGet]
        [ProducesResponseType(typeof(GetQuestionsResponseModel), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> GetQuestions([FromQuery] string questionSet)
        {
            var query = new GetQuestionsQuery { QuestionSetName = questionSet };
            var questions = await Mediator.Send(query);
            return Ok(questions);
        }

        /// <summary>
        /// Submit user responses to a questionnaire.
        /// </summary>
        /// <param name="questionResponseCommand"></param>
        /// <returns></returns>
        [HttpPost]
        [ProducesResponseType(typeof(GetQuestionsResponseModel), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> SubmitQuestionnaireResponses(SubmitQuestionnaireResponsesCommand questionResponseCommand)
        {
            var questions = await Mediator.Send(questionResponseCommand);
            return Ok(questions);
        }
    }
}
