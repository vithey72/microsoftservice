﻿namespace BNine.API.Controllers;

using System.Net;
using System.Threading.Tasks;
using Application.Aggregates.Configuration;
using Application.Aggregates.Configuration.Models;
using Application.Aggregates.Configuration.Queries.GetAppMenu;
using Application.Aggregates.Configuration.Queries.GetConfiguration;
using BNine.Application.Aggregates.Configuration.Queries.GetWalletBlocks;
using Enums;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

[Route("configuration")]
public class ConfigurationController : ApiController
{
    public ConfigurationController(ILogger<ConfigurationController> logger) : base(logger)
    {
    }

    [AllowAnonymous]
    [HttpGet]
    [ProducesResponseType(typeof(ConfigurationResponseModel), (int)HttpStatusCode.OK)]
    [ProducesResponseType((int)HttpStatusCode.BadRequest)]
    public async Task<IActionResult> Get(
        [FromHeader(Name = "B9App-Platform")]
        MobilePlatform? mobilePlatform,
        [FromHeader(Name = "B9App-Version")]
        [ModelBinder(BinderType = typeof(VersionModelBinder))]
        Version appVersion)
    {
        var request =
            new GetConfigurationQuery
            (appVersion: appVersion,
                mobilePlatform: mobilePlatform);
        var response = await Mediator.Send(request);
        return Ok(response);
    }

    [ProducesResponseType(typeof(GetAppMenuViewModel), Status200OK)]
    [HttpGet("menu")]
    public async Task<IActionResult> GetAppMenu()
    {
        var response = await Mediator.Send(new GetAppMenuQuery());
        return Ok(response);
    }

    /// <summary>
    /// Returns a model describing wallet block collection and it's presentation parameters
    /// </summary>
    /// <returns></returns>
    [HttpGet("wallet-blocks")]
    [ProducesResponseType(typeof(WalletBlockCollectionViewModel), (int)HttpStatusCode.OK)]
    public async Task<IActionResult> GetWalletBlockCollectionViewModel()
    {
        var response = await Mediator.Send(new GetWalletBlockCollectionQuery());
        return Ok(response);
    }
}
