﻿namespace BNine.API.Controllers;

using BNine.Application.Aggregates.CommonModels.Dialog;
using BNine.Application.Aggregates.Configuration.Queries.GetConfiguration;
using BNine.Application.Aggregates.CreditScore.Models;
using BNine.Application.Aggregates.CreditScore.Queries;
using BNine.Application.Exceptions;
using BNine.Application.Interfaces;
using BNine.Constants;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

/// <summary>
/// Controller with endpoints providing a contract for credit score flow
/// </summary>
[Authorize]
[Route("credit-score")]
[ApiController]
public class CreditScoreController : ApiController
{
    private readonly ICurrentUserService _currentUserService;

    /// <inheritdoc />
    public CreditScoreController(
        ILogger<CreditScoreController> logger,
        ICurrentUserService currentUserService
        ) : base(logger)
    {
        _currentUserService = currentUserService;
    }

    /// <summary>
    /// Returns a basic tariff credit score offer info screen view model
    /// </summary>
    /// <returns></returns>
    [HttpGet("basic-offer")]
    [ProducesResponseType(typeof(CreditScoreBasicOfferInfoScreenViewModel), Status200OK)]
    [ProducesResponseType(Status204NoContent)]
    [ProducesResponseType(Status401Unauthorized)]
    [ProducesResponseType(Status400BadRequest)]
    public async Task<IActionResult> GetBasicOffer()
    {
        await ThrowIfNoCheckScorePaidServiceFeature();

        var response = await Mediator.Send(new GetCreditScoreBasicOfferInfoScreenQuery());

        return NoContentOrResult(response);
    }

    /// <summary>
    /// Returns a basic tariff credit score buying confirmation screen dialog
    /// </summary>
    /// <returns></returns>
    [HttpGet("basic-offer/confirmation")]
    [ProducesResponseType(typeof(GenericConfirmationDialogViewModel), Status200OK)]
    [ProducesResponseType(Status401Unauthorized)]
    [ProducesResponseType(Status400BadRequest)]
    public async Task<IActionResult> GetBasicOfferBuyingConfirmation()
    {
        await ThrowIfNoCheckScorePaidServiceFeature();

        var response = await Mediator.Send(new GetCreditScoreBasicOfferConfirmationDialogQuery());
        return Ok(response);
    }

    /// <summary>
    /// Activate the credit score service and charge client for the period.
    /// </summary>
    [HttpPut("basic-offer")]
    [ProducesResponseType(typeof(GenericActivateServiceResultDialogViewModel), Status200OK)]
    [ProducesResponseType(Status401Unauthorized)]
    [ProducesResponseType(Status400BadRequest)]
    public async Task<IActionResult> ActivateCheckScorePaidService()
    {
        await ThrowIfNoCheckScorePaidServiceFeature();

        var response = await Mediator.Send(new ActivateCheckCreditScorePaidServiceQuery
        {
            MbanqAccessToken = _currentUserService.UserMbanqToken,
            Ip = HttpContext.Connection.RemoteIpAddress!.ToString(),
        });
        return Ok(response);
    }

    private async Task ThrowIfNoCheckScorePaidServiceFeature()
    {
        var configuration = await Mediator.Send(new GetConfigurationQuery());
        if (!configuration.HasFeatureEnabled(FeaturesNames.Features.CheckScorePaidService))
        {
            throw new ValidationException("feature",
                $"User does not have access to {FeaturesNames.Features.CheckScorePaidService} feature.");
        }
    }
}
