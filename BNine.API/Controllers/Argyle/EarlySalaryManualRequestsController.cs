﻿namespace BNine.API.Controllers.Argyle
{
    using System.Net;
    using System.Threading.Tasks;
    using Application.Aggregates.EarlySalary.EarlySalary.Queries.DownloadManualForm;
    using BNine.Application.Aggregates.EarlySalary.EarlySalary.Commands.SendEarlySalaryManualForm;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.Extensions.Logging;

    [Route("early-salary-manual-requests")]
    [Authorize]
    public class EarlySalaryManualRequestsController : ApiController
    {
        /// <inheritdoc />
        public EarlySalaryManualRequestsController(ILogger<EarlySalaryManualRequestsController> logger) : base(logger)
        {
        }

        /// <summary>
        /// Create early salary manual request
        /// </summary>
        /// <param name="command"></param>
        /// <returns></returns>
        [HttpPost]
        [ProducesResponseType((int)HttpStatusCode.Unauthorized)]
        [ProducesResponseType((int)HttpStatusCode.NoContent)]
        public async Task<IActionResult> Create([FromBody] SendEarlySalaryManualFormCommand command)
        {
            await Mediator.Send(command);
            return NoContent();
        }

        /// <summary>
        /// Download a PDF file of manual advance form.
        /// </summary>
        /// <param name="userId">Id of the user in database</param>
        /// <param name="hashCode">Hash of constant user entity fields that serves as password</param>
        /// <returns></returns>
        [HttpGet("download")]
        [AllowAnonymous]
        public async Task<IActionResult> DownloadManualForm([FromQuery] Guid userId, [FromQuery] string hashCode)
        {
            var data = await Mediator.Send(new DownloadManualFormQuery
            {
                UserId = userId,
                HashCode = hashCode,
            });

            return File(data, "application/pdf", "Generated application (Early salary).pdf");
        }
    }
}
