﻿namespace BNine.API.Controllers.Argyle
{
    using System;
    using System.Net;
    using System.Threading.Tasks;
    using BNine.Application.Aggregates.EarlySalary.ArgyleSettings.Command.UpdateArgyleSettings;
    using BNine.Application.Aggregates.EarlySalary.ArgyleSettings.Queries.GetArgyleSettings;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.Extensions.Configuration;
    using Microsoft.Extensions.Logging;

    /// <summary>
    /// This controller should be removed when admin panel is implemented
    /// </summary>
    [Route("argyle-settings")]
    public class ArgyleSettingsController : ApiController
    {
        private readonly IConfiguration _configuration;

        public ArgyleSettingsController(ILogger<ArgyleSettingsController> logger, IConfiguration configuration) : base(logger)
        {
            _configuration = configuration;
        }

        /// <summary>
        /// Get argyle settings
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [ProducesResponseType((int)HttpStatusCode.Unauthorized)]
        [ProducesResponseType(typeof(ArgyleSettingsInfo), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> Get([FromQuery] Guid? token)
        {
            var apiKey = _configuration["APIKey"];
            if (!token.HasValue || !token.ToString().Equals(apiKey, StringComparison.OrdinalIgnoreCase))
            {
                return Unauthorized();
            };
            var result = await Mediator.Send(new GetArgyleSettingsQuery());
            return Ok(result);
        }

        /// <summary>
        /// Put argyle settings
        /// </summary>
        /// <returns></returns>
        [HttpPut]
        [ProducesResponseType((int)HttpStatusCode.Unauthorized)]
        [ProducesResponseType((int)HttpStatusCode.NoContent)]
        public async Task<IActionResult> Put([FromQuery] Guid? token, [FromBody] UpdateArgyleSettingsCommand command)
        {
            var apiKey = _configuration["APIKey"];
            if (!token.HasValue || !token.ToString().Equals(apiKey, StringComparison.OrdinalIgnoreCase))
            {
                return Unauthorized();
            };
            await Mediator.Send(command);
            return NoContent();
        }
    }
}
