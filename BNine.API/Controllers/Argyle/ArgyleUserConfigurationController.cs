﻿namespace BNine.API.Controllers.Argyle;

using System.Net;
using System.Threading.Tasks;
using Application.Aggregates.EarlySalary.WidgetConfiguration.Commands.Create;
using Application.Aggregates.EarlySalary.WidgetConfiguration.Commands.Update;
using Application.Aggregates.EarlySalary.WidgetConfiguration.Models;
using Application.Aggregates.EarlySalary.WidgetConfiguration.Queries.Get;
using ControllerAttributes;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

/// <summary>
/// Argyle controller.
/// </summary>
[Authorize]
[EnsureUserIsActive]
[Route("early-salary/widget-configuration")]
public class ArgyleUserConfigurationController : ApiController
{
    /// <inheritdoc />
    public ArgyleUserConfigurationController(ILogger<ArgyleUserConfigurationController> logger) : base(logger)
    {
    }

    /// <summary>
    /// Returns credentials and settings to access Argyle/Truv via SDK, and creates user, if necessary.
    /// </summary>
    [HttpGet]
    [ProducesResponseType((int)HttpStatusCode.NotFound)]
    [ProducesResponseType(typeof(ArgyleUserConfiguration), Status200OK)]
    public async Task<IActionResult> GetPayrollProviderUserConfiguration()
    {
        var result = await Mediator.Send(new GetArgyleUserConfigurationQuery());
        return Ok(result);
    }

    /// <summary>
    /// Creates early salary widget details with provided token and user id.
    /// Throws exception if payroll provider for user is not Argyle.
    /// </summary>
    [Obsolete("Token is generated in the GET method above.")]
    [HttpPost("token")]
    [ProducesResponseType(Status201Created)]
    [ProducesResponseType(typeof(ProblemDetails), Status500InternalServerError)]
    public async Task<IActionResult> CreateToken([FromBody] CreateArgyleTokenCommand command)
    {
        await Mediator.Send(command);
        return Created();
    }

    /// <summary>
    /// Updates early salary widget details token.
    /// </summary>
    [Obsolete("Token is updated via backend in GET method above")]
    [HttpPut("token")]
    [ProducesResponseType(Status204NoContent)]
    public async Task<IActionResult> UpdateToken([FromBody] UpdateWidgetTokenCommand command)
    {
        await Mediator.Send(command);
        return NoContent();
    }
}
