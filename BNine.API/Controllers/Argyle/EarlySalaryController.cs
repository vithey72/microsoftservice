﻿namespace BNine.API.Controllers.Argyle
{
    using System.Net;
    using System.Threading.Tasks;
    using Application.Aggregates.CheckingAccounts.Queries.GetInfo;
    using BNine.Application.Aggregates.EarlySalary.EarlySalary.Queries.GetPdf;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.Extensions.Logging;

    [Authorize]
    [Route("early-salary")]
    public class EarlySalaryController : ApiController
    {
        /// <inheritdoc />
        public EarlySalaryController(ILogger<EarlySalaryController> logger) : base(logger)
        {
        }

        /// <summary>
        /// generate and send pdf to user email
        /// </summary>
        /// <returns></returns>
        [HttpGet("pdf")]
        [ProducesResponseType(typeof(bool), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> GetPdf()
        {
            var result = await Mediator.Send(new GetInfoQuery());

            var emailSendResponse = await Mediator.Send(
                new GetPdfQuery
                {
                    Beneficiary = result.OwnerFirstName + " " + result.OwnerLastName,
                    AccountNumber = result.AccountNumber,
                    RoutingNumber = result.RoutingNumber
                }
            );

            return Ok(emailSendResponse);
        }
    }
}
