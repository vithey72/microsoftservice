﻿namespace BNine.API.Controllers.Argyle
{
    using System.Net;
    using System.Threading.Tasks;
    using BNine.Application.Aggregates.EarlySalary.EarlySalaryManualSettings.Queries.GetArgyleSettings;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.Extensions.Logging;

    [Route("early-salary-manual-settings")]
    [Authorize]
    public class EarlySalaryManualSettingsController : ApiController
    {
        public EarlySalaryManualSettingsController(ILogger<EarlySalaryManualSettingsController> logger) : base(logger)
        {
        }

        /// <summary>
        /// Get early salary manual form settings
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [ProducesResponseType((int)HttpStatusCode.Unauthorized)]
        [ProducesResponseType(typeof(EarlySalaryManualInfo), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> Get()
        {
            var result = await Mediator.Send(new GetEarlySalaryManualSettingsQuery());
            return Ok(result);
        }
    }
}
