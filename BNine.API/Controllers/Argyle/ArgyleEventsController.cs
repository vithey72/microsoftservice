﻿namespace BNine.API.Controllers.Argyle
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using BNine.Application.Aggregates.PaycheckSwitch;
    using BNine.Application.Extensions;
    using BNine.Application.Interfaces;
    using BNine.Application.Interfaces.ServiceBus;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.Extensions.Logging;
    using Newtonsoft.Json;

    [Route("argyle-events")]
    [Authorize]
    public class ArgyleEventsController : ApiController
    {
        private readonly IServiceBus _serviceBus;
        private readonly ICurrentUserService _currentUserService;
        private readonly IPaycheckSwitchDiscovery _paycheckSwitchDiscovery;

        /// <inheritdoc />
        public ArgyleEventsController(
            IServiceBus serviceBus,
            ILogger<ArgyleEventsController> logger,
            ICurrentUserService currentUserService,
            IPaycheckSwitchDiscovery paycheckSwitchDiscovery
            )
            : base(logger)
        {
            _currentUserService = currentUserService;
            _serviceBus = serviceBus;
            _paycheckSwitchDiscovery = paycheckSwitchDiscovery;
        }

        /// <summary>
        /// Post argyle ui event to service bus topic
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> PostEvent([FromBody] ArgyleUIEventModel model)
        {
            model.Items.Add("b9UserId", _currentUserService.UserId().ToString());
            model.Items.Add("createdAt", DateTime.UtcNow.ToString("yyyy'-'MM'-'dd'T'HH':'mm':'ss.fffffff"));

            var paycheckSwitch = await _paycheckSwitchDiscovery.DiscoverUserPaycheckSwitch(_currentUserService.UserId(), HttpContext.RequestAborted);
            var @event = await paycheckSwitch.CreateIntegrationEvent(model.Event, model.Items, HttpContext.RequestAborted);

            await _serviceBus.Publish(@event);

            return NoContent();
        }
    }

    public class ArgyleUIEventModel
    {
        [JsonProperty("event")]
        public string Event
        {
            get; set;
        }

        [JsonProperty("items")]
        public Dictionary<string, object> Items
        {
            get; set;
        } = new Dictionary<string, object>();
    }
}
