﻿namespace BNine.API.Controllers;

using System.Threading.Tasks;
using Application.Aggregates.CommonModels.Dialog;
using Application.Aggregates.Evaluation.Commands.SaveUserDocument;
using Application.Aggregates.Evaluation.Models;
using Application.Aggregates.Evaluation.Queries.GetDocumentVerificationConfig;
using Application.Aggregates.Onboarding.Commands.CompletePreActivationStep;
using Application.Aggregates.Onboarding.Commands.SaveUserSelfieOnRegistration;
using Application.Aggregates.Onboarding.Models;
using Application.Aggregates.Onboarding.Queries.GetPreActivationStepStatus;
using Application.Aggregates.Users.Queries.ConfirmSummaryInformationNew;
using Application.Interfaces;
using BNine.Application.Aggregates.Selfie.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

/// <summary>
/// Controller with methods for onboarding process. Should be used for onboarding steps that use new KYC flow (Socure).
/// Will have relevant methods from <see cref="ProfileController"/> moved here.
/// </summary>
[Route("onboarding")]
[Authorize]
public class OnboardingController
    : ApiController
{
    private readonly ICurrentUserService _currentUserService;

    /// <inheritdoc />
    public OnboardingController(
        ILogger<OnboardingController> logger,
        ICurrentUserService currentUserService
        ) : base(logger)
    {
        _currentUserService = currentUserService;
    }

    /// <summary>
    /// Returns text and config to be used on the Proof of Identity screen
    /// </summary>
    [HttpGet("documentVerification")]
    [ProducesResponseType(typeof(DocumentVerificationStepConfigViewModel), Status200OK)]
    public async Task<IActionResult> GetDocumentVerificationConfig()
    {
        var result = await Mediator.Send(new GetDocumentVerificationStepConfigQuery());

        return Ok(result);
    }

    /// <summary>
    /// Save document OCR results. Drivers license and such.
    /// </summary>
    [HttpPut("documentVerification")]
    [ProducesResponseType(Status200OK)]
    [ProducesResponseType(Status400BadRequest)]
    public async Task<IActionResult> UpdateDocumentVerification([FromBody] SaveUserDocumentCommand command)
    {
        command.UserId = _currentUserService.UserId!.Value;
        await Mediator.Send(command);
        return Ok();
    }

    /// <summary>
    /// Upload a client's selfie.
    /// </summary>
    [HttpPut("selfie")]
    [ProducesResponseType(Status200OK)]
    [ProducesResponseType(Status400BadRequest)]
    public async Task<IActionResult> UploadSelfie([FromForm] SelfieUpload selfieFile)
    {
        if (selfieFile == null)
        {
            return BadRequest("File isn't provided");
        }

        var command = new UploadUserSelfieOnRegistrationCommand
        {
            SelfieFile = selfieFile,
            UserId = _currentUserService.UserId!.Value,
        };

        await Mediator.Send(command);
        return Ok();
    }

    /// <summary>
    /// Confirm summary and perform KYC on user.
    /// </summary>
    /// <param name="query">Sigma device ID.</param>
    /// <returns>200 means no action required, 406 means it's either a resubmit or a duplicate.</returns>
    [HttpPut("summary")]
    [ProducesResponseType(Status200OK)]
    [ProducesResponseType(Status400BadRequest)]
    [ProducesResponseType(typeof(GenericFailDialog), Status406NotAcceptable)]
    public async Task<IActionResult> ConfirmSummary(ConfirmSummaryNewQuery query)
    {
        query.IpAddress = HttpContext.Connection.RemoteIpAddress!.ToString();

        var response = await Mediator.Send(query);
        if (response.IsSuccess)
        {
            return Ok();
        }

        return NotAcceptable(response.Fail);
    }

    /// <summary>
    /// Returns details for the final action required before client is activated in the app (like purchasing a tariff).
    /// </summary>
    [HttpGet("preActivation")]
    [ProducesResponseType(typeof(PreActivationStepStatusViewModel), Status200OK)]
    [ProducesResponseType(Status204NoContent)]
    public async Task<IActionResult> GetPreActivationActions()
    {
        var result = await Mediator.Send(new GetPreActivationStepStatusQuery());
        if (result == null)
        {
            return NoContent();
        }

        return Ok(result);
    }

    /// <summary>
    /// Save document OCR results. Drivers license and such.
    /// </summary>
    [HttpPut("preActivation")]
    [ProducesResponseType(Status200OK)]
    [ProducesResponseType(Status400BadRequest)]
    public async Task<IActionResult> CompletePreActivationStep([FromBody] CompletePreActivationStepCommand command)
    {
        await Mediator.Send(command);
        return Ok();
    }
}
