﻿namespace BNine.API.Controllers;

using System.Threading.Tasks;
using Application.Aggregates.Users.Models;
using Application.Aggregates.Users.Queries.Cashback.ActivateCashback;
using Application.Aggregates.Users.Queries.Cashback.GetAvailableCashbackCategories;
using Application.Aggregates.Users.Queries.Cashback.GetCashbackAvailability;
using Application.Aggregates.Users.Queries.Cashback.GetCashbackProgramStatus;
using Application.Aggregates.Users.Queries.Cashback.GetCashbackSlideshow;
using Application.Aggregates.Users.Queries.Cashback.SaveCategoriesRating;
using Application.Interfaces;
using ControllerAttributes;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using NSwag.Annotations;

/// <summary>
/// Controller responsible for allowing users to enable cashback and choose categories.
/// Also provides reference values of current cashback program settings.
/// </summary>
[Authorize]
[OpenApiTag("Cashback Program")]
public class CashbackProgramController : ApiController
{
    private readonly ICurrentUserService _currentUser;

    /// <summary>
    /// Standard constructor.
    /// </summary>
    public CashbackProgramController(
        ILogger<CashbackProgramController> logger,
        ICurrentUserService currentUser)
        : base(logger)
    {
        _currentUser = currentUser;
    }

    /// <summary>
    /// Returns viewmodel for displaying cashback status for current or upcoming month.
    /// </summary>
    [EnsureUserIsActive]
    [HttpGet("cashback-program")]
    [ProducesResponseType(typeof(CashbackAvailabilityViewModel), Status200OK)]
    public async Task<IActionResult> GetCashbackAvailability()
    {
        var result = await Mediator.Send(new GetCashbackAvailabilityQuery());
        return Ok(result);
    }

    /// <summary>
    /// Get all cashback categories that B9 offers.
    /// </summary>
    [AllowAnonymous]
    [HttpGet("cashback-program/categories")]
    [ProducesResponseType(typeof(AvailableCashbackCategoriesResponse), Status200OK)]
    public async Task<IActionResult> GetAvailableCategories()
    {
        var response = await Mediator.Send(new GetAvailableCashbackCategoriesQuery());
        return Ok(response);
    }

    /// <summary>
    /// Returns information on current and upcoming cashback month and total amount earned and stuff.
    /// </summary>
    [HttpGet("cashback-program/status")]
    [ProducesResponseType(typeof(CashbackProgramStatusViewModel), Status200OK)]
    [ProducesResponseType(Status401Unauthorized)]
    public async Task<IActionResult> GetProgramStatus()
    {
        var response = await Mediator.Send(new GetCashbackProgramStatusQuery
        {
            MbanqAccessToken = _currentUser.UserMbanqToken,
            Ip = HttpContext.Connection.RemoteIpAddress!.ToString(),
        });
        return Ok(response);
    }

    /// <summary>
    /// Get text and images for screens that serve as a miniature cashback tutorial.
    /// </summary>
    /// <returns></returns>
    [HttpGet("cashback-program/slideshow")]
    [ProducesResponseType(typeof(CashbackProgramSlideshowViewModel), Status200OK)]
    [ProducesResponseType(Status401Unauthorized)]
    public async Task<IActionResult> GetCashbackSlideshow()
    {
        var response = await Mediator.Send(new GetCashbackSlideshowQuery());
        return Ok(response);
    }

    /// <summary>
    /// Activates cashback for the current or upcoming month (depending on date of the month).
    /// </summary>
    [EnsureUserIsActive]
    [HttpPost("cashback-program/activate")]
    [ProducesResponseType(typeof(CashbackAvailabilityViewModel), Status200OK)]
    public async Task<IActionResult> ActivateCashback(ActivateCashbackQuery query)
    {
        var result = await Mediator.Send(query);
        if (result != null)
        {
            return Ok(result);
        }

        return BadRequest("Cashback has already been activated");
    }

    /// <summary>
    /// Save how user rates cashback categories for selected month.
    /// </summary>
    [HttpPost("cashback-program/rate")]
    [ProducesResponseType(Status202Accepted)]
    [ProducesResponseType(Status204NoContent)]
    public async Task<IActionResult> SaveCategoriesRating(SaveCategoriesRatingQuery query)
    {
        var result = await Mediator.Send(query);
        if (result)
        {
            return Accepted();
        }

        return NoContent();
    }
}
