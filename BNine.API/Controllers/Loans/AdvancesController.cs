﻿namespace BNine.API.Controllers.Loans;

using System.Net;
using Application.Aggregates.CreditLines.Models;
using Application.Aggregates.CreditLines.Queries.AdvanceSimulation;
using Application.Aggregates.CreditLines.Queries.GetAdvanceStats;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

/// <summary>
/// Controller for fetching data of our core product - Advances.
/// </summary>
[Authorize]
[Route("advances")]
public class AdvancesController : ApiController
{
    /// <inheritdoc />
    public AdvancesController(
        ILogger<AdvancesController> logger
    )
        : base(logger)
    {
    }

    /// <summary>
    /// Get viewmodel to display Advance widget.
    /// Will replace /loans/stats in the future.
    /// </summary>
    [HttpGet("stats")]
    [ProducesResponseType(typeof(AdvanceWidgetViewModel), Status200OK)]
    [ProducesResponseType((int)HttpStatusCode.BadRequest)]
    public async Task<IActionResult> GetAdvanceStats()
    {
        var response = await Mediator.Send(new GetAdvanceWidgetViewModelQuery());
        return Ok(response);
    }

    /// <summary>
    /// Get viewmodel for Advance limits simulation, current user limits
    /// </summary>
    [HttpGet("simulation")]
    [ProducesResponseType(typeof(AdvanceSimulationViewModel), Status200OK)]
    [ProducesResponseType((int)HttpStatusCode.BadRequest)]
    public async Task<IActionResult> GetAdvanceSimulation()
    {
        var response = await Mediator.Send(new GetAdvanceSimulationQuery());
        return Ok(response);
    }

    /// <summary>
    /// Get viewmodel for Advance limits Chart simulation, based on user request params
    /// </summary>
    [HttpPost("simulation/chart")]
    [ProducesResponseType(typeof(AdvanceSimulationChartViewModel), Status200OK)]
    [ProducesResponseType((int)HttpStatusCode.BadRequest)]
    public async Task<IActionResult> RefreshAdvanceSimulationChart(RefreshAdvanceSimulationChartQuery query)
    {
        var response = await Mediator.Send(query);
        return Ok(response);
    }
}
