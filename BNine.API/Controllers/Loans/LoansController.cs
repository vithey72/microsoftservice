﻿namespace BNine.API.Controllers
{
    using System.Net;
    using System.Threading.Tasks;
    using Application.Aggregates.CreditLines.Models.LoanConditions;
    using Application.Aggregates.CreditLines.Queries.LoanConditions;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.Extensions.Logging;

    [Route("loans")]
    [ApiController]
    [Authorize]
    public class LoansController : ApiController
    {
        public LoansController(ILogger<LoansController> logger)
           : base(logger)
        {
        }

        /// <summary>
        /// Get minimum requirements to get Advance.
        /// </summary>
        /// <returns></returns>
        [HttpGet("conditions")]
        [ProducesResponseType(typeof(GetLoanConditionsResponse), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetLoanConditions()
        {
            var response = await Mediator.Send(new GetLoanConditionsQuery());
            return Ok(response);
        }
    }
}
