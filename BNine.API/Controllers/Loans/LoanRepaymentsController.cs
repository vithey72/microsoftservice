﻿namespace BNine.API.Controllers;

using System.Threading.Tasks;
using Application.Aggregates.AdvanceWidget.Commands.CreateRepaymentTransfer;
using Application.Aggregates.AdvanceWidget.Models;
using Application.Aggregates.AdvanceWidget.Queries.GetAdvanceRepaymentDetails;
using BNine.Application.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

/// <summary>
/// Controller for repayments of advance.
/// </summary>
[Route("loans")]
[Route("advances")]
[ApiController]
[Authorize]
public class LoanRepaymentsController : ApiController
{
    private ICurrentUserService CurrentUser
    {
        get;
    }

    /// <summary>
    /// Controller for repayments of advance.
    /// </summary>
    public LoanRepaymentsController(
        ILogger<LoanRepaymentsController> logger,
        ICurrentUserService currentUser)
        : base(logger)
    {
        CurrentUser = currentUser;
    }

    /// <summary>
    /// Get viewmodel for rendering of the repayment screen.
    /// </summary>
    /// <returns></returns>
    [HttpGet("repayments")]
    [ProducesResponseType(typeof(AdvanceRepaymentViewModel), Status200OK)]
    public async Task<IActionResult> GetRepaymentDetails()
    {
        var query = new GetAdvanceRepaymentDetailsQuery
        {
            Ip = HttpContext.Connection.RemoteIpAddress!.ToString(),
            MbanqAccessToken = CurrentUser.UserMbanqToken,
        };

        var result = await Mediator.Send(query);

        return Ok(result);
    }

    /// <summary>
    /// Initiate repayment transfer
    /// </summary>
    /// <returns></returns>
    [HttpPost("repayments")]
    [ProducesResponseType(Status200OK)]
    [ProducesResponseType(Status400BadRequest)]
    public async Task<IActionResult> CreateRepaymentTransfer()
    {
        var command = new CreateRepaymentTransferCommand();

        var mbanqToken = CurrentUser.UserMbanqToken;

        command.Ip = HttpContext.Connection.RemoteIpAddress!.ToString();
        command.MbanqAccessToken = mbanqToken;

        var result = await Mediator.Send(command);

        return Ok(result);
    }
}
