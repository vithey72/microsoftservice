﻿namespace BNine.API.Controllers;

using System.Net;
using System.Threading.Tasks;
using Application.Aggregates.CommonModels.Dialog;
using Application.Aggregates.CreditLines.Commands.AfterAdvanceTip;
using Application.Aggregates.CreditLines.Commands.CancelDelayedBoost;
using Application.Aggregates.CreditLines.Commands.CreateLoanTransfer;
using Application.Aggregates.CreditLines.Models;
using Application.Aggregates.CreditLines.Queries.ActivateAdvanceBoost;
using Application.Aggregates.CreditLines.Queries.ActivateAdvanceBoost.Models;
using Application.Aggregates.CreditLines.Queries.GetAdvanceBoostOffer;
using Application.Aggregates.CreditLines.Queries.GetAdvanceUnfreezeOffer;
using Application.Aggregates.CreditLines.Queries.GetAfterAdvanceTipDialog;
using Application.Aggregates.CreditLines.Queries.GetDelayedBoostStatus;
using Application.Aggregates.CreditLines.Queries.UnfreezeAdvanceExtraAmount;
using Application.Interfaces;
using BNine.Application.Aggregates.CreditLines.Queries.GetAdvanceOffer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

/// <summary>
/// Disbursement means getting an Advance.
/// </summary>
[Route("loans")]
[ApiController]
[Authorize]
public class LoanDisbursementsController : ApiController
{
    private readonly ICurrentUserService _currentUserService;

    /// <inheritdoc />
    public LoanDisbursementsController(
        ILogger<LoanDisbursementsController> logger,
        ICurrentUserService currentUserService
    )
        : base(logger)
    {
        _currentUserService = currentUserService;
    }

    /// <summary>
    /// Get Advance taking dialogs.
    /// </summary>
    [HttpGet("disbursements")]
    [ProducesResponseType(typeof(GetAdvanceOfferResponseViewModel), Status200OK)]
    [ProducesResponseType((int)HttpStatusCode.BadRequest)]
    [ProducesResponseType(Status204NoContent)]
    public async Task<IActionResult> GetAdvanceOffer()
    {
        var result = await Mediator.Send(new GetAdvanceOfferQuery());

        return NoContentOrResult(result);
    }

    /// <summary>
    /// Initiate new loan transfer
    /// </summary>
    /// <returns></returns>
    [HttpPost("disbursements")]
    [ProducesResponseType(typeof(CreditSimpleModel), Status200OK)]
    [ProducesResponseType(Status400BadRequest)]
    public async Task<IActionResult> CreateLoanTransfer()
    {
        var command = new CreateLoanTransferCommand();
        command.IpAddress = HttpContext.Connection.RemoteIpAddress.ToString();
        var result = await Mediator.Send(command);

        return Ok(result);
    }

    /// <summary>
    /// Get Advance with delayed boost status
    /// </summary>
    /// <returns></returns>
    [HttpGet("disbursements/boost/delayed-status-dialog")]
    [ProducesResponseType(typeof(DelayedBoostStatusDialog), Status200OK)]
    public async Task<IActionResult> GetDelayedBoostStatus()
    {
        var result = await Mediator.Send(new GetDelayedBoostStatusQuery());

        if (!result.HasContent)
        {
            return NoContent();
        }

        return Ok(result.ViewModel);
    }


    /// <summary>
    /// Get Advance Boost dialog.
    /// </summary>
    [Obsolete("Use 'disbursements/boostV2' instead")]
    [HttpGet("disbursements/boost")]
    [ProducesResponseType(typeof(GetAdvanceBoostResponseViewModel), Status200OK)]
    public async Task<IActionResult> GetAdvanceBoostOffer()
    {
        var result = await Mediator.Send(new GetAdvanceBoostOfferQuery());
        return Ok(result);
    }

    /// <summary>
    /// Endpoint to handle flow that includes delayed boost functionality
    /// </summary>
    /// <returns></returns>
    [HttpGet("disbursements/boostV2")]
    [ProducesResponseType(typeof(GetAdvanceBoostWithDelayResponseViewModel), Status200OK)]
    public async Task<IActionResult> GetAdvanceBoostWithDelayOffer()
    {
        var result = await Mediator.Send(new GetAdvanceBoostWithDelayOfferQuery());
        return Ok(result);
    }

    /// <summary>
    /// Cancel delayed boost
    /// </summary>
    /// <returns></returns>
    [HttpDelete("disbursements/delayed-boost")]
    [ProducesResponseType(typeof(GenericDialogBodyViewModel), Status200OK)]
    public async Task<IActionResult> CancelDelayedBoost()
    {
        var result = await Mediator.Send(new CancelDelayedBoostCommand());
        return Ok(result);
    }


    /// <summary>
    /// Get Advance Boost dialog.
    /// </summary>
    [HttpPost("disbursements/boost")]
    [ProducesResponseType(typeof(GenericSuccessDialog), Status200OK)]
    [ProducesResponseType(typeof(GenericFailDialog), Status406NotAcceptable)]
    public async Task<IActionResult> ActivateAdvanceBoost(ActivateAdvanceBoostRequest request)
    {

        var query = new ActivateAdvanceBoostQuery
        {
            IsDelayedBoost = request.IsFree,
            IsReadyToDisburse = !request.IsFree,
            BoostId = request.BoostId,
            Ip = HttpContext.Connection.RemoteIpAddress!.ToString(),
            MbanqAccessToken = _currentUserService.UserMbanqToken
        };

        var result = await Mediator.Send(query);
        if (result.IsSuccess)
        {
            return Ok(result.Success);
        }

        return NotAcceptable(result.Fail);
    }

    /// <summary>
    /// Get advance unfreeze offer details.
    /// </summary>
    [HttpGet("disbursements/unfreeze")]
    [ProducesResponseType(typeof(AdvanceUnfreezeOfferViewModel), Status200OK)]
    [ProducesResponseType((int)HttpStatusCode.BadRequest)]
    public async Task<IActionResult> GetAdvanceUnfreezeOffer()
    {
        var query = new GetAdvanceUnfreezeOfferQuery();
        var result = await Mediator.Send(query);

        return Ok(result);
    }

    /// <summary>
    /// Purchase and enable extra advance amount.
    /// </summary>
    [HttpPost("disbursements/unfreeze")]
    [ProducesResponseType(typeof(UnfreezeAdvanceExtraAmountResultViewModel), Status200OK)]
    [ProducesResponseType((int)HttpStatusCode.BadRequest)]
    public async Task<IActionResult> UnfreezeExtraAdvanceAmount()
    {
        var query = new UnfreezeAdvanceExtraAmountQuery
        {
            Ip = HttpContext.Connection.RemoteIpAddress!.ToString(),
            MbanqAccessToken = _currentUserService.UserMbanqToken,
        };
        var result = await Mediator.Send(query);

        return Ok(result);
    }

    /// <summary>
    /// Get a dialog that offers to leave a tip. Also lists the offered amounts of tips.
    /// </summary>
    [HttpGet("disbursements/tip")]
    [ProducesResponseType(typeof(GetAfterAdvanceTipDialogViewModel), Status200OK)]
    [ProducesResponseType(Status400BadRequest)]
    public async Task<IActionResult> GetAfterAdvanceTipDialog()
    {
        var query = new GetAfterAdvanceTipDialogQuery
        {
            Ip = HttpContext.Connection.RemoteIpAddress!.ToString(),
            MbanqAccessToken = _currentUserService.UserMbanqToken,
        };
        var result = await Mediator.Send(query);

        return Ok(result);
    }

    /// <summary>
    /// Leave a tip to us.
    /// </summary>
    /// <returns></returns>
    [HttpPost("disbursements/tip")]
    [ProducesResponseType(Status204NoContent)]
    [ProducesResponseType(Status400BadRequest)]
    public async Task<IActionResult> AfterAdvanceTip(AfterAdvanceTipCommand command)
    {
        command.Ip = HttpContext.Connection.RemoteIpAddress!.ToString();
        command.MbanqAccessToken = _currentUserService.UserMbanqToken;
        var result = await Mediator.Send(command);

        return Ok(result);
    }
}
