﻿namespace BNine.API.Controllers.Loans
{
    using System;
    using System.Threading.Tasks;
    using BNine.Application.Aggregates.LoanSpecialOffers.Commands.CreateLoanSpecialOffer;
    using BNine.Application.Aggregates.LoanSpecialOffers.Commands.RemoveLoanSpecialOffer;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.Extensions.Configuration;
    using Microsoft.Extensions.Logging;

    [Route("loans/special-offers")]
    public class LoanSpecialOffersController : ApiController
    {
        private readonly IConfiguration _configuration;
        public LoanSpecialOffersController(ILogger<LoanSpecialOffersController> logger, IConfiguration configuration) : base(logger)
        {
            _configuration = configuration;
        }

        /// <summary>
        /// Create special offers
        /// </summary>
        /// <param name="token"></param>
        /// <param name="command"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> Create([FromQuery] Guid? token, [FromBody] CreateLoanSpecialOfferCommand command)
        {
            var apiKey = _configuration["APIKey"];
            if (!token.HasValue || !token.ToString().Equals(apiKey, StringComparison.OrdinalIgnoreCase))
            {
                return Unauthorized();
            };

            await Mediator.Send(command);

            return NoContent();
        }

        /// <summary>
        /// Delete special offers
        /// </summary>
        /// <param name="token"></param>
        /// <param name="command"></param>
        /// <returns></returns>
        [HttpDelete]
        public async Task<IActionResult> Delete([FromQuery] Guid? token, [FromBody] RemoveLoanSpecialOfferCommand command)
        {
            var apiKey = _configuration["APIKey"];
            if (!token.HasValue || !token.ToString().Equals(apiKey, StringComparison.OrdinalIgnoreCase))
            {
                return Unauthorized();
            };

            await Mediator.Send(command);

            return NoContent();
        }
    }
}
