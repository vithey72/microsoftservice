﻿namespace BNine.API.Controllers;

using Application.Aggregates.AprilTaxSystem.Models;
using Application.Aggregates.AprilTaxSystem.Queries.GetAprilLoginScreen;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

/// <summary>
/// Our partner https://www.getapril.com/ that provides users with simplified
/// tax filling functionality.
/// </summary>
[Authorize]
[Route("april")]
public class AprilTaxSystemController : ApiController
{
    /// <inheritdoc />
    public AprilTaxSystemController(
        ILogger<AprilTaxSystemController> logger
        )
        : base(logger)
    {
    }

    /// <summary>
    /// Generate a screen's viewmodel with a button containing one-time url for user to access web version of April.
    /// </summary>
    /// <returns></returns>
    [HttpGet("login-screen")]
    [ProducesResponseType(typeof(GetAprilLoginScreenViewModel), Status200OK)]
    public async Task<IActionResult> GetAprilLoginScreen(CancellationToken cancellationToken)
    {
        var query = new GetAprilLoginScreenQuery();
        var response = await Mediator.Send(query, cancellationToken);

        return Ok(response);
    }
}
