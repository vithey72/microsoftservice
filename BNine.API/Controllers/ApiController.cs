﻿namespace BNine.API.Controllers
{
    using System.Globalization;
    using BNine.Application.Aggregates.CommonModels;
    using MediatR;
    using Microsoft.AspNetCore.Localization;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.Extensions.DependencyInjection;
    using Microsoft.Extensions.Logging;

#pragma warning disable CS1591

    /// <summary>
    /// Base controller with crucial universal dependencies.
    /// </summary>
    [ApiController]
    public abstract class ApiController
        : ControllerBase
    {
        protected ILogger Logger
        {
            get;
        }

        private IMediator _mediator;

        protected IMediator Mediator => _mediator ??= HttpContext.RequestServices.GetService<IMediator>();

        /// <summary>
        /// Initialize controller and most essential dependencies.
        /// </summary>
        protected ApiController(ILogger logger)
        {
            Logger = logger;
        }

        [NonAction]
        protected CultureInfo GetCultureInfo()
        {
            var cultureFeature = Request.HttpContext.Features.Get<IRequestCultureFeature>();
            return cultureFeature!.RequestCulture.Culture;
        }

        protected CreatedResult Created(object value)
        {
            return Created("", value);
        }

        protected CreatedResult Created()
        {
            return Created("", "");
        }

        protected static ObjectResult NotAcceptable(object body)
        {
            return new ObjectResult(body) { StatusCode = Status406NotAcceptable };
        }

        protected IActionResult NoContentOrResult<T>(OptionViewModelResult<T> result) where T : class
        {
            if (result.HasContent)
            {
                return Ok(result.ViewModel);
            }

            return NoContent();
        }
    }
}
