﻿namespace BNine.API.Controllers.Selfie;

using System.Net;
using Application.Aggregates.Selfie.Commands;
using Application.Aggregates.Selfie.Models;
using Enums;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
[Route("selfie")]
[Authorize]
public class SelfieController : ApiController
{
    /// <summary>
    /// Handles selfie-related requests
    /// </summary>
    /// <param name="logger"></param>
    public SelfieController(ILogger<SelfieController> logger) : base(logger)
    {
    }

    /// <summary>
    /// Saves a user selfie to a blob storage dir based on the type provided
    /// </summary>
    /// <param name="selfieUpload"></param>
    /// <param name="selfieInitiatingFlow"></param>
    /// <returns></returns>
    [HttpPost]
    [ProducesResponseType((int)HttpStatusCode.NoContent)]
    public async Task<IActionResult> UploadSelfie([FromForm] SelfieUpload selfieUpload, SelfieInitiatingFlow selfieInitiatingFlow)
    {

        if (selfieUpload == null)
        {
            return BadRequest("File isn't provided");
        }
        var command = new UploadSelfieWithTypeCommand
        {
            ContentType = selfieUpload.Image.ContentType,
            FileExtension = Path.GetExtension(selfieUpload.Image.FileName),
            SelfieSourceFlow = selfieInitiatingFlow
        };

        using (var memoryStream = new MemoryStream())
        {
            await selfieUpload.Image.CopyToAsync(memoryStream);
            command.FileData = memoryStream.ToArray();
        }

        await Mediator.Send(command);

        return NoContent();
    }


}
