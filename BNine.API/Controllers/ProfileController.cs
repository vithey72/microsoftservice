﻿namespace BNine.API.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Net;
    using System.Threading.Tasks;
    using Application.Aggregates.Cards.Queries.CreateCard;
    using Application.Aggregates.Emails.Commands.Registration.Finished;
    using Application.Aggregates.Plaid.Models;
    using Application.Aggregates.Plaid.Queries.GetProfileBankAccounts;
    using Application.Aggregates.Users.Commands.ResetPassword;
    using Application.Aggregates.Users.Commands.Update.ConfirmSummaryInformation;
    using Application.Aggregates.Users.Commands.Update.UpdateAddress;
    using Application.Aggregates.Users.Commands.Update.UpdateAgreements;
    using Application.Aggregates.Users.Commands.Update.UpdateIdentity;
    using Application.Aggregates.Users.Commands.Update.UpdateNotificationsState;
    using Application.Aggregates.Users.Commands.Update.UpdatePassword;
    using Application.Aggregates.Users.Commands.Update.UpdatePersonalInformation;
    using Application.Aggregates.Users.Commands.Update.UpdateProfilePhoto;
    using Application.Aggregates.Users.Models;
    using Application.Aggregates.Users.Queries.GetProfile;
    using Application.Aggregates.Users.Queries.GetProfilePhoto;
    using Application.Aggregates.Users.Queries.GetUserName;
    using Application.Helpers;
    using Application.Interfaces;
    using BNine.Application.Aggregates.Users.Commands.Update.UpdateChangePhoneConfirmationCode;
    using BNine.Application.Aggregates.Users.Commands.Update.UpdatedEmail;
    using BNine.Application.Aggregates.Users.Commands.Update.UpdatePhoneNumber;
    using BNine.Application.Aggregates.Users.Commands.Update.VerifyPhoneNumber;
    using BNine.Application.Aggregates.Users.Queries.VerifyNewEmailQuery;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.Extensions.Logging;
    using BNine.Application.Aggregates.Users.Queries.GetUserStatus;
    using Infrastructure.Helpers;
    using BNine.Application.Aggregates.Users.Commands.Update.DeleteProfilePhoto;

    /// <summary>
    /// Controller with methods for displaying the profile screen on frontend and updating some of its values.
    /// </summary>
    [Route("profile")]
    [Authorize]
    public class ProfileController
        : ApiController
    {
        private readonly ICurrentUserService _currentUser;
        private readonly IPartnerProviderService _partnerProvider;

        /// <inheritdoc />
        public ProfileController(
            ILogger<ProfileController> logger,
            ICurrentUserService currentUser,
            IPartnerProviderService partnerProvider
            ) : base(logger)
        {
            _currentUser = currentUser;
            _partnerProvider = partnerProvider;
        }

        /// <summary>
        /// Get profile
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [ProducesResponseType((int)HttpStatusCode.Unauthorized)]
        [ProducesResponseType(typeof(Profile), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> Get()
        {
            var result = await Mediator.Send(new GetProfileQuery());

            return Ok(result);
        }

        /// <summary>
        /// Get profile username by phone number
        /// </summary>
        /// <returns></returns>
        [HttpGet("username")]
        [ProducesResponseType((int)HttpStatusCode.Unauthorized)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> GetUserName([FromQuery] GetUserNameQuery command)
        {
            var result = await Mediator.Send(command);

            return Ok(result);
        }

        /// <summary>
        /// Get profile photo
        /// </summary>
        /// <returns></returns>
        [HttpGet("profilePhoto")]
        [ProducesResponseType((int)HttpStatusCode.Unauthorized)]
        [ProducesResponseType(typeof(byte[]), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> GetProfilePhoto()
        {
            var result = await Mediator.Send(new GetProfilePhotoQuery());

            return Ok(result);
        }

        /// <summary>
        /// Remove profile photo
        /// </summary>
        /// <returns></returns>
        [HttpDelete("profilePhoto")]
        [ProducesResponseType((int)HttpStatusCode.Unauthorized)]
        [ProducesResponseType((int)HttpStatusCode.NotFound)]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        public async Task<IActionResult> DeleteProfilePhoto()
        {
            var result = await Mediator.Send(new DeleteProfilePhotoCommand());

            return Ok(result);
        }

        /// <summary>
        /// Set personal information
        /// </summary>
        /// <param name="command"></param>
        /// <returns></returns>
        [HttpPut("personalInformation")]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> UpdatePersonalInformation([FromBody] UpdatePersonalInformationCommand command)
        {
            await Mediator.Send(command);

            return Ok();
        }

        /// <summary>
        /// Set address
        /// </summary>
        /// <param name="command"></param>
        /// <returns></returns>
        [HttpPut("address")]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> UpdateAddress([FromBody] UpdateAddressCommand command)
        {
            await Mediator.Send(command);

            return Ok();
        }

        /// <summary>
        /// Set identity information
        /// </summary>
        /// <param name="command"></param>
        /// <returns></returns>
        [HttpPut("identity")]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> UpdateIdentity([FromBody] UpdateIdentityCommand command)
        {
            command.UserId = _currentUser.UserId.Value;

            await Mediator.Send(command);

            return Ok();
        }

        /// <summary>
        /// Confirm agreements
        /// </summary>
        /// <returns></returns>
        [HttpPut("agreements")]
        [Obsolete]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> UpdateAgreements()
        {
            await Mediator.Send(new UpdateAgreementsCommand
            {
                IpAddress = HttpContext.Connection.RemoteIpAddress.ToString()
            });

            return Ok();
        }

        /// <summary>
        /// Confirm agreements with automatically creating new virtual debit card for new customer
        /// </summary>
        /// <returns></returns>
        [HttpPut("agreements/create-card")]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> UpdateAgreementsWCard()
        {
            await Mediator.Send(new UpdateAgreementsCommand
            {
                IpAddress = HttpContext.Connection.RemoteIpAddress!.ToString()
            });

            var registrationSuccess = await Mediator.Send(new CheckUserIsActiveQuery());
            if (registrationSuccess)
            {
                try
                {
                    if (_partnerProvider.GetPartner().RequiresVirtualCardCreationOnOnboarding())
                    {
                        await Mediator.Send(new CreateCardQuery
                        {
                            Ip = HttpContext.Connection.RemoteIpAddress.ToString(),
                            CardType = Enums.DebitCard.CardType.VirtualDebit
                        });
                    }
                    await Mediator.Send(new RegistrationFinishedEmailCommand { UserId = _currentUser.UserId!.Value });
                }
                catch (Exception ex)
                {
                    Logger.LogError($"An error has occured during create card, message: {ex.Message}", ex);
                }
            }

            return Ok();
        }

        /// <summary>
        /// Confirm summary
        /// </summary>
        /// <returns></returns>
        [Obsolete("/onbarding/summary")]
        [HttpPut("summary")]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> ConfirmSummary([FromBody] ConfirmSummaryCommand command)
        {
            command.IpAddress = HttpContext.Connection.RemoteIpAddress.ToString();

            await Mediator.Send(command);

            return Ok();
        }

        /// <summary>
        /// Upload profile photo
        /// </summary>
        /// <returns></returns>
        [HttpPut("uploadProfilePhoto")]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> UpdateProfilePhoto([FromForm] UpdateProfilePhotoCommand command)
        {
            var response = await Mediator.Send(command);

            return Ok(response);
        }

        /// <summary>
        /// Change password
        /// </summary>
        /// <returns></returns>
        [HttpPut("change-password")]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> UpdatePassword([FromBody] UpdatePasswordCommand command)
        {
            await Mediator.Send(command);

            return Ok();
        }

        /// <summary>
        /// Reset password
        /// </summary>
        /// <returns></returns>
        [AllowAnonymous]
        [HttpPut("resetPassword")]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> ResetPassword([FromBody] ResetPasswordCommand command)
        {
            await Mediator.Send(command);

            return Ok();
        }

        /// <summary>
        /// Get profile bank accounts
        /// </summary>
        /// <returns></returns>
        [Obsolete]
        [HttpGet("profileBankAccounts")]
        [ProducesResponseType((int)HttpStatusCode.Unauthorized)]
        [ProducesResponseType(typeof(List<ProfileBankAccount>), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> GetProfileBankAccounts()
        {
            var result = await Mediator.Send(new GetProfileBankAccountsQuery());

            return Ok(result);
        }

        /// <summary>
        /// Set notifications enabled flag
        /// </summary>
        /// <returns></returns>
        [HttpPut("toggleNotifications")]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> UpdateNotificationsState([FromBody] UpdateNotificationsStateCommand command)
        {
            await Mediator.Send(command);

            return Ok();
        }

        /// <summary>
        /// Update Email
        /// </summary>
        /// <returns></returns>
        [HttpPut("email")]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> UpdateEmail([FromBody] UpdateEmailCommand command)
        {
            await Mediator.Send(command);

            return Ok();
        }

        /// <summary>
        /// Verify Updated Email
        /// </summary>
        /// <returns></returns>
        [HttpGet("email-verification")]
        [ProducesResponseType(typeof(ConfirmUpdateEmailResponseModel), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> VerifyUpdatedEmail([FromQuery] VerifyNewEmailQuery command)
        {
            var result = await Mediator.Send(command);

            return Ok(result);
        }

        /// <summary>
        /// Update Phone Number
        /// </summary>
        /// <returns></returns>
        [HttpPut("phone-number")]
        [ProducesResponseType(typeof(PrimitiveResult<Guid>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> UpdatePhoneNumber([FromBody] UpdatePhoneNumberCommand command)
        {
            var result = await Mediator.Send(command);

            return Ok(new PrimitiveResult<Guid>(result));
        }

        /// <summary>
        /// Resend sms with confirmation code for changing phone number
        /// </summary>
        /// <returns></returns>
        [HttpPut("phone-number-confirmation-code")]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> UpdateChangePhoneConfirmationCode([FromQuery] UpdateChangePhoneConfirmationCodeCommand query)
        {
            await Mediator.Send(query);

            return Ok();
        }

        /// <summary>
        /// Verify Updated Phone Number
        /// </summary>
        /// <returns></returns>
        [HttpPost("phone-number-verification")]
        [ProducesResponseType(typeof(bool), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> VerifyUpdatedPhoneNumber([FromBody] VerifyPhoneNumberCommand command)
        {
            var result = await Mediator.Send(command);

            return Ok(result);
        }
    }
}
