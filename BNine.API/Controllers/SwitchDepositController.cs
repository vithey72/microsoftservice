﻿namespace BNine.API.Controllers;

using Application.Aggregates.Deposit.Models;
using Application.Aggregates.Deposit.Queries;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

/// <summary>
/// Provides data for Switch Deposit screen
/// </summary>
[Route("switch-deposit")]
[Authorize]
[ApiController]
public class SwitchDepositController : ApiController
{
    /// <inheritdoc />
    public SwitchDepositController(ILogger<SwitchDepositController> logger) : base(logger)
    {
    }
    /// <summary>
    /// Returns viewmodel to render the screen from.
    /// </summary>
    [HttpGet]
    [ProducesResponseType(typeof(SwitchDepositScreenViewModel), Status200OK)]
    public async Task<IActionResult> GetScreenViewModel()
    {
        var response = await Mediator.Send(new SwitchDepositScreenQuery());
        return Ok(response);
    }
}
