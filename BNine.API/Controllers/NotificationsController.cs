﻿namespace BNine.API.Controllers
{
    using System.Threading.Tasks;
    using Application.Aggregates.Notifications.Commands.SendNotifications;
    using Application.Aggregates.PushNotifications.Commands.MarkAllNotificationsAsRead;
    using Application.Aggregates.PushNotifications.Commands.MarkNotificationAsRead;
    using BNine.Application.Aggregates.Notifications.Models;
    using BNine.Application.Aggregates.Notifications.Queries.GetNotifications;
    using BNine.Application.Models;
    using BNine.Enums;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.Extensions.Logging;

    /// <summary>
    /// Contains endpoint that returns push-notifications. For some reason contains admin endpoint for sending push-notifications.
    /// </summary>
    [Route("notifications")]
    [Authorize]
    public class NotificationsController : ApiController
    {
        /// <inheritdoc />
        public NotificationsController(ILogger<NotificationsController> logger) : base(logger)
        {
        }

        /// <summary>
        /// Get notifications that user received in our app.
        /// </summary>
        [HttpGet]
        [ProducesResponseType(typeof(PaginatedList<Notification>), Status200OK)]
        public async Task<IActionResult> GetNotifications([FromQuery] NotificationTypeEnum[] filter, int skip = 0, int take = 20)
        {
            var result = await Mediator.Send(new GetNotificationsQuery()
            {
                Skip = skip,
                Take = take,
                Filter = filter.ToList(),
            });

            return Ok(result);
        }

        /// <summary>
        /// Sends notifications to provided users with provided message and parameters.
        /// Requires admin API key to be present in header.
        /// </summary>
        /// <param name="command"></param>
        /// <returns></returns>
        [HttpPost]
        [AllowAnonymous]
        [ProducesResponseType(Status202Accepted)]
        public async Task<IActionResult> SendNotifications([FromBody] SendNotificationsCommand command)
        {
            await Mediator.Send(command);
            return Accepted();
        }

        /// <summary>
        /// Marks notification as read
        /// </summary>
        /// <param name="command"></param>
        /// <returns></returns>
        [Obsolete("Use method below for mass marking as read")]
        [HttpPut("is-read")]
        [ProducesResponseType(Status202Accepted)]
        [ProducesResponseType(Status404NotFound)]
        public async Task<IActionResult> MarkAsRead([FromBody] MarkNotificationAsReadCommand command)
        {
            await Mediator.Send(command);
            return Accepted();
        }

        /// <summary>
        /// Mark all user's notifications as read.
        /// </summary>
        [HttpPut("is-read/all")]
        [ProducesResponseType(Status204NoContent)]
        public async Task<IActionResult> MarkEverythingAsRead()
        {
            await Mediator.Send(new MarkAllNotificationsAsReadCommand());
            return NoContent();
        }
    }
}
