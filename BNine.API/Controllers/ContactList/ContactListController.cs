﻿namespace BNine.API.Controllers.ContactList;

using Application.Aggregates.ContactList;
using Application.Aggregates.ContactList.Models;
using Application.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

/// <summary>
/// Contains methods for managing client's contact list
/// </summary>
[Authorize]
[Route("contact-list")]
public class ContactListController : ApiController
{
    private readonly ILogger<ContactListController> _logger;
    private readonly ICurrentUserService _currentUserService;

    /// <inheritdoc />
    public ContactListController(ILogger<ContactListController> logger, ICurrentUserService currentUserService) : base(logger)
    {
        _logger = logger;
        _currentUserService = currentUserService;
    }

    /// <summary>
    /// Checks for other users in the system that have presence in the client's contact list
    /// </summary>
    [HttpPost]
    [ProducesResponseType(typeof(ContactListMatchesResponse), Status200OK)]
    public async Task<IActionResult> MatchUserContactListForExistingMembers(MatchUserContactListQuery query)
    {
        query.UserId = _currentUserService.UserId!.Value;
        var res = await Mediator.Send(query);
        return Ok(res);
    }
}

