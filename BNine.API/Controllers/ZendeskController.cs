﻿namespace BNine.API.Controllers
{
    using System.Collections.Generic;
    using System.Net;
    using System.Threading.Tasks;
    using BNine.Application.Aggregates.Zendesk.Queries.GetZendeskCategories;
    using BNine.Application.Aggregates.Zendesk.Queries.GetZendeskFAQ;
    using BNine.Application.Aggregates.Zendesk.Queries.GetZendeskFAQSections;
    using BNine.Application.Models.Zendesk;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.Extensions.Logging;

    /// <summary>
    /// Provides everything related to documentation and FAQ to users.
    /// </summary>
    [Route("zendesk")]
    [Authorize]
    public class ZendeskController : ApiController
    {
        /// <inheritdoc />
        public ZendeskController(ILogger<ZendeskController> logger) : base(logger)
        {
        }

        /// <summary>
        /// Get zendesk categories
        /// </summary>
        /// <returns></returns>
        [HttpGet("categories")]
        [ProducesResponseType((int)HttpStatusCode.OK, Type = typeof(IEnumerable<ZendeskCategoryListItem>))]
        public async Task<IActionResult> GetZendeskCategories()
        {
            var result = await Mediator.Send(new GetZendeskCategoriesQuery(GetCultureInfo()));

            return Ok(result);
        }

        /// <summary>
        /// Get zendesk FAQ
        /// </summary>
        [Obsolete(("Use /faq-sections"))]
        [HttpGet("faq")]
        [ProducesResponseType((int)HttpStatusCode.OK, Type = typeof(IEnumerable<ZendeskFAQListItem>))]
        public async Task<IActionResult> GetZendeskFAQ()
        {
            var result = await Mediator.Send(new GetZendeskFAQQuery(GetCultureInfo()));

            return Ok(result);
        }

        /// <summary>
        /// Get zendesk FAQ sections
        /// </summary>
        /// <returns></returns>
        [HttpGet("faq-sections")]
        [ProducesResponseType((int)HttpStatusCode.OK, Type = typeof(IEnumerable<FAQSections>))]
        public async Task<IActionResult> GetZendeskFAQSections()
        {
            var result = await Mediator.Send(new GetZendeskFAQSectionsQuery());

            return Ok(result);
        }
    }
}
