﻿namespace BNine.API.Controllers
{
    using System.Net;
    using System.Threading.Tasks;
    using BNine.Application.Aggregates.CardProduct.Queries.GetMaximumCardLimits;
    using BNine.Application.Aggregates.Cards.Models;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.Extensions.Logging;

    [Route("debit-card-product")]
    [ApiController]
    [Authorize]
    public class DebitCardProductController : ApiController
    {
        public DebitCardProductController(ILogger<DebitCardProductController> logger)
            : base(logger)
        {

        }

        /// <summary>
        /// Returns maximum value of card limits
        /// </summary>
        /// <returns></returns>
        [HttpGet("limits")]
        [ProducesResponseType(typeof(CardLimitsResponseModel), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetCardLimits()
        {
            var result = await Mediator.Send(new GetMaximumCardLimitsQuery());

            return Ok(result);
        }
    }
}
