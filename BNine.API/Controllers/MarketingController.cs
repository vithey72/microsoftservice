﻿namespace BNine.API.Controllers;

using System.Threading.Tasks;
using Application.Aggregates.Banners.Models;
using Application.Aggregates.Banners.Queries;
using Application.Aggregates.Banners.Queries.GetInfoPopupBanner;
using Application.Aggregates.Marketing.Queries.GetPostAdvanceDisbursementPremiumSwitchOfferQuery;
using Application.Aggregates.Marketing.Queries.GetUnemploymentProtectionMarketResearchScreen;
using Application.Aggregates.MarketingThirdParty.Models;
using Application.Aggregates.MarketingThirdParty.Queries.GetAprilMarketingInfoScreen;
using Application.Aggregates.MarketingThirdParty.Queries.GetQuickBooksInfoScreen;
using Application.Exceptions.ApiResponse;
using Application.Interfaces;
using BNine.Application.Aggregates.CommonModels.Dialog;
using BNine.Application.Aggregates.Marketing.Model;
using Constants;
using Enums;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

/// <summary>
/// Controller with endpoints providing data for banners and other app parts
/// in order to promote some of our services to users.
/// </summary>
[Authorize]
[Route("marketing")]
public class MarketingController : ApiController
{
    private readonly ICurrentUserService _currentUserService;

    /// <inheritdoc />
    public MarketingController(ILogger<MarketingController> logger, ICurrentUserService currentUserService)
        : base(logger)
    {
        _currentUserService = currentUserService;
    }

    /// <summary>
    /// Returns a list of banners to be displayed at the top of the Wallet screen.
    /// </summary>
    [Obsolete("Too ugly to be left alive.")]
    [HttpGet("wallet-top-banner")]
    [ProducesResponseType(typeof(GetWalletScreenTopBannerResponse), Status200OK)]
    public async Task<IActionResult> GetWalletScreenTopBanner()
    {
        var response = await Mediator.Send(new GetWalletScreenTopBannerQuery
        {
            Ip = HttpContext.Connection.RemoteIpAddress!.ToString(),
            MbanqAccessToken = _currentUserService.UserMbanqToken,
        });

        return Ok(response);
    }

    /// <summary>
    /// Returns a list popups to be displayed on a corresponding screen to the user.
    /// </summary>
    [AllowAnonymous]
    [HttpGet("info-popup")]
    [ProducesResponseType(typeof(GetInfoPopupBannerResponse), Status200OK)]
    public async Task<IActionResult> GetInfoPopupBanner([FromQuery] AppScreens screen)
    {
        var response = await Mediator.Send(new GetInfoPopupBannerQuery
        {
            Screen = screen,
            Ip = HttpContext.Connection.RemoteIpAddress!.ToString(),
            MbanqAccessToken = _currentUserService?.UserMbanqToken,
        });

        return Ok(response);
    }

    /// <summary>
    /// Returns a third party info screen based on type requested
    /// </summary>
    /// <param name="thirdPartyType"></param>
    /// <returns></returns>
    /// <exception cref="ApiResponseException"></exception>
    [HttpGet("third-party-benefits")]
    [ProducesResponseType(typeof(MarketingThirdPartyInfoScreenViewModel), Status200OK)]
    public async Task<IActionResult> GetThirdPartyInfoScreen([FromQuery] MarketingThirdPartyType thirdPartyType)
    {
        var res = thirdPartyType switch
        {
            MarketingThirdPartyType.April => await Mediator.Send(new GetAprilMarketingInfoScreenQuery()),
            MarketingThirdPartyType.QuickBooks => await Mediator.Send(new GetQuickBooksInfoScreenQuery()),
            _ => throw new ApiResponseException(ApiResponseErrorCodes.WrongMarketingThirdParty,
                "You have requested a third-party type that does not exist")
        };

        return Ok(res);
    }

    [HttpGet("unemployment-protection-research")]
    [ProducesResponseType(typeof(GenericDialogV2), Status200OK)]
    public async Task<IActionResult> GetUnemploymentProtectionMarketResearchScreen()
    {
        var response = await Mediator.Send(new GetUnemploymentProtectionMarketResearchScreenQuery());
        return Ok(response);
    }

    [HttpGet("post-disbursement-premium-switch-offer")]
    [ProducesResponseType(typeof(PostAdvancePremiumSwitchOfferViewModel), Status200OK)]
    [ProducesResponseType(Status400BadRequest)]
    public async Task<IActionResult> GetPostAdvanceDisbursementPremiumSwitchOffer()
    {
        var response = await Mediator.Send(
            new GetPostAdvanceDisbursementPremiumSwitchOfferQuery());
        return Ok(response);
    }
}

