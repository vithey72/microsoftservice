﻿namespace BNine.API.Controllers
{
    using System;
    using System.Net;
    using System.Threading.Tasks;
    using Application.Aggregates.ReferalProgramm.Commands.ActivateBonusCode;
    using Application.Aggregates.ReferalProgramm.Commands.DeleteReceivedBonusBlock;
    using Application.Aggregates.ReferalProgramm.Models;
    using Application.Aggregates.ReferalProgramm.Queries.GetBonusCode;
    using Application.Aggregates.ReferalProgramm.Queries.GetReceivedBonusBlockStatus;
    using Application.Interfaces;
    using BNine.Application.Aggregates.ReferalProgramm.Commands.DeleteActivationEligibilityBlock;
    using BNine.Application.Aggregates.ReferalProgramm.Queries.GetBonusCodeActivationEligibility;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.Extensions.Logging;

    /// <summary>
    /// Controller for the invite-a-friend functionality.
    /// </summary>
    [Route("referal")]
    [Authorize]
    public class ReferralProgramController : ApiController
    {
        private ICurrentUserService CurrentUser
        {
            get;
        }

        /// <inheritdoc />
        public ReferralProgramController(ILogger<ReferralProgramController> logger, ICurrentUserService currentUser)
            : base(logger)
        {
            CurrentUser = currentUser;
        }

        /// <summary>
        /// Get bonus code for sharing
        /// </summary>
        /// <returns></returns>
        [HttpGet("code")]
        [ProducesResponseType(typeof(BonusCodeModel), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetBonusCode()
        {
            var query = new GetBonusCodeQuery();

            var result = await Mediator.Send(query);

            return Ok(result);
        }

        /// <summary>
        /// Activate bonus code
        /// </summary>
        /// <returns></returns>
        [HttpPost("bonus-code-activation")]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> ActivateBonusCode([FromBody] ActivateBonusCodeCommand command)
        {
            command.UserId = CurrentUser.UserId.Value;
            command.Ip = HttpContext.Connection.RemoteIpAddress.ToString();
            command.MbanqAccessToken = CurrentUser.UserMbanqToken;

            try
            {
                await Mediator.Send(command);
                return Ok();
            }
            catch (Exception ex)
            {
                Logger.LogError(ex, "Encountered an exception trying to activate bonus code");
                return BadRequest();
            }
        }

        /// <summary>
        /// Determine whether the user is eligible for bonus code activation (happy case), or if he already activated one in the past,
        /// or if he hasn't deposited minimum required funds into his account.
        /// </summary>
        /// <returns></returns>
        [HttpGet("bonus-code-activation-eligibility")]
        [ProducesResponseType(typeof(BonusCodeActivationEligibilityModel), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetBonusCodeActivationEligibility()
        {
            var query = new GetBonusCodeActivationEligibilityQuery { Ip = HttpContext.Connection.RemoteIpAddress!.ToString() };

            var result = await Mediator.Send(query);

            return Ok(result);
        }

        /// <summary>
        /// Shows or hides eligibility banner for a user
        /// </summary>
        /// <returns></returns>
        [HttpDelete("bonus-code-activation-eligibility")]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> DeleteActivationEligibilityBlock()
        {
            var command = new DeleteActivationEligibilityBlockCommand();

            await Mediator.Send(command);

            return Ok();
        }

        /// <summary>
        /// Get received-bonus block display status
        /// </summary>
        /// <returns></returns>
        [HttpGet("received-bonus-block-status")]
        [ProducesResponseType(typeof(bool), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetReceivedBonusBlockStatus()
        {
            var query = new GetReceivedBonusBlockStatusQuery();

            var result = await Mediator.Send(query);

            return Ok(result);
        }

        /// <summary>
        /// Turn off received-bonus block
        /// </summary>
        /// <returns></returns>
        [HttpDelete("received-bonus-block-status")]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> DeleteReceivedBonusBlock()
        {
            var command = new DeleteReceivedBonusBlockCommand();

            await Mediator.Send(command);

            return Ok();
        }
    }
}
