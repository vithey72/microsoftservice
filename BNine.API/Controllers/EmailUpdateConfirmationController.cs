﻿namespace BNine.API.Controllers;

using BNine.Application.Aggregates.Users.Queries.ConfirmUpdatedEmailQuery;
using System.Net;
using Microsoft.AspNetCore.Mvc;

/// <summary>
/// Controller with method for email update confirmation.
/// </summary>
[Route("email-update-confirmation")]
public class EmailUpdateConfirmationController : ApiController
{
    /// <inheritdoc />
    public EmailUpdateConfirmationController(
        ILogger<EmailUpdateConfirmationController> logger
        ) : base(logger)
    {

    }

    /// <summary>
    /// Verify Updated Email
    /// </summary>
    /// <returns></returns>
    [HttpGet()]
    [ProducesResponseType((int)HttpStatusCode.OK)]
    [ProducesResponseType((int)HttpStatusCode.BadRequest)]
    public async Task<IActionResult> ConfirmUpdatedEmail([FromQuery] ConfirmUpdatedEmailQuery command)
    {
        await Mediator.Send(command);

        return Ok();
    }
}
