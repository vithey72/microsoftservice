﻿namespace BNine.API.Controllers;

using Application.Aggregates.IncomeAndTaxesScreen.Models;
using Application.Aggregates.IncomeAndTaxesScreen.Queries;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

/// <summary>
/// Provides data for the Income and Taxes screen, accessible from Wallet screen.
/// Aka Employment Mode.
/// </summary>
[Authorize]
[Route("income-and-taxes")]
public class IncomeAndTaxesScreenController : ApiController
{
    /// <inheritdoc />
    public IncomeAndTaxesScreenController(ILogger<IncomeAndTaxesScreenController> logger)
        : base(logger)
    {
    }

    /// <summary>
    /// Returns viewmodel to render the screen from.
    /// </summary>
    [HttpGet]
    [ProducesResponseType(typeof(IncomeAndTaxesScreenViewModel), Status200OK)]
    public async Task<IActionResult> GetScreenViewModel()
    {
        var response = await Mediator.Send(new GetIncomeAndTaxesScreenQuery());
        return Ok(response);
    }
}
