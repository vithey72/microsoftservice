﻿namespace BNine.API.Controllers
{
    using System.Net;
    using System.Threading.Tasks;
    using BNine.Application.Aggregates.ATMs.Models;
    using BNine.Application.Aggregates.ATMs.Queries.GetATMsLocations;
    using BNine.Application.Models;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.Extensions.Logging;

    [Authorize]
    [Route("atms")]
    public class ATMsController : ApiController
    {
        public ATMsController(ILogger<ATMsController> logger) : base(logger)
        {
        }

        /// <summary>
        /// Get ATMs locations
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [ProducesResponseType((int)HttpStatusCode.Unauthorized)]
        [ProducesResponseType(typeof(PaginatedList<ATMInfoModel>), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> GetATMs(
            [FromQuery] double longitude,
            [FromQuery] double latitude,
            [FromQuery] long radius,
            [FromQuery] long skip = 0,
            [FromQuery] long take = 20
            )
        {
            var query = new GetATMsLocationsQuery(
                longitude: longitude,
                latitude: latitude,
                radius: radius,
                skip: skip,
                take: take);

            var result = await Mediator.Send(query);

            return Ok(result);
        }
    }
}
