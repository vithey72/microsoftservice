﻿namespace BNine.API.Controllers.ExternalCards
{
    using System.Collections.Generic;
    using System.Net;
    using System.Threading.Tasks;
    using Application.Aggregates.Selfie.Commands;
    using BNine.Application.Aggregates.ExternalCards.Commands.DeleteExternalCard;
    using BNine.Application.Aggregates.ExternalCards.Commands.SaveExternalCardDetails;
    using BNine.Application.Aggregates.ExternalCards.Queries.GetExternalCardFees;
    using BNine.Application.Aggregates.ExternalCards.Queries.GetExternalCards;
    using BNine.Application.Aggregates.ExternalCards.Queries.GetExternalCardsLimits;
    using BNine.Application.Aggregates.ExternalCards.Queries.GetPublicKey;
    using BNine.Application.Aggregates.ExternalCards.Queries.GetTransactionLimits;
    using BNine.Application.Interfaces;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.Extensions.Logging;

    [Route("external-cards")]
    [Authorize]
    public class ExternalCardsController : ApiController
    {
        private ICurrentUserService CurrentUser
        {
            get;
        }

        public ExternalCardsController(ICurrentUserService currentUser, ILogger<ExternalCardsController> logger) : base(logger)
        {
            CurrentUser = currentUser;
        }

        /// <summary>
        /// Get public key
        /// </summary>
        /// <returns></returns>
        [HttpGet("public-key")]
        [ProducesResponseType(typeof(PublicKeyInfo), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> GetPublicKey()
        {
            var result = await Mediator.Send(new GetPublicKeyQuery());
            return Ok(result);
        }

        /// <summary>
        /// Get external card limits
        /// </summary>
        /// <returns></returns>
        [HttpGet("limits")]
        [ProducesResponseType(typeof(ExternalCardsLimits), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> GetExternalCardLimits()
        {
            var result = await Mediator.Send(new GetExternalCardLimitsQuery());
            return Ok(result);
        }

        /// <summary>
        /// Get external card fees
        /// </summary>
        /// <returns></returns>
        [HttpGet("fees")]
        [ProducesResponseType(typeof(ExternalCardFee), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> GetExternalCardFees()
        {
            var result = await Mediator.Send(new GetExternalCardFeeQuery());
            return Ok(result);
        }

        /// <summary>
        /// Get transaction limits
        /// </summary>
        /// <returns></returns>
        [HttpGet("transaction-limits")]
        [ProducesResponseType(typeof(TransactionLimitsInfo), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> GetTransactionLimits()
        {
            var result = await Mediator.Send(new GetTransactionLimitsQuery());
            return Ok(result);
        }

        /// <summary>
        /// Get external cards
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [ProducesResponseType(typeof(IEnumerable<ExternalCardDetails>), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> Get()
        {
            var result = await Mediator.Send(new GetExternalCardsQuery(CurrentUser.UserMbanqToken, HttpContext.Connection.RemoteIpAddress.ToString()));
            return Ok(result);
        }

        /// <summary>
        /// Save card name
        /// </summary>
        /// <returns></returns>
        [HttpPut("{id}")]
        [ProducesResponseType((int)HttpStatusCode.NoContent)]
        public async Task<IActionResult> Put([FromRoute] long id, [FromBody] SaveExternalCardDetailsCommand command)
        {
            command.Id = id;
            await Mediator.Send(command);
            return NoContent();
        }

        /// <summary>
        /// Delete external card
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete("{id}")]
        [ProducesResponseType((int)HttpStatusCode.NoContent)]
        public async Task<IActionResult> Delete([FromRoute] long id)
        {
            await Mediator.Send(new DeleteExternalCardCommand(id, CurrentUser.UserMbanqToken, HttpContext.Connection.RemoteIpAddress.ToString()));
            return NoContent();
        }

        /// <summary>
        /// Save user selfie
        /// </summary>
        /// <returns></returns>
        [Obsolete("Use /selfie instead of /external-cards/selfie")]
        [HttpPost("selfie")]
        [ProducesResponseType((int)HttpStatusCode.NoContent)]
        public async Task<IActionResult> UploadSelfie()
        {
            var files = Request.Form.Files;

            if (files.Count == 0)
            {
                return BadRequest("File isn't provided");
            }

            var selfieFile = files[0];

            var command = new UploadSelfieCommand()
            {
                ContentType = selfieFile.ContentType,
                FileExtension = Path.GetExtension(selfieFile.FileName)
            };

            using (var memoryStream = new MemoryStream())
            {
                await selfieFile.CopyToAsync(memoryStream);
                command.FileData = memoryStream.ToArray();
            }

            await Mediator.Send(command);

            return NoContent();
        }

    }
}
