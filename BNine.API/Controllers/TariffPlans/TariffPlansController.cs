﻿namespace BNine.API.Controllers.TariffPlans
{
    using Application.Aggregates.CommonModels.Dialog;
    using Application.Aggregates.TariffPlans.Queries.GetTariffsTimeline;
    using Application.Aggregates.TariffPlans.Queries.PurchasePlanOnOnboarding;
    using Application.Aggregates.TariffPlans.Queries.SwitchTariff;
    using Application.Interfaces;
    using BNine.Application.Aggregates.TariffPlans.Models;
    using BNine.Application.Aggregates.TariffPlans.Queries;
    using BNine.Application.Aggregates.TariffPlans.Queries.GetConfirmationDetails;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.Extensions.Logging;

    /// <summary>
    /// Controller that allows clients to see available tariff options and control their current tariff.
    /// </summary>
    [Route("tariff-plans")]
    [Authorize]
    public class TariffPlansController : ApiController
    {
        private readonly ICurrentUserService _currentUserService;

        /// <inheritdoc />
        public TariffPlansController(ILogger<TariffPlansController> logger, ICurrentUserService currentUserService)
            : base(logger)
        {
            _currentUserService = currentUserService;
        }

        /// <summary>
        /// Returns viewmodel for displaying the client's current tariff and their other available options.
        /// </summary>
        [HttpGet]
        [ProducesResponseType(Status401Unauthorized)]
        [ProducesResponseType(typeof(TariffPlansListModel), Status200OK)]
        public async Task<IActionResult> Get()
        {
            var result = await Mediator.Send(new GetTariffPlansInfoQuery());

            return Ok(result);
        }

        /// <summary>
        /// Returns viewmodel for displaying a dialogue that prompts tariff change.
        /// </summary>
        [HttpGet("confirmation-details")]
        [ProducesResponseType(Status401Unauthorized)]
        [ProducesResponseType(typeof(ConfirmationDetailsModel), Status200OK)]
        public async Task<IActionResult> GetConfirmationDetails(Guid tariffPlanId)
        {
            var result = await Mediator.Send(new GetConfirmationDetailsQuery
            {
                TariffPlanId = tariffPlanId
            });

            return Ok(result);
        }

        /// <summary>
        /// Swap current tariff for a different one.
        /// </summary>
        [HttpPut("switch-tariff")]
        [ProducesResponseType(Status401Unauthorized)]
        [ProducesResponseType(typeof(TariffPlansTimelineViewModel), Status200OK)]
        [ProducesResponseType(Status400BadRequest)]
        public async Task<IActionResult> SwitchTariff(SwitchTariffQuery query)
        {
            var response = await Mediator.Send(query);

            return Ok(response);
        }

        /// <summary>
        /// See details of current and upcoming plans.
        /// </summary>
        [HttpGet("details")]
        [ProducesResponseType(typeof(TariffPlansTimelineViewModel), Status200OK)]
        public async Task<IActionResult> GetPlanDetails()
        {
            var query = new GetTariffsTimelineQuery { CalledAfterPurchase = false };
            var response = await Mediator.Send(query);

            return Ok(response);
        }

        /// <summary>
        /// Add money to account from external card and purchase a subscription.
        /// </summary>
        [HttpPut("buy-on-onboarding")]
        [ProducesResponseType(typeof(GenericSuccessDialog), Status200OK)]
        [ProducesResponseType(typeof(GenericFailDialog), Status406NotAcceptable)]
        public async Task<IActionResult> PurchasePlanOnOnboarding(PurchasePlanOnOnboardingCommand query)
        {
            query.MbanqAccessToken = _currentUserService.UserMbanqToken;
            query.Ip = HttpContext.Connection.RemoteIpAddress!.ToString();

            var response = await Mediator.Send(query);
            return response.IsSuccess
                ? Ok(response.Success)
                : NotAcceptable(response.Fail);
        }
    }
}
