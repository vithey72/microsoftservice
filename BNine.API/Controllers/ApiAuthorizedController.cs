﻿namespace BNine.API.Controllers
{
    using System.Globalization;
    using Application.Abstractions;
    using Application.Interfaces;
    using MediatR;
    using Microsoft.AspNetCore.Localization;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.Extensions.DependencyInjection;
    using Microsoft.Extensions.Logging;

#pragma warning disable CS1591

    /// <summary>
    /// Base controller with crucial universal dependencies.
    /// </summary>
    [ApiController]
    public abstract class ApiAuthorizedController
        : ControllerBase
    {

        protected ILogger Logger
        {
            get;
        }

        private IMediator _mediator;

        protected IMediator Mediator => _mediator ??= HttpContext.RequestServices.GetService<IMediator>();
        protected readonly ICurrentUserService CurrentUserService;

        protected ApiAuthorizedController(ILogger logger, ICurrentUserService currentUserService)
            : base()
        {
            CurrentUserService = currentUserService;
            Logger = logger;
        }

        [NonAction]
        protected CultureInfo GetCultureInfo()
        {
            var cultureFeature = Request.HttpContext.Features.Get<IRequestCultureFeature>();
            return cultureFeature!.RequestCulture.Culture;
        }

        protected CreatedResult Created(object value)
        {
            return Created("", value);
        }

        protected CreatedResult Created()
        {
            return Created("", "");
        }

        protected void AddMbanqAuthorizationDetails(MBanqSelfServiceUserRequest request)
        {
            request.Ip = HttpContext.Connection.RemoteIpAddress!.ToString();
            request.MbanqAccessToken = CurrentUserService.UserMbanqToken;
        }
    }
}
