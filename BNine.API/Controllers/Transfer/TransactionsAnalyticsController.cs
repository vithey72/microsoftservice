﻿namespace BNine.API.Controllers.Transfer;

using Application.Aggregates.CheckingAccounts.Models.TransactionAnalytics;
using Application.Aggregates.CheckingAccounts.Queries.TransactionAnalytics;
using Application.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

[Authorize]
[Route("transaction-analytics")]
public class TransactionsAnalyticsController : ApiController
{
    private readonly ICurrentUserService _currentUser;

    public TransactionsAnalyticsController(
        ILogger<TransactionsAnalyticsController> logger,
        ICurrentUserService currentUser) : base(logger)
    {
        _currentUser = currentUser;
    }

    /// <summary>
    /// Get Transaction Categories list or list of end transactions
    /// </summary>
    [HttpPost]
    [ProducesResponseType(typeof(TransactionAnalyticsViewModel), Status200OK)]
    [ProducesResponseType(Status400BadRequest)]
    public async Task<IActionResult> GetTransactionAnalytics([FromBody] GetTransactionAnalyticsQuery query)
    {
        query.Ip = HttpContext.Connection.RemoteIpAddress!.ToString();
        query.MbanqAccessToken = _currentUser.UserMbanqToken;

        var result = await Mediator.Send(query);
        return Ok(result);
    }
}
