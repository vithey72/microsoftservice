﻿namespace BNine.API.Controllers.Transfer;

using System.Threading.Tasks;
using Application.Aggregates.CheckingAccounts.Queries.GetFilteredTransfers;
using Application.Aggregates.CheckingAccounts.Queries.GetTransferDetails;
using Application.Aggregates.Transfers.Commands.CreateACHTransfer;
using Application.Aggregates.Transfers.Commands.CreateInternalTransfer;
using Application.Aggregates.Transfers.Commands.CreatePushToExternalCardTransfer;
using Application.Aggregates.Transfers.Commands.ReplenishFromExternalCard;
using Application.Aggregates.Transfers.Commands.ReplenishFromPlaidLinkedAccountCommand;
using Application.Aggregates.Transfers.Models;
using Application.Aggregates.Transfers.Queries.GetPullFromCardTransferDetailedAmount;
using Application.Aggregates.Transfers.Queries.GetPushToCardTransferDetailedAmount;
using Application.Interfaces;
using Application.Aggregates.CheckingAccounts.Models;
using Application.Aggregates.CommonModels.Dialog;
using Application.Aggregates.Transfers.Commands.CreateRewardsTransfer;
using Application.Models;
using Enums.Transfers.Controller;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using DebitDetailedAmount =
    Application.Aggregates.Transfers.Queries.GetPullFromCardTransferDetailedAmount.DetailedAmount;
using CreditDetailedAmount =
    Application.Aggregates.Transfers.Queries.GetPushToCardTransferDetailedAmount.DetailedAmount;
using Controllers;

/// <summary>
/// Controller with methods for performing push and pull transfers between user account
/// and external cards and accounts. Provides transfer history of user.
/// </summary>
[Authorize]
[Route("transfers")]
public class TransfersController : ApiController
{
    private readonly ICurrentUserService _currentUser;
    private readonly IConfiguration _configuration;
    private readonly ICurrentUserIpAddressService _currentUserIpAddressService;

    /// <inheritdoc />
    public TransfersController(ILogger<TransfersController> logger, ICurrentUserService currentUser,
        IConfiguration configuration,
        ICurrentUserIpAddressService currentUserIpAddressService) : base(logger)
    {
        ArgumentNullException.ThrowIfNull(currentUser);
        _currentUser = currentUser;
        _configuration = configuration;
        _currentUserIpAddressService = currentUserIpAddressService;
    }

    /// <summary>
    /// Create internal transfer
    /// </summary>
    [HttpPost("credit/internal")]
    [ProducesResponseType(typeof(int), Status201Created)]
    [ProducesResponseType(Status400BadRequest)]
    public async Task<IActionResult> CreateInternalTransfer(
        [FromBody] CreateInternalTransferCommand command)
    {
        command.Ip = HttpContext.Connection.RemoteIpAddress!.ToString();
        command.MbanqAccessToken = _currentUser.UserMbanqToken;

        var result = await Mediator.Send(command);
        return Created(result);
    }

    /// <summary>
    /// Create transfer from rewards to main account
    /// </summary>
    /// <param name="command"></param>
    /// <returns></returns>
    [HttpPost("credit/rewards")]
    [ProducesResponseType(typeof(GenericSuccessDialog), Status200OK)]
    [ProducesResponseType(typeof(GenericFailDialog), Status406NotAcceptable)]
    public async Task<IActionResult> CreateRewardsTransfer(
        [FromBody] CreateRewardsTransferCommand command)
    {
        command.UserId = _currentUser.UserId!.Value;
        command.IpAddress = _currentUserIpAddressService.CurrentIp;
        command.MBanqToken = _currentUser.UserMbanqToken;
        var result = await Mediator.Send(command);
        return result.IsSuccess ?
            Ok(result.Success) : NotAcceptable(result.Fail);

    }

    /// <summary>
    /// Create credit ACH transfer
    /// </summary>
    /// <param name="command"></param>
    /// <returns></returns>
    [HttpPost("credit/secure-ach")]
    [ProducesResponseType(typeof(GenericSuccessDialog), Status200OK)]
    [ProducesResponseType(typeof(GenericFailDialog), Status406NotAcceptable)]
    [ProducesResponseType(Status400BadRequest)]
    public async Task<IActionResult> CreateAchTransfer(
        [FromBody] CreateACHTransferCommand command)
    {
        command.Ip = HttpContext.Connection.RemoteIpAddress!.ToString();
        command.MbanqAccessToken = _currentUser.UserMbanqToken;

        var ipAddress = HttpContext.Connection.RemoteIpAddress?.ToString();

        if (command.DeviceDetails is not null)
        {
            command.DeviceDetails.ExternalIpAddress = ipAddress;
        }

        var result = await Mediator.Send(command);

        if (result.IsSuccess)
        {
            return Ok(result.Success);
        }

        return NotAcceptable(result.Fail);
    }

    /// <summary>
    /// Create ACH Debit transfer
    /// </summary>
    /// <param name="command"></param>
    /// <returns></returns>
    [HttpPost("debit/ach")]
    [ProducesResponseType(typeof(int), Status201Created)]
    [ProducesResponseType(Status400BadRequest)]
    public async Task<IActionResult> CreateACHDebitTransfer(
        [FromBody] ReplenishFromPlaidLinkedAccountCommand command)
    {
        command.Ip = HttpContext.Connection.RemoteIpAddress!.ToString();
        command.MbanqAccessToken = _currentUser.UserMbanqToken;

        var result = await Mediator.Send(command);
        return Created(result);
    }

    /// <summary>
    /// Create external card Debit transfer
    /// </summary>
    /// <param name="command"></param>
    /// <returns></returns>
    [HttpPost("debit/external-card")]
    [ProducesResponseType(typeof(ExternalCardTransferInfo), Status201Created)]
    [ProducesResponseType(Status400BadRequest)]
    public async Task<IActionResult> CreateExternalCardDebitTransfer(
        [FromBody] ReplenishFromExternalCardCommand command)
    {
        command.Ip = HttpContext.Connection.RemoteIpAddress!.ToString();
        command.MbanqAccessToken = _currentUser.UserMbanqToken;

        var result = await Mediator.Send(command);
        return Created(result);
    }

    /// <summary>
    /// Get pull external card transfer amount with fees
    /// </summary>
    /// <param name="amount"></param>
    /// <returns></returns>
    [HttpGet("debit/external-card/detailed-amount")]
    [ProducesResponseType(typeof(DebitDetailedAmount), Status200OK)]
    public async Task<IActionResult> GetExternalCardDebitTransferDetailedAmount(
        [FromQuery] decimal amount)
    {
        var result = await Mediator.Send(new GetPullTransferDetailedAmountQuery(amount));
        return Ok(result);
    }

    /// <summary>
    /// Create external card Credit transfer
    /// </summary>
    /// <param name="command"></param>
    /// <returns></returns>
    [HttpPost("credit/external-card")]
    [ProducesResponseType(typeof(PushToExternalCardResponse), Status201Created)]
    [ProducesResponseType(Status400BadRequest)]
    public async Task<IActionResult> CreateExternalCardCreditTransfer(
        [FromBody] CreatePushToExternalCardTransferCommand command)
    {
        command.Ip = HttpContext.Connection.RemoteIpAddress!.ToString();
        command.MbanqAccessToken = _currentUser.UserMbanqToken;

        var result = await Mediator.Send(command);
        return Created(result);
    }

    /// <summary>
    /// Get push external card transfer amount with fees
    /// </summary>
    [HttpGet("credit/external-card/detailed-amount")]
    [ProducesResponseType(typeof(CreditDetailedAmount), Status200OK)]
    public async Task<IActionResult> GetExternalCardCreditTransferDetailedAmount(
        [FromQuery] decimal amount)
    {
        var result = await Mediator.Send(new GetPushToCardTransferDetailedAmountQuery(amount));
        return Ok(result);
    }

    /// <summary>
    /// Get a filtered concatenated list of completed, pending and rejected transactions.
    /// </summary>
    [HttpPost("filter")]
    [ProducesResponseType(typeof(PaginatedFilteredList<BankTransferViewModel, GetFilteredTransfersFilter>),
        Status200OK)]
    public async Task<IActionResult> GetFilteredTransfers([FromBody] GetFilteredTransfersQuery query)
    {
        query.Ip = HttpContext.Connection.RemoteIpAddress!.ToString();
        query.MbanqAccessToken = _currentUser.UserMbanqToken;
        var response = await Mediator.Send(query);
        return Ok(response);
    }

    /// <summary>
    /// Get details for a single transaction received from <see cref="GetFilteredTransfers"/>.
    /// </summary>
    [HttpGet("{type}/{id:int}/details")]
    [ProducesResponseType(typeof(BankTransferDetailsViewModel), Status200OK)]
    public async Task<IActionResult> GetTransferDetails([FromRoute] BankTransferCompletionState type,
        [FromRoute] int id)
    {
        var query = new GetTransferDetailsQuery
        {
            TransferId = id,
            Type = type,
            MbanqAccessToken = _currentUser.UserMbanqToken,
            Ip = HttpContext.Connection.RemoteIpAddress!.ToString()
        };
        var response = await Mediator.Send(query);
        return Ok(response);
    }
}
