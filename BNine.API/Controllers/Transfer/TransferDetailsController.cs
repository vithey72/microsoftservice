﻿namespace BNine.API.Controllers.Transfer;

using Application.Aggregates.CheckingAccounts.Queries.GetRepeatTransactionProperties;
using Application.Aggregates.CommonModels.Dialog;
using Application.Aggregates.Transfers.Commands.MarkDepositAsPotentialIncome;
using Application.Aggregates.Transfers.Queries.CheckTransferIsPotentialIncome;
using Application.Aggregates.Transfers.Queries.GetTransferDetailsPdf;
using Application.Interfaces;
using Application.Models.MBanq.Transfers;
using Enums.Transfers.Controller;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

/// <summary>
/// Encapsulates methods that are called from Transfer Details screen.
/// Except Favorite button, which has own controller.
/// </summary>
[Authorize]
public class TransferDetailsController : ApiAuthorizedController
{
    /// <inheritdoc />
    public TransferDetailsController(
        ILogger<TransferDetailsController> logger,
        ICurrentUserService currentUserService
        ) : base(logger, currentUserService)
    {
    }


    /// <summary>
    /// Get fields for triggering repeat of specified transaction. Response content depends on type of transaction.
    /// </summary>
    [HttpGet("/transfers/{id:int}/repeat")]
    [ProducesResponseType(typeof(GetRepeatTransactionPropertiesResponse), Status200OK)]
    public async Task<IActionResult> GetRepeatTransactionProperties([FromRoute] int id)
    {
        var query = new GetRepeatTransactionPropertiesQuery
        {
            TransactionId = id,
        };
        AddMbanqAuthorizationDetails(query);
        var response = await Mediator.Send(query);
        return Ok(response);
    }

    /// <summary>
    /// Generate a PDF file based on this transfer's details.
    /// </summary>
    [HttpGet("/transfers/{type}/{transferId:int}/share/pdf")]
    [HttpGet("/transfers/share/{type}/{transferId:int}/pdf")]
    [ProducesResponseType(Status200OK)]
    public async Task<IActionResult> GetTransferDetailsPdf([FromRoute] BankTransferCompletionState type, [FromRoute] int transferId,
        CancellationToken cancellationToken)
    {
        var query = new GetTransferDetailsPdfQuery
        {
            TransferId = transferId,
            TransferType = type,
        };
        AddMbanqAuthorizationDetails(query);
        var result = await Mediator.Send(query, cancellationToken);
        return File(result, "application/pdf");
    }

    /// <summary>
    /// Check if this ACH transfer is already marked as potential income.
    /// </summary>
    [HttpGet("/transfers/{transferId:int}/is-payroll")]
    [ProducesResponseType(typeof(GenericSuccessFlagResponse), Status200OK)]
    [ProducesResponseType(Status400BadRequest)]
    public async Task<IActionResult> CheckTransferIsPotentialIncome([FromRoute] int transferId)
    {
        var command = new CheckTransferIsPotentialIncomeQuery
        {
            TransferId = transferId,
        };
        AddMbanqAuthorizationDetails(command);
        var response = await Mediator.Send(command);
        return Ok(response);
    }

    /// <summary>
    /// Mark this ACH transfer as potential income.
    /// </summary>
    [HttpPost("/transfers/{transferId:int}/is-payroll")]
    [ProducesResponseType(typeof(GenericSuccessFlagResponse), Status200OK)]
    [ProducesResponseType(Status400BadRequest)]
    public async Task<IActionResult> MarkDepositAsPotentialIncome([FromRoute] int transferId)
    {
        var command = new MarkDepositAsPotentialIncomeCommand
        {
            TransferId = transferId,
        };
        AddMbanqAuthorizationDetails(command);
        var response = await Mediator.Send(command);
        return Ok(response);
    }
}
