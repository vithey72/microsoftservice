﻿namespace BNine.API.Controllers.Transfer
{
    using Application.Aggregates.FavoriteTransfers.Command.CreateFavoriteTransferCommand;
    using Application.Aggregates.FavoriteTransfers.Command.DeleteFavoriteTransferCommand;
    using Application.Aggregates.FavoriteTransfers.Command.UpdateFavoriteTransferCommand;
    using Application.Aggregates.FavoriteTransfers.Model;
    using Application.Aggregates.FavoriteTransfers.Queries.GetAllFavoriteTransfersQuery;
    using Application.Interfaces;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.Extensions.Logging;

    /// <summary>
    /// Controller to perform CRUD operations on entities of favorited transfers that may be repeated by user in the future.
    /// </summary>
    [Authorize]
    [Route("favorite-transfers")]
    public class FavoriteTransfersController : ApiAuthorizedController
    {
        /// <inheritdoc />
        public FavoriteTransfersController(ILogger<FavoriteTransfersController> logger, ICurrentUserService currentUser)
            : base(logger, currentUser)
        {
        }

        /// <summary>
        /// Add transfer to list of favorites for user.
        /// </summary>
        [HttpPost]
        [ProducesResponseType(typeof(Guid), Status201Created)]
        [ProducesResponseType(Status400BadRequest)]
        public async Task<IActionResult> CreateFavoriteTransfer([FromBody] CreateFavoriteTransferCommand command)
        {
            AddMbanqAuthorizationDetails(command);
            var result = await Mediator.Send(command);
            return Created(result);
        }

        /// <summary>
        /// Get user's favorited transfers.
        /// </summary>
        [HttpGet("all")]
        [ProducesResponseType(typeof(FavoriteTransfersListViewModel), Status200OK)]
        public async Task<IActionResult> GetFavoriteTransfers([FromQuery] int take = 999)
        {
            var query = new GetAllFavoriteTransfersQuery
            {
                Take = take,
                IncludeSuggestions = false,
            };
            AddMbanqAuthorizationDetails(query);
            var result = await Mediator.Send(query);
            return Ok(result);
        }

        /// <summary>
        /// Update favorite transfer (i.e. rename).
        /// </summary>
        [HttpPut("{id:guid}")]
        [ProducesResponseType(Status204NoContent)]
        [ProducesResponseType(Status400BadRequest)]
        public async Task<IActionResult> UpdateFavoriteTransfer([FromRoute] Guid id, [FromBody] UpdateFavoriteTransferCommand command)
        {
            command.Id = id;
            AddMbanqAuthorizationDetails(command);

            await Mediator.Send(command);
            return Ok();
        }

        /// <summary>
        /// Delete transfer from list of favorites.
        /// </summary>
        [HttpDelete("{id:guid}")]
        [ProducesResponseType(Status204NoContent)]
        [ProducesResponseType(Status400BadRequest)]
        public async Task<IActionResult> DeleteFavoriteTransfer([FromRoute] Guid id)
        {
            await Mediator.Send(new DeleteFavoriteTransferCommand { Id = id });
            return Ok();
        }
    }
}
