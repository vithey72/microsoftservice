﻿namespace BNine.API.Controllers.Transfer;

using Application.Aggregates.TransfersCommonInfo.Models;
using Application.Aggregates.TransfersCommonInfo.Queries.Deposit;
using Application.Aggregates.TransfersCommonInfo.Queries.SendMoney;
using Application.Exceptions.ApiResponse;
using Constants;
using Enums.Transfers.Controller;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

/// <summary>
/// Controller to retrieve data for transfer info screens
/// </summary>
[Authorize]
[Route("transfers/common-info")]

public class TransferCommonInfoController : ApiController
{
    /// <inheritdoc />
    public TransferCommonInfoController(
        ILogger<TransferCommonInfoController> logger) :
        base(logger)
    {
    }

    /// <summary>
    /// Provides a viewmodel based on the chosen type
    /// </summary>
    /// <param name="transferCommonDetailsType"></param>
    /// <returns></returns>
    /// <exception cref="ApiResponseException"></exception>
    [HttpGet]
    [ProducesResponseType(typeof(TransfersCommonInfoViewModel), Status200OK)]
    public async Task<IActionResult> GetDepositCommonInfo(TransferCommonDetailsType transferCommonDetailsType)
    {
        var res = transferCommonDetailsType switch
        {
            TransferCommonDetailsType.Deposit => await Mediator.Send(new TransferDepositCommonInfoQuery()),
            TransferCommonDetailsType.SendMoney => await Mediator.Send(new TransferSendMoneyCommonInfoQuery()),
            _ => throw new ApiResponseException(ApiResponseErrorCodes.WrongTransferCommonInfoScreenType,
                "You have requested a transfer-common-info screen type that does not exist")
        };

        return Ok(res);
    }
}
