﻿namespace BNine.API.Controllers.Transfer;

using Application.Aggregates.Transfers.Commands.SendTransferSecurityOTP;
using Application.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

[Authorize]
[Route("transfers-security")]
public class TransferSecurityController : ApiController
{
    private readonly ICurrentUserService _currentUser;

    /// <inheritdoc />
    public TransferSecurityController(ILogger<TransfersController> logger, ICurrentUserService currentUser) :
        base(logger)
    {
        ArgumentNullException.ThrowIfNull(currentUser);
        _currentUser = currentUser;
    }

    /// <summary>
    /// Send an OTP code to user using Push notification or SMS
    /// </summary>
    [HttpPost("otp/push")]
    [ProducesResponseType(typeof(int), Status201Created)]
    [ProducesResponseType(Status400BadRequest)]
    public async Task<IActionResult> SendTransferSecurityOtpPushCode(
        [FromBody] SendTransferSecurityOtpPushCommand pushCommand)
    {
        var ipAddress = HttpContext.Connection.RemoteIpAddress?.ToString();
        pushCommand.ExternalIpAddress = ipAddress;

        var result = await Mediator.Send(pushCommand);
        return Created(result);
    }

    /// <summary>
    /// Send an OTP code to user using SMS
    /// </summary>
    [HttpPost("otp/sms")]
    [ProducesResponseType(typeof(int), Status201Created)]
    [ProducesResponseType(Status400BadRequest)]
    public async Task<IActionResult> SendTransferSecurityOtpSmsCode(
        [FromBody] SendTransferSecurityOtpSmsCommand pushCommand)
    {
        var ipAddress = HttpContext.Connection.RemoteIpAddress?.ToString();
        pushCommand.ExternalIpAddress = ipAddress;

        var result = await Mediator.Send(pushCommand);
        return Created(result);
    }
}
