﻿namespace BNine.API.Controllers.Transfer;

using System.Collections.Generic;
using System.Threading.Tasks;
using BNine.Application.Aggregates.CheckingAccounts.Models;
using BNine.Application.Aggregates.CheckingAccounts.Queries.GetPendingTransactions;
using BNine.Application.Aggregates.CheckingAccounts.Queries.GetTransactions;
using BNine.Application.Interfaces;
using BNine.Application.Models;
using BNine.Application.Models.MBanq;
using Enums.Transfers;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

[Obsolete]
[Authorize]
public class LegacyTransfersController : ApiController
{
    private readonly ICurrentUserService _currentUser;

    public LegacyTransfersController(ILogger<LegacyTransfersController> logger, ICurrentUserService currentUser) : base(logger)
    {
        ArgumentNullException.ThrowIfNull(currentUser);
        _currentUser = currentUser;
    }

    /// <summary>
    /// Get client transfers
    /// </summary>
    /// <returns></returns>
    [HttpGet("transfers")]
    [ProducesResponseType(typeof(PaginatedList<SavingsTransaction>), Status200OK)]
    public async Task<IActionResult> GetTransactions(
        [FromQuery] int skip = 0,
        [FromQuery] int take = 20,
        [FromQuery] IEnumerable<TransactionType> transactionTypes = null)
    {
        var result = await Mediator.Send(new GetTransactionsQuery()
        {
            Skip = skip,
            Take = take,
            TransactionTypes = transactionTypes,
            IpAddress = HttpContext.Connection.RemoteIpAddress.ToString(),
            MbanqAccessToken = _currentUser.UserMbanqToken
        });
        return Ok(result);
    }

    /// <summary>
    /// Get client pending transfers
    /// </summary>
    /// <returns></returns>
    [HttpGet("transfers/pending")]
    [ProducesResponseType(typeof(PaginatedList<PendingTransactionItem>), Status200OK)]
    public async Task<IActionResult> GetPendingTransactions(
        [FromQuery] int skip = 0,
        [FromQuery] int take = 20)
    {
        var result = await Mediator.Send(new GetPendingTransactionsQuery()
        {
            Skip = skip,
            Take = take,
            IpAddress = HttpContext.Connection.RemoteIpAddress.ToString(),
            MbanqAccessToken = _currentUser.UserMbanqToken
        });
        return Ok(result);
    }
}
