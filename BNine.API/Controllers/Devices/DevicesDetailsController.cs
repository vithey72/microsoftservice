﻿namespace BNine.API.Controllers.Devices
{
    using System.Net;
    using System.Threading.Tasks;
    using Application.Aggregates.Devices.Commands.AddDetails;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.Extensions.Logging;

    [Authorize]
    [ApiController]
    [Route("devices/details")]
    public class DevicesDetailsController : ApiController
    {
        public DevicesDetailsController(ILogger<DevicesDetailsController> logger) : base(logger)
        {
        }

        /// <summary>
        /// Adds device details
        /// </summary>
        /// <param name="command"></param>
        /// <returns></returns>
        [HttpPost]
        [ProducesResponseType((int)HttpStatusCode.Created)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> AddDeviceDetails([FromBody] AddDeviceDetailsCommand command)
        {
            var ipAddress = HttpContext.Connection.RemoteIpAddress?.ToString();
            command.ExternalIpAddress = ipAddress;
            Logger.LogInformation($"IP address from deviceDetails: {ipAddress}");

            await Mediator.Send(command);
            return Created();
        }
    }
}
