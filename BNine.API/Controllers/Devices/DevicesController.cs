﻿namespace BNine.API.Controllers.Devices
{
    using System;
    using System.Net;
    using System.Threading.Tasks;
    using Application.Aggregates.Devices.Commands.RegisterDevice;
    using Application.Aggregates.Devices.Commands.UnregisterDevice;
    using Enums;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.Extensions.Logging;

    /// <summary>
    /// Contains methods for registering and unregistering mobile devices of users.
    /// </summary>
    [Authorize]
    [Route("devices")]
    public class DevicesController
        : ApiController
    {
        /// <inheritdoc />
        public DevicesController(ILogger<DevicesController> logger) : base(logger)
        {
        }

        /// <summary>
        /// Register a device for push notifications
        /// </summary>
        /// <param name="id"></param>
        /// <param name="type"></param>
        /// <returns>Id of the registered device</returns>
        /// <response code="200">Returns the id of device</response>
        /// <response code="400">Command is null or invalid</response>
        [HttpPut]
        [ProducesResponseType((int)HttpStatusCode.OK, Type = typeof(Guid))]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> Register([FromQuery] string id, [FromQuery] MobilePlatform type)
        {
            var result = await Mediator.Send(new RegisterDeviceCommand() { DeviceType = type, ExternalDeviceId = id });

            return Ok(result);
        }

        /// <summary>
        /// Unregister a device from push notifications
        /// </summary>
        /// <param name="id"></param>
        /// <returns>Ok result</returns>
        /// <response code="200">Returns Ok result</response>
        /// <response code="400">Command is null or invalid</response>
        /// <response code="403">Forbidden resource</response>
        [HttpDelete]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.Forbidden)]
        public async Task<IActionResult> Unregister([FromQuery] string id)
        {
            await Mediator.Send(new UnregisterDeviceCommand(id));

            return Ok();
        }
    }
}
