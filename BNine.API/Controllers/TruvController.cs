﻿namespace BNine.API.Controllers;

using BNine.Application.Aggregates.EarlySalary.WidgetConfiguration.Models;
using BNine.Application.Aggregates.EarlySalary.WidgetConfiguration.Queries.Get;
using ControllerAttributes;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

/// <summary>
/// Controller for Truv specific endpoints
/// </summary>
[Authorize]
[EnsureUserIsActive]
[Route("truv")]
public class TruvController : ApiController
{
    /// <inheritdoc/>
    public TruvController(ILogger<TruvController> logger) : base(logger)
    {
    }

    /// <summary>
    /// Returns bridge token to access Truv via SDK, and creates user in Truv, if necessary.
    /// Throws exception if payroll provider for user is not Truv.
    /// </summary>
    [HttpGet("bridge-token")]
    [ProducesResponseType(Status404NotFound)]
    [ProducesResponseType(Status400BadRequest)]
    [ProducesResponseType(typeof(TruvUserConfiguration), Status200OK)]
    public async Task<IActionResult> UpdateIfNeededAndGetBridgeToken()
    {
        var result = await Mediator.Send(new GetTruvUserConfigurationQuery());
        return Ok(result);
    }

    /// <summary>
    /// Initializes backend with Truv public token so it could exchange data with Truv API.
    /// Throws exception if payroll provider for user is not Truv.
    /// </summary>
    [HttpPost("public-token")]
    [ProducesResponseType(Status404NotFound)]
    [ProducesResponseType(Status400BadRequest)]
    [ProducesResponseType(Status200OK)]
    public async Task<IActionResult> UpdatePublicToken(UpdateTruvUserPublicTokenQuery query)
    {
        await Mediator.Send(query);

        return Ok();
    }
}
