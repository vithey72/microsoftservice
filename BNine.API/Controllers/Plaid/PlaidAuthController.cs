﻿#pragma warning disable CS1591
namespace BNine.API.Controllers.Plaid
{
    using System.Net;
    using System.Threading.Tasks;
    using Application.Aggregates.Plaid.Commands.CreateAccount;
    using Application.Aggregates.Plaid.Queries.GetLinkToken;
    using Enums;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.Extensions.Logging;

    [Obsolete("We will not be using Plaid anymore")]
    [Route("plaid")]
    [Authorize]
    public class PlaidAuthController : ApiController
    {
        public PlaidAuthController(ILogger<PlaidAuthController> logger) : base(logger)
        {
        }

        /// <summary>
        /// Get link token
        /// </summary>
        /// <returns></returns>
        [HttpGet("linkToken")]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> GetLinkToken([FromQuery] MobilePlatform platform = MobilePlatform.iOs)
        {
            var result = await Mediator.Send(new GetLinkTokenQuery(platform));
            return Ok(result);
        }

        /// <summary>
        /// Create account
        /// </summary>
        /// <returns></returns>
        [HttpPost("bankAccount")]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        [ProducesResponseType(typeof(bool), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> CreateAccount([FromBody] CreateAccountCommand command)
        {
            var result = await Mediator.Send(command);

            return Ok(result);
        }
    }
}
