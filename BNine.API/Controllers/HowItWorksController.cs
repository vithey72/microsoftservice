﻿namespace BNine.API.Controllers;
using System.Net;
using Application.Aggregates.HowItWorks;
using Application.Aggregates.HowItWorks.Queries.ArgyleNoSwitch;
using Application.Aggregates.HowItWorks.Queries.ArgyleSwitched;
using Application.Aggregates.HowItWorks.Queries.CommonVirtualCard;
using Application.Exceptions.ApiResponse;
using Constants;
using Enums;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

/// <summary>
/// Controller to provide front-end with the data for HowItWorks flow screens
/// </summary>
[Authorize]
[Route("how-it-works")]
public class HowItWorksController : ApiController
{
    /// <inheritdoc />
    public HowItWorksController(ILogger<HowItWorksController> logger) :
        base(logger)
    {
    }
    /// <summary>
    /// Provides the data for the screen in accordance with the requested screen type
    /// </summary>
    /// <param name="howItWorksScreenType"></param>
    /// <returns>Returns HowItWorksScreenViewModel</returns>
    /// <exception cref="ApiResponseException"></exception>
    [HttpGet("screen")]
    [ProducesResponseType(typeof(HowItWorksScreenViewModel), (int)HttpStatusCode.OK)]
    public async Task<IActionResult> GetHowItWorksScreen([FromQuery] HowItWorksScreenType howItWorksScreenType)
    {
        var res = howItWorksScreenType switch
        {
            HowItWorksScreenType.ArgyleNotSwitched => await Mediator.Send(new GetHowItWorksArgyleNoSwitchScreenQuery()),
            HowItWorksScreenType.ArgyleSwitched => await Mediator.Send(new GetHowItWorksArgyleSwitchedScreenQuery()),
            HowItWorksScreenType.CommonVirtualCard => await Mediator.Send(new GetHowItWorksCommonVirtualCardScreenQuery()),
            _ => throw new ApiResponseException(ApiResponseErrorCodes.WrongHowItWorksScreenType,
                "You have requested a how-it-works screen type that does not exist")
        };

        return Ok(res);
    }
}
