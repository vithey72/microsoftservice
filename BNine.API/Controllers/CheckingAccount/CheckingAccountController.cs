﻿namespace BNine.API.Controllers.CheckingAccount
{
    using System;
    using System.Net;
    using System.Threading.Tasks;
    using Application.Aggregates.CheckingAccounts.Queries.GetBalance.Models;
    using Application.Aggregates.CheckingAccounts.Queries.GetRewardAccount;
    using Application.Aggregates.CheckingAccounts.Queries.GetRewardAccount.Models;
    using Application.Interfaces;
    using BNine.Application.Aggregates.CheckingAccounts.Models;
    using BNine.Application.Aggregates.CheckingAccounts.Queries.GetBalance;
    using BNine.Application.Aggregates.CheckingAccounts.Queries.GetInfo;
    using BNine.Application.Aggregates.CheckingAccounts.Queries.GetReport;
    using BNine.Application.Aggregates.SavingAccountSettings.Queries.GetSavingAccountSettings;
    using Enums;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.Extensions.Logging;

    [Authorize]
    [Route("checking-accounts")]
    public class CheckingAccountController
        : ApiController
    {
        private readonly ICurrentUserService _currentUserService;
        private readonly ICurrentUserIpAddressService _currentUserIpAddressService;

        public CheckingAccountController(ILogger<CheckingAccountController> logger,
            ICurrentUserService currentUserService,
            ICurrentUserIpAddressService currentUserIpAddressService
            )
            : base(logger)
        {
            _currentUserService = currentUserService;
            _currentUserIpAddressService = currentUserIpAddressService;
        }

        /// <summary>
        ///
        /// </summary>
        /// <returns></returns>
        [HttpGet("info")]
        [ProducesResponseType(typeof(CheckingAccountInfo), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> GetInfo()
        {
            var result = await Mediator.Send(new GetInfoQuery());
            return Ok(result);
        }

        /// <summary>
        ///
        /// </summary>
        /// <returns></returns>
        [HttpGet("balance")]
        [ProducesResponseType(typeof(AccountBalance), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> GetBalance()
        {
            var request = new GetBalanceQuery
            {
                Ip = HttpContext.Connection.RemoteIpAddress.ToString()
            };
            var result = await Mediator.Send(request);
            return Ok(result);
        }

        /// <summary>
        /// Get Balance VM for accounts specified in the request
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost("balance/advanced")]
        [ProducesResponseType(typeof(List<AccountBalanceAdvanced>), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> GetAdvancedBalance(GetBalanceAdvancedRequest request)
        {

            if(request.AccountsToGet == null || request.AccountsToGet.Count == 0)
            {
                request.AccountsToGet = new List<BankAccountTypeEnum>()
                {
                    BankAccountTypeEnum.Main,
                    BankAccountTypeEnum.Reward
                };

            }

            var query = new GetBalanceAdvancedQuery(
                request,
                _currentUserIpAddressService.CurrentIp,
                _currentUserService.UserId!.Value,
                _currentUserService.UserMbanqToken);
            var result = await Mediator.Send(query);
            return Ok(result);
        }

        /// <summary>
        /// Returns the VM for the rewards account screen
        /// </summary>
        /// <returns></returns>
        [HttpGet("rewards-screen")]
        [ProducesResponseType(typeof(RewardAccountScreenViewModel), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> GetRewardsScreen()
        {
            var query = new GetRewardsAccountScreenViewModelQuery(
                _currentUserIpAddressService.CurrentIp,
                _currentUserService.UserId!.Value,
                _currentUserService.UserMbanqToken);
            var result = await Mediator.Send(query);
            return Ok(result);
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="email"></param>
        /// <param name="startingDate"></param>
        /// <param name="endingDate"></param>
        /// <returns></returns>
        [HttpGet("report")]
        [ProducesResponseType((int)HttpStatusCode.Accepted)]
        public async Task<IActionResult> GetReport([FromQuery] string email, [FromQuery] DateTime? startingDate = null, [FromQuery] DateTime? endingDate = null)
        {
            await Mediator.Send(
                new GetAccountStatementReportQuery
                {
                    StartingDate = startingDate,
                    EndingDate = endingDate,
                    Email = email,
                    MbanqAccessToken = _currentUserService.UserMbanqToken,
                    Ip = HttpContext.Connection.RemoteIpAddress.ToString()
                });
            return Accepted();
        }

        /// <summary>
        /// Get saving account settings
        /// </summary>
        /// <returns></returns>
        [HttpGet("settings")]
        [ProducesResponseType(typeof(SavingAccountSettingsInfo), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> GetAccountSettings()
        {
            var result = await Mediator.Send(new GetSavingAccountSettingsQuery());
            return Ok(result);
        }
    }
}
