﻿namespace BNine.API.Controllers
{
    using Application.Aggregates.Array.Queries;
    using Application.Aggregates.Configuration.Queries.GetConfiguration;
    using Application.Exceptions;
    using Array.Aggregates.Models;
    using Constants;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Mvc;

    /// <summary>
    /// Controller for communicating with Array - credit score rating service provider.
    /// User can only access it if they have the feature <see cref="FeaturesNames.Features.ArrayCreditRating"/> enabled.
    /// </summary>
    [Authorize]
    [Route("array")]
    public class ArrayController
        : ApiController
    {
        public ArrayController(ILogger<ArrayController> logger) : base(logger)
        {
        }

        /// <summary>
        /// Generate a fresh User Token and get other properties to authorize current user. If user is not yet enrolled, returns 400.
        /// If user is enrolled but not authorized (have not yet passed quiz), then user token is null.
        /// </summary>
        [HttpGet("customer")]
        [ProducesResponseType(typeof(GetArrayCustomerPropertiesResponse), Status200OK)]
        [ProducesResponseType(Status400BadRequest)]
        public async Task<IActionResult> GetCustomerProperties()
        {
            await ThrowIfNoArrayFeature();
            var result = await Mediator.Send(new GetArrayCustomerPropertiesQuery());
            if (result == null)
            {
                return BadRequest(new { message = "User is not registered in Array" });
            }

            return Ok(result);
        }

        /// <summary>
        /// Accept B9 and Array terms and enroll into Array. If user is already enrolled, returns 204.
        /// </summary>
        [HttpPost("terms-acceptance")]
        [ProducesResponseType(Status201Created)]
        [ProducesResponseType(Status204NoContent)]
        [ProducesResponseType(Status400BadRequest)]
        public async Task<IActionResult> AcceptTerms()
        {
            await ThrowIfNoArrayFeature();
            var success = await Mediator.Send(new CreateArrayUserQuery());
            return success ? Created() : NoContent();
        }

        private async Task ThrowIfNoArrayFeature()
        {
            var configuration = await Mediator.Send(new GetConfigurationQuery());
            if (!configuration.HasFeatureEnabled(FeaturesNames.Features.ArrayCreditRating))
            {
                throw new ValidationException("feature",
                    $"You don't have access to {FeaturesNames.Features.ArrayCreditRating} feature.");
            }
        }
    }
}
