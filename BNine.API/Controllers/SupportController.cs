﻿namespace BNine.API.Controllers
{
    using System.Threading.Tasks;
    using Application.Aggregates.Support.Models;
    using Application.Aggregates.Support.Queries.GetSupportConfirmationScreen;
    using Application.Aggregates.Support.Queries.GetSupportGreetingsScreen;
    using Application.Aggregates.Support.Queries.GetSupportVipOfferScreen;
    using Application.Aggregates.Support.Queries.UpdateSupportVipStatus;
    using Application.Interfaces;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.Extensions.Logging;

    /// <summary>
    /// Controller that provides data for Support tab in app.
    /// </summary>
    [Route("support")]
    [Authorize]
    [ApiController]
    public class SupportController : ApiController
    {
        private readonly ICurrentUserService _currentUserService;

        /// <inheritdoc />
        public SupportController(
            ILogger<SupportController> logger,
            ICurrentUserService currentUserService
            ) : base(logger)
        {
            _currentUserService = currentUserService;
        }

        /// <summary>
        /// Provides viewmodel for rendering support greetings screen.
        /// </summary>
        [HttpGet]
        [ProducesResponseType(Status401Unauthorized)]
        [ProducesResponseType(typeof(SupportGreetingsScreenViewModel), Status200OK)]
        public async Task<IActionResult> GetGreetingsScreen()
        {
            var response = await Mediator.Send(new GetSupportGreetingsScreenQuery());
            return Ok(response);
        }

        /// <summary>
        /// Fetch viewmodel for screen that sells VIP support.
        /// </summary>
        [HttpGet("vip-offer")]
        [ProducesResponseType(Status401Unauthorized)]
        [ProducesResponseType(typeof(SupportVipOfferScreenViewModel), Status200OK)]
        public async Task<IActionResult> GetVipOfferScreen()
        {
            var response = await Mediator.Send(new GetSupportVipOfferScreenQuery());
            return Ok(response);
        }

        /// <summary>
        /// Fetch viewmodel for screen that prompts VIP status enabling.
        /// </summary>
        [HttpGet("vip-offer/confirmation")]
        [ProducesResponseType(Status401Unauthorized)]
        [ProducesResponseType(typeof(SupportVipOfferConfirmationViewModel), Status200OK)]
        public async Task<IActionResult> GetVipConfirmationScreen()
        {
            var response = await Mediator.Send(new GetSupportConfirmationScreenQuery());
            return Ok(response);
        }

        /// <summary>
        /// Enable the VIP status and charge client for the period.
        /// </summary>
        [HttpPut("vip-offer")]
        [ProducesResponseType(Status401Unauthorized)]
        [ProducesResponseType(typeof(SupportVipOfferActivateResponse), Status200OK)]
        public async Task<IActionResult> UpdateVipStatus()
        {
            var response = await Mediator.Send(new UpdateSupportVipStatusQuery
            {
                MbanqAccessToken = _currentUserService.UserMbanqToken,
                Ip = HttpContext.Connection.RemoteIpAddress!.ToString(),
            });
            return Ok(response);
        }
    }
}
