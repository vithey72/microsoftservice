﻿namespace BNine.API.Controllers.Account;

using Application.Aggregates.CommonModels.Dialog;
using Application.Aggregates.Users.Queries.CloseAccount.CloseClientAccountSelfServiced;
using Application.Aggregates.Users.Queries.CloseAccount.GetAccountClosureChecklistScreen;
using Application.Aggregates.Users.Queries.CloseAccount.GetAccountClosureInitDialog;
using Application.Interfaces;
using BNine.API.ControllerAttributes;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

/// <summary>
/// Methods related to the process of account closure by clients themselves.
/// </summary>
[Authorize]
[Route("account/closure")]
public class AccountClosureController : ApiController
{
    private readonly ICurrentUserService _currentUserService;

    /// <inheritdoc />
    public AccountClosureController(
        ILogger<AccountClosureController> logger,
        ICurrentUserService currentUserService
        )
        : base(logger)
    {
        _currentUserService = currentUserService;
    }

    /// <summary>
    /// Get the viewmodel of account closure initiation step.
    /// </summary>
    [HttpGet]
    [ProducesResponseType(typeof(GenericDialogRichBodyViewModel), Status200OK)]
    [EnsureUserIsActive]
    public async Task<IActionResult> GetClosureInitDialog()
    {
        var response = await Mediator.Send(new GetAccountClosureInitDialogQuery());
        return Ok(response);
    }

    /// <summary>
    /// Get the viewmodel of account closure dialog with a checklist,
    /// </summary>
    [HttpGet("checklist")]
    [ProducesResponseType(typeof(GenericDialogWithBulletPoints), Status200OK)]
    public async Task<IActionResult> GetClosureChecklistScreen()
    {
        var response = await Mediator.Send(new GetAccountClosureChecklistScreenQuery
        {
            Ip = HttpContext.Connection.RemoteIpAddress!.ToString(),
            MbanqAccessToken = _currentUserService.UserMbanqToken,
        });
        return Ok(response);
    }

    /// <summary>
    /// Close the account.
    /// </summary>
    [HttpPost]
    [ProducesResponseType(typeof(GenericDialogBodyViewModel), Status200OK)]
    public async Task<IActionResult> CloseAccount()
    {
        var response = await Mediator.Send(new CloseClientAccountSelfServicedQuery());
        return Ok(response);
    }
}
