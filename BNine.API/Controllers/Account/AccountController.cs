﻿namespace BNine.API.Controllers.Account
{
    using System.Net;
    using Application.Aggregates.MbanqAuth.Queries.GetMbanqToken;
    using Application.Aggregates.OTPCodes.Commands.RequestChangePinCode;
    using Application.Aggregates.OTPCodes.Commands.RequestLoginCode;
    using Application.Aggregates.OTPCodes.Commands.RequestRecoveryCode;
    using Application.Aggregates.OTPCodes.Commands.RequestRegistrationCode;
    using Application.Aggregates.OTPCodes.Commands.VerifyRecoveryCode;
    using Application.Aggregates.OTPCodes.Models;
    using Application.Aggregates.OTPCodes.Queries.ValidateSmsCode;
    using Application.Aggregates.Users.Commands.DeleteIdentity;
    using Application.Aggregates.Users.Commands.SignupCommand;
    using Enums;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Mvc;
    using NSwag.Annotations;

    [Route("account")]
    public class AccountController : ApiController
    {
        public AccountController(ILogger<AccountController> logger) : base(logger)
        {
        }

        /// <summary>
        /// Create and send sms with login code
        /// </summary>
        /// <param name="command"></param>
        /// <returns></returns>
        [HttpPost("loginCode")]
        [OpenApiTag("SMS Codes")]
        [ProducesResponseType((int)HttpStatusCode.Created)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> SendLoginCode([FromBody] RequestLoginCodeCommand command)
        {
            await Mediator.Send(command);
            return Created();
        }

        /// <summary>
        /// Create and send sms with registration code
        /// </summary>
        /// <param name="command"></param>
        /// <returns></returns>
        [HttpPost("registrationCode")]
        [OpenApiTag("SMS Codes")]
        [ProducesResponseType(typeof(RegistrationResponse), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> SendRegistrationCode([FromBody] RequestRegistrationCodeCommand command)
        {
            var result = await Mediator.Send(command);
            return Ok(result);
        }

        /// <summary>
        /// Create and send sms with recovery code
        /// </summary>
        /// <param name="command"></param>
        /// <returns></returns>
        [HttpPost("sendRecoveryCode")]
        [OpenApiTag("SMS Codes")]
        [ProducesResponseType((int)HttpStatusCode.Accepted)]
        public async Task<IActionResult> SendRecoveryCode([FromBody] RequestRecoveryCodeCommand command)
        {
            await Mediator.Send(command);
            return Accepted();
        }

        /// <summary>
        /// Verify registration code
        /// </summary>
        /// <param name="command"></param>
        /// <returns></returns>
        [HttpPost("verifyRecoveryCode")]
        [OpenApiTag("SMS Codes")]
        [ProducesResponseType(typeof(Code), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> VerifyRecoveryCode([FromBody] VerifyRecoveryCodeCommand command)
        {
            var result = await Mediator.Send(command);

            return Ok(result);
        }

        /// <summary>
        /// Create and send sms with change pin OTP
        /// </summary>
        /// <param name="command"></param>
        /// <returns></returns>
        [HttpPost("changePinCode")]
        [OpenApiTag("SMS Codes")]
        [ProducesResponseType((int)HttpStatusCode.NoContent)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> SendChangePinCode([FromBody] RequestChangePinCodeCommand command)
        {
            await Mediator.Send(command);
            return NoContent();
        }

        /// <summary>
        /// Create new user
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [ProducesResponseType(typeof(Guid), (int)HttpStatusCode.Created)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> Create([FromBody] SignupCommand model)
        {
            var result = await Mediator.Send(model);
            return Created(result);
        }

        /// <summary>
        /// Validate registration code
        /// </summary>
        /// <param name="phone"></param>
        /// <param name="code"></param>
        /// <returns></returns>
        [HttpGet("registrationCode")]
        [OpenApiTag("SMS Codes")]
        [ProducesResponseType(typeof(Code), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> ValidateRegistrationCode([FromQuery] string phone, [FromQuery] string code)
        {
            var query = new ValidateSmsCodeQuery(phone, code, OTPAction.Signup);

            var result = await Mediator.Send(query);

            return Ok(result);
        }

        /// <summary>
        /// Validate change pin code
        /// </summary>
        /// <param name="phone"></param>
        /// <param name="code"></param>
        /// <returns></returns>
        [HttpGet("changePinCode")]
        [OpenApiTag("SMS Codes")]
        [ProducesResponseType(typeof(Code), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> ValidateChangePinCode([FromQuery] string phone, [FromQuery] string code)
        {
            var query = new ValidateSmsCodeQuery(phone, code, OTPAction.ResetPinCode);

            var result = await Mediator.Send(query);

            return Ok(result);
        }

        /// <summary>
        /// Get Mbanq access token
        /// </summary>
        /// <returns></returns>
        [Authorize]
        [HttpGet("mbanq-token")]
        [OpenApiTag("Mbanq Authorization")]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetMbanqToken()
        {
            var query = new GetMbanqTokenQuery();

            var result = await Mediator.Send(query);

            return Ok(result);
        }

        ///// <summary>
        ///// Deletes user
        ///// </summary>
        ///// <param name="command"></param>
        ///// <returns></returns>
        //[HttpDelete]
        //[ProducesResponseType((int)HttpStatusCode.NotFound)]
        //[ProducesResponseType((int)HttpStatusCode.NoContent)]
        //[ProducesResponseType((int)HttpStatusCode.BadRequest)]
        //public async Task<IActionResult> DeleteUser([FromBody] DeleteUserCommand command)
        //{
        //    await Mediator.Send(command);
        //    return NoContent();
        //}

        /// <summary>
        /// Deletes user identity
        /// </summary>
        /// <param name="command"></param>
        /// <returns></returns>
        [HttpDelete("identity")]
        [ProducesResponseType((int)HttpStatusCode.NotFound)]
        [ProducesResponseType((int)HttpStatusCode.NoContent)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> DeleteUserIdentity([FromBody] DeleteUserIdentityCommand command)
        {
            await Mediator.Send(command);
            return NoContent();
        }
    }
}
