﻿namespace BNine.API.Controllers.CheckCashing
{
    using System;
    using System.Collections.Generic;
    using System.Net;
    using System.Threading.Tasks;
    using Application.Aggregates.CheckCashing.Commands;
    using Application.Aggregates.CheckCashing.Models.Transaction;
    using BNine.Application.Aggregates.CheckCashing.Models;
    using BNine.Application.Aggregates.CheckCashing.Queries;
    using BNine.Application.Interfaces;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.Extensions.Logging;

    /// <summary>
    /// Contains methods that allow to complete the lifecycle of check cashing process.
    /// </summary>
    [Authorize]
    [Route("check-cashing")]
    ////[Feature(Features.CheckCaching)] // TODO:
    public class CheckCashingController
        : ApiController
    {
        public ICurrentUserService CurrentUser
        {
            get;
        }

        public CheckCashingController(ILogger<AlltrustCustomerController> logger, ICurrentUserService currentUser) : base(logger)
        {
            CurrentUser = currentUser;
        }

        /// <summary>
        /// Return check cashing transaction if in progress.
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [ProducesResponseType(typeof(CheckCashingTransactionViewModel), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.NoContent)]
        public async Task<IActionResult> GetTransactionInProgress()
        {
            var transaction = await Mediator.Send(new GetTransactionInProgressQuery());
            if (transaction != null)
            {
                return Ok(transaction);
            }
            return NoContent();
        }

        /// <summary>
        /// Initialize check cashing transaction in our system based on check scans.
        /// </summary>
        /// <param name="checkScans">Object containing images of the check.</param>
        /// <returns></returns>
        [HttpPost]
        [ProducesResponseType(typeof(CheckCashingTransactionViewModel), (int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(CheckCashingInitializationFailedResponse), (int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> InitializeTransaction(CheckScans checkScans)
        {
            var result = await Mediator.Send(new InitializeCheckCashingTransactionQuery { CheckScans = checkScans });
            if (result.Transaction == null)
            {
                return BadRequest(result.FailResponse);
            }
            return Ok(result.Transaction);
        }

        /// <summary>
        /// Update fields of check being cashed and make a decision if this check can be cashed.
        /// </summary>
        /// <returns></returns>
        [HttpPut("{transactionId}")]
        [ProducesResponseType(typeof(CheckCashingTransactionViewModel), (int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(CheckCashingDeniedResponse), (int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> ConfirmTransactionDetails(Guid transactionId, CheckCashingTransactionViewModel transaction)
        {
            if (transactionId == Guid.Empty)
            {
                throw new KeyNotFoundException("Missing transaction Id.");
            }
            transaction.Id = transactionId;

            var confirmResult = await Mediator.Send(new ConfirmTransactionDetailsQuery
            {
                Transaction = transaction
            });
            if (confirmResult.Transaction == null)
            {
                return BadRequest(confirmResult.DenyResponse);
            }

            return Ok(confirmResult.Transaction);
        }

        /// <summary>
        /// Stop cashing process by user request.
        /// </summary>
        /// <returns></returns>
        [HttpDelete("{transactionId}")]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        public async Task<IActionResult> CancelTransaction(Guid transactionId)
        {
            if (transactionId == Guid.Empty)
            {
                throw new KeyNotFoundException("Missing transaction Id.");
            }

            await Mediator.Send(new CancelCheckCashingTransactionCommand
            {
                TransactionId = transactionId,
            });
            return Ok();
        }

        /// <summary>
        /// Add voided check images to transaction. Last user stage of the cashing process.
        /// Returns employer, if check is payroll and this is the first time user cashes from this employer.
        /// </summary>
        /// <returns></returns>
        [HttpPut("{transactionId}/redemption-images")]
        [ProducesResponseType(typeof(SubmitRedemptionImagesResponse), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> SubmitRedemptionImages(Guid transactionId, [FromBody] SubmitRedemptionImagesQuery query)
        {
            if (transactionId == Guid.Empty)
            {
                throw new KeyNotFoundException("Missing transaction Id.");
            }

            query.TransactionId = transactionId;
            query.MbanqAccessToken = CurrentUser.UserMbanqToken;
            query.Ip = HttpContext.Connection.RemoteIpAddress.ToString();

            var submitResult = await Mediator.Send(query);
            var employerName = await Mediator.Send(new GetPayrollCheckEmployerQuery { TransactionId = transactionId });
            submitResult.EmployerName = employerName;

            return Ok(submitResult);
        }

        /// <summary>
        /// Save this check's maker as user's employer.
        /// </summary>
        [HttpPut("{transactionId}/employer/confirmation")]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        public async Task<IActionResult> AddMakerAsEmployer(Guid transactionId)
        {
            if (transactionId == Guid.Empty)
            {
                throw new KeyNotFoundException("Missing transaction Id.");
            }

            await Mediator.Send(new AddMakerAsEmployerCommand { TransactionId = transactionId });

            return Ok();
        }
    }
}
