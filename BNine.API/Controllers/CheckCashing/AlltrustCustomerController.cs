﻿namespace BNine.API.Controllers.CheckCashing
{
    using System.Net;
    using System.Threading.Tasks;
    using Application.Aggregates.CheckCashing.Queries;
    using Application.Aggregates.Configuration.Queries.GetConfiguration;
    using Constants;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.Extensions.Logging;

    /// <summary>
    /// Controller which encapsulates methods for syncing our clients to Alltrust system
    /// to enable them for check cashing process.
    /// </summary>
    [Authorize]
    [Route("check-cashing/customer")]
    public class AlltrustCustomerController
        : ApiController
    {
        /// <summary>
        /// Initialize using base class ctor.
        /// </summary>
        public AlltrustCustomerController(ILogger<AlltrustCustomerController> logger) : base(logger)
        {
        }


        /// <summary>
        /// Checks if a user is trusty enough to have access to our check cashing feature.
        /// </summary>
        /// <returns></returns>
        [HttpGet("is-eligible")]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        public async Task<IActionResult> UserIsEligibleForCheckCashing()
        {
            var configuration = await Mediator.Send(new GetConfigurationQuery());
            var eligibility = (configuration.Features[FeaturesNames.Features.CheckCashing] as dynamic).IsEnabled;
            return Ok(new { isEligible = eligibility });
        }

        /// <summary>
        /// Get Alltrust CustomerId for current user or enroll customer on Alltrust side if required and return the Id.
        /// TODO: Sync user info on consequent calls to the method.
        /// </summary>
        /// <returns></returns>
        [HttpGet("id")]
        [ProducesResponseType((int)HttpStatusCode.Created)]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        public async Task<IActionResult> GetAlltrustCustomerId()
        {
            var alltrustCustomerId = await Mediator.Send(new GetAlltrustCustomerIdQuery());
            return Ok(new { customerId = alltrustCustomerId });
        }
    }
}
