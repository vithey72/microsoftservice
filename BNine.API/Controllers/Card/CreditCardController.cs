﻿namespace BNine.API.Controllers.Card;

using System.Threading.Tasks;
using Application.Aggregates.Cards.Commands.CreateCreditCard;
using Application.Aggregates.Cards.Models;
using Application.Aggregates.Cards.Queries.GetCreditCardStatus;
using Application.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using NSwag.Annotations;

/// <summary>
/// Controller containing methods specific to credit card functionality
/// </summary>
[OpenApiTag("Credit Card")]
[Route("credit-card")]
[ApiController]
[Authorize]
public class CreditCardController : ApiController
{
    private readonly ICurrentUserService _currentUser;

    /// <inheritdoc />
    public CreditCardController(ILogger<CreditCardController> logger, ICurrentUserService currentUser)
        : base(logger)
    {
        _currentUser = currentUser;
    }

    /// <summary>
    /// Create and approve credit card request.
    /// </summary>
    [HttpPost]
    [ProducesResponseType(Status201Created)]
    public async Task<IActionResult> CreateCreditCard()
    {
        var createQuery = new CreateCreditCardCommand();
        await Mediator.Send(createQuery);
        return Created();
    }

    /// <summary>
    /// Get card balance and status.
    /// </summary>
    [HttpGet("status")]
    [ProducesResponseType(typeof(CreditCardStatusViewModel), Status200OK)]
    [ProducesResponseType(Status400BadRequest)]
    public async Task<IActionResult> GetCardStatusAll()
    {
        var result = await Mediator.Send(new GetCreditCardStatusQuery());
        return Ok(result);
    }
}
