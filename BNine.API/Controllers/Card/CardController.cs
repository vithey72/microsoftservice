﻿namespace BNine.API.Controllers.Card;

using System.Threading.Tasks;
using Application.Aggregates.Cards.Commands.ActivateCard;
using Application.Aggregates.Cards.Commands.ActivateVirtualCard;
using Application.Aggregates.Cards.Commands.BlockCard;
using Application.Aggregates.Cards.Commands.ReplaceVirtualDebitCard;
using Application.Aggregates.Cards.Commands.UnblockCard;
using Application.Aggregates.Cards.Models;
using Application.Aggregates.Cards.Queries.GetCardStatus;
using Application.Aggregates.Cards.Queries.GetCardImage;
using Application.Aggregates.Cards.Queries.GetCardShippingStatus;
using Application.Interfaces;
using Infrastructure.Exceptions;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using NSwag.Annotations;
using Application.Aggregates.Cards.Commands.Verify4LastDigits;
using Application.Aggregates.Cards.Queries.CreateCard;
using Application.Aggregates.Cards.Queries.EmbossPhysicalCard;
using Application.Aggregates.Cards.Queries.GetVirtualDebitCardReplacementDetails;
using Application.Aggregates.Cards.Queries.ReplaceDebitCard;
using Application.Aggregates.CommonModels.Dialog;

/// <summary>
/// Controller for manipulations with B9 debit cards issued to our client.
/// Client can have a maximum one active physical card and one active virtual card.
/// </summary>
[OpenApiTag("Card")]
[Route("card")]
[ApiController]
[Authorize]
public class CardController : ApiController
{
    private ICurrentUserService CurrentUser
    {
        get;
    }

    /// <inheritdoc />
    public CardController(ILogger<CardController> logger, ICurrentUserService currentUser)
        : base(logger)
    {
        CurrentUser = currentUser;
    }

    /// <summary>
    /// Create virtual сard.
    /// </summary>
    /// <returns></returns>
    [HttpPost("virtual")]
    [ProducesResponseType(Status201Created)]
    [ProducesResponseType(typeof(ApplicationValidationException), Status400BadRequest)]
    public async Task<IActionResult> CreateCardVirtual()
    {
        var createQuery = new CreateCardQuery
        {
            Ip = HttpContext.Connection.RemoteIpAddress!.ToString(),
            CardType = Enums.DebitCard.CardType.VirtualDebit,
        };

        await Mediator.Send(createQuery);
        return Created();
    }

    /// <summary>
    /// Show a confirmation dialog for payment for physical card.
    /// </summary>
    /// <returns></returns>
    [Obsolete("It's free again.")]
    [HttpGet("physical")]
    [ProducesResponseType(typeof(CreateCardPhysicalDialogViewModel), Status200OK)]
    [ProducesResponseType(typeof(ApplicationValidationException), Status400BadRequest)]
    public async Task<IActionResult> GetCreateCardPhysicalDialog()
    {
        var createQuery = new CreateCardQuery
        {
            Ip = HttpContext.Connection.RemoteIpAddress!.ToString(),
            CardType = Enums.DebitCard.CardType.PhysicalDebit,
        };

        var response = await Mediator.Send(createQuery);
        return Ok(response);
    }

    /// <summary>
    /// Issue a physical card for free with no requirements.
    /// </summary>
    /// <returns></returns>
    [HttpPost("physical")]
    [ProducesResponseType(typeof(GenericSuccessDialog), Status200OK)]
    [ProducesResponseType(typeof(ApplicationValidationException), Status400BadRequest)]
    public async Task<IActionResult> CreateCardPhysical()
    {
        var createQuery = new EmbossPhysicalCardQuery
        {
            Ip = HttpContext.Connection.RemoteIpAddress!.ToString(),
            MbanqAccessToken = CurrentUser.UserMbanqToken,
        };

        var response = await Mediator.Send(createQuery);
        return Ok(response);
    }

    /// <summary>
    /// Activate card
    /// </summary>
    /// <returns></returns>
    [HttpPut("activation")]
    [ProducesResponseType(typeof(ActivationStatusResponseModel), Status200OK)]
    [ProducesResponseType(Status400BadRequest)]
    public async Task<IActionResult> ActivatePhysicalCard([FromBody] ActivateCardCommand command)
    {
        command.Ip = HttpContext.Connection.RemoteIpAddress!.ToString();
        command.MbanqAccessToken = CurrentUser.UserMbanqToken;

        var result = await Mediator.Send(command);

        return Ok(result);
    }

    /// <summary>
    /// Activate virtual card
    /// It's called after PIN set
    /// </summary>
    /// <returns></returns>
    [HttpPut("activation/virtual")]
    [ProducesResponseType(typeof(GenericSuccessDialog), Status200OK)]
    [ProducesResponseType(typeof(GenericFailDialog), Status406NotAcceptable)]
    [ProducesResponseType(Status400BadRequest)]
    public async Task<IActionResult> ActivateVirtualCard([FromBody] ActivateVirtualCardCommand command)
    {
        command.Ip = HttpContext.Connection.RemoteIpAddress!.ToString();
        command.MbanqAccessToken = CurrentUser.UserMbanqToken;

        var result = await Mediator.Send(command);

        if (result.IsSuccess)
        {
            return Ok(result.Success);
        }

        return NotAcceptable(result.Fail);
    }

    /// <summary>
    /// Block card (freeze operations temporarily).
    /// </summary>
    /// <returns></returns>
    [HttpPut("block")]
    [ProducesResponseType(typeof(BlockCardResponse), Status200OK)]
    [ProducesResponseType(Status400BadRequest)]
    public async Task<IActionResult> BlockCard([FromBody] BlockCardCommand command)
    {
        command.MbanqAccessToken = CurrentUser.UserMbanqToken;
        command.Ip = HttpContext.Connection.RemoteIpAddress!.ToString();

        var response = await Mediator.Send(command);

        return Ok(response);
    }

    /// <summary>
    /// Unblock card (unfreeze operations).
    /// </summary>
    /// <returns></returns>
    [HttpPut("unblock")]
    [ProducesResponseType(typeof(BlockCardResponse), Status200OK)]
    [ProducesResponseType(Status400BadRequest)]
    public async Task<IActionResult> UnblockCard([FromBody] UnblockCardCommand command)
    {
        command.MbanqAccessToken = CurrentUser.UserMbanqToken;
        command.Ip = HttpContext.Connection.RemoteIpAddress!.ToString();

        var response = await Mediator.Send(command);

        return Ok(response);
    }

    /// <summary>
    /// Get card status in our workflow.
    /// </summary>
    /// <returns></returns>
    [HttpGet("status")]
    [ProducesResponseType(typeof(List<CardStatusModel>), Status200OK)]
    [ProducesResponseType(Status400BadRequest)]
    public async Task<IActionResult> GetCardStatusAll()
    {
        var result = await Mediator.Send(new GetAllCardsStatusQuery
        {
            MbanqAccessToken = CurrentUser.UserMbanqToken,
            Ip = HttpContext.Connection.RemoteIpAddress!.ToString()
        });

        return Ok(result);
    }

    /// <summary>
    /// Get card image url
    /// </summary>
    /// <returns></returns>
    [HttpGet("image")]
    [ProducesResponseType(typeof(CardImage), Status200OK)]
    [ProducesResponseType(Status400BadRequest)]
    public async Task<IActionResult> GetVisualizationToken([FromQuery] int? id)
    {
        var query = new GetCardImageQuery()
        {
            MbanqAccessToken = CurrentUser.UserMbanqToken,
            Ip = HttpContext.Connection.RemoteIpAddress!.ToString(),
            CardId = id
        };

        var result = await Mediator.Send(query);

        return Ok(result);
    }

    /// <summary>
    /// Get details of replacement procedure.
    /// Backward compatibility for Free Card Reissue
    /// </summary>
    [Obsolete]
    [HttpGet("replacement")]
    [ProducesResponseType(Status204NoContent)]
    public async Task<IActionResult> GetDebitCardReplacementDetails()
    {
        // In case of 204 null response, client will skip the dialog with replacement costs
        return NoContent();
    }

    /// <summary>

    /// Replace card for some reason and charge user if necessary.
    /// </summary>
    /// <returns></returns>
    [HttpPost("replacement")]
    [ProducesResponseType(typeof(GenericSuccessDialog), Status200OK)]
    [ProducesResponseType(typeof(GenericFailDialog), Status406NotAcceptable)]
    [ProducesResponseType(Status400BadRequest)]
    public async Task<IActionResult> ReplaceDebitCard([FromBody] ReplaceDebitCardQuery command)
    {
        command.MbanqAccessToken = CurrentUser.UserMbanqToken;
        command.Ip = HttpContext.Connection.RemoteIpAddress!.ToString();

        var result = await Mediator.Send(command);

        if (result.IsSuccess)
        {
            return Ok(result.Success);
        }

        return NotAcceptable(result.Fail);
    }


    /// Get details of virtual card replacement procedure.
    /// </summary>
    /// <returns>200 - dialog should be displayed. 204 - don't show dialog, proceed with reissue.</returns>
    [HttpGet("replacement/virtual")]
    [ProducesResponseType(typeof(DialogWithCancelButtonViewModel), Status200OK)]
    [ProducesResponseType(Status204NoContent)]
    [ProducesResponseType(Status400BadRequest)]
    public async Task<IActionResult> GetVirtualDebitCardReplacementDetails()
    {
        var command = new GetVirtualDebitCardReplacementDetailsQuery()
        {
            MbanqAccessToken = CurrentUser.UserMbanqToken,
            Ip = HttpContext.Connection.RemoteIpAddress!.ToString(),
        };

        var response = await Mediator.Send(command);
        if (response == null)
        {
            return NoContent();
        }

        return Ok(response);
    }

    /// <summary>
    /// Replace card for some reason and charge user if necessary.
    /// </summary>
    /// <returns></returns>
    [HttpPost("replacement/virtual")]
    [ProducesResponseType(typeof(GenericSuccessDialog), Status200OK)]
    [ProducesResponseType(typeof(GenericFailDialog), Status406NotAcceptable)]
    [ProducesResponseType(Status400BadRequest)]
    public async Task<IActionResult> ReplaceVirtualDebitCard([FromBody] ReplaceVirtualDebitCardCommand command)
    {
        command.MbanqAccessToken = CurrentUser.UserMbanqToken;
        command.Ip = HttpContext.Connection.RemoteIpAddress!.ToString();

        var result = await Mediator.Send(command);

        if (result.IsSuccess)
        {
            return Ok(result.Success);
        }

        return NotAcceptable(result.Fail);
    }

    /// <summary>
    /// Get debit card shipping information.
    /// </summary>
    [Obsolete("Seems unused according to App Insights.")]
    [HttpGet("shipping")]
    [ProducesResponseType(typeof(ShippingHistoryModel), Status200OK)]
    [ProducesResponseType(Status400BadRequest)]
    public async Task<IActionResult> GetCardShippingStatus([FromQuery] int? id)
    {
        var query = new GetCardShippingStatusQuery()
        {
            MbanqAccessToken = CurrentUser.UserMbanqToken,
            Ip = HttpContext.Connection.RemoteIpAddress!.ToString(),
            CardId = id
        };

        var result = await Mediator.Send(query);

        return Ok(result);
    }

    /// <summary>
    /// Get card info. Card type, status, last digits, etc.
    /// </summary>
    [HttpGet]
    [ProducesResponseType(typeof(CardStatusModel), Status200OK)]
    [ProducesResponseType(Status400BadRequest)]
    public async Task<IActionResult> GetCard([FromQuery] int id)
    {
        var query = new GetCardStatusQuery()
        {
            CardId = id,
            MbanqAccessToken = CurrentUser.UserMbanqToken,
            Ip = HttpContext.Connection.RemoteIpAddress!.ToString()
        };

        var result = await Mediator.Send(query);

        return Ok(result);
    }

    /// <summary>
    /// Verify last 4 digits of the physical card for user to confirm physically receiving card.
    /// TODO: needs to be implemented, story: B9-2577
    /// </summary>
    /// <returns></returns>
    [HttpPost("verify-last-4-numbers")]
    public async Task<IActionResult> VerifyLast4Numbers(string last4Digits, int cardId)
    {
        var result = await Mediator.Send(new Verify4LastDigitsCommand
        {
            Last4Numbers = last4Digits,
            CardId = cardId,
            MbanqAccessToken = CurrentUser.UserMbanqToken,
            Ip = HttpContext.Connection.RemoteIpAddress!.ToString()
        });

        return Ok(result);
    }
}
