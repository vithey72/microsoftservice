﻿namespace BNine.API.Controllers.Card;

using System.Net;
using Application.Aggregates.Users.Models.CardLimits;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using NSwag.Annotations;

[OpenApiTag("Card")]
[Route("card/active-limits")]
[Authorize]
public class CardActiveLimitsController : ApiController
{
    public CardActiveLimitsController(ILogger<CardLimitsController> logger) : base(logger)
    {
    }

    /// <summary>
    /// Returns card limits
    /// </summary>
    /// <returns></returns>
    [HttpGet]
    [ProducesResponseType(typeof(UserCardLimits), (int)HttpStatusCode.OK)]
    [ProducesResponseType((int)HttpStatusCode.BadRequest)]
    [ProducesResponseType((int)HttpStatusCode.NotFound)]
    public async Task<IActionResult> GetCardLimits()
    {
        var command = new Application.Aggregates.Cards.Queries.GetCardLimits.GetCardLimitsQuery();

        var result = await Mediator.Send(command);

        return Ok(result);
    }
}
