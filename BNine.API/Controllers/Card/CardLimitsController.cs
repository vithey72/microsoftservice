﻿namespace BNine.API.Controllers.Card
{
    using System.Net;
    using System.Threading.Tasks;
    using Application.Aggregates.Cards.Commands.UpdateCardLimits;
    using Application.Aggregates.Cards.Models;
    using Application.Aggregates.Cards.Queries.GetCardLimitsObselete;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.Extensions.Logging;
    using NSwag.Annotations;

    [Obsolete($"New controller added {nameof(CardActiveLimitsController)}")]
    [Authorize]
    [ApiController]
    [OpenApiTag("Card")]
    [Route("card/limits")]
    public class CardLimitsController : ApiController
    {
        public CardLimitsController(ILogger<CardLimitsController> logger) : base(logger)
        {
        }

        /// <summary>
        /// Returns card limits
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [ProducesResponseType(typeof(CardLimitsResponseModel), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetCardLimits([FromQuery] int? id)
        {
            var command = new GetCardLimitsQueryObselete()
            {
                CardId = id
            };

            var result = await Mediator.Send(command);

            return Ok(result);
        }

        /// <summary>
        /// Set limits for card operations
        /// </summary>
        /// <returns></returns>
        [HttpPut]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> UpdateCardLimits([FromBody] UpdateCardLimitsCommand command)
        {
            await Mediator.Send(command);

            return Ok();
        }
    }
}
