namespace BNine.API.Controllers.Card
{
    using System.Net;
    using System.Threading.Tasks;
    using BNine.Application.Aggregates.Cards.Commands.ConnectWallet;
    using BNine.Application.Aggregates.Cards.Queries.GetWalletsInfo;
    using BNine.Enums;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.Extensions.Logging;
    using NSwag.Annotations;

    [OpenApiTag("Card")]
    [Route("card/wallets")]
    [Authorize]
    public class CardWalletsController : ApiController
    {
        public CardWalletsController(ILogger<CardWalletsController> logger) : base(logger)
        {
        }

        /// <summary>
        /// Connect apple pay
        /// </summary>
        /// <returns></returns>
        [HttpPost("apple")]
        [ProducesResponseType((int)HttpStatusCode.NoContent)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> ConnectApplePay([FromBody] ConnectWalletCommand request)
        {
            var command = new ConnectWalletCommand(DigitalWalletType.Apple){
                CardId = request.CardId
            };

            await Mediator.Send(command);
            return NoContent();
        }

        /// <summary>
        /// Connect google pay
        /// </summary>
        /// <returns></returns>
        [HttpPost("google")]
        [ProducesResponseType((int)HttpStatusCode.NoContent)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> ConnectGooglePay([FromBody] ConnectWalletCommand request)
        {
            var command = new ConnectWalletCommand(DigitalWalletType.Google)
            {
                CardId = request.CardId
            };

            await Mediator.Send(command);
            return NoContent();
        }

        /// <summary>
        /// Connect samsung pay
        /// </summary>
        /// <returns></returns>
        [HttpPost("samsung")]
        [ProducesResponseType((int)HttpStatusCode.NoContent)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> ConnectSamsungPay([FromBody] ConnectWalletCommand request)
        {
            var command = new ConnectWalletCommand(DigitalWalletType.Samsung)
            {
                CardId = request.CardId
            };

            await Mediator.Send(command);
            return NoContent();
        }

        /// <summary>
        /// Get wallets information
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [ProducesResponseType(typeof(WalletsInfo), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> Get([FromQuery] int id)
        {
            var query = new GetWalletsInfoQuery()
            {
                CardId = id
            };

            var result = await Mediator.Send(query);
            return Ok(result);
        }
    }
}
