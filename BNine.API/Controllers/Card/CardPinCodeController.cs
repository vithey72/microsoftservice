namespace BNine.API.Controllers.Card
{
    using System.Net;
    using System.Threading.Tasks;
    using Application.Aggregates.Cards.Commands.ChangePinCode;
    using Application.Aggregates.Cards.Commands.UpdatePinCodeChanged;
    using Application.Aggregates.Cards.Models;
    using Application.Interfaces;
    using BNine.Settings;
    using Enums.DebitCard;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.Extensions.Logging;
    using Microsoft.Extensions.Options;
    using NSwag.Annotations;

    /// <summary>
    /// Controller that returns link to pin code set page, and accepts callbacks after client-side part of the process is done.
    /// </summary>
    [OpenApiTag("Card")]
    [Route("card")]
    [ApiController]
    [Authorize]
    public class CardPinCodeController : ApiController
    {
        private ICurrentUserService CurrentUserService
        {
            get;
        }

        private readonly MBanqSettings _mbanqSettings;
        private readonly IBNineDbContext _bNineDbContext;

        /// <inheritdoc />
        public CardPinCodeController(
            ILogger<CardPinCodeController> logger,
            ICurrentUserService currentUserService,
            IOptions<MBanqSettings> mbanqSettings,
            IBNineDbContext bNineDbContext) : base(logger)
        {
            CurrentUserService = currentUserService;
            _mbanqSettings = mbanqSettings.Value;
            _bNineDbContext = bNineDbContext;
        }

        /// <summary>
        /// Gets web view url to set pin code
        /// </summary>
        /// <returns></returns>
        [HttpPost("pin-code")]
        [ProducesResponseType(typeof(PinCodeWebView), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetPinCodeWebView([FromBody] ChangePinCodeCommand command)
        {
            //var requestUrl = Request.HttpContext.Request.GetUri();
            var successUrl = _mbanqSettings.DebitCardSettings.PinSuccessUrl;//GetFullEndpointUrl(requestUrl, "PinSuccess");
            var failedUrl = _mbanqSettings.DebitCardSettings.PinFailureUrl; // GetFullEndpointUrl(requestUrl, "PinFailure");

            var query = new ChangePinCodeCommand
            {
                MbanqAccessToken = CurrentUserService.UserMbanqToken,
                Ip = HttpContext.Connection.RemoteIpAddress!.ToString(),
                SuccessUrl = successUrl,
                FailureUrl = failedUrl,
                CardId = command.CardId
            };

            var result = await Mediator.Send(query);

            return Ok(result);
        }

        /// <summary>
        /// Endpoint to confirm card delivery
        /// </summary>
        /// <returns></returns>
        [AllowAnonymous]
        [HttpGet("{cardId}/pin-success")]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> PinSuccess([FromRoute] int cardId)
        {
            var command = new UpdatePinCodeSuccessfullyChangedCommand()
            {
                ExternalCardId = cardId
            };

            await Mediator.Send(command);

            return Ok("success");
        }

        /// <summary>
        /// Endpoint to confirm card delivery
        /// Backward compatability
        /// </summary>
        /// <returns></returns>
        [AllowAnonymous]
        [HttpGet("pin-success")]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> PinSuccessBackward()
        {
            var physicalCard = await _bNineDbContext.DebitCards.FirstOrDefaultAsync(x =>
                x.Type == CardType.PhysicalDebit && x.Status == CardStatus.ACTIVE);

            var command = new UpdatePinCodeSuccessfullyChangedCommand() { ExternalCardId = physicalCard.ExternalId };

            await Mediator.Send(command);

            return Ok("success");
        }

        /// <summary>
        /// Something did not go OK.
        /// </summary>
        /// <returns></returns>
        [AllowAnonymous]
        [HttpGet("pin-failure")]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> PinFailure()
        {
            //var command = new ConfirmDeliveryCommand();

            //await Mediator.Send(command);

            return Ok("failure");
        }

        //TODO: Revise
        // private string GetFullEndpointUrl(Uri requestUrl, string actionName)
        // {
        //     return string.Format(
        //         "{0}://{1}{2}",
        //         requestUrl.Scheme,
        //         requestUrl.Authority,
        //         Url.Action(actionName, "DebitCardPinCode"));
        // }
    }
}
