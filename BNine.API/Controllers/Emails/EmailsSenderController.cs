﻿namespace BNine.API.Controllers.Emails
{
    using System.Threading.Tasks;
    using Application.Aggregates.Emails.Commands.Argyle.Connect;
    using Application.Aggregates.Emails.Commands.Onboarding.NotCompleted;
    using BNine.Application.Aggregates.CheckingAccounts.Queries.GetStatements;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.Extensions.Logging;

    /// <summary>
    /// Collection of endpoints that send automated emails. Triggered by Azure functions or other external sources.
    /// </summary>
    [Authorize(Policy = "EmailsSender")]
    [Route("emails")]
    [ApiExplorerSettings(IgnoreApi = true)]
    public class EmailsSenderController : ApiController
    {
        /// <inheritdoc />
        public EmailsSenderController(ILogger<EmailsSenderController> logger) : base(logger)
        {
        }

        /// <summary>
        /// Endpoint which is triggered by CRON function and sends emails to continue registration
        /// </summary>
        /// <returns></returns>
        [HttpPost("registration")]
        public async Task<IActionResult> SendContinueRegistrationEmails()
        {
            await Mediator.Send(new OnboardingNotCompletedEmailCommand());
            return Ok();
        }

        /// <summary>
        /// Endpoint which triggers by CRON function and sends emails to connect argyle
        /// </summary>
        /// <returns></returns>
        [HttpPost("argyle")]
        public async Task<IActionResult> SendArgyleEmails()
        {
            await Mediator.Send(new ConnectArgyleEmailCommand());
            return Ok();
        }

        /// <summary>
        /// An Endpoint, automatically consumed by Azure function, 
        /// for getting month and year statements for all customers had taken transactions for that period.
        /// </summary>
        /// <returns></returns>
        [HttpPost("statements")]
        [AllowAnonymous]
        public async Task<IActionResult> GetStatements([FromBody] GetStatementsQuery query)
        {
            await Mediator.Send(query);
            return Accepted();
        }
    }
}
