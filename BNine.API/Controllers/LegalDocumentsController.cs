﻿namespace BNine.API.Controllers;

using Application.Aggregates.StaticDocuments.Queries;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

/// <summary>
/// Controller for both mobile clients and admin clients querying of static legal documents.
/// </summary>
[AllowAnonymous]
[Route("legal-documents")]
public class LegalDocumentsController : ApiController
{
    /// <inheritdoc />
    public LegalDocumentsController(ILogger<LegalDocumentsController> logger) : base(logger)
    {
    }

    /// <summary>
    /// Get a list of all documents known to system and their details, including download link.
    /// </summary>
    [HttpGet]
    [Route("")]
    public async Task<IActionResult> GetDocuments()
    {
        var response = await Mediator.Send(new GetLegalDocumentsQuery());
        return Ok(response);
    }

    /// <summary>
    /// Get specific document's details, including download link.
    /// </summary>
    /// <param name="key">Unique name of the document</param>
    /// <returns></returns>
    [HttpGet]
    [Route(("{key}"))]
    public async Task<IActionResult> GetDocumentByKey(string key)
    {
        var response = await Mediator.Send(new GetLegalDocumentQuery() { DocumentKey = key });
        return Ok(response);
    }
}
