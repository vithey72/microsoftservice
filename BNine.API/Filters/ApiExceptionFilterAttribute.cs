﻿namespace BNine.API.Filters
{
    using System;
    using System.Collections.Generic;
    using BNine.API.Infrastructure.Exceptions;
    using BNine.API.Infrastructure.Exceptions.ApiResponseException;
    using BNine.Application.Exceptions;
    using BNine.Application.Exceptions.ApiResponse;
    using Microsoft.AspNetCore.Http;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Filters;

    public class ApiExceptionFilterAttribute : ExceptionFilterAttribute
    {
        private readonly IDictionary<Type, Action<ExceptionContext>> exceptionHandlers;

        public ApiExceptionFilterAttribute()
        {
            exceptionHandlers = new Dictionary<Type, Action<ExceptionContext>>
            {
                { typeof(ApiResponseException), HandleApiResponseException },
                { typeof(ValidationException), HandleValidationException },
                { typeof(NotFoundException), HandleNotFoundException },
                { typeof(UnauthorizedAccessException), HandleUnauthorizedAccessException },
                { typeof(IdempotencyViolationException), HandleIdempotencyViolationException }
            };
        }

        public override void OnException(ExceptionContext context)
        {
            HandleException(context);
            base.OnException(context);
        }

        private void HandleException(ExceptionContext context)
        {
            Type type = context.Exception.GetType();
            if (exceptionHandlers.ContainsKey(type))
            {
                exceptionHandlers[type].Invoke(context);
                return;
            }

            if (!context.ModelState.IsValid)
            {
                HandleInvalidModelStateException(context);
                return;
            }

            HandleUnknownException(context);
        }

        private void HandleUnknownException(ExceptionContext context)
        {
            var details = new ProblemDetails
            {
                Status = StatusCodes.Status500InternalServerError,
                Title = "An error occurred while processing your request.",
                Type = "https://tools.ietf.org/html/rfc7231#section-6.6.1"
            };

            context.Result = new ObjectResult(details)
            {
                StatusCode = StatusCodes.Status500InternalServerError
            };

            context.ExceptionHandled = true;
        }

        private void HandleInvalidModelStateException(ExceptionContext context)
        {
            var details = new ValidationProblemDetails(context.ModelState)
            {
                Type = "https://tools.ietf.org/html/rfc7231#section-6.5.1"
            };

            context.Result = new BadRequestObjectResult(details);

            context.ExceptionHandled = true;
        }

        private void HandleValidationException(ExceptionContext context)
        {
            context.Result =
                new ObjectResult(new ApplicationValidationException(
                    ((ValidationException)context.Exception).Errors,
                    ((ValidationException)context.Exception).ErrorCodes))
                {
                    StatusCode = StatusCodes.Status400BadRequest
                };

            context.ExceptionHandled = true;
        }

        private void HandleApiResponseException(ExceptionContext context)
        {
            context.Result =
                new ObjectResult(new ApplicationApiResponseException(
                    ((ApiResponseException)context.Exception).Code,
                    ((ApiResponseException)context.Exception).Message,
                    ((ApiResponseException)context.Exception).Title))
                {
                    StatusCode = StatusCodes.Status400BadRequest
                };

            context.ExceptionHandled = true;
        }

        private void HandleUnauthorizedAccessException(ExceptionContext context)
        {
            context.Result = new UnauthorizedObjectResult(null);
            context.ExceptionHandled = true;
        }

        private void HandleNotFoundException(ExceptionContext context)
        {
            var exception = (NotFoundException)context.Exception;

            context.Result = new ObjectResult(exception.Message)
            {
                StatusCode = StatusCodes.Status404NotFound
            };

            context.ExceptionHandled = true;
        }

        private void HandleIdempotencyViolationException(ExceptionContext context)
        {
            context.Result = new ObjectResult(null)
            {
                StatusCode = Status202Accepted,
            };

            context.ExceptionHandled = true;
        }
    }
}
