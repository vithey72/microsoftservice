﻿#pragma warning disable IDE0005 // Using directive is unnecessary.
#pragma warning disable CS1591 // Missing XML comment for publicly visible type or member

// ReSharper disable RedundantUsingDirective
namespace BNine.API
{
    using System;
    using System.Threading.Tasks;
    using BNine.API.Infrastructure.Extensions;
    using BNine.Application.Interfaces;
    using BNine.Persistence;
    using Microsoft.AspNetCore.Hosting;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.Extensions.DependencyInjection;
    using Microsoft.Extensions.Hosting;
    using Persistence.Seeds;


    public class Program
    {
        public static async Task Main(string[] args)
        {
            var host = CreateHostBuilder(args).Build();
            IWebHostEnvironment env = host.Services.GetRequiredService<IWebHostEnvironment>();

            // make sure that debug runs don't modify any production data

            if (env != null && env.IsProduction())
            {
                var logger = host.Services.GetRequiredService<ILogger<Program>>();
                using (var scope = host.Services.CreateScope())
                {

                    try
                    {
                        logger.LogInformation("Seeding database...");
                        var services = scope.ServiceProvider;
                        var context = services.GetRequiredService<IBNineDbContext>() as BNineDbContext;
                        var partnerService = services.GetRequiredService<IPartnerProviderService>();
                        await BNineDbContextSeed.SeedSampleDataAsync(context, partnerService);
                    }
                    catch(Exception ex)
                    {
                        logger.LogError(ex, "An error occurred while seeding the database.");
                    }
                }
                logger.LogInformation("Seeding database completed.");
            }

            await host.RunAsync();
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .ConfigureAppConfiguration((context, config) =>
                {
                    if (context.HostingEnvironment.IsProduction())
                    {
                        config.ConfigureKeyVault();
                    }
                    else
                    {
                        config.WriteConfigurationSources();
                    }
                })
                .ConfigureWebHostDefaults(webBuilder => webBuilder.UseStartup<Startup>());
    }
}
