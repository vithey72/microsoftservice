﻿namespace BNine.IdentityServer.Infrastructure.Services
{
    using System;
    using System.Linq;
    using System.Security.Claims;
    using System.Threading.Tasks;
    using Application.Aggregates.AdminUser.Queries;
    using Application.Aggregates.Users.Queries.GetUserByPhone;
    using BNine.Application.Aggregates.MbanqAuth.Queries.GetMbanqTokensByLoginAndPassword;
    using BNine.Application.Aggregates.MbanqAuth.Queries.GetMbanqTokensByRefreshToken;
    using BNine.Application.Aggregates.Users.Queries.GetUserById;
    using Application.Models.MBanq;
    using BNine.Application.Extensions;
    using BNine.Application.Models.Dto;
    using IdentityServer4;
    using IdentityServer4.Models;
    using IdentityServer4.Services;
    using MediatR;
    using Newtonsoft.Json;

    public class ProfileService : IProfileService
    {
        private readonly IHttpContextAccessor _httpContext;

        private IMediator Mediator
        {
            get;
        }

        public ProfileService(
            IMediator mediator,
            IHttpContextAccessor httpContext
            )
        {
            Mediator = mediator;
            _httpContext = httpContext;
        }

        public async Task GetProfileDataAsync(ProfileDataRequestContext context)
        {
            context.AddRequestedClaims(context.Subject.Claims);
            switch (context.Client.ClientId)
            {
                case "BNineClient":
                    await AuthorizeClient(context, _httpContext.HttpContext!.RequestAborted);
                    return;
                case "BNineAdminUI":
                    await AuthorizeAdminUi(context);
                    return;
                case "BNineAdmin":
                    await AuthorizeApiAdminClient(context);
                    return;
                default:
                    throw new ArgumentException("Invalid client");
            }
        }

        private Task AuthorizeApiAdminClient(ProfileDataRequestContext context)
        {
            context.IssuedClaims.Add(new Claim(ClaimTypes.Role, "ApiAdmin"));
            context.IssuedClaims.Add(new Claim("username", "User with X-Api-Key"));

            return Task.CompletedTask;
        }

        private async Task AuthorizeAdminUi(ProfileDataRequestContext context)
        {
            var userId = context.Subject.Claims
                .FirstOrDefault(x => x.Type == "sub")?
                .Value;

            if (Guid.TryParse(userId, out var result))
            {
                var apiUser = await Mediator.Send(new GetAdminApiUserByIdQuery
                {
                    Id = result
                });

                var roleClaim = context.IssuedClaims.FirstOrDefault(x => x.Type == "role");

                if (roleClaim != null)
                {
                    context.IssuedClaims.Remove(roleClaim);
                }

                var usernameClaim = context.IssuedClaims.FirstOrDefault(x => x.Type == "username");

                if (usernameClaim != null)
                {
                    context.IssuedClaims.Remove(usernameClaim);
                }

                context.IssuedClaims.Add(new Claim("role", apiUser.Role));
                context.IssuedClaims.Add(new Claim("username", $"{apiUser.FirstName} {apiUser.LastName}"));
            }
        }

        private async Task AuthorizeClient(ProfileDataRequestContext context, CancellationToken cancellationToken)
        {
            var userId = context.Subject.Claims
                .FirstOrDefault(x => x.Type == "sub")?
                .Value;

            User user = null;
            if (Guid.TryParse(userId, out var result))
            {
                user = await Mediator.Send(new GetUserByIdQuery
                {
                    Id = result
                }, cancellationToken);

                var statusClaim = context.IssuedClaims.FirstOrDefault(x => x.Type == "status");

                if (statusClaim != null)
                {
                    context.IssuedClaims.Remove(statusClaim);
                }

                context.IssuedClaims.Add(new Claim("status", user.Status.ToLowerCamelCase()));
            }

            var roleClaims = context.Subject.Claims.Where(x => x.Type == ClaimTypes.Role);

            if (roleClaims != null)
            {
                context.IssuedClaims.AddRange(roleClaims);
            }

            var grantType = context.ValidatedRequest.Raw.GetValues("grant_type")!.First().ToLower();
            if (grantType is "password" or "biometrical" or "passcode")
            {
                var userName = context.ValidatedRequest.Raw.GetValues("username")!.First();
                var password = context.ValidatedRequest.Raw.GetValues("password")!.First();

                user = await Mediator.Send(new GetUserByPhoneQuery
                {
                    PhoneNumber = userName
                }, cancellationToken);

                var mbanqTokens = await Mediator.Send(new GetMbanqTokensByLoginAndPasswordQuery
                {
                    Login = user.Id.ToString(),
                    Password = password
                }, cancellationToken);

                AddMBanqClaims(context, mbanqTokens, user);
            }
            else if (grantType == "refresh_token")
            {
                var mbanqRefreshToken = context.Subject.Claims
                    .First(x => x.Type.ToLower() == "mbanq_refresh_token")
                    .Value;

                var mbanqTokens = await Mediator.Send(new GetMbanqTokensByRefreshTokenQuery
                {
                    RefreshToken = mbanqRefreshToken,
                    UserId = userId
                }, cancellationToken);

                AddMBanqClaims(context, mbanqTokens, user);
            }
        }

        public Task IsActiveAsync(IsActiveContext context)
        {
            context.IsActive = true;
            return Task.CompletedTask;
        }

        private static void AddMBanqClaims(ProfileDataRequestContext context, AuthorizationTokensResponseModel mbanqTokens,
            User user)
        {
            context.IssuedClaims.Add(new Claim("mbanq_access_token", mbanqTokens.AccessToken));
            context.IssuedClaims.Add(new Claim("mbanq_refresh_token", mbanqTokens.RefreshToken));
            context.IssuedClaims.Add(new Claim("groups", JsonConvert.SerializeObject(user.Groups),
                IdentityServerConstants.ClaimValueTypes.Json));
        }
    }
}
