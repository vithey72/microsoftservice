﻿namespace BNine.IdentityServer
{
    using System.Collections.Generic;
    using System.Security.Claims;
    using System.Threading.Tasks;
    using Application.Aggregates.AdminUser.Queries;
    using Application.Aggregates.Users.Queries.GetUserByPhone;
    using BNine.Application.Aggregates.OTPCodes.Commands.DeactivateCode;
    using BNine.Application.Aggregates.OTPCodes.Queries.ValidateSmsCode;
    using BNine.Application.Extensions;
    using BNine.Common.TableConnect.Common;
    using BNine.Enums;
    using BNine.ResourceLibrary;
    using IdentityServer4.Models;
    using IdentityServer4.Validation;
    using MediatR;
    using Microsoft.Extensions.Localization;

    public class CustomResourceOwnerPasswordValidator : IResourceOwnerPasswordValidator
    {
        private readonly IHttpContextAccessor _httpContext;

        private IMediator Mediator
        {
            get;
        }

        private IStringLocalizer<SharedResource> SharedLocalizer
        {
            get;
        }

        public CustomResourceOwnerPasswordValidator(
            IMediator mediator,
            IStringLocalizer<SharedResource> sharedLocalizer,
            IHttpContextAccessor httpContext
            )
        {
            _httpContext = httpContext;
            Mediator = mediator;
            SharedLocalizer = sharedLocalizer;
        }

        public async Task ValidateAsync(ResourceOwnerPasswordValidationContext context)
        {
            switch (context.Request.ClientId)
            {
                case "BNineClient":
                    await ValidateBNineClient(context, _httpContext.HttpContext!.RequestAborted);
                    return;
                case "BNineAdminUI":
                    await ValidateBNineAdminUiClient(context);
                    return;
                default:
                    await SetInvalidResult(context);
                    return;
            }
        }

        private async Task ValidateBNineClient(ResourceOwnerPasswordValidationContext context, CancellationToken cancellationToken)
        {
            var user = await Mediator.Send(new GetUserByPhoneQuery
            {
                PhoneNumber = context.Request.Raw["username"]
            }, cancellationToken);

            if (user == null || !CryptoHelper.VerifyHashedPassword(user.PasswordHash, context.Password))
            {
                context.Result = new GrantValidationResult(
                    TokenRequestErrors.InvalidRequest,
                    SharedLocalizer["incorrectCredentialsValidationErrorMessage"].Value);

                return;
            }

            if (user.Status != Enums.UserStatus.PendingPersonalInformation)
            {
                if (string.IsNullOrEmpty(context.Request.Raw["code"]))
                {
                    context.Result = new GrantValidationResult(
                        TokenRequestErrors.InvalidRequest,
                        SharedLocalizer["requiredCodeErrorMessage"].Value);

                    return;
                }

                var isCodeValid = (await Mediator.Send(new ValidateSmsCodeQuery(
                    context.UserName,
                    context.Request.Raw["code"],
                    OTPAction.Login),
                    cancellationToken)).IsValid;

                if (!isCodeValid)
                {
                    context.Result = new GrantValidationResult(
                        TokenRequestErrors.InvalidRequest,
                        SharedLocalizer["invalidCodeValidationErrorMessage"].Value);

                    return;
                }

                await Mediator.Send(new DeactivateCodeCommand
                {
                    Code = context.Request.Raw["code"],
                    PhoneNumber = context.Request.Raw["username"]
                }, CancellationToken.None);
            }

            var claims = new List<Claim>
            {
                new Claim("status", user.Status.ToLowerCamelCase())
            };

            context.Result = new GrantValidationResult(user.Id.ToString(), "password", claims, "local");
        }

        private async Task ValidateBNineAdminUiClient(ResourceOwnerPasswordValidationContext context)
        {
            var user = await Mediator.Send(new GetAdminApiUserByEmailQuery
            {
                Email = context.Request.Raw["username"]
            });

            if (user == null || !CryptoHelper.VerifyHashedPassword(user.PasswordHash, context.Password))
            {
                context.Result = new GrantValidationResult(
                    TokenRequestErrors.InvalidRequest,
                    SharedLocalizer["incorrectCredentialsValidationErrorMessage"].Value);

                return;
            }

            var claims = new List<Claim>
            {
                new("role", user.Role),
                new("sub", user.Id.ToString()),
                new("username", $"{user.FirstName} {user.LastName}"),
            };

            context.Result = new GrantValidationResult(user.Id.ToString(), "password", claims, "local");
        }

        private Task SetInvalidResult(ResourceOwnerPasswordValidationContext context)
        {
            context.Result = new GrantValidationResult(
                TokenRequestErrors.InvalidRequest,
                SharedLocalizer["invalidClientErrorMessage"].Value);
            return Task.CompletedTask;
        }
    }
}
