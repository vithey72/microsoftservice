﻿namespace BNine.IdentityServer.Infrastructure.Services
{
    using System;
    using System.IdentityModel.Tokens.Jwt;
    using System.Text;
    using System.Threading.Tasks;
    using Azure.Identity;
    using Azure.Security.KeyVault.Keys.Cryptography;
    using IdentityServer4.Configuration;
    using IdentityServer4.Services;
    using Microsoft.AspNetCore.Authentication;
    using Microsoft.Extensions.Logging;
    using Newtonsoft.Json;

    public class KeyVaultTokenCreationService : DefaultTokenCreationService
    {
        //private readonly IConfiguration configuration;

        public KeyVaultTokenCreationService(ISystemClock clock, IKeyMaterialService keys, IdentityServerOptions options, ILogger<DefaultTokenCreationService> logger)
        : base(clock, keys, options, logger)
        {
            //configuration = configuration;
        }

        protected override async Task<string> CreateJwtAsync(JwtSecurityToken jwt)
        {
            try
            {
                var keyVaultEndpoint = Environment.GetEnvironmentVariable("KEYVAULT_ENDPOINT");
                var plainText = $"{jwt.EncodedHeader}.{jwt.EncodedPayload}";

                byte[] hash;

                using (var hasher = CryptoHelper.GetHashAlgorithmForSigningAlgorithm(jwt.SignatureAlgorithm))
                {
                    hash = hasher.ComputeHash(Encoding.UTF8.GetBytes(plainText));
                }

                Logger.LogInformation("keyVaultEndpoint: {keyVaultEndpoint}", keyVaultEndpoint);

                var cryptoClient = new CryptographyClient(
                    keyId: new Uri($"{keyVaultEndpoint}/keys/IdentityServerSettings--SigningKey"),
                    credential: new DefaultAzureCredential());

                var signResult = await cryptoClient.SignAsync(new SignatureAlgorithm(jwt.SignatureAlgorithm), hash);

                Logger.LogInformation("signResult: {signResult}", JsonConvert.SerializeObject(signResult));

                return $"{plainText}.{Base64UrlTextEncoder.Encode(signResult.Signature)}";
            }
            catch (Exception ex)
            {
                Logger.LogError(ex, "some error has occured {ex}", JsonConvert.SerializeObject(ex));
                throw;
            }
        }
    }
}
