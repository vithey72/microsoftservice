﻿namespace BNine.IdentityServer.Infrastructure.Services
{
    using System.Security.Claims;
    using Application.Aggregates.Users.Queries.GetUserByPhone;
    using BNine.Application.Extensions;
    using BNine.Common.TableConnect.Common;
    using BNine.ResourceLibrary;
    using IdentityServer4.Models;
    using IdentityServer4.Validation;
    using MediatR;
    using Microsoft.Extensions.Localization;

    public class CustomPasscodeGrantValidator : IExtensionGrantValidator
    {
        private readonly IHttpContextAccessor _httpContext;

        private IMediator Mediator
        {
            get;
        }

        private IStringLocalizer<SharedResource> SharedLocalizer
        {
            get;
        }

        public CustomPasscodeGrantValidator(
            IMediator mediator,
            IStringLocalizer<SharedResource> sharedLocalizer,
            IHttpContextAccessor httpContext
            )
        {
            Mediator = mediator;
            SharedLocalizer = sharedLocalizer;
            _httpContext = httpContext;
        }

        public string GrantType => "passcode";

        public async Task ValidateAsync(ExtensionGrantValidationContext context)
        {
            switch (context.Request.ClientId)
            {
                case "BNineClient":
                    await ValidateBNineClient(context, _httpContext.HttpContext!.RequestAborted);
                    return;
                default:
                    await SetInvalidResult(context);
                    return;
            }
        }

        private Task SetInvalidResult(ExtensionGrantValidationContext context)
        {
            context.Result = new GrantValidationResult(TokenRequestErrors.InvalidRequest, SharedLocalizer["invalidClientErrorMessage"].Value);

            return Task.FromResult(context.Result);
        }

        private async Task ValidateBNineClient(ExtensionGrantValidationContext context, CancellationToken cancellationToken)
        {
            var user = await Mediator.Send(new GetUserByPhoneQuery
            {
                PhoneNumber = context.Request.Raw["username"]
            }, cancellationToken);

            if (user == null || !CryptoHelper.VerifyHashedPassword(user.PasswordHash, context.Request.Raw["password"]))
            {
                context.Result = new GrantValidationResult(
                    TokenRequestErrors.InvalidRequest,
                    SharedLocalizer["incorrectCredentialsValidationErrorMessage"].Value);

                return;
            }

            var claims = new List<Claim>
            {
                new("status", user.Status.ToLowerCamelCase())
            };

            context.Result = new GrantValidationResult(user.Id.ToString(), "passcode", claims, "local");
        }
    }
}
