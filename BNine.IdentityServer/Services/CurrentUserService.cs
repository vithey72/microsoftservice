﻿namespace BNine.IdentityServer.Services
{
    using BNine.Application.Interfaces;
    using Microsoft.AspNetCore.Http;
    using System;
    using System.IdentityModel.Tokens.Jwt;
    using System.Linq;
    using System.Security.Claims;
    using Enums;

    public class CurrentUserService
        : ICurrentUserService
    {
        private readonly IHttpContextAccessor _httpContextAccessor;

        public CurrentUserService(IHttpContextAccessor httpContextAccessor)
        {
            _httpContextAccessor = httpContextAccessor;
        }

        public Guid? UserId
        {
            get
            {
                var stringId = _httpContextAccessor.HttpContext?.User?.FindFirstValue(ClaimTypes.NameIdentifier);

                return Guid.TryParse(stringId, out var guid) ? (Guid?)guid : null;
            }
        }

        public string? UserMbanqToken
        {
            get
            {
                var stringToken = _httpContextAccessor.HttpContext?.User?.FindFirstValue(ClaimTypes.Authentication);

                return (stringToken != null) ? stringToken : null;
            }
        }

        public Guid[] GroupIds
        {
            get
            {
                return new Guid[0];
            }
        }

        public JwtSecurityToken AccessToken
        {
            get
            {
                if(!_httpContextAccessor.HttpContext.Request.Headers.Any(x => x.Key == "Authorization"))
                {
                    return null;
                }

                var bearerJwtToken = _httpContextAccessor.HttpContext.Request.Headers.First(x => x.Key == "Authorization").Value.ToString().Split(" ")[1];

                var handler = new JwtSecurityTokenHandler();

                return handler.ReadToken(bearerJwtToken) as JwtSecurityToken;
            }
        }

        public MobilePlatform Platform
        {
            get;
        }
    }
}
