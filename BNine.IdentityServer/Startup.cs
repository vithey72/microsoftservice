﻿// Copyright (c) Brock Allen & Dominick Baier. All rights reserved.
// Licensed under the Apache License, Version 2.0. See LICENSE in the project root for license information.


namespace BNine
{
    using System.Reflection;
    using Application.Services;
    using BNine.Application.Aggregates.ServiceBusEvents.Commands.PublishB9Event;
    using BNine.Application.Interfaces;
    using BNine.IdentityServer;
    using BNine.IdentityServer.Extensions;
    using BNine.IdentityServer.Services;
    using BNine.MBanq.Client;
    using BNine.MBanq.Infrastructure;
    using BNine.Persistence;
    using BNine.ServiceBus;
    using IdentityServer4.Validation;
    using MediatR;
    using Microsoft.AspNetCore.Builder;
    using Microsoft.AspNetCore.Hosting;
    using Microsoft.Extensions.Configuration;
    using Microsoft.Extensions.DependencyInjection;
    using Microsoft.Extensions.Hosting;
    using Microsoft.Extensions.Options;

    public class Startup
    {
        public IWebHostEnvironment Environment
        {
            get;
        }
        public IConfiguration Configuration
        {
            get;
        }

        public Startup(IWebHostEnvironment environment, IConfiguration configuration)
        {
            Environment = environment;
            Configuration = configuration;
        }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddTransient<IRequestHandler<PublishB9EntityUpdateCommand, Unit>, PublishB9EntityUpdateCommandHandler>();
            services.AddAzureServiceBus(Configuration);
            services.AddAutoMapper(Assembly.GetAssembly(typeof(IBNineDbContext)));
            services.AddMediatr(Configuration);
            services.AddPersistence(Configuration);
            services.AddSettings(Configuration);
            services.AddControllersWithViews();
            services.AddLocalization(Configuration);
            services.ConfugureIdentityServer(Configuration, Environment);
            services.AddScoped<ICurrentUserService, CurrentUserService>();
            services.AddScoped<IPartnerProviderService, PartnerProviderService>();
            services.AddMBanqInfrastructure(Configuration);
            services.AddMBanqClient();
            services.AddHttpContextAccessor();
            services.AddTransient<IResourceOwnerPasswordValidator, CustomResourceOwnerPasswordValidator>();
            services.AddCors(Configuration);
        }

        public void Configure(IApplicationBuilder app)
        {
            if (Environment.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            // this custom middleware prevents redirects to https for openid-configuration endpoints
            app.Use(async (ctx, next) =>
            {
                ctx.Request.Scheme = "https";
                await next();
            });

            app.UseHsts();

            app.UseRequestLocalization(app.ApplicationServices.GetService<IOptions<RequestLocalizationOptions>>().Value);

            app.UseHttpsRedirection();
            app.UseStaticFiles();

            app.UseCors("BnineCorsPolicy");

            app.UseRouting();
            app.UseIdentityServer();
            app.UseAuthorization();
            app.UseEndpoints(endpoints => endpoints.MapDefaultControllerRoute());
        }
    }
}
