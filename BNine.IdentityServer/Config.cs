﻿namespace BNine
{
    using IdentityServer4;
    using IdentityServer4.Models;
    using System.Collections.Generic;
    using Microsoft.Extensions.Configuration;

    public static class Config
    {
        public static IEnumerable<IdentityResource> IdentityResources =>
            new IdentityResource[]
            {
                new IdentityResources.OpenId(),
                new IdentityResources.Profile(),
            };

        public static IEnumerable<ApiScope> ApiScopes =>
            new ApiScope[]
            {
                new ApiScope("BNineAPI", "BNine API")
            };

        public static IEnumerable<ApiResource> GetApiResources(IConfiguration configuration)
        {
            var resourceName = configuration["IdentityServerApiResources:Username"];
            var password = configuration["IdentityServerApiResources:Password"];
            if (string.IsNullOrEmpty(resourceName) || string.IsNullOrEmpty(password))
            {
                return System.Array.Empty<ApiResource>();
            }

            return new List<ApiResource>
            {
                new ApiResource(resourceName, "B9_Auth_Resource")
                {
                    ApiSecrets = new List<Secret>
                    {
                        new Secret(password.Sha256())
                    },
                    Scopes = new List<string>() { "BNineAPI" },
                }
            };
        }

        public static IEnumerable<Client> Clients(IConfiguration configuration)
        {
            var adminRedirectUris = new List<string> { $"{configuration["AdminAppSettings:BaseUri"]}/auth-callback", $"{configuration["AdminAppSettings:BaseUri"]}/assets/silent-refresh.html" };
            var adminPostLogoutRedirectUris = new List<string> { configuration["AdminAppSettings:BaseUri"] };
            var adminAllowedCorsOrigins = new List<string> { configuration["AdminAppSettings:BaseUri"] };

            if (bool.TryParse(configuration["IsDevelopmentCleintsAllowed"], out var IsDevelopmentCleintsAllowed))
            {
                if (IsDevelopmentCleintsAllowed)
                {
                    adminRedirectUris.Add("http://localhost:4200/auth-callback");
                    adminRedirectUris.Add("http://localhost:4200/assets/silent-refresh.html");
                    adminPostLogoutRedirectUris.Add("http://localhost:4200");
                    adminAllowedCorsOrigins.Add("http://localhost:4200");
                }
            }

            return new Client[]
           {
                new Client
                {
                    ClientId = "BNineClient",
                    AllowedGrantTypes = new List<string> { "password", "biometrical", "passcode"},
                    Description ="BNine client",
                    ClientSecrets =
                    {
                        new Secret("39c9b86d-da05-4636-9833-55d7214c6962".Sha256())
                    },

                    AccessTokenLifetime = 900,
                    AllowedScopes = new List<string>
                    {
                        IdentityServerConstants.StandardScopes.OpenId,
                        IdentityServerConstants.StandardScopes.Profile,
                        IdentityServerConstants.StandardScopes.Email,
                        "BNineAPI",
                        "offline_access"
                    },
                    AllowOfflineAccess = true,
                    AllowAccessTokensViaBrowser = true,
                    RequireConsent = false,
                    UpdateAccessTokenClaimsOnRefresh = true,
                    RefreshTokenExpiration = TokenExpiration.Sliding,
                    SlidingRefreshTokenLifetime = 20 * 300,
                    RefreshTokenUsage = TokenUsage.OneTimeOnly
                    //AbsoluteRefreshTokenLifetime = 600
                },
                new Client
                {
                    ClientId = configuration["AzureFunctionsClientId"],
                    AllowedGrantTypes = GrantTypes.ClientCredentials,
                    ClientSecrets = { new Secret(configuration["AzureFunctionsSecret"].Sha256()) },
                    RequireClientSecret = true,
                    AccessTokenLifetime = 180,
                    AllowedScopes = new List<string> {
                        "BNineAPI",
                    },
                },
                new Client
                {
                    ClientId = "BNineAdminUI",
                    AllowedGrantTypes = new List<string> { "password"},
                    Description ="BNine admin UI",
                    ClientSecrets =
                    {
                        new Secret("39c9b86d-da05-4636-9833-55d7214c6963".Sha256())
                    },
                    AccessTokenLifetime = 180,
                    AllowedScopes = new List<string>
                    {
                        IdentityServerConstants.StandardScopes.OpenId,
                        IdentityServerConstants.StandardScopes.Profile,
                        IdentityServerConstants.StandardScopes.Email,
                        "BNineAPI",
                        "offline_access"
                    },
                    AllowOfflineAccess = true,
                    AllowAccessTokensViaBrowser = true,
                    RequireConsent = false,
                    UpdateAccessTokenClaimsOnRefresh = true,
                    RefreshTokenExpiration = TokenExpiration.Sliding,
                    SlidingRefreshTokenLifetime = 20 * 60,
                    RefreshTokenUsage = TokenUsage.OneTimeOnly
                },
                new Client
                {
                    ClientId = "BNineAdmin",
                    ClientName = "BNineAdmin",
                    AllowedGrantTypes = GrantTypes.Code,
                    RequirePkce = true,
                    RequireClientSecret = false,
                    AccessTokenLifetime = 80,
                    RedirectUris = adminRedirectUris,
                    PostLogoutRedirectUris = adminPostLogoutRedirectUris,
                    AllowedCorsOrigins = adminAllowedCorsOrigins,
                    AllowedScopes = new List<string> {
                        IdentityServerConstants.StandardScopes.OpenId,
                        IdentityServerConstants.StandardScopes.Profile,
                        IdentityServerConstants.StandardScopes.Email,
                        "BNineAPI",
                        "offline_access"
                    },
                    EnableLocalLogin = false,
                    AllowAccessTokensViaBrowser = true
                },
           };
        }
    }
}
