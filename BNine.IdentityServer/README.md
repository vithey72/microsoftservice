# BNine Identity Server

- [BNine Identity Server](#bnine-identity-server)
  - [Overview](#overview)
  - [Launch application](#launch-application)
    - [Configure application](#configure-application)
      - [Configure secrets](#configure-secrets)
    - [Run application](#run-application)
      - [Visual Studio](#visual-studio)
      - [CLI](#cli)
  - [Docker](#docker)
    - [Build image](#build-image)
    - [Run container](#run-container)
  - [Tests](#tests)

## Overview

> Please note that all commands in this README should be executed from the current folder: `<repo folder>`.

## Launch application

### Configure application

Before running application, please configure mandatory options. An example of parameters can be found in the file `appsettings.json`.

> Please do not change this file if it is not related to changing (adding, removing) the functionality of the application.
> **Never store passwords or other sensitive data in this file and its descendants (appsettings.*.json).**

For development purpose, please create and configure mandatory options in `appsettings.Development.json`:

**TBD** Add appsettings

#### Configure secrets

During the development it's recommended to use [**The Secret Manager tool**](https://docs.microsoft.com/en-us/aspnet/core/security/app-secrets?view=aspnetcore-6.0&tabs=windows) to store sensitive data. [How to set secrets](https://docs.microsoft.com/en-us/aspnet/core/security/app-secrets?view=aspnetcore-6.0&tabs=windows#set-a-secret) for application.

> The Secret Manager tool doesn't encrypt the stored secrets and shouldn't be treated as a trusted store. It's **for development purposes only** - to avoid accidental committing of sensitive data to the git repository. The keys and values are stored in a JSON configuration file in the user profile directory.

Please configure the following options using the Secret Manager tool:

**TBD** Add app secrets

### Run application

You can observe launched application using the following URLs:

- [Application Url](https://localhost:5000)

#### Visual Studio

To run the application in Visual Studio, please do the following:

- Make sure to set `BNine.IdentityServer` as a `Startup project`;
- Select `BNine.IdentityServer` profile;
- Hit `F5` to run application.

#### CLI

To run the application via CLI, please execute the following command:

```bash
dotnet run --project BNine.IdentityServer/BNine.IdentityServer.csproj --launch-profile 'BNine.IdentityServer'
```

## Docker

### Build image

To build a Docker image, please do the following:

```bash
docker build -f BNine.IdentityServer/Dockerfile -t bnine/identity-server:latest .
```

### Run container

To run a container, please do the following:

```bash
docker run --name bnine-identity-server bnine/identity-server:latest
```

> Don't forget to pass environment variables.

## Health checks

**TBD** Add health checks

## Tests

**TBD** Add tests
