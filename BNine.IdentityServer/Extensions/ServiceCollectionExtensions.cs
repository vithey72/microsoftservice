﻿namespace BNine.IdentityServer.Extensions
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.Globalization;
    using System.Linq;
    using System.Security.Cryptography;
    using Application.Aggregates.AdminUser.Queries;
    using Application.Aggregates.Users.Queries.GetUserByPhone;
    using Azure;
    using Azure.Identity;
    using Azure.Security.KeyVault.Keys;
    using BNine.Application.Aggregates.Administrators.Commands.CreateAdministratorWithExternalIdentity;
    using BNine.Application.Aggregates.Administrators.Models;
    using BNine.Application.Aggregates.Administrators.Queries.GetAdministratorByExternalProvider;
    using BNine.Application.Aggregates.MbanqAuth.Queries.GetMbanqTokensByLoginAndPassword;
    using BNine.Application.Aggregates.MbanqAuth.Queries.GetMbanqTokensByRefreshToken;
    using BNine.Application.Aggregates.OTPCodes.Commands.DeactivateCode;
    using BNine.Application.Aggregates.OTPCodes.Models;
    using BNine.Application.Aggregates.OTPCodes.Queries.ValidateSmsCode;
    using BNine.Application.Aggregates.Users.Queries.GetUserById;
    using Application.Aggregates.Users.Queries.GetUserGroups;
    using BNine.Application.Behaviours;
    using BNine.Application.Models.Dto;
    using BNine.Application.Models.MBanq;
    using BNine.IdentityServer.Infrastructure.Services;
    using BNine.Settings;
    using IdentityServer4.Services;
    using MediatR;
    using MediatR.Registration;
    using Microsoft.AspNetCore.Builder;
    using Microsoft.AspNetCore.Hosting;
    using Microsoft.AspNetCore.Localization;
    using Microsoft.Extensions.Configuration;
    using Microsoft.Extensions.DependencyInjection;
    using Microsoft.IdentityModel.Logging;
    using Microsoft.IdentityModel.Tokens;
    using Microsoft.Extensions.Hosting;
    using Microsoft.AspNetCore.Identity;
    using IdentityServer4;
    using Microsoft.IdentityModel.Protocols.OpenIdConnect;
    using BNine.Domain.Entities.Features;
    using BNine.Application.Aggregates.Configuration.Queries.GetConfiguration;
    using BNine.Application.Aggregates.Configuration.Models;
    using AdminApiUser = Application.Models.Dto.AdminApiUser;

    internal static class ServiceCollectionExtensions
    {
        internal static IServiceCollection AddSettings(this IServiceCollection services, IConfiguration configuration)
        {
            services.ConfigureAndValidateApplicationSettings<MBanqSettings>(configuration);
            services.ConfigureAndValidateApplicationSettings<AlwaysEncryptedSettings>(configuration);

            return services;
        }

        internal static IServiceCollection ConfigureAndValidateApplicationSettings<T>(
            this IServiceCollection @this,
            IConfiguration configuration) where T : class, IApplicationSettings
            => @this.Configure<T>(configuration.GetSection(typeof(T).Name))
                    .PostConfigure<T>(settings =>
                    {
                        var configurationErrors = settings.ValidateApplicationSettings().ToArray();
                        if (configurationErrors.Any())
                        {
                            var aggregatedErrors = string.Join("\n", configurationErrors);
                            throw new ApplicationException(
                                $"Found {configurationErrors.Length} configuration error(s) in {typeof(T).Name}:\n{aggregatedErrors}");
                        }
                    });

        internal static IEnumerable<string> ValidateApplicationSettings(this IApplicationSettings @this)
        {
            var context = new ValidationContext(@this, serviceProvider: null, items: null);
            var results = new List<ValidationResult>();
            Validator.TryValidateObject(@this, context, results, true);
            foreach (var validationResult in results)
            {
                yield return validationResult.ErrorMessage;
            }
        }

        internal static IServiceCollection AddCors(this IServiceCollection services, IConfiguration configuration)
        {
            var allowerDomains = new List<string> { "https://*.bnine.com", configuration["AdminAppSettings:BaseUri"]};

            if (bool.TryParse(configuration["IsDevelopmentCleintsAllowed"], out var IsDevelopmentCleintsAllowed))
            {
                if (IsDevelopmentCleintsAllowed)
                {
                    allowerDomains.Add("http://localhost:4200");
                    allowerDomains.Add("http://localhost:5000");
                    allowerDomains.Add("https://localhost:5000");
                }
            }

            services.AddCors((options) =>
            {
                options.AddPolicy("BnineCorsPolicy",
                    builder => builder
                    .SetIsOriginAllowedToAllowWildcardSubdomains()
                    .WithOrigins(allowerDomains.ToArray())
                    .AllowAnyMethod()
                    .AllowCredentials()
                    .AllowAnyHeader()
                    .Build());
            });

            return services;
        }

        internal static IServiceCollection AddMediatr(this IServiceCollection services, IConfiguration configuration)
        {
            var serviceConfig = new MediatRServiceConfiguration();
            ServiceRegistrar.AddRequiredServices(services, serviceConfig);
            services.AddTransient(typeof(IPipelineBehavior<,>), typeof(ValidationBehaviour<,>));
            services.AddTransient(typeof(IPipelineBehavior<,>), typeof(UnhandledExceptionBehaviour<,>));

            //register mediator handlers manually here
            services.AddTransient<IRequestHandler<GetConfigurationQuery, ConfigurationResponseModel>, GetConfigurationQueryHandler>();
            services.AddTransient<IRequestHandler<GetUserGroupsQuery, IEnumerable<Group>>, GetUserGroupsQueryHandler>();
            services.AddTransient<IRequestHandler<GetUserByPhoneQuery, User>, GetUserByPhoneQueryHandler>();
            services.AddTransient<IRequestHandler<DeactivateCodeCommand, Unit>, DeactivateCodeCommandHandler>();
            services.AddTransient<IRequestHandler<ValidateSmsCodeQuery, Code>, ValidateSmsCodeQueryHandler>();
            services.AddTransient<IRequestHandler<GetMbanqTokensByLoginAndPasswordQuery, AuthorizationTokensResponseModel>, GetMbanqTokensByLoginAndPasswordQueryHandler>();
            services.AddTransient<IRequestHandler<GetMbanqTokensByRefreshTokenQuery, AuthorizationTokensResponseModel>, GetMbanqTokensByRefreshTokenQueryHandler>();
            services.AddTransient<IRequestHandler<GetUserByIdQuery, User>, GetUserByIdQueryHandler>();
            services.AddTransient<IRequestHandler<CreateAdministratorWithExternalIdentityCommand, AdministratorDetailsModel>, CreateAdministratorWithExternalIdentityCommandHandler>();
            services.AddTransient<IRequestHandler<GetAdministratorByExternalProviderQuery, AdministratorDetailsModel>, GetAdministratorByExternalProviderQueryHandler>();

            services.AddTransient<IRequestHandler<GetAdminApiUserByIdQuery, AdminApiUser>, GetAdminApiUserByIdQueryHandler>();
            services.AddTransient<IRequestHandler<GetAdminApiUserByEmailQuery, AdminApiUser>, GetAdminApiUserByEmailHandler>();

            return services;
        }

        internal static IServiceCollection AddLocalization(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddLocalization(options =>
            {
                options.ResourcesPath = "Resources";
            });

            services.Configure<RequestLocalizationOptions>(
                options =>
                {
                    options.SupportedCultures = new List<CultureInfo>
                    {
                        new CultureInfo("en"),
                        new CultureInfo("es")
                    };

                    options.DefaultRequestCulture = new RequestCulture(culture: "en", uiCulture: "en");
                    options.SupportedUICultures = options.SupportedCultures;

                    options.RequestCultureProviders = new List<IRequestCultureProvider>
                    {
                       new AcceptLanguageHeaderRequestCultureProvider()
                    };
                });

            return services;
        }

        internal static IServiceCollection ConfugureIdentityServer(this IServiceCollection services, IConfiguration configuration, IWebHostEnvironment environment)
        {
            IdentityModelEventSource.ShowPII = true;

            var builder = services.AddIdentityServer();
            builder.AddInMemoryIdentityResources(Config.IdentityResources);
            builder.AddInMemoryApiScopes(Config.ApiScopes);
            builder.AddInMemoryApiResources(Config.GetApiResources(configuration));
            builder.AddInMemoryClients(Config.Clients(configuration));
            builder.AddExtensionGrantValidator<CustomBiometricalGrantValidator>();
            builder.AddExtensionGrantValidator<CustomPasscodeGrantValidator>();

            services.AddTransient<IProfileService, ProfileService>();
            if (environment.IsProduction())
            {
                // TODO: move to extension

                var keyClient = new KeyClient(new Uri(configuration["KEYVAULT_ENDPOINT"]), new DefaultAzureCredential());
                Response<KeyVaultKey> response = keyClient.GetKey("IdentityServerSettings--SigningKey");

                AsymmetricSecurityKey key;
                string algorithm;

                if (response.Value.KeyType == KeyType.Ec)
                {
                    ECDsa ecDsa = response.Value.Key.ToECDsa();
                    key = new ECDsaSecurityKey(ecDsa) { KeyId = response.Value.Properties.Version };

                    // parse from curve
                    if (response.Value.Key.CurveName == KeyCurveName.P256)
                    {
                        algorithm = "ES256";
                    }
                    else if (response.Value.Key.CurveName == KeyCurveName.P384)
                    {
                        algorithm = "ES384";
                    }
                    else if (response.Value.Key.CurveName == KeyCurveName.P521)
                    {
                        algorithm = "ES521";
                    }
                    else
                    {
                        throw new NotSupportedException();
                    }
                }
                else if (response.Value.KeyType == KeyType.Rsa)
                {
                    RSA rsa = response.Value.Key.ToRSA();
                    key = new RsaSecurityKey(rsa) { KeyId = response.Value.Properties.Version };
                    algorithm = "PS256";
                }
                else
                {
                    throw new NotSupportedException();
                }

                builder.AddSigningCredential(key, algorithm);
                services.AddTransient<ITokenCreationService, KeyVaultTokenCreationService>();
            }
            else
            {
                builder.AddDeveloperSigningCredential();
            }

            services.AddAuthentication()
                .AddCookie(IdentityConstants.ApplicationScheme, o =>
                {
                    o.Cookie.Name = IdentityConstants.ApplicationScheme;
                    o.ExpireTimeSpan = TimeSpan.FromMinutes(5);
                })
                .AddOpenIdConnect("aad", "Azure AD", options =>
                {
                    options.SignInScheme = IdentityServerConstants.ExternalCookieAuthenticationScheme;
                    options.SignOutScheme = IdentityServerConstants.SignoutScheme;

                    options.Authority = $"https://login.windows.net/{configuration["AzureActiveDirectorySettings:TenantId"]}";
                    options.ClientId = configuration["AzureActiveDirectorySettings:ClientId"];
                    options.ResponseType = OpenIdConnectResponseType.IdToken;
                    options.CallbackPath = "/signin-aad";
                    options.SignedOutCallbackPath = "/signout-callback-aad";
                    options.RemoteSignOutPath = "/signout-aad";
                    options.Scope.Add("roles");
                    options.TokenValidationParameters = new TokenValidationParameters
                    {
                        NameClaimType = "name",
                        RoleClaimType = "roles"
                    };
                });

            return services;
        }
    }
}
