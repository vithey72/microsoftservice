﻿using IdentityModel;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace BNine.IdentityServer.Controllers.User;

[Authorize]
[Route("api/[controller]")]
[ApiController]
public class UsersController : ControllerBase
{
    /// <summary>
    /// Get user full name from claims
    /// Use cookies from the request
    /// </summary>
    /// <returns></returns>
    [HttpGet]
    public IActionResult GetCurrentUserName()
    {
        var userNameClaim = User.FindFirst(JwtClaimTypes.Name);

        if (userNameClaim.Value is null)
        {
            return NotFound();
        }

        return Ok(userNameClaim.Value);
    }

}
