﻿namespace BNine.Constants;

public static class DeepLinkRoutes
{
    public static string None = "none";

    public static string Wallet = "wallet";

    public static string Cards = "cards";

    public static string TransfersHistory = "transactions-history";

    public static string Support = "support";

    public static string Profile = "profile";

    /// <summary>
    /// Connection to Argyle screen (online option).
    /// </summary>
    public static string Argyle = "argyle";

    /// <summary>
    /// Manual payroll connection screen.
    /// </summary>
    public static string EarlySalaryManualForm = "early-salary-manual-form";

    /// <summary>
    /// Flow of switching payroll to B9.
    /// </summary>
    public static string SwitchDeposit = "switch_deposit_page";

    /// <summary>
    /// Flow of disconnecting payroll.
    /// </summary>
    public static string DisconnectPayroll = "disconnect_payroll";

    public static string Credit = "credit";

    public static string Bonus = "bonus";

    public static string PullTransaction = "pull_transaction";

    public static string PushTransaction = "push_transaction";

    public static readonly string AchTransfer = "ach-transfer";

    public static readonly string InternalTransfer = "internal-transfer";

    public static readonly string WalletInfo = "wallet-info";

    public static string ManualSurvey = "manual_survey";

    public static string PremiumTariffPlan = "premium-tariff-plan";

    public static string AdvanceTariffPlan = "advance-tariff-plan";

    /// <summary>
    /// Advance calculator screen.
    /// </summary>
    public static string B9Score = "b9-score";

    public static string CreditScore = "credit-score";

    public static string CashbackProgram = "cashback-program";

    [Obsolete("We have better instruments for that now.")]
    public static string ExternalLink = "external-link";

    /// <summary>
    /// April tax platform.
    /// </summary>
    public static string April = "tax-service-info";

    /// <summary>
    /// Quickbooks direct deposit info.
    /// </summary>
    public static string QuickBooksInfo = "quickbooks-info";

    public static string QuickBooksDialog = "quickbooks-dialog";
    /// <summary>
    /// How It Works flow
    /// </summary>
    public static string HowItWorksVirtualCard = "how-it-works-virtual-card";

    /// <summary>
    /// Beginning of the flow of marking transactions as income (not the list of transactions itself).
    /// </summary>
    public static string MarkPotentialIncome = "mark-transaction-info";

    /// <summary>
    /// Dialog that prompts user giving us a rating in Store/Play.
    /// </summary>
    public static string RateUsDialog = "rate-us-proposal";

    /// <summary>
    /// Dialog for taking an Advance Extension deal.
    /// </summary>
    public static string AdvanceExtension = "advance-extension";

    /// <summary>
    /// Dialog for repayment of advance
    /// </summary>
    public static string AdvanceRepayment = "advance-repay";

    /// <summary>
    /// Dialog for taking an advance.
    /// </summary>
    public static string AdvanceDisbursal = "advance-disbursal";

    public static readonly string ImmediateBoost = "immediate-boost";

    /// <summary>
    /// "Wait for your first paycheck" dialog.
    /// </summary>
    public static string ExpectingFirstPayrollDialog = "waiting-for-first-paycheck-dialog";

    /// <summary>
    /// "Wait for your first paycheck" dialog.
    /// </summary>
    public static string InsufficientDepositAmountDialog = "insufficient-deposit-amount-dialog";

    /// <summary>
    /// Opens a quiz screen. Which questions there are depends on deeplink arguments.
    /// </summary>
    public static string UserQuestionnaire = "user-questionnaire";

    /// <summary>
    /// Where we go when trying to motivate new user to connect Argyle.
    /// </summary>
    public static string WalletAdvanceDescription = "wallet-advance-description";

    /// <summary>
    /// Screen representing employment status and related products for the client.
    /// </summary>
    public static string IncomeAndTaxes = "employment-mode";

    /// <summary>
    /// Leads to the dialog for creating a bank statement for a selected period.
    /// </summary>
    public static string AccountStatement = "account-statement";

    public static string NotificationCenter = "notification-center";

    /// <summary>
    /// https://bninecom.atlassian.net/browse/B9-4883
    /// </summary>
    public static string ThanksForYourInterestAlert = "thanks-for-your-interest-alert";

    /// <summary>
    /// https://bninecom.atlassian.net/browse/B9-4883
    /// </summary>
    public static string UnemploymentProtectionMarketResearch = "unemployment-protection-market-research";

    public static string ReplenishmentMethods = "replenish-methods";

    public static string SendMoney = "send-money";

    /// <summary>
    /// The page with the closure checklist.
    /// </summary>
    public static string AccountClosureChecklist = "account-closure-info";

    public static string PremiumAfterAdvanceOffer = "premium-after-advance-offer";

    public static string RewardsRedeem = "rewards-redeem";

    public static string RewardsActivation = "rewards-activation";
}
