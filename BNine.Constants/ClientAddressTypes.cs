﻿namespace BNine.Constants
{
    public static class ClientAddressTypes
    {
        public static readonly string Business = "BUSINESS";

        public static readonly string Postal = "POSTAL";

        public static readonly string Tax = "TAX";

        public static readonly string Primary = "PRIMARY";
    }
}
