﻿// ReSharper disable InconsistentNaming
namespace BNine.Constants
{
    /// <summary>
    /// B9 domain specific transaction types.
    /// </summary>
    public static class TransactionTypes
    {
        public const string EXTERNAL_CARD = "EXTERNALCARD";
        public const string PAY_CHARGE = "Pay Charge";
        public const string CARD_AUTHORIZE_PAYMENT = "CARD_AUTHORIZE_PAYMENT";
        public const string DOMESTIC_ATM_WITHDRAWAL_FEE = "DOMESTIC_ATM_WITHDRAWAL_FEE";
        public const string INTERNATIONAL_ATM_WITHDRAWAL_FEE = "INTERNATIONAL_ATM_WITHDRAWAL_FEE";
        public const string INTERNATIONAL_TRANSACTION_FEE = "INTERNATIONAL_TRANSACTION_FEE";
        public const string SETTLEMENT_RETURN_CREDIT = "SETTLEMENT_RETURN_CREDIT";
        public const string REJECTED = "Rejected";
        public const string ACH = "ACH";
        public const string ATM = "ATM";
        public const string CASH = "CASH";
        public const string INTERNAL = "INTERNAL";
        public const string VIP_SUPPORT = "Premium support fee";
        public const string ADVANCE_EXTENSION_FEE = "Advance unfreeze fee";
        public const string ADVANCE_BOOST_FEE = "Advance Boost fee";
        public const string PHYSICAL_CARD_REPLACEMENT_FEE = "Physical card replacement fee";
        public const string PHYSICAL_CARD_EMBOSSING_FEE = "Physical card embossing fee";
        public const string AFTER_ADVANCE_TIP = "After Advance Tip";

        public const string MONEY_TRANSFER = "MONEY_TRANSFER";
        public const string ADVANCE = "ADVANCE";
        public const string REPAYMENT = "REPAYMENT";
        public const string B9_BONUS = "B9 BONUS";
        public const string B9_CARD = "B9 Card";
        public const string TOP_UP_WALLET = "Top-up Wallet";
        public const string PUSH_TO_EXTERNAL_CARD = "PUSH_TO_EXTERNAL_CARD";
        public const string REVERSED_TRANSACTION = "REVERSED_TRANSACTION";
        public const string REVERSED_PAYCHARGE = "REVERSED_PAYCHARGE";
        public const string REVERSED = "REVERSED";

        public const string CREDIT_SCORE_FEE = "Credit Score fee";

        public const string EXTERNAL_CARD_PULL_TRANSACTION_FEE = "EXTERNAL_CARD_PULL_TRANSACTION_FEE";
        public const string EXTERNAL_CARD_PUSH_TRANSACTION_FEE = "EXTERNAL_CARD_PUSH_TRANSACTION_FEE";
        public const string MCC_FEE = "MCC_FEE";
    }
}
