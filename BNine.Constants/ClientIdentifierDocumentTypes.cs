﻿namespace BNine.Constants;

public static class ClientIdentifierDocumentTypes
{
    public static readonly string Passport = "Passport";
    public static readonly string ForeignDriverLicense = "ForeignDriverLicense";
    public static readonly string ConsularId = "ConsularId";
    public static readonly string Visa = "Visa";
    public static readonly string Other = "Other";
    public static readonly string SSN = "SSN";
    public static readonly string DriverLicense = "DriverLicense";
    public static readonly string StateIdentityCard = "StateIdentityCard";
    public static readonly string WorkPermitCard = "WorkPermitCard";
    public static readonly string PermanentResidentCard = "PermanentResidentCard";
    public static readonly string ForeignPassport = "ForeignPassport";
    public static readonly string ITIN = "ITIN";
}
