﻿namespace BNine.Constants
{
    public static class PushNotificationCategory
    {
        public static string CardCharge { get; } = "Card Charge";
        public static string CardDeclined { get; } = "Card Declined";
        public static string AccountCharge { get; } = "Account Charge";
        public static string AdvanceActivated { get; } = "Advance Activated";
        public static string AdvanceUpdated { get; } = "Advance Updated";
        public static string PaydayHasComeEarly { get; } = "Payday has come early!";
        public static string AdvanceRepaid { get; } = "Advance repaid";
        public static string MoneyReceived { get; } = "Money received";
        public static string PaymentStarted { get; } = "Payment started";
        public static string ActivateCard { get; } = "Activate Card";
        public static string ReturnedPayment { get; } = "Return of Transaction";

        public static string PinWasChangedSuccessfully { get; } = "PIN was changed successfully";

        public static string YourB9CardWasActivated { get; } = "B9 card was activated";

        public static string YourB9CardHasBeenBlocked { get; } = "B9 Card Has Been Blocked";

        public static string AddressAssociatedWithB9AccountWasChanged { get; } = "Address associated with B9 account was changed";

        public static string YourB9PasswordWasChanged { get; } = "B9 password was changed";

        public static string YourB9CardHasBeenSuccessfullyReissued { get; } = "B9 card has been successfully reissued";

        public static string YourEmailAssociatedWithB9WasChanged { get; } = "Email associated with B9 was changed";

        public static string YourPhoneAssociatedWithB9AccountWasChanged { get; } = "Phone associated with B9 account was changed";

        public static string YourB9CardSuccessfullyUnlocked { get; } = "B9 Card successfully unlocked";

        public static string ATMWithdrawal { get; } = "ATM withdrawal";
    }
}
