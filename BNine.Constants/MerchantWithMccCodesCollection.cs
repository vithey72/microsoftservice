﻿namespace BNine.Constants
{
    using System.Collections.Generic;
    public record MerchantWithMccCodesCollection(List<int> Codes, string Description, string IconUrl);
}
