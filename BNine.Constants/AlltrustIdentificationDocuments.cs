﻿namespace BNine.Constants
{
    public class AlltrustIdentificationDocuments
    {
        public static readonly string Passport = "Passport";
        public static readonly string DriverLicense = "Driver's License";
        public static readonly string IdCard = "ID Card";
        public static readonly string ConsularCard = "Consular Card";
        public static readonly string NationalId = "National ID";
        public static readonly string ForeignDriversLicense = "Foreign DL";
        public static readonly string Other = "Other";
    }
}
