﻿namespace BNine.Constants
{
    public static class HttpClientName
    {
        public static readonly string MBanqAuthorized = "MBanqAuthorized";

        public static readonly string MBanqSelfService = "MBanqSelfService";

        public static readonly string DocsAlloy = "DocsAlloy";

        public static readonly string Plaid = "Plaid";

        public static readonly string Argyle = "Argyle";

        public static readonly string Truv = "Truv";

        public static readonly string Zendesk = "Zendesk";

        public static readonly string Alltrust = "Alltrust";

        public static readonly string AppsflyerS2S = "AppsflyerS2S";

        public static readonly string Array = "Array";

        public static readonly string DwhIntegration = "DwhIntegration";

        public static readonly string Socure = "Socure";
        public static readonly string SocureDocuments = "SocureDocuments";

        public static readonly string April = "April";
    }
}
