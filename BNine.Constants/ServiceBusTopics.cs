﻿namespace BNine.Constants
{
    public static class ServiceBusTopics
    {
        public const string B9LoanCalculatedLimits = "b9.loan-calculated-limits";
        public const string B9EarlySalaryWidgetConfigurations = "b9.early-salary-widget-configurations";
        public const string B9KYCVerifications = "b9.kyc-verifications";
        public const string B9DeviceDetails = "b9.device-details";
        public const string B9BonusCodes = "b9.bonus-codes";
        public const string B9BonusCodeActivations = "b9.bonus-code-activations";
        public const string B9AppsflyerId = "b9.appsflyer-id";
        public const string B9LoanSettings = "b9.loan-settings";
        public const string B9User = "b9.user";
        public const string B9UserV2 = "b9.user.v2";
        public const string B9SendGrid = "b9.sendgrid-events";
        public const string B9UsedPaidServices = "b9.used-paid-services";
        public const string B9AdvanceBoost = "b9.advance-boost";
        public const string B9GatewayPrefix = "b9.";

        public const string MbanqCards = "mbanq.cards";
        public const string MbanqTransfers = "mbanq.transfers";
        public const string MbanqCardSettlements = "mbanq.card-settlements";

        public const string Argyle = "argyle";
        public const string ArgyleUI = "argyle-ui";
        public const string TruvUI = "truv-ui";

        public const string ItUserEvents = "it.user-events";
        public const string ItCashbackAmount = "it.cashback-amount";
        public const string ItAdvanceStats = "it.advance";
        public const string ItAdvanceWithDelayedBoostFailed = "it.advance-with-delayed-boost-failed";
        public const string ItUserStats = "it.user-stats";
    }
}
