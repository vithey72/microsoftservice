﻿namespace BNine.Constants
{
    public static class UiEventSource
    {
        public static class Screens
        {
            public static string Argyle = "argyle";
            public static string Wallet = "wallet";
            public static string Questionnaire = "questionnaire";
            public static string ManualForm = "manual-form";
            public static string GetAdvance = "get-advance";
        }
        public static class Actions
        {
            public static string Click = "click";
            public static string Show = "show";
            public static string Complete = "complete";
        }
    }
}
