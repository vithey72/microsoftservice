﻿namespace BNine.Constants
{
    public static class ExternalCardsLimits
    {
        public static decimal TransactionLimit { get; } = 500;

        public static decimal DailyLimit { get; } = 2500;

        public static decimal MonthlyLimit { get; } = 10000;
    }
}
