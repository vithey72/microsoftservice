﻿namespace BNine.Constants
{
    public static class ContainerNames
    {
        public static readonly string BnineContainer = "bnine-container";
        public static readonly string SharedContainer = "static-documents-container";
        public static readonly string SuccessfulChecks = "successful-checks";
        public static readonly string FailedChecks = "failed-checks";
        public static readonly string ClientDocumentsContainer = "client-documents-container";
        public static readonly string ClientSelfieContainer = "client-selfie-container";
        public static readonly string ClientSelfieContainerWithType = "client-selfie-container-with-type";
    }
}
