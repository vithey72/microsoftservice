﻿namespace BNine.Constants;

using System.Globalization;

public static class DateFormat
{
    public static readonly string Default = "dd MM yyyy";

    public static readonly string Full = "dd MM yyyy HH:mm:ss";

    public static readonly string MonthFirst = "MMM dd, yyyy";

    public static readonly string MBanqFormat = "dd MMMM yyyy";

    public static readonly string SocureDate = "yyyy-MM-dd";

    public static readonly string DwhDateFormat = "yyyy-MM-dd";

    public static string ToUniversalIso8601(this DateTime dateTime)
    {
        return dateTime.ToUniversalTime().ToString("u").Replace(" ", "T");
    }

    public static CultureInfo GetEnCulture()
    {
        return new CultureInfo("en-US");
    }

    public static DateTime GetEstNow()
    {
        var timeUtc = DateTime.UtcNow;
        TimeZoneInfo easternZone = TimeZoneInfo.FindSystemTimeZoneById("Eastern Standard Time");
        return TimeZoneInfo.ConvertTimeFromUtc(timeUtc, easternZone);
    }
}
