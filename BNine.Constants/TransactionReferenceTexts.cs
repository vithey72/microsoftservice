﻿namespace BNine.Constants;

public static class TransactionReferenceTexts
{
    /// <summary>
    /// A type of reference note used by mbanq in format: "TRANSFER. B9 Organization REF: Cashback Incenti"
    /// </summary>
    public const string CashbackReferenceText = "Cashback Incenti";

    public const string ReferralBonusText = "Incentive bonus";

    public const string ReferralReferenceText = "Refferal Incenti";
}
