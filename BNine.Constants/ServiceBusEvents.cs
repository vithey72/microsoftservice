﻿namespace BNine.Constants
{
    public static class ServiceBusEvents
    {
        public static readonly string B9Created = "created";
        public static readonly string B9Activated = "activated";
        public static readonly string B9Deleted = "deleted";
        public static readonly string B9Updated = "updated";
        public static readonly string B9Blocked = "blocked";
        public static readonly string B9Unblocked = "unblocked";
        public static readonly string B9UserSettingsUpdated = "settings-updated";
        public static readonly string B9UserIdentityCreated = "identity-created";
        public static readonly string B9UserIdentityUpdated = "identity-updated";
        public static readonly string B9UserAddressCreated = "address-created";
        public static readonly string B9UserAddressUpdated = "address-updated";
        public static readonly string B9UserDocumentsCreated = "documents-created";
        public static readonly string B9UserDocumentsUpdated = "documents-updated";
        public static readonly string B9UserClientRatingAndLoanCreated = "client-rating-and-loan-created";
        public static readonly string B9UserClientRatingAndLoanUpdated = "client-rating-and-loan-updated";
        public static readonly string B9EarlySalarySettingsCreated = "early-salary-settings-created";
        public static readonly string B9SendGridEvent = "sendgrid-event";
        public static readonly string B9CashbackMonth = "cashback-month";
        public static readonly string B9AdvanceBoostActivated = "boost-activated";
        public static readonly string B9AdvanceBoostActivatedSchedule = "boost-activated-schedule";
        public static readonly string B9AdvanceBoostDeactivatedSchedule = "boost-deactivated-schedule";
        public static readonly string B9AdvanceBoostAutoActivationFailed = "advance-boost-auto-activation-failed";

        public static readonly string MBanqTransferCreatedEvent = "mbanq.transfers.created";
        public static readonly string MBanqCardAuthorizationEvent = "mbanq.cards.card-authorization";
        public static readonly string MBanqCardSettlementCreatedEvent = "mbanq.cards.card-settlement.created";
        public static readonly string MBanqCardFulfillmentStatusEvent = "mbanq.cards.card-fulfillment";

        public static readonly string ArgyleEvent = "argyle";
        public static readonly string ArgyleUIEvent = "argyle-ui";
        public static readonly string TruvUIEvent = "truv-ui";

        public static readonly string UsedPaidServiceEventPrefix = "used-paid-services.";
        public static readonly string UserPaidServicePurchasePostfix = "-paid";
        public static readonly string UserPaidServiceActivationPostfix = "-activated";

    }
}
