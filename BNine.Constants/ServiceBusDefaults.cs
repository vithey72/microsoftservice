﻿namespace BNine.Constants
{
    public static class ServiceBusDefaults
    {
        public static readonly string UpdatedFieldsAdditionalProperty = "updatedFields";
    }
}
