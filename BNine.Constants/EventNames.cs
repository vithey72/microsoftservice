﻿namespace BNine.Constants;

public static class EventNames
{
    public static class UserAccountClosure
    {
        public static string CloseAccountButton = "close-account-button";
        public static string GoBackButton = "back-button";
    }

    public static class Menu
    {
        public static string WalletTile = "wallet";
        public static string CardsTile = "cards";
        public static string AdvanceTile = "advance";
        public static string CashbackTile = "cashback";
        public static string HistoryTile = "history";
        public static string IncomeTile = "income";
        public static string SupportTile = "support";
        public static string ProfileTile = "profile";
        public static string NotificationsTile = "notifications";
        public static string TariffsTile = "tariffs";
        public static string AccountStatementTile = "statement";
        public static string SwitchDepositTile = "argyle";

    }
}
