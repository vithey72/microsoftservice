﻿namespace BNine.Constants;


public static class MccCodes
{
    public static readonly MerchantWithMccCodesCollection[] AllCodeEntitiesArray = new[]
    {
        new MerchantWithMccCodesCollection(new List<int> { 6011}, "ATMs",
            "https://b9prodaccount.blob.core.windows.net/static-documents-container/MccIcons/atms.png"),
        new MerchantWithMccCodesCollection(new List<int> { 5814}, "Fast food",
            "https://b9prodaccount.blob.core.windows.net/static-documents-container/MccIcons/fast_food.png"),
        new MerchantWithMccCodesCollection(new List<int> { 6012}, "Financial Institutions - Merchandise and Services",
            "https://b9prodaccount.blob.core.windows.net/static-documents-container/MccIcons/financial_institutions.png"),
        new MerchantWithMccCodesCollection(new List<int> { 6051}, "Funding of electronic, crypto wallets",
            "https://b9prodaccount.blob.core.windows.net/static-documents-container/MccIcons/funding_of_electronic_crypto_wallets.png"),
        new MerchantWithMccCodesCollection(new List<int> { 4829}, "Money transfers",
            "https://b9prodaccount.blob.core.windows.net/static-documents-container/MccIcons/money_transfers.png"),
        new MerchantWithMccCodesCollection(new List<int> { 5310}, "Sales of a variety of goods at discounted prices",
            "https://b9prodaccount.blob.core.windows.net/static-documents-container/MccIcons/sales_of_variety_of_goods.png"),
        new MerchantWithMccCodesCollection(new List<int> { 5399}, "Small retail outlets and medium-sized stores",
            "https://b9prodaccount.blob.core.windows.net/static-documents-container/MccIcons/small_retail_outlets.png"),
        new MerchantWithMccCodesCollection(new List<int> { 4121}, "Taxi",
            "https://b9prodaccount.blob.core.windows.net/static-documents-container/MccIcons/taxis.png"),
        new MerchantWithMccCodesCollection(new List<int> { 4814}, "Telecommunications services",
            "https://b9prodaccount.blob.core.windows.net/static-documents-container/MccIcons/telecommunication_services.png"),
        new MerchantWithMccCodesCollection(new List<int> { 5999}, "Various stores and specialty retailers",
            "https://b9prodaccount.blob.core.windows.net/static-documents-container/MccIcons/various_stores_and_retailers.png"),
        new MerchantWithMccCodesCollection(new List<int> { 5977, 7230, 7297, 7298 }, "Beauty",
            "https://b9prodaccount.blob.core.windows.net/static-documents-container/MccIcons/beauty.png"),
        new MerchantWithMccCodesCollection(new List<int> { 5297, 5298, 5300, 5411, 5412, 5422, 5441, 5451, 5462, 5499, 5715, 5921 }, "Supermarkets / Online-stores",
            "https://b9prodaccount.blob.core.windows.net/static-documents-container/MccIcons/supermarkets_online_stores_cart_178_178.png"),
        new MerchantWithMccCodesCollection(new List<int> {  2741, 5111, 5192, 5942, 5994 }, "Book stores",
            "https://b9prodaccount.blob.core.windows.net/static-documents-container/MccIcons/book_stores.png"),
        new MerchantWithMccCodesCollection(new List<int> {  5816, 5818 }, "Digital Goods",
            "https://b9prodaccount.blob.core.windows.net/static-documents-container/MccIcons/digital_goods.png"),
        new MerchantWithMccCodesCollection(new List<int> { 7911, 7922, 7929, 7932, 7933, 7941, 7991, 7992, 7993, 7994, 7996, 7997, 7998, 7999, 8664 }, "Entertainment",
            "https://b9prodaccount.blob.core.windows.net/static-documents-container/MccIcons/entertainment.png"),
        new MerchantWithMccCodesCollection(new List<int> { 5172, 5541, 5542, 5983 }, "Gas stations",
            "https://b9prodaccount.blob.core.windows.net/static-documents-container/MccIcons/gas_stations.png"),
        new MerchantWithMccCodesCollection(new List<int> {  7829, 7832, 7841 }, "Movies",
            "https://b9prodaccount.blob.core.windows.net/static-documents-container/MccIcons/movies.png"),
        new MerchantWithMccCodesCollection(new List<int> { 5122, 5292, 5295, 5912 }, "Pharmacies",
            "https://b9prodaccount.blob.core.windows.net/static-documents-container/MccIcons/pharmacies.png"),
        new MerchantWithMccCodesCollection(new List<int> { 5811, 5812, 5813 }, "Restaurants",
            "https://b9prodaccount.blob.core.windows.net/static-documents-container/MccIcons/restaurants.png"),
    };
}

