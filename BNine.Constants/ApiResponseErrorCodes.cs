﻿namespace BNine.Constants
{
    public class ApiResponseErrorCodes
    {
        public const string InsufficientFunds = "error.insufficient-funds";
        public const string OnetimeTranLimitExceeded = "error.onetime-tranlimit-exceeded";
        public const string DailyAmountLimitExceeded = "error.daily-amountlimit-exceeded";
        public const string DailyCountLimitExceeded = "error.daily-countlimit-exceeded";
        public const string WeeklyAmountLimitExceeded = "error.weekly-amountlimit-exceeded";
        public const string WeeklyCountLimitExceeded = "error.weekly-countlimit-exceeded";
        public const string MonthlyAmountLimitExceeded = "error.monthly-amountlimit-exceeded";
        public const string MonthlyCountLimitExceeded = "error.monthly-countlimit-exceeded";
        public const string OnetimeMaxAmountExceeded = "error.onetime-maxamount-exceeded";
        public const string OnetimeMinAmountExceeded = "error.onetime-minamount-exceeded";
        public const string ErrorServer = "error.server";
        public const string ErrorCardVendor = "error.card-vendor";
        public const string ErrorValidation = "error.validation";
        public const string ErrorAccountBlocked = "error.account-blocked";
        public const string ExternalcardCreditTransfer = "error.externalcard-credit-transfer";
        public const string OverdueTariffPayment = "error.overdue-tariff-payment";
        public const string WrongTariffPlan = "error.wrong-tariff-plan";
        public const string WrongMarketingThirdParty = "error.wrong-marketing-third-party";
        public const string WrongHowItWorksScreenType = "error.wrong-howitworks-screen-type";
        public const string WrongTransferCommonInfoScreenType = "error.wrong-transfer-common-info-screen-type";
        public const string Last4Digits = "error.last-4-digits";
        public const string PasswordHasBeenUsed = "error.password-already-used";
        public const string UpdateAddressDisabled = "error.update-address-disabled";
        public const string ExternalCardWasRemoved = "error.external-card-was-removed";
        public const string NeedToRetry = "error.need-to-retry";
        public const string DuplicateKey = "error.duplicate-key";
        public const string TooManyTries = "error.too-many-tries";
        public const string OtpFailed = "error.otp-failed";
        public const string AccountBalance = "error.account-balance";
        public const string MBanqValidation = "error.mbanq-validation";
        public const string VirtualCardActivation = "error.virtual-card.activation";
        public const string VirtualCardReplacementLimit = "error.virtual-card.replacement-limit";
    }
}
