﻿namespace BNine.Constants
{
    public static class ErrorCodes
    {
        public static class User
        {
            public static string ActiveUserWithProvidedPhoneAlreadyExists = "ErrCode_PhoneNumber_ActiveUserAlreadyExists";
            public static string InactiveUserWithProvidedPhoneAlreadyExists = "ErrCode_PhoneNumber_InactiveUserAlreadyExists";
        }
    }
}
