﻿namespace BNine.Constants
{
    public class UserEventNames
    {
        public const string ArgyleBannerSeen = "argyle.pay-distribution-banner.displayed";
        public const string ReferralBannerClosed = "banner.cross-button.click";
        public const string ReferralBannerButtonClicked = "banner.button.click";
        public const string UserQuizPassedPrefix = "questionnaire.quiz-completed.";
        public const string CashbackOnboardingSlideshowSeen = "cashback.onboarding-slideshow.displayed";

        public static class ManualForm
        {
            public static string Prefix = "manual-form-survey";
            public static string SentToEmployer = Prefix + "-sent-employer-click";
            public static string Asap = Prefix + "-asap-click";
            public static string ChangedMind = Prefix + "-changed-mind-click";
            public static string Closed = Prefix + "-close-click";
            public static string Displaying = Prefix;
        }
    }
}
