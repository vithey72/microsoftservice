﻿namespace BNine.Constants
{
    public static class BankInformation
    {
        public static readonly string BankAddress = "6070 Poplar Ave suite 200, Memphis, TN 38119";

        public static readonly string RoutingNumber = "084106768";
    }
}
