﻿namespace BNine.Constants
{
    using System.Text.Json;

    public static class FeaturesNames
    {
        public static class Features
        {
            public const string ArgyleSuccessScreen = "ArgyleSuccessScreen";
            public const string CheckCashing = "CheckCashing";
            public const string UserApplicationEvaluation = "UserApplicationEvaluation";
            public const string ArgyleCrossButtonHiding = "ArgyleCrossButtonHiding";
            public const string WelcomePaycheckSwitchPayrollDepositScreen = "WelcomePaycheckSwitchPayrollDepositScreen";
            public const string InfoPopup = "InfoPopup";
            public const string UserQuestionnaire = "UserQuestionnaire";
            public const string SmsVerificationDevelopmentMode = "SmsVerificationDevelopmentMode";
            public const string AppVersionRequirements = "AppVersionRequirements";
            public const string ManualFormSurvey = "ManualFormSurvey";
            public const string ArrayCreditRating = "ArrayCreditRating";
            public const string TariffPlansButton = "TariffPlansButton";
            public const string FavoriteTransfers = "FavoriteTransfers";
            public const string CashbackProgramOnboarding = "CashbackProgramOnboarding";
            public const string UseNewKycFlow = "UseNewKycFlow";
            public const string PayOnboarding = "PayOnboarding";
            public const string AdvanceWidgetBanner = "AdvanceWidgetBanner";
            public const string ChristmasTime = "ChristmasTime";
            public const string AfterAdvanceTips = "AfterAdvanceTips";
            public const string SelfieOnboarding = "SelfieOnboarding";
            public const string ClientSelfieForOnboarding = "ClientSelfieForOnboarding";
            public const string ScanVouchedBackSide = "ScanVouchedBackSide";
            public const string IncomeAndTaxesScreen = "IncomeAndTaxesScreen";

            public const string CreditCardInternalTest = "CreditCardInternalTest";
            public const string NeedPinForVirtualCard = "NeedPinForVirtualCard";
            public const string TransactionAnalytics = "TransactionAnalytics";
            public const string RateYourExperience = "RateYourExperience";
            public const string ExternalCardSelfie = "ExternalCardSelfie";
            public const string AprilTaxPlatform = "AprilTaxPlatform";
            public const string QuickBooksPlatform = "QuickBooksPlatform";
            public const string NewAdvanceWidgetSource = "NewAdvanceWidgetSource";
            public const string AdvanceLimitSimulator = "AdvanceLimitSimulator";
            public static string FirebaseSplitTesting = "FirebaseSplitTesting";
            public static readonly string TestFeatureForVersionAndPlatform = "TestFeatureForVersionAndPlatform";
            public const string AccountClosure = "AccountClosure";
            public static string SwitchToPremiumAfterAdvance = "SwitchToPremiumAfterAdvance";
            public const string CheckScorePaidService = "CheckScorePaidService";
            public const string BeforeAdvancePremiumMotivationExperiment = "BeforeAdvancePremiumMotivationExperiment";
            public const string CurrentAdditionalAccountsInternal = "CurrentAdditionalAccountsInternal";
            public const string CurrentAdditionalAccountsExternal = "CurrentAdditionalAccountsExternal";
            public const string WalletInfoSection = "WalletInfoSection";

            [Obsolete("Delete if you find them")]
            public const string MarketingBanners = "MarketingBanners";
            public const string TransfersScreen = "TransfersScreen";

        }

        public static class Groups
        {
            public const string Default = "Default";
            public const string SeenArgyleSuccessScreen = "SeenArgyleSuccessScreen";
            public const string ShowManualFormSurvey = "ShowManualFormSurvey";
            public const string ArrayCreditRatingInternalAdopters = "ArrayCreditRatingInternalAdopters";
            public const string B9Core = "B9Core";
            public const string SeenCashbackOnboardingSlideshow = "SeenCashbackOnboardingSlideshow";

            public const string PayOnboardingGroupA = "PayOnboardingGroupA";
            public const string PayOnboardingGroupB = "PayOnboardingGroupB";
            public const string PayOnboardingGroupC = "PayOnboardingGroupC";
            public const string PayOnboardingGroupD = "PayOnboardingGroupD";
            public const string PayOnboardingDoneWithExperimentGroup = "PayOnboardingDoneWithExperimentGroup";

            public const string AdvanceWidgetBannerGroupA = "AdvanceWidgetBannerGroupA";
            public const string AdvanceWidgetBannerGroupB = "AdvanceWidgetBannerGroupB";


            [Obsolete("Delete if you find them")]
            public const string TransfersScreenInternalAdopters = "TransfersScreenInternalAdopters";
            public const string SalaryAffiliateProgramA = "SalaryAffiliateProgramA";
            public const string SalaryAffiliateProgramB = "SalaryAffiliateProgramB";
        }

        public static class Settings
        {
            public static class CheckCashing
            {
                public const string InstantAllocation = "InstantAllocation";
                public const string MaxAmount = "MaxAmount";
            }

            public static class UserApplicationEvaluation
            {
                public const string AdvanceTakenCounter = "AdvanceTakenCounter";
                public const string IsActive = "IsActive";
                public static readonly string ConfigDefaultSettingsValue =
                $@"{{
                      ""{AdvanceTakenCounter}"": 2,
                      ""{IsActive}"": false,
                  }}";
            }

            public static class ArgyleCrossButtonHiding
            {
                public const string TimeMilliseconds = "TimeMilliseconds";
            }

            public static class WelcomePaycheckSwitchPayrollDepositScreen
            {
                public const string ScreenId = "ScreenId";
            }

            public static class AppVersionRequirements
            {
                public const string RequiredVersion = "RequiredVersion";
                public const string IOs = "IOs";
                public const string Android = "Android";
            }

            public static class Questionnaire
            {
                public const string Passed = "Passed";

                public static string QuizGroupName(string name) => "Passed" + name + "Quiz";

                public static class Quizzes
                {
                    public const string Default = "Default";
                    public const string Activity = "Activity";
                    public const string RateYourExperience = "RateYourExperience";
                    public const string SourceOfIncome = "SourceOfIncome";
                }
            }

            public static class ManualFormSurvey
            {
                public const string IsSurveyVisible = "IsSurveyVisible";
            }

            public static class ArrayCreditRating
            {
                public const string TermsAndConditions = "TermsAndConditions";

                public const string TermsAndConditionsValue =
                    "{" + @$"""{TermsAndConditions}"": """ +
                    "By checking the box and clicking on the ‘Continue’ button below, you agree to the terms and conditions, acknowledge " +
                    "receipt of our privacy policy and agree to its terms, and confirm your authorization for B9 to obtain your credit " +
                    "profile from any consumer reporting agency to display to you, to confirm your identity to avoid fraudulent transactions " +
                    "in your name, and to enable any consumer reporting agency to monitor your credit for changes.\n" +
                    "Verify your identity by answering the questions on the next screen accurately.Please be aware that the credit bureau can " +
                    "block your credit profile for up to 30 days after many unsuccessful attempts." +
                    @"""}";
            }

            public static class FavoriteTransfers
            {
                public const string FavoriteTransfersSettings =
                    "{ \"showOnWallet\": true, \"showOnSendMoneyDialog\": true }";
            }

            public static class InfoPopup
            {
                public const string VipiskaPopupName = "VipiskaPopup";
            }

            public static class PayOnboarding
            {
                private static string SettingsBasic = "{ \"priceAdvance\": 9.99, \"feeAdvance\": null, " +
                                                     "\"pricePremium\": 19.99, \"feePremium\": 1, ";

                public static string SettingsGroupA = "{ \"groupName\": \"GroupA\" }";

                public static string SettingsGroupB = SettingsBasic +
                                                      "\"flow\": \"beforeArgyle\", \"forbidSkip\": false, \"groupName\": \"GroupB\" }";

                public static string SettingsGroupC = SettingsBasic +
                                                      "\"flow\": \"afterArgyle\", \"forbidSkip\": false, \"groupName\": \"GroupC\" }";

                public static string SettingsGroupD = SettingsBasic +
                                                      "\"flow\": \"beforeArgyle\", \"forbidSkip\": true, \"groupName\": \"GroupD\" }";
            }

            public static class AlternativeAdvanceWidget
            {
                public static string SettingsGroupA = "{ \"groupName\": \"GroupA\" }";
                public static string SettingsGroupB = "{ \"groupName\": \"GroupB\" }";
            }

            public static class FirebaseSplitTesting
            {
                public static string TwoFreeAtmWithdrawalsForPremiumUsersMonthly = "premiumPlanAtmFee";
                public static string Settings = JsonSerializer.Serialize(new
                {
                    runningTests = new[] { TwoFreeAtmWithdrawalsForPremiumUsersMonthly }
                });
            }

            public static class SwitchToPremiumAfterAdvance
            {
                public static string ShowCooldownHoursName = "ShowCooldownHours";
                public static string Settings = "{ \"" + ShowCooldownHoursName + "\": 0 }";
            }
        }
    }
}
