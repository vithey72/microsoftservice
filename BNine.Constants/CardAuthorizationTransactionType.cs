﻿namespace BNine.Constants
{
    public static class CardAuthorizationTransactionType
    {
        public const string AUTH = "AUTH";
        public const string PAYMENT = "PAYMENT";
        public const string AVS = "AVS";
        public const string ATM = "ATM";
        public const string REVERSAL = "REVERSAL";
    }
}
