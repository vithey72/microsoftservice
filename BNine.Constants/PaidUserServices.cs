﻿namespace BNine.Constants;

using System;

public static class PaidUserServices
{
    public static readonly Guid DefaultPlan = new("fbf9e0e1-00f0-419a-b172-8b09c6a6d50d");
    public static readonly Guid Basic1Month = new("b9491ab9-601b-406b-a689-a9b311773b73");
    public static readonly Guid Premium1Month = new("b6edf67d-5fdd-4246-b47a-259b4c4e49be");
    public static readonly Guid Premium3Month10Sale = new("EAC3A65F-7703-ED11-B47A-000D3A31D12D");

    [Obsolete("Could not clear this option with compliance.")]
    public static readonly Guid Premium3Month20Sale = new("6C2B90BC-7703-ED11-B47A-000D3A31D12D");

    public static readonly Guid PremiumSupport = new("2DC6AEEC-5EE7-4661-9338-50B3101E59DA");
    public static readonly Guid AdvanceUnfreeze5 = new("7A143E78-9022-44C7-9645-D933D7F29430");
    public static readonly Guid AdvanceUnfreeze10 = new("AC5B3D89-FA4C-43E9-B308-9C4A0C26D863");
    public static readonly Guid AdvanceUnfreeze15 = new("8C67B8A0-9C03-464D-9055-2425ABB5A490");
    public static readonly Guid PhysicalCardEmbossing = new("20AF6F52-54AA-4F20-AAD5-C314153E4106");
    public static readonly Guid PhysicalCardReplacement = new("9D4F7B80-6E25-45DF-94FC-AEF077D4CF3E");
    public static readonly Guid TariffBasicPurchaseOnOnboarding = new("c1d68b39-e503-4b3d-924c-0318ace1bb46");
    public static readonly Guid AdvanceBoost999 = new("252c84cb-a679-4aa5-8c24-e34f550fda3c");
    public static readonly Guid AdvanceBoost1599 = new("0a59264a-7b62-419f-8183-ffb80d54d44f");
    public static readonly Guid AdvanceBoost1999 = new("3d3712c6-604f-43f2-a3f9-f5a55800adb4");

    public static readonly Guid AfterAdvanceTip2 = new("eac381d5-ad0e-4dd3-a1fc-5f7293a34891");
    public static readonly Guid AfterAdvanceTip3 = new("7bfd404c-7688-4f90-b3cf-c61f70431333");
    public static readonly Guid AfterAdvanceTip5 = new("a6ef9444-f165-4bd1-9f56-ff0c07adb12f");
    public static readonly Guid AfterAdvanceTip6 = new("6eee9521-d835-4265-af9e-ec0d3935c62a");
    public static readonly Guid AfterAdvanceTip8 = new("04d918db-ff13-46de-abdd-ee5f2276c6b4");
    public static readonly Guid AfterAdvanceTip10 = new("a12b4050-19bf-4b43-8ce9-ef4b549430f7");
    public static readonly Guid AfterAdvanceTip12 = new("6ee6a056-af28-4e7e-b4bf-bbac91cd3787");
    public static readonly Guid AfterAdvanceTip20 = new("c20b586f-2d4b-4675-bc6a-6833b335d1da");

    public static readonly Guid CheckCreditScorePaidService5USD = new("554a350c-5313-11ee-be56-0242ac120002");
    public static readonly Guid CheckCreditScorePaidService10USD = new("b2f65678-402a-11ee-be56-0242ac120002");

    // ReSharper disable MemberHidesStaticFromOuterClass
    public static class Names
    {
        public const string PremiumSupportName = "Premium Support";
        public const string AdvanceUnfreeze5 = "Advance unfreeze fee 5";
        public const string AdvanceUnfreeze10 = "Advance unfreeze fee 10";
        public const string AdvanceUnfreeze15 = "Advance unfreeze fee 15";
        public const string PhysicalCardEmbossing = "Physical card embossing fee";
        public const string PhysicalCardReplacement = "Physical card replacement fee";
        public const string TariffBasicPurchaseOnOnboarding = "Monthly Membership Default OT";
        public const string AdvanceBoost999 = "Advance Boost fee 9.99";
        public const string AdvanceBoost1599 = "Advance Boost fee 15.99";
        public const string AdvanceBoost1999 = "Advance Boost fee 19.99";
        public const string AfterAdvanceTip2 = "After Advance Tip 2";
        public const string AfterAdvanceTip3 = "After Advance Tip 3";
        public const string AfterAdvanceTip5 = "After Advance Tip 5";
        public const string AfterAdvanceTip6 = "After Advance Tip 6";
        public const string AfterAdvanceTip8 = "After Advance Tip 8";
        public const string AfterAdvanceTip10 = "After Advance Tip 10";
        public const string AfterAdvanceTip12 = "After Advance Tip 12";
        public const string AfterAdvanceTip20 = "After Advance Tip 20";
        public const string CheckCreditScorePaidService = "Check credit score paid service";
    }

    public static class Prices
    {
        public const decimal AdvanceBoost999 = 9.99m;
        public const decimal AdvanceBoost1599 = 15.99m;
        public const decimal AdvanceBoost1999 = 19.99m;
        public static readonly int[] TipsAmounts = { 2, 5, 6, 12, 20 };
        public const int DefaultTipIndex = 2;
    }
    public static class AfterAdvanceTipsValuesWithIds
    {
        public static readonly Dictionary<int, Guid> Data =
            new()
            {
                { 2, AfterAdvanceTip2 },
                { 5, AfterAdvanceTip5 },
                { 6, AfterAdvanceTip6 },
                { 8, AfterAdvanceTip8 },
                { 10, AfterAdvanceTip10 },
                { 12, AfterAdvanceTip12 },
                { 20, AfterAdvanceTip20 }
            };

    }
}
