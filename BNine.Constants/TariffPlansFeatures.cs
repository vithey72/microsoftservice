﻿namespace BNine.Constants;

// we don't want to mess with the data applied on the migration stage at the moment as we had to manually sync it for every partner
// and if we were to introduce changes in migration we would have to manually sync it again
// so we attempt to enrich models from migrations on the seed stage instead, in accordance with certain conditions

public static class TariffPlansFeatures
{
    // these texts are used as IDs to match and populate concrete features with concrete hint object
    public static class PremiumTexts
    {
        public static readonly string PremiumIsBasicPlusAsKey = @"Everything in the \nB9 Basic Plan — plus:";
        public static readonly string PremiumIsBasicPlus = "Everything in the \nB9 Basic Plan — plus:";
        public static readonly string PremiumAdvanceAmountText = "Advance up to $500";
        public static readonly string CreditScoreText = "Credit Score";
        public static readonly string CreditScoreAdditionalText = "Credit Score is not a product of Evolve Bank & Trust, and offered by third party provider and affiliate of B9.";
        public static readonly string PremiumSupportText = "Premium Support";
        public static readonly string PremiumSupportAdditionalText = @"• Dedicated priority answer line
• We will answer you in an hour

Our Customer Support Team is active the following hours:
• Mon-Fri: 6 AM to 9 PM Pacific Standard Time (9 AM to 12 AM Eastern Standard Time)
• Sat-Sun: 6 AM to 6 PM Pacific Standard Time (9 AM to 9 PM Eastern Standard Time)

Time of response may depend on external factors.";
        public static readonly string AtmWithdrawals = "2 Free ATM Withdrawals";
        public static readonly string AtmWithdrawalsAdditionalText = @"2 Free B9 ATM withdrawal fees

• B9 charges a $2.50 fee with all ATM withdrawals, separate from the fee charged by an ATM. Each month, that B9 fee—for your first two withdrawals only—will not be charged.
• Even though it’ll look like the ATM charged you our B9 ATM fee, it’s just a temporary hold which will be released in a few days.
• Specified period: from the first day of the month to the 30th/31st.";
    }

    public static class BasicTexts
    {
        public static readonly string BasicAdvanceAmountText = "Advance up to $100";
        public static readonly string VisaDebitCardTextAsKey = @"B9 Visa® Debit Card with \nup to 5% cashback";
        public static readonly string VisaDebitCardText = "B9 Visa® Debit Card with \nup to 5% cashback";
        public static readonly string BasicAdvanceInstantServiceKey = "No-fee, fast access cash advance on approval";
        public static readonly string BasicAdvanceInstantService = "Get advances fast";
        public static readonly string InstantTransfer = "Quick transfers to B9 Members";
    }

    public static readonly string PremiumTariffFeaturesRaw = @$"[
                  {{
                    ""Text"": ""Everything in the \nB9 Basic Plan — plus:""
                  }},
                  {{
                    ""Text"": ""{PremiumTexts.PremiumAdvanceAmountText}"",
                    ""AdditionalInfo"": ""B9 Basic℠ and B9 Premium℠ are optional services, offered by B9 to its members that require to set up a payroll direct deposits to the B9 Account each month. All B9 Basic℠ and B9 Premium℠ members start with up to $100 early pay advance. B9 Premium℠ members have a potential for it to increase up to $500 early pay advance. All B9 members are informed of their current available maxes in the B9 mobile app.Their limit may change at any time, at B9's discretion.""
                  }},
                  {{
                    ""Text"": ""{PremiumTexts.CreditScoreText}"",
                    ""additionalInfo"": ""Credit Score is not a product of Evolve Bank & Trust, and offered by third party provider and affiliate of B9.""
                  }},
                  {{
                    ""Text"": ""{PremiumTexts.PremiumSupportText}"",
                    ""additionalInfo"": ""• Dedicated priority answer line \n• We will answer you in an hour \n\nOur Customer Support Team is active the following hours: \n• Mon-Fri: 6 AM to 9 PM Pacific Standard Time (9 AM to 12 AM Eastern Standard Time) \n• Sat-Sun: 6 AM to 6 PM Pacific Standard Time (9 AM to 9 PM Eastern Standard Time) \n\nTime of response may depend on external factors.""
                  }},
                  {{
                    ""Text"": ""{PremiumTexts.AtmWithdrawals}"",
                    ""additionalInfo"": ""2 Free B9 ATM withdrawal fees \n\n• B9 charges a $2.50 fee with all ATM withdrawals, separate from the fee charged by an ATM. Each month, that B9 fee—for your first two withdrawals only—will not be charged. \n• Even though it’ll look like the ATM charged you our B9 ATM fee, it’s just a temporary hold which will be released in a few days. \n• Specified period: from the first day of the month to the 30th/31st.""
                  }}
                ]
    ";

    public static readonly string BasicTariffFeaturesRaw = @$"[
                  {{
                    ""Text"": ""{BasicTexts.VisaDebitCardTextAsKey}""
                  }},
                  {{
                    ""Text"": ""{BasicTexts.BasicAdvanceAmountText}""
                  }},
                  {{
                    ""Text"": ""{BasicTexts.BasicAdvanceInstantService}""
                  }},
                  {{
                    ""Text"": ""{BasicTexts.InstantTransfer}""
                  }}
                ]";
}
