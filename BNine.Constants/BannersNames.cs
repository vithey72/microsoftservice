﻿namespace BNine.Constants;

public static class BannersNames
{
    public const string EligibleForBonusBanner = "EligibleForBonusActivation";
    public const string SendNewReferralCodeBanner = "SendNewReferralCode";
    public const string CashbackBanner = "CashbackBanner";
    public const string PremiumTariff3MonthsDiscount = "PremiumTariff3MonthsDiscount";
}
