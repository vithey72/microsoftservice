﻿namespace BNine.AdminAPI
{
    using BNine.AdminAPI.Infrastructure.Extensions;
    using BNine.Argyle;
    using BNine.MBanq.Infrastructure;
    using BNine.Persistence;
    using BNine.ServiceBus;
    using BNine.Truv;
    using Infrastructure.Middleware;
    using MBanq.Administration;
    using MBanq.Client;
    using Microsoft.AspNetCore.Builder;
    using Microsoft.AspNetCore.Hosting;
    using Microsoft.Extensions.Configuration;
    using Microsoft.Extensions.DependencyInjection;
    using Microsoft.Extensions.Hosting;

    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration
        {
            get;
        }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddLocalization(options =>
            {
                options.ResourcesPath = "Resources";
            });

            services
                .AddAPI()
                .AddArgyle(Configuration)
                .AddTruv(Configuration)
                .AddMBanqAdministration()
                .AddMBanqClient()
                .AddMBanqInfrastructure(Configuration)
                .AddPersistence(Configuration)
                .AddHttpContextAccessor()
                .AddAuthentication(Configuration)
                .AddMediatr()
                .AddSettings(Configuration)
                .AddSwagger()
                .ConfigureCors(Configuration)
                .AddAzureServiceBus(Configuration)
                .AddHealthChecks()
                .AddDbContextCheck<BNineDbContext>()
                ;

            services.AddMemoryCache();
        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                // overrides ApiExceptionFilterForAdminAttribute code 500 behavior
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseMiddleware<SwaggerBasicAuthMiddleware>();
            }

            app.UseHealthChecks("/health");

            app.UseHttpsRedirection();

            app.UseOpenApi();

            app.UseSwaggerUi3();

            app.UseCors("BnineCorsPolicy");

            app.UseRouting();

            app.UseAuthentication();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapDefaultControllerRoute();
            });
        }
    }
}
