# BNine Admin API

- [BNine Admin API](#bnine-admin-api)
  - [Overview](#overview)
  - [Launch application](#launch-application)
    - [Configure application](#configure-application)
      - [Configure secrets](#configure-secrets)
    - [Run application](#run-application)
      - [Visual Studio](#visual-studio)
      - [CLI](#cli)
  - [Docker](#docker)
    - [Build image](#build-image)
    - [Run container](#run-container)
  - [Tests](#tests)

## Overview

> Please note that all commands in this README should be executed from the current folder: `<repo folder>`.

## Launch application

### Configure application

Before running application, please configure mandatory options. An example of parameters can be found in the file `appsettings.json`.

> Please do not change this file if it is not related to changing (adding, removing) the functionality of the application.
> **Never store passwords or other sensitive data in this file and its descendants (appsettings.*.json).**

For development purpose, please create and configure mandatory options in `appsettings.Development.json`:

**TBD** Add appsettings

#### Configure secrets

During the development it's recommended to use [**The Secret Manager tool**](https://docs.microsoft.com/en-us/aspnet/core/security/app-secrets?view=aspnetcore-6.0&tabs=windows) to store sensitive data. [How to set secrets](https://docs.microsoft.com/en-us/aspnet/core/security/app-secrets?view=aspnetcore-6.0&tabs=windows#set-a-secret) for application.

> The Secret Manager tool doesn't encrypt the stored secrets and shouldn't be treated as a trusted store. It's **for development purposes only** - to avoid accidental committing of sensitive data to the git repository. The keys and values are stored in a JSON configuration file in the user profile directory.

Please configure the following options using the Secret Manager tool:

**TBD** Add app secrets

### Run application

You can observe launched API using the following URLs:

- [Swagger UI](https://localhost:5002/swagger)

This service provides OpenAPI specification which generates automatically on build.

#### Visual Studio

To run the application in Visual Studio, please do the following:

- Make sure to set `BNine.AdminAPI` as a `Startup project`;
- Select `BNine.AdminAPI` profile;
- Hit `F5` to run application.

#### CLI

To run the application via CLI, please execute the following command:

```bash
dotnet run --project BNine.AdminAPI/BNine.AdminAPI.csproj --launch-profile 'BNine.AdminAPI'
```

## Docker

### Build image

To build a Docker image, please do the following:

```bash
docker build -f BNine.AdminAPI/Dockerfile -t bnine/admin-api:latest .
```

### Run container

To run a container, please do the following:

```bash
docker run --name bnine-admin-api bnine/admin-api:latest
```

> Don't forget to pass environment variables.

## Health checks

**TBD** Add health checks

## Tests

**TBD** Add tests
