﻿namespace BNine.AdminAPI.Infrastructure.Extensions
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Security.Claims;
    using System.Threading.Tasks;
    using Application.Aggregates.AdminUser.Commands;
    using Application.Aggregates.AdminUser.Queries;
    using Application.Aggregates.Banners.Commands.DeleteInfoPopup;
    using Application.Aggregates.Banners.Models;
    using Application.Aggregates.Banners.Queries.CreateInfoPopupBanner;
    using Application.Aggregates.Banners.Queries.GetInfoPopupBanners;
    using Application.Aggregates.Banners.Queries.UpdateInfoPopupBanner;
    using Application.Aggregates.Cards.Commands.SyncCardStatuses;
    using Application.Aggregates.CheckingAccounts.Queries.GetAccountStatementAsAdmin;
    using Application.Aggregates.Configuration.Commands.AddContent;
    using Application.Aggregates.Configuration.Commands.UpdateConfiguration;
    using Application.Aggregates.Configuration.Models;
    using Application.Aggregates.EarlySalary.EarlySalary.Queries.GetArgyleAllocations;
    using Application.Aggregates.Emails.Commands.Documents.SpendingAccountStatement;
    using Application.Aggregates.ExternalCards.Queries.GetExternalCardsHistory;
    using Application.Aggregates.Features.Commands.DeleteGroup;
    using Application.Aggregates.Features.Models;
    using Application.Aggregates.Features.Queries.AddUsersToGroup;
    using Application.Aggregates.Features.Queries.CreateGroup;
    using Application.Aggregates.Features.Queries.ExcludeUsersFromGroup;
    using Application.Aggregates.Features.Queries.GetAllGroups;
    using Application.Aggregates.Features.Queries.UpdateGroup;
    using Application.Aggregates.LogAdminAction.Queries;
    using Application.Aggregates.Placeholders.Commands.AddOrUpdatePlaceholders;
    using Application.Aggregates.Placeholders.Commands.UpdatePlaceholdersArray;
    using Application.Aggregates.Placeholders.Models;
    using Application.Aggregates.Placeholders.Queries;
    using Application.Aggregates.StaticDocuments.Commands;
    using Application.Aggregates.StaticDocuments.Queries;
    using Application.Aggregates.TransferErrorLog.Models;
    using Application.Aggregates.TransferErrorLog.Queries;
    using Application.Aggregates.Users.Commands.AddCurrentAccountsIfNeeded;
    using Application.Aggregates.Users.Commands.CloseAccount.CloseClientAccount;
    using Application.Aggregates.Users.Commands.CloseAccount.MarkLoansAsClosedIfNeeded;
    using Application.Aggregates.Users.Commands.EnableSurveyForUser;
    using Application.Aggregates.Users.Commands.SwitchCashbackForUsers;
    using Application.Aggregates.Users.Commands.UpdateClientFullName;
    using Application.Aggregates.Users.Models.UserAdvancesHistory;
    using Application.Aggregates.Users.Models.UserCashback;
    using Application.Aggregates.Users.Models.UserDelayedBoostInfo;
    using Application.Aggregates.Users.Queries;
    using Application.Aggregates.Users.Queries.CloseAccount.ValidateCloseClientAccount;
    using Application.Aggregates.Users.Queries.GetAdvancesHistory;
    using Application.Aggregates.Users.Queries.GetClientDocuments;
    using Application.Aggregates.Users.Queries.GetDelayedBoostInfo;
    using Application.Aggregates.Users.Queries.GetTariffInfo;
    using Application.Aggregates.Users.Queries.GetUserGroups;
    using Application.Aggregates.Users.Queries.GetUsersInGroup;
    using Application.Interfaces.Firebase;
    using Application.Interfaces.ServiceBus;
    using Application.Models.AdminActionLog;
    using Application.Models.Dto;
    using Application.Models.Emails.EmailsWithTemplate;
    using Application.Models.MBanq;
    using Application.Services;
    using Application.Services.ClientDocument;
    using AspNetCore.Authentication.ApiKey;
    using BlobStorage;
    using BNine.AdminAPI.Infrastructure.Filters;
    using BNine.Application.Aggregates.Argyle.Users.Commands.DeleteArgyleUser;
    using BNine.Application.Aggregates.CheckingAccounts.Commands.BlockSavingAccount;
    using BNine.Application.Aggregates.CheckingAccounts.Commands.UnblockSavingAccount;
    using BNine.Application.Aggregates.CheckingAccounts.Queries.GetSavingAccountInfo;
    using BNine.Application.Aggregates.CommonModels;
    using BNine.Application.Aggregates.CommonModels.Dialog;
    using BNine.Application.Aggregates.CreditScore.Models;
    using BNine.Application.Aggregates.CreditScore.Queries;
    using BNine.Application.Aggregates.EarlySalary.ArgyleSettings.Command.UpdateArgyleSettings;
    using BNine.Application.Aggregates.EarlySalary.ArgyleSettings.Queries.GetArgyleSettings;
    using BNine.Application.Aggregates.EarlySalary.EarlySalaryManualSettings.Command.UpdateEarlySalaryManualSettings;
    using BNine.Application.Aggregates.EarlySalary.EarlySalaryManualSettings.Queries.GetArgyleSettings;
    using BNine.Application.Aggregates.IncomeAndTaxesScreen.Models;
    using BNine.Application.Aggregates.IncomeAndTaxesScreen.Queries.GetEmployersFromArgyle;
    using BNine.Application.Aggregates.LoanSettings.Queries.GetACHPrincipalKeywords;
    using BNine.Application.Aggregates.LogAdminAction.Commands;
    using BNine.Application.Aggregates.PaycheckSwitch;
    using BNine.Application.Aggregates.Payroll;
    using BNine.Application.Aggregates.ServiceBusEvents.Commands.PublishB9Event;
    using BNine.Application.Aggregates.Settings.LoanSettings.Commands.UpdateLoanSettings;
    using BNine.Application.Aggregates.Users.AccountStateMachine;
    using BNine.Application.Aggregates.Users.Closure;
    using BNine.Application.Aggregates.Users.Commands.Block;
    using BNine.Application.Aggregates.Users.Commands.CloseAccount.CloseClientAccountInternal;
    using BNine.Application.Aggregates.Users.Commands.Unblock;
    using BNine.Application.Aggregates.Users.Commands.Update.ForceUpdateAddress;
    using BNine.Application.Aggregates.Users.Commands.Update.ForceUpdatedEmail;
    using BNine.Application.Aggregates.Users.Commands.Update.ForceUpdatePhoneNumber;
    using BNine.Application.Aggregates.Users.EventHandlers.OnboardingStepFailed;
    using BNine.Application.Aggregates.Users.EventHandlers.OnboardingStepSuccess;
    using BNine.Application.Aggregates.Users.EventHandlers.UserBlocked;
    using BNine.Application.Aggregates.Users.Models;
    using BNine.Application.Aggregates.Users.Models.TransactionLimits;
    using BNine.Application.Aggregates.Users.Queries.GetBlockUserReasons;
    using BNine.Application.Aggregates.Users.Queries.GetOnboardingDetails;
    using BNine.Application.Aggregates.Users.Queries.GetTransactionLimitsQuery;
    using BNine.Application.Aggregates.Users.Queries.GetUserDetails;
    using BNine.Application.Aggregates.Users.Queries.GetUsers;
    using BNine.Application.Behaviours;
    using BNine.Application.Interfaces;
    using BNine.Application.Interfaces.Bank.Administration;
    using BNine.Application.Interfaces.Bank.Client;
    using BNine.Application.Models;
    using BNine.Application.Services.AdminAPI;
    using BNine.Domain.Events;
    using BNine.Domain.Events.Users;
    using BNine.DwhIntegration;
    using BNine.MBanq.Administration.Services.Clients;
    using BNine.MBanq.Administration.Services.Users;
    using BNine.MBanq.Client.Services.ClientAddress;
    using BNine.Settings;
    using BNine.Settings.DwhIntegration;
    using Domain.Entities.Features;
    using Firebase.Services;
    using FluentValidation;
    using FluentValidation.AspNetCore;
    using MBanq.Administration.Services.Classifications;
    using MBanq.Client.Services.Transfers;
    using MediatR;
    using MediatR.Registration;
    using Microsoft.AspNetCore.Authentication.JwtBearer;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.Extensions.Configuration;
    using Microsoft.Extensions.DependencyInjection;
    using Microsoft.IdentityModel.Tokens;
    using Newtonsoft.Json.Converters;
    using Newtonsoft.Json.Serialization;
    using NotificationHub.Services;
    using NSwag;
    using NSwag.Generation.Processors.Security;
    using SendGrid.Services;
    using ServiceBus;
    using Settings.Blob;
    using SavingAccountInfo = Application.Aggregates.CheckingAccounts.Queries.GetSavingAccountInfo.SavingAccountInfo;

    internal static class ServiceCollectionExtensions
    {
        internal static IServiceCollection AddAuthentication(this IServiceCollection services,
            IConfiguration configuration)
        {
            services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
                .AddJwtBearer(JwtBearerDefaults.AuthenticationScheme, opts =>
                {
                    opts.Authority = configuration["Authority"];
                    opts.RequireHttpsMetadata = true;

                    opts.TokenValidationParameters = new TokenValidationParameters
                    {
                        NameClaimType = "name",
                        RoleClaimType = ClaimTypes.Role,
                        ValidateAudience = false,
                        ClockSkew = new TimeSpan(0, 0, 0)
                    };
                })
                .AddApiKeyInHeader(ApiKeyDefaults.AuthenticationScheme, opts =>
                {
                    opts.Realm = "Admin API";
                    opts.KeyName = "X-Api-Key";
                    opts.Events = new ApiKeyEvents
                    {
                        OnValidateKey = context =>
                        {
                            if (context.ApiKey == configuration.GetValue<string>("ApiKey"))
                            {
                                context.ValidationSucceeded(new Claim[]
                                {
                                    new Claim("name", "API"), new Claim(ClaimTypes.Role, "ApiAdmin")
                                });
                            }
                            else
                            {
                                context.ValidationFailed();
                            }

                            return Task.CompletedTask;
                        }
                    };
                });
            ;

            services.AddAuthorization(options =>
            {
                options.DefaultPolicy = new AuthorizationPolicyBuilder()
                    .AddAuthenticationSchemes("Bearer", "ApiKey")
                    .RequireClaim(ClaimTypes.Role)
                    .Build();
            });

            return services;
        }

        internal static IServiceCollection AddSettings(this IServiceCollection services, IConfiguration configuration)
        {
            services.ConfigureAndValidateApplicationSettings<AlwaysEncryptedSettings>(configuration);
            services.ConfigureAndValidateApplicationSettings<ArgyleSettings>(configuration);
            services.ConfigureAndValidateApplicationSettings<TruvSettings>(configuration);
            services.ConfigureAndValidateApplicationSettings<MBanqSettings>(configuration);
            services.ConfigureAndValidateApplicationSettings<BlobStorageSettings>(configuration);
            services.ConfigureAndValidateApplicationSettings<AzureServiceBusSettings>(configuration);
            services.ConfigureAndValidateApplicationSettings<AzureNotificationHubSettings>(configuration);
            services.ConfigureAndValidateApplicationSettings<SendGridSettings>(configuration);
            services.ConfigureAndValidateApplicationSettings<EmailTemplatesSettings>(configuration);
            services.ConfigureAndValidateApplicationSettings<DwhIntegrationSettings>(configuration);

            return services;
        }

        internal static IServiceCollection AddAPI(this IServiceCollection services)
        {
            services
                .AddControllers(options => options.Filters.Add(new ApiExceptionFilterAttribute()))
                .AddFluentValidation()
                .AddNewtonsoftJson(options =>
                {
                    options.SerializerSettings.ContractResolver = new DefaultContractResolver
                    {
                        NamingStrategy = new CamelCaseNamingStrategy { ProcessDictionaryKeys = true, }
                    };
                    options.SerializerSettings.Converters.Add(new StringEnumConverter());
                    options.SerializerSettings.DateFormatString = "yyyy'-'MM'-'dd'T'HH':'mm':'ss.fffffff";
                });

            services.AddScoped<IAdminApiCurrentUserService, AdminApiCurrentUserService>();
            services.AddScoped<ICurrentUserService, AdminApiCurrentUserService>();
            services.AddScoped<IPartnerProviderService, PartnerProviderService>();
            services.AddTransient<ITariffPlanService, TariffPlanService>();
            services.AddTransient<IBankClassificationsService, MBanqClassificationsService>();
            services.AddTransient<ILoanSettingsProvider, LoanSettingsProvider>();
            services.AddTransient<IClientAccountSettingsProvider, ClientAccountSettingsProvider>();
            services.AddTransient<IPayrollConnectionsProvider, PayrollConnectionsProvider>();

            services.Configure<ApiBehaviorOptions>(options =>
            {
                options.SuppressModelStateInvalidFilter = true;
            });

            return services;
        }

        internal static IServiceCollection AddSwagger(this IServiceCollection services)
        {
            services.AddOpenApiDocument(configure =>
            {
                configure.Title = "BNine Admin API";
                configure.AllowReferencesWithProperties = true;
                configure.AddSecurity("JWT", Enumerable.Empty<string>(),
                    new OpenApiSecurityScheme
                    {
                        Type = OpenApiSecuritySchemeType.ApiKey,
                        Name = "Authorization",
                        In = OpenApiSecurityApiKeyLocation.Header,
                        Description = "Type into the textbox: Bearer {your JWT token}."
                    });

                configure.OperationProcessors.Add(new AspNetCoreOperationSecurityScopeProcessor("JWT"));
            });

            return services;
        }

        internal static IServiceCollection ConfigureCors(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddCors((options) =>
            {
                options.AddPolicy("BnineCorsPolicy",
                    builder => builder
                        .SetIsOriginAllowedToAllowWildcardSubdomains()
                        .WithOrigins(configuration.GetSection("AdminApiAllowedOrigins").Get<string[]>())
                        .AllowAnyMethod()
                        .AllowCredentials()
                        .AllowAnyHeader()
                        .Build());
            });

            return services;
        }

        internal static IServiceCollection AddMediatr(this IServiceCollection services)
        {
            services.AddAutoMapper(Assembly.GetAssembly(typeof(IBNineDbContext)));
            services.AddValidatorsFromAssembly(Assembly.GetAssembly(typeof(IBNineDbContext)));

            var serviceConfig = new MediatRServiceConfiguration();
            ServiceRegistrar.AddRequiredServices(services, serviceConfig);
            services.AddTransient(typeof(IPipelineBehavior<,>), typeof(ValidationBehaviour<,>));
            services.AddTransient(typeof(IPipelineBehavior<,>), typeof(UnhandledExceptionBehaviour<,>));
            services.AddBlobStorage();
            services.AddDwhIntegration();

            services.AddTransient<IBankUsersService, MBanqUsersService>();
            services.AddTransient<IBankClientsService, MBanqClientsService>();
            services.AddTransient<IBankSavingsTransactionsService, MBanqSavingsTransactionsService>();
            services.AddTransient<IServiceBus, AzureServiceBus>();
            services.AddTransient<IDynamicLinkService, DynamicLinkService>();
            services.AddTransient<IEmailService, SendGridService>();
            services.AddTransient<IBankClientAddressService, MBanqClientAddressService>();
            services.AddTransient<ITransferIconsAndNamesProviderService, TransferIconsAndNamesProviderService>();
            services.AddTransient<IPushNotificationService, PushNotificationService>();
            services.AddTransient<INotificationUsersService, NotificationUsersService>();
            services.AddTransient<IFeatureConfigurationService, FeatureConfigurationService>();
            services.AddScoped<IMarketingBannersAdminService, MarketingBannersAdminService>();
            services.AddTransient<IClientDocumentsOriginalsPersistenceService, ClientDocumentsOriginalsPersistenceService>();
            services.AddTransient<IPaidUserServicesService, PaidUserServicesService>();
            services.AddTransient<ICheckCreditScorePaidService, CheckCreditScorePaidService>();
            services.AddTransient<ICreditScoreBasicOfferInfoScreenViewModelFactory, CreditScoreBasicOfferInfoScreenViewModelFactory>();

            services.AddTransient<IRequestHandler<ForceUpdatePhoneNumberCommand, Unit>, ForceUpdatePhoneNumberCommandHandler>();
            services.AddTransient<IRequestHandler<ForceUpdateEmailCommand, Unit>, ForceUpdateEmailCommandHandler>();
            services.AddTransient<IRequestHandler<ForceUpdateAddressCommand, Unit>, ForceUpdateAddressCommandHandler>();
            services.AddTransient<IRequestHandler<GetUsersQuery, PaginatedList<UserInfoListItem>>, GetUsersQueryHandler>();
            services.AddTransient<IRequestHandler<BlockUserCommand, Unit>, BlockUserCommandHandler>();
            services.AddTransient<IRequestHandler<UnblockUserCommand, Unit>, UnblockUserCommandHandler>();
            services.AddTransient<IRequestHandler<GetUserDetailsQuery, UserDetails>, GetUserDetailsQueryHandler>();
            services.AddTransient<IRequestHandler<GetOnboardingDetailsQuery, OnboardingDetails>, GetOnboardingDetailsQueryHandler>();
            services.AddTransient<IRequestHandler<GetBlockUserReasonsQuery, IEnumerable<BlockUserReasonListItem>>, GetBlockUserReasonsQueryHandler>();
            services.AddTransient<IRequestHandler<GetACHPrincipalKeywordsQuery, IEnumerable<string>>, GetACHPrincipalKeywordsQueryHandler>();
            services.AddTransient<IRequestHandler<AddPayrollKeywordCommand, Unit>, AddPayrollKeywordCommandHandler>();
            services.AddTransient<IRequestHandler<AddContentCommand, UpdatedDocumentResponseModel>, AddContentCommandHandler>();
            services.AddTransient<IRequestHandler<UpdateConfigurationCommand, Unit>, UpdateConfigurationCommandHandler>();
            services.AddTransient<IRequestHandler<GetArgyleSettingsQuery, ArgyleSettingsInfo>, GetArgyleSettingsQueryHandler>();
            services.AddTransient<IRequestHandler<UpdateArgyleSettingsCommand, Unit>, UpdateArgyleSettingsCommandHandler>();
            services.AddTransient<IRequestHandler<GetEarlySalaryManualSettingsQuery, EarlySalaryManualInfo>, GetEarlySalaryManualSettingsQueryHandler>();
            services.AddTransient<IRequestHandler<UpdateEarlySalaryManualSettingsCommand, Unit>, UpdateEarlySalaryManualSettingsCommandHandler>();
            services.AddTransient<IRequestHandler<GetSavingAccountInfoQuery, SavingAccountInfo>, GetSavingAccountInfoQueryHandler>();
            services.AddTransient<IRequestHandler<BlockSavingAccountCommand, Unit>, BlockSavingAccountCommandHandler>();
            services.AddTransient<IRequestHandler<UnblockSavingAccountCommand, Unit>, UnblockSavingAccountCommandHandler>();
            services.AddTransient<IRequestHandler<DeleteArgyleUserCommand, Unit>, DeleteArgyleUserCommandHandler>();
            services.AddTransient<IRequestHandler<PublishB9EntityUpdateCommand, Unit>, PublishB9EntityUpdateCommandHandler>();
            services.AddTransient<IRequestHandler<PublishB9EventCommand, Unit>, PublishB9EventCommandHandler>();
            services.AddTransient<IRequestHandler<GetArgyleAllocationsQuery, IEnumerable<PayAllocationsListItem>>, GetArgyleAllocationsQueryHandler>();
            services.AddTransient<IRequestHandler<EnableSurveyForUserCommand, Unit>, EnableSurveyForUserCommandHandler>();
            services.AddTransient<IRequestHandler<SwitchCashbackForUsersCommand, Unit>, SwitchCashbackForUsersCommandHandler>();
            services.AddTransient<IRequestHandler<GetAccountStatementAsAdminQuery, Unit>, GetAccountStatementAsAdminQueryHandler>();
            services.AddTransient<IRequestHandler<DownloadAccountStatementAsAdminQuery, EmailAttachmentData>, DownloadAccountStatementAsAdminQueryHandler>();
            services.AddTransient<IRequestHandler<SpendingAccountStatementEmailCommand, Unit>, SpendingAccountStatementEmailCommandHandler>();
            services.AddTransient<IRequestHandler<SwitchCashbackForUsersCommand, Unit>, SwitchCashbackForUsersCommandHandler>();
            services.AddTransient<IRequestHandler<LogAdminActionCommand, Unit>, LogAdminActionCommandHandler>();
            services.AddTransient<IRequestHandler<GetUserTransactionLimitsQuery, UserTransactionLimitsViewModel>, GetUserTransactionLimitsQueryHandler>();
            services.AddTransient<IRequestHandler<ValidateCloseClientAccountQuery, ClientAccountClosureValidationResult>, ValidateCloseClientAccountQueryHandler>();
            services.AddTransient<IRequestHandler<CloseClientAccountCommand, Unit>, CloseClientAccountCommandHandler>();
            services.AddTransient<IRequestHandler<GetEmployersFromArgyleQuery, IncomeAndTaxesScreenViewModel.Employer[]>, GetEmployersFromArgyleQueryHandler>();
            services.AddPaycheckSwitch();
            services.AddTransient<IRequestHandler<CloseClientAccountInternalCommand, Unit>, CloseClientAccountInternalCommandHandler>();
            services.AddTransient<IAccountClosurePrerequisitesProvider, AccountClosurePrerequisitesProvider>();
            services.AddTransient<IClientAccountBlockedStateProvider, ClientAccountBlockedStateProvider>();
            services.AddTransient<IAccountStateMachineFactory, AccountStateMachineFactory>();
            services.AddTransient<IClientAccountBlockedStateFactory, ClientAccountBlockedStateFactory>();
            services.AddTransient<IAccountClosureScheduler, AccountClosureScheduler>();
            services.AddTransient<IRequestHandler<GetLegalDocumentQuery, DocumentResponseModel>, GetLegalDocumentQueryHandler>();
            services.AddTransient<IRequestHandler<GetLegalDocumentsQuery, List<DocumentResponseModel>>, GetLegalDocumentsQueryHandler>();

            services.AddTransient<IRequestHandler<GetAllGroupsQuery, GetAllGroupsResponse>, GetAllGroupsQueryHandler>();
            services.AddTransient<IRequestHandler<GetUserGroupsQuery, IEnumerable<Group>>, GetUserGroupsQueryHandler>();
            services.AddTransient<IRequestHandler<GetUsersInGroupQuery, GroupViewModel>, GetUsersInGroupQueryHandler>();
            services.AddTransient<IRequestHandler<CreateGroupQuery, GroupViewModel>, CreateGroupQueryHandler>();
            services.AddTransient<IRequestHandler<DeleteGroupCommand, Unit>, DeleteGroupCommandHandler>();
            services.AddTransient<IRequestHandler<UpdateGroupCommand, GroupViewModel>, UpdateGroupCommandHandler>();
            services.AddTransient<IRequestHandler<GetAllInfoPopupBannersQuery, GetAllInfoPopupBannersResponse>, GetAllInfoPopupBannersQueryHandler>();
            services.AddTransient<IRequestHandler<CreateInfoPopupBannerQuery, InfoPopupBannerInstanceSettings>, CreateInfoPopupBannerQueryHandler>();
            services.AddTransient<IRequestHandler<UpdateInfoPopupBannerQuery, InfoPopupBannerInstanceSettings>, UpdateInfoPopupBannerQueryHandler>();
            services.AddTransient<IRequestHandler<AddUsersToGroupQuery, AddOrRemoveUsersFromGroupResponse>, AddUsersToGroupQueryHandler>();
            services.AddTransient<IRequestHandler<UpdateGroupCommand, GroupViewModel>, UpdateGroupCommandHandler>();
            services.AddTransient<IRequestHandler<ExcludeUsersFromGroupCommand, AddOrRemoveUsersFromGroupResponse>, ExcludeUsersFromGroupCommandHandler>();
            services.AddTransient<IRequestHandler<DeleteInfoPopupCommand, Unit>, DeleteInfoPopupCommandHandler>();

            services.AddTransient<INotificationHandler<DomainEventNotification<UserBlockedEvent>>, UserBlockedEventHandler>();
            services.AddTransient<INotificationHandler<DomainEventNotification<UserUnblockedEvent>>, UserUnblockedEventHandler>();
            services.AddTransient<INotificationHandler<DomainEventNotification<OnboardingStepFailedEvent>>, OnboardingStepFailedEventHandler>();
            services.AddTransient<INotificationHandler<DomainEventNotification<OnboardingStepSuccessEvent>>, OnboardingStepSuccessEventHandler>();
            services.AddTransient<IRequestHandler<ChangePasswordCommand, Unit>, ChangePasswordCommandHandler>();
            services.AddTransient<IRequestHandler<CreateAdminApiUserCommand, AdminApiUser>, CreateAdminApiUserCommandHandler>();
            services.AddTransient<IRequestHandler<UpdateAdminApiUserCommand, AdminApiUser>, UpdateAdminApiUserCommandHandler>();
            services.AddTransient<IRequestHandler<GetAdminApiUsersCommand, List<AdminApiUser>>, GetAdminApiUsersCommandHandler>();
            services.AddTransient<IRequestHandler<GetAdminApiUserByIdQuery, AdminApiUser>, GetAdminApiUserByIdQueryHandler>();
            services.AddTransient<IRequestHandler<SyncDebitCardStatusesCommand, Unit>, SyncDebitCardStatusesCommandHandler>();
            services.AddTransient<IRequestHandler<AddOrUpdatePlaceholdersCommand, Unit>, AddOrUpdatePlaceholdersCommandHandler>();
            services.AddTransient<IRequestHandler<UpdatePlaceholdersArrayCommand, Unit>, UpdatePlaceholdersArrayCommandHandler>();
            services.AddTransient<IRequestHandler<GetPlaceholdersQuery, PlaceholdersModel>, GetPlaceholdersQueryHandler>();
            services.AddTransient<IRequestHandler<GetClientDocumentsQuery, List<ClientDocument>>, GetClientDocumentsQueryHandler>();
            services.AddTransient<IRequestHandler<GetUserUpdatesQuery, List<UserUpdate>>, GetUserUpdatesQueryHandler>();
            services.AddTransient<IRequestHandler<UpdateClientFullNameCommand, Unit>, UpdateClientFullNameCommandHandler>();
            services.AddTransient<IRequestHandler<GetExternalCardsHistoryQuery, List<ExternalCardOperationHistory>>, GetExternalCardsHistoryQueryHandler>();
            services.AddTransient<IRequestHandler<GetTransferErrorLogsQuery, PaginatedList<TransferErrorLogViewModel>>, GetTransferErrorLogsQueryHandler>();
            services.AddTransient<IRequestHandler<GetUserCashbackHistoryQuery, UserCashbackHistoryViewModel>, GetUserCashbackHistoryQueryHandler>();
            services.AddTransient<IRequestHandler<UpdateLegalDocumentCommand, Unit>, UpdateLegalDocumentCommandHandler>();
            services.AddTransient<IRequestHandler<MarkLoansAsClosedIfNeededCommand, Unit>, MarkLoansAsClosedIfNeededCommandHandler>();
            services.AddSingleton<IDateTimeProviderService, DateTimeProviderService>();
            services.AddSingleton<IDateTimeUtcProviderService, DateTimeProviderService>();
            services.AddTransient<IRequestHandler<GetCreditScoreBasicOfferInfoScreenQuery, OptionViewModelResult<CreditScoreBasicOfferInfoScreenViewModel>>, GetCreditScoreBasicOfferInfoScreenQueryHandler>();
            services.AddTransient<IRequestHandler<GetCreditScoreBasicOfferConfirmationDialogQuery, GenericConfirmationDialogViewModel>, GetCreditScoreBasicOfferConfirmationDialogQueryHandler>();
            services.AddTransient<IRequestHandler<ActivateCheckCreditScorePaidServiceQuery, GenericActivateServiceResultDialogViewModel>, ActivateCheckCreditScorePaidServiceQueryHandler>();
            services.AddTransient<IRequestHandler<GetTariffInfoQuery, TariffPlansInfoAdminDto>, GetTariffInfoQueryHandler>();
            services.AddTransient<IRequestHandler<GetAdvancesHistoryQuery, AdvancesHistoryViewModel>, GetAdvancesHistoryQueryHandler>();
            services.AddTransient<IRequestHandler<GetDelayedBoostInfoQuery, UserDelayedBoostInfoViewModel>, GetDelayedBoostInfoQueryHandler>();
            services.AddTransient<IRequestHandler<CreateCurrentAccountsIfNeededCommand, Unit>, CreateCurrentAccountsIfNeededCommandHandler>();

            return services;
        }


        internal static IServiceCollection ConfigureAndValidateApplicationSettings<T>(
            this IServiceCollection @this,
            IConfiguration configuration) where T : class, IApplicationSettings
            => @this.Configure<T>(configuration.GetSection(typeof(T).Name))
                .PostConfigure<T>(settings =>
                {
                    var configurationErrors = settings.ValidateApplicationSettings().ToArray();
                    if (configurationErrors.Any())
                    {
                        var aggregatedErrors = string.Join("\n", configurationErrors);
                        throw new ApplicationException(
                            $"Found {configurationErrors.Length} configuration error(s) in {typeof(T).Name}:\n{aggregatedErrors}");
                    }
                });
    }
}
