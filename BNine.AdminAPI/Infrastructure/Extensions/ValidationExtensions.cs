﻿namespace BNine.AdminAPI.Infrastructure.Extensions
{
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using BNine.Settings;

    public static class ValidationExtensions
    {
        public static IEnumerable<string> ValidateApplicationSettings(this IApplicationSettings @this)
        {
            var context = new ValidationContext(@this, serviceProvider: null, items: null);
            var results = new List<ValidationResult>();
            Validator.TryValidateObject(@this, context, results, true);
            foreach (var validationResult in results)
            {
                yield return validationResult.ErrorMessage;
            }
        }
    }
}
