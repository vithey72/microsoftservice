﻿namespace BNine.AdminAPI.Infrastructure.Filters
{
    using System;
    using System.Collections.Generic;
    using BNine.AdminAPI.Infrastructure.Exceptions;
    using BNine.Application.Exceptions;
    using Microsoft.AspNetCore.Http;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Filters;

    public class ApiExceptionFilterAttribute : ExceptionFilterAttribute
    {
        protected readonly IDictionary<Type, Action<ExceptionContext>> ExceptionHandlers;

        public ApiExceptionFilterAttribute()
        {
            ExceptionHandlers = new Dictionary<Type, Action<ExceptionContext>>
            {
                { typeof(ValidationException), HandleValidationException },
                { typeof(NotFoundException), HandleNotFoundException },
                { typeof(UnauthorizedAccessException), HandleUnauthorizedAccessException },
            };
        }

        public override void OnException(ExceptionContext context)
        {
            HandleException(context);

            //var requestTelemetry = context.HttpContext.Features.Get<RequestTelemetry>();
            //requestTelemetry?.Properties.Add("Exception", context.Exception.ToString());

            base.OnException(context);
        }

        private void HandleException(ExceptionContext context)
        {
            var type = context.Exception.GetType();
            if (ExceptionHandlers.ContainsKey(type))
            {
                ExceptionHandlers[type].Invoke(context);
                return;
            }

            if (!context.ModelState.IsValid)
            {
                HandleInvalidModelStateException(context);
                return;
            }

            // We don't need to hide details from internal users
            // HandleUnknownException(context);
        }

        private void HandleUnknownException(ExceptionContext context)
        {
            var details = new ProblemDetails
            {
                Status = StatusCodes.Status500InternalServerError,
                Title = "An error occurred while processing your request.",
                Type = "https://tools.ietf.org/html/rfc7231#section-6.6.1"
            };

            context.Result = new ObjectResult(details)
            {
                StatusCode = StatusCodes.Status500InternalServerError
            };

            context.ExceptionHandled = true;
        }

        protected void HandleInvalidModelStateException(ExceptionContext context)
        {
            var details = new ValidationProblemDetails(context.ModelState)
            {
                Type = "https://tools.ietf.org/html/rfc7231#section-6.5.1"
            };

            context.Result = new BadRequestObjectResult(details);

            context.ExceptionHandled = true;
        }

        private void HandleValidationException(ExceptionContext context)
        {
            context.Result =
                new ObjectResult(new ApplicationValidationException(
                    ((ValidationException)context.Exception).Errors,
                    ((ValidationException)context.Exception).ErrorCodes))
                {
                    StatusCode = StatusCodes.Status400BadRequest
                };

            context.ExceptionHandled = true;
        }

        private void HandleUnauthorizedAccessException(ExceptionContext context)
        {
            context.Result = new UnauthorizedObjectResult(null);
            context.ExceptionHandled = true;
        }

        private void HandleNotFoundException(ExceptionContext context)
        {
            var exception = (NotFoundException)context.Exception;

            context.Result = new ObjectResult(exception.Message)
            {
                StatusCode = StatusCodes.Status404NotFound
            };

            context.ExceptionHandled = true;
        }
    }
}
