﻿namespace BNine.AdminAPI.Infrastructure.Exceptions
{
    using System.Collections.Generic;
    using Newtonsoft.Json;

    public class ApplicationValidationException
    {
        [JsonProperty("errors")]
        public IDictionary<string, string[]> Failures
        {
            get;
        }

        [JsonProperty("errorCodes")]
        public IDictionary<string, string[]> ErrorCodes
        {
            get;
        }

        public ApplicationValidationException(IDictionary<string, string[]> failures, IDictionary<string, string[]> errorCodes)
        {
            Failures = failures;
            ErrorCodes = errorCodes;
        }
    }
}
