﻿namespace BNine.AdminAPI.Controllers
{
    using System.Net;
    using System.Threading.Tasks;
    using BNine.Application.Aggregates.EarlySalary.ArgyleSettings.Command.UpdateArgyleSettings;
    using BNine.Application.Aggregates.EarlySalary.ArgyleSettings.Queries.GetArgyleSettings;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.Extensions.Logging;

    [Authorize]
    [Route("argyle-settings")]
    public class ArgyleSettingsController : ApiController
    {
        public ArgyleSettingsController(ILogger<ArgyleSettingsController> logger) : base(logger)
        {
        }

        /// <summary>
        /// Get argyle settings
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [ProducesResponseType((int)HttpStatusCode.Unauthorized)]
        [ProducesResponseType(typeof(ArgyleSettingsInfo), (int)HttpStatusCode.OK)]
        [Authorize(Roles = "Admin, Agent, LightAgent, Viewer, ApiAdmin")]
        public async Task<IActionResult> Get()
        {
            var result = await Mediator.Send(new GetArgyleSettingsQuery());
            return Ok(result);
        }

        /// <summary>
        /// Put argyle settings
        /// </summary>
        /// <returns></returns>
        [HttpPut]
        [ProducesResponseType((int)HttpStatusCode.Unauthorized)]
        [ProducesResponseType((int)HttpStatusCode.NoContent)]
        [Authorize(Roles = "Admin, Agent, LightAgent, ApiAdmin")]
        public async Task<IActionResult> Put([FromBody] UpdateArgyleSettingsCommand command)
        {
            await Mediator.Send(command);
            return NoContent();
        }
    }
}
