﻿namespace BNine.AdminAPI.Controllers;

using System.Threading.Tasks;
using Application.Aggregates.AdminUser.Commands;
using Application.Aggregates.AdminUser.Queries;
using Application.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

//Do not expose swagger for this controller
[ApiExplorerSettings(IgnoreApi = true)]
[Authorize]
[Route("profile")]
public class AccountsController : ApiController
{
    private readonly ICurrentUserService _currentUserService;

    public AccountsController(ILogger<AccountsController> logger, ICurrentUserService currentUserService) : base(logger)
    {
        _currentUserService = currentUserService;
    }

    [HttpGet]
    public async Task<IActionResult> GetProfile()
    {
        var userId = _currentUserService.UserId;

        if (!userId.HasValue)
        {
            return BadRequest("Please contact administrator");
        }

        var command = new GetAdminApiUserByIdQuery()
        {
            Id = userId.Value
        };

        var result = await Mediator.Send(command);

        return Ok(result);
    }

    [HttpPost("change-password")]
    public async Task<IActionResult> ChangePassword([FromBody] ChangePasswordCommand command)
    {
        var userId = _currentUserService.UserId;

        if (!userId.HasValue)
        {
            return BadRequest("Please contact administrator");
        }

        command.UserId = userId.Value;

        var result = await Mediator.Send(command);

        return Ok(result);
    }
}
