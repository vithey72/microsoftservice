﻿namespace BNine.AdminAPI.Controllers;

using System;
using System.Threading.Tasks;
using Application.Aggregates.AdminUser.Commands;
using Application.Aggregates.AdminUser.Queries;
using Application.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

//Do not expose swagger for this controller
[ApiExplorerSettings(IgnoreApi = true)]
[Authorize]
[Route("admin-user")]
public class AdminApiUsersController : ApiController
{
    private readonly ICurrentUserService _currentUserService;

    public AdminApiUsersController(ILogger<AdminApiUsersController> logger, ICurrentUserService currentUserService) : base(logger)
    {
        _currentUserService = currentUserService;
    }

    [HttpGet]
    [Authorize(Roles = "Admin")]
    public async Task<IActionResult> GetAdminApiUsers()
    {
        var result = await Mediator.Send(new GetAdminApiUsersCommand());

        return Ok(result);
    }

    [HttpPost]
    [Authorize(Roles = "Admin")]
    public async Task<IActionResult> CreateAdminUser([FromBody] CreateAdminApiUserCommand command)
    {
        var result = await Mediator.Send(command);

        return Ok(result);
    }

    [HttpPut("{id}")]
    [Authorize(Roles = "Admin")]
    public async Task<IActionResult> EditAdminUser([FromRoute] Guid id, [FromBody] UpdateAdminApiUserCommand command)
    {
        command.UserId = id;

        var result = await Mediator.Send(command);

        return Ok(result);
    }
}
