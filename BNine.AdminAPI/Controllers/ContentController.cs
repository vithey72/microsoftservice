﻿namespace BNine.AdminAPI.Controllers;

using System.Threading.Tasks;
using Application.Aggregates.Configuration.Commands.AddContent;
using Application.Aggregates.Configuration.Commands.UpdateConfiguration;
using Application.Aggregates.Configuration.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

[Authorize]
[Route("content")]
public class ContentController : ApiController
{
    public ContentController(ILogger<ContentController> logger) : base(logger)
    {
    }

    /// <summary>
    /// Create or Update file
    /// </summary>
    /// <param name="command"><see cref="AddContentCommand"/></param>
    /// <returns></returns>
    [Authorize(Roles = "Admin, ApiAdmin")]
    [HttpPut("file")]
    [ProducesResponseType(typeof(UpdatedDocumentResponseModel), Status200OK)]
    [ProducesResponseType(Status400BadRequest)]
    public async Task<IActionResult> UploadFile([FromForm] AddContentCommand command)
    {
        var response = await Mediator.Send(command);
        return Ok(response);
    }

    /// <summary>
    /// Update current configuration
    /// </summary>
    /// <param name="command"><see cref="UpdateConfigurationCommand"/></param>
    /// <returns></returns>
    [Authorize(Roles = "Admin, ApiAdmin")]
    [HttpPut]
    [ProducesResponseType(Status204NoContent)]
    [ProducesResponseType(Status400BadRequest)]
    public async Task<IActionResult> UpdateConfiguration(UpdateConfigurationCommand command)
    {
        await Mediator.Send(command);
        return NoContent();
    }
}
