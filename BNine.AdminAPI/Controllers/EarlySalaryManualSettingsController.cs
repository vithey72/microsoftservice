﻿namespace BNine.AdminAPI.Controllers
{
    using System.Net;
    using System.Threading.Tasks;
    using BNine.Application.Aggregates.EarlySalary.EarlySalaryManualSettings.Command.UpdateEarlySalaryManualSettings;
    using BNine.Application.Aggregates.EarlySalary.EarlySalaryManualSettings.Queries.GetArgyleSettings;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.Extensions.Logging;

    [Authorize]
    [Route("early-salary-manual-settings")]
    public class EarlySalaryManualSettingsController : ApiController
    {
        public EarlySalaryManualSettingsController(ILogger<EarlySalaryManualSettingsController> logger) : base(logger)
        {
        }

        /// <summary>
        /// Get early salary manual form settings
        /// </summary>
        /// <returns></returns>
        [Authorize(Roles = "Admin, Agent, LightAgent, Viewer, ApiAdmin")]
        [HttpGet]
        [ProducesResponseType((int)HttpStatusCode.Unauthorized)]
        [ProducesResponseType(typeof(EarlySalaryManualInfo), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> Get()
        {
            var result = await Mediator.Send(new GetEarlySalaryManualSettingsQuery());
            return Ok(result);
        }

        /// <summary>
        /// Put early salary manual form settings
        /// </summary>
        /// <returns></returns>
        [Authorize(Roles = "Admin, ApiAdmin")]
        [HttpPut]
        [ProducesResponseType((int)HttpStatusCode.Unauthorized)]
        [ProducesResponseType((int)HttpStatusCode.NoContent)]
        public async Task<IActionResult> Put([FromBody] UpdateEarlySalaryManualSettingsCommand command)
        {
            await Mediator.Send(command);
            return NoContent();
        }
    }
}
