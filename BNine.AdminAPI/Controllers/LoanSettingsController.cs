﻿namespace BNine.AdminAPI.Controllers
{
    using System.Collections.Generic;
    using System.Net;
    using System.Threading.Tasks;
    using BNine.Application.Aggregates.LoanSettings.Queries.GetACHPrincipalKeywords;
    using BNine.Application.Aggregates.Settings.LoanSettings.Commands.UpdateLoanSettings;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.Extensions.Logging;

    [Authorize]
    [Route("loan-settings")]
    public class LoanSettingsController : ApiController
    {
        public LoanSettingsController(ILogger<LoanSettingsController> logger) : base(logger)
        {
        }

        /// <summary>
        /// Get ACH principal keywords
        /// </summary>
        /// <returns></returns>
        [Authorize(Roles = "Admin, Agent, LightAgent, Viewer, ApiAdmin")]
        [HttpGet("principal-keywords")]
        [ProducesResponseType((int)HttpStatusCode.Unauthorized)]
        [ProducesResponseType(typeof(IEnumerable<string>), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> Get()
        {
            var result = await Mediator.Send(new GetACHPrincipalKeywordsQuery());
            return Ok(result);
        }

        /// <summary>
        /// Set ACH principal keywords
        /// </summary>
        /// <param name="command"></param>
        /// <returns></returns>
        [Authorize(Roles = "Admin, ApiAdmin")]
        [HttpPost("principal-keywords")]
        [ProducesResponseType((int)HttpStatusCode.Unauthorized)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        [ProducesResponseType((int)HttpStatusCode.NoContent)]
        public async Task<IActionResult> AddKeyword(AddPayrollKeywordCommand command)
        {
            await Mediator.Send(command);
            return NoContent();
        }
    }
}
