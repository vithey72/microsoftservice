﻿namespace BNine.AdminAPI.Controllers;

using Application.Aggregates.Configuration.Models;
using Application.Aggregates.StaticDocuments.Commands;
using Application.Aggregates.StaticDocuments.Models;
using Application.Aggregates.StaticDocuments.Queries;
using Infrastructure.Exceptions;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

/// <summary>
/// Controller for both mobile clients and admin clients querying of static legal documents.
/// </summary>
[Authorize]
[Route("legal-documents")]
public class LegalDocumentsController : ApiController
{
    /// <inheritdoc />
    public LegalDocumentsController(ILogger<LegalDocumentsController> logger) : base(logger)
    {
    }

    /// <summary>
    /// Get a list of all documents known to system and their details, including download link.
    /// Should be without sensitive data and stay under anonymous access as used from the BNine sales external website
    /// </summary>
    [HttpGet]
    [Route("")]
    [AllowAnonymous]
    [ProducesResponseType(typeof(List<DocumentResponseModel>), Status200OK)]
    [ProducesResponseType(typeof(ApplicationValidationException), Status400BadRequest)]
    public async Task<IActionResult> GetDocuments()
    {
        var response = await Mediator.Send(new GetLegalDocumentsQuery());
        return Ok(response);
    }

    [HttpPut]
    [Route("update")]
    [ProducesResponseType(Status204NoContent)]
    [ProducesResponseType(Status404NotFound)]
    [ProducesResponseType(typeof(ApplicationValidationException), Status400BadRequest)]
    public async Task<IActionResult> UpdateDocument([FromForm] UploadNewDocumentVersion uploadedDocument, CancellationToken cancellationToken)
    {
        if (uploadedDocument?.File == null)
        {
            return BadRequest("File wasn't provided");
        }

        var command = new UpdateLegalDocumentCommand(
            documentKey: uploadedDocument.DocumentKey,
            uploadedDocument.File.ContentType,
            fileExtension: Path.GetExtension(uploadedDocument.File.FileName));

        using var memoryStream = new MemoryStream();
        await uploadedDocument.File.CopyToAsync(memoryStream, cancellationToken);
        command.FileData = memoryStream.ToArray();

        await Mediator.Send(command, cancellationToken);
        return NoContent();
    }
}
