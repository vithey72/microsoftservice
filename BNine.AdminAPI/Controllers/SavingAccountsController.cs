﻿namespace BNine.AdminAPI.Controllers
{
    using System;
    using System.Net;
    using System.Threading.Tasks;
    using Application.Aggregates.CheckingAccounts.Queries.GetAccountStatementAsAdmin;
    using BNine.Application.Aggregates.CheckingAccounts.Commands.BlockSavingAccount;
    using BNine.Application.Aggregates.CheckingAccounts.Commands.UnblockSavingAccount;
    using BNine.Application.Aggregates.CheckingAccounts.Queries.GetSavingAccountInfo;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.Extensions.Logging;

    [Authorize]
    [Route("clients/{clientId}/saving-account")]
    public class SavingAccountsController : ApiController
    {
        public SavingAccountsController(ILogger<SavingAccountsController> logger) : base(logger)
        {
        }

        /// <summary>
        /// Get saving account info
        /// </summary>
        /// <param name="clientId"></param>
        /// <returns></returns>
        [Authorize(Roles = "Admin, Agent, LightAgent, Viewer, ApiAdmin")]
        [HttpGet]
        [ProducesResponseType(typeof(SavingAccountInfo), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.Unauthorized)]
        [ProducesResponseType((int)HttpStatusCode.NotFound)]
        public async Task<IActionResult> Get([FromRoute] Guid clientId)
        {
            var result = await Mediator.Send(new GetSavingAccountInfoQuery(clientId));
            return Ok(result);
        }

        /// <summary>
        /// Block saving account
        /// </summary>
        /// <param name="clientId"></param>
        /// <returns></returns>
        [Authorize(Roles = "Admin, Agent, ApiAdmin")]
        [HttpPost("block")]
        [ProducesResponseType((int)HttpStatusCode.NoContent)]
        [ProducesResponseType((int)HttpStatusCode.Unauthorized)]
        [ProducesResponseType((int)HttpStatusCode.NotFound)]
        public async Task<IActionResult> Block([FromRoute] Guid clientId, [FromBody] BlockSavingAccountCommand command)
        {
            command.UserId = clientId;
            await Mediator.Send(command);
            return NoContent();
        }

        /// <summary>
        /// Unblock saving account
        /// </summary>
        /// <param name="clientId"></param>
        /// <returns></returns>
        [Authorize(Roles = "Admin, Agent, ApiAdmin")]
        [HttpPost("unblock")]
        [ProducesResponseType((int)HttpStatusCode.NoContent)]
        [ProducesResponseType((int)HttpStatusCode.Unauthorized)]
        [ProducesResponseType((int)HttpStatusCode.NotFound)]
        public async Task<IActionResult> Unblock([FromRoute] Guid clientId)
        {
            await Mediator.Send(new UnblockSavingAccountCommand(clientId));
            return NoContent();
        }

        [Authorize(Roles = "Admin, Agent, LightAgent, Viewer, ApiAdmin")]
        [HttpGet("statement")]
        [ProducesResponseType(Status200OK)]
        public async Task<IActionResult> GetAccountStatement([FromRoute] Guid clientId, [FromQuery] string email,
            [FromQuery] DateTime? startDate, [FromQuery] DateTime? endDate)
        {
            await Mediator.Send(new GetAccountStatementAsAdminQuery
            {
                UserId = clientId,
                SendToEmail = email,
                StartingDate = startDate,
                EndingDate = endDate,
            });

            return Ok();
        }

        [Authorize(Roles = "Admin, Agent, LightAgent, Viewer, ApiAdmin")]
        [HttpGet("statement/download")]
        [ProducesResponseType(Status200OK)]
        public async Task<IActionResult> DownloadAccountStatement([FromRoute] Guid clientId,
            [FromQuery] DateTime? startDate, [FromQuery] DateTime? endDate)
        {
            var result = await Mediator.Send(new DownloadAccountStatementAsAdminQuery
            {
                UserId = clientId,
                StartingDate = startDate,
                EndingDate = endDate,
            });

            return Ok(result);
        }
    }
}
