﻿namespace BNine.AdminAPI.Controllers;

using System;
using System.Net;
using System.Threading.Tasks;
using Application.Aggregates.ExternalCards.Queries.GetExternalCardsHistory;
using Application.Aggregates.LogAdminAction.Queries;
using Application.Aggregates.TransferErrorLog.Queries;
using Application.Aggregates.Users.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

[Authorize(Roles = "Admin, Agent, LightAgent, Viewer, ApiAdmin")]
[Route("activity-log")]
public class ActivityLogController: ApiController
{
    public ActivityLogController(ILogger<ActivityLogController> logger) : base(logger)
    {
    }

    [HttpGet("{clientId}/user-updates")]
    [ProducesResponseType((int)HttpStatusCode.Unauthorized)]
    [ProducesResponseType(typeof(ClientDocument), (int)HttpStatusCode.OK)]
    public async Task<IActionResult> GetUserUpdates([FromRoute] Guid clientId)
    {
        var result = await Mediator.Send(new GetUserUpdatesQuery()
        {
            ClientId = clientId
        });

        return Ok(result);
    }

    [HttpGet("{clientId}/external-cards")]
    [ProducesResponseType((int)HttpStatusCode.Unauthorized)]
    [ProducesResponseType(typeof(ClientDocument), (int)HttpStatusCode.OK)]
    public async Task<IActionResult> GetExternalCardsOperationHistory([FromRoute] Guid clientId)
    {
        var result = await Mediator.Send(new GetExternalCardsHistoryQuery()
        {
            ClientId = clientId
        });

        return Ok(result);
    }

    [HttpGet("{clientId}/transfer-errors")]
    [ProducesResponseType((int)HttpStatusCode.Unauthorized)]
    [ProducesResponseType(typeof(ClientDocument), (int)HttpStatusCode.OK)]
    public async Task<IActionResult> GetTransferErrorLogs([FromRoute] Guid clientId)
    {
        var result = await Mediator.Send(new GetTransferErrorLogsQuery()
        {
            ClientId = clientId
        });

        return Ok(result);
    }
}
