﻿namespace BNine.AdminAPI.Controllers;

using Application.Aggregates.Users.Commands.AddCurrentAccountsIfNeeded;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

[Route("client-bank-accounts")]
public class ClientCurrentAccountsController : ApiController
{
    private readonly ILogger<ClientCurrentAccountsController> _logger;
    private readonly IMediator _mediator;

    public ClientCurrentAccountsController(
        ILogger<ClientCurrentAccountsController> logger,
        IMediator mediator
        )
        : base(logger)
    {
        _logger = logger;
        _mediator = mediator;
    }

    [HttpPost]
    [Authorize(Roles = "Admin, Agent, LightAgent, ApiAdmin")]
    [ProducesResponseType(Status401Unauthorized)]
    [ProducesResponseType(Status200OK)]
    public async Task<IActionResult> CreateCurrentAccountsIfDontExist([FromQuery] Guid userId)
    {
        await _mediator.Send(new CreateCurrentAccountsIfNeededCommand(userId));
        return Ok();
    }

}
