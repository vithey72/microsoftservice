﻿namespace BNine.AdminAPI.Controllers;

using System;
using System.Net;
using System.Threading.Tasks;
using Application.Aggregates.Users.Models.UserCashback;
using Application.Aggregates.Users.Queries;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

[Authorize(Roles = "Admin, Agent, LightAgent, Viewer, ApiAdmin")]
[Route("client-cashback")]
public class ClientCashbackController : ApiController
{
    public ClientCashbackController(ILogger<ClientCashbackController> logger) : base(logger)
    {
    }

    [HttpGet("{clientId}/history")]
    [ProducesResponseType((int)HttpStatusCode.Unauthorized)]
    [ProducesResponseType(typeof(UserCashbackHistoryViewModel), (int)HttpStatusCode.OK)]
    public async Task<IActionResult> GetCashbackHistory([FromRoute] Guid clientId)
    {
        var result = await Mediator.Send(new GetUserCashbackHistoryQuery()
        {
            UserId = clientId
        });

        return Ok(result);
    }
}
