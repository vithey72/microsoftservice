﻿namespace BNine.AdminAPI.Controllers;

using System.Net;
using System.Threading.Tasks;
using Application.Aggregates.Users.Models;
using Application.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

[Authorize]
[Route("client-document-files")]
public class ClientDocumentFilesController : ApiController
{
    private readonly IClientDocumentsOriginalsPersistenceService _clientDocumentsOriginalsPersistenceService;

    public ClientDocumentFilesController(
        ILogger<ClientDocumentFilesController> logger,
        IClientDocumentsOriginalsPersistenceService clientDocumentsOriginalsPersistenceService) : base(logger)
    {
        _clientDocumentsOriginalsPersistenceService = clientDocumentsOriginalsPersistenceService;
    }

    /// <summary>
    /// Get document file in Base64 format
    /// </summary>
    [Authorize(Roles = "Admin, Agent, LightAgent, Viewer, ApiAdmin")]
    [HttpGet("{fileName}")]
    [ProducesResponseType((int)HttpStatusCode.Unauthorized)]
    [ProducesResponseType(typeof(ClientDocument), (int)HttpStatusCode.OK)]
    public async Task<IActionResult> DownloadDocument([FromRoute] string fileName)
    {
        var result = await _clientDocumentsOriginalsPersistenceService.DownloadClientDocumentFile(fileName);

        return Ok(result);
    }
}
