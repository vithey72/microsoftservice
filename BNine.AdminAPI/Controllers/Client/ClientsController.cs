﻿namespace BNine.AdminAPI.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Net;
    using System.Threading.Tasks;
    using Application.Aggregates.Users.Commands.Block;
    using Application.Aggregates.Users.Commands.CloseAccount.CloseClientAccount;
    using Application.Aggregates.Users.Commands.EnableSurveyForUser;
    using Application.Aggregates.Users.Commands.Unblock;
    using Application.Aggregates.Users.Commands.UpdateClientFullName;
    using Application.Aggregates.Users.Models;
    using Application.Aggregates.Users.Models.UserAdvancesHistory;
    using Application.Aggregates.Users.Models.UserDelayedBoostInfo;
    using Application.Aggregates.Users.Queries.CloseAccount.ValidateCloseClientAccount;
    using Application.Aggregates.Users.Queries.GetAdvancesHistory;
    using Application.Aggregates.Users.Queries.GetDelayedBoostInfo;
    using Application.Aggregates.Users.Queries.GetOnboardingDetails;
    using Application.Aggregates.Users.Queries.GetTariffInfo;
    using Application.Aggregates.Users.Queries.GetUserDetails;
    using Application.Aggregates.Users.Queries.GetUsers;
    using Application.Exceptions;
    using Application.Models;
    using BNine.Application.Aggregates.Users.Commands.Update.ForceUpdateAddress;
    using BNine.Application.Aggregates.Users.Commands.Update.ForceUpdatedEmail;
    using BNine.Application.Aggregates.Users.Commands.Update.ForceUpdatePhoneNumber;
    using BNine.Application.Aggregates.Users.Models.TransactionLimits;
    using BNine.Application.Aggregates.Users.Queries.GetTransactionLimitsQuery;
    using Enums;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.Extensions.Logging;

    [Authorize]
    [Route("clients")]
    public class ClientsController : ApiController
    {
        public ClientsController(ILogger<ClientsController> logger) : base(logger)
        {
        }

        /// <summary>
        /// Get list of clients
        /// </summary>
        /// <param name="search"></param>
        /// <param name="statuses"></param>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        [Authorize(Roles = "Admin, Agent, LightAgent, Viewer, ApiAdmin")]
        [HttpGet]
        [ProducesResponseType((int)HttpStatusCode.Unauthorized)]
        [ProducesResponseType(typeof(PaginatedList<UserInfoListItem>), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> Get([FromQuery] string search, [FromQuery] IEnumerable<UserStatus> statuses,
            [FromQuery] int? pageIndex, [FromQuery] int? pageSize)
        {
            var result = await Mediator.Send(new GetUsersQuery(search, statuses, pageIndex, pageSize));
            return Ok(result);
        }

        /// <summary>
        /// Get client details
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [Authorize(Roles = "Admin, Agent, LightAgent, Viewer, ApiAdmin")]
        [HttpGet("{id}")]
        [ProducesResponseType((int)HttpStatusCode.Unauthorized)]
        [ProducesResponseType(typeof(UserDetails), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> GetDetails([FromRoute] Guid id)
        {
            var result = await Mediator.Send(new GetUserDetailsQuery(id));
            return Ok(result);
        }

        /// <summary>
        /// Get client onboarding info
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [Authorize(Roles = "Admin, Agent, LightAgent, Viewer, ApiAdmin")]
        [HttpGet("{id}/onboarding-details")]
        [ProducesResponseType((int)HttpStatusCode.Unauthorized)]
        [ProducesResponseType(typeof(OnboardingDetails), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> GetOnboardingDetails([FromRoute] Guid id)
        {
            var result = await Mediator.Send(new GetOnboardingDetailsQuery(id));
            return Ok(result);
        }

        /// <summary>
        /// Get client transaction limits from dwh
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [Authorize(Roles = "Admin, Agent, LightAgent, Viewer, ApiAdmin")]
        [HttpGet("{id}/transaction-limits")]
        [ProducesResponseType((int)HttpStatusCode.Unauthorized)]
        [ProducesResponseType(typeof(UserTransactionLimitsViewModel), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> GetTransactionLimits([FromRoute] Guid id)
        {
            var result = await Mediator.Send(new GetUserTransactionLimitsQuery(id));
            return Ok(result);
        }

        /// <summary>
        /// Block user
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [Authorize(Roles = "Admin, Agent, LightAgent, ApiAdmin")]
        [HttpPost("{id}/block")]
        [ProducesResponseType(Status204NoContent)]
        [ProducesResponseType(Status401Unauthorized)]
        [ProducesResponseType(Status404NotFound)]
        public async Task<IActionResult> Block([FromRoute] Guid id, [FromBody] BlockUserCommand command)
        {
            command.Id = id;
            await Mediator.Send(command);
            return NoContent();
        }

        /// <summary>
        /// Unblock user
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [Authorize(Roles = "Admin, Agent, LightAgent, ApiAdmin")]
        [HttpPost("{id}/unblock")]
        [ProducesResponseType(Status204NoContent)]
        [ProducesResponseType(Status401Unauthorized)]
        [ProducesResponseType(Status404NotFound)]
        public async Task<IActionResult> Unblock([FromRoute] Guid id)
        {
            await Mediator.Send(new UnblockUserCommand(id));
            return NoContent();
        }

        /// <summary>
        /// Change user phone
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [Authorize(Roles = "Admin, Agent, ApiAdmin")]
        [HttpPut("{id}/phone")]
        [ProducesResponseType(Status204NoContent)]
        [ProducesResponseType(Status401Unauthorized)]
        [ProducesResponseType(Status404NotFound)]
        public async Task<IActionResult> UpdatePhone([FromRoute] Guid id,
            [FromBody] ForceUpdatePhoneNumberCommand forceUpdatePhoneNumberCommand)
        {
            forceUpdatePhoneNumberCommand.UserId = id;
            await Mediator.Send(forceUpdatePhoneNumberCommand);
            return NoContent();
        }

        /// <summary>
        /// Change user address
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [Authorize(Roles = "Admin, Agent, ApiAdmin")]
        [HttpPut("{id}/address")]
        [ProducesResponseType(Status204NoContent)]
        [ProducesResponseType(Status401Unauthorized)]
        [ProducesResponseType(Status404NotFound)]
        public async Task<IActionResult> UpdateAddress([FromRoute] Guid id,
            [FromBody] ForceUpdateAddressCommand forceUpdateAddressCommand)
        {
            forceUpdateAddressCommand.UserId = id;
            await Mediator.Send(forceUpdateAddressCommand);
            return NoContent();
        }

        /// <summary>
        /// Change user email
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [Authorize(Roles = "Admin, Agent, LightAgent, ApiAdmin")]
        [HttpPut("{id}/email")]
        [ProducesResponseType(Status204NoContent)]
        [ProducesResponseType(Status401Unauthorized)]
        [ProducesResponseType(Status404NotFound)]
        public async Task<IActionResult> UpdateEmail([FromRoute] Guid id,
            [FromBody] ForceUpdateEmailCommand forceUpdateEmailCommand)
        {
            forceUpdateEmailCommand.UserId = id;
            await Mediator.Send(forceUpdateEmailCommand);
            return NoContent();
        }

        /// <summary>
        /// Change user name
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [Authorize(Roles = "Admin, Agent, LightAgent, ApiAdmin")]
        [HttpPut("{id}/full-name")]
        [ProducesResponseType(Status204NoContent)]
        [ProducesResponseType(Status401Unauthorized)]
        [ProducesResponseType(Status404NotFound)]
        [ProducesResponseType(typeof(ValidationProblemDetails), (int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> UpdateClientFullName([FromRoute] Guid id,
            [FromBody] UpdateClientFullNameCommand command)
        {
            command.UserId = id;
            await Mediator.Send(command);
            return NoContent();
        }

        [Authorize(Roles = "Admin, Agent, LightAgent, ApiAdmin")]
        [HttpPut("manual-form-survey")]
        [ProducesResponseType(Status204NoContent)]
        [ProducesResponseType(Status401Unauthorized)]
        [ProducesResponseType(Status404NotFound)]
        public async Task<IActionResult> EnableSurveyForUser(
            [FromBody] EnableSurveyForUserCommand enableSurveyForUserCommand)
        {
            await Mediator.Send(enableSurveyForUserCommand);
            return NoContent();
        }

        [Authorize(Roles = "Admin, Agent, ApiAdmin")]
        [HttpPost("{id}/close-account")]
        [ProducesResponseType((int)HttpStatusCode.NoContent)]
        [ProducesResponseType((int)HttpStatusCode.Unauthorized)]
        [ProducesResponseType(Status400BadRequest)]
        public async Task<IActionResult> CloseAccount(
            [FromRoute] Guid id, [FromBody] CloseClientAccountCommand closeClientAccountCommand)
        {
            var validationResult = await Mediator.Send(new ValidateCloseClientAccountQuery { UserId = id });
            if (!validationResult.CanClose)
            {
                throw new ValidationException("user", validationResult.GetTextForException());
            }
            closeClientAccountCommand.UserId = id;

            await Mediator.Send(closeClientAccountCommand);

            return NoContent();
        }

        [Authorize(Roles = "Admin, Agent, ApiAdmin")]
        [HttpGet("{id}/tariff-info")]
        [ProducesResponseType(typeof(TariffPlansInfoAdminDto), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.Unauthorized)]
        public  async Task<IActionResult> GetTariffInfo([FromRoute] Guid id)
        {
            var result = await Mediator.Send(new GetTariffInfoQuery(id));
            return Ok(result);
        }

        [Authorize(Roles = "Admin, Agent, ApiAdmin")]
        [HttpGet("{id}/advances-history")]
        [ProducesResponseType(typeof(AdvancesHistoryViewModel), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.Unauthorized)]
        public async Task<IActionResult> GetAdvancesHistory([FromRoute] Guid id)
        {
            var result = await Mediator.Send(new GetAdvancesHistoryQuery(id));
            return Ok(result);
        }


        [Authorize(Roles = "Admin, Agent, ApiAdmin")]
        [HttpGet("{id}/delayed-boost-info")]
        [ProducesResponseType(typeof(UserDelayedBoostInfoViewModel), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.Unauthorized)]
        public async Task<IActionResult> GetDelayedBoost([FromRoute] Guid id)
        {
            var result = await Mediator.Send(new GetDelayedBoostInfoQuery(id));
            return Ok(result);
        }



    }
}
