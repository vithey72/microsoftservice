﻿namespace BNine.AdminAPI.Controllers;

using System;
using System.Net;
using System.Threading.Tasks;
using Application.Aggregates.Users.Models;
using Application.Aggregates.Users.Queries.GetClientDocuments;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

[Authorize]
[Route("client-documents")]
public class ClientDocumentsController: ApiController
{
    public ClientDocumentsController(ILogger<ClientDocumentsController> logger) : base(logger)
    {
    }

    /// <summary>
    /// Get client documents including Identity Documents / Selfies
    /// </summary>
    [Authorize(Roles = "Admin, Agent, LightAgent, Viewer, ApiAdmin")]
    [HttpGet("{clientId}")]
    [ProducesResponseType((int)HttpStatusCode.Unauthorized)]
    [ProducesResponseType(typeof(ClientDocument), (int)HttpStatusCode.OK)]
    public async Task<IActionResult> GetClientDocuments([FromRoute] Guid clientId)
    {
        var result = await Mediator.Send(new GetClientDocumentsQuery()
        {
            ClientId = clientId
        });

        return Ok(result);
    }
}
