﻿namespace BNine.AdminAPI.Controllers
{
    using System.Threading.Tasks;
    using Application.Aggregates.Users.Commands.SwitchCashbackForUsers;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.Extensions.Logging;

    [Authorize(Roles = "Admin, Agent, LightAgent, ApiAdmin")]
    [Route("cashback")]
    public class CashbackController : ApiController
    {
        public CashbackController(ILogger<CashbackController> logger) : base(logger)
        {
        }

        [HttpPost("switch-for-users")]
        [ProducesResponseType(Status204NoContent)]
        public async Task<IActionResult> SwitchForUsers([FromBody] SwitchCashbackForUsersCommand command)
        {
            await Mediator.Send(command);
            return NoContent();
        }
    }
}
