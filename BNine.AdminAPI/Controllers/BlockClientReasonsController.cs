﻿namespace BNine.AdminAPI.Controllers
{
    using System.Collections.Generic;
    using System.Net;
    using System.Threading.Tasks;
    using Application.Aggregates.Users.Commands.SwitchCashbackForUsers;
    using BNine.Application.Aggregates.Users.Queries.GetBlockUserReasons;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.Extensions.Logging;

    [Authorize(Roles = "Admin, Agent, LightAgent, Viewer, ApiAdmin")]
    [Route("block-client-reasons")]
    public class BlockClientReasonsController : ApiController
    {
        public BlockClientReasonsController(ILogger<BlockClientReasonsController> logger) : base(logger)
        {
        }

        /// <summary>
        /// Get list of block client reasons
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [ProducesResponseType((int)HttpStatusCode.Unauthorized)]
        [ProducesResponseType(typeof(IEnumerable<BlockUserReasonListItem>), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> Get()
        {
            var result = await Mediator.Send(new GetBlockUserReasonsQuery());
            return Ok(result);
        }

        [Authorize(Roles = "Admin, Agent, LightAgent, ApiAdmin")]
        [HttpPost("switch-for-users")]
        [ProducesResponseType(Status204NoContent)]
        public async Task<IActionResult> SwitchForUsers([FromBody] SwitchCashbackForUsersCommand command)
        {
            await Mediator.Send(command);
            return NoContent();
        }
    }
}
