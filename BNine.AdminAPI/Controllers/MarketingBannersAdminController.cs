﻿namespace BNine.AdminAPI.Controllers;

using System.Threading.Tasks;
using Application.Aggregates.Banners.Commands.DeleteInfoPopup;
using Application.Aggregates.Banners.Models;
using Application.Aggregates.Banners.Queries.CreateInfoPopupBanner;
using Application.Aggregates.Banners.Queries.GetInfoPopupBanners;
using Application.Aggregates.Banners.Queries.UpdateInfoPopupBanner;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

[Authorize]
[Route("marketing-admin")]
public class MarketingBannersAdminController : ApiController
{
    public MarketingBannersAdminController(ILogger<MarketingBannersAdminController> logger) : base(logger)
    {
    }

    /// <summary>
    /// See all Info Popups.
    /// </summary>
    [Authorize(Roles = "Admin, Agent, LightAgent, Viewer, ApiAdmin")]
    [HttpGet("info-popup")]
    [ProducesResponseType(typeof(GetAllInfoPopupBannersResponse), Status200OK)]
    public async Task<IActionResult> GetInfoPopupBanners()
    {
        var banners = await Mediator.Send(new GetAllInfoPopupBannersQuery());
        return Ok(banners);
    }

    /// <summary>
    /// Create a new Info Popup. Name must be unique.
    /// </summary>
    [Authorize(Roles = "Admin, ApiAdmin")]
    [HttpPost("info-popup")]
    [ProducesResponseType(typeof(InfoPopupBannerInstanceSettings), Status200OK)]
    public async Task<IActionResult> CreateInfoPopupBanner(CreateInfoPopupBannerQuery query)
    {
        var banners = await Mediator.Send(query);
        return Ok(banners);
    }

    /// <summary>
    /// Update popup's settings. All settings must be included in the request.
    /// </summary>
    [Authorize(Roles = "Admin, ApiAdmin")]
    [HttpPut("info-popup")]
    [ProducesResponseType(typeof(InfoPopupBannerInstanceSettings), Status200OK)]
    public async Task<IActionResult> UpdateInfoPopupBanner(UpdateInfoPopupBannerQuery query)
    {
        var banners = await Mediator.Send(query);
        return Ok(banners);
    }

    /// <summary>
    /// Delete popup with this Id. Does not affect groups that it is connected with.
    /// </summary>
    [Authorize(Roles = "Admin, ApiAdmin")]
    [HttpDelete("info-popup/{id}")]
    [ProducesResponseType(typeof(InfoPopupBannerInstanceSettings), Status200OK)]
    public async Task<IActionResult> DeleteInfoPopupBanner(Guid id)
    {
        var banners = await Mediator.Send(new DeleteInfoPopupCommand(id));
        return NoContent();
    }
}
