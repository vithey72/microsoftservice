﻿namespace BNine.AdminAPI.Controllers;

using Application.Aggregates.Placeholders.Commands.AddOrUpdatePlaceholders;
using Application.Aggregates.Placeholders.Commands.UpdatePlaceholdersArray;
using Application.Aggregates.Placeholders.Models;
using Application.Aggregates.Placeholders.Queries;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

[Authorize]
[Route("placeholders")]
public class PlaceholdersController : ApiController
{
    public PlaceholdersController(ILogger<PlaceholdersController> logger)
        : base(logger)
    {
    }

    [HttpGet]
    [Authorize(Roles = "Admin, ApiAdmin")]
    [ProducesResponseType(typeof(PlaceholdersModel), Status200OK)]
    public async Task<IActionResult> GetPlaceholders([FromQuery] Guid userId)
    {
        var query = new GetPlaceholdersQuery(userId);
        var model = await Mediator.Send(query);

        return Ok(model);
    }

    [HttpPut]
    [Authorize(Roles = "Admin, ApiAdmin")]
    [ProducesResponseType(Status201Created)]
    [ProducesResponseType(Status202Accepted)]
    public async Task<IActionResult> AddOrUpdatePlaceholders([FromBody] PlaceholdersModel model,
        CancellationToken cancellationToken)
    {
        var command = new AddOrUpdatePlaceholdersCommand(model);
        await Mediator.Send(command, cancellationToken);

        return Accepted();
    }

    [HttpPut("array")]
    [Authorize(Roles = "Admin, ApiAdmin")]
    [ProducesResponseType(Status201Created)]
    [ProducesResponseType(Status202Accepted)]
    public async Task<IActionResult> AddOrUpdatePlaceholdersArray([FromBody] PlaceholdersModel[] model,
        CancellationToken cancellationToken)
    {
        var command = new UpdatePlaceholdersArrayCommand(model);
        await Mediator.Send(command, cancellationToken);

        return Accepted();
    }
}
