﻿namespace BNine.AdminAPI.Controllers;

using System;
using System.Threading.Tasks;
using Application.Aggregates.Features.Commands.DeleteGroup;
using Application.Aggregates.Features.Models;
using Application.Aggregates.Features.Queries.AddUsersToGroup;
using Application.Aggregates.Features.Queries.CreateGroup;
using Application.Aggregates.Features.Queries.ExcludeUsersFromGroup;
using Application.Aggregates.Features.Queries.GetAllGroups;
using Application.Aggregates.Features.Queries.UpdateGroup;
using Application.Aggregates.Users.Queries.GetUserGroups;
using Application.Aggregates.Users.Queries.GetUsersInGroup;
using Domain.Entities.Features;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

[Authorize]
[Route("features")]
public class FeaturesController : ApiController
{
    public FeaturesController(ILogger<FeaturesController> logger) : base(logger)
    {
    }

    [Authorize(Roles = "Admin, Agent, LightAgent, Viewer, ApiAdmin")]
    [HttpGet("groups")]
    [ProducesResponseType(typeof(GetAllGroupsResponse), Status200OK)]
    public async Task<IActionResult> GetAllGroups([FromQuery] GroupKind groupType, CancellationToken token)
    {
        var result = await Mediator.Send(new GetAllGroupsQuery(groupType), token);

        return Ok(result);
    }

    /// <summary>
    /// Get a list of groups that user is a member of.
    /// </summary>
    [Authorize(Roles = "Admin, Agent, LightAgent, Viewer, ApiAdmin")]
    [HttpGet("groups/by-user/{userId}")]
    [ProducesResponseType(typeof(Group[]), Status200OK)]
    public async Task<IActionResult> GetGroupsForUser([FromRoute] Guid userId)
    {
        var result = await Mediator.Send(new GetUserGroupsQuery(userId));
        result = result.GroupBy(x => x.Id).Select(x => x.First());
        foreach (var group in result)
        {
            group.FeatureGroups = null;
        }
        return Ok(result);
    }

    /// <summary>
    /// List all group members.
    /// </summary>
    [Authorize(Roles = "Admin, Agent, LightAgent, Viewer, ApiAdmin")]
    [HttpGet("groups/{groupId}")]
    [ProducesResponseType(typeof(GroupViewModel), Status200OK)]
    public async Task<IActionResult> GetUsersInGroup([FromRoute] Guid groupId, CancellationToken token)
    {
        var result = await Mediator.Send(new GetUsersInGroupQuery(groupId), token);
        return Ok(result);
    }

    /// <summary>
    /// Create an empty group. Supported types are UserList and Personal.
    /// TODO: creating Sample groups.
    /// </summary>
    [Authorize(Roles = "Admin, ApiAdmin")]
    [HttpPost("groups")]
    [ProducesResponseType(typeof(GroupViewModel), Status200OK)]
    public async Task<IActionResult> CreateGroup(CreateGroupQuery query)
    {
        var result = await Mediator.Send(query);
        return Ok(result);
    }

    /// <summary>
    /// Connect an array of Users with one Group. Users previously added remain untouched.
    /// </summary>
    [Authorize(Roles = "Admin, ApiAdmin")]
    [HttpPost("groups/{groupId}")]
    [ProducesResponseType(typeof(AddOrRemoveUsersFromGroupResponse), Status200OK)]
    public async Task<IActionResult> AddUsersToGroup([FromRoute] Guid groupId,
        [FromBody] AddUsersToGroupQuery query, CancellationToken token)
    {
        query = query with { GroupId = groupId };
        var result = await Mediator.Send(query, token);
        return Ok(result);
    }

    /// <summary>
    /// Change a group's name or list of its users.
    /// </summary>
    [Authorize(Roles = "Admin, ApiAdmin")]
    [HttpPut("groups/{groupId}")]
    [ProducesResponseType(typeof(GroupViewModel), Status200OK)]
    public async Task<IActionResult> UpdateGroup([FromRoute] Guid groupId,
        [FromBody] UpdateGroupCommand command, CancellationToken token)
    {
        command = command with { GroupId = groupId };
        var result = await Mediator.Send(command, token);
        return Ok(result);
    }

    /// <summary>
    /// Change a group's name or list of its users.
    /// </summary>
    [Authorize(Roles = "Admin, ApiAdmin")]
    [HttpPost("groups/{groupId}/exclude")]
    [ProducesResponseType(typeof(GroupViewModel), Status200OK)]
    public async Task<IActionResult> ExcludeUsersFromGroup([FromRoute] Guid groupId,
        [FromBody] ExcludeUsersFromGroupCommand command, CancellationToken token)
    {
        command = command with { GroupId = groupId };
        var result = await Mediator.Send(command, token);
        return Ok(result);
    }

    [Authorize(Roles = "Admin, ApiAdmin")]
    [HttpDelete("groups/{groupId}")]
    [ProducesResponseType(Status204NoContent)]
    public async Task<IActionResult> DeleteGroup([FromRoute] Guid groupId)
    {
        await Mediator.Send(new DeleteGroupCommand(groupId));
        return NoContent();
    }
}
