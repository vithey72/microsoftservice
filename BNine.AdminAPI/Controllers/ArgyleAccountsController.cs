﻿namespace BNine.AdminAPI.Controllers
{
    using System;
    using System.Threading.Tasks;
    using BNine.Application.Aggregates.Argyle.Users.Commands.DeleteArgyleUser;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.Extensions.Logging;

    [Authorize]
    [Route("clients/{clientId}")]
    public class ArgyleAccountsController : ApiController
    {
        public ArgyleAccountsController(ILogger<ArgyleAccountsController> logger) : base(logger)
        {
        }

        /// <summary>
        /// Delete client from Argyle
        /// </summary>
        /// <param name="clientId"></param>
        /// <returns></returns>
        [Authorize(Roles = "Admin, Agent, LightAgent, ApiAdmin")]
        [HttpDelete("argyle-user")]
        public async Task<IActionResult> DeleteArgyleUser([FromRoute] Guid clientId)
        {
            await Mediator.Send(new DeleteArgyleUserCommand(clientId));

            return NoContent();
        }
    }
}
