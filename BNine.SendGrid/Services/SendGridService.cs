﻿namespace BNine.SendGrid.Services
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Net;
    using System.Threading.Tasks;
    using Application.Interfaces;
    using Application.Models.Emails.EmailsWithTemplate;
    using global::SendGrid;
    using global::SendGrid.Helpers.Mail;
    using Microsoft.Extensions.Options;
    using Settings;

    public class SendGridService : IEmailService
    {
        private SendGridSettings SendGridSettings
        {
            get;
        }

        public SendGridService(IOptions<SendGridSettings> configuration)
        {
            SendGridSettings = configuration.Value;
        }

        public async Task<EmailsResponse> SendEmail(EmailMessageTemplate templateMessage)
        {
            return await SendEmails(new List<EmailMessageTemplate> {templateMessage});
        }

        public async Task<EmailsResponse> SendEmails(List<EmailMessageTemplate> templateMessages)
        {
            var result = new EmailsResponse();

            if (!templateMessages.Any())
            {
                return result;
            }

            var client = new SendGridClient(SendGridSettings.ApiKey);
            var messages = templateMessages.Select(GetSendGridTemplateMessage).ToList();

            foreach (var message in messages)
            {
                var response = await client.SendEmailAsync(message);
                if (response.StatusCode != HttpStatusCode.Accepted)
                {
                    result.IsSucceeded = false;
                    result.FailedFor.AddRange(message.Personalizations.First().Tos.Select(x => x.Email));
                }
            }

            return result;
        }

        private SendGridMessage GetSendGridTemplateMessage(EmailMessageTemplate templateMessage)
        {
            var gridMessage = new SendGridMessage();

            gridMessage.SetFrom(new EmailAddress(SendGridSettings.SenderEmail, SendGridSettings.SenderName));
            gridMessage.AddTo(new EmailAddress(templateMessage.RecipientEmail));

            foreach (var attachment in templateMessage.Attachments)
            {
                var content = Convert.ToBase64String(attachment.Data);
                gridMessage.AddAttachment(attachment.Name, content);
            }

            gridMessage.SetTemplateId(templateMessage.TemplateId);
            gridMessage.SetTemplateData(templateMessage.TemplateData);


            gridMessage.SetAsm(templateMessage.AsmGroupId, new List<int> { templateMessage.AsmGroupId });

            return gridMessage;
        }

        private EmailAddress GetRecipientEmail(string userEmail)
        {
            var email = SendGridSettings.IsEmailSendingEnabled ? userEmail : SendGridSettings.TestEmailAddress;
            return new EmailAddress(email);
        }
    }
}
