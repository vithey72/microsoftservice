﻿namespace BNine.SendGrid
{
    using BNine.Application.Interfaces;
    using BNine.SendGrid.Services;
    using Microsoft.Extensions.DependencyInjection;

    public static class DependenciesBootstrapper
    {
        public static IServiceCollection AddSendGrid(this IServiceCollection services)
        {
            services.AddTransient<IEmailService, SendGridService>();

            return services;
        }
    }
}
