﻿namespace BNine.Persistence;

using Application.Interfaces;
using Domain.Entities.EventAudit;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;

public class BNineAuditDbContext : DbContext, IBNineAuditDbContext
{
    public BNineAuditDbContext(DbContextOptions<BNineAuditDbContext> options) : base(options)
    {
    }

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {

        modelBuilder.HasDefaultSchema("audit");
        var builder = modelBuilder.Entity<EventAudit>();


        builder.HasKey(p => p.Id);

        builder
            .Property(p => p.DbAuditEntry)
            .HasConversion(dbAudit => JsonConvert.SerializeObject(dbAudit),
                dbAudit => JsonConvert.DeserializeObject<DbAuditEntry>(dbAudit));
        builder.HasIndex(p => p.EventId);
        builder
            .HasIndex(p => new { p.EventTime, p.UserId });



        base.OnModelCreating(modelBuilder);
    }

    public DbSet<EventAudit> EventAuditEntries
    {
        get;
        set;
    }

}
