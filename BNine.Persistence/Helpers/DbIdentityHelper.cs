﻿namespace BNine.Persistence.Helpers;

using Microsoft.EntityFrameworkCore;

public static class DbIdentityHelper
{
    private static Task EnableIdentityInsert<T>(this DbContext context) => SetIdentityInsert<T>(context, enable: true);
    private static Task DisableIdentityInsert<T>(this DbContext context) => SetIdentityInsert<T>(context, enable: false);

    private static Task SetIdentityInsert<T>(DbContext context, bool enable)
    {
        var entityType = context.Model.FindEntityType(typeof(T));
        var value = enable ? "ON" : "OFF";
        if (entityType == null)
        {
            return Task.CompletedTask;
        }
        return context.Database.ExecuteSqlRawAsync(
            $"SET IDENTITY_INSERT {entityType?.GetSchema()}.{entityType?.GetTableName()} {value}");
    }

    public static async Task SaveChangesWithIdentityInsertAsync<T>(this DbContext context)
    {
        await using var transaction = await context.Database.BeginTransactionAsync();
        await context.EnableIdentityInsert<T>();
        await context.SaveChangesAsync();
        await context.DisableIdentityInsert<T>();
        await transaction.CommitAsync();
    }
}
