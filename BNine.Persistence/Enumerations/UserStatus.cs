﻿namespace BNine.Persistence.Enumerations
{
    public class UserStatus
    {
        public Enums.UserStatus Id
        {
            get; set;
        }

        public string Name
        {
            get; set;
        }
    }
}
