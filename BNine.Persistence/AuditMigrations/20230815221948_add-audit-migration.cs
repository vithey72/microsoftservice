﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace BNine.Persistence.AuditMigrations
{
    public partial class addauditmigration : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.EnsureSchema(
                name: "audit");

            migrationBuilder.CreateTable(
                name: "EventAuditEntries",
                schema: "audit",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    UserId = table.Column<Guid>(type: "uniqueidentifier", nullable: true),
                    EventId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    EventName = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    EntityTypeName = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    EventTime = table.Column<DateTime>(type: "datetime2", nullable: false),
                    RawEventModel = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    ParsedEventModel = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    DbAuditEntry = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_EventAuditEntries", x => x.Id);
                });

            migrationBuilder.CreateIndex(
                name: "IX_EventAuditEntries_EventId",
                schema: "audit",
                table: "EventAuditEntries",
                column: "EventId");

            migrationBuilder.CreateIndex(
                name: "IX_EventAuditEntries_EventTime_UserId",
                schema: "audit",
                table: "EventAuditEntries",
                columns: new[] { "EventTime", "UserId" },
                unique: true,
                filter: "[UserId] IS NOT NULL");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "EventAuditEntries",
                schema: "audit");
        }
    }
}
