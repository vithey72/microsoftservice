﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace BNine.Persistence.AuditMigrations
{
    public partial class updateauditcompositekey : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_EventAuditEntries_EventTime_UserId",
                schema: "audit",
                table: "EventAuditEntries");

            migrationBuilder.CreateIndex(
                name: "IX_EventAuditEntries_EventTime_UserId",
                schema: "audit",
                table: "EventAuditEntries",
                columns: new[] { "EventTime", "UserId" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_EventAuditEntries_EventTime_UserId",
                schema: "audit",
                table: "EventAuditEntries");

            migrationBuilder.CreateIndex(
                name: "IX_EventAuditEntries_EventTime_UserId",
                schema: "audit",
                table: "EventAuditEntries",
                columns: new[] { "EventTime", "UserId" },
                unique: true,
                filter: "[UserId] IS NOT NULL");
        }
    }
}
