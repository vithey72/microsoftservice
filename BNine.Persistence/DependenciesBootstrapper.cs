﻿namespace BNine.Persistence
{
    using BNine.Application.Interfaces;
    using BNine.Persistence.Services;
    using Interceptors;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.EntityFrameworkCore.Diagnostics;
    using Microsoft.Extensions.Configuration;
    using Microsoft.Extensions.DependencyInjection;

    public static class DependenciesBootstrapper
    {
        public static IServiceCollection AddPersistence(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddDbContext<BNineDbContext>(options =>
            {

                options.UseSqlServer(
                        configuration.GetConnectionString("BNineConnection"),
                        b => b.MigrationsAssembly(typeof(BNineDbContext).Assembly.FullName));

                options.EnableDetailedErrors();
                options.EnableSensitiveDataLogging();
            });

            services.AddDbContext<BNineServiceDbContext>(options =>
            {

                options.UseSqlServer(
                        configuration.GetConnectionString("BNineConnection"));

                options.EnableDetailedErrors();
                options.EnableSensitiveDataLogging();
            });

            services.AddDbContext<BNineAuditDbContext>(options =>
            {

                options.UseSqlServer(
                        configuration.GetConnectionString("BNineConnection"),
                        b => b.MigrationsAssembly(typeof(BNineAuditDbContext).Assembly.FullName));

                options.EnableDetailedErrors();
                options.EnableSensitiveDataLogging();
            });


            services.AddScoped<IDomainEventsService, DomainEventsService>();
            services.AddScoped<IBNineDbContext>(provider => provider.GetService<BNineDbContext>());
            services.AddScoped<IBNineServiceDbContext>(provider => provider.GetService<BNineServiceDbContext>());
            services.AddScoped<IBNineAuditDbContext>(provider => provider.GetService<BNineAuditDbContext>());
            services.AddScoped<ISaveChangesInterceptor, EventSaveChangesAuditInterceptor>();


            return services;
        }
    }
}
