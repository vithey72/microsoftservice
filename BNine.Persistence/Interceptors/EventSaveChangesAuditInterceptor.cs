﻿namespace BNine.Persistence.Interceptors;

using Application.Interfaces;
using Domain.Entities.EventAudit;
using Domain.Interfaces.EventAudit;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using Microsoft.EntityFrameworkCore.Diagnostics;
using Microsoft.Extensions.Logging;

/// <summary>
///  this interceptor is meant to enrich the AuditEvent with the Db audit data
/// (i.e. the audit message and the entity state)
///  if, for some reason, the AuditEvent is not found in the database (has not been created yet??), it will be created
/// </summary>
public class EventSaveChangesAuditInterceptor : ISaveChangesInterceptor
{
    private readonly IBNineAuditDbContext _auditDbContext;
    private readonly ILogger<EventSaveChangesAuditInterceptor> _logger;
    private List<(EventAudit EventAudit, bool isNewEntry)> _eventAudits;

    /// <summary>
    /// Methods that are related to the 3rd step of AuditEventFlow are  <see cref="SavingChanges" /> and <see cref="SavingChangesAsync" />.
    /// On this stage the AuditEvent entity gets populated with DB audit data (i.e. the audit message and the entity state)
    /// They all use <see cref="EnrichOrCreateEventAudits"/> to achieve this.
    /// Methods that are related to the 4th step of AuditEventFlow are  <see cref="SavedChanges" /> , <see cref="SavedChangesAsync" />,
    /// <see cref="SaveChangesFailed" /> and <see cref="SaveChangesFailedAsync"/>.
    /// On this stage the AuditEvent entity gets populated with the processing result (i.e. the error details and the processing status)
    /// </summary>
    /// <param name="auditDbContext"></param>
    /// <param name="logger"></param>
    public EventSaveChangesAuditInterceptor(
        IBNineAuditDbContext auditDbContext,
        ILogger<EventSaveChangesAuditInterceptor> logger
    )
    {
        _auditDbContext = auditDbContext;
        _logger = logger;
    }

    public InterceptionResult<int> SavingChanges(DbContextEventData eventData, InterceptionResult<int> result)
    {
        _eventAudits = EnrichOrCreateEventAudits(eventData.Context);
        var newEntriesToAdd = GetNewEntriesToAddToContext(_eventAudits);
        _auditDbContext.EventAuditEntries.AddRange(newEntriesToAdd);
        SaveChangesLoggingErrors();
        return result;
    }

    public int SavedChanges(SaveChangesCompletedEventData eventData, int result)
    {

        foreach (var ea in _eventAudits)
        {
            var auditEntry = ea.EventAudit;
            auditEntry.DbAuditEntry.IsSuccessful = true;
            auditEntry.DbAuditEntry.ProcessedAt = DateTime.UtcNow;
            ChangeStateIfNeeded(auditEntry);
        }
        SaveChangesLoggingErrors();
        return result;
    }

    public void SaveChangesFailed(DbContextErrorEventData eventData)
    {

        foreach (var ea in _eventAudits)
        {
            var auditEntry = ea.EventAudit;
            auditEntry.DbAuditEntry.IsSuccessful = false;
            auditEntry.DbAuditEntry.ErrorDetails = new ErrorDetails()
            {
                ErrorMessage = eventData.Exception.Message, StackTrace = eventData.Exception.StackTrace
            };
            auditEntry.DbAuditEntry.ProcessedAt = DateTime.UtcNow;
            ChangeStateIfNeeded(auditEntry);
        }
        SaveChangesLoggingErrors();

    }

    public async ValueTask<InterceptionResult<int>> SavingChangesAsync(DbContextEventData eventData, InterceptionResult<int> result,
        CancellationToken cancellationToken = default)
    {
        _eventAudits = EnrichOrCreateEventAudits(eventData.Context);
        var newEntriesToAdd = GetNewEntriesToAddToContext(_eventAudits);
        _auditDbContext.EventAuditEntries.AddRange(newEntriesToAdd);
        await SaveChangesLoggingErrorsAsync(cancellationToken);
        return result;
    }

    public async ValueTask<int> SavedChangesAsync(SaveChangesCompletedEventData eventData, int result,
        CancellationToken cancellationToken = default)
    {


        foreach (var ea in _eventAudits)
        {
            var auditEntry = ea.EventAudit;
            auditEntry.DbAuditEntry.IsSuccessful = true;
            auditEntry.DbAuditEntry.ProcessedAt = DateTime.UtcNow;
            ChangeStateIfNeeded(auditEntry);

        }
        await SaveChangesLoggingErrorsAsync(cancellationToken);
        return result;
    }

    public async Task SaveChangesFailedAsync(DbContextErrorEventData eventData,
        CancellationToken cancellationToken = default)
    {

        foreach (var ea in _eventAudits)
        {
            var auditEntry = ea.EventAudit;
            auditEntry.DbAuditEntry.IsSuccessful = false;
            auditEntry.DbAuditEntry.ErrorDetails = new ErrorDetails()
            {
                ErrorMessage = eventData.Exception.Message, StackTrace = eventData.Exception.StackTrace
            };
            auditEntry.DbAuditEntry.ProcessedAt = DateTime.UtcNow;
            ChangeStateIfNeeded(auditEntry);
        }


        await SaveChangesLoggingErrorsAsync(cancellationToken);

    }

    private List<(EventAudit EventAudit, bool isNewEntry)> EnrichOrCreateEventAudits(DbContext context)
    {

        var eventAuditEntries = new List<(EventAudit EventAudit, bool isNewEntry)>();
        context.ChangeTracker.DetectChanges();
        var dbEntriesToProcess = context.ChangeTracker.Entries().
            Where(e => (e.State != EntityState.Unchanged && e.State != EntityState.Deleted) &&
                       e.Entity.GetType().GetInterface(nameof(IDbEventAuditableEntity)) != null)
            .ToList();


        foreach (var entry in dbEntriesToProcess)
        {
            {

                var now = DateTime.UtcNow;
                var auditableEntity = (entry.Entity as IDbEventAuditableEntity);

                var auditMessage = entry.State switch
                {
                    EntityState.Deleted => CreateDeletedMessage(entry),
                    EntityState.Modified => CreateModifiedMessage(entry),
                    EntityState.Added => CreateAddedMessage(entry),
                    _ => null
                };

                if (auditableEntity?.EventId == null)
                {
                    // this is not normal and should not happen, but needs be recoded
                    // if you see such entries in the database, please make sure you updated the EventId for the AuditableEntity
                    // before SaveChangesAsync is called
                    var noEventIdNewEvent = new EventAudit()
                    {
                        DbAuditEntry = new DbAuditEntry()
                            { AuditMessage = auditMessage, EntityState = entry.State, CreatedAt = now }
                    };
                    eventAuditEntries.Add((noEventIdNewEvent, true));
                    continue;
                }

                var existingEventAudit =
                    _auditDbContext.EventAuditEntries.FirstOrDefault(ea =>
                        ea.EventId == auditableEntity.EventId);

                if (existingEventAudit == null)
                {

                    // also not expected to happen in the target scenario
                    // if you see such entries check logs for EventAudit record related errors
                    var createdEventAudit = new EventAudit()
                    {
                        EventId = auditableEntity.EventId.Value,
                        EntityTypeName = entry.Metadata.DisplayName(),
                        EventTime = now,
                        DbAuditEntry = new DbAuditEntry()
                            { AuditMessage = auditMessage, EntityState = entry.State, CreatedAt = now }
                    };
                    eventAuditEntries.Add((createdEventAudit, true));
                    continue;

                }

                // finally the expected scenario
                existingEventAudit.EntityTypeName = entry.Metadata.DisplayName();
                existingEventAudit.DbAuditEntry = new DbAuditEntry()
                    { AuditMessage = auditMessage, EntityState = entry.State, CreatedAt = now };
                eventAuditEntries.Add((existingEventAudit, false));
            }
        }

        return eventAuditEntries;


    }

    private List<EventAudit> GetNewEntriesToAddToContext(List<(EventAudit EventAudit, bool isNewEntry)> lst) =>
        lst.Where(ea => ea.isNewEntry).Select(ea => ea.EventAudit).ToList();


    // for some very strange reason EF would have the entities in Unchanged state after the last update
    // DetectChanges did not work, so we had to do it manually
    private void ChangeStateIfNeeded<T>(T entity) where T : class
    {
        var trackedEntity = _auditDbContext.Entry<T>(entity);
        if (trackedEntity.State == EntityState.Unchanged)
        {
            trackedEntity.State = EntityState.Modified;
        }
    }

    private void SaveChangesLoggingErrors()
    {
        try
        {
            _auditDbContext.SaveChanges();
        }
        catch (Exception e)
        {
           _logger.LogError(e, "Failed to save audit changes");
        }
    }

    private async Task SaveChangesLoggingErrorsAsync(CancellationToken cancellationToken)
    {
        try
        {
            await _auditDbContext.SaveChangesAsync(cancellationToken);
        }
        catch (Exception e)
        {
            _logger.LogError(e, "Failed to save audit changes");
        }
    }


    private string CreateAddedMessage(EntityEntry entry)
        => entry.Properties.Aggregate(
            $"Inserting {entry.Metadata.DisplayName()} with ",
            (auditString, property) => auditString + $"{property.Metadata.Name}: '{property.CurrentValue}' ");

    private string CreateModifiedMessage(EntityEntry entry)
        => entry.Properties.Where(property => property.IsModified || property.Metadata.IsPrimaryKey()).Aggregate(
            $"Updating {entry.Metadata.DisplayName()} with ",
            (auditString, property) => auditString + $"{property.Metadata.Name}: '{property.CurrentValue}' ");

    private string CreateDeletedMessage(EntityEntry entry)
        => entry.Properties.Where(property => property.Metadata.IsPrimaryKey()).Aggregate(
            $"Deleting {entry.Metadata.DisplayName()} with ",
            (auditString, property) => auditString + $"{property.Metadata.Name}: '{property.CurrentValue}' ");

}
