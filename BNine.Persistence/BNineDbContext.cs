﻿namespace BNine.Persistence
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Threading;
    using System.Threading.Tasks;
    using Application.Interfaces;
    using Azure.Core;
    using Azure.Identity;
    using BNine.Application.Aggregates.ServiceBusEvents.Commands.PublishB9Event;
    using BNine.Application.Helpers;
    using BNine.Constants;
    using BNine.Domain.Entities.Administrator;
    using BNine.Domain.Entities.BankAccount;
    using BNine.Domain.Entities.BonusCodes;
    using BNine.Domain.Entities.Common;
    using BNine.Domain.Entities.ExternalCards;
    using BNine.Domain.Entities.Features;
    using BNine.Domain.Entities.MBanq;
    using BNine.Domain.Entities.Notification;
    using BNine.Domain.Entities.Settings;
    using BNine.Domain.Entities.TariffPlan;
    using BNine.Domain.Entities.Transfers;
    using BNine.Domain.Entities.User.UpdateInfoProcessing;
    using BNine.Settings;
    using Domain.Entities;
    using Domain.Entities.Banners;
    using Domain.Entities.CashbackProgram;
    using Domain.Entities.CheckCashing;
    using Domain.Entities.PaidUserServices;
    using Domain.Entities.TransfersSecurityOTPs;
    using Domain.Entities.User;
    using Domain.Entities.User.EarlySalary;
    using Domain.Entities.User.UserDetails.Settings;
    using Domain.Entities.UserQuestionnaire;
    using Domain.Interfaces;
    using Domain.ListItems;
    using MediatR;
    using Microsoft.Data.SqlClient;
    using Microsoft.Data.SqlClient.AlwaysEncrypted.AzureKeyVaultProvider;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.EntityFrameworkCore.ChangeTracking;
    using Microsoft.EntityFrameworkCore.Diagnostics;
    using Microsoft.Extensions.Hosting;
    using Microsoft.Extensions.Options;

    public class BNineDbContext : DbContext, IBNineDbContext
    {
        private static bool _isInitialized;
        private readonly IOptions<AuditEventSettings> _auditEventSettings;
        private readonly IDomainEventsService _domainEventsService;
        private readonly ISaveChangesInterceptor _saveChangesInterceptor;
        private readonly IMediator _mediator;
        private static readonly object Locker = new();

        public BNineDbContext(
            DbContextOptions<BNineDbContext> options,
            IOptions<AlwaysEncryptedSettings> alwaysEncryptedOptions,
            IOptions<AuditEventSettings> auditEventSettings,
            IHostEnvironment currentEnvironment,
            IDomainEventsService domainEventsService,
            IMediator mediator,
            ISaveChangesInterceptor saveChangesInterceptor
            ) : base(options)
        {
            _auditEventSettings = auditEventSettings;
            _domainEventsService = domainEventsService;
            _saveChangesInterceptor = saveChangesInterceptor;
            _mediator = mediator;

            if (alwaysEncryptedOptions != null)
            {
                InitializeAzureKeyVaultProvider(alwaysEncryptedOptions.Value, currentEnvironment);
            }
        }

        public BNineDbContext(
            DbContextOptions<BNineDbContext> options)
            : base(options)
        {
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (_auditEventSettings != null && _auditEventSettings.Value.IsEnabled)
            {
                optionsBuilder.AddInterceptors(_saveChangesInterceptor);
            }
            base.OnConfiguring(optionsBuilder);
        }

        private static void InitializeAzureKeyVaultProvider(AlwaysEncryptedSettings alwaysEncryptedSettings, IHostEnvironment currentEnvironment)
        {
            if (!_isInitialized)
            {
                lock (Locker)
                {
                    if (!_isInitialized)
                    {
                        InitializeAzureKeyVaultProviderInternal();
                        _isInitialized = true;
                    }
                }
            }

            void InitializeAzureKeyVaultProviderInternal()
            {
                TokenCredential credentials = currentEnvironment.IsProduction()
                ? new DefaultAzureCredential()
                : new ClientSecretCredential(
                    alwaysEncryptedSettings.AzureADTenantId,
                    alwaysEncryptedSettings.AzureADClientId,
                    alwaysEncryptedSettings.AzureADClientSecret);

                var azureKeyVaultProvider = new SqlColumnEncryptionAzureKeyVaultProvider(credentials);

                Dictionary<string, SqlColumnEncryptionKeyStoreProvider> providers = new()
                {
                    {
                        SqlColumnEncryptionAzureKeyVaultProvider.ProviderName,
                        azureKeyVaultProvider
                    }
                };
                SqlConnection.RegisterColumnEncryptionKeyStoreProviders(providers);
            }
        }

        public DbSet<User> Users
        {
            get; set;
        }

        public DbSet<Administrator> Administrators
        {
            get; set;
        }

        public DbSet<UserSettings> UserSettings
        {
            get;
            set;
        }

        public DbSet<EarlySalaryWidgetConfiguration> EarlySalaryWidgetConfigurations
        {
            get; set;
        }

        public DbSet<Device> Devices
        {
            get; set;
        }

        public DbSet<Notification> Notifications
        {
            get; set;
        }

        public DbSet<OTP> OTPs
        {
            get; set;
        }

        public DbSet<FrequencyOfPayments> FrequenciesOfPayments
        {
            get; set;
        }

        public DbSet<PayMethod> PayMethods
        {
            get; set;
        }

        public DbSet<DocumentType> DocumentTypes
        {
            get; set;
        }

        public DbSet<Plaid> PlaidData
        {
            get;
            set;
        }

        public DbSet<ExternalBankAccount> BankAccounts
        {
            get; set;
        }

        public DbSet<ExternalBankAccountBalance> ExternalBankAccountBalances
        {
            get; set;
        }

        public DbSet<ExternalBankAccountTransfer> ExternalBankAccountTransfers
        {
            get; set;
        }

        /// <inheritdoc/>
        public DbSet<BonusCode> BonusCodes
        {
            get; set;
        }

        /// <inheritdoc/>
        public DbSet<BonusCodeActivation> BonusCodeActivations
        {
            get; set;
        }

        /// <inheritdoc/>
        public DbSet<DebitCard> DebitCards
        {
            get;
            set;
        }

        /// <inheritdoc/>
        public DbSet<ExternalCard> ExternalCards
        {
            get;
            set;
        }

        /// <inheritdoc/>
        public DbSet<LoanAccount> LoanAccounts
        {
            get;
            set;
        }

        /// <inheritdoc/>
        public DbSet<KYCVerificationResult> KYCVerificationResults
        {
            get;
            set;
        }

        public DbSet<AnonymousAppsflyerId> AnonymousAppsflyerIds
        {
            get;
            set;
        }

        public DbSet<UpdateUserInfoRequest> UpdateUserInfoRequests
        {
            get;
            set;
        }

        public DbSet<LoanSpecialOffer> LoanSpecialOffers
        {
            get; set;
        }

        /// <inheritdoc/>
        public DbSet<OnboardingEvent> OnboardingEvents
        {
            get; set;
        }

        /// <inheritdoc/>
        public DbSet<LoanSettings> LoanSettings
        {
            get; set;
        }

        /// <inheritdoc/>
        public DbSet<CommonAccountSettings> CommonAccountSettings
        {
            get; set;
        }

        /// <inheritdoc/>
        public DbSet<WalletBlockCollectionSettings> WalletBlockCollectionSettings
        {
            get; set;
        }

        /// <inheritdoc/>
        public DbSet<AccountClosureSchedule> AccountClosureSchedules
        {
            get; set;
        }

        public DbSet<ACHCreditTransfer> ACHCreditTransfers
        {
            get; set;
        }

        public DbSet<LoanCalculatedLimit> LoanCalculatedLimits
        {
            get; set;
        }

        /// <inheritdoc/>
        public DbSet<BlockUserReason> UserBlockReasons
        {
            get; set;
        }

        public DbSet<SavingAccountSettings> SavingAccountSettings
        {
            get; set;
        }

        public DbSet<ChangeBlockStatusEvent> ChangeBlockStatusEvents
        {
            get; set;
        }

        public DbSet<ZendeskCategory> ZendeskCategories
        {
            get; set;
        }

        public DbSet<Domain.Entities.Settings.ArgyleSettings> ArgyleSettings
        {
            get; set;
        }

        public DbSet<Domain.Entities.Settings.EarlySalaryManualSettings> EarlySalaryManualSettings
        {
            get; set;
        }

        /// <inheritdoc/>
        public DbSet<StaticDocument> StaticDocuments
        {
            get; set;
        }

        public DbSet<AlltrustCustomerSettings> AlltrustCustomerSettings
        {
            get; set;
        }

        public DbSet<CheckCashingTransaction> CheckCashingTransactions
        {
            get;
            set;
        }

        public DbSet<CheckMaker> CheckMakers
        {
            get;
            set;
        }

        public DbSet<EarlySalaryManualRequest> EarlySalaryManualRequests
        {
            get;
            set;
        }

        public DbSet<MBanqChargeProduct> MBanqChargeProducts
        {
            get;
            set;
        }

        public DbSet<Feature> Features
        {
            get;
            set;
        }

        public DbSet<Group> Groups
        {
            get;
            set;
        }

        public DbSet<FeatureGroup> FeatureGroups
        {
            get;
            set;
        }

        public DbSet<UserGroup> UserGroups
        {
            get;
            set;
        }

        public DbSet<Question> Questions
        {
            get;
            set;
        }

        public DbSet<QuestionResponse> QuestionResponses
        {
            get;
            set;
        }

        public DbSet<QuestionSet> QuestionSets
        {
            get;
            set;
        }

        public DbSet<QuestionQuestionSet> QuestionQuestionSets
        {
            get;
            set;
        }

        public DbSet<SubmittedUserQuestionnaire> CompletedUserQuestionnaires
        {
            get;
            set;
        }

        public DbSet<SubmittedQuestionResponse> SubmittedQuestionResponses
        {
            get;
            set;
        }

        public DbSet<SubmittedQuestionResponseQuestionResponse> SubmittedQuestionResponseQuestionResponses
        {
            get;
            set;
        }

        public DbSet<TariffPlan> TariffPlans
        {
            get;
            set;
        }

        public DbSet<UserTariffPlan> UserTariffPlans
        {
            get;
            set;
        }

        public DbSet<TransferMerchantsSettings> TransferMerchantsSettings
        {
            get;
            set;
        }

        public DbSet<TransferIconSettings> TransferIconSettings
        {
            get;
            set;
        }

        public DbSet<MerchantCategoryCodeIconSettings> MerchantCategoryCodeIconSettings
        {
            get;
            set;
        }

        public DbSet<UsedCashback> UsedCashbacks
        {
            get;
            set;
        }

        public DbSet<UserCashbackStatistic> UserCashbackStatistics
        {
            get;
            set;
        }

        public DbSet<CashbackCategory> CashbackCategories
        {
            get;
            set;
        }

        public DbSet<FavoriteUserTransfer> FavoriteUserTransfers
        {
            get;
            set;
        }

        public DbSet<CashbackProgramSettings> CashbackProgramSettings
        {
            get;
            set;
        }

        public DbSet<BaseMarketingBanner> AllMarketingBanners
        {
            get;
            set;
        }

        public DbSet<InfoPopupBanner> InfoPopupMarketingBanners
        {
            get;
            set;
        }

        public DbSet<SeenMarketingBanner> MarketingBannersSeen
        {
            get;
            set;
        }

        public DbSet<PaidUserService> PaidUserServices
        {
            get;
            set;
        }

        public DbSet<UsedPaidUserService> UsedPaidUserServices
        {
            get;
            set;
        }

        public DbSet<Domain.Entities.AdminActionLogEntry.AdminActionLogEntry> AdminActionLogEntry
        {
            get;
            set;
        }

        public DbSet<TransfersSecurityOTP> TransfersSecurityOTPs
        {
            get;
            set;
        }

        public DbSet<AdminApiUser> AdminUsers
        {
            get;
            set;
        }

        public DbSet<UserPlaceholderText> UserPlaceholderTexts
        {
            get;
            set;
        }

        public DbSet<CardProduct> CardProducts
        {
            get;
            set;
        }

        public DbSet<TransactionAnalyticsCategory> TransactionCategories
        {
            get;
            set;
        }

        public DbSet<PotentialAchIncomeTransfer> PotentialAchIncomeTransfers
        {
            get;
            set;
        }

        public DbSet<TransferErrorLog> TransferErrorLogs
        {
            get;
            set;
        }

        public DbSet<UserActivityCooldown> UserActivityCooldowns
        {
            get;
            set;
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder.ApplyConfigurationsFromAssembly(Assembly.GetExecutingAssembly());

            base.OnModelCreating(builder);
        }

        private async Task DispatchEvents(CancellationToken cancellationToken = default)
        {
            var domainEvents = ChangeTracker.Entries<IHaveDomainEvents>()
                    .Select(x => x.Entity)
                    .Where(x => x.DomainEvents.Any())
                    .SelectMany(x => x.DomainEvents)
                    .Where(domainEvent => !domainEvent.IsPublished);

            foreach (var domainEvent in domainEvents.ToList())
            {
                domainEvent.IsPublished = true;
                await _domainEventsService.Publish(domainEvent);
            }
        }

        private void SoftDelete(EntityEntry entry)
        {
            if (entry.Entity is ISoftDeletable)
            {
                var entity = entry.Entity as ISoftDeletable;
                entity.IsDeleted = true;
                entry.State = EntityState.Modified;
            }
        }

        [Obsolete("Use async version with event sending")]
        public override int SaveChanges()
        {
            foreach (var entry in ChangeTracker.Entries()
                      .Where(p => p.State == EntityState.Deleted))
            {
                SoftDelete(entry);
            }

            var result = base.SaveChanges();

            AsyncHelper.RunSync(() => DispatchEvents());

            return result;
        }

        public override async Task<int> SaveChangesAsync(CancellationToken cancellationToken = new())
        {
            foreach (var entry in ChangeTracker.Entries()
                     .Where(p => p.State == EntityState.Deleted))
            {
                SoftDelete(entry);
            }

            // all these go to the topic b9.user.v2
            CollectUpdates<User>("user");
            CollectUpdates<UserSettings>("user", "user-settings.");
            CollectUpdates<Identity>("user", "identity.");
            CollectUpdates<EarlySalaryUserSettings>("user", "early-salary-user-settings.");
            CollectUpdates<Address>("user", "address.");
            CollectUpdates<Document>("user", "document.");
            CollectUpdates<UserTariffPlan>("user", "tariff-plan.");
            CollectUpdates<ClientLoanSettings>("user", "client-loan-settings.");
            CollectUpdates<ClientRating>("user", "client-rating.");

            var result = await base.SaveChangesAsync(cancellationToken);
            if (_mediator != null)
            {
                await PublishUpdates(cancellationToken);
            }
            if (_domainEventsService != null)
            {
                await DispatchEvents(cancellationToken);
            }

            return result;
        }

        private Dictionary<object, EntityState> UpdatedEntries = new();
        private Dictionary<Type, (string TopicName, string EventNamePrefix)> UpdatedTypeTopics = new();

        private void CollectUpdates<T>(string topicName, string eventNamePrefix = "") where T : class, IHistoryEntry
        {
            foreach (var change in ChangeTracker.Entries<T>()
                    .Where(c => new EntityState[] {
                            EntityState.Added,
                            EntityState.Modified,
                            EntityState.Deleted
                        }.Contains(c.State)))
            {
                UpdatedEntries[change.Entity] = change.State;
            }
            UpdatedTypeTopics[typeof(T)] = (topicName, eventNamePrefix);
        }

        private async Task PublishUpdates(CancellationToken cancellationToken)
        {
            foreach (var change in ChangeTracker.Entries().ToList())
            {
                if (change.Entity is IHistoryEntry entity)
                {
                    var state = UpdatedEntries.FirstOrDefault(updated => object.ReferenceEquals(updated.Key, entity)).Value;
                    if (state == default)
                    {
                        continue;
                    }

                    var topic = $"{ServiceBusTopics.B9GatewayPrefix}{UpdatedTypeTopics[entity.GetType()].TopicName}.v2";
                    await _mediator.Send(new PublishB9EntityUpdateCommand(
                        change.Entity,
                        topic,
                        UpdatedTypeTopics[entity.GetType()].EventNamePrefix + UpdatedEntries[entity] switch
                        {
                            EntityState.Added => ServiceBusEvents.B9Created,
                            EntityState.Modified => ServiceBusEvents.B9Updated,
                            EntityState.Deleted => ServiceBusEvents.B9Deleted,
                            _ => throw new InvalidOperationException($"State is not expected: {UpdatedEntries[entity]}")
                        }),
                        cancellationToken);
                }
            }
            UpdatedEntries.Clear();
        }

        public void AddGeneric<T>(T newItem) where T : class
        {
            base.Set<T>().Add(newItem);
        }

        public void RemoveGeneric<T>(T itemToRemove) where T : class
        {
            base.Set<T>().Remove(itemToRemove);
        }
    }
}
