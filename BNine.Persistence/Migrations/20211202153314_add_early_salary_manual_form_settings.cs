﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace BNine.Persistence.Migrations
{
    public partial class add_early_salary_manual_form_settings : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<decimal>(
                name: "PaycheckDepositHistory",
                schema: "user",
                table: "ClientRatings",
                nullable: false,
                oldClrType: typeof(decimal),
                oldType: "decimal(18,6)");

            migrationBuilder.AlterColumn<decimal>(
                name: "OnTimeRepaymentsOfAdvances",
                schema: "user",
                table: "ClientRatings",
                nullable: false,
                oldClrType: typeof(decimal),
                oldType: "decimal(18,6)");

            migrationBuilder.AlterColumn<decimal>(
                name: "CurrentPaycheckDeposits",
                schema: "user",
                table: "ClientRatings",
                nullable: false,
                oldClrType: typeof(decimal),
                oldType: "decimal(18,6)");

            migrationBuilder.AlterColumn<decimal>(
                name: "CardTransactions",
                schema: "user",
                table: "ClientRatings",
                nullable: false,
                oldClrType: typeof(decimal),
                oldType: "decimal(18,6)");

            migrationBuilder.CreateTable(
                name: "EarlySalaryManualSettings",
                schema: "settings",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false, defaultValueSql: "newsequentialid()"),
                    DefaultAmountAllocation = table.Column<decimal>(type: "decimal(18,2)", nullable: false),
                    MinAmountAllocation = table.Column<decimal>(type: "decimal(18,2)", nullable: false),
                    MaxAmountAllocation = table.Column<decimal>(type: "decimal(18,2)", nullable: false),
                    DefaultPercentAllocation = table.Column<decimal>(type: "decimal(18,2)", nullable: false),
                    MinPercentAllocation = table.Column<decimal>(type: "decimal(18,2)", nullable: false),
                    MaxPercentAllocation = table.Column<decimal>(type: "decimal(18,2)", nullable: false),
                    CreatedAt = table.Column<DateTime>(nullable: false),
                    UpdatedAt = table.Column<DateTime>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_EarlySalaryManualSettings", x => x.Id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "EarlySalaryManualSettings",
                schema: "settings");

            migrationBuilder.AlterColumn<decimal>(
                name: "PaycheckDepositHistory",
                schema: "user",
                table: "ClientRatings",
                type: "decimal(18,6)",
                nullable: false,
                oldClrType: typeof(decimal));

            migrationBuilder.AlterColumn<decimal>(
                name: "OnTimeRepaymentsOfAdvances",
                schema: "user",
                table: "ClientRatings",
                type: "decimal(18,6)",
                nullable: false,
                oldClrType: typeof(decimal));

            migrationBuilder.AlterColumn<decimal>(
                name: "CurrentPaycheckDeposits",
                schema: "user",
                table: "ClientRatings",
                type: "decimal(18,6)",
                nullable: false,
                oldClrType: typeof(decimal));

            migrationBuilder.AlterColumn<decimal>(
                name: "CardTransactions",
                schema: "user",
                table: "ClientRatings",
                type: "decimal(18,6)",
                nullable: false,
                oldClrType: typeof(decimal));
        }
    }
}
