﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace BNine.Persistence.Migrations
{
    public partial class add_progressive_green_banner_cooldown : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "SeenTimes",
                schema: "dbo",
                table: "MarketingBannersSeen",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<string>(
                name: "ShowCooldownProgressive",
                schema: "settings",
                table: "MarketingBanners",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.InsertData(
                schema: "settings",
                table: "MarketingBanners",
                columns: new[] { "Id", "ButtonDeeplink", "ButtonText", "ExternalLink", "HasCloseButton", "IsEnabled", "MinUserRegistrationDate", "Name", "ShowCooldown", "ShowCooldownProgressive", "ShowFrom", "ShowTil", "Subtitle", "Title" },
                values: new object[] { new Guid("f8fc9612-6490-49a3-a87d-540532bc2783"), "premium-tariff-plan", "GET MY DISCOUNT", null, true, true, null, "PremiumTariff3MonthsDiscount", null, "[\"1.00:00:00\",\"3.00:00:00\",\"7.00:00:00\"]", new DateTime(2022, 8, 17, 6, 0, 0, 0, DateTimeKind.Unspecified), null, null, "Discounted Premium Plan" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                schema: "settings",
                table: "MarketingBanners",
                keyColumn: "Id",
                keyValue: new Guid("f8fc9612-6490-49a3-a87d-540532bc2783"));

            migrationBuilder.DropColumn(
                name: "SeenTimes",
                schema: "dbo",
                table: "MarketingBannersSeen");

            migrationBuilder.DropColumn(
                name: "ShowCooldownProgressive",
                schema: "settings",
                table: "MarketingBanners");
        }
    }
}
