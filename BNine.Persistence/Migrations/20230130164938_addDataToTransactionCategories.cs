﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace BNine.Persistence.Migrations
{
    public partial class addDataToTransactionCategories : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData(
                table: "TransactionCategories",
                columns: new[] { "Key", "Color", "IconUrl", "MccCodesJsonArrayString", "ParentId", "PaymentTypeIdsJsonArrayString", "Title" },
                values: new object[] { "default", null, null, null, null, null, null });

            migrationBuilder.InsertData(
                table: "TransactionCategories",
                columns: new[] { "Key", "Color", "IconUrl", "MccCodesJsonArrayString", "ParentId", "PaymentTypeIdsJsonArrayString", "Title" },
                values: new object[] { "professional-services", null, null, "[8999]", null, null, "Professional Services" });

            migrationBuilder.InsertData(
                table: "TransactionCategories",
                columns: new[] { "Key", "Color", "IconUrl", "MccCodesJsonArrayString", "ParentId", "PaymentTypeIdsJsonArrayString", "Title" },
                values: new object[] { "atm", "53ACFF", "https://b9devaccount.blob.core.windows.net/static-documents-container/TransactionAnalyticsIcons/ATMIcon.png", "[6011]", "default", null, "ATM" });

            migrationBuilder.InsertData(
                table: "TransactionCategories",
                columns: new[] { "Key", "Color", "IconUrl", "MccCodesJsonArrayString", "ParentId", "PaymentTypeIdsJsonArrayString", "Title" },
                values: new object[] { "card-transactions", "FF0000", "https://b9devaccount.blob.core.windows.net/static-documents-container/TransactionAnalyticsIcons/CardTransactionIcon.png", null, "default", null, "Card Transactions" });

            migrationBuilder.InsertData(
                table: "TransactionCategories",
                columns: new[] { "Key", "Color", "IconUrl", "MccCodesJsonArrayString", "ParentId", "PaymentTypeIdsJsonArrayString", "Title" },
                values: new object[] { "transfers", "8BC446", "https://b9devaccount.blob.core.windows.net/static-documents-container/TransactionAnalyticsIcons/TransferIcon.png", null, "default", null, "Transfers" });

            migrationBuilder.InsertData(
                table: "TransactionCategories",
                columns: new[] { "Key", "Color", "IconUrl", "MccCodesJsonArrayString", "ParentId", "PaymentTypeIdsJsonArrayString", "Title" },
                values: new object[,]
                {
                    { "ach-sent", null, null, null, "transfers", "[2]", "ACH Sent" },
                    { "auto-parts", null, null, "[5533]", "card-transactions", null, "Auto Parts" },
                    { "auto-sales", null, null, "[5511]", "card-transactions", null, "Auto Sales" },
                    { "beauty", null, null, "[5977,7230,7297,7298]", "card-transactions", null, "Beauty" },
                    { "book-stores", null, null, "[2741,5111,5192,5942,5994]", "card-transactions", null, "Book Stores" },
                    { "cable-services", null, null, "[4899]", "card-transactions", null, "Cable Services" },
                    { "car-wash", null, null, "[7542]", "card-transactions", null, "Car Wash" },
                    { "charitable-organizations", null, null, "[8398]", "card-transactions", null, "Charitable Organizations" },
                    { "cigarettes-vapes", null, null, "[5993]", "card-transactions", null, "Cigarettes/Vapes" },
                    { "clothing-stores", null, null, "[5691]", "card-transactions", null, "Clothing Stores" },
                    { "commuter-transportation", null, null, "[4111]", "card-transactions", null, "Commuter Transportation" },
                    { "computer-equipment", null, null, "[5045]", "card-transactions", null, "Computer Equipment" },
                    { "computer-networks", null, null, "[4816]", "card-transactions", null, "Computer Networks" },
                    { "computer-services", null, null, "[7372]", "card-transactions", null, "Computer Services" },
                    { "dating-services", null, null, "[7273]", "card-transactions", null, "Dating Services" },
                    { "department-stores", null, null, "[5311]", "card-transactions", null, "Department Stores" },
                    { "digital-goods", null, null, "[5816,5818]", "card-transactions", null, "Digital Goods" },
                    { "direct-marketing", null, null, "[5967]", "card-transactions", null, "Variety Stores" },
                    { "discount-stores", null, null, "[5310]", "card-transactions", null, "Discount Stores" },
                    { "education", null, null, "[8299]", "card-transactions", null, "Education" },
                    { "electronics", null, null, "[5732]", "card-transactions", null, "Electronics" },
                    { "entertainment", null, null, "[7911,7922,7929,7932,7933,7941,7991,7992,7993,7994,7996,7997,7998,7999,8664]", "card-transactions", null, "Entertainment" },
                    { "equipment-rental", null, null, "[7394]", "card-transactions", null, "Equipment Rental" },
                    { "family-clothing-stores", null, null, "[5651]", "card-transactions", null, "Family Clothing Stores" },
                    { "fastfood", null, null, "[5814]", "card-transactions", null, "Fast Food" },
                    { "financial-institutions", null, null, "[6012]", "card-transactions", null, "Financial Institutions" },
                    { "gas-stations", null, null, "[5172,5541,5542,5983]", "card-transactions", null, "Gas Stations" },
                    { "general-merch", null, null, "[5399]", "card-transactions", null, "Misc. General Merch" },
                    { "gift-shops", null, null, "[5947]", "card-transactions", null, "Gift Shops" },
                    { "government-services", null, null, "[9399]", "card-transactions", null, "Government Services" },
                    { "home-rentals", null, null, "[6513]", "card-transactions", null, "Home Rentals" },
                    { "home-stores", null, null, "[5719]", "card-transactions", null, "Misc. Home Stores" },
                    { "home-supplies", null, null, "[5200]", "card-transactions", null, "Home Supplies" },
                    { "insurance", null, null, "[6300]", "card-transactions", null, "Insurance" },
                    { "internal-transfer-sent", null, null, null, "transfers", "[1]", "Internal" },
                    { "laundry-services", null, null, "[7211]", "card-transactions", null, "Laundry Services" },
                    { "lodging", null, null, "[7011]", "card-transactions", null, "Lodging" },
                    { "misc-apparel", null, null, "[5699]", "card-transactions", null, "Misc. Apparel" },
                    { "money-transfers", null, null, "[4829]", "card-transactions", null, "Mobile Wallet Usage" },
                    { "movies", null, null, "[7829,7832,7841]", "card-transactions", null, "Movies" },
                    { "others", null, null, "[]", "card-transactions", null, "Others" },
                    { "parking", null, null, "[7523]", "card-transactions", null, "Parking" }
                });

            migrationBuilder.InsertData(
                table: "TransactionCategories",
                columns: new[] { "Key", "Color", "IconUrl", "MccCodesJsonArrayString", "ParentId", "PaymentTypeIdsJsonArrayString", "Title" },
                values: new object[,]
                {
                    { "personal-services", null, null, "[7299]", "card-transactions", null, "Personal Services" },
                    { "pet-stores", null, null, "[5995]", "card-transactions", null, "Pet Stores" },
                    { "pharmacies", null, null, "[5122,5292,5295,5912]", "card-transactions", null, "Pharmacy" },
                    { "record-shops", null, null, "[5735]", "card-transactions", null, "Record Shops" },
                    { "restaurants", null, null, "[5811,5812,5813]", "card-transactions", null, "Restaurants" },
                    { "sent-to-external-cards", null, null, null, "transfers", "[5]", "Sent to External Cards" },
                    { "shoe-stores", null, null, "[5661]", "card-transactions", null, "Shoe Stores" },
                    { "software", null, null, "[5734]", "card-transactions", null, "Software" },
                    { "specialty-retail", null, null, "[5999]", "card-transactions", null, "Specialty Retail" },
                    { "sporting-goods", null, null, "[5941]", "card-transactions", null, "Sporting Goods" },
                    { "subscriptions", null, null, "[5968]", "card-transactions", null, "Subscriptions" },
                    { "supermarkets-online-stores", null, null, "[5297,5298,5300,5411,5412,5422,5441,5451,5462,5499,5715,5921]", "card-transactions", null, "Groceries" },
                    { "taxi", null, null, "[4121]", "card-transactions", null, "Taxis" },
                    { "telecommunication-equipment", null, null, "[4812]", "card-transactions", null, "Telecommunication Equipment" },
                    { "telecommunications", null, null, "[4814]", "card-transactions", null, "Telecommunications" },
                    { "thrift-stores", null, null, "[5931]", "card-transactions", null, "Thrift Stores" },
                    { "travel-agencies", null, null, "[4722]", "card-transactions", null, "Travel" },
                    { "used-autos", null, null, "[5521]", "card-transactions", null, "Used Autos" },
                    { "utilities", null, null, "[4900]", "card-transactions", null, "Utilities" },
                    { "variety-stores", null, null, "[5331]", "card-transactions", null, "Variety Stores" },
                    { "wallets-crypto", null, null, "[6051]", "card-transactions", null, "Crypto Wallets" },
                    { "women-clothing", null, null, "[5621]", "card-transactions", null, "Women's Clothing" }
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "TransactionCategories",
                keyColumn: "Key",
                keyValue: "ach-sent");

            migrationBuilder.DeleteData(
                table: "TransactionCategories",
                keyColumn: "Key",
                keyValue: "atm");

            migrationBuilder.DeleteData(
                table: "TransactionCategories",
                keyColumn: "Key",
                keyValue: "auto-parts");

            migrationBuilder.DeleteData(
                table: "TransactionCategories",
                keyColumn: "Key",
                keyValue: "auto-sales");

            migrationBuilder.DeleteData(
                table: "TransactionCategories",
                keyColumn: "Key",
                keyValue: "beauty");

            migrationBuilder.DeleteData(
                table: "TransactionCategories",
                keyColumn: "Key",
                keyValue: "book-stores");

            migrationBuilder.DeleteData(
                table: "TransactionCategories",
                keyColumn: "Key",
                keyValue: "cable-services");

            migrationBuilder.DeleteData(
                table: "TransactionCategories",
                keyColumn: "Key",
                keyValue: "car-wash");

            migrationBuilder.DeleteData(
                table: "TransactionCategories",
                keyColumn: "Key",
                keyValue: "charitable-organizations");

            migrationBuilder.DeleteData(
                table: "TransactionCategories",
                keyColumn: "Key",
                keyValue: "cigarettes-vapes");

            migrationBuilder.DeleteData(
                table: "TransactionCategories",
                keyColumn: "Key",
                keyValue: "clothing-stores");

            migrationBuilder.DeleteData(
                table: "TransactionCategories",
                keyColumn: "Key",
                keyValue: "commuter-transportation");

            migrationBuilder.DeleteData(
                table: "TransactionCategories",
                keyColumn: "Key",
                keyValue: "computer-equipment");

            migrationBuilder.DeleteData(
                table: "TransactionCategories",
                keyColumn: "Key",
                keyValue: "computer-networks");

            migrationBuilder.DeleteData(
                table: "TransactionCategories",
                keyColumn: "Key",
                keyValue: "computer-services");

            migrationBuilder.DeleteData(
                table: "TransactionCategories",
                keyColumn: "Key",
                keyValue: "dating-services");

            migrationBuilder.DeleteData(
                table: "TransactionCategories",
                keyColumn: "Key",
                keyValue: "department-stores");

            migrationBuilder.DeleteData(
                table: "TransactionCategories",
                keyColumn: "Key",
                keyValue: "digital-goods");

            migrationBuilder.DeleteData(
                table: "TransactionCategories",
                keyColumn: "Key",
                keyValue: "direct-marketing");

            migrationBuilder.DeleteData(
                table: "TransactionCategories",
                keyColumn: "Key",
                keyValue: "discount-stores");

            migrationBuilder.DeleteData(
                table: "TransactionCategories",
                keyColumn: "Key",
                keyValue: "education");

            migrationBuilder.DeleteData(
                table: "TransactionCategories",
                keyColumn: "Key",
                keyValue: "electronics");

            migrationBuilder.DeleteData(
                table: "TransactionCategories",
                keyColumn: "Key",
                keyValue: "entertainment");

            migrationBuilder.DeleteData(
                table: "TransactionCategories",
                keyColumn: "Key",
                keyValue: "equipment-rental");

            migrationBuilder.DeleteData(
                table: "TransactionCategories",
                keyColumn: "Key",
                keyValue: "family-clothing-stores");

            migrationBuilder.DeleteData(
                table: "TransactionCategories",
                keyColumn: "Key",
                keyValue: "fastfood");

            migrationBuilder.DeleteData(
                table: "TransactionCategories",
                keyColumn: "Key",
                keyValue: "financial-institutions");

            migrationBuilder.DeleteData(
                table: "TransactionCategories",
                keyColumn: "Key",
                keyValue: "gas-stations");

            migrationBuilder.DeleteData(
                table: "TransactionCategories",
                keyColumn: "Key",
                keyValue: "general-merch");

            migrationBuilder.DeleteData(
                table: "TransactionCategories",
                keyColumn: "Key",
                keyValue: "gift-shops");

            migrationBuilder.DeleteData(
                table: "TransactionCategories",
                keyColumn: "Key",
                keyValue: "government-services");

            migrationBuilder.DeleteData(
                table: "TransactionCategories",
                keyColumn: "Key",
                keyValue: "home-rentals");

            migrationBuilder.DeleteData(
                table: "TransactionCategories",
                keyColumn: "Key",
                keyValue: "home-stores");

            migrationBuilder.DeleteData(
                table: "TransactionCategories",
                keyColumn: "Key",
                keyValue: "home-supplies");

            migrationBuilder.DeleteData(
                table: "TransactionCategories",
                keyColumn: "Key",
                keyValue: "insurance");

            migrationBuilder.DeleteData(
                table: "TransactionCategories",
                keyColumn: "Key",
                keyValue: "internal-transfer-sent");

            migrationBuilder.DeleteData(
                table: "TransactionCategories",
                keyColumn: "Key",
                keyValue: "laundry-services");

            migrationBuilder.DeleteData(
                table: "TransactionCategories",
                keyColumn: "Key",
                keyValue: "lodging");

            migrationBuilder.DeleteData(
                table: "TransactionCategories",
                keyColumn: "Key",
                keyValue: "misc-apparel");

            migrationBuilder.DeleteData(
                table: "TransactionCategories",
                keyColumn: "Key",
                keyValue: "money-transfers");

            migrationBuilder.DeleteData(
                table: "TransactionCategories",
                keyColumn: "Key",
                keyValue: "movies");

            migrationBuilder.DeleteData(
                table: "TransactionCategories",
                keyColumn: "Key",
                keyValue: "others");

            migrationBuilder.DeleteData(
                table: "TransactionCategories",
                keyColumn: "Key",
                keyValue: "parking");

            migrationBuilder.DeleteData(
                table: "TransactionCategories",
                keyColumn: "Key",
                keyValue: "personal-services");

            migrationBuilder.DeleteData(
                table: "TransactionCategories",
                keyColumn: "Key",
                keyValue: "pet-stores");

            migrationBuilder.DeleteData(
                table: "TransactionCategories",
                keyColumn: "Key",
                keyValue: "pharmacies");

            migrationBuilder.DeleteData(
                table: "TransactionCategories",
                keyColumn: "Key",
                keyValue: "professional-services");

            migrationBuilder.DeleteData(
                table: "TransactionCategories",
                keyColumn: "Key",
                keyValue: "record-shops");

            migrationBuilder.DeleteData(
                table: "TransactionCategories",
                keyColumn: "Key",
                keyValue: "restaurants");

            migrationBuilder.DeleteData(
                table: "TransactionCategories",
                keyColumn: "Key",
                keyValue: "sent-to-external-cards");

            migrationBuilder.DeleteData(
                table: "TransactionCategories",
                keyColumn: "Key",
                keyValue: "shoe-stores");

            migrationBuilder.DeleteData(
                table: "TransactionCategories",
                keyColumn: "Key",
                keyValue: "software");

            migrationBuilder.DeleteData(
                table: "TransactionCategories",
                keyColumn: "Key",
                keyValue: "specialty-retail");

            migrationBuilder.DeleteData(
                table: "TransactionCategories",
                keyColumn: "Key",
                keyValue: "sporting-goods");

            migrationBuilder.DeleteData(
                table: "TransactionCategories",
                keyColumn: "Key",
                keyValue: "subscriptions");

            migrationBuilder.DeleteData(
                table: "TransactionCategories",
                keyColumn: "Key",
                keyValue: "supermarkets-online-stores");

            migrationBuilder.DeleteData(
                table: "TransactionCategories",
                keyColumn: "Key",
                keyValue: "taxi");

            migrationBuilder.DeleteData(
                table: "TransactionCategories",
                keyColumn: "Key",
                keyValue: "telecommunication-equipment");

            migrationBuilder.DeleteData(
                table: "TransactionCategories",
                keyColumn: "Key",
                keyValue: "telecommunications");

            migrationBuilder.DeleteData(
                table: "TransactionCategories",
                keyColumn: "Key",
                keyValue: "thrift-stores");

            migrationBuilder.DeleteData(
                table: "TransactionCategories",
                keyColumn: "Key",
                keyValue: "travel-agencies");

            migrationBuilder.DeleteData(
                table: "TransactionCategories",
                keyColumn: "Key",
                keyValue: "used-autos");

            migrationBuilder.DeleteData(
                table: "TransactionCategories",
                keyColumn: "Key",
                keyValue: "utilities");

            migrationBuilder.DeleteData(
                table: "TransactionCategories",
                keyColumn: "Key",
                keyValue: "variety-stores");

            migrationBuilder.DeleteData(
                table: "TransactionCategories",
                keyColumn: "Key",
                keyValue: "wallets-crypto");

            migrationBuilder.DeleteData(
                table: "TransactionCategories",
                keyColumn: "Key",
                keyValue: "women-clothing");

            migrationBuilder.DeleteData(
                table: "TransactionCategories",
                keyColumn: "Key",
                keyValue: "card-transactions");

            migrationBuilder.DeleteData(
                table: "TransactionCategories",
                keyColumn: "Key",
                keyValue: "transfers");

            migrationBuilder.DeleteData(
                table: "TransactionCategories",
                keyColumn: "Key",
                keyValue: "default");
        }
    }
}
