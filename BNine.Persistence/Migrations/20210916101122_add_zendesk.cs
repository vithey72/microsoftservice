﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace BNine.Persistence.Migrations
{
    public partial class add_zendesk : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "ZendeskCategories",
                columns: table => new
                {
                    Key = table.Column<string>(maxLength: 150, nullable: false),
                    Order = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ZendeskCategories", x => x.Key);
                });

            migrationBuilder.CreateTable(
                name: "ZendeskCategoriesValues",
                schema: "translates",
                columns: table => new
                {
                    ZendeskCategoryKey = table.Column<string>(nullable: false),
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Language = table.Column<int>(nullable: false),
                    Text = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ZendeskCategoriesValues", x => new { x.ZendeskCategoryKey, x.Id });
                    table.ForeignKey(
                        name: "FK_ZendeskCategoriesValues_ZendeskCategories_ZendeskCategoryKey",
                        column: x => x.ZendeskCategoryKey,
                        principalTable: "ZendeskCategories",
                        principalColumn: "Key",
                        onDelete: ReferentialAction.Cascade);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "ZendeskCategoriesValues",
                schema: "translates");

            migrationBuilder.DropTable(
                name: "ZendeskCategories");
        }
    }
}
