﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace BNine.Persistence.Migrations
{
    public partial class add_array_customer_timestamps : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<DateTime>(
                name: "ActivatedAt",
                schema: "user",
                table: "ArrayCustomerSettings",
                type: "datetime2",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "DeactivatedAt",
                schema: "user",
                table: "ArrayCustomerSettings",
                type: "datetime2",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ActivatedAt",
                schema: "user",
                table: "ArrayCustomerSettings");

            migrationBuilder.DropColumn(
                name: "DeactivatedAt",
                schema: "user",
                table: "ArrayCustomerSettings");
        }
    }
}
