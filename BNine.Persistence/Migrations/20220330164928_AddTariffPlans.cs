﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace BNine.Persistence.Migrations
{
    public partial class AddTariffPlans : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.EnsureSchema(
                name: "tariff");

            migrationBuilder.CreateTable(
                name: "TariffPlans",
                schema: "tariff",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false, defaultValueSql: "newsequentialid()"),
                    Name = table.Column<string>(type: "nvarchar(512)", maxLength: 512, nullable: false),
                    Type = table.Column<int>(type: "int", nullable: false),
                    MonthlyFee = table.Column<decimal>(type: "decimal(18,6)", nullable: false),
                    ClassificationId = table.Column<int>(type: "int", nullable: false),
                    Picture = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    Features = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TariffPlans", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "UserTariffPlans",
                schema: "tariff",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false, defaultValueSql: "newsequentialid()"),
                    TariffPlanId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    UserId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    CreatedAt = table.Column<DateTime>(type: "datetime2", nullable: false),
                    NextPaymentDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    Status = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UserTariffPlans", x => x.Id);
                    table.ForeignKey(
                        name: "FK_UserTariffPlans_TariffPlans_TariffPlanId",
                        column: x => x.TariffPlanId,
                        principalSchema: "tariff",
                        principalTable: "TariffPlans",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_UserTariffPlans_Users_UserId",
                        column: x => x.UserId,
                        principalSchema: "user",
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.InsertData(
                schema: "tariff",
                table: "TariffPlans",
                columns: new[] { "Id", "ClassificationId", "Features", "MonthlyFee", "Name", "Picture", "Type" },
                values: new object[] { new Guid("b6edf67d-5fdd-4246-b47a-259b4c4e49be"), 691, "[\r\n                    \"B9 Advance Plan\",\r\n                    \"B9 Advance of up to 100% of your Paycheck\",\r\n                    \"Credit Report\",\r\n                    \"Credit Bureau Score\",\r\n                    \"Credit Score Simulator\"\r\n                ]", 19.99m, "B9 Premium Plan", "https://b9devaccount.blob.core.windows.net/static-documents-container/TariffPlans/B9_Premium_Plan-20220330-141043.png", 2 });

            migrationBuilder.InsertData(
                schema: "tariff",
                table: "TariffPlans",
                columns: new[] { "Id", "ClassificationId", "Features", "MonthlyFee", "Name", "Picture", "Type" },
                values: new object[] { new Guid("b9491ab9-601b-406b-a689-a9b311773b73"), 693, "[\r\n                    \"B9 Visa® Debit Card with 4 % Cashback\",\r\n                    \"No Fee instant B9 Advances (up to $300)\",\r\n                    \"No Fee ACH transfers\",\r\n                    \"No Fee instant transfers to\",\r\n                    \"B9 Members\"\r\n                ]", 9.99m, "B9 Advance Plan", "https://b9devaccount.blob.core.windows.net/static-documents-container/TariffPlans/B9_Advance_Plan-20220330-141043.png", 1 });

            migrationBuilder.CreateIndex(
                name: "IX_UserTariffPlans_TariffPlanId",
                schema: "tariff",
                table: "UserTariffPlans",
                column: "TariffPlanId");

            migrationBuilder.CreateIndex(
                name: "IX_UserTariffPlans_UserId",
                schema: "tariff",
                table: "UserTariffPlans",
                column: "UserId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "UserTariffPlans",
                schema: "tariff");

            migrationBuilder.DropTable(
                name: "TariffPlans",
                schema: "tariff");
        }
    }
}
