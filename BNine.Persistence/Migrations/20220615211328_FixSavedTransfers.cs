﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace BNine.Persistence.Migrations
{
    public partial class FixSavedTransfers : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_SavedTransfer_Users_UserId",
                schema: "transfer",
                table: "SavedTransfer");

            migrationBuilder.DropPrimaryKey(
                name: "PK_SavedTransfer",
                schema: "transfer",
                table: "SavedTransfer");

            migrationBuilder.RenameTable(
                name: "SavedTransfer",
                schema: "transfer",
                newName: "SavedTransfers",
                newSchema: "transfer");

            migrationBuilder.RenameIndex(
                name: "IX_SavedTransfer_UserId",
                schema: "transfer",
                table: "SavedTransfers",
                newName: "IX_SavedTransfers_UserId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_SavedTransfers",
                schema: "transfer",
                table: "SavedTransfers",
                column: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_SavedTransfers_Users_UserId",
                schema: "transfer",
                table: "SavedTransfers",
                column: "UserId",
                principalSchema: "user",
                principalTable: "Users",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_SavedTransfers_Users_UserId",
                schema: "transfer",
                table: "SavedTransfers");

            migrationBuilder.DropPrimaryKey(
                name: "PK_SavedTransfers",
                schema: "transfer",
                table: "SavedTransfers");

            migrationBuilder.RenameTable(
                name: "SavedTransfers",
                schema: "transfer",
                newName: "SavedTransfer",
                newSchema: "transfer");

            migrationBuilder.RenameIndex(
                name: "IX_SavedTransfers_UserId",
                schema: "transfer",
                table: "SavedTransfer",
                newName: "IX_SavedTransfer_UserId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_SavedTransfer",
                schema: "transfer",
                table: "SavedTransfer",
                column: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_SavedTransfer_Users_UserId",
                schema: "transfer",
                table: "SavedTransfer",
                column: "UserId",
                principalSchema: "user",
                principalTable: "Users",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
