﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace BNine.Persistence.Migrations
{
    public partial class add_indexes_on_date_columns : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateIndex(
                name: "IX_Documents_SysStartTime",
                table: "Documents",
                column: "SysStartTime",
                schema: "user");

            migrationBuilder.CreateIndex(
                name: "IX_Addresses_SysStartTime",
                table: "Addresses",
                column: "SysStartTime",
                schema: "user");

            migrationBuilder.CreateIndex(
                name: "IX_UserSettings_SysStartTime",
                table: "UserSettings",
                column: "SysStartTime",
                schema: "user");

            migrationBuilder.CreateIndex(
                name: "IX_Users_SysStartTime",
                table: "Users",
                column: "SysStartTime",
                schema: "user");

            migrationBuilder.CreateIndex(
                name: "IX_Identities_SysStartTime",
                table: "Identities",
                column: "SysStartTime",
                schema: "user");

            migrationBuilder.CreateIndex(
                name: "IX_EarlySalarySettings_SysStartTime",
                table: "EarlySalarySettings",
                column: "SysStartTime",
                schema: "user");

            migrationBuilder.CreateIndex(
                name: "IX_Plaid_UpdatedAt",
                schema: "raw_data",
                table: "Plaid",
                column: "UpdatedAt");

            migrationBuilder.CreateIndex(
                name: "IX_Argyle_UpdatedAt",
                schema: "raw_data",
                table: "Argyle",
                column: "UpdatedAt");

            migrationBuilder.CreateIndex(
                name: "IX_OnboardingEvents_CreatedAt",
                schema: "logs",
                table: "OnboardingEvents",
                column: "CreatedAt");

            migrationBuilder.CreateIndex(
                name: "IX_ChangeBlockStatusEvents_CreatedAt",
                schema: "logs",
                table: "ChangeBlockStatusEvents",
                column: "CreatedAt");

            migrationBuilder.CreateIndex(
                name: "IX_OTPs_Date",
                table: "OTPs",
                column: "Date");

            migrationBuilder.CreateIndex(
                name: "IX_Notifications_CreatedAt",
                table: "Notifications",
                column: "CreatedAt");

            migrationBuilder.CreateIndex(
                name: "IX_LoanSpecialOffers_UpdatedAt",
                table: "LoanSpecialOffers",
                column: "UpdatedAt");

            migrationBuilder.CreateIndex(
                name: "IX_KYCVerificationResults_CreatedAt",
                table: "KYCVerificationResults",
                column: "CreatedAt");

            migrationBuilder.CreateIndex(
                name: "IX_ExternalBankAccounts_UpdatedAt",
                table: "ExternalBankAccounts",
                column: "UpdatedAt");

            migrationBuilder.CreateIndex(
                name: "IX_ExternalBankAccountBalances_CreatedAt",
                table: "ExternalBankAccountBalances",
                column: "CreatedAt");

            migrationBuilder.CreateIndex(
                name: "IX_EarlySalaryWidgetConfigurations_UpdatedAt",
                table: "EarlySalaryWidgetConfigurations",
                column: "UpdatedAt");

            migrationBuilder.CreateIndex(
                name: "IX_DevicesDetails_UpdatedAt",
                table: "DevicesDetails",
                column: "UpdatedAt");

            migrationBuilder.CreateIndex(
                name: "IX_Devices_UpdatedAt",
                table: "Devices",
                column: "UpdatedAt");

            migrationBuilder.CreateIndex(
                name: "IX_BonusCodes_UpdatedAt",
                table: "BonusCodes",
                column: "UpdatedAt");

            migrationBuilder.CreateIndex(
                name: "IX_AnonymousAppsflyerIds_CreatedAt",
                table: "AnonymousAppsflyerIds",
                column: "CreatedAt");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_Documents_SysStartTime",
                table: "Documents",
                schema: "user");

            migrationBuilder.DropIndex(
                name: "IX_Addresses_SysStartTime",
                table: "Addresses",
                schema: "user");

            migrationBuilder.DropIndex(
                name: "IX_UserSettings_SysStartTime",
                table: "UserSettings",
                schema: "user");

            migrationBuilder.DropIndex(
                name: "IX_Users_SysStartTime",
                table: "Users",
                schema: "user");

            migrationBuilder.DropIndex(
                name: "IX_Identities_SysStartTime",
                table: "Identities",
                schema: "user");

            migrationBuilder.DropIndex(
                name: "IX_EarlySalarySettings_SysStartTime",
                table: "EarlySalarySettings",
                schema: "user");

            migrationBuilder.DropIndex(
                name: "IX_Plaid_UpdatedAt",
                schema: "raw_data",
                table: "Plaid");

            migrationBuilder.DropIndex(
                name: "IX_Argyle_UpdatedAt",
                schema: "raw_data",
                table: "Argyle");

            migrationBuilder.DropIndex(
                name: "IX_OnboardingEvents_CreatedAt",
                schema: "logs",
                table: "OnboardingEvents");

            migrationBuilder.DropIndex(
                name: "IX_ChangeBlockStatusEvents_CreatedAt",
                schema: "logs",
                table: "ChangeBlockStatusEvents");

            migrationBuilder.DropIndex(
                name: "IX_OTPs_Date",
                table: "OTPs");

            migrationBuilder.DropIndex(
                name: "IX_Notifications_CreatedAt",
                table: "Notifications");

            migrationBuilder.DropIndex(
                name: "IX_LoanSpecialOffers_UpdatedAt",
                table: "LoanSpecialOffers");

            migrationBuilder.DropIndex(
                name: "IX_KYCVerificationResults_CreatedAt",
                table: "KYCVerificationResults");

            migrationBuilder.DropIndex(
                name: "IX_ExternalBankAccounts_UpdatedAt",
                table: "ExternalBankAccounts");

            migrationBuilder.DropIndex(
                name: "IX_ExternalBankAccountBalances_CreatedAt",
                table: "ExternalBankAccountBalances");

            migrationBuilder.DropIndex(
                name: "IX_EarlySalaryWidgetConfigurations_UpdatedAt",
                table: "EarlySalaryWidgetConfigurations");

            migrationBuilder.DropIndex(
                name: "IX_DevicesDetails_UpdatedAt",
                table: "DevicesDetails");

            migrationBuilder.DropIndex(
                name: "IX_Devices_UpdatedAt",
                table: "Devices");

            migrationBuilder.DropIndex(
                name: "IX_BonusCodes_UpdatedAt",
                table: "BonusCodes");

            migrationBuilder.DropIndex(
                name: "IX_AnonymousAppsflyerIds_CreatedAt",
                table: "AnonymousAppsflyerIds");
        }
    }
}
