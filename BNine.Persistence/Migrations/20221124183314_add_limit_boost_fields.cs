﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace BNine.Persistence.Migrations
{
    public partial class add_limit_boost_fields : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<decimal>(
                name: "BoostLimit1",
                schema: "user",
                table: "AdvanceSyncedStats",
                type: "decimal(18,2)",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<decimal>(
                name: "BoostLimit1Price",
                schema: "user",
                table: "AdvanceSyncedStats",
                type: "decimal(18,2)",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<decimal>(
                name: "BoostLimit2",
                schema: "user",
                table: "AdvanceSyncedStats",
                type: "decimal(18,2)",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<decimal>(
                name: "BoostLimit2Price",
                schema: "user",
                table: "AdvanceSyncedStats",
                type: "decimal(18,2)",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<decimal>(
                name: "BoostLimit3",
                schema: "user",
                table: "AdvanceSyncedStats",
                type: "decimal(18,2)",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<decimal>(
                name: "BoostLimit3Price",
                schema: "user",
                table: "AdvanceSyncedStats",
                type: "decimal(18,2)",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<bool>(
                name: "HasBoostLimit",
                schema: "user",
                table: "AdvanceSyncedStats",
                type: "bit",
                nullable: false,
                defaultValue: false);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "BoostLimit1",
                schema: "user",
                table: "AdvanceSyncedStats");

            migrationBuilder.DropColumn(
                name: "BoostLimit1Price",
                schema: "user",
                table: "AdvanceSyncedStats");

            migrationBuilder.DropColumn(
                name: "BoostLimit2",
                schema: "user",
                table: "AdvanceSyncedStats");

            migrationBuilder.DropColumn(
                name: "BoostLimit2Price",
                schema: "user",
                table: "AdvanceSyncedStats");

            migrationBuilder.DropColumn(
                name: "BoostLimit3",
                schema: "user",
                table: "AdvanceSyncedStats");

            migrationBuilder.DropColumn(
                name: "BoostLimit3Price",
                schema: "user",
                table: "AdvanceSyncedStats");

            migrationBuilder.DropColumn(
                name: "HasBoostLimit",
                schema: "user",
                table: "AdvanceSyncedStats");
        }
    }
}
