﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace BNine.Persistence.Migrations
{
    public partial class add_marketing_banners_tables : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "MarketingBanners",
                schema: "settings",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false, defaultValueSql: "newsequentialid()"),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    IsEnabled = table.Column<bool>(type: "bit", nullable: false),
                    Title = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    Subtitle = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Priority = table.Column<int>(type: "int", nullable: false, defaultValue: 0),
                    ButtonText = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    ButtonDeeplink = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    HasCloseButton = table.Column<bool>(type: "bit", nullable: false, defaultValue: true),
                    ShowFrom = table.Column<DateTime>(type: "datetime2", nullable: true),
                    ShowTil = table.Column<DateTime>(type: "datetime2", nullable: true),
                    ShowCooldown = table.Column<TimeSpan>(type: "time", nullable: true),
                    MinUserRegistrationDate = table.Column<DateTime>(type: "datetime2", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MarketingBanners", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "MarketingBannersSeen",
                schema: "dbo",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false, defaultValueSql: "newsequentialid()"),
                    UserId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    BannerId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    BannerName = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    NextDisplayTime = table.Column<DateTime>(type: "datetime2", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MarketingBannersSeen", x => x.Id);
                    table.ForeignKey(
                        name: "FK_MarketingBannersSeen_MarketingBanners_BannerId",
                        column: x => x.BannerId,
                        principalSchema: "settings",
                        principalTable: "MarketingBanners",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_MarketingBannersSeen_Users_UserId",
                        column: x => x.UserId,
                        principalSchema: "user",
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.InsertData(
                schema: "settings",
                table: "MarketingBanners",
                columns: new[] { "Id", "ButtonDeeplink", "ButtonText", "HasCloseButton", "IsEnabled", "MinUserRegistrationDate", "Name", "ShowCooldown", "ShowFrom", "ShowTil", "Subtitle", "Title" },
                values: new object[] { new Guid("b2aa4f13-dc17-4d13-a613-0b6088da5826"), "cashback-program", "ACTIVATE CASHBACK NOW", true, true, null, "CashbackBanner", null, new DateTime(2022, 7, 25, 6, 0, 0, 0, DateTimeKind.Unspecified), null, null, "Pay and earn with B9 cards!" });

            migrationBuilder.CreateIndex(
                name: "IX_MarketingBannersSeen_BannerId",
                schema: "dbo",
                table: "MarketingBannersSeen",
                column: "BannerId");

            migrationBuilder.CreateIndex(
                name: "IX_MarketingBannersSeen_UserId",
                schema: "dbo",
                table: "MarketingBannersSeen",
                column: "UserId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "MarketingBannersSeen",
                schema: "dbo");

            migrationBuilder.DropTable(
                name: "MarketingBanners",
                schema: "settings");
        }
    }
}
