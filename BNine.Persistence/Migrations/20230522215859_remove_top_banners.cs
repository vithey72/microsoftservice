﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace BNine.Persistence.Migrations
{
    public partial class remove_top_banners : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                schema: "settings",
                table: "MarketingBanners",
                keyColumn: "Id",
                keyValue: new Guid("b2aa4f13-dc17-4d13-a613-0b6088da5826"));

            migrationBuilder.DeleteData(
                schema: "settings",
                table: "MarketingBanners",
                keyColumn: "Id",
                keyValue: new Guid("f8fc9612-6490-49a3-a87d-540532bc2783"));

            migrationBuilder.DropColumn(
                name: "HasCloseButton",
                schema: "settings",
                table: "MarketingBanners");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "HasCloseButton",
                schema: "settings",
                table: "MarketingBanners",
                type: "bit",
                nullable: true,
                defaultValue: true);

            migrationBuilder.InsertData(
                schema: "settings",
                table: "MarketingBanners",
                columns: new[] { "Id", "ButtonDeeplink", "ButtonDeeplinkArguments", "ButtonText", "CreatedAt", "EntityType", "ExternalLink", "Groups", "HasCloseButton", "IsEnabled", "MinUserRegistrationDate", "Name", "Priority", "ShowCooldown", "ShowCooldownProgressive", "ShowFrom", "ShowTil", "Subtitle", "Title", "UpdatedAt" },
                values: new object[] { new Guid("b2aa4f13-dc17-4d13-a613-0b6088da5826"), "cashback-program", null, "ACTIVATE CASHBACK NOW", new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "topbanner", null, null, true, true, null, "CashbackBanner", 0, null, null, new DateTime(2022, 7, 25, 6, 0, 0, 0, DateTimeKind.Unspecified), null, null, "Pay and earn with B9 cards!", null });

            migrationBuilder.InsertData(
                schema: "settings",
                table: "MarketingBanners",
                columns: new[] { "Id", "ButtonDeeplink", "ButtonDeeplinkArguments", "ButtonText", "CreatedAt", "EntityType", "ExternalLink", "Groups", "HasCloseButton", "IsEnabled", "MinUserRegistrationDate", "Name", "Priority", "ShowCooldown", "ShowCooldownProgressive", "ShowFrom", "ShowTil", "Subtitle", "Title", "UpdatedAt" },
                values: new object[] { new Guid("f8fc9612-6490-49a3-a87d-540532bc2783"), "premium-tariff-plan", null, "GET MY DISCOUNT", new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "topbanner", null, null, true, true, null, "PremiumTariff3MonthsDiscount", 0, null, "[\"1.00:00:00\",\"3.00:00:00\",\"7.00:00:00\"]", new DateTime(2022, 8, 17, 6, 0, 0, 0, DateTimeKind.Unspecified), null, null, "Discounted Premium Plan", null });
        }
    }
}
