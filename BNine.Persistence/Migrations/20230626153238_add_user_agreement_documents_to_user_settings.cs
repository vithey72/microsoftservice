﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace BNine.Persistence.Migrations
{
    public partial class add_user_agreement_documents_to_user_settings : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "ConfirmedAgreementDocuments",
                schema: "user",
                table: "UserSettings",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "Key",
                schema: "settings",
                table: "StaticDocuments",
                type: "nvarchar(450)",
                nullable: true,
                oldClrType: typeof(string),
                oldType: "nvarchar(max)",
                oldNullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "IsAgreementRequired",
                schema: "settings",
                table: "StaticDocuments",
                type: "bit",
                nullable: false,
                defaultValue: false);

            migrationBuilder.CreateIndex(
                name: "IX_StaticDocument_Key",
                schema: "settings",
                table: "StaticDocuments",
                column: "Key",
                unique: true,
                filter: "[Key] IS NOT NULL");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_StaticDocument_Key",
                schema: "settings",
                table: "StaticDocuments");

            migrationBuilder.DropColumn(
                name: "ConfirmedAgreementDocuments",
                schema: "user",
                table: "UserSettings");

            migrationBuilder.DropColumn(
                name: "IsAgreementRequired",
                schema: "settings",
                table: "StaticDocuments");

            migrationBuilder.AlterColumn<string>(
                name: "Key",
                schema: "settings",
                table: "StaticDocuments",
                type: "nvarchar(max)",
                nullable: true,
                oldClrType: typeof(string),
                oldType: "nvarchar(450)",
                oldNullable: true);
        }
    }
}
