﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace BNine.Persistence.Migrations
{
    public partial class addArgyleData : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Argyle",
                schema: "raw_data",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false, defaultValueSql: "newsequentialid()"),
                    ArgyleUserId = table.Column<Guid>(nullable: false),
                    AccountId = table.Column<Guid>(nullable: false),
                    CreatedAt = table.Column<DateTime>(nullable: false, defaultValueSql: "getutcdate()"),
                    UpdatedAt = table.Column<DateTime>(nullable: false, defaultValueSql: "getutcdate()"),
                    AccountsData = table.Column<string>(nullable: true),
                    ActivitiesData = table.Column<string>(nullable: true),
                    DocumentsData = table.Column<string>(nullable: true),
                    EmploymentsData = table.Column<string>(nullable: true),
                    FinancesData = table.Column<string>(nullable: true),
                    PayoutsData = table.Column<string>(nullable: true),
                    ProfilesData = table.Column<string>(nullable: true),
                    ReputationsData = table.Column<string>(nullable: true),
                    VehiclesData = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Argyle", x => x.Id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Argyle",
                schema: "raw_data");
        }
    }
}
