﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace BNine.Persistence.Migrations
{
    public partial class fix_naming_for_user_settings_agreements : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "ConfirmedAgreementDocuments",
                schema: "user",
                table: "UserSettings",
                newName: "ConfirmedAgreementItems");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "ConfirmedAgreementItems",
                schema: "user",
                table: "UserSettings",
                newName: "ConfirmedAgreementDocuments");
        }
    }
}
