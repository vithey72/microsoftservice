﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace BNine.Persistence.Migrations
{
    public partial class changePaymentTypeToTransferViewTypeInTransactionCategory : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "PaymentTypeIdsJsonArrayString",
                table: "TransactionCategories",
                newName: "TransferDisplayTypeIdsJsonArrayString");

            migrationBuilder.UpdateData(
                table: "TransactionCategories",
                keyColumn: "Key",
                keyValue: "ach-sent",
                column: "TransferDisplayTypeIdsJsonArrayString",
                value: "[22]");

            migrationBuilder.UpdateData(
                table: "TransactionCategories",
                keyColumn: "Key",
                keyValue: "internal-transfer-sent",
                column: "TransferDisplayTypeIdsJsonArrayString",
                value: "[23]");

            migrationBuilder.UpdateData(
                table: "TransactionCategories",
                keyColumn: "Key",
                keyValue: "sent-to-external-cards",
                column: "TransferDisplayTypeIdsJsonArrayString",
                value: "[25]");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "TransferDisplayTypeIdsJsonArrayString",
                table: "TransactionCategories",
                newName: "PaymentTypeIdsJsonArrayString");

            migrationBuilder.UpdateData(
                table: "TransactionCategories",
                keyColumn: "Key",
                keyValue: "ach-sent",
                column: "PaymentTypeIdsJsonArrayString",
                value: "[2]");

            migrationBuilder.UpdateData(
                table: "TransactionCategories",
                keyColumn: "Key",
                keyValue: "internal-transfer-sent",
                column: "PaymentTypeIdsJsonArrayString",
                value: "[1]");

            migrationBuilder.UpdateData(
                table: "TransactionCategories",
                keyColumn: "Key",
                keyValue: "sent-to-external-cards",
                column: "PaymentTypeIdsJsonArrayString",
                value: "[5]");
        }
    }
}
