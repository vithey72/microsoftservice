﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace BNine.Persistence.Migrations
{
    public partial class add_delayed_boost_to_advance_sync : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "BoostLimit1HoursToDelay",
                schema: "user",
                table: "AdvanceSyncedStats",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "BoostLimit2HoursToDelay",
                schema: "user",
                table: "AdvanceSyncedStats",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "BoostLimit3HoursToDelay",
                schema: "user",
                table: "AdvanceSyncedStats",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<string>(
                name: "DelayedBoostSettings",
                schema: "user",
                table: "AdvanceSyncedStats",
                type: "nvarchar(max)",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "BoostLimit1HoursToDelay",
                schema: "user",
                table: "AdvanceSyncedStats");

            migrationBuilder.DropColumn(
                name: "BoostLimit2HoursToDelay",
                schema: "user",
                table: "AdvanceSyncedStats");

            migrationBuilder.DropColumn(
                name: "BoostLimit3HoursToDelay",
                schema: "user",
                table: "AdvanceSyncedStats");

            migrationBuilder.DropColumn(
                name: "DelayedBoostSettings",
                schema: "user",
                table: "AdvanceSyncedStats");
        }
    }
}
