﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace BNine.Persistence.Migrations
{
    public partial class RenameTransferTypeColumnToUserDocumentsTable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "TransferType",
                table: "TransferErrorLogs",
                newName: "TransferNetworkType");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "TransferNetworkType",
                table: "TransferErrorLogs",
                newName: "TransferType");
        }
    }
}
