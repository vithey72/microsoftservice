﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace BNine.Persistence.Migrations
{
    public partial class AddClosedStatusFieldsToUserTable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<DateTime>(
                name: "ClosedAt",
                schema: "user",
                table: "Users",
                type: "datetime2",
                nullable: true);

            migrationBuilder.AddColumn<Guid>(
                name: "UserCloseReasonId",
                schema: "user",
                table: "Users",
                type: "uniqueidentifier",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Users_UserCloseReasonId",
                schema: "user",
                table: "Users",
                column: "UserCloseReasonId");

            migrationBuilder.AddForeignKey(
                name: "FK_Users_UserBlockReasons_UserCloseReasonId",
                schema: "user",
                table: "Users",
                column: "UserCloseReasonId",
                principalSchema: "user",
                principalTable: "UserBlockReasons",
                principalColumn: "Id");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Users_UserBlockReasons_UserCloseReasonId",
                schema: "user",
                table: "Users");

            migrationBuilder.DropIndex(
                name: "IX_Users_UserCloseReasonId",
                schema: "user",
                table: "Users");

            migrationBuilder.DropColumn(
                name: "ClosedAt",
                schema: "user",
                table: "Users");

            migrationBuilder.DropColumn(
                name: "UserCloseReasonId",
                schema: "user",
                table: "Users");
        }
    }
}
