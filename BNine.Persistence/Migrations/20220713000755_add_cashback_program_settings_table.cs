﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace BNine.Persistence.Migrations
{
    public partial class add_cashback_program_settings_table : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "CashbackProgramSettings",
                schema: "settings",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false, defaultValueSql: "newsequentialid()"),
                    MinimumSpentAmount = table.Column<decimal>(type: "decimal(18,2)", nullable: false),
                    SlideshowScreens = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    MinCashbackCategories = table.Column<int>(type: "int", nullable: false),
                    MaxCashbackCategories = table.Column<int>(type: "int", nullable: false),
                    UpdatedAt = table.Column<DateTime>(type: "datetime2", nullable: false, defaultValueSql: "GETDATE()")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CashbackProgramSettings", x => x.Id);
                });

            migrationBuilder.InsertData(
                schema: "settings",
                table: "CashbackProgramSettings",
                columns: new[] { "Id", "MaxCashbackCategories", "MinCashbackCategories", "MinimumSpentAmount", "SlideshowScreens" },
                values: new object[] { new Guid("8d8f7fea-67d5-46aa-857f-ec024d97e25a"), 4, 1, 100m, "{\"screens\":[{\"header\":\"Get cashback every month\",\"text\":\"Select the categories you want to use the most every month. Cashback will be deposited by the 5th of the following month.\",\"buttonText\":\"NEXT\",\"imageUrl\":\"https://b9devaccount.blob.core.windows.net/static-documents-container/CashbackSlideshow/page1.png\"},{\"header\":\"Spend at least $100\",\"text\":\"You will begin to earn cashback as soon as you reach $100 worth of the categories you selected.\",\"buttonText\":\"NEXT\",\"imageUrl\":\"https://b9devaccount.blob.core.windows.net/static-documents-container/CashbackSlideshow/page2.png\"},{\"header\":\"Select categories of cashback\",\"text\":\"Right under selected categories you'll see the categories you chose for that particular month. You will need to select categories every month.\",\"buttonText\":\"NEXT\",\"imageUrl\":\"https://b9devaccount.blob.core.windows.net/static-documents-container/CashbackSlideshow/page3.png\",\"link\":\"https://b9devaccount.blob.core.windows.net/static-documents-container/Terms of Service.pdf\",\"linkText\":\"Terms and conditions\"}]}" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "CashbackProgramSettings",
                schema: "settings");
        }
    }
}
