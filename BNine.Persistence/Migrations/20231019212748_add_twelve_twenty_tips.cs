﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace BNine.Persistence.Migrations
{
    public partial class add_twelve_twenty_tips : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData(
                schema: "tariff",
                table: "PaidUserServices",
                columns: new[] { "Id", "ChargeTypeId", "CreatedAt", "Description", "DurationInDays", "Name", "ObsoletedAt", "ServiceBusName" },
                values: new object[] { new Guid("6ee6a056-af28-4e7e-b4bf-bbac91cd3787"), 72, new DateTime(2023, 10, 19, 0, 0, 0, 0, DateTimeKind.Unspecified), "Tip of $12", 14, "After Advance Tip 12", null, "after_advance_tip" });

            migrationBuilder.InsertData(
                schema: "tariff",
                table: "PaidUserServices",
                columns: new[] { "Id", "ChargeTypeId", "CreatedAt", "Description", "DurationInDays", "Name", "ObsoletedAt", "ServiceBusName" },
                values: new object[] { new Guid("c20b586f-2d4b-4675-bc6a-6833b335d1da"), 73, new DateTime(2023, 10, 19, 0, 0, 0, 0, DateTimeKind.Unspecified), "Tip of $20", 14, "After Advance Tip 20", null, "after_advance_tip" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                schema: "tariff",
                table: "PaidUserServices",
                keyColumn: "Id",
                keyValue: new Guid("6ee6a056-af28-4e7e-b4bf-bbac91cd3787"));

            migrationBuilder.DeleteData(
                schema: "tariff",
                table: "PaidUserServices",
                keyColumn: "Id",
                keyValue: new Guid("c20b586f-2d4b-4675-bc6a-6833b335d1da"));
        }
    }
}
