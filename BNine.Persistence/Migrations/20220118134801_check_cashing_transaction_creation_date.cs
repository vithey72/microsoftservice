﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace BNine.Persistence.Migrations
{
    public partial class check_cashing_transaction_creation_date : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<DateTime>(
                name: "CreatedDate",
                table: "CheckCashingTransactions",
                nullable: false,
                defaultValueSql: "getutcdate()");

            migrationBuilder.CreateIndex(
                name: "IX_CheckCashingTransactions_CreatedDate",
                table: "CheckCashingTransactions",
                column: "CreatedDate");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_CheckCashingTransactions_CreatedDate",
                table: "CheckCashingTransactions");

            migrationBuilder.DropColumn(
                name: "CreatedDate",
                table: "CheckCashingTransactions");
        }
    }
}
