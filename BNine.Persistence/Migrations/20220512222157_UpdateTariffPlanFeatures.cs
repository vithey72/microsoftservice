﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace BNine.Persistence.Migrations
{
    public partial class UpdateTariffPlanFeatures : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                schema: "tariff",
                table: "TariffPlans",
                keyColumn: "Id",
                keyValue: new Guid("b6edf67d-5fdd-4246-b47a-259b4c4e49be"),
                column: "Features",
                value: "[\r\n                    \"Everything in the \\nB9 Advance Plan—plus:\",\r\n                    \"Advance up to 100% \\nof your Paycheck\",\r\n                    \"Credit Report\",\r\n                    \"Credit Score\",\r\n                    \"Credit Score Simulator\"\r\n                ]");

            migrationBuilder.UpdateData(
                schema: "tariff",
                table: "TariffPlans",
                keyColumn: "Id",
                keyValue: new Guid("b9491ab9-601b-406b-a689-a9b311773b73"),
                column: "Features",
                value: "[\r\n                    \"B9 Visa® Debit Card with \\n4% Cashback\",\r\n                    \"Advance up to $300\",\r\n                    \"Get advances instantly\",\r\n                    \"Instant transfers to B9 Members\"\r\n                ]");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                schema: "tariff",
                table: "TariffPlans",
                keyColumn: "Id",
                keyValue: new Guid("b6edf67d-5fdd-4246-b47a-259b4c4e49be"),
                column: "Features",
                value: "[\r\n                    \"B9 Advance Plan\",\r\n                    \"B9 Advance of up to 100%\\n of your Paycheck\",\r\n                    \"Credit Report\",\r\n                    \"Credit Bureau Score\",\r\n                    \"Credit Score Simulator\"\r\n                ]");

            migrationBuilder.UpdateData(
                schema: "tariff",
                table: "TariffPlans",
                keyColumn: "Id",
                keyValue: new Guid("b9491ab9-601b-406b-a689-a9b311773b73"),
                column: "Features",
                value: "[\r\n                    \"B9 Visa® Debit Card with \\n4 % Cashback\",\r\n                    \"No Fee instant B9 Advances \\n(up to $300)\",\r\n                    \"No Fee ACH transfers\",\r\n                    \"No Fee instant transfers to B9 Members\"\r\n                ]");
        }
    }
}
