﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace BNine.Persistence.Migrations
{
    public partial class add_wallets : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.EnsureSchema(
                name: "wallet");

            migrationBuilder.CreateTable(
                name: "AppleWallets",
                schema: "wallet",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false, defaultValueSql: "newsequentialid()"),
                    Type = table.Column<int>(nullable: false),
                    CreatedAt = table.Column<DateTime>(nullable: false),
                    UpdatedAt = table.Column<DateTime>(nullable: false),
                    DeletedAt = table.Column<DateTime>(nullable: true),
                    DebitCardId = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AppleWallets", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AppleWallets_DebitCards_DebitCardId",
                        column: x => x.DebitCardId,
                        principalSchema: "bank",
                        principalTable: "DebitCards",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "GoogleWallets",
                schema: "wallet",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false, defaultValueSql: "newsequentialid()"),
                    Type = table.Column<int>(nullable: false),
                    CreatedAt = table.Column<DateTime>(nullable: false),
                    UpdatedAt = table.Column<DateTime>(nullable: false),
                    DeletedAt = table.Column<DateTime>(nullable: true),
                    DebitCardId = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_GoogleWallets", x => x.Id);
                    table.ForeignKey(
                        name: "FK_GoogleWallets_DebitCards_DebitCardId",
                        column: x => x.DebitCardId,
                        principalSchema: "bank",
                        principalTable: "DebitCards",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "SamsungWallets",
                schema: "wallet",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false, defaultValueSql: "newsequentialid()"),
                    Type = table.Column<int>(nullable: false),
                    CreatedAt = table.Column<DateTime>(nullable: false),
                    UpdatedAt = table.Column<DateTime>(nullable: false),
                    DeletedAt = table.Column<DateTime>(nullable: true),
                    DebitCardId = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SamsungWallets", x => x.Id);
                    table.ForeignKey(
                        name: "FK_SamsungWallets_DebitCards_DebitCardId",
                        column: x => x.DebitCardId,
                        principalSchema: "bank",
                        principalTable: "DebitCards",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_AppleWallets_DebitCardId",
                schema: "wallet",
                table: "AppleWallets",
                column: "DebitCardId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_GoogleWallets_DebitCardId",
                schema: "wallet",
                table: "GoogleWallets",
                column: "DebitCardId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_SamsungWallets_DebitCardId",
                schema: "wallet",
                table: "SamsungWallets",
                column: "DebitCardId",
                unique: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "AppleWallets",
                schema: "wallet");

            migrationBuilder.DropTable(
                name: "GoogleWallets",
                schema: "wallet");

            migrationBuilder.DropTable(
                name: "SamsungWallets",
                schema: "wallet");
        }
    }
}
