﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace BNine.Persistence.Migrations
{
    public partial class add_administrators : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.EnsureSchema(
                name: "administrators");

            migrationBuilder.AddColumn<bool>(
                name: "IsBlocked",
                schema: "user",
                table: "Users",
                nullable: false,
                defaultValue: false);

            migrationBuilder.CreateTable(
                name: "Administrators",
                schema: "administrators",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false, defaultValueSql: "newsequentialid()"),
                    Name = table.Column<string>(maxLength: 512, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Administrators", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "ExternalIdentities",
                schema: "administrators",
                columns: table => new
                {
                    Provider = table.Column<string>(maxLength: 512, nullable: false),
                    Id = table.Column<string>(maxLength: 512, nullable: false),
                    AdministratorId = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ExternalIdentities", x => new { x.Id, x.Provider });
                    table.ForeignKey(
                        name: "FK_ExternalIdentities_Administrators_AdministratorId",
                        column: x => x.AdministratorId,
                        principalSchema: "administrators",
                        principalTable: "Administrators",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_ExternalIdentities_AdministratorId",
                schema: "administrators",
                table: "ExternalIdentities",
                column: "AdministratorId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "ExternalIdentities",
                schema: "administrators");

            migrationBuilder.DropTable(
                name: "Administrators",
                schema: "administrators");

            migrationBuilder.DropColumn(
                name: "IsBlocked",
                schema: "user",
                table: "Users");
        }
    }
}
