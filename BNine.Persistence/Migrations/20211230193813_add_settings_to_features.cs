﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace BNine.Persistence.Migrations
{
    public partial class add_settings_to_features : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "SettingsSchema",
                schema: "features",
                table: "Features",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Settings",
                schema: "features",
                table: "FeatureGroups",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "SettingsSchema",
                schema: "features",
                table: "Features");

            migrationBuilder.DropColumn(
                name: "Settings",
                schema: "features",
                table: "FeatureGroups");
        }
    }
}
