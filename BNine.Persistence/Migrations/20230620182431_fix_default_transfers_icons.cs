﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace BNine.Persistence.Migrations
{
    public partial class fix_default_transfers_icons : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                schema: "settings",
                table: "TransferIconSettings",
                keyColumn: "Id",
                keyValue: 1,
                column: "IconUrl",
                value: "https://b9prodaccount.blob.core.windows.net/static-documents-container/TransactionIcons/01okAuth.png");

            migrationBuilder.UpdateData(
                schema: "settings",
                table: "TransferIconSettings",
                keyColumn: "Id",
                keyValue: 2,
                column: "IconUrl",
                value: "https://b9prodaccount.blob.core.windows.net/static-documents-container/TransactionIcons/02card.png");

            migrationBuilder.UpdateData(
                schema: "settings",
                table: "TransferIconSettings",
                keyColumn: "Id",
                keyValue: 3,
                column: "IconUrl",
                value: "https://b9prodaccount.blob.core.windows.net/static-documents-container/TransactionIcons/03atm.png");

            migrationBuilder.UpdateData(
                schema: "settings",
                table: "TransferIconSettings",
                keyColumn: "Id",
                keyValue: 4,
                column: "IconUrl",
                value: "https://b9prodaccount.blob.core.windows.net/static-documents-container/TransactionIcons/04membership.png");

            migrationBuilder.UpdateData(
                schema: "settings",
                table: "TransferIconSettings",
                keyColumn: "Id",
                keyValue: 5,
                column: "IconUrl",
                value: "https://b9prodaccount.blob.core.windows.net/static-documents-container/TransactionIcons/05fee.png");

            migrationBuilder.UpdateData(
                schema: "settings",
                table: "TransferIconSettings",
                keyColumn: "Id",
                keyValue: 6,
                column: "IconUrl",
                value: "https://b9prodaccount.blob.core.windows.net/static-documents-container/TransactionIcons/09moneybag.png");

            migrationBuilder.UpdateData(
                schema: "settings",
                table: "TransferIconSettings",
                keyColumn: "Id",
                keyValue: 7,
                column: "IconUrl",
                value: "https://b9prodaccount.blob.core.windows.net/static-documents-container/TransactionIcons/07bank.png");

            migrationBuilder.UpdateData(
                schema: "settings",
                table: "TransferIconSettings",
                keyColumn: "Id",
                keyValue: 8,
                column: "IconUrl",
                value: "https://b9prodaccount.blob.core.windows.net/static-documents-container/TransactionIcons/08person.png");

            migrationBuilder.UpdateData(
                schema: "settings",
                table: "TransferIconSettings",
                keyColumn: "Id",
                keyValue: 9,
                column: "IconUrl",
                value: "https://b9prodaccount.blob.core.windows.net/static-documents-container/TransactionIcons/09moneybag.png");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                schema: "settings",
                table: "TransferIconSettings",
                keyColumn: "Id",
                keyValue: 1,
                column: "IconUrl",
                value: "https://b9devaccount.blob.core.windows.net/static-documents-container/TransactionIcons/defaultCardOk.png");

            migrationBuilder.UpdateData(
                schema: "settings",
                table: "TransferIconSettings",
                keyColumn: "Id",
                keyValue: 2,
                column: "IconUrl",
                value: "https://b9devaccount.blob.core.windows.net/static-documents-container/TransactionIcons/cardWarning.png");

            migrationBuilder.UpdateData(
                schema: "settings",
                table: "TransferIconSettings",
                keyColumn: "Id",
                keyValue: 3,
                column: "IconUrl",
                value: "https://b9devaccount.blob.core.windows.net/static-documents-container/TransactionIcons/atm.png");

            migrationBuilder.UpdateData(
                schema: "settings",
                table: "TransferIconSettings",
                keyColumn: "Id",
                keyValue: 4,
                column: "IconUrl",
                value: "https://b9devaccount.blob.core.windows.net/static-documents-container/TransactionIcons/membership.png");

            migrationBuilder.UpdateData(
                schema: "settings",
                table: "TransferIconSettings",
                keyColumn: "Id",
                keyValue: 5,
                column: "IconUrl",
                value: "https://b9devaccount.blob.core.windows.net/static-documents-container/TransactionIcons/fee.png");

            migrationBuilder.UpdateData(
                schema: "settings",
                table: "TransferIconSettings",
                keyColumn: "Id",
                keyValue: 6,
                column: "IconUrl",
                value: "https://b9devaccount.blob.core.windows.net/static-documents-container/TransactionIcons/advance.png");

            migrationBuilder.UpdateData(
                schema: "settings",
                table: "TransferIconSettings",
                keyColumn: "Id",
                keyValue: 7,
                column: "IconUrl",
                value: "https://b9devaccount.blob.core.windows.net/static-documents-container/TransactionIcons/bank.png");

            migrationBuilder.UpdateData(
                schema: "settings",
                table: "TransferIconSettings",
                keyColumn: "Id",
                keyValue: 8,
                column: "IconUrl",
                value: "https://b9devaccount.blob.core.windows.net/static-documents-container/TransactionIcons/person.png");

            migrationBuilder.UpdateData(
                schema: "settings",
                table: "TransferIconSettings",
                keyColumn: "Id",
                keyValue: 9,
                column: "IconUrl",
                value: "https://b9devaccount.blob.core.windows.net/static-documents-container/TransactionIcons/moneyBag.png");
        }
    }
}
