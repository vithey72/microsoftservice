﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace BNine.Persistence.Migrations
{
    public partial class add_id_to_anonymous_appsflyer_users : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "AppsflyerId",
                table: "AnonymousAppsflyerIds",
                maxLength: 512,
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "AppsflyerId",
                table: "AnonymousAppsflyerIds");
        }
    }
}
