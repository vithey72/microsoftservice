﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace BNine.Persistence.Migrations
{
    public partial class add_CommonAccountSettings_BlockClosureOffsetFromActivationInDays : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "BlockClosureOffsetFromActivationInDays",
                schema: "settings",
                table: "CommonAccountSettings",
                type: "int",
                nullable: false,
                defaultValue: 10);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "BlockClosureOffsetFromActivationInDays",
                schema: "settings",
                table: "CommonAccountSettings");
        }
    }
}
