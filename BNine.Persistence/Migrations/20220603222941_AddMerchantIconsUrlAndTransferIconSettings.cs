﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace BNine.Persistence.Migrations
{
    public partial class AddMerchantIconsUrlAndTransferIconSettings : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "PictureUrl",
                schema: "settings",
                table: "TransferMerchantsSettings",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "TransferIconSettings",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false),
                    IconUrl = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TransferIconSettings", x => x.Id);
                });

            migrationBuilder.InsertData(
                table: "TransferIconSettings",
                columns: new[] { "Id", "IconUrl" },
                values: new object[,]
                {
                    { 1, "https://b9devaccount.blob.core.windows.net/static-documents-container/TransactionIcons/defaultCardOk.png" },
                    { 2, "https://b9devaccount.blob.core.windows.net/static-documents-container/TransactionIcons/cardWarning.png" },
                    { 3, "https://b9devaccount.blob.core.windows.net/static-documents-container/TransactionIcons/atm.png" },
                    { 4, "https://b9devaccount.blob.core.windows.net/static-documents-container/TransactionIcons/membership.png" },
                    { 5, "https://b9devaccount.blob.core.windows.net/static-documents-container/TransactionIcons/fee.png" },
                    { 6, "https://b9devaccount.blob.core.windows.net/static-documents-container/TransactionIcons/advance.png" },
                    { 7, "https://b9devaccount.blob.core.windows.net/static-documents-container/TransactionIcons/bank.png" },
                    { 8, "https://b9devaccount.blob.core.windows.net/static-documents-container/TransactionIcons/person.png" },
                    { 9, "https://b9devaccount.blob.core.windows.net/static-documents-container/TransactionIcons/moneyBag.png" }
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "TransferIconSettings");

            migrationBuilder.DropColumn(
                name: "PictureUrl",
                schema: "settings",
                table: "TransferMerchantsSettings");
        }
    }
}
