﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace BNine.Persistence.Migrations
{
    public partial class new_loan_settings : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<decimal>(
                name: "RequiredMinimalDeposit",
                schema: "settings",
                table: "LoanSettings",
                type: "decimal(18,2)",
                nullable: false,
                defaultValue: 300m);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "RequiredMinimalDeposit",
                schema: "settings",
                table: "LoanSettings");
        }
    }
}
