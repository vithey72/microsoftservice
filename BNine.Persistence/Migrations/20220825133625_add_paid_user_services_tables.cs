﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace BNine.Persistence.Migrations
{
    public partial class add_paid_user_services_tables : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "PaidUserServices",
                schema: "tariff",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false, defaultValueSql: "newsequentialid()"),
                    ChargeTypeId = table.Column<int>(type: "int", nullable: false),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    Description = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    DurationInDays = table.Column<int>(type: "int", nullable: false),
                    CreatedAt = table.Column<DateTime>(type: "datetime2", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PaidUserServices", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "UsedPaidUserServices",
                schema: "tariff",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false, defaultValueSql: "newsequentialid()"),
                    UserId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    ServiceId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    ChargeExternalId = table.Column<int>(type: "int", nullable: false),
                    CreatedAt = table.Column<DateTime>(type: "datetime2", nullable: false, defaultValueSql: "GETDATE()"),
                    ValidTo = table.Column<DateTime>(type: "datetime2", nullable: false),
                    IsOverdue = table.Column<bool>(type: "bit", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UsedPaidUserServices", x => x.Id);
                    table.ForeignKey(
                        name: "FK_UsedPaidUserServices_PaidUserServices_ServiceId",
                        column: x => x.ServiceId,
                        principalSchema: "tariff",
                        principalTable: "PaidUserServices",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.InsertData(
                schema: "tariff",
                table: "PaidUserServices",
                columns: new[] { "Id", "ChargeTypeId", "CreatedAt", "Description", "DurationInDays", "Name" },
                values: new object[] { new Guid("2dc6aeec-5ee7-4661-9338-50b3101e59da"), 265, new DateTime(2022, 8, 25, 0, 0, 0, 0, DateTimeKind.Unspecified), "One time payment for 2 weeks of premium tech support", 14, "Premium Support" });

            migrationBuilder.CreateIndex(
                name: "IX_UsedPaidUserServices_ServiceId",
                schema: "tariff",
                table: "UsedPaidUserServices",
                column: "ServiceId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "UsedPaidUserServices",
                schema: "tariff");

            migrationBuilder.DropTable(
                name: "PaidUserServices",
                schema: "tariff");
        }
    }
}
