﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace BNine.Persistence.Migrations
{
    public partial class AddWelcomeScreenPictureUrlToTariffPlan : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "WelcomeScreenPictureUrl",
                schema: "tariff",
                table: "TariffPlans",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.UpdateData(
                schema: "tariff",
                table: "TariffPlans",
                keyColumn: "Id",
                keyValue: new Guid("b6edf67d-5fdd-4246-b47a-259b4c4e49be"),
                column: "WelcomeScreenPictureUrl",
                value: "https://b9devaccount.blob.core.windows.net/static-documents-container/TariffPlans/Welcome_B9_Premium.png");

            migrationBuilder.UpdateData(
                schema: "tariff",
                table: "TariffPlans",
                keyColumn: "Id",
                keyValue: new Guid("b9491ab9-601b-406b-a689-a9b311773b73"),
                column: "WelcomeScreenPictureUrl",
                value: "https://b9devaccount.blob.core.windows.net/static-documents-container/TariffPlans/Welcome_B9_Advance.png");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "WelcomeScreenPictureUrl",
                schema: "tariff",
                table: "TariffPlans");
        }
    }
}
