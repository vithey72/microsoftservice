﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace BNine.Persistence.Migrations
{
    public partial class update_array_customer_table : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "UserToken",
                schema: "user",
                table: "ArrayCustomerSettings",
                newName: "ArrayUserId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "ArrayUserId",
                schema: "user",
                table: "ArrayCustomerSettings",
                newName: "UserToken");
        }
    }
}
