﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace BNine.Persistence.Migrations
{
    public partial class change_entities_for_new_kyc_2 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<Guid>(
                name: "EvaluationExternalId",
                table: "KYCVerificationResults",
                type: "uniqueidentifier",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "EvaluationProvider",
                table: "KYCVerificationResults",
                type: "int",
                nullable: false,
                defaultValue: 1);

            migrationBuilder.AddColumn<int>(
                name: "StatusEnum",
                table: "KYCVerificationResults",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.Sql(@"update KYCVerificationResults set StatusEnum = CASE Status
WHEN 'Approved' THEN 1
WHEN 'Denied' THEN 2
WHEN 'Manual Review' THEN 3
ELSE 0
END");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "EvaluationExternalId",
                table: "KYCVerificationResults");

            migrationBuilder.DropColumn(
                name: "EvaluationProvider",
                table: "KYCVerificationResults");

            migrationBuilder.DropColumn(
                name: "StatusEnum",
                table: "KYCVerificationResults");
        }
    }
}
