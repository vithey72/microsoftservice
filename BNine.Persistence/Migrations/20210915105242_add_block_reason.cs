﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace BNine.Persistence.Migrations
{
    public partial class add_block_reason : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<Guid>(
                name: "UserBlockReasonId",
                schema: "user",
                table: "Users",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "UserBlockReasons",
                schema: "user",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Value = table.Column<string>(nullable: true),
                    Order = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UserBlockReasons", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "ChangeBlockStatusEvents",
                schema: "logs",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false, defaultValueSql: "newsequentialid()"),
                    Action = table.Column<int>(nullable: false),
                    UserBlockReasonId = table.Column<Guid>(nullable: true),
                    UserId = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ChangeBlockStatusEvents", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ChangeBlockStatusEvents_UserBlockReasons_UserBlockReasonId",
                        column: x => x.UserBlockReasonId,
                        principalSchema: "user",
                        principalTable: "UserBlockReasons",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_ChangeBlockStatusEvents_Users_UserId",
                        column: x => x.UserId,
                        principalSchema: "user",
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.InsertData(
                schema: "user",
                table: "UserBlockReasons",
                columns: new[] { "Id", "Order", "Value" },
                values: new object[] { new Guid("7060a426-6f25-431a-a44f-dd40d3dd5dac"), 1, "Voluntary request" });

            migrationBuilder.InsertData(
                schema: "user",
                table: "UserBlockReasons",
                columns: new[] { "Id", "Order", "Value" },
                values: new object[] { new Guid("c92babbf-b706-4c08-8cea-7a83a25e1569"), 2, "Illegible ID" });

            migrationBuilder.InsertData(
                schema: "user",
                table: "UserBlockReasons",
                columns: new[] { "Id", "Order", "Value" },
                values: new object[] { new Guid("852db38e-1074-41b3-b3ed-a198ae8499cc"), 3, "Fraud/Risk rules" });

            migrationBuilder.CreateIndex(
                name: "IX_Users_UserBlockReasonId",
                schema: "user",
                table: "Users",
                column: "UserBlockReasonId");

            migrationBuilder.CreateIndex(
                name: "IX_ChangeBlockStatusEvents_UserBlockReasonId",
                schema: "logs",
                table: "ChangeBlockStatusEvents",
                column: "UserBlockReasonId");

            migrationBuilder.CreateIndex(
                name: "IX_ChangeBlockStatusEvents_UserId",
                schema: "logs",
                table: "ChangeBlockStatusEvents",
                column: "UserId");

            migrationBuilder.AddForeignKey(
                name: "FK_Users_UserBlockReasons_UserBlockReasonId",
                schema: "user",
                table: "Users",
                column: "UserBlockReasonId",
                principalSchema: "user",
                principalTable: "UserBlockReasons",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Users_UserBlockReasons_UserBlockReasonId",
                schema: "user",
                table: "Users");

            migrationBuilder.DropTable(
                name: "ChangeBlockStatusEvents",
                schema: "logs");

            migrationBuilder.DropTable(
                name: "UserBlockReasons",
                schema: "user");

            migrationBuilder.DropIndex(
                name: "IX_Users_UserBlockReasonId",
                schema: "user",
                table: "Users");

            migrationBuilder.DropColumn(
                name: "UserBlockReasonId",
                schema: "user",
                table: "Users");
        }
    }
}
