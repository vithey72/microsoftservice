﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace BNine.Persistence.Migrations
{
    public partial class add_potentian_ach_income_table : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "PotentialAchIncomeTransfers",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    CreatedAt = table.Column<DateTime>(type: "datetime2", nullable: false, defaultValueSql: "GETDATE()"),
                    TransferExternalId = table.Column<int>(type: "int", nullable: false),
                    Reference = table.Column<string>(type: "nvarchar(200)", maxLength: 200, nullable: false),
                    UserId = table.Column<Guid>(type: "uniqueidentifier", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PotentialAchIncomeTransfers", x => x.Id);
                    table.ForeignKey(
                        name: "FK_PotentialAchIncomeTransfers_Users_UserId",
                        column: x => x.UserId,
                        principalSchema: "user",
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_PotentialAchIncomeTransfers_CreatedAt",
                table: "PotentialAchIncomeTransfers",
                column: "CreatedAt");

            migrationBuilder.CreateIndex(
                name: "IX_PotentialAchIncomeTransfers_Reference",
                table: "PotentialAchIncomeTransfers",
                column: "Reference");

            migrationBuilder.CreateIndex(
                name: "IX_PotentialAchIncomeTransfers_TransferExternalId",
                table: "PotentialAchIncomeTransfers",
                column: "TransferExternalId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_PotentialAchIncomeTransfers_UserId",
                table: "PotentialAchIncomeTransfers",
                column: "UserId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "PotentialAchIncomeTransfers");
        }
    }
}
