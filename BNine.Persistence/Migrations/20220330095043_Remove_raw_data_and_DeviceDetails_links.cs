﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace BNine.Persistence.Migrations
{
    public partial class Remove_raw_data_and_DeviceDetails_links : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(
@"
IF EXISTS(SELECT * FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS AS C WHERE C.CONSTRAINT_NAME = 'FK_DevicesDetails_Users_UserId' AND C.TABLE_NAME = 'DevicesDetails')
BEGIN
    ALTER TABLE DevicesDetails
        DROP CONSTRAINT FK_DevicesDetails_Users_UserId
END

IF EXISTS(SELECT * FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS AS C WHERE C.CONSTRAINT_NAME = 'FK_DevicesDetails_Devices_DeviceId' AND C.TABLE_NAME = 'DevicesDetails')
BEGIN
    ALTER TABLE DevicesDetails
        DROP CONSTRAINT FK_DevicesDetails_Devices_DeviceId
END
");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
        }
    }
}
