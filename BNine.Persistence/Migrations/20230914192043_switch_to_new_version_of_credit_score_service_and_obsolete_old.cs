﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace BNine.Persistence.Migrations
{
    public partial class switch_to_new_version_of_credit_score_service_and_obsolete_old : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<DateTime>(
                name: "ObsoletedAt",
                schema: "tariff",
                table: "PaidUserServices",
                type: "datetime2",
                nullable: true);

            migrationBuilder.UpdateData(
                schema: "tariff",
                table: "PaidUserServices",
                keyColumn: "Id",
                keyValue: new Guid("b2f65678-402a-11ee-be56-0242ac120002"),
                column: "ObsoletedAt",
                value: new DateTime(2023, 9, 15, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.InsertData(
                schema: "tariff",
                table: "PaidUserServices",
                columns: new[] { "Id", "ChargeTypeId", "CreatedAt", "Description", "DurationInDays", "Name", "ObsoletedAt", "ServiceBusName" },
                values: new object[] { new Guid("554a350c-5313-11ee-be56-0242ac120002"), 70, new DateTime(2023, 9, 15, 0, 0, 0, 0, DateTimeKind.Unspecified), "Check credit score paid service", 14, "Check credit score paid service", null, "check_credit_score_paid_service" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                schema: "tariff",
                table: "PaidUserServices",
                keyColumn: "Id",
                keyValue: new Guid("554a350c-5313-11ee-be56-0242ac120002"));

            migrationBuilder.DropColumn(
                name: "ObsoletedAt",
                schema: "tariff",
                table: "PaidUserServices");
        }
    }
}
