﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace BNine.Persistence.Migrations
{
    public partial class add_groups_logging_column_to_users : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(
@"BEGIN TRANSACTION

    -- Users
    ALTER TABLE [user].[Users] SET (SYSTEM_VERSIONING = OFF)
    ALTER TABLE [user].[Users] ADD [Groups] nvarchar(max) NULL;
    ALTER TABLE [history].[Users] ADD [Groups] nvarchar(max) NULL;
    ALTER TABLE [user].[Users] ALTER COLUMN [SysStartTime] DROP HIDDEN;
    ALTER TABLE [user].[Users] SET (SYSTEM_VERSIONING = ON (HISTORY_TABLE = [history].Users, DATA_CONSISTENCY_CHECK = ON))

COMMIT TRANSACTION
");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(
@"BEGIN TRANSACTION

    -- Users
    ALTER TABLE [user].[Users] SET (SYSTEM_VERSIONING = OFF)
    ALTER TABLE [user].[Users] DROP COLUMN [Groups];
    ALTER TABLE [history].[Users] DROP COLUMN [Groups];
    ALTER TABLE [user].[Users] ALTER COLUMN [SysStartTime] ADD HIDDEN;
    ALTER TABLE [user].[Users] SET (SYSTEM_VERSIONING = ON (HISTORY_TABLE = [history].Users, DATA_CONSISTENCY_CHECK = ON))    

COMMIT TRANSACTION");
        }
    }
}
