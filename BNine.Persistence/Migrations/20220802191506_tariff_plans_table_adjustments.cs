﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace BNine.Persistence.Migrations
{
    public partial class tariff_plans_table_adjustments : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Discount",
                schema: "tariff",
                table: "TariffPlans");

            migrationBuilder.DropColumn(
                name: "TotalPrice",
                schema: "tariff",
                table: "TariffPlans");

            migrationBuilder.RenameColumn(
                name: "Period",
                schema: "tariff",
                table: "TariffPlans",
                newName: "MonthsDuration");

            migrationBuilder.RenameColumn(
                name: "IsBaseTariff",
                schema: "tariff",
                table: "TariffPlans",
                newName: "IsParent");

            migrationBuilder.RenameColumn(
                name: "Group",
                schema: "tariff",
                table: "TariffPlans",
                newName: "GroupIds");

            migrationBuilder.AddColumn<decimal>(
                name: "DiscountPercentage",
                schema: "tariff",
                table: "TariffPlans",
                type: "decimal(18,2)",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.UpdateData(
                schema: "tariff",
                table: "TariffPlans",
                keyColumn: "Id",
                keyValue: new Guid("6c2b90bc-7703-ed11-b47a-000d3a31d12d"),
                columns: new[] { "DiscountPercentage", "Features" },
                values: new object[] { 20m, "[\r\n                  {\r\n                    \"Text\": \"Everything in the \\nB9 Advance Plan—plus:\"\r\n                  },\r\n                  {\r\n                    \"Text\": \"Advance up to 100 % \\nof your Paycheck\",\r\n                    \"AdditionalInfo\": \"B9 Advance℠ and B9 Premium℠ are optional services, offered by B9 to its members that require to set up a payroll direct deposits to the B9 Account each month.All B9 Advance℠ and B9 Premium℠ members start with up to $100 early pay advance.B9 Premium℠ members have a potential for it to increase up to 100 % of their earnings they deposit from their paycheck to their B9 Account.All B9 members are informed of their current available maxes in the B9 mobile app.Their limit may change at any time, at B9's discretion.\"\r\n                  },\r\n                  {\r\n                    \"Text\": \"Credit Report\",\r\n                    \"additionalInfo\": \"Credit Report is not a product of Evolve Bank & Trust, and offered by third party provider and affiliate of B9.\"\r\n                  },\r\n                  {\r\n                    \"Text\": \"Credit Score\",\r\n                    \"additionalInfo\": \"Credit Score is not a product of Evolve Bank & Trust, and offered by third party provider and affiliate of B9.\"\r\n                  },\r\n                  {\r\n                    \"Text\": \"Credit Score Simulator\",\r\n                    \"additionalInfo\": \"Credit Score Simulator is not a product of Evolve Bank & Trust, and offered by third party provider and affiliate of B9.\"\r\n                  }\r\n                ]" });

            migrationBuilder.UpdateData(
                schema: "tariff",
                table: "TariffPlans",
                keyColumn: "Id",
                keyValue: new Guid("eac3a65f-7703-ed11-b47a-000d3a31d12d"),
                columns: new[] { "DiscountPercentage", "Features" },
                values: new object[] { 10m, "[\r\n                  {\r\n                    \"Text\": \"Everything in the \\nB9 Advance Plan—plus:\"\r\n                  },\r\n                  {\r\n                    \"Text\": \"Advance up to 100 % \\nof your Paycheck\",\r\n                    \"AdditionalInfo\": \"B9 Advance℠ and B9 Premium℠ are optional services, offered by B9 to its members that require to set up a payroll direct deposits to the B9 Account each month.All B9 Advance℠ and B9 Premium℠ members start with up to $100 early pay advance.B9 Premium℠ members have a potential for it to increase up to 100 % of their earnings they deposit from their paycheck to their B9 Account.All B9 members are informed of their current available maxes in the B9 mobile app.Their limit may change at any time, at B9's discretion.\"\r\n                  },\r\n                  {\r\n                    \"Text\": \"Credit Report\",\r\n                    \"additionalInfo\": \"Credit Report is not a product of Evolve Bank & Trust, and offered by third party provider and affiliate of B9.\"\r\n                  },\r\n                  {\r\n                    \"Text\": \"Credit Score\",\r\n                    \"additionalInfo\": \"Credit Score is not a product of Evolve Bank & Trust, and offered by third party provider and affiliate of B9.\"\r\n                  },\r\n                  {\r\n                    \"Text\": \"Credit Score Simulator\",\r\n                    \"additionalInfo\": \"Credit Score Simulator is not a product of Evolve Bank & Trust, and offered by third party provider and affiliate of B9.\"\r\n                  }\r\n                ]" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "DiscountPercentage",
                schema: "tariff",
                table: "TariffPlans");

            migrationBuilder.RenameColumn(
                name: "MonthsDuration",
                schema: "tariff",
                table: "TariffPlans",
                newName: "Period");

            migrationBuilder.RenameColumn(
                name: "IsParent",
                schema: "tariff",
                table: "TariffPlans",
                newName: "IsBaseTariff");

            migrationBuilder.RenameColumn(
                name: "GroupIds",
                schema: "tariff",
                table: "TariffPlans",
                newName: "Group");

            migrationBuilder.AddColumn<int>(
                name: "Discount",
                schema: "tariff",
                table: "TariffPlans",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<decimal>(
                name: "TotalPrice",
                schema: "tariff",
                table: "TariffPlans",
                type: "decimal(18,6)",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.UpdateData(
                schema: "tariff",
                table: "TariffPlans",
                keyColumn: "Id",
                keyValue: new Guid("6c2b90bc-7703-ed11-b47a-000d3a31d12d"),
                columns: new[] { "Discount", "Features", "TotalPrice" },
                values: new object[] { 20, null, 49.99m });

            migrationBuilder.UpdateData(
                schema: "tariff",
                table: "TariffPlans",
                keyColumn: "Id",
                keyValue: new Guid("b6edf67d-5fdd-4246-b47a-259b4c4e49be"),
                column: "TotalPrice",
                value: 19.99m);

            migrationBuilder.UpdateData(
                schema: "tariff",
                table: "TariffPlans",
                keyColumn: "Id",
                keyValue: new Guid("b9491ab9-601b-406b-a689-a9b311773b73"),
                column: "TotalPrice",
                value: 9.99m);

            migrationBuilder.UpdateData(
                schema: "tariff",
                table: "TariffPlans",
                keyColumn: "Id",
                keyValue: new Guid("eac3a65f-7703-ed11-b47a-000d3a31d12d"),
                columns: new[] { "Discount", "Features", "TotalPrice" },
                values: new object[] { 10, null, 54.99m });
        }
    }
}
