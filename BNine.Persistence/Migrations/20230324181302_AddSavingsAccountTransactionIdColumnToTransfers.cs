﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace BNine.Persistence.Migrations
{
    public partial class AddSavingsAccountTransactionIdColumnToTransfers : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "CreditorSavingsAccountTransactionId",
                schema: "bank",
                table: "ACHCreditTransfers",
                type: "int",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "CreditorSavingsAccountTransactionId",
                schema: "bank",
                table: "ACHCreditTransfers");
        }
    }
}
