﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace BNine.Persistence.Migrations
{
    /// <summary>
    /// https://stackoverflow.com/questions/69769871/issue-with-ef-core-5-hasdefaultvalue-ef-pushes-default-value-when-value-is-set
    /// </summary>
    public partial class debit_card_change_type_enum_values : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql("update bank.DebitCards " +
                                 "set Type = 2 where Type = 0");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql("update bank.DebitCards " +
                                 "set Type = 0 where Type = 2");
        }
    }
}
