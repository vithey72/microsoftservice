﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace BNine.Persistence.Migrations
{
    public partial class add_favorite_transfers_text_id_and_indexes : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "CreditorIdentifier",
                schema: "dbo",
                table: "FavoriteUserTransfers",
                type: "nvarchar(450)",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_FavoriteUserTransfers_CreditorIdentifier",
                schema: "dbo",
                table: "FavoriteUserTransfers",
                column: "CreditorIdentifier");

            migrationBuilder.CreateIndex(
                name: "IX_FavoriteUserTransfers_TransferId",
                schema: "dbo",
                table: "FavoriteUserTransfers",
                column: "TransferId",
                unique: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_FavoriteUserTransfers_CreditorIdentifier",
                schema: "dbo",
                table: "FavoriteUserTransfers");

            migrationBuilder.DropColumn(
                name: "CreditorIdentifier",
                schema: "dbo",
                table: "FavoriteUserTransfers");

            migrationBuilder.DropIndex(
                name: "IX_FavoriteUserTransfers_TransferId",
                schema: "dbo",
                table: "FavoriteUserTransfers");
        }
    }
}
