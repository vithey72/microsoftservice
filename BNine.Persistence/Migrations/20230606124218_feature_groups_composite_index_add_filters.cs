﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace BNine.Persistence.Migrations
{
    public partial class feature_groups_composite_index_add_filters : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_FeatureGroups_GroupId_FeatureId",
                schema: "features",
                table: "FeatureGroups");

            migrationBuilder.CreateIndex(
                name: "IX_FeatureGroups_GroupId_FeatureId",
                schema: "features",
                table: "FeatureGroups",
                columns: new[] { "GroupId", "FeatureId" },
                unique: true,
                filter: "[MaxAccountAgeHours] IS NULL\r\n                    AND [MinAccountAgeHours] IS NULL\r\n                    AND [MinRegistrationDate] IS NULL\r\n                    AND [MinAppVersion] IS NULL\r\n                    AND [MobilePlatform] IS NULL");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_FeatureGroups_GroupId_FeatureId",
                schema: "features",
                table: "FeatureGroups");

            migrationBuilder.CreateIndex(
                name: "IX_FeatureGroups_GroupId_FeatureId",
                schema: "features",
                table: "FeatureGroups",
                columns: new[] { "GroupId", "FeatureId" },
                filter: "[MaxAccountAgeHours] IS NULL AND [MinAccountAgeHours] IS NULL AND [MinRegistrationDate] IS NULL");
        }
    }
}
