﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace BNine.Persistence.Migrations
{
    public partial class AddNewFieldInTariff : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "ExternalId",
                schema: "tariff",
                table: "TariffPlans",
                newName: "ExternalClassificationId");

            migrationBuilder.AddColumn<int>(
                name: "ExternalChargeId",
                schema: "tariff",
                table: "TariffPlans",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.UpdateData(
                schema: "tariff",
                table: "TariffPlans",
                keyColumn: "Id",
                keyValue: new Guid("b6edf67d-5fdd-4246-b47a-259b4c4e49be"),
                columns: new[] { "CreatedAt", "ExternalChargeId" },
                values: new object[] { new DateTime(2022, 4, 6, 13, 42, 2, 423, DateTimeKind.Local).AddTicks(9887), 2 });

            migrationBuilder.UpdateData(
                schema: "tariff",
                table: "TariffPlans",
                keyColumn: "Id",
                keyValue: new Guid("b9491ab9-601b-406b-a689-a9b311773b73"),
                columns: new[] { "CreatedAt", "ExternalChargeId" },
                values: new object[] { new DateTime(2022, 4, 6, 13, 42, 2, 423, DateTimeKind.Local).AddTicks(9872), 1 });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ExternalChargeId",
                schema: "tariff",
                table: "TariffPlans");

            migrationBuilder.RenameColumn(
                name: "ExternalClassificationId",
                schema: "tariff",
                table: "TariffPlans",
                newName: "ExternalId");

            migrationBuilder.UpdateData(
                schema: "tariff",
                table: "TariffPlans",
                keyColumn: "Id",
                keyValue: new Guid("b6edf67d-5fdd-4246-b47a-259b4c4e49be"),
                column: "CreatedAt",
                value: new DateTime(2022, 3, 30, 18, 38, 15, 243, DateTimeKind.Local).AddTicks(7050));

            migrationBuilder.UpdateData(
                schema: "tariff",
                table: "TariffPlans",
                keyColumn: "Id",
                keyValue: new Guid("b9491ab9-601b-406b-a689-a9b311773b73"),
                column: "CreatedAt",
                value: new DateTime(2022, 3, 30, 18, 38, 15, 243, DateTimeKind.Local).AddTicks(7033));
        }
    }
}
