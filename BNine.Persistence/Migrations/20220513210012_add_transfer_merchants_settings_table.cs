﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace BNine.Persistence.Migrations
{
    public partial class add_transfer_merchants_settings_table : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "TransferMerchantsSettings",
                schema: "settings",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false, defaultValueSql: "newsequentialid()"),
                    MerchantName = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    MerchantDescriptionKeywords = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    CreatedAt = table.Column<DateTime>(type: "datetime2", nullable: false),
                    UpdatedAt = table.Column<DateTime>(type: "datetime2", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TransferMerchantsSettings", x => x.Id);
                });

            migrationBuilder.CreateIndex(
                name: "IX_TransferMerchantsSettings_MerchantName",
                schema: "settings",
                table: "TransferMerchantsSettings",
                column: "MerchantName",
                unique: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "TransferMerchantsSettings",
                schema: "settings");
        }
    }
}
