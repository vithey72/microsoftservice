﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace BNine.Persistence.Migrations
{
    public partial class default_boostExpressFeeChargeTypeId_setting : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<int>(
                name: "BoostExpressFeeChargeTypeId",
                schema: "settings",
                table: "LoanSettings",
                type: "int",
                nullable: false,
                defaultValue: 68,
                oldClrType: typeof(int),
                oldType: "int");
            migrationBuilder.Sql(
                @"update settings.LoanSettings set BoostExpressFeeChargeTypeId = 68");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<int>(
                name: "BoostExpressFeeChargeTypeId",
                schema: "settings",
                table: "LoanSettings",
                type: "int",
                nullable: false,
                oldClrType: typeof(int),
                oldType: "int",
                oldDefaultValue: 68);
        }
    }
}
