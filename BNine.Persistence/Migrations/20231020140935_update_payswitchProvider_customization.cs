﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace BNine.Persistence.Migrations
{
    public partial class update_payswitchProvider_customization : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "PaycheckSwitchProvider",
                schema: "user",
                table: "EarlySalarySettings");

            migrationBuilder.AddColumn<int>(
                name: "PaycheckSwitchProvider",
                schema: "user",
                table: "UserSettings",
                type: "int",
                nullable: false,
                defaultValue: 1);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "PaycheckSwitchProvider",
                schema: "user",
                table: "UserSettings");

            migrationBuilder.AddColumn<int>(
                name: "PaycheckSwitchProvider",
                schema: "user",
                table: "EarlySalarySettings",
                type: "int",
                nullable: false,
                defaultValue: 1);
        }
    }
}
