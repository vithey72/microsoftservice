﻿using System;
using BNine.Persistence.Configurations.Extensions;
using BNine.Persistence.Constants;
using Microsoft.EntityFrameworkCore.Migrations;

namespace BNine.Persistence.Migrations
{
    public partial class Initial : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.EnsureSchema(
                name: "bank");

            migrationBuilder.EnsureSchema(
                name: "user");

            migrationBuilder.EnsureSchema(
                name: "raw_data");

            migrationBuilder.EnsureSchema(
                name: "translates");

            migrationBuilder.EnsureSchema(
                name: "history");

            migrationBuilder.CreateTable(
                name: "AnonymousAppsflyerIds",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false, defaultValueSql: "newsequentialid()"),
                    Phone = table.Column<string>(maxLength: 16, nullable: false),
                    CreatedAt = table.Column<DateTime>(nullable: false, defaultValueSql: "getutcdate()")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AnonymousAppsflyerIds", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "DocumentTypes",
                columns: table => new
                {
                    Key = table.Column<string>(maxLength: 150, nullable: false),
                    Order = table.Column<int>(nullable: false),
                    ExternalName = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DocumentTypes", x => x.Key);
                });

            migrationBuilder.CreateTable(
                name: "FaqTopics",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_FaqTopics", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "FrequenciesOfPayments",
                columns: table => new
                {
                    Key = table.Column<string>(maxLength: 150, nullable: false),
                    Order = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_FrequenciesOfPayments", x => x.Key);
                });

            migrationBuilder.CreateTable(
                name: "OTPs",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false, defaultValueSql: "newsequentialid()"),
                    Value = table.Column<string>(nullable: false),
                    IsValid = table.Column<bool>(nullable: false),
                    Date = table.Column<DateTime>(nullable: false),
                    PhoneNumber = table.Column<string>(maxLength: 512, nullable: false),
                    Action = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_OTPs", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "PayMethods",
                columns: table => new
                {
                    Key = table.Column<string>(maxLength: 150, nullable: false),
                    Order = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PayMethods", x => x.Key);
                });

            migrationBuilder.CreateTable(
                name: "SourcesOfIncome",
                columns: table => new
                {
                    Key = table.Column<string>(maxLength: 150, nullable: false),
                    Order = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SourcesOfIncome", x => x.Key);
                });

            migrationBuilder.CreateTable(
                name: "UpdateUserInfoRequests",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    UserId = table.Column<Guid>(nullable: false),
                    UpdatedFieldType = table.Column<int>(nullable: false),
                    UpdatedFieldValue = table.Column<string>(nullable: true),
                    RequestDate = table.Column<DateTime>(nullable: false),
                    ConfirmationCode = table.Column<string>(nullable: true),
                    Status = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UpdateUserInfoRequests", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Users",
                schema: "user",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false, defaultValueSql: "newsequentialid()"),
                    CreatedAt = table.Column<DateTime>(nullable: false, defaultValueSql: "getutcdate()"),
                    Phone = table.Column<string>(maxLength: 16, nullable: false),
                    Email = table.Column<string>(maxLength: 512, nullable: false),
                    Password = table.Column<string>(nullable: false),
                    DateOfBirth = table.Column<DateTime>(type: "date", nullable: true),
                    FirstName = table.Column<string>(maxLength: 512, nullable: true),
                    LastName = table.Column<string>(maxLength: 512, nullable: true),
                    Status = table.Column<int>(nullable: false),
                    ProfilePhotoUrl = table.Column<string>(nullable: true),
                    AppsflyerId = table.Column<Guid>(nullable: false),
                    ExternalId = table.Column<int>(nullable: false),
                    ExternalClientId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Users", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "DocumentTypesValues",
                schema: "translates",
                columns: table => new
                {
                    DocumentTypeKey = table.Column<string>(nullable: false),
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Language = table.Column<int>(nullable: false),
                    Text = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DocumentTypesValues", x => new { x.DocumentTypeKey, x.Id });
                    table.ForeignKey(
                        name: "FK_DocumentTypesValues_DocumentTypes_DocumentTypeKey",
                        column: x => x.DocumentTypeKey,
                        principalTable: "DocumentTypes",
                        principalColumn: "Key",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "FaqItems",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    FaqTopicId = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_FaqItems", x => x.Id);
                    table.ForeignKey(
                        name: "FK_FaqItems_FaqTopics_FaqTopicId",
                        column: x => x.FaqTopicId,
                        principalTable: "FaqTopics",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "FaqTopicsTopics",
                schema: "translates",
                columns: table => new
                {
                    FaqTopicId = table.Column<Guid>(nullable: false),
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Language = table.Column<int>(nullable: false),
                    Text = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_FaqTopicsTopics", x => new { x.FaqTopicId, x.Id });
                    table.ForeignKey(
                        name: "FK_FaqTopicsTopics_FaqTopics_FaqTopicId",
                        column: x => x.FaqTopicId,
                        principalTable: "FaqTopics",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "FrequenciesOfPaymentsValues",
                schema: "translates",
                columns: table => new
                {
                    FrequencyOfPaymentsKey = table.Column<string>(nullable: false),
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Language = table.Column<int>(nullable: false),
                    Text = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_FrequenciesOfPaymentsValues", x => new { x.FrequencyOfPaymentsKey, x.Id });
                    table.ForeignKey(
                        name: "FK_FrequenciesOfPaymentsValues_FrequenciesOfPayments_FrequencyOfPaymentsKey",
                        column: x => x.FrequencyOfPaymentsKey,
                        principalTable: "FrequenciesOfPayments",
                        principalColumn: "Key",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "PayMethodsValues",
                schema: "translates",
                columns: table => new
                {
                    PayMethodKey = table.Column<string>(nullable: false),
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Language = table.Column<int>(nullable: false),
                    Text = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PayMethodsValues", x => new { x.PayMethodKey, x.Id });
                    table.ForeignKey(
                        name: "FK_PayMethodsValues_PayMethods_PayMethodKey",
                        column: x => x.PayMethodKey,
                        principalTable: "PayMethods",
                        principalColumn: "Key",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "SourcesOfIncomeValues",
                schema: "translates",
                columns: table => new
                {
                    SourceOfIncomeKey = table.Column<string>(nullable: false),
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Language = table.Column<int>(nullable: false),
                    Text = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SourcesOfIncomeValues", x => new { x.SourceOfIncomeKey, x.Id });
                    table.ForeignKey(
                        name: "FK_SourcesOfIncomeValues_SourcesOfIncome_SourceOfIncomeKey",
                        column: x => x.SourceOfIncomeKey,
                        principalTable: "SourcesOfIncome",
                        principalColumn: "Key",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "BonusCodes",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false, defaultValueSql: "newsequentialid()"),
                    Code = table.Column<string>(maxLength: 6, nullable: false),
                    CreationDate = table.Column<DateTime>(nullable: false),
                    SenderId = table.Column<Guid>(nullable: false),
                    ReceiverId = table.Column<Guid>(nullable: true),
                    IsAlreadyUsed = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BonusCodes", x => x.Id);
                    table.ForeignKey(
                        name: "FK_BonusCodes_Users_ReceiverId",
                        column: x => x.ReceiverId,
                        principalSchema: "user",
                        principalTable: "Users",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_BonusCodes_Users_SenderId",
                        column: x => x.SenderId,
                        principalSchema: "user",
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Devices",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false, defaultValueSql: "newsequentialid()"),
                    ExternalDeviceId = table.Column<string>(nullable: false),
                    RegistrationId = table.Column<string>(nullable: true),
                    Type = table.Column<int>(nullable: false),
                    UserId = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Devices", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Devices_Users_UserId",
                        column: x => x.UserId,
                        principalSchema: "user",
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "EarlySalaryWidgetConfigurations",
                columns: table => new
                {
                    UserId = table.Column<Guid>(nullable: false),
                    WidgetUserId = table.Column<Guid>(nullable: false),
                    Token = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_EarlySalaryWidgetConfigurations", x => x.UserId);
                    table.ForeignKey(
                        name: "FK_EarlySalaryWidgetConfigurations_Users_UserId",
                        column: x => x.UserId,
                        principalSchema: "user",
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "ExternalBankAccounts",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false, defaultValueSql: "newsequentialid()"),
                    BindingDate = table.Column<DateTime>(nullable: false),
                    AccountNumber = table.Column<string>(nullable: false),
                    RoutingNumber = table.Column<string>(nullable: false),
                    UserId = table.Column<Guid>(nullable: false),
                    IsDeleted = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ExternalBankAccounts", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ExternalBankAccounts_Users_UserId",
                        column: x => x.UserId,
                        principalSchema: "user",
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "KYCVerificationResults",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false, defaultValueSql: "newsequentialid()"),
                    EntityToken = table.Column<string>(maxLength: 512, nullable: false),
                    EvaluationToken = table.Column<string>(maxLength: 512, nullable: false),
                    Status = table.Column<string>(maxLength: 150, nullable: false),
                    CreatedAt = table.Column<DateTime>(nullable: false, defaultValueSql: "getutcdate()"),
                    RawEvaluation = table.Column<string>(nullable: false),
                    UserId = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_KYCVerificationResults", x => x.Id);
                    table.ForeignKey(
                        name: "FK_KYCVerificationResults_Users_UserId",
                        column: x => x.UserId,
                        principalSchema: "user",
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Notifications",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false, defaultValueSql: "newsequentialid()"),
                    CreatedAt = table.Column<DateTime>(nullable: false),
                    Text = table.Column<string>(nullable: false),
                    IsRead = table.Column<bool>(nullable: false),
                    UserId = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Notifications", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Notifications_Users_UserId",
                        column: x => x.UserId,
                        principalSchema: "user",
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "CurrentAccount",
                schema: "bank",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false, defaultValueSql: "newsequentialid()"),
                    CreatedAt = table.Column<DateTime>(nullable: false, defaultValueSql: "getutcdate()"),
                    ExternalId = table.Column<int>(nullable: false),
                    ProductId = table.Column<int>(nullable: false),
                    UserId = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CurrentAccount", x => x.Id);
                    table.ForeignKey(
                        name: "FK_CurrentAccount_Users_UserId",
                        column: x => x.UserId,
                        principalSchema: "user",
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "DebitCards",
                schema: "bank",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false, defaultValueSql: "newsequentialid()"),
                    ExternalId = table.Column<int>(nullable: false),
                    Status = table.Column<int>(nullable: false),
                    ShippingStatus = table.Column<int>(nullable: false),
                    UserId = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DebitCards", x => x.Id);
                    table.ForeignKey(
                        name: "FK_DebitCards_Users_UserId",
                        column: x => x.UserId,
                        principalSchema: "user",
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "LoanAccounts",
                schema: "bank",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false, defaultValueSql: "newsequentialid()"),
                    ExternalId = table.Column<int>(nullable: false),
                    ProductId = table.Column<int>(nullable: false),
                    Status = table.Column<int>(nullable: false),
                    UserId = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_LoanAccounts", x => x.Id);
                    table.ForeignKey(
                        name: "FK_LoanAccounts_Users_UserId",
                        column: x => x.UserId,
                        principalSchema: "user",
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Addresses",
                schema: "user",
                columns: table => new
                {
                    UserId = table.Column<Guid>(nullable: false),
                    ZipCode = table.Column<string>(maxLength: 12, nullable: false),
                    City = table.Column<string>(nullable: false),
                    State = table.Column<string>(maxLength: 2, nullable: false),
                    AddressLine = table.Column<string>(nullable: false),
                    Unit = table.Column<string>(nullable: true),
                    ExternalId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Addresses", x => x.UserId);
                    table.ForeignKey(
                        name: "FK_Addresses_Users_UserId",
                        column: x => x.UserId,
                        principalSchema: "user",
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Documents",
                schema: "user",
                columns: table => new
                {
                    UserId = table.Column<Guid>(nullable: false),
                    VouchedJobId = table.Column<string>(nullable: true),
                    Number = table.Column<string>(nullable: true),
                    IssuingState = table.Column<string>(nullable: true),
                    IssuingCountry = table.Column<string>(nullable: true),
                    TypeKey = table.Column<string>(nullable: true),
                    ExternalId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Documents", x => x.UserId);
                    table.ForeignKey(
                        name: "FK_Documents_DocumentTypes_TypeKey",
                        column: x => x.TypeKey,
                        principalTable: "DocumentTypes",
                        principalColumn: "Key");
                    table.ForeignKey(
                        name: "FK_Documents_Users_UserId",
                        column: x => x.UserId,
                        principalSchema: "user",
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "EarlySalarySettings",
                schema: "user",
                columns: table => new
                {
                    UserId = table.Column<Guid>(nullable: false),
                    IsEarlySalaryEnabled = table.Column<bool>(nullable: false),
                    EarlySalarySubscriptionDate = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_EarlySalarySettings", x => x.UserId);
                    table.ForeignKey(
                        name: "FK_EarlySalarySettings_Users_UserId",
                        column: x => x.UserId,
                        principalSchema: "user",
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Identities",
                schema: "user",
                columns: table => new
                {
                    UserId = table.Column<Guid>(nullable: false),
                    Value = table.Column<string>(maxLength: 9, nullable: true),
                    Type = table.Column<int>(nullable: false),
                    ExternalId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Identities", x => x.UserId);
                    table.ForeignKey(
                        name: "FK_Identities_Users_UserId",
                        column: x => x.UserId,
                        principalSchema: "user",
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "ToSAcceptances",
                schema: "user",
                columns: table => new
                {
                    UserId = table.Column<Guid>(nullable: false),
                    Time = table.Column<DateTime>(nullable: false),
                    Ip = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ToSAcceptances", x => x.UserId);
                    table.ForeignKey(
                        name: "FK_ToSAcceptances_Users_UserId",
                        column: x => x.UserId,
                        principalSchema: "user",
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "UserSettings",
                schema: "user",
                columns: table => new
                {
                    UserId = table.Column<Guid>(nullable: false),
                    IsReceivedBonusBlockEnabled = table.Column<bool>(nullable: false),
                    IsResetPasswordAllowed = table.Column<bool>(nullable: false),
                    IsNotificationsEnabled = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UserSettings", x => x.UserId);
                    table.ForeignKey(
                        name: "FK_UserSettings_Users_UserId",
                        column: x => x.UserId,
                        principalSchema: "user",
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "FaqItemsAnswers",
                schema: "translates",
                columns: table => new
                {
                    FaqItemId = table.Column<Guid>(nullable: false),
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Language = table.Column<int>(nullable: false),
                    Text = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_FaqItemsAnswers", x => new { x.FaqItemId, x.Id });
                    table.ForeignKey(
                        name: "FK_FaqItemsAnswers_FaqItems_FaqItemId",
                        column: x => x.FaqItemId,
                        principalTable: "FaqItems",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "FaqItemsQuestions",
                schema: "translates",
                columns: table => new
                {
                    FaqItemId = table.Column<Guid>(nullable: false),
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Language = table.Column<int>(nullable: false),
                    Text = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_FaqItemsQuestions", x => new { x.FaqItemId, x.Id });
                    table.ForeignKey(
                        name: "FK_FaqItemsQuestions_FaqItems_FaqItemId",
                        column: x => x.FaqItemId,
                        principalTable: "FaqItems",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Plaid",
                schema: "raw_data",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false, defaultValueSql: "newsequentialid()"),
                    CreatedAt = table.Column<DateTime>(nullable: false, defaultValueSql: "getutcdate()"),
                    UpdatedAt = table.Column<DateTime>(nullable: false, defaultValueSql: "getutcdate()"),
                    BankAccountId = table.Column<Guid>(nullable: false),
                    Transactions = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Plaid", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Plaid_ExternalBankAccounts_BankAccountId",
                        column: x => x.BankAccountId,
                        principalTable: "ExternalBankAccounts",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_BonusCodes_ReceiverId",
                table: "BonusCodes",
                column: "ReceiverId",
                unique: true,
                filter: "[ReceiverId] IS NOT NULL");

            migrationBuilder.CreateIndex(
                name: "IX_BonusCodes_SenderId",
                table: "BonusCodes",
                column: "SenderId");

            migrationBuilder.CreateIndex(
                name: "IX_Devices_UserId",
                table: "Devices",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_ExternalBankAccounts_UserId",
                table: "ExternalBankAccounts",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_FaqItems_FaqTopicId",
                table: "FaqItems",
                column: "FaqTopicId");

            migrationBuilder.CreateIndex(
                name: "IX_KYCVerificationResults_UserId",
                table: "KYCVerificationResults",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_Notifications_UserId",
                table: "Notifications",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_CurrentAccount_UserId",
                schema: "bank",
                table: "CurrentAccount",
                column: "UserId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_DebitCards_UserId",
                schema: "bank",
                table: "DebitCards",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_LoanAccounts_UserId",
                schema: "bank",
                table: "LoanAccounts",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_Plaid_BankAccountId",
                schema: "raw_data",
                table: "Plaid",
                column: "BankAccountId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Documents_TypeKey",
                schema: "user",
                table: "Documents",
                column: "TypeKey");

            migrationBuilder.AddTemporalTableSupport("Users", "user", MigrationDefaults.VersioningSchema);
            migrationBuilder.AddTemporalTableSupport("Addresses", "user", MigrationDefaults.VersioningSchema);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "AnonymousAppsflyerIds");

            migrationBuilder.DropTable(
                name: "BonusCodes");

            migrationBuilder.DropTable(
                name: "Devices");

            migrationBuilder.DropTable(
                name: "EarlySalaryWidgetConfigurations");

            migrationBuilder.DropTable(
                name: "KYCVerificationResults");

            migrationBuilder.DropTable(
                name: "Notifications");

            migrationBuilder.DropTable(
                name: "OTPs");

            migrationBuilder.DropTable(
                name: "UpdateUserInfoRequests");

            migrationBuilder.DropTable(
                name: "CurrentAccount",
                schema: "bank");

            migrationBuilder.DropTable(
                name: "DebitCards",
                schema: "bank");

            migrationBuilder.DropTable(
                name: "LoanAccounts",
                schema: "bank");

            migrationBuilder.DropTable(
                name: "Plaid",
                schema: "raw_data");

            migrationBuilder.DropTable(
                name: "DocumentTypesValues",
                schema: "translates");

            migrationBuilder.DropTable(
                name: "FaqItemsAnswers",
                schema: "translates");

            migrationBuilder.DropTable(
                name: "FaqItemsQuestions",
                schema: "translates");

            migrationBuilder.DropTable(
                name: "FaqTopicsTopics",
                schema: "translates");

            migrationBuilder.DropTable(
                name: "FrequenciesOfPaymentsValues",
                schema: "translates");

            migrationBuilder.DropTable(
                name: "PayMethodsValues",
                schema: "translates");

            migrationBuilder.DropTable(
                name: "SourcesOfIncomeValues",
                schema: "translates");

            migrationBuilder.DropTable(
                name: "Addresses",
                schema: "user");

            migrationBuilder.DropTable(
                name: "Documents",
                schema: "user");

            migrationBuilder.DropTable(
                name: "EarlySalarySettings",
                schema: "user");

            migrationBuilder.DropTable(
                name: "Identities",
                schema: "user");

            migrationBuilder.DropTable(
                name: "ToSAcceptances",
                schema: "user");

            migrationBuilder.DropTable(
                name: "UserSettings",
                schema: "user");

            migrationBuilder.DropTable(
                name: "ExternalBankAccounts");

            migrationBuilder.DropTable(
                name: "FaqItems");

            migrationBuilder.DropTable(
                name: "FrequenciesOfPayments");

            migrationBuilder.DropTable(
                name: "PayMethods");

            migrationBuilder.DropTable(
                name: "SourcesOfIncome");

            migrationBuilder.DropTable(
                name: "DocumentTypes");

            migrationBuilder.DropTable(
                name: "Users",
                schema: "user");

            migrationBuilder.DropTable(
                name: "FaqTopics");
        }
    }
}
