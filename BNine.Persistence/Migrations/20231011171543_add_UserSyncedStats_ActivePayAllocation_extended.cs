﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace BNine.Persistence.Migrations
{
    public partial class add_UserSyncedStats_ActivePayAllocation_extended : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "ActiveExternalPayAllocationCount",
                schema: "user",
                table: "UserSyncedStats",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<string>(
                name: "ActiveInternalPayAllocationIds",
                schema: "user",
                table: "UserSyncedStats",
                type: "nvarchar(max)",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ActiveExternalPayAllocationCount",
                schema: "user",
                table: "UserSyncedStats");

            migrationBuilder.DropColumn(
                name: "ActiveInternalPayAllocationIds",
                schema: "user",
                table: "UserSyncedStats");
        }
    }
}
