﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace BNine.Persistence.Migrations
{
    public partial class AddHintColumnsToTransactionCategoryTable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "HintButtonText",
                table: "TransactionCategories",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "HintIconUrl",
                table: "TransactionCategories",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "HintSubTitle",
                table: "TransactionCategories",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "HintTitle",
                table: "TransactionCategories",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.UpdateData(
                table: "TransactionCategories",
                keyColumn: "Key",
                keyValue: "atm",
                columns: new[] { "HintButtonText", "HintIconUrl", "HintSubTitle", "HintTitle" },
                values: new object[] { "GOT IT", "https://b9prodaccount.blob.core.windows.net/static-documents-container/TransactionAnalyticsIcons/atmHintIcon.png", "All ATM withdrawals will be displayed in this category.", "ATM" });

            migrationBuilder.UpdateData(
                table: "TransactionCategories",
                keyColumn: "Key",
                keyValue: "card-transactions",
                columns: new[] { "HintButtonText", "HintIconUrl", "HintSubTitle", "HintTitle" },
                values: new object[] { "GOT IT", "https://b9prodaccount.blob.core.windows.net/static-documents-container/TransactionAnalyticsIcons/cardTransactionsHintIcon.png", "If you pay for something with a B9 debit card, all those transactions will be displayed here and categorized for your convenience.", "Card Transactions" });

            migrationBuilder.UpdateData(
                table: "TransactionCategories",
                keyColumn: "Key",
                keyValue: "transfers",
                columns: new[] { "HintButtonText", "HintIconUrl", "HintSubTitle", "HintTitle" },
                values: new object[] { "GOT IT", "https://b9prodaccount.blob.core.windows.net/static-documents-container/TransactionAnalyticsIcons/transafersHintIcon.png", "Here displays all outgoing transfers from your B9 Account (ACH transfers, transfers to external cards, internal transfers to other B9 members).", "Transfers" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "HintButtonText",
                table: "TransactionCategories");

            migrationBuilder.DropColumn(
                name: "HintIconUrl",
                table: "TransactionCategories");

            migrationBuilder.DropColumn(
                name: "HintSubTitle",
                table: "TransactionCategories");

            migrationBuilder.DropColumn(
                name: "HintTitle",
                table: "TransactionCategories");
        }
    }
}
