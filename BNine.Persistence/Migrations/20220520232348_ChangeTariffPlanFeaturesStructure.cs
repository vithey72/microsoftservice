﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace BNine.Persistence.Migrations
{
    public partial class ChangeTariffPlanFeaturesStructure : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<string>(
                name: "Deeplink",
                table: "Notifications",
                type: "nvarchar(max)",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "int",
                oldNullable: true);

            migrationBuilder.UpdateData(
                schema: "tariff",
                table: "TariffPlans",
                keyColumn: "Id",
                keyValue: new Guid("b6edf67d-5fdd-4246-b47a-259b4c4e49be"),
                column: "Features",
                value: "[\r\n                  {\r\n                    \"Text\": \"Everything in the \\nB9 Advance Plan—plus:\"\r\n                  },\r\n                  {\r\n                    \"Text\": \"Advance up to 100 % \\nof your Paycheck\",\r\n                    \"AdditionalInfo\": \"B9 Advance℠ and B9 Premium℠ are optional services, offered by B9 to its members that require to set up a payroll direct deposits to the B9 Account each month.All B9 Advance℠ and B9 Premium℠ members start with up to $100 early pay advance.B9 Premium℠ members have a potential for it to increase up to 100 % of their earnings they deposit from their paycheck to their B9 Account.All B9 members are informed of their current available maxes in the B9 mobile app.Their limit may change at any time, at B9's discretion.\"\r\n                  },\r\n                  {\r\n                    \"Text\": \"Credit Report\"\r\n                  },\r\n                  {\r\n                    \"Text\": \"Credit Score\"\r\n                  },\r\n                  {\r\n                    \"Text\": \"Credit Score Simulator\"\r\n                  }\r\n                ]");

            migrationBuilder.UpdateData(
                schema: "tariff",
                table: "TariffPlans",
                keyColumn: "Id",
                keyValue: new Guid("b9491ab9-601b-406b-a689-a9b311773b73"),
                column: "Features",
                value: "[\r\n                  {\r\n                    \"Text\": \"B9 Visa® Debit Card with \\n4 % Cashback\"\r\n                  },\r\n                  {\r\n                    \"Text\": \"Advance up to $300\"\r\n                  },\r\n                  {\r\n                    \"Text\": \"Get advances instantly\"\r\n                  },\r\n                  {\r\n                    \"Text\": \"Instant transfers to B9 Members\"\r\n                  }\r\n                ]");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<int>(
                name: "Deeplink",
                table: "Notifications",
                type: "int",
                nullable: true,
                oldClrType: typeof(string),
                oldType: "nvarchar(max)",
                oldNullable: true);

            migrationBuilder.UpdateData(
                schema: "tariff",
                table: "TariffPlans",
                keyColumn: "Id",
                keyValue: new Guid("b6edf67d-5fdd-4246-b47a-259b4c4e49be"),
                column: "Features",
                value: "[\r\n                    \"Everything in the \\nB9 Advance Plan—plus:\",\r\n                    \"Advance up to 100% \\nof your Paycheck\",\r\n                    \"Credit Report\",\r\n                    \"Credit Score\",\r\n                    \"Credit Score Simulator\"\r\n                ]");

            migrationBuilder.UpdateData(
                schema: "tariff",
                table: "TariffPlans",
                keyColumn: "Id",
                keyValue: new Guid("b9491ab9-601b-406b-a689-a9b311773b73"),
                column: "Features",
                value: "[\r\n                    \"B9 Visa® Debit Card with \\n4% Cashback\",\r\n                    \"Advance up to $300\",\r\n                    \"Get advances instantly\",\r\n                    \"Instant transfers to B9 Members\"\r\n                ]");
        }
    }
}
