﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace BNine.Persistence.Migrations
{
    public partial class RenameSavingsAccountTransferIdColumn : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "CreditorSavingsAccountTransactionId",
                schema: "bank",
                table: "ACHCreditTransfers",
                newName: "SavingsAccountTransferExternalId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "SavingsAccountTransferExternalId",
                schema: "bank",
                table: "ACHCreditTransfers",
                newName: "CreditorSavingsAccountTransactionId");
        }
    }
}
