﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace BNine.Persistence.Migrations
{
    public partial class add_user_blocked_reason : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData(
                schema: "user",
                table: "UserBlockReasons",
                columns: new[] { "Id", "Order", "Value" },
                values: new object[] { new Guid("c2b0b2a0-5b0a-4b0e-8b0a-5b0a4b0e8b0a"), 4, "User action" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                schema: "user",
                table: "UserBlockReasons",
                keyColumn: "Id",
                keyValue: new Guid("c2b0b2a0-5b0a-4b0e-8b0a-5b0a4b0e8b0a"));
        }
    }
}
