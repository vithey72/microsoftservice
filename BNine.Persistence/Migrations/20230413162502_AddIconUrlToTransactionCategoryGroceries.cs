﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace BNine.Persistence.Migrations
{
    public partial class AddIconUrlToTransactionCategoryGroceries : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "TransactionCategories",
                keyColumn: "Key",
                keyValue: "supermarkets-online-stores",
                column: "IconUrl",
                value: "https://b9devaccount.blob.core.windows.net/static-documents-container/TransactionAnalyticsIcons/CardTransactionIcon.png");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "TransactionCategories",
                keyColumn: "Key",
                keyValue: "supermarkets-online-stores",
                column: "IconUrl",
                value: null);
        }
    }
}
