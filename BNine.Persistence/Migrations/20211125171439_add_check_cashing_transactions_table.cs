﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace BNine.Persistence.Migrations
{
    public partial class add_check_cashing_transactions_table : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "CheckCashingTransactions",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false, defaultValueSql: "newsequentialid()"),
                    UserId = table.Column<Guid>(nullable: false),
                    AlltrustTransactionId = table.Column<string>(nullable: true),
                    AmountRequested = table.Column<int>(nullable: false),
                    AmountTotal = table.Column<int>(nullable: false),
                    Status = table.Column<int>(nullable: false),
                    StatusDate = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CheckCashingTransactions", x => x.Id);
                    table.ForeignKey(
                        name: "FK_CheckCashingTransactions_Users_UserId",
                        column: x => x.UserId,
                        principalSchema: "user",
                        principalTable: "Users",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateIndex(
                name: "IX_CheckCashingTransactions_AlltrustTransactionId",
                table: "CheckCashingTransactions",
                column: "AlltrustTransactionId",
                unique: true,
                filter: "[AlltrustTransactionId] IS NOT NULL");

            migrationBuilder.CreateIndex(
                name: "IX_CheckCashingTransactions_Status",
                table: "CheckCashingTransactions",
                column: "Status");

            migrationBuilder.CreateIndex(
                name: "IX_CheckCashingTransactions_StatusDate",
                table: "CheckCashingTransactions",
                column: "StatusDate");

            migrationBuilder.CreateIndex(
                name: "IX_CheckCashingTransactions_UserId",
                table: "CheckCashingTransactions",
                column: "UserId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "CheckCashingTransactions");
        }
    }
}
