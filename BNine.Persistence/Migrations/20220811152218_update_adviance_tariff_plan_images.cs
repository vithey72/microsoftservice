﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace BNine.Persistence.Migrations
{
    public partial class update_adviance_tariff_plan_images : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                schema: "tariff",
                table: "TariffPlans",
                keyColumn: "Id",
                keyValue: new Guid("b9491ab9-601b-406b-a689-a9b311773b73"),
                columns: new[] { "AdvertisementPictureUrl", "PictureUrl" },
                values: new object[] { "https://b9devaccount.blob.core.windows.net/static-documents-container/TariffPlans/B9_Advance_Plan_1m.png", "https://b9devaccount.blob.core.windows.net/static-documents-container/TariffPlans/B9_Advance_Plan_1m.png" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                schema: "tariff",
                table: "TariffPlans",
                keyColumn: "Id",
                keyValue: new Guid("b9491ab9-601b-406b-a689-a9b311773b73"),
                columns: new[] { "AdvertisementPictureUrl", "PictureUrl" },
                values: new object[] { "https://b9devaccount.blob.core.windows.net/static-documents-container/TariffPlans/B9_Advance_Plan-20220330-141043.png", "https://b9devaccount.blob.core.windows.net/static-documents-container/TariffPlans/B9_Advance_Plan-20220330-141043.png" });
        }
    }
}
