﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace BNine.Persistence.Migrations
{
    public partial class store_cashback_values_in_used_cashbacks_table : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<decimal>(
                name: "EarnedAmount",
                table: "UsedCashbacks",
                type: "decimal(18,2)",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<decimal>(
                name: "SpentAmount",
                table: "UsedCashbacks",
                type: "decimal(18,2)",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<DateTime>(
                name: "EarnedAmountUpdatedAt",
                table: "UsedCashbacks",
                type: "datetime2",
                nullable: false,
                defaultValueSql: "GETDATE()");

            migrationBuilder.CreateTable(
                name: "CashbackStatistics",
                schema: "user",
                columns: table => new
                {
                    UserId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    TotalEarnedAmount = table.Column<decimal>(type: "decimal(18,2)", nullable: false, defaultValue: 0m),
                    EarnedAmountUpdatedAt = table.Column<DateTime>(type: "datetime2", nullable: false, defaultValueSql: "GETDATE()")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CashbackStatistics", x => x.UserId);
                    table.ForeignKey(
                        name: "FK_CashbackStatistics_Users_UserId",
                        column: x => x.UserId,
                        principalSchema: "user",
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "CashbackStatistics",
                schema: "user");

            migrationBuilder.DropColumn(
                name: "EarnedAmount",
                table: "UsedCashbacks");

            migrationBuilder.DropColumn(
                name: "EarnedAmountUpdatedAt",
                table: "UsedCashbacks");

            migrationBuilder.DropColumn(
                name: "SpentAmount",
                table: "UsedCashbacks");
        }
    }
}
