﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace BNine.Persistence.Migrations
{
    public partial class add_new_paid_service_advance_boost : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "OfferId",
                schema: "user",
                table: "AdvanceSyncedStats",
                type: "nvarchar(100)",
                maxLength: 100,
                nullable: true);

            migrationBuilder.InsertData(
                schema: "tariff",
                table: "PaidUserServices",
                columns: new[] { "Id", "ChargeTypeId", "CreatedAt", "Description", "DurationInDays", "Name", "ServiceBusName" },
                values: new object[] { new Guid("252c84cb-a679-4aa5-8c24-e34f550fda3c"), 365, new DateTime(2022, 11, 24, 0, 0, 0, 0, DateTimeKind.Unspecified), "Increase your one time Advance amount", 14, "Advance Boost fee 9.99", "advance_boost" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                schema: "tariff",
                table: "PaidUserServices",
                keyColumn: "Id",
                keyValue: new Guid("252c84cb-a679-4aa5-8c24-e34f550fda3c"));

            migrationBuilder.DropColumn(
                name: "OfferId",
                schema: "user",
                table: "AdvanceSyncedStats");
        }
    }
}
