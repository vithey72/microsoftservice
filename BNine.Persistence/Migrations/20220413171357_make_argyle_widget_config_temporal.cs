﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace BNine.Persistence.Migrations
{
    using Configurations.Extensions;
    using Constants;

    public partial class make_argyle_widget_config_temporal : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddVersioningToExistedTable("EarlySalaryWidgetConfigurations", "dbo", MigrationDefaults.VersioningSchema);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {

        }
    }
}
