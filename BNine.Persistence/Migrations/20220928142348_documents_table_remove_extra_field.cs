﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace BNine.Persistence.Migrations
{
    public partial class documents_table_remove_extra_field : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "SocureBackImageId",
                schema: "user",
                table: "Documents");

            migrationBuilder.RenameColumn(
                name: "SocureFrontImageId",
                schema: "user",
                table: "Documents",
                newName: "DocumentScansExternalId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "DocumentScansExternalId",
                schema: "user",
                table: "Documents",
                newName: "SocureFrontImageId");

            migrationBuilder.AddColumn<Guid>(
                name: "SocureBackImageId",
                schema: "user",
                table: "Documents",
                type: "uniqueidentifier",
                nullable: true);
        }
    }
}
