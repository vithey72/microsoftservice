﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace BNine.Persistence.Migrations
{
    public partial class mergeClothingSubcategoriesIntoSingleCategory : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "TransactionCategories",
                keyColumn: "Key",
                keyValue: "family-clothing-stores");

            migrationBuilder.DeleteData(
                table: "TransactionCategories",
                keyColumn: "Key",
                keyValue: "women-clothing");

            migrationBuilder.UpdateData(
                table: "TransactionCategories",
                keyColumn: "Key",
                keyValue: "clothing-stores",
                column: "MccCodesJsonArrayString",
                value: "[5691,5621,5651]");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "TransactionCategories",
                keyColumn: "Key",
                keyValue: "clothing-stores",
                column: "MccCodesJsonArrayString",
                value: "[5691]");

            migrationBuilder.InsertData(
                table: "TransactionCategories",
                columns: new[] { "Key", "Color", "IconUrl", "MccCodesJsonArrayString", "ParentId", "PaymentTypeIdsJsonArrayString", "Title" },
                values: new object[] { "family-clothing-stores", null, null, "[5651]", "card-transactions", null, "Family Clothing Stores" });

            migrationBuilder.InsertData(
                table: "TransactionCategories",
                columns: new[] { "Key", "Color", "IconUrl", "MccCodesJsonArrayString", "ParentId", "PaymentTypeIdsJsonArrayString", "Title" },
                values: new object[] { "women-clothing", null, null, "[5621]", "card-transactions", null, "Women's Clothing" });
        }
    }
}
