﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace BNine.Persistence.Migrations
{
    public partial class add_card_products_table : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "CardProducts",
                schema: "bank",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false, defaultValueSql: "newsequentialid()"),
                    ExternalId = table.Column<int>(type: "int", nullable: false),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    AssociatedType = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CardProducts", x => x.Id);
                });

            migrationBuilder.InsertData(
                schema: "bank",
                table: "CardProducts",
                columns: new[] { "Id", "AssociatedType", "ExternalId", "Name" },
                values: new object[] { new Guid("18adcdbc-0d44-43ff-a2e0-73e0b408e771"), 1, 1, "Physical Debit Card (w/ Digital First)" });

            migrationBuilder.InsertData(
                schema: "bank",
                table: "CardProducts",
                columns: new[] { "Id", "AssociatedType", "ExternalId", "Name" },
                values: new object[] { new Guid("89de0b18-3e69-4f05-bbf8-f5d21e874b2b"), 2, 4, "Virtual Only Debit Card" });

            migrationBuilder.InsertData(
                schema: "bank",
                table: "CardProducts",
                columns: new[] { "Id", "AssociatedType", "ExternalId", "Name" },
                values: new object[] { new Guid("bff92466-f3ca-4cf7-9ffc-169c99c3f6a7"), 3, 5, "Secure Credit Card" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "CardProducts",
                schema: "bank");
        }
    }
}
