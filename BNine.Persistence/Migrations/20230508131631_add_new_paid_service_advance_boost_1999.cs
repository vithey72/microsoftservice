﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace BNine.Persistence.Migrations
{
    public partial class add_new_paid_service_advance_boost_1999 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData(
                schema: "tariff",
                table: "PaidUserServices",
                columns: new[]
                {
                    "Id", "ChargeTypeId", "CreatedAt", "Description", "DurationInDays", "Name", "ServiceBusName"
                },
                values: new object[]
                {
                    new Guid("3d3712c6-604f-43f2-a3f9-f5a55800adb4"), 859,
                    new DateTime(2023, 5, 8, 0, 0, 0, 0, DateTimeKind.Unspecified),
                    "Increase your one time Advance amount", 14, "Advance Boost fee 19.99", "advance_boost_3"
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                schema: "tariff",
                table: "PaidUserServices",
                keyColumn: "Id",
                keyValue: new Guid("3d3712c6-604f-43f2-a3f9-f5a55800adb4"));
        }
    }
}
