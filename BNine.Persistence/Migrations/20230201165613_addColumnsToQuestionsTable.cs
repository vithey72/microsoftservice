﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace BNine.Persistence.Migrations
{
    public partial class addColumnsToQuestionsTable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "AltButtonText",
                schema: "questionnaire",
                table: "Questions",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Header",
                schema: "questionnaire",
                table: "Questions",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "MainButtonText",
                schema: "questionnaire",
                table: "Questions",
                type: "nvarchar(max)",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "AltButtonText",
                schema: "questionnaire",
                table: "Questions");

            migrationBuilder.DropColumn(
                name: "Header",
                schema: "questionnaire",
                table: "Questions");

            migrationBuilder.DropColumn(
                name: "MainButtonText",
                schema: "questionnaire",
                table: "Questions");
        }
    }
}
