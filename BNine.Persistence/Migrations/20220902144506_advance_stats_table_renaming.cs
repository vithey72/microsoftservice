﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace BNine.Persistence.Migrations
{
    public partial class advance_stats_table_renaming : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "AdvanceExtensionStatistics",
                schema: "user");

            migrationBuilder.CreateTable(
                name: "AdvanceSyncedStats",
                schema: "user",
                columns: table => new
                {
                    UserId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    AvailableLimit = table.Column<int>(type: "int", nullable: false),
                    ExtensionPrice = table.Column<int>(type: "int", nullable: false),
                    ExtensionLimit = table.Column<int>(type: "int", nullable: false),
                    ExtensionExpiresAt = table.Column<DateTime>(type: "datetime2", nullable: false),
                    Rating = table.Column<decimal>(type: "decimal(18,2)", nullable: false),
                    Slider1 = table.Column<decimal>(type: "decimal(18,2)", nullable: false),
                    Slider2 = table.Column<decimal>(type: "decimal(18,2)", nullable: false),
                    Slider3 = table.Column<decimal>(type: "decimal(18,2)", nullable: false),
                    Slider4 = table.Column<decimal>(type: "decimal(18,2)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AdvanceSyncedStats", x => x.UserId);
                    table.ForeignKey(
                        name: "FK_AdvanceSyncedStats_Users_UserId",
                        column: x => x.UserId,
                        principalSchema: "user",
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_AdvanceSyncedStats_ExtensionExpiresAt",
                schema: "user",
                table: "AdvanceSyncedStats",
                column: "ExtensionExpiresAt");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "AdvanceSyncedStats",
                schema: "user");

            migrationBuilder.CreateTable(
                name: "AdvanceExtensionStatistics",
                schema: "user",
                columns: table => new
                {
                    UserId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    AvailableLimit = table.Column<int>(type: "int", nullable: false),
                    ExtensionExpiresAt = table.Column<DateTime>(type: "datetime2", nullable: false),
                    ExtensionLimit = table.Column<int>(type: "int", nullable: false),
                    ExtensionPrice = table.Column<int>(type: "int", nullable: false),
                    Rating = table.Column<decimal>(type: "decimal(18,2)", nullable: false),
                    Slider1 = table.Column<decimal>(type: "decimal(18,2)", nullable: false),
                    Slider2 = table.Column<decimal>(type: "decimal(18,2)", nullable: false),
                    Slider3 = table.Column<decimal>(type: "decimal(18,2)", nullable: false),
                    Slider4 = table.Column<decimal>(type: "decimal(18,2)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AdvanceExtensionStatistics", x => x.UserId);
                    table.ForeignKey(
                        name: "FK_AdvanceExtensionStatistics_Users_UserId",
                        column: x => x.UserId,
                        principalSchema: "user",
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_AdvanceExtensionStatistics_ExtensionExpiresAt",
                schema: "user",
                table: "AdvanceExtensionStatistics",
                column: "ExtensionExpiresAt");
        }
    }
}
