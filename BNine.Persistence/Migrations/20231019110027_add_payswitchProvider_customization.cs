﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace BNine.Persistence.Migrations
{
    public partial class add_payswitchProvider_customization : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<DateTime>(
                name: "TokenValidTo",
                table: "EarlySalaryWidgetConfigurations",
                type: "datetime2",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "PaycheckSwitchProvider",
                schema: "user",
                table: "EarlySalarySettings",
                type: "int",
                nullable: false,
                defaultValue: 1);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "TokenValidTo",
                table: "EarlySalaryWidgetConfigurations");

            migrationBuilder.DropColumn(
                name: "PaycheckSwitchProvider",
                schema: "user",
                table: "EarlySalarySettings");
        }
    }
}
