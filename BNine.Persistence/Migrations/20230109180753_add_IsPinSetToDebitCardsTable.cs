﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace BNine.Persistence.Migrations
{
    public partial class add_IsPinSetToDebitCardsTable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "IsPinSet",
                schema: "bank",
                table: "DebitCards",
                type: "bit",
                nullable: false,
                defaultValue: false);

            // Set IsPinSet to true for active physical cards
            migrationBuilder.Sql(@"
                    UPDATE [bank].[DebitCards]
                    SET [bank].[DebitCards].[IsPinSet] = 1
                    WHERE [bank].[DebitCards].[Status] = 3 and [bank].[DebitCards].[Type] = 1");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "IsPinSet",
                schema: "bank",
                table: "DebitCards");
        }
    }
}
