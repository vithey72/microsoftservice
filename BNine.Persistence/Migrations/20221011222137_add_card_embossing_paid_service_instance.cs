﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace BNine.Persistence.Migrations
{
    public partial class add_card_embossing_paid_service_instance : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData(
                schema: "tariff",
                table: "PaidUserServices",
                columns: new[] { "Id", "ChargeTypeId", "CreatedAt", "Description", "DurationInDays", "Name", "ServiceBusName" },
                values: new object[] { new Guid("20af6f52-54aa-4f20-aad5-c314153e4106"), 336, new DateTime(2022, 10, 11, 0, 0, 0, 0, DateTimeKind.Unspecified), "One time payment for initial creation and shipping of a physical card", 640000, "Physical card embossing fee", "card_embossing" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                schema: "tariff",
                table: "PaidUserServices",
                keyColumn: "Id",
                keyValue: new Guid("20af6f52-54aa-4f20-aad5-c314153e4106"));
        }
    }
}
