﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace BNine.Persistence.Migrations
{
    public partial class add_questionnaire_responses_dates : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "DeteledAt",
                schema: "questionnaire",
                table: "QuestionResponses",
                newName: "DeletedAt");

            migrationBuilder.AddColumn<DateTime>(
                name: "CreatedAt",
                schema: "questionnaire",
                table: "SubmittedQuestionResponses",
                type: "datetime2",
                nullable: false,
                defaultValueSql: "getutcdate()");

            migrationBuilder.AddColumn<DateTime>(
                name: "CreatedAt",
                schema: "questionnaire",
                table: "SubmittedQuestionResponseQuestionResponses",
                type: "datetime2",
                nullable: false,
                defaultValueSql: "getutcdate()");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "CreatedAt",
                schema: "questionnaire",
                table: "SubmittedQuestionResponses");

            migrationBuilder.DropColumn(
                name: "CreatedAt",
                schema: "questionnaire",
                table: "SubmittedQuestionResponseQuestionResponses");

            migrationBuilder.RenameColumn(
                name: "DeletedAt",
                schema: "questionnaire",
                table: "QuestionResponses",
                newName: "DeteledAt");
        }
    }
}
