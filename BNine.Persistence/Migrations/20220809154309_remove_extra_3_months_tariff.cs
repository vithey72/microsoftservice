﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace BNine.Persistence.Migrations
{
    public partial class remove_extra_3_months_tariff : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                schema: "tariff",
                table: "TariffPlans",
                keyColumn: "Id",
                keyValue: new Guid("6c2b90bc-7703-ed11-b47a-000d3a31d12d"));

            migrationBuilder.UpdateData(
                schema: "tariff",
                table: "TariffPlans",
                keyColumn: "Id",
                keyValue: new Guid("eac3a65f-7703-ed11-b47a-000d3a31d12d"),
                columns: new[] { "MonthlyFee", "TotalPrice" },
                values: new object[] { 17.99m, 53.97m });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                schema: "tariff",
                table: "TariffPlans",
                keyColumn: "Id",
                keyValue: new Guid("eac3a65f-7703-ed11-b47a-000d3a31d12d"),
                columns: new[] { "MonthlyFee", "TotalPrice" },
                values: new object[] { 18.33m, 54.99m });

            migrationBuilder.InsertData(
                schema: "tariff",
                table: "TariffPlans",
                columns: new[] { "Id", "AdvertisementPictureUrl", "CreatedAt", "DiscountPercentage", "ExternalChargeId", "ExternalClassificationId", "Features", "GroupIds", "IconUrl", "IsParent", "MonthlyFee", "MonthsDuration", "Name", "PictureUrl", "PreDiscountPrice", "TotalPrice", "Type", "WelcomeScreenPictureUrl" },
                values: new object[] { new Guid("6c2b90bc-7703-ed11-b47a-000d3a31d12d"), "https://b9devaccount.blob.core.windows.net/static-documents-container/TariffPlans/B9_Premium_3m_20disc_ad.png", new DateTime(2022, 4, 20, 0, 0, 0, 0, DateTimeKind.Unspecified), 20m, 4, 758, "[\r\n                  {\r\n                    \"Text\": \"Everything in the \\nB9 Advance Plan—plus:\"\r\n                  },\r\n                  {\r\n                    \"Text\": \"Advance up to 100 % \\nof your Paycheck\",\r\n                    \"AdditionalInfo\": \"B9 Advance℠ and B9 Premium℠ are optional services, offered by B9 to its members that require to set up a payroll direct deposits to the B9 Account each month.All B9 Advance℠ and B9 Premium℠ members start with up to $100 early pay advance.B9 Premium℠ members have a potential for it to increase up to 100 % of their earnings they deposit from their paycheck to their B9 Account.All B9 members are informed of their current available maxes in the B9 mobile app.Their limit may change at any time, at B9's discretion.\"\r\n                  },\r\n                  {\r\n                    \"Text\": \"Credit Report\",\r\n                    \"additionalInfo\": \"Credit Report is not a product of Evolve Bank & Trust, and offered by third party provider and affiliate of B9.\"\r\n                  },\r\n                  {\r\n                    \"Text\": \"Credit Score\",\r\n                    \"additionalInfo\": \"Credit Score is not a product of Evolve Bank & Trust, and offered by third party provider and affiliate of B9.\"\r\n                  },\r\n                  {\r\n                    \"Text\": \"Credit Score Simulator\",\r\n                    \"additionalInfo\": \"Credit Score Simulator is not a product of Evolve Bank & Trust, and offered by third party provider and affiliate of B9.\"\r\n                  }\r\n                ]", null, "https://b9devaccount.blob.core.windows.net/static-documents-container/TariffPlans/B9_Premium_icon.png", false, 16.66m, 3, "B9 Premium 3 months", "https://b9devaccount.blob.core.windows.net/static-documents-container/TariffPlans/B9_Premium_3m_20disc.png", 59.97m, 49.99m, 2, "https://b9devaccount.blob.core.windows.net/static-documents-container/TariffPlans/Welcome_B9_Premium.png" });
        }
    }
}
