﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace BNine.Persistence.Migrations
{
    public partial class add_technical_date_columns : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ClientKey",
                schema: "user",
                table: "ArrayCustomerSettings");

            migrationBuilder.AddColumn<DateTime>(
                name: "CreatedAt",
                schema: "user",
                table: "ArrayCustomerSettings",
                type: "datetime2",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "UpdatedAt",
                schema: "user",
                table: "ArrayCustomerSettings",
                type: "datetime2",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "CreatedAt",
                schema: "user",
                table: "AlltrustCustomerSettings",
                type: "datetime2",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "UpdatedAt",
                schema: "user",
                table: "AlltrustCustomerSettings",
                type: "datetime2",
                nullable: true);

            migrationBuilder.Sql("update [user].AlltrustCustomerSettings set CreatedAt = getdate(), UpdatedAt = getdate();");
            migrationBuilder.Sql("update [user].ArrayCustomerSettings set CreatedAt = COALESCE(ActivatedAt, getdate()), UpdatedAt = getdate();");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "CreatedAt",
                schema: "user",
                table: "ArrayCustomerSettings");

            migrationBuilder.DropColumn(
                name: "UpdatedAt",
                schema: "user",
                table: "ArrayCustomerSettings");

            migrationBuilder.DropColumn(
                name: "CreatedAt",
                schema: "user",
                table: "AlltrustCustomerSettings");

            migrationBuilder.DropColumn(
                name: "UpdatedAt",
                schema: "user",
                table: "AlltrustCustomerSettings");

            migrationBuilder.AddColumn<Guid>(
                name: "ClientKey",
                schema: "user",
                table: "ArrayCustomerSettings",
                type: "uniqueidentifier",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.Sql("update [user].ArrayCustomerSettings set ClientKey = ArrayUserId;");
        }
    }
}
