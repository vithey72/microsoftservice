﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace BNine.Persistence.Migrations
{
    public partial class add_alltrust_customer_settings : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "AlltrustCustomerSettings",
                schema: "user",
                columns: table => new
                {
                    UserId = table.Column<Guid>(nullable: false),
                    AlltrustCustomerId = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AlltrustCustomerSettings", x => x.UserId);
                    table.ForeignKey(
                        name: "FK_AlltrustCustomerSettings_Users_UserId",
                        column: x => x.UserId,
                        principalSchema: "user",
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_AlltrustCustomerSettings_AlltrustCustomerId",
                schema: "user",
                table: "AlltrustCustomerSettings",
                column: "AlltrustCustomerId",
                unique: true,
                filter: "[AlltrustCustomerId] IS NOT NULL");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "AlltrustCustomerSettings",
                schema: "user");
        }
    }
}
