﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace BNine.Persistence.Migrations
{
    public partial class tariff_plans_table_adjustments_4 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                schema: "tariff",
                table: "TariffPlans",
                keyColumn: "Id",
                keyValue: new Guid("b6edf67d-5fdd-4246-b47a-259b4c4e49be"),
                column: "AdvertisementPictureUrl",
                value: "https://b9devaccount.blob.core.windows.net/static-documents-container/TariffPlans/B9_Premium_Plan-20220330-141043.png");

            migrationBuilder.UpdateData(
                schema: "tariff",
                table: "TariffPlans",
                keyColumn: "Id",
                keyValue: new Guid("b9491ab9-601b-406b-a689-a9b311773b73"),
                column: "AdvertisementPictureUrl",
                value: "https://b9devaccount.blob.core.windows.net/static-documents-container/TariffPlans/B9_Advance_Plan-20220330-141043.png");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                schema: "tariff",
                table: "TariffPlans",
                keyColumn: "Id",
                keyValue: new Guid("b6edf67d-5fdd-4246-b47a-259b4c4e49be"),
                column: "AdvertisementPictureUrl",
                value: null);

            migrationBuilder.UpdateData(
                schema: "tariff",
                table: "TariffPlans",
                keyColumn: "Id",
                keyValue: new Guid("b9491ab9-601b-406b-a689-a9b311773b73"),
                column: "AdvertisementPictureUrl",
                value: null);
        }
    }
}
