﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace BNine.Persistence.Migrations
{
    public partial class add_merchant_classification_code_icons_table : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "MerchantCategoryCodeIconSettings",
                schema: "settings",
                columns: table => new
                {
                    Code = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Description = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    IconUrl = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MerchantCategoryCodeIconSettings", x => x.Code);
                });

            migrationBuilder.InsertData(
                schema: "settings",
                table: "MerchantCategoryCodeIconSettings",
                columns: new[] { "Code", "Description", "IconUrl" },
                values: new object[,]
                {
                    { 4121, "Taxi", null },
                    { 4814, "Telecommunications", null },
                    { 4829, "Money transfers", null },
                    { 5310, "Sales of a variety of goods at discounted prices", null },
                    { 5399, "Small retail outlets and medium-sized stores", null },
                    { 5411, "Supermarkets", null },
                    { 5499, "Various grocery stores", null },
                    { 5541, "Fueling stations", null },
                    { 5812, "Restaurants", null },
                    { 5814, "Fast food", null },
                    { 5816, "Digital Goods – Games", null },
                    { 5818, "Digital Goods", null },
                    { 5999, "Various stores and specialty retailers", null },
                    { 6011, "ATM", null },
                    { 6012, "ATM", null },
                    { 6051, "Funding of electronic wallets", null },
                    { 7994, "Video game clubs", null }
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "MerchantCategoryCodeIconSettings",
                schema: "settings");
        }
    }
}
