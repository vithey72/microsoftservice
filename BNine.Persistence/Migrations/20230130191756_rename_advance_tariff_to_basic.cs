﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace BNine.Persistence.Migrations
{
    public partial class rename_advance_tariff_to_basic : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                schema: "tariff",
                table: "PaidUserServices",
                keyColumn: "Id",
                keyValue: new Guid("c1d68b39-e503-4b3d-924c-0318ace1bb46"),
                column: "Description",
                value: "Purchase of Basic tariff discounted by 1$");

            migrationBuilder.UpdateData(
                schema: "tariff",
                table: "TariffPlans",
                keyColumn: "Id",
                keyValue: new Guid("b6edf67d-5fdd-4246-b47a-259b4c4e49be"),
                column: "Features",
                value: "[\r\n                  {\r\n                    \"Text\": \"Everything in the \\nB9 Basic Plan—plus:\"\r\n                  },\r\n                  {\r\n                    \"Text\": \"Advance up to $500\",\r\n                    \"AdditionalInfo\": \"B9 Basic℠ and B9 Premium℠ are optional services, offered by B9 to its members that require to set up a payroll direct deposits to the B9 Account each month. All B9 Basic℠ and B9 Premium℠ members start with up to $100 early pay advance. B9 Premium℠ members have a potential for it to increase up to $500 early pay advance. All B9 members are informed of their current available maxes in the B9 mobile app.Their limit may change at any time, at B9's discretion.\"\r\n                  },\r\n                  {\r\n                    \"Text\": \"Credit Report\",\r\n                    \"additionalInfo\": \"Credit Report is not a product of Evolve Bank & Trust, and offered by third party provider and affiliate of B9.\"\r\n                  },\r\n                  {\r\n                    \"Text\": \"Credit Score\",\r\n                    \"additionalInfo\": \"Credit Score is not a product of Evolve Bank & Trust, and offered by third party provider and affiliate of B9.\"\r\n                  },\r\n                  {\r\n                    \"Text\": \"Credit Score Simulator\",\r\n                    \"additionalInfo\": \"Credit Score Simulator is not a product of Evolve Bank & Trust, and offered by third party provider and affiliate of B9.\"\r\n                  }\r\n                ]");

            migrationBuilder.UpdateData(
                schema: "tariff",
                table: "TariffPlans",
                keyColumn: "Id",
                keyValue: new Guid("b9491ab9-601b-406b-a689-a9b311773b73"),
                column: "Name",
                value: "B9 Basic Plan");

            migrationBuilder.UpdateData(
                schema: "tariff",
                table: "TariffPlans",
                keyColumn: "Id",
                keyValue: new Guid("eac3a65f-7703-ed11-b47a-000d3a31d12d"),
                column: "Features",
                value: "[\r\n                  {\r\n                    \"Text\": \"Everything in the \\nB9 Basic Plan—plus:\"\r\n                  },\r\n                  {\r\n                    \"Text\": \"Advance up to $500\",\r\n                    \"AdditionalInfo\": \"B9 Basic℠ and B9 Premium℠ are optional services, offered by B9 to its members that require to set up a payroll direct deposits to the B9 Account each month. All B9 Basic℠ and B9 Premium℠ members start with up to $100 early pay advance. B9 Premium℠ members have a potential for it to increase up to $500 early pay advance. All B9 members are informed of their current available maxes in the B9 mobile app.Their limit may change at any time, at B9's discretion.\"\r\n                  },\r\n                  {\r\n                    \"Text\": \"Credit Report\",\r\n                    \"additionalInfo\": \"Credit Report is not a product of Evolve Bank & Trust, and offered by third party provider and affiliate of B9.\"\r\n                  },\r\n                  {\r\n                    \"Text\": \"Credit Score\",\r\n                    \"additionalInfo\": \"Credit Score is not a product of Evolve Bank & Trust, and offered by third party provider and affiliate of B9.\"\r\n                  },\r\n                  {\r\n                    \"Text\": \"Credit Score Simulator\",\r\n                    \"additionalInfo\": \"Credit Score Simulator is not a product of Evolve Bank & Trust, and offered by third party provider and affiliate of B9.\"\r\n                  }\r\n                ]");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                schema: "tariff",
                table: "PaidUserServices",
                keyColumn: "Id",
                keyValue: new Guid("c1d68b39-e503-4b3d-924c-0318ace1bb46"),
                column: "Description",
                value: "Purchase of Advance tariff discounted by 1$");

            migrationBuilder.UpdateData(
                schema: "tariff",
                table: "TariffPlans",
                keyColumn: "Id",
                keyValue: new Guid("b6edf67d-5fdd-4246-b47a-259b4c4e49be"),
                column: "Features",
                value: "[\r\n                  {\r\n                    \"Text\": \"Everything in the \\nB9 Advance Plan—plus:\"\r\n                  },\r\n                  {\r\n                    \"Text\": \"Advance up to $500\",\r\n                    \"AdditionalInfo\": \"B9 Basic℠ and B9 Premium℠ are optional services, offered by B9 to its members that require to set up a payroll direct deposits to the B9 Account each month. All B9 Basic℠ and B9 Premium℠ members start with up to $100 early pay advance. B9 Premium℠ members have a potential for it to increase up to $500 early pay advance. All B9 members are informed of their current available maxes in the B9 mobile app.Their limit may change at any time, at B9's discretion.\"\r\n                  },\r\n                  {\r\n                    \"Text\": \"Credit Report\",\r\n                    \"additionalInfo\": \"Credit Report is not a product of Evolve Bank & Trust, and offered by third party provider and affiliate of B9.\"\r\n                  },\r\n                  {\r\n                    \"Text\": \"Credit Score\",\r\n                    \"additionalInfo\": \"Credit Score is not a product of Evolve Bank & Trust, and offered by third party provider and affiliate of B9.\"\r\n                  },\r\n                  {\r\n                    \"Text\": \"Credit Score Simulator\",\r\n                    \"additionalInfo\": \"Credit Score Simulator is not a product of Evolve Bank & Trust, and offered by third party provider and affiliate of B9.\"\r\n                  }\r\n                ]");

            migrationBuilder.UpdateData(
                schema: "tariff",
                table: "TariffPlans",
                keyColumn: "Id",
                keyValue: new Guid("b9491ab9-601b-406b-a689-a9b311773b73"),
                column: "Name",
                value: "B9 Advance Plan");

            migrationBuilder.UpdateData(
                schema: "tariff",
                table: "TariffPlans",
                keyColumn: "Id",
                keyValue: new Guid("eac3a65f-7703-ed11-b47a-000d3a31d12d"),
                column: "Features",
                value: "[\r\n                  {\r\n                    \"Text\": \"Everything in the \\nB9 Advance Plan—plus:\"\r\n                  },\r\n                  {\r\n                    \"Text\": \"Advance up to $500\",\r\n                    \"AdditionalInfo\": \"B9 Basic℠ and B9 Premium℠ are optional services, offered by B9 to its members that require to set up a payroll direct deposits to the B9 Account each month. All B9 Basic℠ and B9 Premium℠ members start with up to $100 early pay advance. B9 Premium℠ members have a potential for it to increase up to $500 early pay advance. All B9 members are informed of their current available maxes in the B9 mobile app.Their limit may change at any time, at B9's discretion.\"\r\n                  },\r\n                  {\r\n                    \"Text\": \"Credit Report\",\r\n                    \"additionalInfo\": \"Credit Report is not a product of Evolve Bank & Trust, and offered by third party provider and affiliate of B9.\"\r\n                  },\r\n                  {\r\n                    \"Text\": \"Credit Score\",\r\n                    \"additionalInfo\": \"Credit Score is not a product of Evolve Bank & Trust, and offered by third party provider and affiliate of B9.\"\r\n                  },\r\n                  {\r\n                    \"Text\": \"Credit Score Simulator\",\r\n                    \"additionalInfo\": \"Credit Score Simulator is not a product of Evolve Bank & Trust, and offered by third party provider and affiliate of B9.\"\r\n                  }\r\n                ]");
        }
    }
}
