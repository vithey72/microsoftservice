﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace BNine.Persistence.Migrations
{
    public partial class users_and_addresses_systime_datatype_fix : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(
@"BEGIN TRANSACTION

	-- Users
	ALTER TABLE [user].Users SET (SYSTEM_VERSIONING = OFF)
	ALTER TABLE [user].Users DROP PERIOD FOR SYSTEM_TIME
	DROP INDEX IF EXISTS IX_Users_SysStartTime ON [user].Users -- is a duplicate of ix_Users and does not need to be re-created
	DROP INDEX ix_Users ON [history].Users
	GO

	ALTER TABLE [user].Users             ALTER COLUMN SysStartTime DATETIME2(6) NOT NULL
	ALTER TABLE [user].Users              ALTER COLUMN SysEndTime   DATETIME2(6) NOT NULL
	ALTER TABLE [history].Users ALTER COLUMN SysStartTime DATETIME2(6) NOT NULL
	ALTER TABLE [history].Users ALTER COLUMN SysEndTime   DATETIME2(6) NOT NULL
	CREATE CLUSTERED INDEX ix_Users ON [history].Users (SysEndTime, SysStartTime)
	GO

	UPDATE [user].Users SET SysEndTime = CONVERT(DATETIME2(6), '9999-12-31 23:59:59.9999999')
	GO

	ALTER TABLE [user].Users ADD PERIOD FOR SYSTEM_TIME (SysStartTime, SysEndTime)
	ALTER TABLE [user].Users SET (SYSTEM_VERSIONING = ON (HISTORY_TABLE = [history].Users, DATA_CONSISTENCY_CHECK = ON))
	GO

	-- Addresses
	ALTER TABLE [user].[Addresses] SET (SYSTEM_VERSIONING = OFF)
    ALTER TABLE [user].[Addresses] DROP PERIOD FOR SYSTEM_TIME
	DROP INDEX IF EXISTS IX_Addresses_SysStartTime ON [user].[Addresses] -- is a duplicate of [ix_Addresses] and does not need to be re-created
    DROP INDEX [ix_Addresses] ON [history].[Addresses]
    GO

    ALTER TABLE [user].[Addresses]               ALTER COLUMN SysStartTime DATETIME2(6) NOT NULL
    ALTER TABLE [user].[Addresses]               ALTER COLUMN SysEndTime   DATETIME2(6) NOT NULL
    ALTER TABLE [history].[Addresses] ALTER COLUMN SysStartTime DATETIME2(6) NOT NULL
    ALTER TABLE [history].[Addresses] ALTER COLUMN SysEndTime   DATETIME2(6) NOT NULL
    CREATE CLUSTERED INDEX [ix_Addresses] ON [history].[Addresses] (SysEndTime, SysStartTime)
    GO

    UPDATE [user].[Addresses] SET SysEndTime = CONVERT(DATETIME2(6), '9999-12-31 23:59:59.9999999')
    GO

    ALTER TABLE [user].[Addresses] ADD PERIOD FOR SYSTEM_TIME (SysStartTime, SysEndTime)
    ALTER TABLE [user].[Addresses] SET (SYSTEM_VERSIONING = ON (HISTORY_TABLE = [history].[Addresses], DATA_CONSISTENCY_CHECK = ON))
    GO

COMMIT TRANSACTION");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(
@"BEGIN TRANSACTION

	-- Users
	ALTER TABLE [user].Users SET (SYSTEM_VERSIONING = OFF)
	ALTER TABLE [user].Users DROP PERIOD FOR SYSTEM_TIME
	DROP INDEX ix_Users ON [history].Users
	GO

	ALTER TABLE [user].Users             ALTER COLUMN SysStartTime DATETIME2(0) NOT NULL
	ALTER TABLE [user].Users              ALTER COLUMN SysEndTime   DATETIME2(0) NOT NULL
	ALTER TABLE [history].Users ALTER COLUMN SysStartTime DATETIME2(0) NOT NULL
	ALTER TABLE [history].Users ALTER COLUMN SysEndTime   DATETIME2(0) NOT NULL
	CREATE CLUSTERED INDEX ix_Users ON [history].Users (SysEndTime, SysStartTime)
	GO

	UPDATE [user].Users SET SysEndTime = CONVERT(DATETIME2(0), '9999-12-31 23:59:59')
	GO

	ALTER TABLE [user].Users ADD PERIOD FOR SYSTEM_TIME (SysStartTime, SysEndTime)
	ALTER TABLE [user].Users SET (SYSTEM_VERSIONING = ON (HISTORY_TABLE = [history].Users, DATA_CONSISTENCY_CHECK = ON))
	GO

	-- Addresses
	ALTER TABLE [user].[Addresses] SET (SYSTEM_VERSIONING = OFF)
    ALTER TABLE [user].[Addresses] DROP PERIOD FOR SYSTEM_TIME
    DROP INDEX [ix_Addresses] ON [history].[Addresses]
    GO

    ALTER TABLE [user].[Addresses]               ALTER COLUMN SysStartTime DATETIME2(0) NOT NULL
    ALTER TABLE [user].[Addresses]               ALTER COLUMN SysEndTime   DATETIME2(0) NOT NULL
    ALTER TABLE [history].[Addresses] ALTER COLUMN SysStartTime DATETIME2(0) NOT NULL
    ALTER TABLE [history].[Addresses] ALTER COLUMN SysEndTime   DATETIME2(0) NOT NULL
    CREATE CLUSTERED INDEX [ix_Addresses] ON [history].[Addresses] (SysEndTime, SysStartTime)
    GO

    UPDATE [user].[Addresses] SET SysEndTime = CONVERT(DATETIME2(0), '9999-12-31 23:59:59')
    GO

    ALTER TABLE [user].[Addresses] ADD PERIOD FOR SYSTEM_TIME (SysStartTime, SysEndTime)
    ALTER TABLE [user].[Addresses] SET (SYSTEM_VERSIONING = ON (HISTORY_TABLE = [history].[Addresses], DATA_CONSISTENCY_CHECK = ON))
    GO

COMMIT TRANSACTION");
        }
    }
}
