﻿using BNine.Persistence.Configurations.Extensions;
using BNine.Persistence.Constants;
using Microsoft.EntityFrameworkCore.Migrations;

namespace BNine.Persistence.Migrations
{
    public partial class add_versioning_for_owned_types : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddVersioningToExistedTable("Documents", "user", MigrationDefaults.VersioningSchema);
            migrationBuilder.AddVersioningToExistedTable("UserSettings", "user", MigrationDefaults.VersioningSchema);
            migrationBuilder.AddVersioningToExistedTable("Identities", "user", MigrationDefaults.VersioningSchema);
            migrationBuilder.AddVersioningToExistedTable("EarlySalarySettings", "user", MigrationDefaults.VersioningSchema);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {

        }
    }
}
