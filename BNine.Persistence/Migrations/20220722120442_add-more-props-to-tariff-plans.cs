﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace BNine.Persistence.Migrations
{
    public partial class addmorepropstotariffplans : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "Discount",
                schema: "tariff",
                table: "TariffPlans",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "Parent",
                schema: "tariff",
                table: "TariffPlans",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "Period",
                schema: "tariff",
                table: "TariffPlans",
                type: "int",
                nullable: false,
                defaultValue: 1);

            migrationBuilder.AddColumn<decimal>(
                name: "TotalPrice",
                schema: "tariff",
                table: "TariffPlans",
                type: "decimal(18,6)",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.UpdateData(
                schema: "tariff",
                table: "TariffPlans",
                keyColumn: "Id",
                keyValue: new Guid("b6edf67d-5fdd-4246-b47a-259b4c4e49be"),
                columns: new[] { "Period", "TotalPrice" },
                values: new object[] { 1, 19.99m });

            migrationBuilder.UpdateData(
                schema: "tariff",
                table: "TariffPlans",
                keyColumn: "Id",
                keyValue: new Guid("b9491ab9-601b-406b-a689-a9b311773b73"),
                columns: new[] { "Period", "TotalPrice" },
                values: new object[] { 1, 9.99m });

            migrationBuilder.InsertData(
                schema: "tariff",
                table: "TariffPlans",
                columns: new[] { "Id", "CreatedAt", "Discount", "ExternalChargeId", "ExternalClassificationId", "Features", "Group", "IconUrl", "MonthlyFee", "Name", "Parent", "Period", "PictureUrl", "TotalPrice", "Type", "WelcomeScreenPictureUrl" },
                values: new object[,]
                {
                    { new Guid("6c2b90bc-7703-ed11-b47a-000d3a31d12d"), new DateTime(2022, 4, 20, 0, 0, 0, 0, DateTimeKind.Unspecified), 20, 4, 758, null, null, "https://b9devaccount.blob.core.windows.net/static-documents-container/TariffPlans/B9_Premium_icon.png", 16.66m, "B9 Premium 3-Month Plan 20% Sale", 2, 3, "https://b9devaccount.blob.core.windows.net/static-documents-container/TariffPlans/B9_Premium_Plan-20220330-141043.png", 49.99m, 4, "https://b9devaccount.blob.core.windows.net/static-documents-container/TariffPlans/Welcome_B9_Premium.png" },
                    { new Guid("eac3a65f-7703-ed11-b47a-000d3a31d12d"), new DateTime(2022, 4, 20, 0, 0, 0, 0, DateTimeKind.Unspecified), 10, 3, 757, null, null, "https://b9devaccount.blob.core.windows.net/static-documents-container/TariffPlans/B9_Premium_icon.png", 18.33m, "B9 Premium 3-Month Plan 10% Sale", 2, 3, "https://b9devaccount.blob.core.windows.net/static-documents-container/TariffPlans/B9_Premium_Plan-20220330-141043.png", 54.99m, 3, "https://b9devaccount.blob.core.windows.net/static-documents-container/TariffPlans/Welcome_B9_Premium.png" }
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                schema: "tariff",
                table: "TariffPlans",
                keyColumn: "Id",
                keyValue: new Guid("6c2b90bc-7703-ed11-b47a-000d3a31d12d"));

            migrationBuilder.DeleteData(
                schema: "tariff",
                table: "TariffPlans",
                keyColumn: "Id",
                keyValue: new Guid("eac3a65f-7703-ed11-b47a-000d3a31d12d"));

            migrationBuilder.DropColumn(
                name: "Discount",
                schema: "tariff",
                table: "TariffPlans");

            migrationBuilder.DropColumn(
                name: "Parent",
                schema: "tariff",
                table: "TariffPlans");

            migrationBuilder.DropColumn(
                name: "Period",
                schema: "tariff",
                table: "TariffPlans");

            migrationBuilder.DropColumn(
                name: "TotalPrice",
                schema: "tariff",
                table: "TariffPlans");
        }
    }
}
