﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace BNine.Persistence.Migrations
{
    public partial class basic_tariff_plan_update_properties : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                schema: "tariff",
                table: "TariffPlans",
                keyColumn: "Id",
                keyValue: new Guid("b6edf67d-5fdd-4246-b47a-259b4c4e49be"),
                column: "Features",
                value: "[\r\n                  {\r\n                    \"Text\": \"Everything in the \\nB9 Basic Plan — plus:\"\r\n                  },\r\n                  {\r\n                    \"Text\": \"Advance up to $500\",\r\n                    \"AdditionalInfo\": \"B9 Basic℠ and B9 Premium℠ are optional services, offered by B9 to its members that require to set up a payroll direct deposits to the B9 Account each month. All B9 Basic℠ and B9 Premium℠ members start with up to $100 early pay advance. B9 Premium℠ members have a potential for it to increase up to $500 early pay advance. All B9 members are informed of their current available maxes in the B9 mobile app.Their limit may change at any time, at B9's discretion.\"\r\n                  },\r\n                  {\r\n                    \"Text\": \"Credit Report\",\r\n                    \"additionalInfo\": \"Credit Report is not a product of Evolve Bank & Trust, and offered by third party provider and affiliate of B9.\"\r\n                  },\r\n                  {\r\n                    \"Text\": \"Credit Score\",\r\n                    \"additionalInfo\": \"Credit Score is not a product of Evolve Bank & Trust, and offered by third party provider and affiliate of B9.\"\r\n                  },\r\n                  {\r\n                    \"Text\": \"Premium Support\",\r\n                    \"additionalInfo\": \"• Dedicated priority answer line \\n• We will answer you in an hour \\n\\nOur Customer Support Team is active the following hours: \\n• Mon-Fri: 5 AM to 9 PM Pacific Standard Time (8 AM to 12 AM Eastern Standard Time) \\n• Sat-Sun: 6 AM to 6 PM Pacific Standard Time (9 AM to 9 PM Eastern Standard Time) \\n\\nTime of response may depend on external factors.\"\r\n                  }\r\n                ]");

            migrationBuilder.UpdateData(
                schema: "tariff",
                table: "TariffPlans",
                keyColumn: "Id",
                keyValue: new Guid("b9491ab9-601b-406b-a689-a9b311773b73"),
                columns: new[] { "AdvertisementPictureUrl", "PictureUrl" },
                values: new object[] { "https://b9prodaccount.blob.core.windows.net/static-documents-container/TariffPlans/B9_Basic_Plan_1m.png", "https://b9prodaccount.blob.core.windows.net/static-documents-container/TariffPlans/B9_Basic_Plan_1m.png" });

            migrationBuilder.UpdateData(
                schema: "tariff",
                table: "TariffPlans",
                keyColumn: "Id",
                keyValue: new Guid("eac3a65f-7703-ed11-b47a-000d3a31d12d"),
                column: "Features",
                value: "[\r\n                  {\r\n                    \"Text\": \"Everything in the \\nB9 Basic Plan — plus:\"\r\n                  },\r\n                  {\r\n                    \"Text\": \"Advance up to $500\",\r\n                    \"AdditionalInfo\": \"B9 Basic℠ and B9 Premium℠ are optional services, offered by B9 to its members that require to set up a payroll direct deposits to the B9 Account each month. All B9 Basic℠ and B9 Premium℠ members start with up to $100 early pay advance. B9 Premium℠ members have a potential for it to increase up to $500 early pay advance. All B9 members are informed of their current available maxes in the B9 mobile app.Their limit may change at any time, at B9's discretion.\"\r\n                  },\r\n                  {\r\n                    \"Text\": \"Credit Report\",\r\n                    \"additionalInfo\": \"Credit Report is not a product of Evolve Bank & Trust, and offered by third party provider and affiliate of B9.\"\r\n                  },\r\n                  {\r\n                    \"Text\": \"Credit Score\",\r\n                    \"additionalInfo\": \"Credit Score is not a product of Evolve Bank & Trust, and offered by third party provider and affiliate of B9.\"\r\n                  },\r\n                  {\r\n                    \"Text\": \"Premium Support\",\r\n                    \"additionalInfo\": \"• Dedicated priority answer line \\n• We will answer you in an hour \\n\\nOur Customer Support Team is active the following hours: \\n• Mon-Fri: 5 AM to 9 PM Pacific Standard Time (8 AM to 12 AM Eastern Standard Time) \\n• Sat-Sun: 6 AM to 6 PM Pacific Standard Time (9 AM to 9 PM Eastern Standard Time) \\n\\nTime of response may depend on external factors.\"\r\n                  }\r\n                ]");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                schema: "tariff",
                table: "TariffPlans",
                keyColumn: "Id",
                keyValue: new Guid("b6edf67d-5fdd-4246-b47a-259b4c4e49be"),
                column: "Features",
                value: "[\r\n                  {\r\n                    \"Text\": \"Everything in the \\nB9 Basic Plan—plus:\"\r\n                  },\r\n                  {\r\n                    \"Text\": \"Advance up to $500\",\r\n                    \"AdditionalInfo\": \"B9 Basic℠ and B9 Premium℠ are optional services, offered by B9 to its members that require to set up a payroll direct deposits to the B9 Account each month. All B9 Basic℠ and B9 Premium℠ members start with up to $100 early pay advance. B9 Premium℠ members have a potential for it to increase up to $500 early pay advance. All B9 members are informed of their current available maxes in the B9 mobile app.Their limit may change at any time, at B9's discretion.\"\r\n                  },\r\n                  {\r\n                    \"Text\": \"Credit Report\",\r\n                    \"additionalInfo\": \"Credit Report is not a product of Evolve Bank & Trust, and offered by third party provider and affiliate of B9.\"\r\n                  },\r\n                  {\r\n                    \"Text\": \"Credit Score\",\r\n                    \"additionalInfo\": \"Credit Score is not a product of Evolve Bank & Trust, and offered by third party provider and affiliate of B9.\"\r\n                  },\r\n                  {\r\n                    \"Text\": \"Premium Support\",\r\n                    \"additionalInfo\": \"• Dedicated priority answer line \\n• We will answer you in an hour \\n\\nOur Customer Support Team is active the following hours: \\n• Mon-Fri: 5 AM to 9 PM Pacific Standard Time (8 AM to 12 AM Eastern Standard Time) \\n• Sat-Sun: 6 AM to 6 PM Pacific Standard Time (9 AM to 9 PM Eastern Standard Time) \\n\\nTime of response may depend on external factors.\"\r\n                  }\r\n                ]");

            migrationBuilder.UpdateData(
                schema: "tariff",
                table: "TariffPlans",
                keyColumn: "Id",
                keyValue: new Guid("b9491ab9-601b-406b-a689-a9b311773b73"),
                columns: new[] { "AdvertisementPictureUrl", "PictureUrl" },
                values: new object[] { "https://b9devaccount.blob.core.windows.net/static-documents-container/TariffPlans/B9_Advance_Plan_1m.png", "https://b9devaccount.blob.core.windows.net/static-documents-container/TariffPlans/B9_Advance_Plan_1m.png" });

            migrationBuilder.UpdateData(
                schema: "tariff",
                table: "TariffPlans",
                keyColumn: "Id",
                keyValue: new Guid("eac3a65f-7703-ed11-b47a-000d3a31d12d"),
                column: "Features",
                value: "[\r\n                  {\r\n                    \"Text\": \"Everything in the \\nB9 Basic Plan—plus:\"\r\n                  },\r\n                  {\r\n                    \"Text\": \"Advance up to $500\",\r\n                    \"AdditionalInfo\": \"B9 Basic℠ and B9 Premium℠ are optional services, offered by B9 to its members that require to set up a payroll direct deposits to the B9 Account each month. All B9 Basic℠ and B9 Premium℠ members start with up to $100 early pay advance. B9 Premium℠ members have a potential for it to increase up to $500 early pay advance. All B9 members are informed of their current available maxes in the B9 mobile app.Their limit may change at any time, at B9's discretion.\"\r\n                  },\r\n                  {\r\n                    \"Text\": \"Credit Report\",\r\n                    \"additionalInfo\": \"Credit Report is not a product of Evolve Bank & Trust, and offered by third party provider and affiliate of B9.\"\r\n                  },\r\n                  {\r\n                    \"Text\": \"Credit Score\",\r\n                    \"additionalInfo\": \"Credit Score is not a product of Evolve Bank & Trust, and offered by third party provider and affiliate of B9.\"\r\n                  },\r\n                  {\r\n                    \"Text\": \"Premium Support\",\r\n                    \"additionalInfo\": \"• Dedicated priority answer line \\n• We will answer you in an hour \\n\\nOur Customer Support Team is active the following hours: \\n• Mon-Fri: 5 AM to 9 PM Pacific Standard Time (8 AM to 12 AM Eastern Standard Time) \\n• Sat-Sun: 6 AM to 6 PM Pacific Standard Time (9 AM to 9 PM Eastern Standard Time) \\n\\nTime of response may depend on external factors.\"\r\n                  }\r\n                ]");
        }
    }
}
