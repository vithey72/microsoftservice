﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace BNine.Persistence.Migrations
{
    public partial class CreateFunctionAndIndexesForFeatures : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"CREATE NONCLUSTERED INDEX IX_Groups_Kind_Composite
                                    ON [features].[Groups] ([Kind])
                                    INCLUDE ([CreatedAt],[UpdatedAt],[DeletedAt],[Name],[Seed],[Rollout],[SubRangeStart],[SubRangeEnd])
                                ");

            migrationBuilder.Sql(@"CREATE NONCLUSTERED INDEX IX_FeatureGroups_GroupId_Composite
                                    ON [features].[FeatureGroups] ([GroupId])
                                    INCLUDE ([CreatedAt],[UpdatedAt],[DeletedAt],[FeatureId],[IsEnabled],[Settings])
                                ");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"DROP INDEX IX_Groups_Kind_Composite
                                    ON[features].[Groups]");
            migrationBuilder.Sql(@"DROP INDEX INDEX IX_FeatureGroups_GroupId_Composite
                                    ON [features].[FeatureGroups]");
        }
    }
}
