﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace BNine.Persistence.Migrations
{
    public partial class add_to_user_settings_new_flag : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "IsPayAllocationEnabled",
                schema: "user",
                table: "UserSettings",
                nullable: false,
                defaultValue: false);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "IsPayAllocationEnabled",
                schema: "user",
                table: "UserSettings");
        }
    }
}
