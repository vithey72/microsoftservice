﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace BNine.Persistence.Migrations
{
    public partial class RenameFieldsTariffPlan : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "Picture",
                schema: "tariff",
                table: "TariffPlans",
                newName: "PictureUrl");

            migrationBuilder.RenameColumn(
                name: "ClassificationId",
                schema: "tariff",
                table: "TariffPlans",
                newName: "ExternalId");

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreatedAt",
                schema: "tariff",
                table: "UserTariffPlans",
                type: "datetime2",
                nullable: false,
                defaultValueSql: "GETDATE()",
                oldClrType: typeof(DateTime),
                oldType: "datetime2");

            migrationBuilder.AddColumn<DateTime>(
                name: "UpdatedAt",
                schema: "tariff",
                table: "UserTariffPlans",
                type: "datetime2",
                nullable: false,
                defaultValueSql: "GETDATE()");

            migrationBuilder.AddColumn<DateTime>(
                name: "CreatedAt",
                schema: "tariff",
                table: "TariffPlans",
                type: "datetime2",
                nullable: false,
                defaultValueSql: "GETDATE()");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "UpdatedAt",
                schema: "tariff",
                table: "UserTariffPlans");

            migrationBuilder.DropColumn(
                name: "CreatedAt",
                schema: "tariff",
                table: "TariffPlans");

            migrationBuilder.RenameColumn(
                name: "PictureUrl",
                schema: "tariff",
                table: "TariffPlans",
                newName: "Picture");

            migrationBuilder.RenameColumn(
                name: "ExternalId",
                schema: "tariff",
                table: "TariffPlans",
                newName: "ClassificationId");

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreatedAt",
                schema: "tariff",
                table: "UserTariffPlans",
                type: "datetime2",
                nullable: false,
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValueSql: "GETDATE()");
        }
    }
}
