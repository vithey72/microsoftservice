﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace BNine.Persistence.Migrations
{
    public partial class update_external_cards_table_name : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ExternalCard_Users_UserId",
                schema: "bank",
                table: "ExternalCard");

            migrationBuilder.DropPrimaryKey(
                name: "PK_ExternalCard",
                schema: "bank",
                table: "ExternalCard");

            migrationBuilder.RenameTable(
                name: "ExternalCard",
                schema: "bank",
                newName: "ExternalCards",
                newSchema: "bank");

            migrationBuilder.RenameIndex(
                name: "IX_ExternalCard_UserId",
                schema: "bank",
                table: "ExternalCards",
                newName: "IX_ExternalCards_UserId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_ExternalCards",
                schema: "bank",
                table: "ExternalCards",
                column: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_ExternalCards_Users_UserId",
                schema: "bank",
                table: "ExternalCards",
                column: "UserId",
                principalSchema: "user",
                principalTable: "Users",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ExternalCards_Users_UserId",
                schema: "bank",
                table: "ExternalCards");

            migrationBuilder.DropPrimaryKey(
                name: "PK_ExternalCards",
                schema: "bank",
                table: "ExternalCards");

            migrationBuilder.RenameTable(
                name: "ExternalCards",
                schema: "bank",
                newName: "ExternalCard",
                newSchema: "bank");

            migrationBuilder.RenameIndex(
                name: "IX_ExternalCards_UserId",
                schema: "bank",
                table: "ExternalCard",
                newName: "IX_ExternalCard_UserId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_ExternalCard",
                schema: "bank",
                table: "ExternalCard",
                column: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_ExternalCard_Users_UserId",
                schema: "bank",
                table: "ExternalCard",
                column: "UserId",
                principalSchema: "user",
                principalTable: "Users",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
