﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace BNine.Persistence.Migrations
{
    public partial class add_inactive_tariff_text : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "ImageSubtitleObject",
                schema: "tariff",
                table: "TariffPlans",
                newName: "AdvertisementImageSubtitleObject");

            migrationBuilder.AddColumn<string>(
                name: "ActiveImageSubtitleObject",
                schema: "tariff",
                table: "TariffPlans",
                type: "nvarchar(max)",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ActiveImageSubtitleObject",
                schema: "tariff",
                table: "TariffPlans");

            migrationBuilder.RenameColumn(
                name: "AdvertisementImageSubtitleObject",
                schema: "tariff",
                table: "TariffPlans",
                newName: "ImageSubtitleObject");
        }
    }
}
