﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace BNine.Persistence.Migrations
{
    public partial class alter_default_tariff_plans : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "IsDisabled",
                schema: "tariff",
                table: "TariffPlans",
                type: "bit",
                nullable: false,
                defaultValue: false);

            migrationBuilder.UpdateData(
                schema: "tariff",
                table: "TariffPlans",
                keyColumn: "Id",
                keyValue: new Guid("b9491ab9-601b-406b-a689-a9b311773b73"),
                column: "ExternalClassificationId",
                value: 5311);

            migrationBuilder.InsertData(
                schema: "tariff",
                table: "TariffPlans",
                columns: new[] { "Id", "AdvertisementPictureUrl", "CreatedAt", "DiscountPercentage", "ExternalChargeId", "ExternalClassificationId", "Features", "GroupIds", "IconUrl", "IsDisabled", "IsParent", "MonthlyFee", "MonthsDuration", "Name", "PictureUrl", "PreDiscountPrice", "TotalPrice", "Type", "WelcomeScreenPictureUrl" },
                values: new object[] { new Guid("fbf9e0e1-00f0-419a-b172-8b09c6a6d50d"), "https://b9prodaccount.blob.core.windows.net/static-documents-container/TariffPlans/B9_Basic_Plan_1m.png", new DateTime(2023, 5, 15, 0, 0, 0, 0, DateTimeKind.Unspecified), 0m, 0, 693, null, null, "https://b9devaccount.blob.core.windows.net/static-documents-container/TariffPlans/B9_Advance_icon.png", true, false, 0m, 1, "B9 Basic Plan", "https://b9prodaccount.blob.core.windows.net/static-documents-container/TariffPlans/B9_Basic_Plan_1m.png", 0m, 0m, 1, "https://b9devaccount.blob.core.windows.net/static-documents-container/TariffPlans/Welcome_B9_Advance.png" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                schema: "tariff",
                table: "TariffPlans",
                keyColumn: "Id",
                keyValue: new Guid("fbf9e0e1-00f0-419a-b172-8b09c6a6d50d"));

            migrationBuilder.DropColumn(
                name: "IsDisabled",
                schema: "tariff",
                table: "TariffPlans");

            migrationBuilder.UpdateData(
                schema: "tariff",
                table: "TariffPlans",
                keyColumn: "Id",
                keyValue: new Guid("b9491ab9-601b-406b-a689-a9b311773b73"),
                column: "ExternalClassificationId",
                value: 693);
        }
    }
}
