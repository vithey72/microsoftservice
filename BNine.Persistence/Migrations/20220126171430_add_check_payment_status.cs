﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace BNine.Persistence.Migrations
{
    public partial class add_check_payment_status : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "PaymentStatus",
                table: "CheckCashingTransactions",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateIndex(
                name: "IX_CheckCashingTransactions_PaymentStatus",
                table: "CheckCashingTransactions",
                column: "PaymentStatus");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_CheckCashingTransactions_PaymentStatus",
                table: "CheckCashingTransactions");

            migrationBuilder.DropColumn(
                name: "PaymentStatus",
                table: "CheckCashingTransactions");
        }
    }
}
