﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace BNine.Persistence.Migrations
{
    public partial class add_bank_account_balances : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "ExternalBankAccountBalances",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false, defaultValueSql: "newsequentialid()"),
                    Currency = table.Column<string>(nullable: true),
                    AvailableAmount = table.Column<decimal>(nullable: true),
                    CurrentAmount = table.Column<decimal>(nullable: true),
                    ExternalBankAccountId = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ExternalBankAccountBalances", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ExternalBankAccountBalances_ExternalBankAccounts_ExternalBankAccountId",
                        column: x => x.ExternalBankAccountId,
                        principalTable: "ExternalBankAccounts",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_ExternalBankAccountBalances_ExternalBankAccountId",
                table: "ExternalBankAccountBalances",
                column: "ExternalBankAccountId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "ExternalBankAccountBalances");
        }
    }
}
