﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace BNine.Persistence.Migrations
{
    public partial class addlimits : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<decimal>(
                name: "PullFromExternalCardMaxAmount",
                schema: "settings",
                table: "SavingAccountSettings",
                type: "decimal(18,2)",
                nullable: false,
                defaultValue: 500m);

            migrationBuilder.AddColumn<decimal>(
                name: "PullFromExternalCardMinAmount",
                schema: "settings",
                table: "SavingAccountSettings",
                type: "decimal(18,2)",
                nullable: false,
                defaultValue: 20m);

            migrationBuilder.AddColumn<decimal>(
                name: "PushToExternalCardMinAmount",
                schema: "settings",
                table: "SavingAccountSettings",
                type: "decimal(18,2)",
                nullable: false,
                defaultValue: 20m);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "PullFromExternalCardMaxAmount",
                schema: "settings",
                table: "SavingAccountSettings");

            migrationBuilder.DropColumn(
                name: "PullFromExternalCardMinAmount",
                schema: "settings",
                table: "SavingAccountSettings");

            migrationBuilder.DropColumn(
                name: "PushToExternalCardMinAmount",
                schema: "settings",
                table: "SavingAccountSettings");
        }
    }
}
