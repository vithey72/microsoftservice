﻿using BNine.Persistence.Configurations.Extensions;
using BNine.Persistence.Constants;
using Microsoft.EntityFrameworkCore.Migrations;

namespace BNine.Persistence.Migrations
{
    public partial class add_versioning_to_client_loan_settings : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddVersioningToExistedTable("ClientLoanSettings", "user", MigrationDefaults.VersioningSchema);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {

        }
    }
}
