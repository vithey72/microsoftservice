﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace BNine.Persistence.Migrations
{
    public partial class Add_additional_rating_fields : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<decimal>(
                name: "CardTransactions",
                schema: "user",
                table: "ClientRatings",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<decimal>(
                name: "CurrentPaycheckDeposits",
                schema: "user",
                table: "ClientRatings",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<decimal>(
                name: "OnTimeRepaymentsOfAdvances",
                schema: "user",
                table: "ClientRatings",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<decimal>(
                name: "PaycheckDepositHistory",
                schema: "user",
                table: "ClientRatings",
                nullable: false,
                defaultValue: 0m);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "CardTransactions",
                schema: "user",
                table: "ClientRatings");

            migrationBuilder.DropColumn(
                name: "CurrentPaycheckDeposits",
                schema: "user",
                table: "ClientRatings");

            migrationBuilder.DropColumn(
                name: "OnTimeRepaymentsOfAdvances",
                schema: "user",
                table: "ClientRatings");

            migrationBuilder.DropColumn(
                name: "PaycheckDepositHistory",
                schema: "user",
                table: "ClientRatings");
        }
    }
}
