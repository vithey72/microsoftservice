﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace BNine.Persistence.Migrations
{
    public partial class add_new_transfer_icons : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData(
                schema: "settings",
                table: "TransferIconSettings",
                columns: new[] { "Id", "IconUrl" },
                values: new object[] { 10, "https://b9devaccount.blob.core.windows.net/static-documents-container/TransactionIcons/10checkmark.png" });

            migrationBuilder.InsertData(
                schema: "settings",
                table: "TransferIconSettings",
                columns: new[] { "Id", "IconUrl" },
                values: new object[] { 11, "https://b9devaccount.blob.core.windows.net/static-documents-container/TransactionIcons/11cross.png" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                schema: "settings",
                table: "TransferIconSettings",
                keyColumn: "Id",
                keyValue: 10);

            migrationBuilder.DeleteData(
                schema: "settings",
                table: "TransferIconSettings",
                keyColumn: "Id",
                keyValue: 11);
        }
    }
}
