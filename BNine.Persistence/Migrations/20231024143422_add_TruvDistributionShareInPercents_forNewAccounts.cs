﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace BNine.Persistence.Migrations
{
    public partial class add_TruvDistributionShareInPercents_forNewAccounts : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "TruvDistributionShareInPercents",
                schema: "settings",
                table: "CommonAccountSettings",
                type: "int",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "TruvDistributionShareInPercents",
                schema: "settings",
                table: "CommonAccountSettings");
        }
    }
}
