﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace BNine.Persistence.Migrations
{
    public partial class rename_user_blocked_reason : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                schema: "user",
                table: "UserBlockReasons",
                keyColumn: "Id",
                keyValue: new Guid("c2b0b2a0-5b0a-4b0e-8b0a-5b0a4b0e8b0a"),
                column: "Value",
                value: "Voluntary self-serviced request");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                schema: "user",
                table: "UserBlockReasons",
                keyColumn: "Id",
                keyValue: new Guid("c2b0b2a0-5b0a-4b0e-8b0a-5b0a4b0e8b0a"),
                column: "Value",
                value: "User action");
        }
    }
}
