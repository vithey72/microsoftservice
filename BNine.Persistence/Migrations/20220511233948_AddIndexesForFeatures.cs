﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace BNine.Persistence.Migrations
{
    public partial class AddIndexesForFeatures : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateIndex(
                name: "IX_UserGroups_GroupId_UserId",
                schema: "features",
                table: "UserGroups",
                columns: new[] { "GroupId", "UserId" },
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_FeatureGroups_GroupId_FeatureId",
                schema: "features",
                table: "FeatureGroups",
                columns: new[] { "GroupId", "FeatureId" },
                unique: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_UserGroups_GroupId_UserId",
                schema: "features",
                table: "UserGroups");

            migrationBuilder.DropIndex(
                name: "IX_FeatureGroups_GroupId_FeatureId",
                schema: "features",
                table: "FeatureGroups");
        }
    }
}
