﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace BNine.Persistence.Migrations
{
    public partial class add_additional_fields : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<DateTime>(
                name: "CreatedAt",
                schema: "bank",
                table: "LoanAccounts",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "DeletedAt",
                schema: "bank",
                table: "LoanAccounts",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "UpdatedAt",
                schema: "bank",
                table: "LoanAccounts",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "CreatedAt",
                schema: "bank",
                table: "DebitCards",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "SuspendedAt",
                schema: "bank",
                table: "DebitCards",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "TerminatedAt",
                schema: "bank",
                table: "DebitCards",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "CreatedAt",
                schema: "administrators",
                table: "ExternalIdentities",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "DeletedAt",
                schema: "administrators",
                table: "ExternalIdentities",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "UpdatedAt",
                schema: "administrators",
                table: "ExternalIdentities",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "CreatedAt",
                schema: "administrators",
                table: "Administrators",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "DeletedAt",
                schema: "administrators",
                table: "Administrators",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "UpdatedAt",
                schema: "administrators",
                table: "Administrators",
                nullable: true);

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreatedAt",
                table: "LoanSpecialOffers",
                nullable: true,
                oldClrType: typeof(DateTime),
                oldType: "datetime2");

            migrationBuilder.AddColumn<DateTime>(
                name: "DeletedAt",
                table: "LoanSpecialOffers",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "UpdatedAt",
                table: "LoanSpecialOffers",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "CreatedAt",
                table: "EarlySalaryWidgetConfigurations",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<DateTime>(
                name: "DeletedAt",
                table: "EarlySalaryWidgetConfigurations",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "UpdatedAt",
                table: "EarlySalaryWidgetConfigurations",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "CreatedAt",
                table: "DevicesDetails",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "DeletedAt",
                table: "DevicesDetails",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "UpdatedAt",
                table: "DevicesDetails",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "CreatedAt",
                table: "Devices",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "DeletedAt",
                table: "Devices",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "UpdatedAt",
                table: "Devices",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "DeletedAt",
                table: "BonusCodes",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "UpdatedAt",
                table: "BonusCodes",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "CreatedAt",
                schema: "bank",
                table: "LoanAccounts");

            migrationBuilder.DropColumn(
                name: "DeletedAt",
                schema: "bank",
                table: "LoanAccounts");

            migrationBuilder.DropColumn(
                name: "UpdatedAt",
                schema: "bank",
                table: "LoanAccounts");

            migrationBuilder.DropColumn(
                name: "CreatedAt",
                schema: "bank",
                table: "DebitCards");

            migrationBuilder.DropColumn(
                name: "SuspendedAt",
                schema: "bank",
                table: "DebitCards");

            migrationBuilder.DropColumn(
                name: "TerminatedAt",
                schema: "bank",
                table: "DebitCards");

            migrationBuilder.DropColumn(
                name: "CreatedAt",
                schema: "administrators",
                table: "ExternalIdentities");

            migrationBuilder.DropColumn(
                name: "DeletedAt",
                schema: "administrators",
                table: "ExternalIdentities");

            migrationBuilder.DropColumn(
                name: "UpdatedAt",
                schema: "administrators",
                table: "ExternalIdentities");

            migrationBuilder.DropColumn(
                name: "CreatedAt",
                schema: "administrators",
                table: "Administrators");

            migrationBuilder.DropColumn(
                name: "DeletedAt",
                schema: "administrators",
                table: "Administrators");

            migrationBuilder.DropColumn(
                name: "UpdatedAt",
                schema: "administrators",
                table: "Administrators");

            migrationBuilder.DropColumn(
                name: "DeletedAt",
                table: "LoanSpecialOffers");

            migrationBuilder.DropColumn(
                name: "UpdatedAt",
                table: "LoanSpecialOffers");

            migrationBuilder.DropColumn(
                name: "CreatedAt",
                table: "EarlySalaryWidgetConfigurations");

            migrationBuilder.DropColumn(
                name: "DeletedAt",
                table: "EarlySalaryWidgetConfigurations");

            migrationBuilder.DropColumn(
                name: "UpdatedAt",
                table: "EarlySalaryWidgetConfigurations");

            migrationBuilder.DropColumn(
                name: "CreatedAt",
                table: "DevicesDetails");

            migrationBuilder.DropColumn(
                name: "DeletedAt",
                table: "DevicesDetails");

            migrationBuilder.DropColumn(
                name: "UpdatedAt",
                table: "DevicesDetails");

            migrationBuilder.DropColumn(
                name: "CreatedAt",
                table: "Devices");

            migrationBuilder.DropColumn(
                name: "DeletedAt",
                table: "Devices");

            migrationBuilder.DropColumn(
                name: "UpdatedAt",
                table: "Devices");

            migrationBuilder.DropColumn(
                name: "DeletedAt",
                table: "BonusCodes");

            migrationBuilder.DropColumn(
                name: "UpdatedAt",
                table: "BonusCodes");

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreatedAt",
                table: "LoanSpecialOffers",
                type: "datetime2",
                nullable: false,
                oldClrType: typeof(DateTime),
                oldNullable: true);
        }
    }
}
