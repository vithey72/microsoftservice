﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace BNine.Persistence.Migrations
{
    public partial class add_current_advances_to_user_stats : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<decimal>(
                name: "CurrentBasicAdvanceAmount",
                schema: "user",
                table: "UserSyncedStats",
                type: "decimal(18,2)",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<decimal>(
                name: "CurrentPremiumAdvanceAmount",
                schema: "user",
                table: "UserSyncedStats",
                type: "decimal(18,2)",
                nullable: false,
                defaultValue: 0m);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "CurrentBasicAdvanceAmount",
                schema: "user",
                table: "UserSyncedStats");

            migrationBuilder.DropColumn(
                name: "CurrentPremiumAdvanceAmount",
                schema: "user",
                table: "UserSyncedStats");
        }
    }
}
