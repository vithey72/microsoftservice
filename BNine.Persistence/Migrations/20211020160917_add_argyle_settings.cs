﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace BNine.Persistence.Migrations
{
    public partial class add_argyle_settings : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "ArgyleSettings",
                schema: "settings",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false, defaultValueSql: "newsequentialid()"),
                    DefaultAmountAllocation = table.Column<decimal>(type: "decimal(18,2)", nullable: false),
                    MinAmountAllocation = table.Column<decimal>(type: "decimal(18,2)", nullable: false),
                    MaxAmountAllocation = table.Column<decimal>(type: "decimal(18,2)", nullable: false),
                    DefaultPercentAllocation = table.Column<decimal>(type: "decimal(18,2)", nullable: false),
                    MinPercentAllocation = table.Column<decimal>(type: "decimal(18,2)", nullable: false),
                    MaxPercentAllocation = table.Column<decimal>(type: "decimal(18,2)", nullable: false),
                    CreatedAt = table.Column<DateTime>(nullable: false),
                    UpdatedAt = table.Column<DateTime>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ArgyleSettings", x => x.Id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "ArgyleSettings",
                schema: "settings");
        }
    }
}
