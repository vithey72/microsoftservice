﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace BNine.Persistence.Migrations
{
    public partial class paid_services_add_bus_name_column : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "ServiceBusName",
                schema: "tariff",
                table: "PaidUserServices",
                type: "nvarchar(max)",
                nullable: false,
                defaultValue: "");

            migrationBuilder.UpdateData(
                schema: "tariff",
                table: "PaidUserServices",
                keyColumn: "Id",
                keyValue: new Guid("2dc6aeec-5ee7-4661-9338-50b3101e59da"),
                column: "ServiceBusName",
                value: "premium_support");

            migrationBuilder.UpdateData(
                schema: "tariff",
                table: "PaidUserServices",
                keyColumn: "Id",
                keyValue: new Guid("7a143e78-9022-44c7-9645-d933d7f29430"),
                column: "ServiceBusName",
                value: "extension");

            migrationBuilder.UpdateData(
                schema: "tariff",
                table: "PaidUserServices",
                keyColumn: "Id",
                keyValue: new Guid("8c67b8a0-9c03-464d-9055-2425abb5a490"),
                column: "ServiceBusName",
                value: "extension");

            migrationBuilder.UpdateData(
                schema: "tariff",
                table: "PaidUserServices",
                keyColumn: "Id",
                keyValue: new Guid("ac5b3d89-fa4c-43e9-b308-9c4a0c26d863"),
                column: "ServiceBusName",
                value: "extension");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ServiceBusName",
                schema: "tariff",
                table: "PaidUserServices");
        }
    }
}
