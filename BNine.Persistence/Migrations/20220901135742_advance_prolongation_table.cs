﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace BNine.Persistence.Migrations
{
    public partial class advance_prolongation_table : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "AdvanceExtensionStatistics",
                schema: "user",
                columns: table => new
                {
                    UserId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    AvailableLimit = table.Column<int>(type: "int", nullable: false),
                    ExtensionPrice = table.Column<int>(type: "int", nullable: false),
                    ExtensionLimit = table.Column<int>(type: "int", nullable: false),
                    ExtensionExpiresAt = table.Column<DateTime>(type: "datetime2", nullable: false),
                    Rating = table.Column<decimal>(type: "decimal(18,2)", nullable: false),
                    Slider1 = table.Column<decimal>(type: "decimal(18,2)", nullable: false),
                    Slider2 = table.Column<decimal>(type: "decimal(18,2)", nullable: false),
                    Slider3 = table.Column<decimal>(type: "decimal(18,2)", nullable: false),
                    Slider4 = table.Column<decimal>(type: "decimal(18,2)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AdvanceExtensionStatistics", x => x.UserId);
                    table.ForeignKey(
                        name: "FK_AdvanceExtensionStatistics_Users_UserId",
                        column: x => x.UserId,
                        principalSchema: "user",
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.InsertData(
                schema: "tariff",
                table: "PaidUserServices",
                columns: new[] { "Id", "ChargeTypeId", "CreatedAt", "Description", "DurationInDays", "Name" },
                values: new object[] { new Guid("7a143e78-9022-44c7-9645-d933d7f29430"), 332, new DateTime(2022, 8, 30, 0, 0, 0, 0, DateTimeKind.Unspecified), "One time payment for 1 use of Unfreeze service", 0, "Advance unfreeze fee 5" });

            migrationBuilder.InsertData(
                schema: "tariff",
                table: "PaidUserServices",
                columns: new[] { "Id", "ChargeTypeId", "CreatedAt", "Description", "DurationInDays", "Name" },
                values: new object[] { new Guid("8c67b8a0-9c03-464d-9055-2425abb5a490"), 334, new DateTime(2022, 8, 30, 0, 0, 0, 0, DateTimeKind.Unspecified), "Ine time payment for 1 use of Unfreeze service", 0, "Advance unfreeze fee 15" });

            migrationBuilder.InsertData(
                schema: "tariff",
                table: "PaidUserServices",
                columns: new[] { "Id", "ChargeTypeId", "CreatedAt", "Description", "DurationInDays", "Name" },
                values: new object[] { new Guid("ac5b3d89-fa4c-43e9-b308-9c4a0c26d863"), 333, new DateTime(2022, 8, 30, 0, 0, 0, 0, DateTimeKind.Unspecified), "One time payment for 1 use of Unfreeze service", 0, "Advance unfreeze fee 10" });

            migrationBuilder.CreateIndex(
                name: "IX_AdvanceExtensionStatistics_ExtensionExpiresAt",
                schema: "user",
                table: "AdvanceExtensionStatistics",
                column: "ExtensionExpiresAt");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "AdvanceExtensionStatistics",
                schema: "user");

            migrationBuilder.DeleteData(
                schema: "tariff",
                table: "PaidUserServices",
                keyColumn: "Id",
                keyValue: new Guid("7a143e78-9022-44c7-9645-d933d7f29430"));

            migrationBuilder.DeleteData(
                schema: "tariff",
                table: "PaidUserServices",
                keyColumn: "Id",
                keyValue: new Guid("8c67b8a0-9c03-464d-9055-2425abb5a490"));

            migrationBuilder.DeleteData(
                schema: "tariff",
                table: "PaidUserServices",
                keyColumn: "Id",
                keyValue: new Guid("ac5b3d89-fa4c-43e9-b308-9c4a0c26d863"));
        }
    }
}
