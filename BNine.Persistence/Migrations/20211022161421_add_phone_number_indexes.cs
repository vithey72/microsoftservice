﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace BNine.Persistence.Migrations
{
    public partial class add_phone_number_indexes : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropPrimaryKey(
                name: "PK_OTPs",
                table: "OTPs");

            migrationBuilder.AddPrimaryKey(
                name: "PK_OTPs",
                table: "OTPs",
                column: "Id")
                .Annotation("SqlServer:Clustered", false);

            migrationBuilder.CreateIndex(
                name: "IX_Users_Phone",
                schema: "user",
                table: "Users",
                column: "Phone");

            migrationBuilder.CreateIndex(
                name: "IX_OTPs_PhoneNumber",
                table: "OTPs",
                column: "PhoneNumber")
                .Annotation("SqlServer:Clustered", true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_Users_Phone",
                schema: "user",
                table: "Users");

            migrationBuilder.DropPrimaryKey(
                name: "PK_OTPs",
                table: "OTPs");

            migrationBuilder.DropIndex(
                name: "IX_OTPs_PhoneNumber",
                table: "OTPs");

            migrationBuilder.AddPrimaryKey(
                name: "PK_OTPs",
                table: "OTPs",
                column: "Id");
        }
    }
}
