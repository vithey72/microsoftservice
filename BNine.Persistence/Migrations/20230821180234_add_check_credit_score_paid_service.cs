﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace BNine.Persistence.Migrations
{
    public partial class add_check_credit_score_paid_service : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData(
                schema: "tariff",
                table: "PaidUserServices",
                columns: new[] { "Id", "ChargeTypeId", "CreatedAt", "Description", "DurationInDays", "Name", "ServiceBusName" },
                values: new object[] { new Guid("b2f65678-402a-11ee-be56-0242ac120002"), 69, new DateTime(2023, 8, 21, 0, 0, 0, 0, DateTimeKind.Unspecified), "Check credit score paid service", 14, "Check credit score paid service", "check_credit_score_paid_service" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                schema: "tariff",
                table: "PaidUserServices",
                keyColumn: "Id",
                keyValue: new Guid("b2f65678-402a-11ee-be56-0242ac120002"));
        }
    }
}
