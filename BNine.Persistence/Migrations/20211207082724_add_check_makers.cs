﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace BNine.Persistence.Migrations
{
    public partial class add_check_makers : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<Guid>(
                name: "CurrentEmployerId",
                schema: "user",
                table: "AlltrustCustomerSettings",
                nullable: true);

            migrationBuilder.AddColumn<Guid>(
                name: "CheckMakerId",
                table: "CheckCashingTransactions",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "CheckMakers",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false, defaultValueSql: "newsequentialid()"),
                    MakerRoutingNumber = table.Column<string>(nullable: true),
                    MakerAccountNumber = table.Column<string>(nullable: true),
                    MakerName = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CheckMakers", x => x.Id);
                });

            migrationBuilder.CreateIndex(
                name: "IX_AlltrustCustomerSettings_CurrentEmployerId",
                schema: "user",
                table: "AlltrustCustomerSettings",
                column: "CurrentEmployerId");

            migrationBuilder.CreateIndex(
                name: "IX_CheckCashingTransactions_CheckMakerId",
                table: "CheckCashingTransactions",
                column: "CheckMakerId");

            migrationBuilder.CreateIndex(
                name: "IX_CheckMaker_MakerRoutingNumber_MakerAccountNumber",
                table: "CheckMakers",
                columns: new[] { "MakerRoutingNumber", "MakerAccountNumber" },
                unique: true,
                filter: "[MakerRoutingNumber] IS NOT NULL AND [MakerAccountNumber] IS NOT NULL");

            migrationBuilder.AddForeignKey(
                name: "FK_CheckCashingTransactions_CheckMakers_CheckMakerId",
                table: "CheckCashingTransactions",
                column: "CheckMakerId",
                principalTable: "CheckMakers",
                principalColumn: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_AlltrustCustomerSettings_CheckMakers_CurrentEmployerId",
                schema: "user",
                table: "AlltrustCustomerSettings",
                column: "CurrentEmployerId",
                principalTable: "CheckMakers",
                principalColumn: "Id");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_CheckCashingTransactions_CheckMakers_CheckMakerId",
                table: "CheckCashingTransactions");

            migrationBuilder.DropForeignKey(
                name: "FK_AlltrustCustomerSettings_CheckMakers_CurrentEmployerId",
                schema: "user",
                table: "AlltrustCustomerSettings");

            migrationBuilder.DropTable(
                name: "CheckMakers");

            migrationBuilder.DropIndex(
                name: "IX_AlltrustCustomerSettings_CurrentEmployerId",
                schema: "user",
                table: "AlltrustCustomerSettings");

            migrationBuilder.DropIndex(
                name: "IX_CheckCashingTransactions_CheckMakerId",
                table: "CheckCashingTransactions");

            migrationBuilder.DropColumn(
                name: "CurrentEmployerId",
                schema: "user",
                table: "AlltrustCustomerSettings");

            migrationBuilder.DropColumn(
                name: "CheckMakerId",
                table: "CheckCashingTransactions");
        }
    }
}
