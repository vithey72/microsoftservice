﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace BNine.Persistence.Migrations
{
    public partial class cashback_is_200_bucks : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                schema: "settings",
                table: "CashbackProgramSettings",
                keyColumn: "Id",
                keyValue: new Guid("8d8f7fea-67d5-46aa-857f-ec024d97e25a"),
                columns: new[] { "MinimumSpentAmount", "SlideshowScreens" },
                values: new object[] { 200m, "{\"screens\":[{\"header\":\"Get cashback every month\",\"text\":\"Select the categories you want to use the most every month. Cashback will be deposited by the 5th of the following month.\",\"buttonText\":\"NEXT\",\"imageUrl\":\"https://b9devaccount.blob.core.windows.net/static-documents-container/CashbackSlideshow/page1.png\"},{\"header\":\"Spend at least $200\",\"text\":\"You will begin to earn cashback as soon as you reach $200 worth of the categories you selected.\",\"buttonText\":\"NEXT\",\"imageUrl\":\"https://b9devaccount.blob.core.windows.net/static-documents-container/CashbackSlideshow/page2.png\"},{\"header\":\"Select categories of cashback\",\"text\":\"Right under selected categories you'll see the categories you chose for that particular month. You will need to select categories every month.\",\"buttonText\":\"NEXT\",\"imageUrl\":\"https://b9devaccount.blob.core.windows.net/static-documents-container/CashbackSlideshow/page3.png\",\"link\":\"https://b9devaccount.blob.core.windows.net/static-documents-container/Terms of Service.pdf\",\"linkText\":\"Terms and conditions\"}]}" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                schema: "settings",
                table: "CashbackProgramSettings",
                keyColumn: "Id",
                keyValue: new Guid("8d8f7fea-67d5-46aa-857f-ec024d97e25a"),
                columns: new[] { "MinimumSpentAmount", "SlideshowScreens" },
                values: new object[] { 100m, "{\"screens\":[{\"header\":\"Get cashback every month\",\"text\":\"Select the categories you want to use the most every month. Cashback will be deposited by the 5th of the following month.\",\"buttonText\":\"NEXT\",\"imageUrl\":\"https://b9devaccount.blob.core.windows.net/static-documents-container/CashbackSlideshow/page1.png\"},{\"header\":\"Spend at least $100\",\"text\":\"You will begin to earn cashback as soon as you reach $100 worth of the categories you selected.\",\"buttonText\":\"NEXT\",\"imageUrl\":\"https://b9devaccount.blob.core.windows.net/static-documents-container/CashbackSlideshow/page2.png\"},{\"header\":\"Select categories of cashback\",\"text\":\"Right under selected categories you'll see the categories you chose for that particular month. You will need to select categories every month.\",\"buttonText\":\"NEXT\",\"imageUrl\":\"https://b9devaccount.blob.core.windows.net/static-documents-container/CashbackSlideshow/page3.png\",\"link\":\"https://b9devaccount.blob.core.windows.net/static-documents-container/Terms of Service.pdf\",\"linkText\":\"Terms and conditions\"}]}" });
        }
    }
}
