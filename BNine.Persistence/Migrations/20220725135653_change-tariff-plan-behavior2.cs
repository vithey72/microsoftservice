﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace BNine.Persistence.Migrations
{
    public partial class changetariffplanbehavior2 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                schema: "tariff",
                table: "TariffPlans",
                keyColumn: "Id",
                keyValue: new Guid("b6edf67d-5fdd-4246-b47a-259b4c4e49be"),
                column: "Features",
                value: "[\r\n                  {\r\n                    \"Text\": \"Everything in the \\nB9 Advance Plan—plus:\"\r\n                  },\r\n                  {\r\n                    \"Text\": \"Advance up to 100 % \\nof your Paycheck\",\r\n                    \"AdditionalInfo\": \"B9 Advance℠ and B9 Premium℠ are optional services, offered by B9 to its members that require to set up a payroll direct deposits to the B9 Account each month.All B9 Advance℠ and B9 Premium℠ members start with up to $100 early pay advance.B9 Premium℠ members have a potential for it to increase up to 100 % of their earnings they deposit from their paycheck to their B9 Account.All B9 members are informed of their current available maxes in the B9 mobile app.Their limit may change at any time, at B9's discretion.\"\r\n                  },\r\n                  {\r\n                    \"Text\": \"Credit Report\",\r\n                    \"additionalInfo\": \"Credit Report is not a product of Evolve Bank & Trust, and offered by third party provider and affiliate of B9.\"\r\n                  },\r\n                  {\r\n                    \"Text\": \"Credit Score\",\r\n                    \"additionalInfo\": \"Credit Score is not a product of Evolve Bank & Trust, and offered by third party provider and affiliate of B9.\"\r\n                  },\r\n                  {\r\n                    \"Text\": \"Credit Score Simulator\",\r\n                    \"additionalInfo\": \"Credit Score Simulator is not a product of Evolve Bank & Trust, and offered by third party provider and affiliate of B9.\"\r\n                  }\r\n                ]");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                schema: "tariff",
                table: "TariffPlans",
                keyColumn: "Id",
                keyValue: new Guid("b6edf67d-5fdd-4246-b47a-259b4c4e49be"),
                column: "Features",
                value: "[\r\n                  {\r\n                    \"Text\": \"Everything in the \\nB9 Advance Plan—plus:\"\r\n                  },\r\n                  {\r\n                    \"Text\": \"Advance up to 100 % \\nof your Paycheck\",\r\n                    \"AdditionalInfo\": \"B9 Advance℠ and B9 Premium℠ are optional services, offered by B9 to its members that require to set up a payroll direct deposits to the B9 Account each month.All B9 Advance℠ and B9 Premium℠ members start with up to $100 early pay advance.B9 Premium℠ members have a potential for it to increase up to 100 % of their earnings they deposit from their paycheck to their B9 Account.All B9 members are informed of their current available maxes in the B9 mobile app.Their limit may change at any time, at B9's discretion.\"\r\n                  },\r\n                  {\r\n                    \"Text\": \"Credit Report\"\r\n                  },\r\n                  {\r\n                    \"Text\": \"Credit Score\"\r\n                  },\r\n                  {\r\n                    \"Text\": \"Credit Score Simulator\"\r\n                  }\r\n                ]");
        }
    }
}
