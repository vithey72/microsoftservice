﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace BNine.Persistence.Migrations
{
    public partial class ChangeSchemaTransferIconSettings : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameTable(
                name: "TransferIconSettings",
                newName: "TransferIconSettings",
                newSchema: "settings");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameTable(
                name: "TransferIconSettings",
                schema: "settings",
                newName: "TransferIconSettings");
        }
    }
}
