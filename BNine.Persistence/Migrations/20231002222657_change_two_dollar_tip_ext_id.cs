﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace BNine.Persistence.Migrations
{
    public partial class change_two_dollar_tip_ext_id : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                schema: "tariff",
                table: "PaidUserServices",
                keyColumn: "Id",
                keyValue: new Guid("eac381d5-ad0e-4dd3-a1fc-5f7293a34891"),
                column: "ChargeTypeId",
                value: 71);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                schema: "tariff",
                table: "PaidUserServices",
                keyColumn: "Id",
                keyValue: new Guid("eac381d5-ad0e-4dd3-a1fc-5f7293a34891"),
                column: "ChargeTypeId",
                value: 2014);
        }
    }
}
