﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace BNine.Persistence.Migrations
{
    public partial class add_accountClosureSchedule : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "AccountClosureSchedule",
                schema: "user",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false, defaultValueSql: "newsequentialid()"),
                    CreatedAt = table.Column<DateTime>(type: "datetime2", nullable: false, defaultValueSql: "getutcdate()"),
                    UserId = table.Column<Guid>(type: "uniqueidentifier", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AccountClosureSchedule", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AccountClosureSchedule_Users_UserId",
                        column: x => x.UserId,
                        principalSchema: "user",
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "CommonAccountSettings",
                schema: "settings",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false, defaultValueSql: "newsequentialid()"),
                    VoluntaryClosureDelayInDays = table.Column<int>(type: "int", nullable: false, defaultValue: 30),
                    PendingClosureCheckGapInHours = table.Column<int>(type: "int", nullable: false, defaultValue: 24)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CommonAccountSettings", x => x.Id);
                });

            migrationBuilder.InsertData(
                schema: "settings",
                table: "CommonAccountSettings",
                column: "Id",
                value: new Guid("1406ff7e-3f65-4f9f-a30a-27db54cda9dc"));

            migrationBuilder.CreateIndex(
                name: "IX_AccountClosureSchedule_CreatedAt",
                schema: "user",
                table: "AccountClosureSchedule",
                column: "CreatedAt");

            migrationBuilder.CreateIndex(
                name: "IX_AccountClosureSchedule_UserId",
                schema: "user",
                table: "AccountClosureSchedule",
                column: "UserId",
                unique: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "AccountClosureSchedule",
                schema: "user");

            migrationBuilder.DropTable(
                name: "CommonAccountSettings",
                schema: "settings");
        }
    }
}
