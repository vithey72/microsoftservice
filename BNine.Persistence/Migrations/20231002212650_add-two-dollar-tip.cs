﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace BNine.Persistence.Migrations
{
    public partial class addtwodollartip : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData(
                schema: "tariff",
                table: "PaidUserServices",
                columns: new[] { "Id", "ChargeTypeId", "CreatedAt", "Description", "DurationInDays", "Name", "ObsoletedAt", "ServiceBusName" },
                values: new object[] { new Guid("eac381d5-ad0e-4dd3-a1fc-5f7293a34891"), 2014, new DateTime(2023, 10, 2, 0, 0, 0, 0, DateTimeKind.Unspecified), "Tip of $2", 14, "After Advance Tip 2", null, "after_advance_tip" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                schema: "tariff",
                table: "PaidUserServices",
                keyColumn: "Id",
                keyValue: new Guid("eac381d5-ad0e-4dd3-a1fc-5f7293a34891"));
        }
    }
}
