﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace BNine.Persistence.Migrations
{
    public partial class set_CommonAccountSettings_BlockClosureOffsetFromActivationInDays_backToFive : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                schema: "settings",
                table: "CommonAccountSettings",
                keyColumn: "Id",
                keyValue: new Guid("1406ff7e-3f65-4f9f-a30a-27db54cda9dc"),
                column: "BlockClosureOffsetFromActivationInDays",
                value: 5);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                schema: "settings",
                table: "CommonAccountSettings",
                keyColumn: "Id",
                keyValue: new Guid("1406ff7e-3f65-4f9f-a30a-27db54cda9dc"),
                column: "BlockClosureOffsetFromActivationInDays",
                value: 0);
        }
    }
}
