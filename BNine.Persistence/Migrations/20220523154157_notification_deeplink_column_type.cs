﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace BNine.Persistence.Migrations
{
    public partial class notification_deeplink_column_type : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "LegacyDeeplink",
                table: "Notifications",
                type: "int",
                nullable: true);

            migrationBuilder.Sql("update Notifications set LegacyDeeplink = Deeplink");

            migrationBuilder.AlterColumn<string>(
                name: "Deeplink",
                table: "Notifications",
                type: "nvarchar(max)",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "int",
                oldNullable: true);

            migrationBuilder.Sql(
@"update Notifications set Deeplink = CASE LegacyDeeplink
WHEN 0 THEN 'none'
WHEN 1 THEN 'wallet'
WHEN 2 THEN 'argyle'
WHEN 3 THEN 'credit'
WHEN 4 THEN 'bonus'
WHEN 5 THEN 'early-salary-manual-form'
WHEN 6 THEN 'pull_transaction'
WHEN 7 THEN 'push_transaction'
WHEN 8 THEN 'manual_survey'
ELSE NULL
END");

            migrationBuilder.DropColumn(
                name: "LegacyDeeplink",
                table: "Notifications");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "RevertDeeplink",
                table: "Notifications",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.Sql("update Notifications set RevertDeeplink = Deeplink");

            // need to drop since alter fails due to invalid data conversion
            migrationBuilder.DropColumn(
                name: "Deeplink",
                table: "Notifications");
            migrationBuilder.AddColumn<int>(
                name: "Deeplink",
                table: "Notifications",
                type: "int",
                nullable: true);

            migrationBuilder.Sql(
@"update Notifications set Deeplink = CASE RevertDeeplink
WHEN 'none' THEN 0
WHEN 'wallet' THEN 1
WHEN 'argyle' THEN 2
WHEN 'credit' THEN 3
WHEN 'bonus' THEN 4
WHEN 'early-salary-manual-form' THEN 5
WHEN 'pull_transaction' THEN 6
WHEN 'push_transaction' THEN 7
WHEN 'manual_survey' THEN 8
ELSE NULL
END");

            migrationBuilder.DropColumn(
                name: "RevertDeeplink",
                table: "Notifications");
        }
    }
}
