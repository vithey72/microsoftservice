﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace BNine.Persistence.Migrations
{
    public partial class updated_transaction_analytics_categories : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "TransactionCategories",
                keyColumn: "Key",
                keyValue: "supermarkets-online-stores",
                column: "Title",
                value: "Supermarkets & Groceries");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
   
            migrationBuilder.UpdateData(
                table: "TransactionCategories",
                keyColumn: "Key",
                keyValue: "supermarkets-online-stores",
                column: "Title",
                value: "Groceries");
        }
    }
}
