﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace BNine.Persistence.Migrations
{
    public partial class changetariffplanbehavior : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Parent",
                schema: "tariff",
                table: "TariffPlans");

            migrationBuilder.AlterColumn<int>(
                name: "Period",
                schema: "tariff",
                table: "TariffPlans",
                type: "int",
                nullable: false,
                oldClrType: typeof(int),
                oldType: "int",
                oldDefaultValue: 1);

            migrationBuilder.AddColumn<bool>(
                name: "IsBaseTariff",
                schema: "tariff",
                table: "TariffPlans",
                type: "bit",
                nullable: false,
                defaultValue: false);

            migrationBuilder.UpdateData(
                schema: "tariff",
                table: "TariffPlans",
                keyColumn: "Id",
                keyValue: new Guid("6c2b90bc-7703-ed11-b47a-000d3a31d12d"),
                column: "Type",
                value: 2);

            migrationBuilder.UpdateData(
                schema: "tariff",
                table: "TariffPlans",
                keyColumn: "Id",
                keyValue: new Guid("b6edf67d-5fdd-4246-b47a-259b4c4e49be"),
                column: "IsBaseTariff",
                value: true);

            migrationBuilder.UpdateData(
                schema: "tariff",
                table: "TariffPlans",
                keyColumn: "Id",
                keyValue: new Guid("b9491ab9-601b-406b-a689-a9b311773b73"),
                column: "IsBaseTariff",
                value: true);

            migrationBuilder.UpdateData(
                schema: "tariff",
                table: "TariffPlans",
                keyColumn: "Id",
                keyValue: new Guid("eac3a65f-7703-ed11-b47a-000d3a31d12d"),
                column: "Type",
                value: 2);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "IsBaseTariff",
                schema: "tariff",
                table: "TariffPlans");

            migrationBuilder.AlterColumn<int>(
                name: "Period",
                schema: "tariff",
                table: "TariffPlans",
                type: "int",
                nullable: false,
                defaultValue: 1,
                oldClrType: typeof(int),
                oldType: "int");

            migrationBuilder.AddColumn<int>(
                name: "Parent",
                schema: "tariff",
                table: "TariffPlans",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.UpdateData(
                schema: "tariff",
                table: "TariffPlans",
                keyColumn: "Id",
                keyValue: new Guid("6c2b90bc-7703-ed11-b47a-000d3a31d12d"),
                columns: new[] { "Parent", "Type" },
                values: new object[] { 2, 4 });

            migrationBuilder.UpdateData(
                schema: "tariff",
                table: "TariffPlans",
                keyColumn: "Id",
                keyValue: new Guid("eac3a65f-7703-ed11-b47a-000d3a31d12d"),
                columns: new[] { "Parent", "Type" },
                values: new object[] { 2, 3 });
        }
    }
}
