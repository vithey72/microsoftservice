﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace BNine.Persistence.Migrations
{
    public partial class add_card_replacement_paid_service : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData(
                schema: "tariff",
                table: "PaidUserServices",
                columns: new[] { "Id", "ChargeTypeId", "CreatedAt", "Description", "DurationInDays", "Name", "ServiceBusName" },
                values: new object[] { new Guid("9d4f7b80-6e25-45df-94fc-aef077d4cf3e"), 337, new DateTime(2022, 10, 12, 0, 0, 0, 0, DateTimeKind.Unspecified), "Payment for every replacement of physical card after the first one", 640000, "Physical card replacement fee", "card_replacement" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                schema: "tariff",
                table: "PaidUserServices",
                keyColumn: "Id",
                keyValue: new Guid("9d4f7b80-6e25-45df-94fc-aef077d4cf3e"));
        }
    }
}
