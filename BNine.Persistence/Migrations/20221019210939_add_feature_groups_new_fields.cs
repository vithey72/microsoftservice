﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace BNine.Persistence.Migrations
{
    public partial class add_feature_groups_new_fields : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_FeatureGroups_GroupId_FeatureId",
                schema: "features",
                table: "FeatureGroups");

            migrationBuilder.AddColumn<int>(
                name: "MaxAccountAgeHours",
                schema: "features",
                table: "FeatureGroups",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "MinAccountAgeHours",
                schema: "features",
                table: "FeatureGroups",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "MinRegistrationDate",
                schema: "features",
                table: "FeatureGroups",
                type: "datetime2",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_FeatureGroups_GroupId_FeatureId",
                schema: "features",
                table: "FeatureGroups",
                columns: new[] { "GroupId", "FeatureId" },
                unique: true,
                filter: "[MaxAccountAgeHours] IS NULL AND [MinAccountAgeHours] IS NULL AND [MinRegistrationDate] IS NULL");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_FeatureGroups_GroupId_FeatureId",
                schema: "features",
                table: "FeatureGroups");

            migrationBuilder.DropColumn(
                name: "MaxAccountAgeHours",
                schema: "features",
                table: "FeatureGroups");

            migrationBuilder.DropColumn(
                name: "MinAccountAgeHours",
                schema: "features",
                table: "FeatureGroups");

            migrationBuilder.DropColumn(
                name: "MinRegistrationDate",
                schema: "features",
                table: "FeatureGroups");

            migrationBuilder.CreateIndex(
                name: "IX_FeatureGroups_GroupId_FeatureId",
                schema: "features",
                table: "FeatureGroups",
                columns: new[] { "GroupId", "FeatureId" },
                unique: true);
        }
    }
}
