﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace BNine.Persistence.Migrations
{
    public partial class add_banners_external_link_follow_type : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "ExternalLinkType",
                schema: "settings",
                table: "MarketingBanners",
                type: "nvarchar(max)",
                nullable: false,
                defaultValue: "Default");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ExternalLinkType",
                schema: "settings",
                table: "MarketingBanners");
        }
    }
}
