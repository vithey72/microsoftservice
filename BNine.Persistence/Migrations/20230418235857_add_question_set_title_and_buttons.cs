﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace BNine.Persistence.Migrations
{
    public partial class add_question_set_title_and_buttons : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "AltButtonText",
                schema: "questionnaire",
                table: "QuestionSets",
                type: "nvarchar(50)",
                maxLength: 50,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Header",
                schema: "questionnaire",
                table: "QuestionSets",
                type: "nvarchar(100)",
                maxLength: 100,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "MainButtonText",
                schema: "questionnaire",
                table: "QuestionSets",
                type: "nvarchar(50)",
                maxLength: 50,
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "AltButtonText",
                schema: "questionnaire",
                table: "QuestionSets");

            migrationBuilder.DropColumn(
                name: "Header",
                schema: "questionnaire",
                table: "QuestionSets");

            migrationBuilder.DropColumn(
                name: "MainButtonText",
                schema: "questionnaire",
                table: "QuestionSets");
        }
    }
}
