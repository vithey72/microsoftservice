﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace BNine.Persistence.Migrations
{
    public partial class add_advance_boost_option_2_charge : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData(
                schema: "tariff",
                table: "PaidUserServices",
                columns: new[] { "Id", "ChargeTypeId", "CreatedAt", "Description", "DurationInDays", "Name", "ServiceBusName" },
                values: new object[] { new Guid("0a59264a-7b62-419f-8183-ffb80d54d44f"), 62, new DateTime(2023, 4, 19, 0, 0, 0, 0, DateTimeKind.Unspecified), "Increase your one time Advance amount", 14, "Advance Boost fee 15.99", "advance_boost_2" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                schema: "tariff",
                table: "PaidUserServices",
                keyColumn: "Id",
                keyValue: new Guid("0a59264a-7b62-419f-8183-ffb80d54d44f"));
        }
    }
}
