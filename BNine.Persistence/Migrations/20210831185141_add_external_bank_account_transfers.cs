﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace BNine.Persistence.Migrations
{
    public partial class add_external_bank_account_transfers : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "ExternalBankAccountTransfers",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false, defaultValueSql: "newsequentialid()"),
                    ExternalTransferId = table.Column<int>(nullable: false),
                    ExternalBankAccountId = table.Column<Guid>(nullable: false),
                    CreatedAt = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ExternalBankAccountTransfers", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ExternalBankAccountTransfers_ExternalBankAccounts_ExternalBankAccountId",
                        column: x => x.ExternalBankAccountId,
                        principalTable: "ExternalBankAccounts",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_ExternalBankAccountTransfers_ExternalBankAccountId",
                table: "ExternalBankAccountTransfers",
                column: "ExternalBankAccountId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "ExternalBankAccountTransfers");
        }
    }
}
