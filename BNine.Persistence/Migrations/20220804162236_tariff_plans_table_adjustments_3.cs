﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace BNine.Persistence.Migrations
{
    public partial class tariff_plans_table_adjustments_3 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "AdvertisementPictureUrl",
                schema: "tariff",
                table: "TariffPlans",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<decimal>(
                name: "PreDiscountPrice",
                schema: "tariff",
                table: "TariffPlans",
                type: "decimal(18,2)",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.UpdateData(
                schema: "tariff",
                table: "TariffPlans",
                keyColumn: "Id",
                keyValue: new Guid("6c2b90bc-7703-ed11-b47a-000d3a31d12d"),
                columns: new[] { "AdvertisementPictureUrl", "PictureUrl", "PreDiscountPrice" },
                values: new object[] { "https://b9devaccount.blob.core.windows.net/static-documents-container/TariffPlans/B9_Premium_3m_20disc_ad.png", "https://b9devaccount.blob.core.windows.net/static-documents-container/TariffPlans/B9_Premium_3m_20disc.png", 59.97m });

            migrationBuilder.UpdateData(
                schema: "tariff",
                table: "TariffPlans",
                keyColumn: "Id",
                keyValue: new Guid("b6edf67d-5fdd-4246-b47a-259b4c4e49be"),
                column: "PreDiscountPrice",
                value: 19.99m);

            migrationBuilder.UpdateData(
                schema: "tariff",
                table: "TariffPlans",
                keyColumn: "Id",
                keyValue: new Guid("b9491ab9-601b-406b-a689-a9b311773b73"),
                columns: new[] { "Features", "PreDiscountPrice" },
                values: new object[] { "[\r\n                  {\r\n                    \"Text\": \"B9 Visa® Debit Card with \\nup to 5% cashback\"\r\n                  },\r\n                  {\r\n                    \"Text\": \"Advance up to $300\"\r\n                  },\r\n                  {\r\n                    \"Text\": \"Get advances instantly\"\r\n                  },\r\n                  {\r\n                    \"Text\": \"Instant transfers to B9 Members\"\r\n                  }\r\n                ]", 9.99m });

            migrationBuilder.UpdateData(
                schema: "tariff",
                table: "TariffPlans",
                keyColumn: "Id",
                keyValue: new Guid("eac3a65f-7703-ed11-b47a-000d3a31d12d"),
                columns: new[] { "AdvertisementPictureUrl", "PictureUrl", "PreDiscountPrice" },
                values: new object[] { "https://b9devaccount.blob.core.windows.net/static-documents-container/TariffPlans/B9_Premium_3m_10disc_ad.png", "https://b9devaccount.blob.core.windows.net/static-documents-container/TariffPlans/B9_Premium_3m_10disc.png", 59.97m });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "AdvertisementPictureUrl",
                schema: "tariff",
                table: "TariffPlans");

            migrationBuilder.DropColumn(
                name: "PreDiscountPrice",
                schema: "tariff",
                table: "TariffPlans");

            migrationBuilder.UpdateData(
                schema: "tariff",
                table: "TariffPlans",
                keyColumn: "Id",
                keyValue: new Guid("6c2b90bc-7703-ed11-b47a-000d3a31d12d"),
                column: "PictureUrl",
                value: "https://b9devaccount.blob.core.windows.net/static-documents-container/TariffPlans/B9_Premium_Plan-20220330-141043.png");

            migrationBuilder.UpdateData(
                schema: "tariff",
                table: "TariffPlans",
                keyColumn: "Id",
                keyValue: new Guid("b9491ab9-601b-406b-a689-a9b311773b73"),
                column: "Features",
                value: "[\r\n                  {\r\n                    \"Text\": \"B9 Visa® Debit Card with \\n4 % Cashback\"\r\n                  },\r\n                  {\r\n                    \"Text\": \"Advance up to $300\"\r\n                  },\r\n                  {\r\n                    \"Text\": \"Get advances instantly\"\r\n                  },\r\n                  {\r\n                    \"Text\": \"Instant transfers to B9 Members\"\r\n                  }\r\n                ]");

            migrationBuilder.UpdateData(
                schema: "tariff",
                table: "TariffPlans",
                keyColumn: "Id",
                keyValue: new Guid("eac3a65f-7703-ed11-b47a-000d3a31d12d"),
                column: "PictureUrl",
                value: "https://b9devaccount.blob.core.windows.net/static-documents-container/TariffPlans/B9_Premium_Plan-20220330-141043.png");
        }
    }
}
