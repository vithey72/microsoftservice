﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace BNine.Persistence.Migrations
{
    public partial class AddRangeMccCodesUpdateSeedOfTransactionCategory : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "TransactionCategories",
                keyColumn: "Key",
                keyValue: "auto-parts");

            migrationBuilder.DeleteData(
                table: "TransactionCategories",
                keyColumn: "Key",
                keyValue: "auto-sales");

            migrationBuilder.DeleteData(
                table: "TransactionCategories",
                keyColumn: "Key",
                keyValue: "cable-services");

            migrationBuilder.DeleteData(
                table: "TransactionCategories",
                keyColumn: "Key",
                keyValue: "car-wash");

            migrationBuilder.DeleteData(
                table: "TransactionCategories",
                keyColumn: "Key",
                keyValue: "charitable-organizations");

            migrationBuilder.DeleteData(
                table: "TransactionCategories",
                keyColumn: "Key",
                keyValue: "cigarettes-vapes");

            migrationBuilder.DeleteData(
                table: "TransactionCategories",
                keyColumn: "Key",
                keyValue: "commuter-transportation");

            migrationBuilder.DeleteData(
                table: "TransactionCategories",
                keyColumn: "Key",
                keyValue: "computer-equipment");

            migrationBuilder.DeleteData(
                table: "TransactionCategories",
                keyColumn: "Key",
                keyValue: "computer-networks");

            migrationBuilder.DeleteData(
                table: "TransactionCategories",
                keyColumn: "Key",
                keyValue: "dating-services");

            migrationBuilder.DeleteData(
                table: "TransactionCategories",
                keyColumn: "Key",
                keyValue: "department-stores");

            migrationBuilder.DeleteData(
                table: "TransactionCategories",
                keyColumn: "Key",
                keyValue: "direct-marketing");

            migrationBuilder.DeleteData(
                table: "TransactionCategories",
                keyColumn: "Key",
                keyValue: "education");

            migrationBuilder.DeleteData(
                table: "TransactionCategories",
                keyColumn: "Key",
                keyValue: "electronics");

            migrationBuilder.DeleteData(
                table: "TransactionCategories",
                keyColumn: "Key",
                keyValue: "equipment-rental");

            migrationBuilder.DeleteData(
                table: "TransactionCategories",
                keyColumn: "Key",
                keyValue: "gift-shops");

            migrationBuilder.DeleteData(
                table: "TransactionCategories",
                keyColumn: "Key",
                keyValue: "government-services");

            migrationBuilder.DeleteData(
                table: "TransactionCategories",
                keyColumn: "Key",
                keyValue: "home-rentals");

            migrationBuilder.DeleteData(
                table: "TransactionCategories",
                keyColumn: "Key",
                keyValue: "home-supplies");

            migrationBuilder.DeleteData(
                table: "TransactionCategories",
                keyColumn: "Key",
                keyValue: "laundry-services");

            migrationBuilder.DeleteData(
                table: "TransactionCategories",
                keyColumn: "Key",
                keyValue: "misc-apparel");

            migrationBuilder.DeleteData(
                table: "TransactionCategories",
                keyColumn: "Key",
                keyValue: "parking");

            migrationBuilder.DeleteData(
                table: "TransactionCategories",
                keyColumn: "Key",
                keyValue: "personal-services");

            migrationBuilder.DeleteData(
                table: "TransactionCategories",
                keyColumn: "Key",
                keyValue: "professional-services");

            migrationBuilder.DeleteData(
                table: "TransactionCategories",
                keyColumn: "Key",
                keyValue: "record-shops");

            migrationBuilder.DeleteData(
                table: "TransactionCategories",
                keyColumn: "Key",
                keyValue: "shoe-stores");

            migrationBuilder.DeleteData(
                table: "TransactionCategories",
                keyColumn: "Key",
                keyValue: "software");

            migrationBuilder.DeleteData(
                table: "TransactionCategories",
                keyColumn: "Key",
                keyValue: "sporting-goods");

            migrationBuilder.DeleteData(
                table: "TransactionCategories",
                keyColumn: "Key",
                keyValue: "telecommunication-equipment");

            migrationBuilder.DeleteData(
                table: "TransactionCategories",
                keyColumn: "Key",
                keyValue: "thrift-stores");

            migrationBuilder.DeleteData(
                table: "TransactionCategories",
                keyColumn: "Key",
                keyValue: "used-autos");

            migrationBuilder.DeleteData(
                table: "TransactionCategories",
                keyColumn: "Key",
                keyValue: "variety-stores");

            migrationBuilder.AddColumn<int>(
                name: "MccRangeEnd",
                table: "TransactionCategories",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "MccRangeStart",
                table: "TransactionCategories",
                type: "int",
                nullable: true);

            migrationBuilder.UpdateData(
                table: "TransactionCategories",
                keyColumn: "Key",
                keyValue: "clothing-stores",
                column: "MccCodesJsonArrayString",
                value: "[5691,5651,5621,5699,5661]");

            migrationBuilder.UpdateData(
                table: "TransactionCategories",
                keyColumn: "Key",
                keyValue: "computer-services",
                columns: new[] { "MccCodesJsonArrayString", "Title" },
                values: new object[] { "[5734,4816,7372,5045]", "Computer Software and Services" });

            migrationBuilder.UpdateData(
                table: "TransactionCategories",
                keyColumn: "Key",
                keyValue: "general-merch",
                column: "MccCodesJsonArrayString",
                value: "[5399,5732,5311,5331,5941]");

            migrationBuilder.UpdateData(
                table: "TransactionCategories",
                keyColumn: "Key",
                keyValue: "home-stores",
                column: "MccCodesJsonArrayString",
                value: "[5719,5200]");

            migrationBuilder.UpdateData(
                table: "TransactionCategories",
                keyColumn: "Key",
                keyValue: "telecommunications",
                column: "MccCodesJsonArrayString",
                value: "[4814,4812,4899]");

            migrationBuilder.InsertData(
                table: "TransactionCategories",
                columns: new[] { "Key", "Color", "HintButtonText", "HintIconUrl", "HintSubTitle", "HintTitle", "IconUrl", "MccCodesJsonArrayString", "MccRangeEnd", "MccRangeStart", "ParentId", "Title", "TransferDisplayTypeIdsJsonArrayString" },
                values: new object[,]
                {
                    { "airline-tickets", null, null, null, null, null, null, "[4304,4415,4418,4511,4582]", 3350, 3000, "card-transactions", "Airline Tickets", null },
                    { "auto-services", null, null, null, null, null, null, "[5511,5521,5531,5532,5533,7542]", null, null, "card-transactions", "Auto Services", null }
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "TransactionCategories",
                keyColumn: "Key",
                keyValue: "airline-tickets");

            migrationBuilder.DeleteData(
                table: "TransactionCategories",
                keyColumn: "Key",
                keyValue: "auto-services");

            migrationBuilder.DropColumn(
                name: "MccRangeEnd",
                table: "TransactionCategories");

            migrationBuilder.DropColumn(
                name: "MccRangeStart",
                table: "TransactionCategories");

            migrationBuilder.UpdateData(
                table: "TransactionCategories",
                keyColumn: "Key",
                keyValue: "clothing-stores",
                column: "MccCodesJsonArrayString",
                value: "[5691,5621,5651]");

            migrationBuilder.UpdateData(
                table: "TransactionCategories",
                keyColumn: "Key",
                keyValue: "computer-services",
                columns: new[] { "MccCodesJsonArrayString", "Title" },
                values: new object[] { "[7372]", "Computer Services" });

            migrationBuilder.UpdateData(
                table: "TransactionCategories",
                keyColumn: "Key",
                keyValue: "general-merch",
                column: "MccCodesJsonArrayString",
                value: "[5399]");

            migrationBuilder.UpdateData(
                table: "TransactionCategories",
                keyColumn: "Key",
                keyValue: "home-stores",
                column: "MccCodesJsonArrayString",
                value: "[5719]");

            migrationBuilder.UpdateData(
                table: "TransactionCategories",
                keyColumn: "Key",
                keyValue: "telecommunications",
                column: "MccCodesJsonArrayString",
                value: "[4814]");

            migrationBuilder.InsertData(
                table: "TransactionCategories",
                columns: new[] { "Key", "Color", "HintButtonText", "HintIconUrl", "HintSubTitle", "HintTitle", "IconUrl", "MccCodesJsonArrayString", "ParentId", "Title", "TransferDisplayTypeIdsJsonArrayString" },
                values: new object[,]
                {
                    { "auto-parts", null, null, null, null, null, null, "[5533]", "card-transactions", "Auto Parts", null },
                    { "auto-sales", null, null, null, null, null, null, "[5511]", "card-transactions", "Auto Sales", null },
                    { "cable-services", null, null, null, null, null, null, "[4899]", "card-transactions", "Cable Services", null },
                    { "car-wash", null, null, null, null, null, null, "[7542]", "card-transactions", "Car Wash", null },
                    { "charitable-organizations", null, null, null, null, null, null, "[8398]", "card-transactions", "Charitable Organizations", null },
                    { "cigarettes-vapes", null, null, null, null, null, null, "[5993]", "card-transactions", "Cigarettes/Vapes", null },
                    { "commuter-transportation", null, null, null, null, null, null, "[4111]", "card-transactions", "Commuter Transportation", null },
                    { "computer-equipment", null, null, null, null, null, null, "[5045]", "card-transactions", "Computer Equipment", null },
                    { "computer-networks", null, null, null, null, null, null, "[4816]", "card-transactions", "Computer Networks", null },
                    { "dating-services", null, null, null, null, null, null, "[7273]", "card-transactions", "Dating Services", null },
                    { "department-stores", null, null, null, null, null, null, "[5311]", "card-transactions", "Department Stores", null },
                    { "direct-marketing", null, null, null, null, null, null, "[5967]", "card-transactions", "Variety Stores", null },
                    { "education", null, null, null, null, null, null, "[8299]", "card-transactions", "Education", null },
                    { "electronics", null, null, null, null, null, null, "[5732]", "card-transactions", "Electronics", null },
                    { "equipment-rental", null, null, null, null, null, null, "[7394]", "card-transactions", "Equipment Rental", null },
                    { "gift-shops", null, null, null, null, null, null, "[5947]", "card-transactions", "Gift Shops", null },
                    { "government-services", null, null, null, null, null, null, "[9399]", "card-transactions", "Government Services", null },
                    { "home-rentals", null, null, null, null, null, null, "[6513]", "card-transactions", "Home Rentals", null },
                    { "home-supplies", null, null, null, null, null, null, "[5200]", "card-transactions", "Home Supplies", null },
                    { "laundry-services", null, null, null, null, null, null, "[7211]", "card-transactions", "Laundry Services", null },
                    { "misc-apparel", null, null, null, null, null, null, "[5699]", "card-transactions", "Misc. Apparel", null },
                    { "parking", null, null, null, null, null, null, "[7523]", "card-transactions", "Parking", null },
                    { "personal-services", null, null, null, null, null, null, "[7299]", "card-transactions", "Personal Services", null },
                    { "professional-services", null, null, null, null, null, null, "[8999]", null, "Professional Services", null },
                    { "record-shops", null, null, null, null, null, null, "[5735]", "card-transactions", "Record Shops", null },
                    { "shoe-stores", null, null, null, null, null, null, "[5661]", "card-transactions", "Shoe Stores", null },
                    { "software", null, null, null, null, null, null, "[5734]", "card-transactions", "Software", null },
                    { "sporting-goods", null, null, null, null, null, null, "[5941]", "card-transactions", "Sporting Goods", null },
                    { "telecommunication-equipment", null, null, null, null, null, null, "[4812]", "card-transactions", "Telecommunication Equipment", null },
                    { "thrift-stores", null, null, null, null, null, null, "[5931]", "card-transactions", "Thrift Stores", null },
                    { "used-autos", null, null, null, null, null, null, "[5521]", "card-transactions", "Used Autos", null },
                    { "variety-stores", null, null, null, null, null, null, "[5331]", "card-transactions", "Variety Stores", null }
                });
        }
    }
}
