﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace BNine.Persistence.Migrations
{
    public partial class update_adviance_tariff_plan_images_2 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                schema: "tariff",
                table: "TariffPlans",
                keyColumn: "Id",
                keyValue: new Guid("b6edf67d-5fdd-4246-b47a-259b4c4e49be"),
                columns: new[] { "AdvertisementPictureUrl", "PictureUrl" },
                values: new object[] { "https://b9devaccount.blob.core.windows.net/static-documents-container/TariffPlans/B9_Premium_1m.png", "https://b9devaccount.blob.core.windows.net/static-documents-container/TariffPlans/B9_Premium_1m.png" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                schema: "tariff",
                table: "TariffPlans",
                keyColumn: "Id",
                keyValue: new Guid("b6edf67d-5fdd-4246-b47a-259b4c4e49be"),
                columns: new[] { "AdvertisementPictureUrl", "PictureUrl" },
                values: new object[] { "https://b9devaccount.blob.core.windows.net/static-documents-container/TariffPlans/B9_Premium_Plan-20220330-141043.png", "https://b9devaccount.blob.core.windows.net/static-documents-container/TariffPlans/B9_Premium_Plan-20220330-141043.png" });
        }
    }
}
