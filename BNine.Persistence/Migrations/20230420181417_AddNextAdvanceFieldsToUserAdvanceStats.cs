﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace BNine.Persistence.Migrations
{
    public partial class AddNextAdvanceFieldsToUserAdvanceStats : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<decimal>(
                name: "MaximumBasicAdvanceAmount",
                schema: "user",
                table: "UserSyncedStats",
                type: "decimal(18,2)",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<decimal>(
                name: "NextBasicAdvanceAmount",
                schema: "user",
                table: "UserSyncedStats",
                type: "decimal(18,2)",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<decimal>(
                name: "NextPremiumAdvanceAmount",
                schema: "user",
                table: "UserSyncedStats",
                type: "decimal(18,2)",
                nullable: false,
                defaultValue: 0m);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "MaximumBasicAdvanceAmount",
                schema: "user",
                table: "UserSyncedStats");

            migrationBuilder.DropColumn(
                name: "NextBasicAdvanceAmount",
                schema: "user",
                table: "UserSyncedStats");

            migrationBuilder.DropColumn(
                name: "NextPremiumAdvanceAmount",
                schema: "user",
                table: "UserSyncedStats");
        }
    }
}
