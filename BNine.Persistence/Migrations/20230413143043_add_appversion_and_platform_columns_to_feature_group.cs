﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace BNine.Persistence.Migrations
{
    public partial class add_appversion_and_platform_columns_to_feature_group : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "AppVersion",
                schema: "features",
                table: "FeatureGroups",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "MobilePlatform",
                schema: "features",
                table: "FeatureGroups",
                type: "int",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "AppVersion",
                schema: "features",
                table: "FeatureGroups");

            migrationBuilder.DropColumn(
                name: "MobilePlatform",
                schema: "features",
                table: "FeatureGroups");
        }
    }
}
