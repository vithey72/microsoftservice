﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace BNine.Persistence.Migrations
{
    public partial class advance_stats_tables_adjustment : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<decimal>(
                name: "ExtensionPrice",
                schema: "user",
                table: "AdvanceSyncedStats",
                type: "decimal(18,2)",
                nullable: false,
                oldClrType: typeof(int),
                oldType: "int");

            migrationBuilder.AlterColumn<decimal>(
                name: "ExtensionLimit",
                schema: "user",
                table: "AdvanceSyncedStats",
                type: "decimal(18,2)",
                nullable: false,
                oldClrType: typeof(int),
                oldType: "int");

            migrationBuilder.AlterColumn<DateTime>(
                name: "ExtensionExpiresAt",
                schema: "user",
                table: "AdvanceSyncedStats",
                type: "datetime2",
                nullable: true,
                oldClrType: typeof(DateTime),
                oldType: "datetime2");

            migrationBuilder.AlterColumn<decimal>(
                name: "AvailableLimit",
                schema: "user",
                table: "AdvanceSyncedStats",
                type: "decimal(18,2)",
                nullable: false,
                oldClrType: typeof(int),
                oldType: "int");

            migrationBuilder.CreateIndex(
                name: "IX_UsedPaidUserServices_UserId",
                schema: "tariff",
                table: "UsedPaidUserServices",
                column: "UserId");

            migrationBuilder.AddForeignKey(
                name: "FK_UsedPaidUserServices_Users_UserId",
                schema: "tariff",
                table: "UsedPaidUserServices",
                column: "UserId",
                principalSchema: "user",
                principalTable: "Users",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_UsedPaidUserServices_Users_UserId",
                schema: "tariff",
                table: "UsedPaidUserServices");

            migrationBuilder.DropIndex(
                name: "IX_UsedPaidUserServices_UserId",
                schema: "tariff",
                table: "UsedPaidUserServices");

            migrationBuilder.AlterColumn<int>(
                name: "ExtensionPrice",
                schema: "user",
                table: "AdvanceSyncedStats",
                type: "int",
                nullable: false,
                oldClrType: typeof(decimal),
                oldType: "decimal(18,2)");

            migrationBuilder.AlterColumn<int>(
                name: "ExtensionLimit",
                schema: "user",
                table: "AdvanceSyncedStats",
                type: "int",
                nullable: false,
                oldClrType: typeof(decimal),
                oldType: "decimal(18,2)");

            migrationBuilder.AlterColumn<DateTime>(
                name: "ExtensionExpiresAt",
                schema: "user",
                table: "AdvanceSyncedStats",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "AvailableLimit",
                schema: "user",
                table: "AdvanceSyncedStats",
                type: "int",
                nullable: false,
                oldClrType: typeof(decimal),
                oldType: "decimal(18,2)");
        }
    }
}
