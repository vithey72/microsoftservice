﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace BNine.Persistence.Migrations
{
    public partial class changeTransactionCategoryTitle : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "TransactionCategories",
                keyColumn: "Key",
                keyValue: "internal-transfer-sent",
                column: "Title",
                value: "Internal Transfer Sent");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "TransactionCategories",
                keyColumn: "Key",
                keyValue: "internal-transfer-sent",
                column: "Title",
                value: "Internal");
        }
    }
}
