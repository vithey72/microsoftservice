﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace BNine.Persistence.Migrations
{
    public partial class insert_paid_service_advance_tariff_on_onboarding : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData(
                schema: "tariff",
                table: "PaidUserServices",
                columns: new[] { "Id", "ChargeTypeId", "CreatedAt", "Description", "DurationInDays", "Name", "ServiceBusName" },
                values: new object[] { new Guid("c1d68b39-e503-4b3d-924c-0318ace1bb46"), 338, new DateTime(2022, 10, 25, 0, 0, 0, 0, DateTimeKind.Unspecified), "Purchase of Advance tariff discounted by 1$", 30, "Monthly Membership Default OT", "advance_tariff_on_onboarding" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                schema: "tariff",
                table: "PaidUserServices",
                keyColumn: "Id",
                keyValue: new Guid("c1d68b39-e503-4b3d-924c-0318ace1bb46"));
        }
    }
}
