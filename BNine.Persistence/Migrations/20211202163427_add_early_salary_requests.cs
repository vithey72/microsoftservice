﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace BNine.Persistence.Migrations
{
    public partial class add_early_salary_requests : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "EarlySalaryManualRequests",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false, defaultValueSql: "newsequentialid()"),
                    CreatedAt = table.Column<DateTime>(nullable: false),
                    UserId = table.Column<Guid>(nullable: false),
                    AmountAllocation = table.Column<decimal>(type: "decimal(18, 2)", nullable: true),
                    PercentAllocation = table.Column<decimal>(type: "decimal(18, 2)", nullable: true),
                    Email = table.Column<string>(maxLength: 512, nullable: false),
                    EmployerName = table.Column<string>(maxLength: 512, nullable: true),
                    EmployerContactPerson = table.Column<string>(maxLength: 512, nullable: true),
                    EmployerEmail = table.Column<string>(maxLength: 512, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_EarlySalaryManualRequests", x => x.Id);
                    table.ForeignKey(
                        name: "FK_EarlySalaryManualRequests_Users_UserId",
                        column: x => x.UserId,
                        principalSchema: "user",
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_EarlySalaryManualRequests_UserId",
                table: "EarlySalaryManualRequests",
                column: "UserId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "EarlySalaryManualRequests");
        }
    }
}
