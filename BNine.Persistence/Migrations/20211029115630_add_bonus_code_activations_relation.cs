﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace BNine.Persistence.Migrations
{
    public partial class add_bonus_code_activations_relation : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "BonusCodeActivations",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false, defaultValueSql: "newsequentialid()"),
                    BonusCodeId = table.Column<Guid>(nullable: false),
                    ReceiverId = table.Column<Guid>(nullable: false),
                    CreatedAt = table.Column<DateTime>(nullable: false),
                    UpdatedAt = table.Column<DateTime>(nullable: true),
                    ReceiverTransferSuccess = table.Column<bool>(nullable: false),
                    SenderTransferSuccess = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BonusCodeActivations", x => x.Id);
                    table.ForeignKey(
                        name: "FK_BonusCodeActivations_BonusCodes_BonusCodeId",
                        column: x => x.BonusCodeId,
                        principalTable: "BonusCodes",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_BonusCodeActivations_Users_ReceiverId",
                        column: x => x.ReceiverId,
                        principalSchema: "user",
                        principalTable: "Users",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateIndex(
                name: "IX_BonusCodeActivations_BonusCodeId",
                table: "BonusCodeActivations",
                column: "BonusCodeId");

            migrationBuilder.CreateIndex(
                name: "IX_BonusCodeActivations_CreatedAt",
                table: "BonusCodeActivations",
                column: "CreatedAt");

            migrationBuilder.CreateIndex(
                name: "IX_BonusCodeActivations_ReceiverId",
                table: "BonusCodeActivations",
                column: "ReceiverId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_BonusCodeActivations_UpdatedAt",
                table: "BonusCodeActivations",
                column: "UpdatedAt");

            migrationBuilder.Sql(
                @"
                insert into dbo.BonusCodeActivations
                (
                     BonusCodeId
                    ,ReceiverId
                    ,CreatedAt
                    ,ReceiverTransferSuccess
                    ,SenderTransferSuccess
                )
                select
                     Id
                    ,ReceiverId
                    ,COALESCE(UpdatedAt, CreationDate)
                    ,1
                    ,1
                from dbo.BonusCodes bc
                where bc.IsAlreadyUsed = 1
                ;");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "BonusCodeActivations");
        }
    }
}
