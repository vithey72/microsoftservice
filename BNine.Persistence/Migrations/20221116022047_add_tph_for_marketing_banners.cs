﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace BNine.Persistence.Migrations
{
    public partial class add_tph_for_marketing_banners : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                schema: "settings",
                table: "MarketingBanners",
                keyColumn: "Id",
                keyValue: new Guid("b2aa4f13-dc17-4d13-a613-0b6088da5826"));

            migrationBuilder.DeleteData(
                schema: "settings",
                table: "MarketingBanners",
                keyColumn: "Id",
                keyValue: new Guid("f8fc9612-6490-49a3-a87d-540532bc2783"));

            migrationBuilder.AlterColumn<bool>(
                name: "HasCloseButton",
                schema: "settings",
                table: "MarketingBanners",
                type: "bit",
                nullable: true,
                defaultValue: true,
                oldClrType: typeof(bool),
                oldType: "bit",
                oldDefaultValue: true);

            migrationBuilder.AddColumn<string>(
                name: "AppScreen",
                schema: "settings",
                table: "MarketingBanners",
                type: "nvarchar(50)",
                maxLength: 50,
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "CannotBeClosed",
                schema: "settings",
                table: "MarketingBanners",
                type: "bit",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "EntityType",
                schema: "settings",
                table: "MarketingBanners",
                type: "nvarchar(max)",
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<string>(
                name: "PictureUrl",
                schema: "settings",
                table: "MarketingBanners",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.InsertData(
                schema: "settings",
                table: "MarketingBanners",
                columns: new[] { "Id", "AppScreen", "ButtonDeeplink", "ButtonText", "CannotBeClosed", "EntityType", "ExternalLink", "IsEnabled", "MinUserRegistrationDate", "Name", "PictureUrl", "ShowCooldown", "ShowCooldownProgressive", "ShowFrom", "ShowTil", "Subtitle", "Title" },
                values: new object[] { new Guid("8b8c0da8-7613-4975-a03a-e3158643633a"), "Login", null, "OK", false, "infopopup", null, false, null, "sample", null, null, null, null, null, "lorem", "Sample infopopup" });

            migrationBuilder.InsertData(
                schema: "settings",
                table: "MarketingBanners",
                columns: new[] { "Id", "ButtonDeeplink", "ButtonText", "EntityType", "ExternalLink", "HasCloseButton", "IsEnabled", "MinUserRegistrationDate", "Name", "ShowCooldown", "ShowCooldownProgressive", "ShowFrom", "ShowTil", "Subtitle", "Title" },
                values: new object[] { new Guid("b2aa4f13-dc17-4d13-a613-0b6088da5826"), "cashback-program", "ACTIVATE CASHBACK NOW", "topbanner", null, true, true, null, "CashbackBanner", null, null, new DateTime(2022, 7, 25, 6, 0, 0, 0, DateTimeKind.Unspecified), null, null, "Pay and earn with B9 cards!" });

            migrationBuilder.InsertData(
                schema: "settings",
                table: "MarketingBanners",
                columns: new[] { "Id", "ButtonDeeplink", "ButtonText", "EntityType", "ExternalLink", "HasCloseButton", "IsEnabled", "MinUserRegistrationDate", "Name", "ShowCooldown", "ShowCooldownProgressive", "ShowFrom", "ShowTil", "Subtitle", "Title" },
                values: new object[] { new Guid("f8fc9612-6490-49a3-a87d-540532bc2783"), "premium-tariff-plan", "GET MY DISCOUNT", "topbanner", null, true, true, null, "PremiumTariff3MonthsDiscount", null, "[\"1.00:00:00\",\"3.00:00:00\",\"7.00:00:00\"]", new DateTime(2022, 8, 17, 6, 0, 0, 0, DateTimeKind.Unspecified), null, null, "Discounted Premium Plan" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                schema: "settings",
                table: "MarketingBanners",
                keyColumn: "Id",
                keyValue: new Guid("8b8c0da8-7613-4975-a03a-e3158643633a"));

            migrationBuilder.DropColumn(
                name: "AppScreen",
                schema: "settings",
                table: "MarketingBanners");

            migrationBuilder.DropColumn(
                name: "CannotBeClosed",
                schema: "settings",
                table: "MarketingBanners");

            migrationBuilder.DropColumn(
                name: "EntityType",
                schema: "settings",
                table: "MarketingBanners");

            migrationBuilder.DropColumn(
                name: "PictureUrl",
                schema: "settings",
                table: "MarketingBanners");

            migrationBuilder.AlterColumn<bool>(
                name: "HasCloseButton",
                schema: "settings",
                table: "MarketingBanners",
                type: "bit",
                nullable: false,
                defaultValue: true,
                oldClrType: typeof(bool),
                oldType: "bit",
                oldNullable: true,
                oldDefaultValue: true);
        }
    }
}
