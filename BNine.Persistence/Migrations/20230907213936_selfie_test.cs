﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace BNine.Persistence.Migrations
{
    public partial class selfie_test : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "StatusDetailed",
                schema: "user",
                table: "Users",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "StepDetailed",
                schema: "logs",
                table: "OnboardingEvents",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.Sql("update [user].Users set StatusDetailed = Status * 10;");
            migrationBuilder.Sql("update [logs].OnboardingEvents set StepDetailed = Step * 10;");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "StatusDetailed",
                schema: "user",
                table: "Users");

            migrationBuilder.DropColumn(
                name: "StepDetailed",
                schema: "logs",
                table: "OnboardingEvents");
        }
    }
}
