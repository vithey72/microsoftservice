﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace BNine.Persistence.Migrations
{
    public partial class truv_task_status_accounting : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "PendingRefreshTaskId",
                table: "TruvAccount",
                newName: "PendingTaskId");

            migrationBuilder.AlterColumn<string>(
                name: "LinkId",
                table: "TruvAccount",
                type: "nvarchar(450)",
                nullable: true,
                oldClrType: typeof(string),
                oldType: "nvarchar(max)",
                oldNullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "IsRefreshTaskScheduled",
                table: "TruvAccount",
                type: "bit",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<DateTime>(
                name: "PendingTaskStatusUpdatedAt",
                table: "TruvAccount",
                type: "datetime2",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_TruvAccount_LinkId",
                table: "TruvAccount",
                column: "LinkId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_TruvAccount_LinkId",
                table: "TruvAccount");

            migrationBuilder.DropColumn(
                name: "IsRefreshTaskScheduled",
                table: "TruvAccount");

            migrationBuilder.DropColumn(
                name: "PendingTaskStatusUpdatedAt",
                table: "TruvAccount");

            migrationBuilder.RenameColumn(
                name: "PendingTaskId",
                table: "TruvAccount",
                newName: "PendingRefreshTaskId");

            migrationBuilder.AlterColumn<string>(
                name: "LinkId",
                table: "TruvAccount",
                type: "nvarchar(max)",
                nullable: true,
                oldClrType: typeof(string),
                oldType: "nvarchar(450)",
                oldNullable: true);
        }
    }
}
