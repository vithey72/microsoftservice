﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace BNine.Persistence.Migrations
{
    public partial class update_truv_account_index : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_TruvAccount_LinkId",
                table: "TruvAccount");

            migrationBuilder.AlterColumn<string>(
                name: "LinkId",
                table: "TruvAccount",
                type: "nvarchar(max)",
                nullable: true,
                oldClrType: typeof(string),
                oldType: "nvarchar(450)",
                oldNullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_TruvAccount_TruvUserId",
                table: "TruvAccount",
                column: "TruvUserId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_TruvAccount_TruvUserId",
                table: "TruvAccount");

            migrationBuilder.AlterColumn<string>(
                name: "LinkId",
                table: "TruvAccount",
                type: "nvarchar(450)",
                nullable: true,
                oldClrType: typeof(string),
                oldType: "nvarchar(max)",
                oldNullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_TruvAccount_LinkId",
                table: "TruvAccount",
                column: "LinkId");
        }
    }
}
