﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace BNine.Persistence.Migrations
{
    public partial class AddIconToTariffPlan : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "IconUrl",
                schema: "tariff",
                table: "TariffPlans",
                type: "nvarchar(max)",
                nullable: false,
                defaultValue: "");

            migrationBuilder.UpdateData(
                schema: "tariff",
                table: "TariffPlans",
                keyColumn: "Id",
                keyValue: new Guid("b6edf67d-5fdd-4246-b47a-259b4c4e49be"),
                columns: new[] { "CreatedAt", "Features", "IconUrl" },
                values: new object[] { new DateTime(2022, 4, 14, 18, 18, 13, 384, DateTimeKind.Local).AddTicks(4074), "[\r\n                    \"B9 Advance Plan\",\r\n                    \"B9 Advance of up to 100%\\n of your Paycheck\",\r\n                    \"Credit Report\",\r\n                    \"Credit Bureau Score\",\r\n                    \"Credit Score Simulator\"\r\n                ]", "https://b9devaccount.blob.core.windows.net/static-documents-container/TariffPlans/B9_Premium_icon.png" });

            migrationBuilder.UpdateData(
                schema: "tariff",
                table: "TariffPlans",
                keyColumn: "Id",
                keyValue: new Guid("b9491ab9-601b-406b-a689-a9b311773b73"),
                columns: new[] { "CreatedAt", "Features", "IconUrl" },
                values: new object[] { new DateTime(2022, 4, 14, 18, 18, 13, 384, DateTimeKind.Local).AddTicks(4063), "[\r\n                    \"B9 Visa® Debit Card with \\n4 % Cashback\",\r\n                    \"No Fee instant B9 Advances \\n(up to $300)\",\r\n                    \"No Fee ACH transfers\",\r\n                    \"No Fee instant transfers to B9 Members\"\r\n                ]", "https://b9devaccount.blob.core.windows.net/static-documents-container/TariffPlans/B9_Advance_icon.png" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "IconUrl",
                schema: "tariff",
                table: "TariffPlans");

            migrationBuilder.UpdateData(
                schema: "tariff",
                table: "TariffPlans",
                keyColumn: "Id",
                keyValue: new Guid("b6edf67d-5fdd-4246-b47a-259b4c4e49be"),
                columns: new[] { "CreatedAt", "Features" },
                values: new object[] { new DateTime(2022, 4, 6, 13, 42, 2, 423, DateTimeKind.Local).AddTicks(9887), "[\r\n                    \"B9 Advance Plan\",\r\n                    \"B9 Advance of up to 100% of your Paycheck\",\r\n                    \"Credit Report\",\r\n                    \"Credit Bureau Score\",\r\n                    \"Credit Score Simulator\"\r\n                ]" });

            migrationBuilder.UpdateData(
                schema: "tariff",
                table: "TariffPlans",
                keyColumn: "Id",
                keyValue: new Guid("b9491ab9-601b-406b-a689-a9b311773b73"),
                columns: new[] { "CreatedAt", "Features" },
                values: new object[] { new DateTime(2022, 4, 6, 13, 42, 2, 423, DateTimeKind.Local).AddTicks(9872), "[\r\n                    \"B9 Visa® Debit Card with 4 % Cashback\",\r\n                    \"No Fee instant B9 Advances (up to $300)\",\r\n                    \"No Fee ACH transfers\",\r\n                    \"No Fee instant transfers to\",\r\n                    \"B9 Members\"\r\n                ]" });
        }
    }
}
