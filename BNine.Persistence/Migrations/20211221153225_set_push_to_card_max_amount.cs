﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace BNine.Persistence.Migrations
{
    public partial class set_push_to_card_max_amount : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<decimal>(
                name: "PushToExternalCardMaxAmount",
                schema: "settings",
                table: "SavingAccountSettings",
                type: "decimal(18,2)",
                nullable: false,
                defaultValue: 500m);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "PushToExternalCardMaxAmount",
                schema: "settings",
                table: "SavingAccountSettings");
        }
    }
}
