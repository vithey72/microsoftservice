﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace BNine.Persistence.Migrations
{
    public partial class update_tariff_plan_images : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                schema: "tariff",
                table: "TariffPlans",
                keyColumn: "Id",
                keyValue: new Guid("eac3a65f-7703-ed11-b47a-000d3a31d12d"),
                columns: new[] { "AdvertisementPictureUrl", "PictureUrl" },
                values: new object[] { "https://b9devaccount.blob.core.windows.net/static-documents-container/TariffPlans/B9_Premium_3m_1799_ad.png", "https://b9devaccount.blob.core.windows.net/static-documents-container/TariffPlans/B9_Premium_3m_1799.png" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                schema: "tariff",
                table: "TariffPlans",
                keyColumn: "Id",
                keyValue: new Guid("eac3a65f-7703-ed11-b47a-000d3a31d12d"),
                columns: new[] { "AdvertisementPictureUrl", "PictureUrl" },
                values: new object[] { "https://b9devaccount.blob.core.windows.net/static-documents-container/TariffPlans/B9_Premium_3m_10disc_ad.png", "https://b9devaccount.blob.core.windows.net/static-documents-container/TariffPlans/B9_Premium_3m_10disc.png" });
        }
    }
}
