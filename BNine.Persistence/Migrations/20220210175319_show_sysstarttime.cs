﻿using System.Diagnostics.CodeAnalysis;
using Microsoft.EntityFrameworkCore.Migrations;

namespace BNine.Persistence.Migrations
{
    [SuppressMessage("Naming styles", "IDE1006")]
    public partial class show_sysstarttime : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(
                @"BEGIN TRANSACTION
                    ALTER TABLE [user].Addresses  ALTER COLUMN [SysStartTime] DROP HIDDEN;
                    ALTER TABLE [user].EarlySalarySettings  ALTER COLUMN [SysStartTime] DROP HIDDEN;
                    ALTER TABLE [user].UserSettings  ALTER COLUMN [SysStartTime] DROP HIDDEN;
                    ALTER TABLE [user].ClientLoanSettings  ALTER COLUMN [SysStartTime] DROP HIDDEN;
                    ALTER TABLE [user].ClientRatings  ALTER COLUMN [SysStartTime] DROP HIDDEN;
                    ALTER TABLE [user].Documents  ALTER COLUMN [SysStartTime] DROP HIDDEN;
                    ALTER TABLE [user].Identities  ALTER COLUMN [SysStartTime] DROP HIDDEN;
                COMMIT TRANSACTION
                ");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(
                @"BEGIN TRANSACTION
                    ALTER TABLE [user].Addresses  ALTER COLUMN [SysStartTime] ADD HIDDEN;
                    ALTER TABLE [user].EarlySalarySettings  ALTER COLUMN [SysStartTime] ADD HIDDEN;
                    ALTER TABLE [user].UserSettings  ALTER COLUMN [SysStartTime] ADD HIDDEN;
                    ALTER TABLE [user].ClientLoanSettings  ALTER COLUMN [SysStartTime] ADD HIDDEN;
                    ALTER TABLE [user].ClientRatings  ALTER COLUMN [SysStartTime] ADD HIDDEN;
                    ALTER TABLE [user].Documents  ALTER COLUMN [SysStartTime] ADD HIDDEN;
                    ALTER TABLE [user].Identities  ALTER COLUMN [SysStartTime] ADD HIDDEN;
                COMMIT TRANSACTION
                ");
        }
    }
}
