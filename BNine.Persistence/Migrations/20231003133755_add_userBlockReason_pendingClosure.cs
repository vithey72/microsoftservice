﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace BNine.Persistence.Migrations
{
    public partial class add_userBlockReason_pendingClosure : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData(
                schema: "user",
                table: "UserBlockReasons",
                columns: new[] { "Id", "Order", "Value" },
                values: new object[] { new Guid("2859a701-ad03-4421-8f75-36095a412490"), 5, "Pending account closure" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                schema: "user",
                table: "UserBlockReasons",
                keyColumn: "Id",
                keyValue: new Guid("2859a701-ad03-4421-8f75-36095a412490"));
        }
    }
}
