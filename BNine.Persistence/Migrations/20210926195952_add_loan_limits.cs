﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace BNine.Persistence.Migrations
{
    public partial class add_loan_limits : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.EnsureSchema(
                name: "settings");

            migrationBuilder.CreateTable(
                name: "LoanCalculatedLimits",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false, defaultValueSql: "newsequentialid()"),
                    CreatedAt = table.Column<DateTime>(nullable: false),
                    DeletedAt = table.Column<DateTime>(nullable: true),
                    Amount = table.Column<decimal>(type: "decimal(18,2)", nullable: false),
                    UserId = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_LoanCalculatedLimits", x => x.Id);
                    table.ForeignKey(
                        name: "FK_LoanCalculatedLimits_Users_UserId",
                        column: x => x.UserId,
                        principalSchema: "user",
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "ACHCreditTransfers",
                schema: "bank",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false, defaultValueSql: "newsequentialid()"),
                    ExternalId = table.Column<int>(nullable: false),
                    Reference = table.Column<string>(nullable: true),
                    Amount = table.Column<decimal>(type: "decimal(18,8)", nullable: false),
                    UserId = table.Column<Guid>(nullable: false),
                    CreatedAt = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ACHCreditTransfers", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ACHCreditTransfers_Users_UserId",
                        column: x => x.UserId,
                        principalSchema: "user",
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "LoanSettings",
                schema: "settings",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false, defaultValueSql: "newsequentialid()"),
                    ACHPrincipalKeywords = table.Column<string>(nullable: true),
                    Limit = table.Column<decimal>(type: "decimal(18,2)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_LoanSettings", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "ClientRatings",
                schema: "user",
                columns: table => new
                {
                    UserId = table.Column<Guid>(nullable: false),
                    CreditRating = table.Column<decimal>(type: "decimal(18, 6)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ClientRatings", x => x.UserId);
                    table.ForeignKey(
                        name: "FK_ClientRatings_Users_UserId",
                        column: x => x.UserId,
                        principalSchema: "user",
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_LoanCalculatedLimits_UserId",
                table: "LoanCalculatedLimits",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_ACHCreditTransfers_UserId",
                schema: "bank",
                table: "ACHCreditTransfers",
                column: "UserId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "LoanCalculatedLimits");

            migrationBuilder.DropTable(
                name: "ACHCreditTransfers",
                schema: "bank");

            migrationBuilder.DropTable(
                name: "LoanSettings",
                schema: "settings");

            migrationBuilder.DropTable(
                name: "ClientRatings",
                schema: "user");
        }
    }
}
