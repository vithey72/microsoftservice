﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace BNine.Persistence.Migrations
{
    public partial class add_user_questionnaire : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.EnsureSchema(
                name: "questionnaire");

            migrationBuilder.CreateTable(
                name: "Questions",
                schema: "questionnaire",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false, defaultValueSql: "newsequentialid()"),
                    Title = table.Column<string>(maxLength: 150, nullable: false),
                    Tag = table.Column<string>(maxLength: 50, nullable: true),
                    QuestionType = table.Column<int>(nullable: false),
                    Order = table.Column<int>(nullable: false),
                    OpenResponseCharacterLimit = table.Column<int>(nullable: false),
                    DeletedAt = table.Column<DateTime>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Questions", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "QuestionSets",
                schema: "questionnaire",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false, defaultValueSql: "newsequentialid()"),
                    Name = table.Column<string>(maxLength: 50, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_QuestionSets", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "QuestionResponses",
                schema: "questionnaire",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false, defaultValueSql: "newsequentialid()"),
                    QuestionId = table.Column<Guid>(nullable: false),
                    Text = table.Column<string>(maxLength: 150, nullable: false),
                    Order = table.Column<int>(nullable: false),
                    DeteledAt = table.Column<DateTime>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_QuestionResponses", x => x.Id);
                    table.ForeignKey(
                        name: "FK_QuestionResponses_Questions_QuestionId",
                        column: x => x.QuestionId,
                        principalSchema: "questionnaire",
                        principalTable: "Questions",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "QuestionQuestionSets",
                schema: "questionnaire",
                columns: table => new
                {
                    QuestionId = table.Column<Guid>(nullable: false),
                    QuestionSetId = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_QuestionQuestionSets", x => new { x.QuestionId, x.QuestionSetId });
                    table.ForeignKey(
                        name: "FK_QuestionQuestionSets_Questions_QuestionId",
                        column: x => x.QuestionId,
                        principalSchema: "questionnaire",
                        principalTable: "Questions",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_QuestionQuestionSets_QuestionSets_QuestionSetId",
                        column: x => x.QuestionSetId,
                        principalSchema: "questionnaire",
                        principalTable: "QuestionSets",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "SubmittedUserQuestionnaire",
                schema: "questionnaire",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false, defaultValueSql: "newsequentialid()"),
                    UserId = table.Column<Guid>(nullable: false),
                    QuestionSetId = table.Column<Guid>(nullable: false),
                    CompletionDate = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SubmittedUserQuestionnaire", x => x.Id);
                    table.ForeignKey(
                        name: "FK_SubmittedUserQuestionnaire_QuestionSets_QuestionSetId",
                        column: x => x.QuestionSetId,
                        principalSchema: "questionnaire",
                        principalTable: "QuestionSets",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "SubmittedQuestionResponses",
                schema: "questionnaire",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false, defaultValueSql: "newsequentialid()"),
                    CompletedUserQuestionnaireId = table.Column<Guid>(nullable: false),
                    QuestionId = table.Column<Guid>(nullable: false),
                    OpenQuestionResponse = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SubmittedQuestionResponses", x => x.Id);
                    table.ForeignKey(
                        name: "FK_SubmittedQuestionResponses_SubmittedUserQuestionnaire_CompletedUserQuestionnaireId",
                        column: x => x.CompletedUserQuestionnaireId,
                        principalSchema: "questionnaire",
                        principalTable: "SubmittedUserQuestionnaire",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_SubmittedQuestionResponses_Questions_QuestionId",
                        column: x => x.QuestionId,
                        principalSchema: "questionnaire",
                        principalTable: "Questions",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "SubmittedQuestionResponseQuestionResponses",
                schema: "questionnaire",
                columns: table => new
                {
                    SubmittedQuestionResponseId = table.Column<Guid>(nullable: false),
                    QuestionResponseId = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SubmittedQuestionResponseQuestionResponses", x => new { x.SubmittedQuestionResponseId, x.QuestionResponseId });
                    table.ForeignKey(
                        name: "FK_SubmittedQuestionResponseQuestionResponses_QuestionResponses_QuestionResponseId",
                        column: x => x.QuestionResponseId,
                        principalSchema: "questionnaire",
                        principalTable: "QuestionResponses",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_SubmittedQuestionResponseQuestionResponses_SubmittedQuestionResponses_SubmittedQuestionResponseId",
                        column: x => x.SubmittedQuestionResponseId,
                        principalSchema: "questionnaire",
                        principalTable: "SubmittedQuestionResponses",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateIndex(
                name: "IX_QuestionQuestionSets_QuestionSetId",
                schema: "questionnaire",
                table: "QuestionQuestionSets",
                column: "QuestionSetId");

            migrationBuilder.CreateIndex(
                name: "IX_QuestionResponses_QuestionId",
                schema: "questionnaire",
                table: "QuestionResponses",
                column: "QuestionId");

            migrationBuilder.CreateIndex(
                name: "IX_QuestionSet_Name",
                schema: "questionnaire",
                table: "QuestionSets",
                column: "Name",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_SubmittedQuestionResponseQuestionResponses_QuestionResponseId",
                schema: "questionnaire",
                table: "SubmittedQuestionResponseQuestionResponses",
                column: "QuestionResponseId");

            migrationBuilder.CreateIndex(
                name: "IX_SubmittedQuestionResponses_CompletedUserQuestionnaireId",
                schema: "questionnaire",
                table: "SubmittedQuestionResponses",
                column: "CompletedUserQuestionnaireId");

            migrationBuilder.CreateIndex(
                name: "IX_SubmittedQuestionResponses_QuestionId",
                schema: "questionnaire",
                table: "SubmittedQuestionResponses",
                column: "QuestionId");

            migrationBuilder.CreateIndex(
                name: "IX_SubmittedUserQuestionnaire_QuestionSetId",
                schema: "questionnaire",
                table: "SubmittedUserQuestionnaire",
                column: "QuestionSetId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "QuestionQuestionSets",
                schema: "questionnaire");

            migrationBuilder.DropTable(
                name: "SubmittedQuestionResponseQuestionResponses",
                schema: "questionnaire");

            migrationBuilder.DropTable(
                name: "QuestionResponses",
                schema: "questionnaire");

            migrationBuilder.DropTable(
                name: "SubmittedQuestionResponses",
                schema: "questionnaire");

            migrationBuilder.DropTable(
                name: "SubmittedUserQuestionnaire",
                schema: "questionnaire");

            migrationBuilder.DropTable(
                name: "Questions",
                schema: "questionnaire");

            migrationBuilder.DropTable(
                name: "QuestionSets",
                schema: "questionnaire");
        }
    }
}
