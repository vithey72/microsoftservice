﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace BNine.Persistence.Migrations
{
    public partial class add_tips_user_services : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData(
                schema: "tariff",
                table: "PaidUserServices",
                columns: new[] { "Id", "ChargeTypeId", "CreatedAt", "Description", "DurationInDays", "Name", "ServiceBusName" },
                values: new object[,]
                {
                    { new Guid("04d918db-ff13-46de-abdd-ee5f2276c6b4"), 370, new DateTime(2022, 12, 8, 0, 0, 0, 0, DateTimeKind.Unspecified), "Tip of $8", 14, "After Advance Tip 8", "after_advance_tip" },
                    { new Guid("7bfd404c-7688-4f90-b3cf-c61f70431333"), 368, new DateTime(2022, 12, 8, 0, 0, 0, 0, DateTimeKind.Unspecified), "Tip of $3", 14, "After Advance Tip 3", "after_advance_tip" },
                    { new Guid("a12b4050-19bf-4b43-8ce9-ef4b549430f7"), 371, new DateTime(2022, 12, 8, 0, 0, 0, 0, DateTimeKind.Unspecified), "Tip of $10", 14, "After Advance Tip 10", "after_advance_tip" },
                    { new Guid("a6ef9444-f165-4bd1-9f56-ff0c07adb12f"), 369, new DateTime(2022, 12, 8, 0, 0, 0, 0, DateTimeKind.Unspecified), "Tip of $5", 14, "After Advance Tip 5", "after_advance_tip" }
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                schema: "tariff",
                table: "PaidUserServices",
                keyColumn: "Id",
                keyValue: new Guid("04d918db-ff13-46de-abdd-ee5f2276c6b4"));

            migrationBuilder.DeleteData(
                schema: "tariff",
                table: "PaidUserServices",
                keyColumn: "Id",
                keyValue: new Guid("7bfd404c-7688-4f90-b3cf-c61f70431333"));

            migrationBuilder.DeleteData(
                schema: "tariff",
                table: "PaidUserServices",
                keyColumn: "Id",
                keyValue: new Guid("a12b4050-19bf-4b43-8ce9-ef4b549430f7"));

            migrationBuilder.DeleteData(
                schema: "tariff",
                table: "PaidUserServices",
                keyColumn: "Id",
                keyValue: new Guid("a6ef9444-f165-4bd1-9f56-ff0c07adb12f"));
        }
    }
}
