﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace BNine.Persistence.Migrations
{
    public partial class change_version_naming_in_feature_group : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "AppVersion",
                schema: "features",
                table: "FeatureGroups",
                newName: "MinAppVersion");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "MinAppVersion",
                schema: "features",
                table: "FeatureGroups",
                newName: "AppVersion");
        }
    }
}
