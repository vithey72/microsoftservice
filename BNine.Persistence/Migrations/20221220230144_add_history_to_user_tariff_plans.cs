﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace BNine.Persistence.Migrations
{
    using Configurations.Extensions;
    using Constants;

    public partial class add_history_to_user_tariff_plans : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddVersioningToExistedTable("UserTariffPlans", "tariff", MigrationDefaults.VersioningSchema);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {

        }
    }
}
