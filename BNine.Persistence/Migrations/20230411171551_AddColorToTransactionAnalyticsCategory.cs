﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace BNine.Persistence.Migrations
{
    public partial class AddColorToTransactionAnalyticsCategory : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "TransactionCategories",
                keyColumn: "Key",
                keyValue: "supermarkets-online-stores",
                column: "Color",
                value: "12B76A");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "TransactionCategories",
                keyColumn: "Key",
                keyValue: "supermarkets-online-stores",
                column: "Color",
                value: null);
        }
    }
}
