﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace BNine.Persistence.Migrations
{
    public partial class add_initial_amount : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<DateTime>(
                name: "CreatedAt",
                schema: "user",
                table: "PlaidSettings",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<string>(
                name: "Currency",
                table: "ExternalBankAccounts",
                nullable: true);

            migrationBuilder.AddColumn<decimal>(
                name: "InitialAvailableAmount",
                table: "ExternalBankAccounts",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "CreatedAt",
                schema: "user",
                table: "PlaidSettings");

            migrationBuilder.DropColumn(
                name: "Currency",
                table: "ExternalBankAccounts");

            migrationBuilder.DropColumn(
                name: "InitialAvailableAmount",
                table: "ExternalBankAccounts");
        }
    }
}
