﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace BNine.Persistence.Migrations
{
    public partial class add_user_synced_stats_table : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "UserSyncedStats",
                schema: "user",
                columns: table => new
                {
                    UserId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    IsWaitingForArgyle = table.Column<bool>(type: "bit", nullable: false),
                    HasPayAllocation = table.Column<bool>(type: "bit", nullable: false),
                    AdvancesTakenCount = table.Column<int>(type: "int", nullable: false),
                    PayrollSumFor14Days = table.Column<decimal>(type: "decimal(18,2)", nullable: false),
                    LastAdvanceTakenAmount = table.Column<decimal>(type: "decimal(18,2)", nullable: false),
                    PayrollsTotalCount = table.Column<int>(type: "int", nullable: false),
                    UpdatedAt = table.Column<DateTime>(type: "datetime2", nullable: false, defaultValueSql: "GETDATE()")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UserSyncedStats", x => x.UserId);
                    table.ForeignKey(
                        name: "FK_UserSyncedStats_Users_UserId",
                        column: x => x.UserId,
                        principalSchema: "user",
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "UserSyncedStats",
                schema: "user");
        }
    }
}
