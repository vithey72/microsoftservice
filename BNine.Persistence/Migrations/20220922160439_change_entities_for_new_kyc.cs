﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace BNine.Persistence.Migrations
{
    public partial class change_entities_for_new_kyc : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "OcrProvider",
                schema: "user",
                table: "Documents",
                type: "int",
                nullable: false,
                defaultValue: 1);

            migrationBuilder.AddColumn<Guid>(
                name: "SocureBackImageId",
                schema: "user",
                table: "Documents",
                type: "uniqueidentifier",
                nullable: true);

            migrationBuilder.AddColumn<Guid>(
                name: "SocureFrontImageId",
                schema: "user",
                table: "Documents",
                type: "uniqueidentifier",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "OcrProvider",
                schema: "user",
                table: "Documents");

            migrationBuilder.DropColumn(
                name: "SocureBackImageId",
                schema: "user",
                table: "Documents");

            migrationBuilder.DropColumn(
                name: "SocureFrontImageId",
                schema: "user",
                table: "Documents");
        }
    }
}
