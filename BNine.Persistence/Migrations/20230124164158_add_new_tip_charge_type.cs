﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace BNine.Persistence.Migrations
{
    public partial class add_new_tip_charge_type : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData(
                schema: "tariff",
                table: "PaidUserServices",
                columns: new[] { "Id", "ChargeTypeId", "CreatedAt", "Description", "DurationInDays", "Name", "ServiceBusName" },
                values: new object[] { new Guid("6eee9521-d835-4265-af9e-ec0d3935c62a"), 59, new DateTime(2023, 1, 24, 0, 0, 0, 0, DateTimeKind.Unspecified), "Tip of $6", 14, "After Advance Tip 6", "after_advance_tip" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                schema: "tariff",
                table: "PaidUserServices",
                keyColumn: "Id",
                keyValue: new Guid("6eee9521-d835-4265-af9e-ec0d3935c62a"));
        }
    }
}
