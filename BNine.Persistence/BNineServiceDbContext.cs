﻿namespace BNine.Persistence
{
    using System;
    using System.Threading.Tasks;
    using Application.Interfaces;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.EntityFrameworkCore.Infrastructure;
    using Microsoft.EntityFrameworkCore.Storage;

    public class BNineServiceDbContext : DbContext, IBNineServiceDbContext
    {
        public BNineServiceDbContext(DbContextOptions<BNineServiceDbContext> options) : base(options)
        {
        }

        public async Task<IAsyncDisposable> CreateTransactionalAppLockAsync(string resource)
        {
            var @lock = new TransactionalAppLock(Database, resource);
            await @lock.BeginTransactionAndLock();
            return @lock;
        }

        private class TransactionalAppLock : IAsyncDisposable
        {
            private IDbContextTransaction _transaction;
            private readonly string _resourceId;
            private readonly DatabaseFacade _database;

            private const int LockTimeout = 30000;

            public TransactionalAppLock(DatabaseFacade database, string resourceId)
            {
                _resourceId = resourceId;
                _database = database;
            }

            public async Task BeginTransactionAndLock()
            {
                _transaction = await _database.BeginTransactionAsync();

                FormattableString command = $@"DECLARE @result INT;
                                               EXEC @result = sp_getapplock {_resourceId}, 'Exclusive', 'Transaction', {LockTimeout}
                                               IF @result < 0
                                               RAISERROR('ERROR: cannot get the lock {_resourceId} in less than {LockTimeout} milliseconds.', 16, 1);";

                await _database.ExecuteSqlInterpolatedAsync(command);
            }

            public async ValueTask DisposeAsync()
            {
                await _transaction.CommitAsync();
                await _transaction.DisposeAsync();
            }
        }
    }
}
