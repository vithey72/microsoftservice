﻿namespace BNine.Persistence.Services
{
    using System;
    using System.Threading.Tasks;
    using BNine.Application.Models;
    using BNine.Application.Interfaces;
    using BNine.Domain.Infrastructure;
    using MediatR;
    using Microsoft.Extensions.Logging;

    public class DomainEventsService : IDomainEventsService
    {
        private readonly ILogger<DomainEventsService> _logger;
        private readonly IMediator _mediator;

        public DomainEventsService(ILogger<DomainEventsService> logger, IMediator mediator)
        {
            _logger = logger;
            _mediator = mediator;
        }

        public async Task Publish(DomainEvent domainEvent)
        {
            _logger.LogInformation("Publishing domain event. Event - {event}", domainEvent.GetType().Name);
            await _mediator.Publish(GetNotificationCorrespondingToDomainEvent(domainEvent));
        }

        private INotification GetNotificationCorrespondingToDomainEvent(DomainEvent domainEvent)
        {
            return (INotification)Activator.CreateInstance(
                typeof(DomainEventNotification<>).MakeGenericType(domainEvent.GetType()), domainEvent);
        }
    }
}
