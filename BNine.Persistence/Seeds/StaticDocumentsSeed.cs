﻿namespace BNine.Persistence.Seeds;

using Domain.Entities.Settings;
using Microsoft.EntityFrameworkCore;

public class StaticDocumentsSeed
{
    public static async Task SeedAsync(BNineDbContext dbContext)
    {
        await AddTermsOfService(dbContext);

        //TODO: seed rest of the docs
    }

    private static async Task AddTermsOfService(BNineDbContext dbContext)
    {
        if (await AlreadyExists(dbContext, StaticDocumentKeys.TermsOfServiceKey))
        {
            return;
        }

        var tosDocument = new StaticDocument
        {
            Key = StaticDocumentKeys.TermsOfServiceKey,
            Title = "Terms of Service",
            StoragePath = "https://b9prodaccount.blob.core.windows.net/static-documents-container/Terms of Service 27.1.2023.pdf",
            Version = 1,
            CreatedAt = DateTime.UtcNow,
            UpdatedAt = DateTime.UtcNow,
        };
        dbContext.Add(tosDocument);
        await dbContext.SaveChangesAsync(CancellationToken.None);
    }

    private static async Task<bool> AlreadyExists(BNineDbContext context, string key)
    {
        return await context.StaticDocuments.AnyAsync(x => x.Key == key);
    }
}
