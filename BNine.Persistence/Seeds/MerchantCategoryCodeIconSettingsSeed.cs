﻿namespace BNine.Persistence.Seeds;

using BNine.Application.Interfaces;
using BNine.Constants;
using BNine.Enums;
using Domain.Entities.Settings;
using Helpers;
using Microsoft.EntityFrameworkCore;

public static class MerchantCategoryCodeIconSettingsSeed
{

    public static async Task SeedAsync(BNineDbContext dbContext, IPartnerProviderService partnerProviderService)
    {
        if (partnerProviderService.GetPartner() is (PartnerApp.Default or PartnerApp.MBanqApp))
        {
            return;
        }

        var entriesToAddLst = new List<MerchantCategoryCodeIconSettings>();
        var existingMerchantsCategoryCodeIconSettings = await dbContext.MerchantCategoryCodeIconSettings.ToListAsync();

        foreach (var entry in MccCodes.AllCodeEntitiesArray)
        {
            foreach (var code in entry.Codes)
            {
                CreateOrUpdateIfNeeded(entriesToAddLst, existingMerchantsCategoryCodeIconSettings,
                      code, entry.Description, entry.IconUrl);
            }
        }

        await dbContext.MerchantCategoryCodeIconSettings.AddRangeAsync(entriesToAddLst);
        await dbContext.SaveChangesWithIdentityInsertAsync<MerchantCategoryCodeIconSettings>();
    }

    private static void CreateOrUpdateIfNeeded(List<MerchantCategoryCodeIconSettings> entriesToAddLst, IReadOnlyCollection<MerchantCategoryCodeIconSettings> existingEntries, int code, string description, string iconUrl)
    {
        var existingEntry = existingEntries.FirstOrDefault(x => x.Code == code);
        if (existingEntry != null)
        {
            if (existingEntry.Description != description)
            {
                existingEntry.Description = description;

            }
            if (existingEntry.IconUrl != iconUrl)
            {
                existingEntry.IconUrl = iconUrl;
            }
        }
        else
        {
            entriesToAddLst.Add(new MerchantCategoryCodeIconSettings
            {
                Code = code,
                Description = description,
                IconUrl = iconUrl
            });
        }
    }

}
