﻿namespace BNine.Persistence.Seeds;

using System.Collections.Immutable;
using Application.Aggregates.CommonModels.Buttons;
using Application.Aggregates.CommonModels.DialogElements;
using Application.Aggregates.CommonModels.Text;
using Application.Aggregates.TariffPlans.Models;
using Application.Interfaces;
using BNine.Constants;
using Domain.Entities.TariffPlan;
using Enums;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
public static class TariffPlansSeed
{

    private static readonly string AdvanceTextFromPremiumTariffPlan =
        "B9 Basic℠ and B9 Premium℠ are optional services, offered by B9 to its members that require to set up a payroll " +
        "direct deposits to the B9 Account each month.\n\nAll B9 Basic℠ and B9 Premium℠ members start with up to $100 early pay advance. " +
        "B9 Premium℠ members have a potential for it to increase up to $500 early pay advance. " +
        "All B9 members are informed of their current available maxes in the B9 mobile app. " +
        "Their limit may change at any time, at B9's discretion.";

    private static readonly string AdvanceTextFromPremiumTariffPlanForAdditionalInfo =
        "B9 Basic℠ and B9 Premium℠ are optional services, offered by B9 to its members that require to set up a payroll " +
        "direct deposits to the B9 Account each month. All B9 Basic℠ and B9 Premium℠ members start with up to $100 early pay advance. " +
        "B9 Premium℠ members have a potential for it to increase up to $500 early pay advance. " +
        "All B9 members are informed of their current available maxes in the B9 mobile app. " +
        "Their limit may change at any time, at B9's discretion.";

    private static readonly string HintText =
        "Free membership for all clients whose income amount transferred to the B9 Account " +
        "is more than $5,000/month.\r\nDeposits from multiple employers count! Government benefits also qualify!";

    private static readonly HintWithDialogViewModel HintObject = new HintWithDialogViewModel
    {
        EventTracking = new EventTrackingObject
        (
            new UserEventInstructionsObject("close-button", null),
            null
        ),
        ImageUrl = "https://b9prodaccount.blob.core.windows.net/static-documents-container/Misc/HowItWorks/how_it_works_mainimg_bnine_v2.png",
        Title = "NO SUBSCRIPTION FEE!",
        Subtitle = new LinkedText(
            Text:
            "For all clients whose direct deposit amount transferred to their B9 Account is more than $5K/month.\n" +
            "Deposits from multiple employers count. Government benefits also qualify."),
        Buttons = new ButtonViewModel[]
        {
            new ButtonViewModel
            {
                Text = "+ DEPOSIT",
                Style = ButtonStyleEnum.Solid,
                Type = LinkFollowMode.Default,
                EventTracking =
                    new EventTrackingObject(
                        new UserEventInstructionsObject("deposit-button", null),
                        null),
                IsDisabled = false
            }.WithDeeplink(DeepLinkRoutes.Argyle)
        },
        IsCloseButtonHidden = false
    };

    private static readonly string BaseContainerUrl =
        "https://b9prodaccount.blob.core.windows.net/static-documents-container/Misc/";

    private static readonly TariffPlanModel.TariffViewmodelImageSubtitle BasicTariffViewModelImageSubtitle =
        new TariffPlanModel.TariffViewmodelImageSubtitle
        {
            Text = new FormattedText
            (
                "B9 Basic Plan\n{FULLPRICE_OR} {ZERO_MONTHLY_FEE}",
                new[]
                {
                    new FormattedTextFragmentViewModel(
                        Placeholder: "{FULLPRICE_OR}",
                        Text: "$9.99 or",
                        Emphasis: TypographicalEmphasis.None,
                        Color: ThematicColor.None,
                        FontSizeMultiplier: 1.0m),
                    new FormattedTextFragmentViewModel(
                        Placeholder: "{ZERO_MONTHLY_FEE}",
                        Text: "$0 monthly fee",
                        Emphasis: TypographicalEmphasis.Underlined,
                        Color: ThematicColor.None,
                        FontSizeMultiplier: 1.0m)
                }
            ),
            HintText = HintText,
            Hint = HintObject,
            MainColor = "ffffff",
            GradientColor = null
        };

    private static readonly TariffPlanModel.TariffViewmodelImageSubtitle PremiumTariffViewModelImageSubtitle =
        new TariffPlanModel.TariffViewmodelImageSubtitle
        {
            Text = new FormattedText
            (
                "B9 Premium Plan\n{FULLPRICE_OR} {ZERO_MONTHLY_FEE}",
                new[]
                {
                    new FormattedTextFragmentViewModel(
                        Placeholder: "{FULLPRICE_OR}",
                        Text: "$19.99 or",
                        Emphasis: TypographicalEmphasis.None,
                        Color: ThematicColor.None,
                        FontSizeMultiplier: 1.0m),
                    new FormattedTextFragmentViewModel(
                        Placeholder: "{ZERO_MONTHLY_FEE}",
                        Text: "$0 monthly fee",
                        Emphasis: TypographicalEmphasis.Underlined,
                        Color: ThematicColor.None,
                        FontSizeMultiplier: 1.0m),
                }
            ),
            HintText = HintText,
            Hint = HintObject,
            MainColor = "EED790",
            GradientColor = null
        };


    private static readonly ImmutableDictionary<Guid, TariffPlanModel.TariffViewmodelImageSubtitle>
        StandardImageSubtitleObjectsForTariffPlans =
            new Dictionary<Guid, TariffPlanModel.TariffViewmodelImageSubtitle>()
            {
                { PaidUserServices.DefaultPlan, BasicTariffViewModelImageSubtitle },
                { PaidUserServices.Basic1Month, BasicTariffViewModelImageSubtitle },
                { PaidUserServices.Premium1Month, PremiumTariffViewModelImageSubtitle },
                { PaidUserServices.Premium3Month10Sale, PremiumTariffViewModelImageSubtitle }
            }.ToImmutableDictionary();

    private record TariffFeatureKey(Guid TariffId, string NameKey);
    private record TariffFeatureSnapshot(string Text, HintWithDialogViewModel Hint, string AdditionalInfo, string Key = null);

    private class TariffFeatureEntity
    {
        public string Key
        {
            get; set;
        }

        public string Text
        {
            get; set;
        }

        public HintWithDialogViewModel Hint
        {
            get;
            set;
        }

        public string AdditionalInfo
        {
            get; set;
        }
    }

    private static readonly TariffFeatureSnapshot PremiumIsBasicPlusFeature = new(
        TariffPlansFeatures.PremiumTexts.PremiumIsBasicPlus,
        null,
        null
    );

    private static readonly TariffFeatureSnapshot PremiumAdvanceAmountFeature = new(
        TariffPlansFeatures.PremiumTexts.PremiumAdvanceAmountText,
        new HintWithDialogViewModel()
        {
            Title = "B9 Advance",
            Subtitle = new LinkedText(AdvanceTextFromPremiumTariffPlan),
            ImageUrl = string.Concat(BaseContainerUrl, "tariff_bnine_advance.png"),
            Buttons = new ButtonViewModel[]
            {
                new ButtonViewModel("GOT IT", ButtonStyleEnum.Solid)
            },
            IsCloseButtonHidden = false
        },
        AdvanceTextFromPremiumTariffPlanForAdditionalInfo
    );

    private static readonly TariffFeatureSnapshot CreditScoreFeature = new(
        TariffPlansFeatures.PremiumTexts.CreditScoreText,
        new HintWithDialogViewModel()
        {
            Title = "Credit Report",
            Subtitle = new LinkedText(TariffPlansFeatures.PremiumTexts.CreditScoreAdditionalText),
            ImageUrl = string.Concat(BaseContainerUrl, "tariff_credit_report.png"),
            Buttons = new ButtonViewModel[]
            {
                new ButtonViewModel("GOT IT", ButtonStyleEnum.Solid)
            },
            IsCloseButtonHidden = false
        },
        TariffPlansFeatures.PremiumTexts.CreditScoreAdditionalText
    );

    private static readonly TariffFeatureSnapshot PremiumSupportFeature = new(
        TariffPlansFeatures.PremiumTexts.PremiumSupportText,
        new HintWithDialogViewModel()
        {
            Title = TariffPlansFeatures.PremiumTexts.PremiumSupportText,
            Subtitle = new LinkedText(TariffPlansFeatures.PremiumTexts.PremiumSupportAdditionalText),
            ImageUrl = string.Concat(BaseContainerUrl, "tariff_premuim_support.png"),
            Buttons = new ButtonViewModel[]
            {
                new ButtonViewModel("GOT IT", ButtonStyleEnum.Solid)
            },
            IsCloseButtonHidden = false
        },
        TariffPlansFeatures.PremiumTexts.PremiumSupportAdditionalText
    );

    private static readonly TariffFeatureSnapshot TwoFreeWithdrawalsFeature = new(
        TariffPlansFeatures.PremiumTexts.AtmWithdrawals,
        new HintWithDialogViewModel()
        {
            Title = TariffPlansFeatures.PremiumTexts.AtmWithdrawals,
            Subtitle = new LinkedText(TariffPlansFeatures.PremiumTexts.AtmWithdrawalsAdditionalText),
            ImageUrl = string.Concat(BaseContainerUrl, "tariff_two_free_atm_withdrawals.png"),
            Buttons = new ButtonViewModel[]
            {
                new ButtonViewModel("GOT IT", ButtonStyleEnum.Solid)
            },
            IsCloseButtonHidden = false
        },
        TariffPlansFeatures.PremiumTexts.AtmWithdrawalsAdditionalText
    );

    private static readonly ImmutableDictionary<TariffFeatureKey, TariffFeatureSnapshot> FeatureSnapshotsByFeatureKey =
        new Dictionary<TariffFeatureKey, TariffFeatureSnapshot>
        {
            {
                new (PaidUserServices.Basic1Month, TariffPlansFeatures.BasicTexts.VisaDebitCardTextAsKey.ReplaceNewLine()),
                new (
                    TariffPlansFeatures.BasicTexts.VisaDebitCardText,
                    new HintWithDialogViewModel()
                    {
                        Title = TariffPlansFeatures.BasicTexts.VisaDebitCardText,
                        ImageUrl = string.Concat(BaseContainerUrl, "tariff_bnine_card.png"),
                        Buttons = new ButtonViewModel[]
                        {
                            new ButtonViewModel("GOT IT", ButtonStyleEnum.Solid)
                        },
                        IsCloseButtonHidden = false
                    },
                    null
                )
            },
            {
                new (PaidUserServices.Basic1Month, TariffPlansFeatures.BasicTexts.BasicAdvanceAmountText.ReplaceNewLine()),
                new (
                    TariffPlansFeatures.BasicTexts.BasicAdvanceAmountText,
                    new HintWithDialogViewModel()
                    {
                        Title = "B9 Advance",
                        Subtitle = new LinkedText(AdvanceTextFromPremiumTariffPlan),
                        ImageUrl = string.Concat(BaseContainerUrl, "tariff_bnine_advance.png"),
                        Buttons = new ButtonViewModel[]
                        {
                            new ButtonViewModel("GOT IT", ButtonStyleEnum.Solid)
                        },
                        IsCloseButtonHidden = false
                    },
                    null
                )
            },
            {
                new (PaidUserServices.Basic1Month, TariffPlansFeatures.BasicTexts.BasicAdvanceInstantServiceKey),
                new (
                    TariffPlansFeatures.BasicTexts.BasicAdvanceInstantService,
                    null,
                    null,
                    TariffPlansFeatures.BasicTexts.BasicAdvanceInstantServiceKey
                )
            },
            {
                new (PaidUserServices.Basic1Month, TariffPlansFeatures.BasicTexts.InstantTransfer.ReplaceNewLine()),
                new (
                    TariffPlansFeatures.BasicTexts.InstantTransfer,
                    new HintWithDialogViewModel()
                    {
                        Title = TariffPlansFeatures.BasicTexts.InstantTransfer,
                        ImageUrl = string.Concat(BaseContainerUrl, "tariff_instant_transfers.png"),
                        Buttons = new ButtonViewModel[]
                        {
                            new ButtonViewModel("GOT IT", ButtonStyleEnum.Solid)
                        },
                        IsCloseButtonHidden = false
                    },
                    null
                )
            },
            {
                new (PaidUserServices.Premium1Month, TariffPlansFeatures.PremiumTexts.PremiumIsBasicPlusAsKey.ReplaceNewLine()),
                PremiumIsBasicPlusFeature
            },
            {
                new (PaidUserServices.Premium1Month, TariffPlansFeatures.PremiumTexts.PremiumAdvanceAmountText.ReplaceNewLine()),
                PremiumAdvanceAmountFeature
            },
            {
                new (PaidUserServices.Premium1Month, TariffPlansFeatures.PremiumTexts.CreditScoreText.ReplaceNewLine()),
                CreditScoreFeature
            },
            {
                new (PaidUserServices.Premium1Month, TariffPlansFeatures.PremiumTexts.PremiumSupportText.ReplaceNewLine()),
                PremiumSupportFeature
            },
            {
                new (PaidUserServices.Premium1Month, TariffPlansFeatures.PremiumTexts.AtmWithdrawals.ReplaceNewLine()),
                TwoFreeWithdrawalsFeature
            },
            {
                new (PaidUserServices.Premium3Month10Sale, TariffPlansFeatures.PremiumTexts.PremiumIsBasicPlusAsKey.ReplaceNewLine()),
                PremiumIsBasicPlusFeature
            },
            {
                new (PaidUserServices.Premium3Month10Sale, TariffPlansFeatures.PremiumTexts.PremiumAdvanceAmountText.ReplaceNewLine()),
                PremiumAdvanceAmountFeature
            },
            {
                new (PaidUserServices.Premium3Month10Sale, TariffPlansFeatures.PremiumTexts.CreditScoreText.ReplaceNewLine()),
                CreditScoreFeature
            },
            {
                new (PaidUserServices.Premium3Month10Sale, TariffPlansFeatures.PremiumTexts.PremiumSupportText.ReplaceNewLine()),
                PremiumSupportFeature
            },
            {
                new (PaidUserServices.Premium3Month10Sale, TariffPlansFeatures.PremiumTexts.AtmWithdrawals.ReplaceNewLine()),
                TwoFreeWithdrawalsFeature
            },
        }.ToImmutableDictionary();

    public static async Task SeedAsync(BNineDbContext dbContext, IPartnerProviderService partnerProviderService, bool isForceUpdateRequired = false)
    {
        var tariffPlans = await dbContext.TariffPlans.ToListAsync();
        await SeedImageSubtitleObjectsIfNeeded(dbContext, tariffPlans, partnerProviderService, isForceUpdateRequired);
        UpdateTariffPlansFeatures(tariffPlans, partnerProviderService);
        await dbContext.SaveChangesAsync();
    }

    private static async Task SeedImageSubtitleObjectsIfNeeded(BNineDbContext dbContext, List<TariffPlan> tariffPlans,
        IPartnerProviderService partnerProviderService, bool isForceUpdateRequired)
    {


        // for now there was no request whatsoever to implement this for partners
        if (partnerProviderService.GetPartner() is not (PartnerApp.Default or PartnerApp.MBanqApp))
        {
            return;
        }

        foreach (var tariffPlan in tariffPlans)
        {

            var activeImageSubtitleObjectToSet =
                GetImageSubtitleObject(tariffPlan.ActiveImageSubtitleObject, tariffPlan.Id, isForceUpdateRequired);
            var advertisementImageSubtitleObjectToSet =
                GetImageSubtitleObject(tariffPlan.AdvertisementImageSubtitleObject, tariffPlan.Id, isForceUpdateRequired);

            tariffPlan.ActiveImageSubtitleObject = activeImageSubtitleObjectToSet;
            tariffPlan.AdvertisementImageSubtitleObject = advertisementImageSubtitleObjectToSet;

            await dbContext.SaveChangesAsync();
        }


    }

    private static void UpdateTariffPlansFeatures(ICollection<TariffPlan> tariffPlans,
        IPartnerProviderService partnerProviderService)
    {
        // for now there was no request whatsoever to implement this for partners
        if (partnerProviderService.GetPartner() is not (PartnerApp.Default or PartnerApp.MBanqApp))
        {
            return;
        }

        foreach (var tariffPlan in tariffPlans)
        {
            if (string.IsNullOrEmpty(tariffPlan.Features))
            {
                continue;
            }

            var currentFeatures = JsonConvert.DeserializeObject<TariffFeatureEntity[]>(tariffPlan.Features)!;

            var featuresModified = false;
            foreach (var currentFeature in currentFeatures)
            {
                var featureKey = new TariffFeatureKey(tariffPlan.Id, currentFeature.Key ?? currentFeature.Text.ReplaceNewLine());
                if (FeatureSnapshotsByFeatureKey.TryGetValue(featureKey, out var featureSnapshot))
                {
                    currentFeature.Key = featureSnapshot.Key;
                    currentFeature.Text = featureSnapshot.Text;
                    currentFeature.Hint = featureSnapshot.Hint;
                    currentFeature.AdditionalInfo = featureSnapshot.AdditionalInfo;
                    featuresModified = true;
                }
            }

            if (featuresModified)
            {
                tariffPlan.Features = JsonConvert.SerializeObject(currentFeatures);
            }
        }
    }

    private static string GetImageSubtitleObject(string imageSubtitleObject, Guid tariffPlanId, bool isForcedUpdateRequired)
    {
        var standartImageSubtitleObject = StandardImageSubtitleObjectsForTariffPlans[tariffPlanId];

        var standardVmImageSubtitle =
            JsonConvert.SerializeObject(standartImageSubtitleObject);

        if (imageSubtitleObject == null || isForcedUpdateRequired)
        {
            return standardVmImageSubtitle;
        }

        var currentImageSubtitle =
            JsonConvert.DeserializeObject<TariffPlanModel.TariffViewmodelImageSubtitle>(imageSubtitleObject);

        if (currentImageSubtitle?.Hint == null)
        {
            return standardVmImageSubtitle;
        }

        currentImageSubtitle.Text = standartImageSubtitleObject.Text;
        imageSubtitleObject = JsonConvert.SerializeObject(currentImageSubtitle);

        return imageSubtitleObject;
    }

    private static string ReplaceNewLine(this string text)
    {
        return text.Replace("\n", " ").Replace(@"\n", " ");
    }
}
