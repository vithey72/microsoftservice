﻿namespace BNine.Persistence.Seeds
{
    using System;
    using System.Collections.Generic;
    using System.Collections.Immutable;
    using System.Linq;
    using System.Threading.Tasks;
    using Application.Interfaces;
    using BNine.Domain.Entities.Settings;
    using BNine.Domain.ListItems;
    using BNine.Persistence.Configurations.Settings;
    using Domain.Entities.Common;

    public static class BNineDbContextSeed
    {
        public static async Task SeedSampleDataAsync(BNineDbContext context, IPartnerProviderService partnerProviderService)
        {
            await DocumentTypesSeed.SeedAsync(context);

            await ZendeskCategories(context);

            await SavingAccountSettingsInitialization(context);

            await LoanSettingsInitialization(context);

            await ArgyleSettingsInitialization(context);

            await QuestionnairesSeed.SeedAsync(context);

            await TransferMerchantsDbSeed.SeedAsync(context, partnerProviderService);

            await TariffPlansSeed.SeedAsync(context, partnerProviderService);

            await FeaturesSeed.SeedAsync(context, partnerProviderService);

            await MerchantCategoryCodeIconSettingsSeed.SeedAsync(context, partnerProviderService);

            await WalletBlockCollectionSeed.SeedAsync(context, partnerProviderService);
        }

        private static async Task ArgyleSettingsInitialization(BNineDbContext context)
        {
            #region Argyle Settings Initialization

            if (!context.ArgyleSettings.Any())
            {
                context.ArgyleSettings.Add(
                    new ArgyleSettings
                    {
                        Id = Guid.Parse("2d60257f-c71d-ec11-a0b3-7c50793c1398"),
                        DefaultAmountAllocation = 400,
                        MinAmountAllocation = 100,
                        MaxAmountAllocation = 10000,
                        DefaultPercentAllocation = 30,
                        MaxPercentAllocation = 100,
                        MinPercentAllocation = 1
                    });

                await context.SaveChangesAsync();
            }

            if (!context.EarlySalaryManualSettings.Any())
            {
                context.EarlySalaryManualSettings.Add(
                    new EarlySalaryManualSettings
                    {
                        Id = Guid.Parse("2d60257f-c72d-ec11-a0b3-7c50793c1398"),
                        DefaultAmountAllocation = 1000,
                        MinAmountAllocation = 100,
                        MaxAmountAllocation = 9999999,
                        DefaultPercentAllocation = 100,
                        MaxPercentAllocation = 100,
                        MinPercentAllocation = 1
                    });

                await context.SaveChangesAsync();
            }

            #endregion Argyle Of Income Initialization
        }

        internal static async Task LoanSettingsInitialization(BNineDbContext context)
        {
            #region Loan Settings Initialization

            if (!context.LoanSettings.Any())
            {
                context.LoanSettings.AddRange(
                    new LoanSettings
                    {
                        Id = Guid.Parse("39701af6-89a0-4ed5-82d0-95fca1316745"),
                        Limit = 200,
                        BoostExpressFeeChargeTypeId = LoanSettingsConfiguration.DefaultBoostExpressFeeChargeTypeId,
                        RequiredMinimalDeposit = 300,
                        ACHPrincipalKeywords = ImmutableSortedSet.Create(
                            "SALE ID",
                            "SALARY",
                            "PAYROLL",
                            "PAYMENTS ID",
                            "PAYMENT ID",
                            "lyft",
                            "GUSTO:",
                            "FED SAL ID",
                            "EDIPYMENTS",
                            "EDI PYMNTS",
                            "EDI PAYMNT",
                            "DoorDash, Inc.",
                            "DIRECT DEP ID",
                            "DIRDEP45 ID",
                            "DIR DEP ID",
                            "ALLOTMENT ID",
                            "REPUBLICPL",
                            "FED SAL ID",
                            "JONESVILLE ID",
                            "PAYRLL",
                            "K-mac enterprise"
                        )
                    });

                await context.SaveChangesAsync();
            }

            #endregion Sources Of Income Initialization
        }

        private static async Task SavingAccountSettingsInitialization(BNineDbContext context)
        {
            #region Saving Account Settings Initialization

            if (!context.SavingAccountSettings.Any())
            {
                context.SavingAccountSettings.AddRange(
                    new SavingAccountSettings
                    {
                        Id = Guid.Parse("479e0450-3e27-4bda-a30a-5ff71656a723"),
                        IsPullFromExternalCardEnabled = false
                    });

                await context.SaveChangesAsync();
            }

            #endregion Sources Of Income Initialization
        }

        private static async Task ZendeskCategories(BNineDbContext context)
        {
            #region Zendesk categories

            if (!context.ZendeskCategories.Any())
            {
                context.ZendeskCategories.AddRange(
                    new ZendeskCategory
                    {
                        Key = "problems_with_transfer",
                        Value = new List<LocalizedText>
                        {
                            new LocalizedText
                            {
                                Language = Enums.Language.English,
                                Text = "Problems with transfer"
                            },
                            new LocalizedText
                            {
                                Language = Enums.Language.Spanish,
                                Text = "Problemas con la transferencia"
                            }
                        },
                        Order = 1
                    },
                    new ZendeskCategory
                    {
                        Key = "missing_credit",
                        Value = new List<LocalizedText>
                        {
                            new LocalizedText
                            {
                                Language = Enums.Language.English,
                                Text = "Missing credit"
                            },
                            new LocalizedText
                            {
                                Language = Enums.Language.Spanish,
                                Text = "Falta crédito"
                            }
                        },
                        Order = 2
                    },
                    new ZendeskCategory
                    {
                        Key = "having_problems_submitting_my_id",
                        Value = new List<LocalizedText>
                        {
                            new LocalizedText
                            {
                                Language = Enums.Language.English,
                                Text = "Having problems submitting my ID"
                            },
                            new LocalizedText
                            {
                                Language = Enums.Language.Spanish,
                                Text = "Tengo problemas para enviar mi ID"
                            }
                        },
                        Order = 3
                    },
                    new ZendeskCategory
                    {
                        Key = "unauthorized_transaction",
                        Value = new List<LocalizedText>
                        {
                            new LocalizedText
                            {
                                Language = Enums.Language.English,
                                Text = "Unauthorized Transaction"
                            },
                            new LocalizedText
                            {
                                Language = Enums.Language.Spanish,
                                Text = "Transacción no autorizada"
                            }
                        },
                        Order = 4
                    },
                    new ZendeskCategory
                    {
                        Key = "advance_service",
                        Value = new List<LocalizedText>
                        {
                            new LocalizedText
                            {
                                Language = Enums.Language.English,
                                Text = "Advance Service"
                            },
                            new LocalizedText
                            {
                                Language = Enums.Language.Spanish,
                                Text = "Servicio anticipado"
                            }
                        },
                        Order = 5
                    },
                    new ZendeskCategory
                    {
                        Key = "close_account",
                        Value = new List<LocalizedText>
                        {
                            new LocalizedText
                            {
                                Language = Enums.Language.English,
                                Text = "Close account"
                            },
                            new LocalizedText
                            {
                                Language = Enums.Language.Spanish,
                                Text = "Cerrar cuenta"
                            }
                        },
                        Order = 6
                    },
                    new ZendeskCategory
                    {
                        Key = "update_pi",
                        Value = new List<LocalizedText>
                        {
                            new LocalizedText
                            {
                                Language = Enums.Language.English,
                                Text = "Update personal information"
                            },
                            new LocalizedText
                            {
                                Language = Enums.Language.Spanish,
                                Text = "Actualizar información personal"
                            }
                        },
                        Order = 7
                    },
                    new ZendeskCategory
                    {
                        Key = "other",
                        Value = new List<LocalizedText>
                        {
                            new LocalizedText
                            {
                                Language = Enums.Language.English,
                                Text = "Other"
                            },
                            new LocalizedText
                            {
                                Language = Enums.Language.Spanish,
                                Text = "Otro"
                            }
                        },
                        Order = 8
                    });

                await context.SaveChangesAsync();
            }

            #endregion
        }
    }
}
