﻿namespace BNine.Persistence.Seeds
{
    using BNine.Constants;
    using Domain.Entities.UserQuestionnaire;
    using Microsoft.EntityFrameworkCore;

    public class QuestionnairesSeed
    {
        public static async Task SeedAsync(BNineDbContext context)
        {
            await CreateDefaultQuestions(context);

            await CreateActivityQuestions(context);

            await CreateRateYourExperienceQuestions(context);

            await CreateSourceOfIncomeQuestions(context);
        }

        private static async Task CreateDefaultQuestions(BNineDbContext context)
        {
            var defaultQuestions = new[]
            {
                new Question(
                    new Guid("A3565E0C-2A9E-EC11-A99B-000D3A336B21"),
                    "Sample text A",
                    1,
                    new[]
                    {
                        new QuestionResponse(new Guid("A5565E0C-2A9E-EC11-A99B-000D3A336B21"), "Option A", 1),
                        new QuestionResponse(new Guid("A6565E0C-2A9E-EC11-A99B-000D3A336B21"), "Option B", 2),
                    },
                    false),
                new Question(
                    new Guid("A4565E0C-2A9E-EC11-A99B-000D3A336B21"),
                    "Sample text B",
                    2,
                    50),
            };
            await CreateQuestionSet(
                context,
                new Guid("A2565E0C-2A9E-EC11-A99B-000D3A336B21"),
                FeaturesNames.Settings.Questionnaire.Quizzes.Default,
                defaultQuestions);
        }

        private static async Task CreateActivityQuestions(BNineDbContext context)
        {
            var activityQuestions = new[]
            {
                new Question(
                    new Guid("439CF168-76CD-EC11-997E-000D3A32056C"),
                    "How much are you ready to transfer to your B9 account within two weeks to receive your B9 Advance?",
                    0,
                    new[]
                    {
                        new QuestionResponse(new Guid("529CF168-76CD-EC11-997E-000D3A32056C"), "I'm not ready yet", 1),
                        new QuestionResponse(new Guid("4D9CF168-76CD-EC11-997E-000D3A32056C"), "$300", 2),
                        new QuestionResponse(new Guid("4C9CF168-76CD-EC11-997E-000D3A32056C"), "$800", 3),
                        new QuestionResponse(new Guid("5C9CF168-76CD-EC11-997E-000D3A32056C"), "$2000", 4),
                    },
                    false,
                    "ProjectedDepositAmount"),
                new Question(
                    new Guid("449CF168-76CD-EC11-997E-000D3A32056C"),
                    "Please specify the sector in which you work",
                    1,
                    new[]
                    {
                        new QuestionResponse(new Guid("4E9CF168-76CD-EC11-997E-000D3A32056C"), "Advertising and marketing", 1),
                        new QuestionResponse(new Guid("479CF168-76CD-EC11-997E-000D3A32056C"), "Agriculture", 2),
                        new QuestionResponse(new Guid("499CF168-76CD-EC11-997E-000D3A32056C"), "Computer and technology", 3),
                        new QuestionResponse(new Guid("549CF168-76CD-EC11-997E-000D3A32056C"), "Delivery services", 4),
                        new QuestionResponse(new Guid("489CF168-76CD-EC11-997E-000D3A32056C"), "Construction", 5),
                        new QuestionResponse(new Guid("4A9CF168-76CD-EC11-997E-000D3A32056C"), "Education", 6),
                        new QuestionResponse(new Guid("4F9CF168-76CD-EC11-997E-000D3A32056C"), "E-commerce", 7),
                        new QuestionResponse(new Guid("569CF168-76CD-EC11-997E-000D3A32056C"), "Health care/medicine", 8),
                        new QuestionResponse(new Guid("5A9CF168-76CD-EC11-997E-000D3A32056C"), "Home improvement/repair", 9),
                        new QuestionResponse(new Guid("589CF168-76CD-EC11-997E-000D3A32056C"), "Finance and economic", 10),
                        new QuestionResponse(new Guid("5B9CF168-76CD-EC11-997E-000D3A32056C"), "Food & Beverage", 11),
                        new QuestionResponse(new Guid("519CF168-76CD-EC11-997E-000D3A32056C"), "Retail sales/stores", 12),
                        new QuestionResponse(new Guid("559CF168-76CD-EC11-997E-000D3A32056C"), "Transportation", 13),
                        new QuestionResponse(new Guid("509CF168-76CD-EC11-997E-000D3A32056C"), "Self-employed/Freelance", 14),
                        new QuestionResponse(new Guid("599CF168-76CD-EC11-997E-000D3A32056C"), "Others", 15),
                    },
                    false,
                    "SectorOfWork"),
                new Question(
                    new Guid("429CF168-76CD-EC11-997E-000D3A32056C"),
                    "Please specify the size of the company you work for",
                    2,
                    new[]
                    {
                        new QuestionResponse(new Guid("579CF168-76CD-EC11-997E-000D3A32056C"), "Less than 10 people", 1),
                        new QuestionResponse(new Guid("4B9CF168-76CD-EC11-997E-000D3A32056C"), "from 10 to 50", 2),
                        new QuestionResponse(new Guid("539CF168-76CD-EC11-997E-000D3A32056C"), "from 50 to 200", 3),
                        new QuestionResponse(new Guid("469CF168-76CD-EC11-997E-000D3A32056C"), "from 200 to 1000", 4),
                        new QuestionResponse(new Guid("459CF168-76CD-EC11-997E-000D3A32056C"), "More than 1000", 5),
                    },
                    false,
                    "CompanySize"),
            };
            await CreateQuestionSet(
                context,
                new Guid("419CF168-76CD-EC11-997E-000D3A32056C"),
                FeaturesNames.Settings.Questionnaire.Quizzes.Activity,
                activityQuestions);
        }

        private static async Task CreateRateYourExperienceQuestions(BNineDbContext context)
        {
            var header = "We want to hear you!";
            var mainButtonText = "SEND";
            var altButtonText = "DO IT LATER";
            var rateYourExperienceQuestions = new[]
            {
                new Question(
                    new Guid("FBD90936-F9B6-ED11-A8E1-00224803DBD5"),
                    "How was your experience with B9?",
                    0,
                    new[]
                    {
                        new QuestionResponse(new Guid("FED90936-F9B6-ED11-A8E1-00224803DBD5"), "Upset", 1),
                        new QuestionResponse(new Guid("FFD90936-F9B6-ED11-A8E1-00224803DBD5"), "Not satisfied", 2),
                        new QuestionResponse(new Guid("FDD90936-F9B6-ED11-A8E1-00224803DBD5"), "Somewhat satisfied", 3),
                        new QuestionResponse(new Guid("00DA0936-F9B6-ED11-A8E1-00224803DBD5"), "Satisfied", 4),
                        new QuestionResponse(new Guid("01DA0936-F9B6-ED11-A8E1-00224803DBD5"), "Wow, I like B9!", 5),
                    },
                    false,
                    null,
                    header,
                    mainButtonText,
                    altButtonText
                ),
                new Question(
                    new Guid("FCD90936-F9B6-ED11-A8E1-00224803DBD5"),
                    "Tell us why not 5 stars?",
                    1,
                    2000,
                    null,
                    header,
                    mainButtonText,
                    altButtonText
                )
            };

            await CreateQuestionSet(context,
                new Guid("FAD90936-F9B6-ED11-A8E1-00224803DBD5"),
                FeaturesNames.Settings.Questionnaire.Quizzes.RateYourExperience,
                rateYourExperienceQuestions,
                header,
                null,
                mainButtonText,
                altButtonText);
        }

        private static async Task CreateSourceOfIncomeQuestions(BNineDbContext context)
        {
            var sourceOfIncomeQuestions = new[]
            {
                new Question(
                    new Guid("A518E126-F9ED-ED11-8E8B-002248049944"),
                    "What's the source of your income? Name the company if applicable.",
                    0,
                    2000,
                    null
                ),
                new Question(
                    new Guid("A718E126-F9ED-ED11-8E8B-002248049944"),
                    "What's your monthly income?",
                    1,
                    100,
                    null
                ),
                new Question(
                    new Guid("A818E126-F9ED-ED11-8E8B-002248049944"),
                    "How do you get paid? Choose as many answers as apply.",
                    2,
                    new[]
                    {
                        new QuestionResponse(new Guid("47F12EC7-13DA-4876-8E22-11E63EEE01B2"), "Cash", 1),
                        new QuestionResponse(new Guid("AF18E126-F9ED-ED11-8E8B-002248049944"), "Bank Direct Deposit", 2),
                        new QuestionResponse(new Guid("AA18E126-F9ED-ED11-8E8B-002248049944"), "Card", 3),
                        new QuestionResponse(new Guid("AD18E126-F9ED-ED11-8E8B-002248049944"), "Check", 4),
                        new QuestionResponse(new Guid("AE18E126-F9ED-ED11-8E8B-002248049944"), "Cash App", 5),
                        new QuestionResponse(new Guid("A918E126-F9ED-ED11-8E8B-002248049944"), "Venmo", 6),
                        new QuestionResponse(new Guid("B118E126-F9ED-ED11-8E8B-002248049944"), "Crypto Transfers", 7),
                    },
                    true,
                    null
                ),
                new Question(
                    new Guid("A618E126-F9ED-ED11-8E8B-002248049944"),
                    "How often to do get paid? Choose as many answers as apply.",
                    3,
                    new[]
                    {
                        new QuestionResponse(new Guid("B318E126-F9ED-ED11-8E8B-002248049944"), "One-time", 1),
                        new QuestionResponse(new Guid("B218E126-F9ED-ED11-8E8B-002248049944"), "Weekly", 2),
                        new QuestionResponse(new Guid("AC18E126-F9ED-ED11-8E8B-002248049944"), "Every 2 weeks", 3),
                        new QuestionResponse(new Guid("AB18E126-F9ED-ED11-8E8B-002248049944"), "Monthly", 4),
                        new QuestionResponse(new Guid("B018E126-F9ED-ED11-8E8B-002248049944"), "Per service", 5),
                    },
                    true,
                    null
                ),
                new Question(
                    new Guid("D3A419A7-B6C5-4663-9B8F-CC96C181DCB9"),
                    "Do you deposit your income to B9?",
                    4,
                    new[]
                    {
                        new QuestionResponse(new Guid("1C1A14B3-93B4-46DD-8CCD-B78AB83E0795"), "Yes", 1),
                        new QuestionResponse(new Guid("1BB87E2D-7041-46C4-BD2E-E35530CA88C5"), "No", 2),
                    },
                    false
                ),
            };

            await CreateQuestionSet(
                context,
                new Guid("A418E126-F9ED-ED11-8E8B-002248049944"),
                FeaturesNames.Settings.Questionnaire.Quizzes.SourceOfIncome,
                sourceOfIncomeQuestions,
                "New source of income",
                "Knowing a little bit more about your gig helps us make B9 better.",
                "SEND");
        }

        private static async Task CreateQuestionSet(
            BNineDbContext context,
            Guid setId,
            string setName,
            Question[] questions,
            string title = null, string subtitle = null, string mainButtonText = null, string altButtonText = null)
        {
            var defaultSet = await context.QuestionSets.FirstOrDefaultAsync(qs => qs.Name == setName);
            if (defaultSet == null)
            {
                context.QuestionSets.Add(
                    new QuestionSet
                    {
                        Id = setId,
                        Name = setName,
                        Header = title,
                        MainButtonText = mainButtonText,
                        AltButtonText = altButtonText,
                        Questions = questions
                            .Select(q => new QuestionQuestionSet { Question = q })
                            .ToArray()
                    });

                await context.SaveChangesAsync();
            }
        }
    }
}

