﻿namespace BNine.Persistence.Seeds;

using BNine.Application.Aggregates.CommonModels;
using BNine.Application.Aggregates.CommonModels.DialogElements;
using BNine.Application.Aggregates.CommonModels.Text;
using BNine.Application.Aggregates.Configuration.Queries.GetWalletBlocks;
using BNine.Application.Interfaces;
using BNine.Domain.Entities.Settings;
using BNine.Enums;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;

public static class WalletBlockCollectionSeed
{
    public static async Task SeedAsync(BNineDbContext context, IPartnerProviderService partnerProviderService)
    {
        var settings = await context.WalletBlockCollectionSettings.FirstOrDefaultAsync();
        if (settings == null)
        {
            settings = new WalletBlockCollectionSettings
            {
                Id = Guid.NewGuid(),
            };

            context.WalletBlockCollectionSettings.Add(settings);
        }

        var viewModel = GetViewModel(partnerProviderService);

        settings.TextDescription = JsonConvert.SerializeObject(viewModel);

        await context.SaveChangesAsync();
    }

    private static WalletBlockCollectionViewModel GetViewModel(IPartnerProviderService partnerProviderService)
    {
        var viewModel = new WalletBlockCollectionViewModel
        {
            WidthPercentage = 100,
        };

        var partner = partnerProviderService.GetPartner();
        var blocks = new List<WalletBlockViewModel>();

        if (partner is PartnerApp.Default)
        {
            blocks.Add(GetIncomeTaxesWalletBlockModel());
        }

        if (partner
            is PartnerApp.Default
            or PartnerApp.MBanqApp
            or PartnerApp.Poetryy
            or PartnerApp.ManaPacific
            or PartnerApp.P2P
            or PartnerApp.PayHammer)
        {
            blocks.Add(GetInviteFriendWalletBlockModel());
        }

        if (blocks.Count > 1)
        {
            viewModel.WidthPercentage = 70;
        }
        viewModel.Blocks = blocks.ToArray();

        return viewModel;
    }

    private static WalletBlockViewModel GetIncomeTaxesWalletBlockModel()
    {
        var title = new FormattedText("{MAIN}", new[]
        {
            new FormattedTextFragmentViewModel("{MAIN}", "Income",
                HexColor: new ColorViewModel("FFFFFFFF"), Emphasis: TypographicalEmphasis.Bold),
        });

        var subtitle = new FormattedText("{MAIN}", new[]
        {
            new FormattedTextFragmentViewModel("{MAIN}", "Manage and control your income in few clicks",
                HexColor: new ColorViewModel("FFFFFFFF")),
        });

        var deeplink = new DeeplinkObject("employment-mode", null);

        var eventTracking = new EventTrackingObject(new UserEventInstructionsObject("block", new
        {
            source = "IncomeTaxes"
        }), null);

        return new WalletBlockViewModel
        {
            OrderNumber = 1,
            Title = title,
            Subtitle = subtitle,
            ImageUrl = "https://b9prodaccount.blob.core.windows.net/static-documents-container/Misc/income_taxes_wallet_block_icon.png",
            BackgroundColor = new ColorViewModel("FF14273A"),
            Deeplink = deeplink,
            EventTracking = eventTracking,
        };
    }

    private static WalletBlockViewModel GetInviteFriendWalletBlockModel()
    {
        var title = new FormattedText("{MAIN}", new[]
        {
            new FormattedTextFragmentViewModel("{MAIN}", "Invite a friend",
                HexColor: new ColorViewModel("FF000000"), Emphasis: TypographicalEmphasis.Bold),
        });

        var subtitle = new FormattedText("{MAIN}", new[]
        {
            new FormattedTextFragmentViewModel("{MAIN}", "$5 for you, $5 for your friend. \nInvite everyone you know!",
                HexColor: new ColorViewModel("FF000000")),
        });

        var deeplink = new DeeplinkObject("bonus", null);

        var eventTracking = new EventTrackingObject(new UserEventInstructionsObject("block", new
        {
            source = "InviteFriend"
        }), null);

        return new WalletBlockViewModel
        {
            OrderNumber = 2,
            Title = title,
            Subtitle = subtitle,
            ImageUrl = "https://b9prodaccount.blob.core.windows.net/static-documents-container/Misc/invite_friend_wallet_block_icon.png",
            BackgroundColor = new ColorViewModel("FFFFFFFF"),
            Deeplink = deeplink,
            EventTracking = eventTracking,
        };
    }
}
