﻿namespace BNine.Persistence.Seeds;

using BNine.Application.Interfaces;
using BNine.Constants;
using Domain.Entities.Features;
using Domain.Entities.Features.Groups;
using Enums;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

public static class FeaturesSeed
{
    public static async Task SeedAsync(BNineDbContext context, IPartnerProviderService partnerProviderService)
    {
        await ObsoleteFeatures.PurgeObsoleteFeatures(context);

        await CreateSmsVerificationDevelopmentModeFeature(context, isEnabled: false);

        await CreateArgyleSuccessScreenFeature(context);

        await CreateCheckCashingFeature(context);

        await CreateUserApplicationEvaluationFeature(context);

        await CreateArgyleCloseButtonHidingFeature(context);

        await CreateWelcomeScreenPayrollSwitchFeature(context);

        await CreateQuestionnaireFeatureWithActivityQuiz(context);

        await CreateMinAppVersionFeature(context);

        await CreateManualFormSurveyFeature(context);

        await CreateArrayCreditRatingFeature(context);

        await CreateTariffPlansButtonFeature(context);

        await CreateCashbackOnboardingScreensFeature(context);

        await CreateKycFeature(context);

        await CreatePayOnboardingFeatureAndExperiment(context,
            experimentEnabled: false,
            minUserRegistrationDate: new DateTime(2042, 12, 9));

        await CreateFavoriteTransfersFeature(context);

        await CreateChristmasTimeFeature(context, isEnabled: false);

        await AddAfterAdvanceTipsFeature(context, isEnabled: false);

        await CreateSelfieOnboardingFeature(context);

        await CreateScanVouchedBackSideFeature(context);

        await CreateIncomeAndTaxesScreenFeature(context, isEnabled: false);

        await CreateScanVouchedBackSideFeature(context);

        await CreateCreditCardInternalTest(context, isEnabled: false);

        await CreateNeedPinForVirtualCardFeature(context, isEnabled: false);

        await CreateTransactionAnalyticsFeature(context, isEnabled: false);

        await CreateRateYourExperienceFeature(context, isEnabled: false);

        await CreateExternalCardSelfieFeature(context, isEnabled: false);

        await CreateAdvanceWidgetBannerFeature(context, isEnabled: false);

        await CreateAprilTaxPlatformFeature(context, isEnabled: false);

        await CreateQuickBooksPlatformFeature(context, false);

        await CreateNewAdvanceWidgetSourceFeature(context, isEnabled: false);

        await CreateAdvanceLimitSimulatorFeature(context, isEnabled: false);

        await CreateFirebaseSplitTestingFeature(context, isEnabled: false);

        await CreateTempExperimentFeature(context, isEnabled: false);

        await CreateClientSelfieForOnboardingFeature(context, isEnabled: false);

        await CreateAccountClosureFeature(context, isEnabled: false);

        await CreateSwitchToPremiumAfterAdvanceFeature(context, isEnabled: false);

        await CreateCheckScorePaidServiceFeature(context, isEnabled: false);

        await CreateBeforeAdvancePremiumMotivationExperimentFeature(context, isEnabled: true);

        await CreateCurrentAdditionalAccountsInternalFeature(context, isEnabled: false);

        await CreateCurrentAdditionalAccountsExternalFeature(context, isEnabled: false);

        await CreateWalletInfoSectionFeature(context, partnerProviderService);
    }

    public static async Task SeedTestFeaturesWithPlatformAndVersionAsync(BNineDbContext dbContext)
    {
        await CreateTestFeaturesWithPlatformAndVersion(context: dbContext, true);
    }

    private static async Task CreateSmsVerificationDevelopmentModeFeature(BNineDbContext context, bool isEnabled)
    {
        await CreateFeature(context, FeaturesNames.Features.SmsVerificationDevelopmentMode);

        await CreateFeatureGroup(context, FeaturesNames.Features.SmsVerificationDevelopmentMode, FeaturesNames.Groups.Default, isEnabled);
    }

    private static async Task CreateExternalCardSelfieFeature(BNineDbContext context, bool isEnabled)
    {
        await CreateFeature(context, FeaturesNames.Features.ExternalCardSelfie);

        await CreateFeatureGroup(context, FeaturesNames.Features.ExternalCardSelfie, FeaturesNames.Groups.Default, isEnabled);
    }

    private static async Task CreateClientSelfieForOnboardingFeature(BNineDbContext context, bool isEnabled)
    {
        await CreateFeature(context, FeaturesNames.Features.ClientSelfieForOnboarding);

        await CreateFeatureGroup(context, FeaturesNames.Features.ClientSelfieForOnboarding, FeaturesNames.Groups.Default, isEnabled);
    }

    private static async Task CreateRateYourExperienceFeature(BNineDbContext context, bool isEnabled)
    {
        await CreateFeature(context, FeaturesNames.Features.RateYourExperience);

        await CreateFeatureGroup(context, FeaturesNames.Features.RateYourExperience, FeaturesNames.Groups.Default, isEnabled);

        //730 hours (1 month)
        var minAccountAgeHours = 730;
        //1460 hours (2 months)
        var maxAccountAgeHours = 1460;

        await AddFeatureGroupConstraints(context, FeaturesNames.Features.RateYourExperience, FeaturesNames.Groups.Default,
            minAccountAgeHours, maxAccountAgeHours);
    }

    private static async Task CreateTransactionAnalyticsFeature(BNineDbContext context, bool isEnabled)
    {
        await CreateFeature(context, FeaturesNames.Features.TransactionAnalytics);

        await CreateFeatureGroup(context, FeaturesNames.Features.TransactionAnalytics, FeaturesNames.Groups.Default, isEnabled);
    }

    private static async Task CreateNeedPinForVirtualCardFeature(BNineDbContext context, bool isEnabled)
    {
        await CreateFeature(context, FeaturesNames.Features.NeedPinForVirtualCard);

        await CreateFeatureGroup(context, FeaturesNames.Features.NeedPinForVirtualCard, FeaturesNames.Groups.Default, isEnabled);
    }

    private static async Task CreateArgyleSuccessScreenFeature(BNineDbContext context)
    {
        await CreateFeature(context, FeaturesNames.Features.ArgyleSuccessScreen);
        await CreateFeatureGroup(context, FeaturesNames.Features.ArgyleSuccessScreen, FeaturesNames.Groups.Default,
            true);
        await CreateFeatureGroup(context, FeaturesNames.Features.ArgyleSuccessScreen,
            FeaturesNames.Groups.SeenArgyleSuccessScreen, false);
    }

    private static async Task CreateCheckCashingFeature(BNineDbContext context)
    {
        await CreateFeature(context, FeaturesNames.Features.CheckCashing,
            $@"{{
                    ""$schema"": ""http://json-schema.org/draft-04/schema#"",
                    ""type"": ""object"",
                    ""properties"": {{
                        ""{FeaturesNames.Settings.CheckCashing.InstantAllocation}"": {{
                            ""type"": ""number"",
                            ""default"": 0,
                            ""serverOnly"": true
                        }},
                        ""{FeaturesNames.Settings.CheckCashing.MaxAmount}"": {{
                            ""type"": ""number"",
                            ""default"": 1000.00,
                            ""serverOnly"": true
                        }}
                    }},
                    ""required"": [
                        ""{FeaturesNames.Settings.CheckCashing.InstantAllocation}"",
                        ""{FeaturesNames.Settings.CheckCashing.MaxAmount}""
                    ]
                }}");

        await CreateFeatureGroup(context, FeaturesNames.Features.CheckCashing, FeaturesNames.Groups.Default, false,
            $@"{{
                        ""{FeaturesNames.Settings.CheckCashing.InstantAllocation}"": 20.00,
                        ""{FeaturesNames.Settings.CheckCashing.MaxAmount}"": 1000.00
                   }}");
    }

    private static async Task CreateUserApplicationEvaluationFeature(BNineDbContext context)
    {
        await CreateFeature(context, FeaturesNames.Features.UserApplicationEvaluation,
            $@"{{
                      ""$schema"": ""http://json-schema.org/draft-04/schema#"",
                      ""type"": ""object"",
                      ""properties"": {{
                            ""{FeaturesNames.Settings.UserApplicationEvaluation.AdvanceTakenCounter}"": {{
                                ""type"": ""integer""
                            }},
                            ""{FeaturesNames.Settings.UserApplicationEvaluation.IsActive}"": {{
                                ""type"": ""boolean""
                            }}
                      }},
                      ""required"": [
                          ""{FeaturesNames.Settings.UserApplicationEvaluation.AdvanceTakenCounter}"",
                          ""{FeaturesNames.Settings.UserApplicationEvaluation.IsActive}""
                      ]
                 }}");

        await CreateFeatureGroup(context,
            FeaturesNames.Features.UserApplicationEvaluation,
            FeaturesNames.Groups.Default,
            true,
            FeaturesNames.Settings.UserApplicationEvaluation.ConfigDefaultSettingsValue);
    }

    private static async Task CreateArgyleCloseButtonHidingFeature(BNineDbContext context)
    {
        await CreateFeature(context, FeaturesNames.Features.ArgyleCrossButtonHiding,
            $@"{{
                   ""$schema"": ""http://json-schema.org/draft-04/schema#"",
                   ""type"": ""object"",
                   ""properties"": {{
                        ""{FeaturesNames.Settings.ArgyleCrossButtonHiding.TimeMilliseconds}"": {{
                            ""type"": ""integer""
                        }}
                   }},
				    ""required"": [
                        ""{FeaturesNames.Settings.ArgyleCrossButtonHiding.TimeMilliseconds}""
				    ]
			  }}");
    }

    private static async Task CreateWelcomeScreenPayrollSwitchFeature(BNineDbContext context)
    {
        await CreateFeature(context, FeaturesNames.Features.WelcomePaycheckSwitchPayrollDepositScreen,
            $@"{{
                  ""$schema"": ""http://json-schema.org/draft-04/schema#"",
                  ""type"": ""object"",
                  ""properties"": {{
                                ""{FeaturesNames.Settings.WelcomePaycheckSwitchPayrollDepositScreen.ScreenId}"": {{
                                    ""type"": ""string""
                                }}
                            }},
                  ""required"": [
                    ""{FeaturesNames.Settings.WelcomePaycheckSwitchPayrollDepositScreen.ScreenId}""
                  ]
                }}");
    }

    private static async Task CreateQuestionnaireFeatureWithActivityQuiz(BNineDbContext context)
    {
        await CreateFeature(context, FeaturesNames.Features.UserQuestionnaire,
            $@"{{
                  ""$schema"": ""http://json-schema.org/draft-04/schema#"",
                  ""type"": ""object"",
                  ""properties"": {{
                    ""{FeaturesNames.Settings.Questionnaire.Passed}"" : {{
                    ""type"": ""object"",
                    ""properties"" : {{
                    ""{FeaturesNames.Settings.Questionnaire.Quizzes.Activity}"": {{ ""type"": ""boolean"" }},
                    ""{FeaturesNames.Settings.Questionnaire.Quizzes.Default}"": {{ ""type"": ""boolean"" }}
                  }}}}}},
                  ""required"": [ ]
                }}");

        await CreateFeatureGroup(context,
            FeaturesNames.Features.UserQuestionnaire,
            FeaturesNames.Groups.Default,
            true,
            $@"{{
                ""Passed"" : {{
                ""{FeaturesNames.Settings.Questionnaire.Quizzes.Activity}"": false,
                 ""{FeaturesNames.Settings.Questionnaire.Quizzes.Default}"": false
                }}}}");

        await CreateFeatureGroup(context,
            FeaturesNames.Features.UserQuestionnaire,
            FeaturesNames.Settings.Questionnaire.QuizGroupName(FeaturesNames.Settings.Questionnaire.Quizzes.Activity),
            true,
            $@"{{
                ""Passed"" : {{
                ""{FeaturesNames.Settings.Questionnaire.Quizzes.Activity}"": true
                }}}}");

        await CreateFeatureGroup(context,
            FeaturesNames.Features.UserQuestionnaire,
            FeaturesNames.Settings.Questionnaire.QuizGroupName(FeaturesNames.Settings.Questionnaire.Quizzes.Default),
            true,
            $@"{{
                ""Passed"" : {{
                 ""{FeaturesNames.Settings.Questionnaire.Quizzes.Default}"": true
                }}}}");
    }

    private static async Task CreateMinAppVersionFeature(BNineDbContext context)
    {
        await CreateFeature(context, FeaturesNames.Features.AppVersionRequirements,
            $@"{{
                  ""$schema"": ""http://json-schema.org/draft-04/schema#"",
                  ""type"": ""object"",
                  ""properties"": {{
                    ""{FeaturesNames.Settings.AppVersionRequirements.RequiredVersion}"": {{
                      ""type"": ""object"",
                      ""properties"": {{
                        ""{FeaturesNames.Settings.AppVersionRequirements.IOs}"": {{
                          ""type"": ""string""
                        }},
                        ""{FeaturesNames.Settings.AppVersionRequirements.Android}"": {{
                          ""type"": ""string""
                        }}
                      }},
                      ""required"": [
                        ""{FeaturesNames.Settings.AppVersionRequirements.IOs}"",
                        ""{FeaturesNames.Settings.AppVersionRequirements.Android}""
                      ]
                    }}
                  }},
                  ""required"": [
                    ""{FeaturesNames.Settings.AppVersionRequirements.RequiredVersion}""
                  ]
                }}");

        await CreateFeatureGroup(context,
            FeaturesNames.Features.AppVersionRequirements,
            FeaturesNames.Groups.Default,
            true,
            $@"{{
                      ""{FeaturesNames.Settings.AppVersionRequirements.RequiredVersion}"": {{
                        ""{FeaturesNames.Settings.AppVersionRequirements.IOs}"": ""1.29.1"",
                        ""{FeaturesNames.Settings.AppVersionRequirements.Android}"": ""1.26.0""
                      }}
                   }}");
    }

    private static async Task CreateManualFormSurveyFeature(BNineDbContext context)
    {
        await CreateFeature(context, FeaturesNames.Features.ManualFormSurvey,
            $@"{{
                  ""$schema"": ""http://json-schema.org/draft-04/schema#"",
                  ""type"": ""object"",
                  ""properties"": {{
                  ""{FeaturesNames.Settings.ManualFormSurvey.IsSurveyVisible}"": {{ ""type"": ""boolean"" }}}}}}");

        await CreateFeatureGroup(context,
            FeaturesNames.Features.ManualFormSurvey,
            FeaturesNames.Groups.Default,
            true,
            $@"{{ ""{FeaturesNames.Settings.ManualFormSurvey.IsSurveyVisible}"": false }}");

        await CreateFeatureGroup(context,
            FeaturesNames.Features.ManualFormSurvey,
            FeaturesNames.Groups.ShowManualFormSurvey,
            true,
            $@"{{ ""{FeaturesNames.Settings.ManualFormSurvey.IsSurveyVisible}"": true }}");
    }

    private static async Task CreateArrayCreditRatingFeature(BNineDbContext context)
    {
        await CreateFeature(context, FeaturesNames.Features.ArrayCreditRating,
            $@"{{
                  ""$schema"": ""http://json-schema.org/draft-04/schema#"",
                  ""type"": ""object"",
                  ""properties"": {{
                    ""{FeaturesNames.Settings.ArrayCreditRating.TermsAndConditions}"": {{
                      ""type"": ""string""
                    }}
                  }},
                  ""required"": [
                    ""{FeaturesNames.Settings.ArrayCreditRating.TermsAndConditions}""
                  ]
                }}");

        await CreateFeatureGroup(context,
            FeaturesNames.Features.ArrayCreditRating,
            FeaturesNames.Groups.Default,
            false,
            FeaturesNames.Settings.ArrayCreditRating.TermsAndConditionsValue);

        await CreateFeatureGroup(context,
            FeaturesNames.Features.ArrayCreditRating,
            FeaturesNames.Groups.ArrayCreditRatingInternalAdopters,
            true,
            FeaturesNames.Settings.ArrayCreditRating.TermsAndConditionsValue);
    }

    private static async Task CreateTariffPlansButtonFeature(BNineDbContext context)
    {
        await CreateFeature(context, FeaturesNames.Features.TariffPlansButton);
        await CreateFeatureGroup(context, FeaturesNames.Features.TariffPlansButton, FeaturesNames.Groups.Default,
            false);
    }

    private static async Task CreateCashbackOnboardingScreensFeature(BNineDbContext context)
    {
        await CreateFeature(context, FeaturesNames.Features.CashbackProgramOnboarding);
        await CreateFeatureGroup(context, FeaturesNames.Features.CashbackProgramOnboarding,
            FeaturesNames.Groups.Default,
            true);
        await CreateFeatureGroup(context, FeaturesNames.Features.CashbackProgramOnboarding,
            FeaturesNames.Groups.SeenCashbackOnboardingSlideshow, false);
    }

    private static async Task CreateKycFeature(BNineDbContext context)
    {
        await CreateFeature(context, FeaturesNames.Features.UseNewKycFlow);
        await CreateFeatureGroup(context, FeaturesNames.Features.UseNewKycFlow, FeaturesNames.Groups.Default, false);
    }

    private static async Task CreatePayOnboardingFeatureAndExperiment(BNineDbContext context, bool experimentEnabled, DateTime minUserRegistrationDate)
    {
        if (!FeatureExists(context, FeaturesNames.Features.PayOnboarding))
        {
            await CreateFeature(context, FeaturesNames.Features.PayOnboarding);

            await CreateSamplingFeatureGroup(context,
                FeaturesNames.Features.PayOnboarding,
                FeaturesNames.Groups.PayOnboardingGroupA,
                FeaturesNames.Settings.PayOnboarding.SettingsGroupA,
                1, 1, 0, 0.70, experimentEnabled,
                minUserRegistrationDate);

            await CreateSamplingFeatureGroup(context,
                FeaturesNames.Features.PayOnboarding,
                FeaturesNames.Groups.PayOnboardingGroupB,
                FeaturesNames.Settings.PayOnboarding.SettingsGroupB,
                1, 1, 0.70, 0.80, experimentEnabled,
                minUserRegistrationDate);

            await CreateSamplingFeatureGroup(context,
                FeaturesNames.Features.PayOnboarding,
                FeaturesNames.Groups.PayOnboardingGroupC,
                FeaturesNames.Settings.PayOnboarding.SettingsGroupC,
                1, 1, 0.80, 0.90, experimentEnabled,
                minUserRegistrationDate);

            await CreateSamplingFeatureGroup(context,
                FeaturesNames.Features.PayOnboarding,
                FeaturesNames.Groups.PayOnboardingGroupD,
                FeaturesNames.Settings.PayOnboarding.SettingsGroupD,
                1, 1, 0.90, 1.00, experimentEnabled,
                minUserRegistrationDate);

            await CreateFeatureGroup(context,
                FeaturesNames.Features.PayOnboarding,
                FeaturesNames.Groups.PayOnboardingDoneWithExperimentGroup,
                false);
        }
    }

    private static async Task CreateAdvanceWidgetBannerFeature(BNineDbContext context, bool isEnabled)
    {
        if (!FeatureExists(context, FeaturesNames.Features.AdvanceWidgetBanner))
        {
            await CreateFeature(context, FeaturesNames.Features.AdvanceWidgetBanner);

            await CreateFeatureGroup(context,
                FeaturesNames.Features.AdvanceWidgetBanner,
                FeaturesNames.Groups.Default, isEnabled);
        }
    }

    private static async Task CreateFavoriteTransfersFeature(BNineDbContext context)
    {
        await CreateFeature(context, FeaturesNames.Features.FavoriteTransfers);
        await CreateFeatureGroup(context,
            FeaturesNames.Features.FavoriteTransfers,
            FeaturesNames.Groups.Default,
            true,
            FeaturesNames.Settings.FavoriteTransfers.FavoriteTransfersSettings);
    }


    private static async Task CreateChristmasTimeFeature(BNineDbContext context, bool isEnabled)
    {
        await CreateFeature(context, FeaturesNames.Features.ChristmasTime);

        await CreateFeatureGroup(context, FeaturesNames.Features.ChristmasTime,
            FeaturesNames.Groups.Default, isEnabled);
    }

    private static async Task AddAfterAdvanceTipsFeature(BNineDbContext dbContext, bool isEnabled)
    {
        await CreateFeature(dbContext, FeaturesNames.Features.AfterAdvanceTips);
        await CreateFeatureGroup(dbContext,
            FeaturesNames.Features.AfterAdvanceTips,
            FeaturesNames.Groups.Default,
            isEnabled);
    }

    private static async Task CreateSelfieOnboardingFeature(BNineDbContext context)
    {
        await CreateFeature(context, FeaturesNames.Features.SelfieOnboarding,
            null);

        await CreateFeatureGroup(context, FeaturesNames.Features.SelfieOnboarding, FeaturesNames.Groups.Default, true,
            null);
    }

    private static async Task CreateScanVouchedBackSideFeature(BNineDbContext context)
    {
        await CreateFeature(context, FeaturesNames.Features.ScanVouchedBackSide);

        await CreateFeatureGroup(context, FeaturesNames.Features.ScanVouchedBackSide, FeaturesNames.Groups.Default, true);
    }

    private static async Task CreateIncomeAndTaxesScreenFeature(BNineDbContext context, bool isEnabled)
    {
        if (!FeatureExists(context, FeaturesNames.Features.IncomeAndTaxesScreen))
        {
            await CreateFeature(context, FeaturesNames.Features.IncomeAndTaxesScreen);

            await CreateFeatureGroup(context,
                FeaturesNames.Features.IncomeAndTaxesScreen,
                FeaturesNames.Groups.Default,
                isEnabled);

            await CreateFeatureGroup(context,
                FeaturesNames.Features.IncomeAndTaxesScreen,
                FeaturesNames.Groups.B9Core,
                !isEnabled);
        }
    }


    private static async Task CreateCreditCardInternalTest(BNineDbContext context, bool isEnabled)
    {
        if (!FeatureExists(context, FeaturesNames.Features.CreditCardInternalTest))
        {
            await CreateFeature(context, FeaturesNames.Features.CreditCardInternalTest);

            await CreateFeatureGroup(context,
                FeaturesNames.Features.IncomeAndTaxesScreen,
                FeaturesNames.Groups.Default,
                isEnabled);

            await CreateFeatureGroup(context,
                FeaturesNames.Features.CreditCardInternalTest,
                FeaturesNames.Groups.B9Core,
                !isEnabled);
        }
    }

    private static async Task CreateAprilTaxPlatformFeature(BNineDbContext context, bool isEnabled)
    {
        if (!FeatureExists(context, FeaturesNames.Features.AprilTaxPlatform))
        {
            await CreateFeature(context, FeaturesNames.Features.AprilTaxPlatform);

            await CreateFeatureGroup(context,
                FeaturesNames.Features.AprilTaxPlatform,
                FeaturesNames.Groups.Default,
                isEnabled);
        }
    }
    private static async Task CreateQuickBooksPlatformFeature(BNineDbContext context, bool isEnabled)
    {
        if (!FeatureExists(context, FeaturesNames.Features.QuickBooksPlatform))
        {
            await CreateFeature(context, FeaturesNames.Features.QuickBooksPlatform);

            await CreateFeatureGroup(context,
                FeaturesNames.Features.QuickBooksPlatform,
                FeaturesNames.Groups.Default,
                isEnabled);
        }
    }

    private static async Task CreateNewAdvanceWidgetSourceFeature(BNineDbContext context, bool isEnabled)
    {
        if (!FeatureExists(context, FeaturesNames.Features.NewAdvanceWidgetSource))
        {
            await CreateFeature(context, FeaturesNames.Features.NewAdvanceWidgetSource);

            await CreateFeatureGroup(context,
                FeaturesNames.Features.NewAdvanceWidgetSource,
                FeaturesNames.Groups.Default,
                isEnabled);

            await CreateFeatureGroup(context,
                FeaturesNames.Features.NewAdvanceWidgetSource,
                FeaturesNames.Groups.B9Core,
                !isEnabled);
        }
    }

    private static async Task CreateAdvanceLimitSimulatorFeature(BNineDbContext context, bool isEnabled)
    {
        await CreateFeature(context, FeaturesNames.Features.AdvanceLimitSimulator);

        await CreateFeatureGroup(context, FeaturesNames.Features.AdvanceLimitSimulator, FeaturesNames.Groups.Default, isEnabled);
    }

    private static async Task CreateFirebaseSplitTestingFeature(BNineDbContext context, bool isEnabled)
    {
        if (!FeatureExists(context, FeaturesNames.Features.FirebaseSplitTesting))
        {
            await CreateFeature(context, FeaturesNames.Features.FirebaseSplitTesting);
            await CreateFeatureGroup(context,
                FeaturesNames.Features.FirebaseSplitTesting,
                FeaturesNames.Groups.Default,
                isEnabled,
                FeaturesNames.Settings.FirebaseSplitTesting.Settings);
        }
    }

    private static async Task CreateTestFeaturesWithPlatformAndVersion(BNineDbContext context, bool isEnabled)
    {
        var baseName = FeaturesNames.Features.TestFeatureForVersionAndPlatform;
        await CreateTestFeatureWithPlatformAndVersion(
            context:context,
            mobilePlatform:null,
            appVersion:null,
            featureName: $"{baseName}_Default",
            isEnabled: isEnabled);
        await CreateTestFeatureWithPlatformAndVersion(
            context: context,
            mobilePlatform: MobilePlatform.iOs,
            appVersion: Version.Parse("1.2.1"),
            featureName: $"{baseName}_IOSWithVersion",
            isEnabled: isEnabled);
        await CreateTestFeatureWithPlatformAndVersion(
            context: context,
            mobilePlatform: MobilePlatform.Android,
            appVersion: Version.Parse("1.2.1"),
            featureName: $"{baseName}_AndroidWithVersion",
            isEnabled: isEnabled);
        await CreateTestFeatureWithPlatformAndVersion(
            context: context,
            mobilePlatform: MobilePlatform.iOs,
            appVersion: null,
            featureName: $"{baseName}_IOSWithoutVersion",
            isEnabled: isEnabled);
        await CreateTestFeatureWithPlatformAndVersion(
            context: context,
            mobilePlatform: null,
            appVersion: null,
            featureName: $"{baseName}_TestFeatureOverride",
            isEnabled: false,
            allowDuplicates: true);
        await CreateTestFeatureWithPlatformAndVersion(
            context: context,
            mobilePlatform: MobilePlatform.iOs,
            appVersion: Version.Parse("1.2.1"),
            featureName: $"{baseName}_TestFeatureOverride",
            isEnabled: isEnabled,
            allowDuplicates: true);
    }

    private static async Task CreateTestFeatureWithPlatformAndVersion(BNineDbContext context,
        MobilePlatform? mobilePlatform, Version appVersion, string featureName, bool isEnabled, bool allowDuplicates = false)
    {
        await CreateFeature(context, featureName);
        await CreateFeatureGroup(context, featureName,
            FeaturesNames.Groups.Default, isEnabled, null, mobilePlatform, appVersion, allowDuplicates);
    }


    private static async Task CreateTempExperimentFeature(BNineDbContext context, bool isEnabled)
    {
        await CreateFeature(context, "TempExperimentFeature");

        await CreateFeatureGroup(context, "TempExperimentFeature", FeaturesNames.Groups.Default, isEnabled);
    }

    private static async Task CreateAccountClosureFeature(BNineDbContext context, bool isEnabled)
    {
        await CreateFeature(context, FeaturesNames.Features.AccountClosure);

        await CreateFeatureGroup(context, FeaturesNames.Features.AccountClosure, FeaturesNames.Groups.Default, isEnabled);
    }

    private static async Task CreateSwitchToPremiumAfterAdvanceFeature(BNineDbContext context, bool isEnabled)
    {
        if (!FeatureExists(context, FeaturesNames.Features.SwitchToPremiumAfterAdvance))
        {
            await CreateFeature(context, FeaturesNames.Features.SwitchToPremiumAfterAdvance);

            await CreateFeatureGroup(context,
                FeaturesNames.Features.SwitchToPremiumAfterAdvance,
                FeaturesNames.Groups.Default,
                isEnabled,
                FeaturesNames.Settings.SwitchToPremiumAfterAdvance.Settings);
        }
    }

    private static async Task CreateCheckScorePaidServiceFeature(BNineDbContext context, bool isEnabled = false)
    {
        await CreateFeature(context, FeaturesNames.Features.CheckScorePaidService);

        await CreateFeatureGroup(context, FeaturesNames.Features.CheckScorePaidService, FeaturesNames.Groups.Default, isEnabled);
    }

    private static async Task CreateBeforeAdvancePremiumMotivationExperimentFeature(BNineDbContext context, bool isEnabled = false)
    {
        await CreateFeature(context, FeaturesNames.Features.BeforeAdvancePremiumMotivationExperiment);

        await CreateFeatureGroup(context, FeaturesNames.Features.BeforeAdvancePremiumMotivationExperiment,
            FeaturesNames.Groups.Default, isEnabled);
    }

    private static async Task CreateCurrentAdditionalAccountsInternalFeature(BNineDbContext context, bool isEnabled = false)
    {
        await CreateFeature(context, FeaturesNames.Features.CurrentAdditionalAccountsInternal);

        await CreateFeatureGroup(context, FeaturesNames.Features.CurrentAdditionalAccountsInternal,
            FeaturesNames.Groups.Default, isEnabled);
    }

    private static async Task CreateCurrentAdditionalAccountsExternalFeature(BNineDbContext context, bool isEnabled = false)
    {
        await CreateFeature(context, FeaturesNames.Features.CurrentAdditionalAccountsExternal);

        await CreateFeatureGroup(context, FeaturesNames.Features.CurrentAdditionalAccountsExternal,
            FeaturesNames.Groups.Default, isEnabled);
    }

    private static async Task CreateWalletInfoSectionFeature(BNineDbContext context, IPartnerProviderService partnerProviderService)
    {
        var isEnabled = IsWalletInfoSectionEnabled(partnerProviderService);

        await CreateFeature(context, FeaturesNames.Features.WalletInfoSection);

        await CreateFeatureGroup(context, FeaturesNames.Features.WalletInfoSection, FeaturesNames.Groups.Default, isEnabled);
    }

    private static bool IsWalletInfoSectionEnabled(IPartnerProviderService partnerProviderService)
    {
        var partner = partnerProviderService.GetPartner();

        if (partner
            is PartnerApp.Default
            or PartnerApp.MBanqApp
            or PartnerApp.Poetryy
            or PartnerApp.ManaPacific
            or PartnerApp.P2P
            or PartnerApp.PayHammer)
        {
            return true;
        }

        return false;
    }

    private static bool FeatureExists(BNineDbContext dbContext, string featureName)
    {
        return dbContext.Features.Any(f => f.Name == featureName);
    }


    private static async Task CreateFeature(BNineDbContext dbContext, string featureName, string schema = null)
    {
        var existing = await dbContext.Features
            .FirstOrDefaultAsync(f => f.Name == featureName);

        if (schema != null)
        {
            try
            {
                JObject.Parse(schema);
            }
            catch (Exception ex)
            {
                throw new JsonException($"Feature schema Json for feature {featureName} is invalid", ex);
            }
        }

        if (existing == null)
        {
            var now = DateTime.Now;
            var feature = new Feature { CreatedAt = now, UpdatedAt = now, Name = featureName, SettingsSchema = schema };
            await dbContext.Features.AddAsync(feature);
        }

        await dbContext.SaveChangesAsync();
    }

    private static async Task CreateSamplingFeatureGroup(BNineDbContext dbContext, string featureName, string groupName,
        string featureGroupSettings, int seed, double rollout, double rangeStart, double rangeEnd, bool isEnabled,
        DateTime? minRegistrationDate = null)
    {
        var existingGroup = await dbContext.Groups
            .FirstOrDefaultAsync(f => f.Name == groupName);
        if (existingGroup == null)
        {
            existingGroup = await CreateSplitTestingGroup(dbContext, groupName, seed, rollout, rangeStart, rangeEnd);
        }

        var groupId = existingGroup.Id;
        var featureId = dbContext.Features
            .FirstOrDefault(f => f.Name == featureName)!
            .Id;
        var existingFeatureGroup = await dbContext.FeatureGroups
            .FirstOrDefaultAsync(fg =>
                fg.GroupId == groupId &&
                fg.FeatureId == featureId);

        if (existingFeatureGroup == null)
        {
            var now = DateTime.Now;
            var featureGroup = new FeatureGroup
            {
                CreatedAt = now,
                UpdatedAt = now,
                GroupId = groupId,
                FeatureId = featureId,
                IsEnabled = isEnabled,
                Settings = featureGroupSettings,
                MinRegistrationDate = minRegistrationDate,
            };
            await dbContext.FeatureGroups.AddAsync(featureGroup);
        }

        await dbContext.SaveChangesAsync();
    }

    private static async Task CreateFeatureGroup(BNineDbContext dbContext, string featureName, string groupName,
        bool isEnabled, string settingsJson = null,
        MobilePlatform? mobilePlatform = null, Version appVersion = null, bool allowDuplicates = false)
    {
        var existingGroup = await dbContext.Groups
            .FirstOrDefaultAsync(f => f.Name == groupName);
        if (existingGroup == null)
        {
            existingGroup = await CreateGroup(dbContext, groupName);
        }

        var groupId = existingGroup.Id;
        var featureId = dbContext.Features
            .FirstOrDefault(f => f.Name == featureName)!
            .Id;
        var existingFeatureGroup = await dbContext.FeatureGroups
            .FirstOrDefaultAsync(fg =>
                fg.GroupId == groupId &&
                fg.FeatureId == featureId);

        if (settingsJson != null)
        {
            try
            {
                JObject.Parse(settingsJson);
            }
            catch (Exception ex)
            {
                throw new JsonException($"Settings schema Json is invalid for FeatureGroup {featureName}-{groupName}",
                    ex);
            }
        }

        if (allowDuplicates || existingFeatureGroup == null)
        {
            var now = DateTime.Now;
            var featureGroup = new FeatureGroup
            {
                CreatedAt = now,
                UpdatedAt = now,
                GroupId = groupId,
                FeatureId = featureId,
                IsEnabled = isEnabled,
                Settings = settingsJson,
                MobilePlatform = mobilePlatform,
                MinAppVersion = appVersion
            };
            await dbContext.FeatureGroups.AddAsync(featureGroup);
        }

        await dbContext.SaveChangesAsync();
    }

    private static async Task<Group> CreateSplitTestingGroup(BNineDbContext dbContext, string groupName, int seed,
        double rollout, double rangeStart, double rangeEnd)
    {
        var existingGroup = await dbContext.Groups.FirstOrDefaultAsync(g => g.Name == groupName);
        if (existingGroup != null)
        {
            return existingGroup;
        }

        var now = DateTime.Now;
        var group = new SampleGroup
        {
            Name = groupName,
            Kind = GroupKind.Sample,
            CreatedAt = now,
            UpdatedAt = now,
            Seed = seed,
            Rollout = rollout,
            SubRangeStart = rangeStart,
            SubRangeEnd = rangeEnd
        };
        var result = (await dbContext.Groups.AddAsync(group)).Entity;
        await dbContext.SaveChangesAsync();

        return result;
    }

    private static async Task<Group> CreateGroup(BNineDbContext dbContext, string groupName)
    {
        Group result;
        var now = DateTime.Now;
        if (groupName == FeaturesNames.Groups.Default)
        {
            var group = new DefaultGroup
            {
                Name = groupName, Kind = GroupKind.Default, CreatedAt = now, UpdatedAt = now,
            };
            result = (await dbContext.Groups.AddAsync(group)).Entity;
            await dbContext.SaveChangesAsync();
        }
        else
        {
            var group = new UserListGroup
            {
                Name = groupName, Kind = GroupKind.UserList, CreatedAt = now, UpdatedAt = now,
            };
            result = (await dbContext.Groups.AddAsync(group)).Entity;
            await dbContext.SaveChangesAsync();
        }

        return result;
    }

    private static async Task AddFeatureGroupConstraints(BNineDbContext dbContext, string featureName, string groupName,
        int? minAccAgeHours = null, int? maxAccAgeHours = null,
        DateTime? minRegistrationDate = null, DateTime? maxRegistrationDate = null)
    {
        var group = await dbContext.Groups.FirstOrDefaultAsync(x => x.Name == groupName);
        if (group == null)
        {
            return;
        }

        var feature = await dbContext.Features.FirstOrDefaultAsync(x => x.Name == featureName);
        if (feature == null)
        {
            return;
        }

        var featureGroup = await dbContext.FeatureGroups.FirstOrDefaultAsync(fg =>
            fg.FeatureId == feature.Id && fg.GroupId == group.Id);
        if (featureGroup == null)
        {
            return;
        }

        featureGroup.MinAccountAgeHours = minAccAgeHours;
        featureGroup.MaxAccountAgeHours = maxAccAgeHours;
        featureGroup.MinRegistrationDate = minRegistrationDate;
        featureGroup.MaxAccountAgeHours = maxAccAgeHours;

        await dbContext.SaveChangesAsync();
    }

    private static class ObsoleteFeatures
    {
        private const string NextAdvanceMotivationInfo = "NextAdvanceMotivationInfo";
        private const string BannerWidget = "BannerWidget";

        private static string[] ObsoleteFeatureNames =
        {
            NextAdvanceMotivationInfo,
            BannerWidget,
        };

        public static async Task PurgeObsoleteFeatures(BNineDbContext context)
        {
            var existingObsolete = await context.Features
                .Where(f => ObsoleteFeatureNames.Contains(f.Name))
                .ToArrayAsync();

            if (existingObsolete.Length == 0)
            {
                return;
            }

            context.Features.RemoveRange(existingObsolete);

            await context.SaveChangesAsync();
        }
    }
}
