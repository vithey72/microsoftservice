﻿namespace BNine.Persistence.Seeds
{
    using System.Collections.Immutable;
    using BNine.Application.Interfaces;
    using BNine.Enums;
    using Domain.Entities.Settings;
    using Microsoft.EntityFrameworkCore;

    public static class TransferMerchantsDbSeed
    {
        public static async Task SeedAsync(BNineDbContext context, IPartnerProviderService partnerProviderService)
        {
            if (partnerProviderService.GetPartner() is (PartnerApp.Default or PartnerApp.MBanqApp))
            {
                return;
            }
            var merchantsToAdd = new List<TransferMerchantsSettings>();
            var existingMerchants = await context.TransferMerchantsSettings.ToListAsync();

            AddMerchant(merchantsToAdd, existingMerchants, "amazon", "Amazon", new[] { "AMAZON", "AMZ*" }, "https://b9prodaccount.blob.core.windows.net/static-documents-container/TransactionMerchants/amazon.png");
            AddMerchant(merchantsToAdd, existingMerchants, "amazonPrime", "AMAZON PRIME", new[] { "AMAZON PRIME", "AMAZONPRIME" }, "https://b9prodaccount.blob.core.windows.net/static-documents-container/TransactionMerchants/amazon_prime.png ", 1);
            AddMerchant(merchantsToAdd, existingMerchants, "apple", "Apple", new[] { "APPLE.COM", "APPLE STORE", "APPLE CASH" }, "https://b9prodaccount.blob.core.windows.net/static-documents-container/TransactionMerchants/apple.png");
            AddMerchant(merchantsToAdd, existingMerchants, "google", "Google", new[] { "GOOGLE" }, "https://b9prodaccount.blob.core.windows.net/static-documents-container/TransactionMerchants/google.png");
            AddMerchant(merchantsToAdd, existingMerchants, "lyft", "Lyft", new[] { "LYFT" }, "https://b9prodaccount.blob.core.windows.net/static-documents-container/TransactionMerchants/lyft.png");
            AddMerchant(merchantsToAdd, existingMerchants, "cashapp", "Cash App", new[] { "CASH APP*" }, "https://b9prodaccount.blob.core.windows.net/static-documents-container/TransactionMerchants/cashapp.png");
            AddMerchant(merchantsToAdd, existingMerchants, "uber", "Uber", new[] { "UBER", "UBR*" }, "https://b9prodaccount.blob.core.windows.net/static-documents-container/TransactionMerchants/uber.png");
            AddMerchant(merchantsToAdd, existingMerchants, "uberEats", "UBER EATS", new[] { "UBER EATS", "UBER EATS", "UBER* EATS", "UBER*EATS", "UBER EATS", "UBER * EATS", "UBER * EATS", "UBER *EATS", "UBER *VERIFY EATS" }, "https://b9prodaccount.blob.core.windows.net/static-documents-container/TransactionMerchants/uber_eats.png", 1);
            AddMerchant(merchantsToAdd, existingMerchants, "doordash", "DoorDash", new[] { "DOORDASH", "DD DOORDASH" }, "https://b9prodaccount.blob.core.windows.net/static-documents-container/TransactionMerchants/doordash.png");
            AddMerchant(merchantsToAdd, existingMerchants, "paypal", "PayPal", new[] { "PP", "PAYPAL" }, "https://b9prodaccount.blob.core.windows.net/static-documents-container/TransactionMerchants/paypal.png");
            AddMerchant(merchantsToAdd, existingMerchants, "meta", "Facebook", new[] { "FACEBOOK", "FBPAY" }, "https://b9prodaccount.blob.core.windows.net/static-documents-container/TransactionMerchants/facebook.png");
            AddMerchant(merchantsToAdd, existingMerchants, "walmart", "Walmart", new[] { "WALMART" }, "https://b9devaccount.blob.core.windows.net/static-documents-container/TransactionMerchants/walmart.png");
            AddMerchant(merchantsToAdd, existingMerchants, "venmo", "Venmo", new[] { "VENMO" }, "https://b9devaccount.blob.core.windows.net/static-documents-container/TransactionMerchants/venmo.png");
            AddMerchant(merchantsToAdd, existingMerchants, "pulsz", "Pulsz.com", new[] { "PULSZ" }, "https://b9devaccount.blob.core.windows.net/static-documents-container/TransactionMerchants/pulsz.png");
            AddMerchant(merchantsToAdd, existingMerchants, "klarna", "KLARNA", new[] { "KLARNA" }, "https://b9devaccount.blob.core.windows.net/static-documents-container/TransactionMerchants/klarna.png");
            AddMerchant(merchantsToAdd, existingMerchants, "netflix", "Netflix", new[] { "NETFLIX" }, "https://b9devaccount.blob.core.windows.net/static-documents-container/TransactionMerchants/netflix.png");
            AddMerchant(merchantsToAdd, existingMerchants, "coinbase", "Coinbase", new[] { "COINBASE" }, "https://b9devaccount.blob.core.windows.net/static-documents-container/TransactionMerchants/coinbase.png");
            AddMerchant(merchantsToAdd, existingMerchants, "starbucks", "Starbucks", new[] { "STARBUCKS" }, "https://b9devaccount.blob.core.windows.net/static-documents-container/TransactionMerchants/starbucks.png");
            AddMerchant(merchantsToAdd, existingMerchants, "zip", "ZIP.co", new[] { "ZIP.CO" }, "https://b9devaccount.blob.core.windows.net/static-documents-container/TransactionMerchants/zip_co.png");
            AddMerchant(merchantsToAdd, existingMerchants, "chumbacasino", "Chumbacasino", new[] { "CHUMBA GOLD" }, "https://b9devaccount.blob.core.windows.net/static-documents-container/TransactionMerchants/chumba_casino.png");
            AddMerchant(merchantsToAdd, existingMerchants, "luckyland", "Luckyland", new[] { "LUCKYLAND", "711 LUCKY LAND" }, "https://b9devaccount.blob.core.windows.net/static-documents-container/TransactionMerchants/luckyland.png");
            AddMerchant(merchantsToAdd, existingMerchants, "sezzle", "SEZZLE", new[] { "SEZZLE" }, "https://b9devaccount.blob.core.windows.net/static-documents-container/TransactionMerchants/sezzle.png");
            AddMerchant(merchantsToAdd, existingMerchants, "tmobile", "TMOBILE", new[] { "TMOBILE" }, "https://b9devaccount.blob.core.windows.net/static-documents-container/TransactionMerchants/tmobile.png");
            AddMerchant(merchantsToAdd, existingMerchants, "microsoft", "Microsoft", new[] { "MICROSOFT" }, "https://b9devaccount.blob.core.windows.net/static-documents-container/TransactionMerchants/microsoft.png");
            AddMerchant(merchantsToAdd, existingMerchants, "spotify", "Spotify", new[] { "SPOTIFY" }, "https://b9devaccount.blob.core.windows.net/static-documents-container/TransactionMerchants/spotify.png");
            AddMerchant(merchantsToAdd, existingMerchants, "skillz", "Skillz", new[] { "SKILLZ" }, "https://b9devaccount.blob.core.windows.net/static-documents-container/TransactionMerchants/skillz.png");
            AddMerchant(merchantsToAdd, existingMerchants, "moneygram", "MONEYGRAM", new[] { "MONEYGRAM" }, "https://b9devaccount.blob.core.windows.net/static-documents-container/TransactionMerchants/moneygram.png");
            AddMerchant(merchantsToAdd, existingMerchants, "7eleven", "7-ELEVEN", new[] { "7-ELEVEN" }, "https://b9devaccount.blob.core.windows.net/static-documents-container/TransactionMerchants/7eleven.png");
            AddMerchant(merchantsToAdd, existingMerchants, "crypto", "Crypto.com", new[] { "CRYPTO.COM", "2CRYPTO.COM" }, "https://b9devaccount.blob.core.windows.net/static-documents-container/TransactionMerchants/crypto_com.png");

            await context.TransferMerchantsSettings.AddRangeAsync(merchantsToAdd);
            await context.SaveChangesAsync();
        }

        private static void AddMerchant(List<TransferMerchantsSettings> entitiesList, List<TransferMerchantsSettings> existingMerchants,
            string name, string displayName, string[] descriptionKeywords, string pictureUrl, int priority = 0)
        {
            var existingMerchant = existingMerchants.FirstOrDefault(x => x.MerchantName.Equals(name, StringComparison.CurrentCultureIgnoreCase));
            if (existingMerchant != null)
            {
                existingMerchant.MerchantDisplayName = displayName ?? existingMerchant.MerchantDisplayName;
                existingMerchant.PictureUrl = pictureUrl ?? existingMerchant.PictureUrl;
                return;
            }
            var entity = new TransferMerchantsSettings
            {
                MerchantName = name,
                MerchantDisplayName = displayName,
                MerchantDescriptionKeywords = ImmutableSortedSet.Create(descriptionKeywords),
                CreatedAt = DateTime.Now,
                PictureUrl = pictureUrl,
                Priority = priority
            };
            entitiesList.Add(entity);
        }
    }
}

