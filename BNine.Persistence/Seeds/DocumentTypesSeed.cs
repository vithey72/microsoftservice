﻿namespace BNine.Persistence.Seeds;

using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Application.Interfaces;
using BNine.Constants;
using Domain.Entities.Common;
using Enums;
using DocumentType = Domain.ListItems.DocumentType;

public static class DocumentTypesSeed
{
    public static async Task SeedAsync(IBNineDbContext context)
    {
        if (!context.DocumentTypes.Any())
        {
            context.DocumentTypes.AddRange(
                new DocumentType
                {
                    Key = ClientIdentifierDocumentTypes.Passport,
                    Value = new List<LocalizedText>
                    {
                            new LocalizedText { Language = Language.English, Text = "U.S. Passport card/bio data page" },
                            new LocalizedText
                            {
                                Language = Language.Spanish,
                                Text = "Tarjeta de pasaporte de EE. UU. / Página de datos biográficos"
                            }
                    },
                    ExternalName = "Passport",
                    Order = 1
                },
                new DocumentType
                {
                    Key = ClientIdentifierDocumentTypes.ForeignDriverLicense,
                    Value = new List<LocalizedText>
                    {
                            new LocalizedText { Language = Language.English, Text = "Foreign Driver's License" },
                            new LocalizedText { Language = Language.Spanish, Text = "[SE]Foreign Driver's License" }
                    },
                    ExternalName = "Foreign Driver's License",
                    Order = 2
                },
                new DocumentType
                {
                    Key = ClientIdentifierDocumentTypes.ConsularId,
                    Value = new List<LocalizedText>
                    {
                            new LocalizedText { Language = Language.English, Text = "Consular ID" },
                            new LocalizedText { Language = Language.Spanish, Text = "[SE]Consular ID" }
                    },
                    ExternalName = "Consular ID",
                    Order = 3
                },
                new DocumentType
                {
                    Key = ClientIdentifierDocumentTypes.Visa,
                    Value = new List<LocalizedText>
                    {
                            new LocalizedText { Language = Language.English, Text = "Visa" },
                            new LocalizedText { Language = Language.Spanish, Text = "[SE]Visa" }
                    },
                    ExternalName = "Visa",
                    Order = 4
                },
                new DocumentType
                {
                    Key = ClientIdentifierDocumentTypes.Other,
                    Value = new List<LocalizedText>
                    {
                            new LocalizedText { Language = Language.English, Text = "Other" },
                            new LocalizedText { Language = Language.Spanish, Text = "[SE]Other" }
                    },
                    ExternalName = "Other",
                    Order = 5
                },
                new DocumentType
                {
                    Key = ClientIdentifierDocumentTypes.SSN,
                    Value = new List<LocalizedText>
                    {
                            new LocalizedText { Language = Language.English, Text = "Social Security Number" },
                            new LocalizedText { Language = Language.Spanish, Text = "[SE]Social Security Number" }
                    },
                    ExternalName = "Social Security Number",
                    Order = 6
                },
                new DocumentType
                {
                    Key = ClientIdentifierDocumentTypes.DriverLicense,
                    Value = new List<LocalizedText>
                    {
                            new LocalizedText { Language = Language.English, Text = "Driver license" },
                            new LocalizedText { Language = Language.Spanish, Text = "Licencia de conducir" }
                    },
                    ExternalName = "Driver's License",
                    Order = 7
                },
                new DocumentType
                {
                    Key = ClientIdentifierDocumentTypes.StateIdentityCard,
                    Value = new List<LocalizedText>
                    {
                            new LocalizedText { Language = Language.English, Text = "State Identity Card" },
                            new LocalizedText { Language = Language.Spanish, Text = "[SE]State Identity Card" }
                    },
                    ExternalName = "State Identity Card",
                    Order = 8
                },
                new DocumentType
                {
                    Key = ClientIdentifierDocumentTypes.WorkPermitCard,
                    Value = new List<LocalizedText>
                    {
                            new LocalizedText { Language = Language.English, Text = "Work Permit Card" },
                            new LocalizedText { Language = Language.Spanish, Text = "Tarjeta de permiso de trabajo" }
                    },
                    ExternalName = "Work permit (EAD) card",
                    Order = 9
                },
                new DocumentType
                {
                    Key = ClientIdentifierDocumentTypes.PermanentResidentCard,
                    Value = new List<LocalizedText>
                    {
                            new LocalizedText { Language = Language.English, Text = "Permanent Resident Card" },
                            new LocalizedText { Language = Language.Spanish, Text = "[SE]Permanent Resident Card" }
                    },
                    ExternalName = "Permanent Resident Card",
                    Order = 10
                },
                new DocumentType
                {
                    Key = ClientIdentifierDocumentTypes.ForeignPassport,
                    Value = new List<LocalizedText>
                    {
                            new LocalizedText { Language = Language.English, Text = "Foreign Passport Biodata paged" },
                            new LocalizedText
                                { Language = Language.Spanish, Text = "Página de datos personales del pasaporte extranjero" }
                    },
                    ExternalName = "Foreign Passport",
                    Order = 11
                },
                new DocumentType
                {
                    Key = ClientIdentifierDocumentTypes.ITIN,
                    Value = new List<LocalizedText>
                    {
                            new LocalizedText { Language = Language.English, Text = "Taxpayer Identification Number" },
                            new LocalizedText { Language = Language.Spanish, Text = "[SE]Taxpayer Identification Number" }
                    },
                    ExternalName = "Taxpayer Identification Number",
                    Order = 12
                });

            await context.SaveChangesAsync(CancellationToken.None);
        }
    }
}
