﻿namespace BNine.Persistence.Configurations.Plaid
{
    using System.Linq;
    using Domain.Entities;
    using Extensions;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.EntityFrameworkCore.Metadata.Builders;
    using Newtonsoft.Json;

    public class PlaidConfiguration : IEntityTypeConfiguration<Plaid>
    {
        public void Configure(EntityTypeBuilder<Plaid> builder)
        {
            builder.MapTable("Plaid", "raw_data");

            builder.Property(x => x.CreatedAt)
                .IsRequired()
                .ValueGeneratedOnAdd()
                .HasDefaultValueSql("getutcdate()");

            builder.Property(x => x.UpdatedAt)
                .IsRequired()
                .ValueGeneratedOnAdd()
                .HasDefaultValueSql("getutcdate()");

            builder.Property(x => x.Transactions).HasConversion(
                x => JsonConvert.SerializeObject(x.Select(JsonConvert.DeserializeObject<object>)),
                x => JsonConvert.DeserializeObject<object[]>(x).Select(JsonConvert.SerializeObject).ToList());

            builder.HasOne(x => x.BankAccount)
                .WithOne(x => x.PlaidData)
                .HasForeignKey<Plaid>(x => x.BankAccountId)
                .OnDelete(DeleteBehavior.Cascade);

            builder.HasIndex(x => x.UpdatedAt)
                .HasDatabaseName("IX_Plaid_UpdatedAt");
        }
    }
}
