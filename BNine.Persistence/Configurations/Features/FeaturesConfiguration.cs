﻿namespace BNine.Persistence.Configurations.Features
{
    using BNine.Domain.Entities.Features;
    using Extensions;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.EntityFrameworkCore.Metadata.Builders;

    public class FeaturesConfiguration : IEntityTypeConfiguration<Feature>
    {
        public void Configure(EntityTypeBuilder<Feature> builder)
        {
            builder.MapTable("Features", "features");
        }
    }
}
