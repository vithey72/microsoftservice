﻿namespace BNine.Persistence.Configurations.Features
{
    using System;
    using BNine.Domain.Entities.Features;
    using BNine.Domain.Entities.Features.Groups;
    using Extensions;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.EntityFrameworkCore.Metadata.Builders;

    public class GroupsConfiguration : IEntityTypeConfiguration<Group>
    {
        public void Configure(EntityTypeBuilder<Group> builder)
        {
            builder.MapTable("Groups", "features")
                .HasDiscriminator(g => g.Kind)
                .HasValue<DefaultGroup>(GroupKind.Default)
                .HasValue<SampleGroup>(GroupKind.Sample)
                .HasValue<UserListGroup>(GroupKind.UserList)
                .HasValue<PersonalGroup>(GroupKind.Personal);
        }
    }

    public class DefaultGroupsConfiguration : IEntityTypeConfiguration<DefaultGroup>
    {
        public void Configure(EntityTypeBuilder<DefaultGroup> builder)
        {
            builder.HasData(new DefaultGroup
            {
                Id = Guid.Parse("731c70e5-d85b-ec11-94f6-000d3a338cd7"),
                CreatedAt = DateTime.Parse("2021-12-13 05:52:47.1900000"),
                UpdatedAt = DateTime.Parse("2021-12-13 05:52:47.1900000"),
                Name = "Default",
                Kind = GroupKind.Default,
            });
        }
    }
}
