﻿namespace BNine.Persistence.Configurations.Features
{
    using BNine.Domain.Entities.Features;
    using Extensions;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.EntityFrameworkCore.Metadata.Builders;

    public class FeatureGroupsConfiguration : IEntityTypeConfiguration<FeatureGroup>
    {
        public void Configure(EntityTypeBuilder<FeatureGroup> builder)
        {
            builder.MapTable("FeatureGroups", "features");

            builder.Property(fg => fg.MinAppVersion)
                .HasConversion(
                    v => v.ToString(),
                    v => Version.Parse(v));

            builder.HasOne(x => x.Feature)
                .WithMany(x => x.FeatureGroups)
                .HasForeignKey(x => x.FeatureId);

            builder.HasOne(x => x.Group)
                .WithMany(x => x.FeatureGroups)
                .HasForeignKey(x => x.GroupId);

            builder.HasIndex(g => g.GroupId);

            builder
                .HasIndex(f => new { f.GroupId, f.FeatureId })
                .HasFilter(
                    @"[MaxAccountAgeHours] IS NULL
                    AND [MinAccountAgeHours] IS NULL
                    AND [MinRegistrationDate] IS NULL
                    AND [MinAppVersion] IS NULL
                    AND [MobilePlatform] IS NULL")
                .IsUnique();
        }
    }
}
