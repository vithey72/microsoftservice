﻿namespace BNine.Persistence.Configurations.Features
{
    using BNine.Domain.Entities.Features;
    using Extensions;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.EntityFrameworkCore.Metadata.Builders;

    public class UserGroupsConfiguration : IEntityTypeConfiguration<UserGroup>
    {
        public void Configure(EntityTypeBuilder<UserGroup> builder)
        {
            builder.MapTable("UserGroups", "features");

            builder.HasOne(x => x.User)
                .WithMany(x => x.UserGroups)
                .HasForeignKey(x => x.UserId);

            builder.HasOne(x => x.Group)
                .WithMany(x => x.UserGroups)
                .HasForeignKey(x => x.GroupId);

            builder.HasIndex(u => u.GroupId);

            builder.HasIndex(u => new { u.GroupId, u.UserId }).IsUnique();
        }
    }
}
