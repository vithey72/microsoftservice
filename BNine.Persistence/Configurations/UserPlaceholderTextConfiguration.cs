﻿namespace BNine.Persistence.Configurations;

using Domain.Entities;
using Extensions;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

public class UserPlaceholderTextConfiguration : IEntityTypeConfiguration<UserPlaceholderText>
{
    public void Configure(EntityTypeBuilder<UserPlaceholderText> builder)
    {
        builder.MapTable("UserPlaceholderTexts");

        builder.Property(uc => uc.Key)
            .HasMaxLength(100)
            .IsRequired();

        builder.Property(uc => uc.Value)
            .HasMaxLength(4000)
            .IsRequired();

        builder.HasIndex(x => new { x.UserId, x.Key })
            .HasDatabaseName("IX_UserPlaceholderTexts_UserId_Key")
            .IsUnique();

        builder.Property(uc => uc.CreatedAt)
            .HasDefaultValueSql("GETDATE()");

        builder.HasOne(x => x.User)
            .WithMany(x => x.UserPlaceholderTexts)
            .HasForeignKey(x => x.UserId);
    }
}
