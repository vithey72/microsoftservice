﻿namespace BNine.Persistence.Configurations
{
    using Domain.Entities;
    using Extensions;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.EntityFrameworkCore.Metadata.Builders;

    public class BonusCodesConfiguration : IEntityTypeConfiguration<BonusCode>
    {
        public void Configure(EntityTypeBuilder<BonusCode> builder)
        {
            builder.MapTable("BonusCodes");

            builder.Property(x => x.Code).IsRequired().HasMaxLength(6);

            builder.HasOne(x => x.Receiver)
                .WithOne()
                .HasForeignKey<BonusCode>(x => x.ReceiverId)
                .IsRequired(false)
                .OnDelete(DeleteBehavior.NoAction);

            builder.HasOne(x => x.Sender)
                .WithMany()
                .HasForeignKey(x => x.SenderId)
                .OnDelete(DeleteBehavior.Cascade);

            builder.HasMany(x => x.Activations)
                .WithOne(x => x.BonusCode)
                .HasForeignKey(x => x.BonusCodeId)
                .OnDelete(DeleteBehavior.Cascade);

            builder.HasIndex(x => x.UpdatedAt)
                .HasDatabaseName("IX_BonusCodes_UpdatedAt");
        }
    }
}
