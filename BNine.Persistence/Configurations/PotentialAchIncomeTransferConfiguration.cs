﻿namespace BNine.Persistence.Configurations
{
    using Domain.Entities;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.EntityFrameworkCore.Metadata.Builders;

    public class PotentialAchIncomeTransferConfiguration : IEntityTypeConfiguration<PotentialAchIncomeTransfer>
    {
        public void Configure(EntityTypeBuilder<PotentialAchIncomeTransfer> builder)
        {
            builder.ToTable("PotentialAchIncomeTransfers");

            builder.Property(x => x.Reference)
                .IsRequired()
                .HasMaxLength(200);

            builder.Property(x => x.TransferExternalId)
                .IsRequired();

            builder.Property(x => x.CreatedAt)
                .HasDefaultValueSql("GETDATE()");

            builder.HasIndex(x => x.Reference)
                .HasDatabaseName("IX_PotentialAchIncomeTransfers_Reference");

            builder.HasIndex(x => x.CreatedAt)
                .HasDatabaseName("IX_PotentialAchIncomeTransfers_CreatedAt");

            builder.HasIndex(x => x.TransferExternalId)
                .HasDatabaseName("IX_PotentialAchIncomeTransfers_TransferExternalId")
                .IsUnique();

            builder.HasOne(x => x.User)
                .WithMany(x => x.PotentialAchIncomeTransfers)
                .HasForeignKey(x => x.UserId)
                ;
        }
    }
}
