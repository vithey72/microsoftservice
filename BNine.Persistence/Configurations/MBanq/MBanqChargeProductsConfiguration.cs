﻿namespace BNine.Persistence.Configurations.MBanq
{
    using BNine.Domain.Entities.MBanq;
    using BNine.Persistence.Configurations.Extensions;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.EntityFrameworkCore.Metadata.Builders;

    public class MBanqChargeProductsConfiguration : IEntityTypeConfiguration<MBanqChargeProduct>
    {
        public void Configure(EntityTypeBuilder<MBanqChargeProduct> builder)
        {
            builder.MapTable("MBanqChargeProducts", "mbanq");
        }
    }
}
