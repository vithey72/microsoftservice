﻿namespace BNine.Persistence.Configurations;

using Domain.Entities.CashbackProgram;
using Extensions;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Newtonsoft.Json;

public class CashbackCategoriesConfiguration : IEntityTypeConfiguration<CashbackCategory>
{
    public void Configure(EntityTypeBuilder<CashbackCategory> builder)
    {
        builder.MapTable("CashbackCategories", "settings");

        builder.Property(cc => cc.Name).IsRequired();
        builder.Property(cc => cc.Percentage).IsRequired().HasColumnType("decimal(18, 6)");
        builder.Property(cc => cc.ReadableName).IsRequired();
        builder.Property(cc => cc.UpdatedAt).IsRequired(false);
        builder.Property(cc => cc.IsDisabled).HasDefaultValue(false);

        builder.Property(cc => cc.CreatedAt).HasDefaultValueSql("GETDATE()");

        builder.Property(x => x.MccCodes).HasConversion(
            x => JsonConvert.SerializeObject(x),
            x => JsonConvert.DeserializeObject<int[]>(x));
    }
}
