﻿namespace BNine.Persistence.Configurations.User;

using BNine.Domain.Entities.User;
using BNine.Persistence.Configurations.Extensions;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

public class AccountClosureScheduleConfiguration : IEntityTypeConfiguration<AccountClosureSchedule>
{
    public void Configure(EntityTypeBuilder<AccountClosureSchedule> builder)
    {
        builder.MapBase("AccountClosureSchedule", "user");

        builder.Property(x => x.CreatedAt)
            .IsRequired(true)
            .ValueGeneratedOnAdd()
            .HasDefaultValueSql("getutcdate()");

        builder.HasOne(x => x.User)
            .WithOne(x => x.AccountClosureSchedule)
            .HasForeignKey<AccountClosureSchedule>(x => x.UserId)
            .IsRequired(true)
            .OnDelete(DeleteBehavior.Cascade);

        builder.HasIndex(x => x.CreatedAt)
            .HasDatabaseName("IX_AccountClosureSchedule_CreatedAt");
    }
}
