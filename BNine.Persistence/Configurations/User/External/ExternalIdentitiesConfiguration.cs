﻿//namespace BNine.Persistence.Configurations.User.External
//{
//    using Domain.Entities.User;
//    using Domain.Entities.User.External;
//    using Domain.ListItems;
//    using Extensions;
//    using Microsoft.EntityFrameworkCore;
//    using Microsoft.EntityFrameworkCore.Metadata.Builders;

//    public class ExternalIdentitiesConfiguration : IEntityTypeConfiguration<ExternalIdentity>
//    {
//        public void Configure(EntityTypeBuilder<ExternalIdentity> builder)
//        {
//            builder.MapExternalEntity("ExternalIdentities");

//            builder.Property(x => x.ProvidedDocumentTypeKey).HasEnumKeyMaxLength();

//            builder.HasOne(x => x.ProvidedDocumentType)
//                .WithMany()
//                .HasForeignKey(x => x.ProvidedDocumentTypeKey)
//                .OnDelete(DeleteBehavior.NoAction);

//            builder.HasOne(x => x.User)
//                .WithOne(x => x.ExternalIdentity)
//                .HasForeignKey<ExternalIdentity>(x => x.UserId)
//                .OnDelete(DeleteBehavior.Cascade);

//            builder.HasOne(x => x.ExternalClient)
//                .WithOne(x => x.ExternalIdentity)
//                .HasForeignKey<ExternalIdentity>(x => x.ExternalClientId)
//                .OnDelete(DeleteBehavior.NoAction);
//        }
//    }
//}
