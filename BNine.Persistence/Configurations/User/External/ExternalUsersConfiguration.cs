﻿//namespace BNine.Persistence.Configurations.User.External
//{
//    using Domain.Entities.User.External;
//    using Extensions;
//    using Microsoft.EntityFrameworkCore;
//    using Microsoft.EntityFrameworkCore.Metadata.Builders;

//    public class ExternalUsersConfiguration : IEntityTypeConfiguration<ExternalUser>
//    {
//        public void Configure(EntityTypeBuilder<ExternalUser> builder)
//        {
//            builder.MapExternalEntity("ExternalUsers");

//            builder.HasOne(x => x.User)
//                .WithOne(x => x.ExternalUser)
//                .HasForeignKey<ExternalUser>(x => x.UserId)
//                .OnDelete(DeleteBehavior.Cascade);
//        }
//    }
//}
