﻿//namespace BNine.Persistence.Configurations.User.External
//{
//    using Domain.Entities.User.External;
//    using Extensions;
//    using Microsoft.EntityFrameworkCore;
//    using Microsoft.EntityFrameworkCore.Metadata.Builders;

//    public class ExternalTinNumbersConfiguration : IEntityTypeConfiguration<ExternalTinNumber>
//    {
//        public void Configure(EntityTypeBuilder<ExternalTinNumber> builder)
//        {
//            builder.MapExternalEntity("ExternalTinNumbers");

//            builder.HasOne(x => x.User)
//                .WithOne(x => x.ExternalTinNumber)
//                .HasForeignKey<ExternalTinNumber>(x => x.UserId)
//                .OnDelete(DeleteBehavior.Cascade);

//            builder.HasOne(x => x.ExternalClient)
//                .WithOne(x => x.ExternalTinNumber)
//                .HasForeignKey<ExternalTinNumber>(x => x.ExternalClientId)
//                .OnDelete(DeleteBehavior.NoAction);
//        }
//    }
//}
