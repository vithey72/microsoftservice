﻿namespace BNine.Persistence.Configurations.User.EarlySalary
{
    using BNine.Domain.Entities.User.EarlySalary;
    using BNine.Persistence.Configurations.Extensions;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.EntityFrameworkCore.Metadata.Builders;

    public class EarlySalaryManualRequestsConfiguration : IEntityTypeConfiguration<EarlySalaryManualRequest>
    {
        public void Configure(EntityTypeBuilder<EarlySalaryManualRequest> builder)
        {
            builder.MapTable("EarlySalaryManualRequests");

            builder.Property(x => x.PercentAllocation).HasColumnType("decimal(18, 2)").IsRequired(false);
            builder.Property(x => x.AmountAllocation).HasColumnType("decimal(18, 2)").IsRequired(false);
            builder.Property(x => x.Email).IsRequired().HasMediumMaxLength();
            builder.Property(x => x.EmployerEmail).HasMediumMaxLength();
            builder.Property(x => x.EmployerName).HasMediumMaxLength();
            builder.Property(x => x.EmployerContactPerson).HasMediumMaxLength();

            builder.HasOne(x => x.User)
                .WithMany()
                .HasForeignKey(x => x.UserId)
                .OnDelete(DeleteBehavior.Cascade);

            builder.Ignore(x => x.DomainEvents);
        }
    }
}
