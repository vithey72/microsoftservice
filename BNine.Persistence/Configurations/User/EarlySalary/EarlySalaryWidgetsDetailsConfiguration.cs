﻿namespace BNine.Persistence.Configurations.User.EarlySalary
{
    using Domain.Entities.User.EarlySalary;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.EntityFrameworkCore.Metadata.Builders;

    public class EarlySalaryWidgetsDetailsConfiguration : IEntityTypeConfiguration<EarlySalaryWidgetConfiguration>
    {
        public void Configure(EntityTypeBuilder<EarlySalaryWidgetConfiguration> builder)
        {
            builder.ToTable("EarlySalaryWidgetConfigurations");
            builder.HasKey(x => x.UserId);

            builder.Property(x => x.WidgetUserId).IsRequired();
            builder.Property(x => x.Token).IsRequired();

            builder.HasOne(x => x.User)
                .WithOne(x => x.EarlySalaryWidgetConfiguration)
                .HasForeignKey<EarlySalaryWidgetConfiguration>(x => x.UserId)
                .OnDelete(DeleteBehavior.Cascade);
        }
    }
}
