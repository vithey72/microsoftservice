﻿namespace BNine.Persistence.Configurations.User
{
    using System;
    using BNine.Constants;
    using BNine.Domain.Entities.User;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.EntityFrameworkCore.Metadata.Builders;

    public class UserBlockReasonsConfiguration
     : IEntityTypeConfiguration<BlockUserReason>
    {
        public void Configure(EntityTypeBuilder<BlockUserReason> builder)
        {
            builder.ToTable("UserBlockReasons", "user");

            builder.HasKey(x => x.Id);

            builder.HasData(
                new BlockUserReason
                {
                    Id = Guid.Parse("7060a426-6f25-431a-a44f-dd40d3dd5dac"),
                    Value = "Voluntary request",
                    Order = 1
                },
                new BlockUserReason
                {
                    Id = Guid.Parse("c92babbf-b706-4c08-8cea-7a83a25e1569"),
                    Value = "Illegible ID",
                    Order = 2
                },
                new BlockUserReason
                {
                    Id = Guid.Parse("852db38e-1074-41b3-b3ed-a198ae8499cc"),
                    Value = "Fraud/Risk rules",
                    Order = 3
                },
                new BlockUserReason()
                {
                    Id = Guid.Parse("c2b0b2a0-5b0a-4b0e-8b0a-5b0a4b0e8b0a"),
                    Value = "Voluntary self-serviced request",
                    Order = 4
                },
                new BlockUserReason()
                {
                    Id = ClientAccount.UserBlockReasonIdForPendingClosure,
                    Value = "Pending account closure",
                    Order = 5,
                });
        }
    }
}
