﻿namespace BNine.Persistence.Configurations.User
{
    using BNine.Domain.Entities.User;
    using BNine.Persistence.Configurations.Extensions;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.EntityFrameworkCore.Metadata.Builders;

    public class ChangeBlockStatusEventsConfiguration : IEntityTypeConfiguration<ChangeBlockStatusEvent>
    {
        public void Configure(EntityTypeBuilder<ChangeBlockStatusEvent> builder)
        {
            builder.MapTable("ChangeBlockStatusEvents", "logs");

            builder.HasOne(x => x.UserBlockReason)
                .WithMany()
                .HasForeignKey(x => x.UserBlockReasonId)
                .IsRequired(false);

            builder.HasOne(x => x.User)
                .WithMany()
                .HasForeignKey(x => x.UserId)
                .OnDelete(DeleteBehavior.Cascade);

            builder.HasIndex(x => x.CreatedAt)
                .HasDatabaseName("IX_ChangeBlockStatusEvents_CreatedAt");
        }
    }
}
