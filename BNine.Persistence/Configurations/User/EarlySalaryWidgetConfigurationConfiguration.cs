﻿namespace BNine.Persistence.Configurations.User
{
    using BNine.Domain.Entities.User.EarlySalary;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.EntityFrameworkCore.Metadata.Builders;

    public class EarlySalaryWidgetConfigurationConfiguration : IEntityTypeConfiguration<EarlySalaryWidgetConfiguration>
    {
        public void Configure(EntityTypeBuilder<EarlySalaryWidgetConfiguration> builder)
        {
            builder.HasIndex(x => x.UpdatedAt)
                .HasDatabaseName("IX_EarlySalaryWidgetConfigurations_UpdatedAt");
        }
    }
}
