﻿namespace BNine.Persistence.Configurations.User;

using BNine.Domain.Entities.User.EarlySalary;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

public class TruvAccountConfiguration : IEntityTypeConfiguration<TruvAccount>
{
    public void Configure(EntityTypeBuilder<TruvAccount> builder)
    {
        builder.ToTable("TruvAccount");
        builder.HasKey(x => x.UserId);

        builder.Property(x => x.TruvUserId).IsRequired();

        builder.HasOne(x => x.User)
            .WithOne(x => x.TruvAccount)
            .HasForeignKey<TruvAccount>(x => x.UserId)
            .OnDelete(DeleteBehavior.Cascade);

        builder.HasIndex(p => p.TruvUserId);
    }
}
