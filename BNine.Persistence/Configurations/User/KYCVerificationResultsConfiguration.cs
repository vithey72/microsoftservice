﻿namespace BNine.Persistence.Configurations.User
{
    using Domain.Entities.User;
    using Enums;
    using Extensions;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.EntityFrameworkCore.Metadata.Builders;

    public class KYCVerificationResultsConfiguration : IEntityTypeConfiguration<KYCVerificationResult>
    {
        public void Configure(EntityTypeBuilder<KYCVerificationResult> builder)
        {
            builder.MapTable("KYCVerificationResults");

            builder.Property(x => x.EntityToken)
                .HasMediumMaxLength()
                .IsRequired();

            builder.Property(x => x.EvaluationToken)
                .HasMediumMaxLength()
                .IsRequired();

            builder.Property(x => x.Status)
                .HasEnumKeyMaxLength()
                .IsRequired();

            builder.Property(x => x.CreatedAt)
                .IsRequired()
                .ValueGeneratedOnAdd()
                .HasDefaultValueSql("getutcdate()");

            builder.Property(x => x.RawEvaluation)
                .IsRequired();

            builder.HasOne(x => x.User)
                .WithMany(x => x.KycVerificationResults)
                .HasForeignKey(x => x.UserId)
                .OnDelete(DeleteBehavior.Cascade);

            builder.Property(x => x.StatusEnum)
                .IsRequired()
                .HasDefaultValue(KycStatusEnum.Unknown);

            builder.Property(x => x.EvaluationProvider)
                .IsRequired()
                .HasDefaultValue(EvaluationProviderEnum.Alloy);

            builder.Property(x => x.OverridenByRule).HasMaxLength(100);

            builder.HasIndex(x => x.CreatedAt)
                .HasDatabaseName("IX_KYCVerificationResults_CreatedAt");
        }
    }
}
