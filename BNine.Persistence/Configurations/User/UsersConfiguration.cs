﻿namespace BNine.Persistence.Configurations.User
{
    using Domain.Entities.User;
    using Domain.Entities.User.UserDetails.Settings;
    using Enums;
    using Extensions;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.EntityFrameworkCore.Metadata.Builders;
    using Newtonsoft.Json;

    public class UsersConfiguration
        : IEntityTypeConfiguration<User>
    {
        public void Configure(EntityTypeBuilder<User> builder)
        {
            builder.MapBase("Users", "user");

            builder.Property(x => x.CreatedAt)
                .IsRequired(true)
                .ValueGeneratedOnAdd()
                .HasDefaultValueSql("getutcdate()");

            builder.Property(x => x.Email)
                .HasMaxLength(512)
                .IsRequired();

            builder.Property(x => x.Phone)
               .HasPhoneMaxLength()
               .IsRequired();

            builder.Property(x => x.FirstName)
                .HasMediumMaxLength()
                .IsRequired(false);

            builder.Property(x => x.LastName)
                .HasMediumMaxLength()
                .IsRequired(false);

            builder.Property(x => x.DateOfBirth)
                .HasColumnType("date")
                .IsRequired(false);

            builder.Property(x => x.Password)
                .IsRequired();

            builder.Property(x => x.AppsflyerId)
                .IsRequired();

            builder.Property(x => x.IsBlocked)
                .HasDefaultValue(false);

            builder
                .Property(x => x.SysStartTime)
                .ValueGeneratedOnAddOrUpdate();

            builder.OwnsOne(x => x.Address, ba =>
            {
                ba.ToTable("Addresses", "user");
                ba.WithOwner().HasForeignKey("UserId");

                ba.Property(x => x.City).IsRequired();
                ba.Property(x => x.Unit).IsRequired(false);
                ba.Property(x => x.AddressLine).IsRequired();
                ba.Property(x => x.State).IsRequired().HasMaxLength(2);
                ba.Property(x => x.ZipCode).IsRequired().HasMaxLength(12);
                ba.Property(x => x.SysStartTime).ValueGeneratedOnAddOrUpdate();
            });

            builder.OwnsOne(x => x.ClientRating, ba =>
            {
                ba.ToTable("ClientRatings", "user");
                ba.Property(x => x.CreditRating).HasColumnType("decimal(18, 6)").IsRequired();
                ba.Property(x => x.SysStartTime).ValueGeneratedOnAddOrUpdate();
                ba.WithOwner().HasForeignKey("UserId");
            });

            builder.OwnsOne(x => x.ClientLoanSettings, ba =>
            {
                ba.ToTable("ClientLoanSettings", "user");
                ba.Property(x => x.LoanLimit).HasColumnType("decimal(18, 6)").IsRequired();
                ba.Property(x => x.SysStartTime).ValueGeneratedOnAddOrUpdate();
                ba.WithOwner().HasForeignKey("UserId");
            });

            builder.OwnsOne(x => x.ToSAcceptance, ba =>
            {
                ba.ToTable("ToSAcceptances", "user");
                ba.WithOwner().HasForeignKey("UserId");

                ba.Property(x => x.Time).IsRequired();
                ba.Property(x => x.Ip).IsRequired();
            });

            builder.OwnsOne(x => x.EarlySalarySettings, ba =>
            {
                ba.ToTable("EarlySalarySettings");
                ba.WithOwner().HasForeignKey("UserId");

                ba.Property(x => x.EarlySalarySubscriptionDate).IsRequired();
                ba.Property(x => x.SysStartTime).ValueGeneratedOnAddOrUpdate();
            });

            builder.OwnsOne(x => x.Settings, ba =>
            {
                ba.ToTable("UserSettings");
                ba.WithOwner().HasForeignKey("UserId");

                ba.Property(x => x.IsResetPasswordAllowed);
                ba.Property(x => x.IsReceivedBonusBlockEnabled);
                ba.Property(x => x.IsActivationEligibilityBlockEnabled)
                  .HasDefaultValue(true);
                ba.Property(x => x.SysStartTime).ValueGeneratedOnAddOrUpdate();
                ba.Property(x => x.PaycheckSwitchProvider).IsRequired().HasDefaultValue(PaycheckSwitchProvider.Argyle);
            });

            builder.OwnsOne(x => x.Settings, ba =>
            {
                ba.ToTable("UserSettings");
                ba.WithOwner().HasForeignKey("UserId");

                ba.Property(x => x.IsResetPasswordAllowed);
                ba.Property(x => x.IsReceivedBonusBlockEnabled);
                ba.Property(x => x.SysStartTime).ValueGeneratedOnAddOrUpdate();
                ba.Property(x => x.ConfirmedAgreementItems)
                    .HasConversion(
                        v => JsonConvert.SerializeObject(v,
                            new JsonSerializerSettings { NullValueHandling = NullValueHandling.Ignore }),
                        v => JsonConvert.DeserializeObject<List<string>>(v,
                            new JsonSerializerSettings { NullValueHandling = NullValueHandling.Ignore })
                    );
            });

            builder.OwnsOne(x => x.Identity, ba =>
            {
                ba.ToTable("Identities", "user");
                ba.WithOwner().HasForeignKey("UserId");

                ba.Property(x => x.Value).HasMaxLength(9);
                    ba.HasIndex(x => x.Value).IsUnique(false).HasDatabaseName("IX_Identities_Value");

                ba.Property(x => x.Type).IsRequired();
                ba.Property(x => x.ExternalId).IsRequired(false);
                ba.Property(x => x.SysStartTime).ValueGeneratedOnAddOrUpdate();
            });
            builder.OwnsOne(x => x.Document, ba =>
            {
                ba.ToTable("Documents", "user");
                ba.WithOwner().HasForeignKey("UserId");

                ba.HasOne(x => x.Type)
                    .WithMany()
                    .HasForeignKey(x => x.TypeKey)
                    .OnDelete(DeleteBehavior.NoAction);
                ba.Property(x => x.SysStartTime).ValueGeneratedOnAddOrUpdate();

                ba.Property(x => x.OcrProvider).HasDefaultValue(EvaluationProviderEnum.Alloy);
                ba.Property(x => x.DocumentScansExternalId).IsRequired(false);

                ba.Property(x => x.Number).HasMaxLength(40);
                ba.HasIndex(x => x.Number).IsUnique(false).HasDatabaseName("IX_Documents_Number");
            });

            builder.HasMany(x => x.BankAccounts)
                .WithOne(x => x.User)
                .HasForeignKey(x => x.UserId)
                .OnDelete(DeleteBehavior.Cascade);

            builder.HasMany(x => x.Devices)
                .WithOne(x => x.User)
                .HasForeignKey(x => x.UserId)
                .OnDelete(DeleteBehavior.Cascade);

            builder.HasMany(x => x.DebitCards)
                .WithOne(x => x.User)
                .HasForeignKey(x => x.UserId)
                .OnDelete(DeleteBehavior.Cascade);

            builder.HasMany(x => x.Notifications)
                .WithOne(x => x.User)
                .HasForeignKey(x => x.UserId)
                .OnDelete(DeleteBehavior.Cascade);

            builder.HasMany(x => x.LoanAccounts)
                .WithOne(x => x.User)
                .HasForeignKey(x => x.UserId)
                .OnDelete(DeleteBehavior.Cascade);

            builder.HasMany(x => x.KycVerificationResults)
                .WithOne(x => x.User)
                .HasForeignKey(x => x.UserId)
                .OnDelete(DeleteBehavior.Cascade);

            builder.Ignore(x => x.DomainEvents);

            builder.HasIndex(x => x.Phone)
                .HasDatabaseName("IX_Users_Phone")
                .IsUnique(false);

            builder.OwnsOne(x => x.AlltrustCustomerSettings, ba =>
            {
                ba.ToTable("AlltrustCustomerSettings", "user");
                ba.WithOwner().HasForeignKey("UserId");

                ba.Property(x => x.AlltrustCustomerId).IsRequired(false);
                ba.HasIndex(x => x.AlltrustCustomerId)
                  .HasDatabaseName("IX_AlltrustCustomerSettings_AlltrustCustomerId")
                  .IsUnique(true);

                ba.HasOne(x => x.CurrentEmployer)
                    .WithMany()
                    .HasForeignKey(x => x.CurrentEmployerId)
                    .IsRequired(false)
                    .OnDelete(DeleteBehavior.NoAction);
            });

            builder.OwnsOne(x => x.ArrayCustomerSettings, ba =>
            {
                ba.ToTable("ArrayCustomerSettings", "user");
                ba.WithOwner().HasForeignKey("UserId");

                ba.Property(x => x.UserId).IsRequired();
                ba.Property(x => x.ArrayUserId).IsRequired();
                ba.HasIndex(x => x.UserId)
                    .HasDatabaseName("IX_ArrayCustomerSettings_UserId")
                    .IsUnique();
                ba.HasIndex(x => x.ArrayUserId)
                    .HasDatabaseName("IX_ArrayCustomerSettings_ArrayUserId")
                    .IsUnique();
            });

            builder.OwnsOne(x => x.CashbackStatistic, ba =>
            {
                ba.ToTable("CashbackStatistics", "user");
                ba.WithOwner().HasForeignKey("UserId");

                ba.Property(x => x.TotalEarnedAmount).HasDefaultValue(0M);

                ba.Property(x => x.EarnedAmountUpdatedAt).HasDefaultValueSql("GETDATE()");
            });

            builder.OwnsOne(x => x.AdvanceStats, ba =>
            {
                ba.ToTable("AdvanceSyncedStats", "user");
                ba.WithOwner().HasForeignKey("UserId");

                ba.Property(x => x.UpdatedAt).IsRequired().HasDefaultValueSql("GETDATE()");
                ba.Property(x => x.OfferId).HasMaxLength(100);

                ba.Property(x => x.DelayedBoostSettings)
                    .HasConversion(
                    v => JsonConvert.SerializeObject(v,
                        new JsonSerializerSettings { NullValueHandling = NullValueHandling.Ignore }),
                    v => JsonConvert.DeserializeObject<DelayedBoostSettings>(v,
                        new JsonSerializerSettings { NullValueHandling = NullValueHandling.Ignore })
                );

                ba.HasIndex(x => x.ExtensionExpiresAt)
                    .HasDatabaseName("IX_AdvanceSyncedStats_ExtensionExpiresAt")
                    .IsUnique(false);
            });

            builder.OwnsOne(x => x.UserSyncedStats, ba =>
            {
                ba.ToTable("UserSyncedStats", "user");
                ba.WithOwner().HasForeignKey("UserId");

                ba.Property(x => x.UpdatedAt)
                    .IsRequired()
                    .HasDefaultValueSql("GETDATE()");
            });
        }
    }
}
