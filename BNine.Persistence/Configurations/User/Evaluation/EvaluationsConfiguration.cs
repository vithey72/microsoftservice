﻿//namespace BNine.Persistence.Configurations.User.Evaluation
//{
//    using Domain.Entities.User.Evaluation;
//    using Extensions;
//    using Microsoft.EntityFrameworkCore;
//    using Microsoft.EntityFrameworkCore.Metadata.Builders;

//    public class EvaluationsConfiguration : IEntityTypeConfiguration<Evaluation>
//    {
//        public void Configure(EntityTypeBuilder<Evaluation> builder)
//        {
//            builder.MapTable("Evaluations");

//            builder.Property(x => x.EvaluationToken).IsRequired();

//            builder.HasOne(x => x.User)
//                .WithMany(x => x.Evaluations)
//                .HasForeignKey(x => x.UserId)
//                .OnDelete(DeleteBehavior.Cascade);
//        }
//    }
//}
