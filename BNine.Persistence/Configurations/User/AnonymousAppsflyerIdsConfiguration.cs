﻿namespace BNine.Persistence.Configurations.User
{
    using Domain.Entities;
    using Extensions;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.EntityFrameworkCore.Metadata.Builders;

    public class AnonymousAppsflyerIdsConfiguration : IEntityTypeConfiguration<AnonymousAppsflyerId>
    {
        public void Configure(EntityTypeBuilder<AnonymousAppsflyerId> builder)
        {
            builder.MapTable("AnonymousAppsflyerIds");

            builder.Property(x => x.Phone)
                .HasPhoneMaxLength()
                .IsRequired();

            builder.Property(x => x.AppsflyerId)
                .HasMediumMaxLength()
                .IsRequired(false);

            builder.Property(x => x.CreatedAt)
                .IsRequired()
                .ValueGeneratedOnAdd()
                .HasDefaultValueSql("getutcdate()");

            builder.HasIndex(x => x.CreatedAt)
                .HasDatabaseName("IX_AnonymousAppsflyerIds_CreatedAt");
        }
    }
}
