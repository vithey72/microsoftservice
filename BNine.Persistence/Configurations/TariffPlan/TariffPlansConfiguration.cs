﻿namespace BNine.Persistence.Configurations.TariffPlan
{
    using Microsoft.EntityFrameworkCore;
    using Domain.Entities.TariffPlan;
    using Extensions;
    using BNine.Constants;
    using Enums.TariffPlan;
    using Newtonsoft.Json;

    public class TariffPlansConfiguration : IEntityTypeConfiguration<TariffPlan>
    {
        public void Configure(Microsoft.EntityFrameworkCore.Metadata.Builders.EntityTypeBuilder<TariffPlan> builder)
        {
            builder.MapTable("TariffPlans", "tariff");

            builder.Property(x => x.Name)
                .HasMediumMaxLength()
                .IsRequired();

            builder.Property(x => x.ShortName)
                .HasMediumMaxLength()
                .IsRequired();

            builder.Property(x => x.ExternalClassificationId)
                .IsRequired();

            builder.Property(x => x.MonthlyFee)
                .HasColumnType("decimal(18, 6)")
                .IsRequired();

            builder.Property(x => x.CreatedAt)
                .HasDefaultValueSql("GETDATE()")
                .IsRequired();

            builder.Property(x => x.PictureUrl)
                .IsRequired();

            builder.Property(x => x.IconUrl)
                .IsRequired();

            builder.Property(x => x.Type)
                .IsRequired();

            builder.Property(x => x.MonthsDuration)
                .IsRequired();

            builder.Property(x => x.GroupIds).HasConversion(
                x => JsonConvert.SerializeObject(x),
                x => JsonConvert.DeserializeObject<Guid[]>(x));

            builder.Metadata.SetIsTableExcludedFromMigrations(true);

            var creationDate = new DateTime(2022, 4, 20);

            // the free 1 month period after sign-up
            var defaultTariff = new TariffPlan
            {
                Id = PaidUserServices.DefaultPlan,
                Name = "B9 Basic Plan",
                ShortName = "B9 Basic Plan",
                ExternalClassificationId = 693, // prod has a different value, consult https://bninecom.atlassian.net/browse/B9-4996
                IsDisabled = true,
                Type = TariffPlanFamily.Advance,
                MonthlyFee = 0,
                TotalPrice = 0,
                MonthsDuration = 1,
                CreatedAt = new DateTime(2023, 5, 15),
                IsParent = false,
                PictureUrl = "https://b9prodaccount.blob.core.windows.net/static-documents-container/TariffPlans/B9_Basic_Plan_1m.png",
                AdvertisementPictureUrl = "https://b9prodaccount.blob.core.windows.net/static-documents-container/TariffPlans/B9_Basic_Plan_1m.png",
                IconUrl = "https://b9devaccount.blob.core.windows.net/static-documents-container/TariffPlans/B9_Advance_icon.png",
                WelcomeScreenPictureUrl = "https://b9devaccount.blob.core.windows.net/static-documents-container/TariffPlans/Welcome_B9_Advance.png",
            };

            var basicTariff = new TariffPlan
            {
                Id = PaidUserServices.Basic1Month,
                Name = "B9 Basic Plan",
                ShortName = "B9 Basic Plan",
                ExternalClassificationId = 5311, // prod has a different value, consult https://bninecom.atlassian.net/browse/B9-4996
                ExternalChargeId = 1,
                MonthlyFee = 9.99m,
                CreatedAt = creationDate,
                Type = Enums.TariffPlan.TariffPlanFamily.Advance,
                Features = TariffPlansFeatures.BasicTariffFeaturesRaw,
                PictureUrl = "https://b9prodaccount.blob.core.windows.net/static-documents-container/TariffPlans/B9_Basic_Plan_1m.png",
                AdvertisementPictureUrl = "https://b9prodaccount.blob.core.windows.net/static-documents-container/TariffPlans/B9_Basic_Plan_1m.png",
                IconUrl = "https://b9devaccount.blob.core.windows.net/static-documents-container/TariffPlans/B9_Advance_icon.png",
                WelcomeScreenPictureUrl = "https://b9devaccount.blob.core.windows.net/static-documents-container/TariffPlans/Welcome_B9_Advance.png",
                MonthsDuration = 1,
                DiscountPercentage = 0,
                IsParent = true,
                TotalPrice = 9.99m,
                PreDiscountPrice = 9.99m,
            };
            var premiumTariff = new TariffPlan
            {
                Id = PaidUserServices.Premium1Month,
                Name = "B9 Premium Plan",
                ShortName = "B9 Premium Plan",
                ExternalClassificationId = 691,
                ExternalChargeId = 2,
                MonthlyFee = 19.99m,
                Type = Enums.TariffPlan.TariffPlanFamily.Premium,
                CreatedAt = creationDate,
                Features = TariffPlansFeatures.PremiumTariffFeaturesRaw,
                PictureUrl = "https://b9devaccount.blob.core.windows.net/static-documents-container/TariffPlans/B9_Premium_1m.png",
                AdvertisementPictureUrl = "https://b9devaccount.blob.core.windows.net/static-documents-container/TariffPlans/B9_Premium_1m.png",
                IconUrl = "https://b9devaccount.blob.core.windows.net/static-documents-container/TariffPlans/B9_Premium_icon.png",
                WelcomeScreenPictureUrl = "https://b9devaccount.blob.core.windows.net/static-documents-container/TariffPlans/Welcome_B9_Premium.png",
                MonthsDuration = 1,
                DiscountPercentage = 0,
                IsParent = true,
                TotalPrice = 19.99m,
                PreDiscountPrice = 19.99m,
            };

            var premium3mTariff = new TariffPlan
            {
                Id = PaidUserServices.Premium3Month10Sale,
                Name = "B9 Premium 3 months",
                ShortName = "B9 Premium Plan",
                ExternalClassificationId = 757,
                ExternalChargeId = 3,
                MonthlyFee = 17.99m,
                Features = premiumTariff.Features,
                Type = Enums.TariffPlan.TariffPlanFamily.Premium,
                CreatedAt = creationDate,
                PictureUrl = "https://b9devaccount.blob.core.windows.net/static-documents-container/TariffPlans/B9_Premium_3m_1799.png",
                AdvertisementPictureUrl = "https://b9devaccount.blob.core.windows.net/static-documents-container/TariffPlans/B9_Premium_3m_1799_ad.png",
                IconUrl = "https://b9devaccount.blob.core.windows.net/static-documents-container/TariffPlans/B9_Premium_icon.png",
                WelcomeScreenPictureUrl = "https://b9devaccount.blob.core.windows.net/static-documents-container/TariffPlans/Welcome_B9_Premium.png",
                MonthsDuration = 3,
                IsParent = false,
                DiscountPercentage = 10,
                TotalPrice = 53.97m,
                PreDiscountPrice = 59.97m,
            };

            builder.HasData(
                defaultTariff,
                basicTariff,
                premiumTariff,
                premium3mTariff);
        }
    }
}
