﻿namespace BNine.Persistence.Configurations.TariffPlan
{
    using BNine.Domain.Entities.TariffPlan;
    using BNine.Persistence.Configurations.Extensions;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.EntityFrameworkCore.Metadata.Builders;

    public class UserTariffPlansConfiguration : IEntityTypeConfiguration<UserTariffPlan>
    {
        public void Configure(EntityTypeBuilder<UserTariffPlan> builder)
        {
            builder.MapTable("UserTariffPlans", "tariff");

            builder.Property(x => x.Status)
                .IsRequired();

            builder.Property(x => x.TariffPlanId)
                .IsRequired();

            builder.Property(x => x.CreatedAt)
                .HasDefaultValueSql("GETDATE()")
                .IsRequired();

            builder.Property(x => x.UpdatedAt)
                .HasDefaultValueSql("GETDATE()")
                .IsRequired();

            builder.Property(x => x.UserId)
                .IsRequired();

            builder.HasOne(x => x.TariffPlan)
                .WithMany(x => x.UserTariffPlans)
                .HasForeignKey(x => x.TariffPlanId)
                .OnDelete(DeleteBehavior.Cascade);

            builder.HasOne(x => x.User)
                .WithMany(x => x.UserTariffPlans)
                .HasForeignKey(x => x.UserId)
                .OnDelete(DeleteBehavior.Cascade);

            builder.Property(x => x.SysStartTime).ValueGeneratedOnAddOrUpdate();
        }
    }
}
