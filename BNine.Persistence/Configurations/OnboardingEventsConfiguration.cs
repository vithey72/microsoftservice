﻿namespace BNine.Persistence.Configurations
{
    using BNine.Domain.Entities;
    using BNine.Persistence.Configurations.Extensions;
    using Enums;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.EntityFrameworkCore.Metadata.Builders;

    public class OnboardingEventsConfiguration : IEntityTypeConfiguration<OnboardingEvent>
    {
        public void Configure(EntityTypeBuilder<OnboardingEvent> builder)
        {
            builder.MapTable("OnboardingEvents", "logs");

            builder.Property(x => x.Message)
                .IsRequired(false);

            builder.HasOne(x => x.User)
                .WithMany(x => x.OnboardingErrors)
                .HasForeignKey(x => x.UserId)
                .OnDelete(DeleteBehavior.Cascade);

            builder.HasIndex(x => x.CreatedAt)
                .HasDatabaseName("IX_OnboardingEvents_CreatedAt");

            builder.Property(x => x.SourcePlatform)
                .HasDefaultValue(EventSourcePlatform.Backend)
                .IsRequired();
        }
    }
}
