﻿namespace BNine.Persistence.Configurations
{
    using Domain.Entities;
    using Extensions;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.EntityFrameworkCore.Metadata.Builders;

    public class OTPsConfiguration : IEntityTypeConfiguration<OTP>
    {
        public void Configure(EntityTypeBuilder<OTP> builder)
        {
            builder.ToTable("OTPs");

            builder.HasKey(x => x.Id)
                .IsClustered(false);

            builder.Property(x => x.Id)
                .ValueGeneratedOnAdd()
                .HasDefaultValueSql("newsequentialid()");

            builder.Property(x => x.Value).IsRequired();
            builder.Property(x => x.PhoneNumber).IsRequired().HasPhoneMaxLength();
            builder.Property(x => x.IpAddress).IsRequired().HasIpAddressMaxLength();

            builder.HasIndex(x => x.Date)
                .HasDatabaseName("IX_OTPs_Date");

            builder.HasIndex(x => x.PhoneNumber)
                .HasDatabaseName("IX_OTPs_PhoneNumber")
                .IsClustered(true)
                .IsUnique(false);


        }
    }
}
