﻿namespace BNine.Persistence.Configurations.Settings
{
    using BNine.Domain.Entities.Settings;
    using BNine.Persistence.Configurations.Extensions;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.EntityFrameworkCore.Metadata.Builders;

    public class SavingAccountSettingsConfigurations : IEntityTypeConfiguration<SavingAccountSettings>
    {
        public void Configure(EntityTypeBuilder<SavingAccountSettings> builder)
        {
            builder.MapTable("SavingAccountSettings", "settings");

            builder.Property(r => r.PullFromExternalCardMinAmount)
              .HasColumnType("decimal(18,2)")
              .HasDefaultValue(20);

            builder.Property(r => r.PullFromExternalCardMaxAmount)
              .HasColumnType("decimal(18,2)")
              .HasDefaultValue(500);

            builder.Property(r => r.PushToExternalCardMinAmount)
              .HasColumnType("decimal(18,2)")
              .HasDefaultValue(20);

            builder.Property(r => r.PushToExternalCardMaxAmount)
              .HasColumnType("decimal(18,2)")
              .HasDefaultValue(500);
        }
    }
}
