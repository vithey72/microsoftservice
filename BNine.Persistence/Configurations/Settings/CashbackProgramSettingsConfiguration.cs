﻿namespace BNine.Persistence.Configurations.Settings
{
    using BNine.Domain.Entities.Settings;
    using BNine.Persistence.Configurations.Extensions;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.EntityFrameworkCore.Metadata.Builders;

    public class CashbackProgramSettingsConfiguration : IEntityTypeConfiguration<CashbackProgramSettings>
    {
        public void Configure(EntityTypeBuilder<CashbackProgramSettings> builder)
        {
            builder.MapTable("CashbackProgramSettings", "settings");

            builder.Property(cs => cs.MinimumSpentAmount)
               .HasColumnType("decimal(18,2)")
               .IsRequired();

            builder.Property(cs => cs.SlideshowScreens)
                .IsRequired();

            builder.Property(cs => cs.MinCashbackCategories)
                .IsRequired();

            builder.Property(cs => cs.MaxCashbackCategories)
                .IsRequired();

            builder.Property(cs => cs.UpdatedAt)
                .HasDefaultValueSql("GETDATE()");

            builder.HasData(
                new List<CashbackProgramSettings>
                {
                    new ()
                    {
                        Id = new Guid("8D8F7FEA-67D5-46AA-857F-EC024D97E25A"),
                        MinCashbackCategories = 1,
                        MaxCashbackCategories = 4,
                        MinimumSpentAmount = 200M,
                        SlideshowScreens = "{\"screens\":[{\"header\":\"Get cashback every month\",\"text\":\"Select the categories you want to use the most every month. Cashback will be deposited by the 5th of the following month.\",\"buttonText\":\"NEXT\",\"imageUrl\":\"https://b9devaccount.blob.core.windows.net/static-documents-container/CashbackSlideshow/page1.png\"},{\"header\":\"Spend at least $200\",\"text\":\"You will begin to earn cashback as soon as you reach $200 worth of the categories you selected.\",\"buttonText\":\"NEXT\",\"imageUrl\":\"https://b9devaccount.blob.core.windows.net/static-documents-container/CashbackSlideshow/page2.png\"},{\"header\":\"Select categories of cashback\",\"text\":\"Right under selected categories you'll see the categories you chose for that particular month. You will need to select categories every month.\",\"buttonText\":\"NEXT\",\"imageUrl\":\"https://b9devaccount.blob.core.windows.net/static-documents-container/CashbackSlideshow/page3.png\",\"link\":\"https://b9devaccount.blob.core.windows.net/static-documents-container/Terms of Service.pdf\",\"linkText\":\"Terms and conditions\"}]}",
                    }
                });
        }
    }
}
