﻿namespace BNine.Persistence.Configurations.Settings
{
    using BNine.Domain.Entities.Settings;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.EntityFrameworkCore.Metadata.Builders;

    public class MerchantCategoryCodeIconSettingsConfiguration : IEntityTypeConfiguration<MerchantCategoryCodeIconSettings>
    {
        public void Configure(EntityTypeBuilder<MerchantCategoryCodeIconSettings> builder)
        {
            builder.ToTable("MerchantCategoryCodeIconSettings", "settings");
            builder.HasKey(x => x.Code);

            builder.HasData(
                new MerchantCategoryCodeIconSettings { Code = 4829, Description = "Money transfers" },
                new MerchantCategoryCodeIconSettings { Code = 5814, Description = "Fast food" },
                new MerchantCategoryCodeIconSettings { Code = 5541, Description = "Fueling stations" },
                new MerchantCategoryCodeIconSettings { Code = 5411, Description = "Supermarkets" },
                new MerchantCategoryCodeIconSettings { Code = 6011, Description = "ATM" },
                new MerchantCategoryCodeIconSettings { Code = 6012, Description = "ATM" },
                new MerchantCategoryCodeIconSettings { Code = 5399, Description = "Small retail outlets and medium-sized stores" },
                new MerchantCategoryCodeIconSettings { Code = 5812, Description = "Restaurants" },
                new MerchantCategoryCodeIconSettings { Code = 4121, Description = "Taxi" },
                new MerchantCategoryCodeIconSettings { Code = 6051, Description = "Funding of electronic wallets" },
                new MerchantCategoryCodeIconSettings { Code = 5999, Description = "Various stores and specialty retailers" },
                new MerchantCategoryCodeIconSettings { Code = 5816, Description = "Digital Goods – Games" },
                new MerchantCategoryCodeIconSettings { Code = 5310, Description = "Sales of a variety of goods at discounted prices" },
                new MerchantCategoryCodeIconSettings { Code = 7994, Description = "Video game clubs" },
                new MerchantCategoryCodeIconSettings { Code = 5499, Description = "Various grocery stores" },
                new MerchantCategoryCodeIconSettings { Code = 4814, Description = "Telecommunications" },
                new MerchantCategoryCodeIconSettings { Code = 5818, Description = "Digital Goods" }
            );
        }
    }
}
