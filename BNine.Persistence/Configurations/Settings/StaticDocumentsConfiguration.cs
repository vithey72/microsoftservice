﻿namespace BNine.Persistence.Configurations.Settings
{
    using BNine.Domain.Entities.Settings;
    using BNine.Persistence.Configurations.Extensions;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.EntityFrameworkCore.Metadata.Builders;

    public class StaticDocumentsConfiguration : IEntityTypeConfiguration<StaticDocument>
    {
        public void Configure(EntityTypeBuilder<StaticDocument> builder)
        {
            builder.MapTable("StaticDocuments", "settings");

            builder.Property(x => x.IsAgreementRequired)
                .IsRequired()
                .HasDefaultValue(false);

            builder.HasIndex(x => x.UpdatedAt)
               .HasDatabaseName("IX_StaticDocument_UpdatedAt");

            builder.HasIndex(x => x.Key)
                .IsUnique(true)
                .HasDatabaseName("IX_StaticDocument_Key");
        }
    }
}
