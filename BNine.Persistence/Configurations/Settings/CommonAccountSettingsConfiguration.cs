﻿namespace BNine.Persistence.Configurations.Settings;

using BNine.Domain.Entities.Settings;
using BNine.Persistence.Configurations.Extensions;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

public class CommonAccountSettingsConfiguration : IEntityTypeConfiguration<CommonAccountSettings>
{
    public void Configure(EntityTypeBuilder<CommonAccountSettings> builder)
    {
        builder.MapTable("CommonAccountSettings", "settings");

        builder.Property(x => x.VoluntaryClosureDelayInDays)
            .IsRequired()
            .HasDefaultValue(30);

        builder.Property(r => r.PendingClosureCheckGapInHours)
            .IsRequired()
            .HasDefaultValue(24);

        builder.Property(r => r.BlockClosureOffsetFromActivationInDays)
            .IsRequired()
            .HasDefaultValue(10);

        builder.Property(r => r.TruvDistributionShareInPercents)
            .IsRequired(true)
            .HasDefaultValue(0);

        builder.HasData(new CommonAccountSettings
        {
            Id = Guid.Parse("1406ff7e-3f65-4f9f-a30a-27db54cda9dc"),
            BlockClosureOffsetFromActivationInDays = 5,
        });
    }
}
