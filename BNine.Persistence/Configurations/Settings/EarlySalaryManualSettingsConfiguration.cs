﻿namespace BNine.Persistence.Configurations.Settings
{
    using BNine.Domain.Entities.Settings;
    using BNine.Persistence.Configurations.Extensions;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.EntityFrameworkCore.Metadata.Builders;

    public class EarlySalaryManualSettingsConfiguration : IEntityTypeConfiguration<EarlySalaryManualSettings>
    {
        public void Configure(EntityTypeBuilder<EarlySalaryManualSettings> builder)
        {
            builder.MapTable("EarlySalaryManualSettings", "settings");

            builder.Property(r => r.DefaultAmountAllocation)
               .HasColumnType("decimal(18,2)")
               .IsRequired(true);

            builder.Property(r => r.MaxAmountAllocation)
               .HasColumnType("decimal(18,2)")
               .IsRequired(true);

            builder.Property(r => r.MinAmountAllocation)
               .HasColumnType("decimal(18,2)")
               .IsRequired(true);

            builder.Property(r => r.DefaultPercentAllocation)
               .HasColumnType("decimal(18,2)")
               .IsRequired(true);

            builder.Property(r => r.MaxPercentAllocation)
               .HasColumnType("decimal(18,2)")
               .IsRequired(true);

            builder.Property(r => r.MinPercentAllocation)
               .HasColumnType("decimal(18,2)")
               .IsRequired(true);
        }
    }
}
