﻿namespace BNine.Persistence.Configurations.Settings
{
    using System.Collections.Immutable;
    using BNine.Domain.Entities.Settings;
    using BNine.Persistence.Configurations.Extensions;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.EntityFrameworkCore.Metadata.Builders;
    using Newtonsoft.Json;

    public class  LoanSettingsConfiguration : IEntityTypeConfiguration<LoanSettings>
    {
        public const int DefaultBoostExpressFeeChargeTypeId = 68;// prod value, differs on stage (1395)

        public void Configure(EntityTypeBuilder<LoanSettings> builder)
        {
            builder.MapTable("LoanSettings", "settings");

            builder.Property(r => r.Limit)
               .HasColumnType("decimal(18,2)")
               .IsRequired(true);

            builder.Property(x => x.BoostExpressFeeChargeTypeId)
                .IsRequired()
                .HasDefaultValue(DefaultBoostExpressFeeChargeTypeId);

            builder.Property(r => r.RequiredMinimalDeposit)
                .HasColumnType("decimal(18,2)")
                .IsRequired(true)
                .HasDefaultValue(300.00);

            builder.Property(x => x.ACHPrincipalKeywords).HasConversion(
                x => JsonConvert.SerializeObject(x),
                x => ImmutableSortedSet.Create(JsonConvert.DeserializeObject<string[]>(x)));
        }
    }
}
