﻿namespace BNine.Persistence.Configurations.Settings
{
    using BNine.Domain.Entities.Settings;
    using BNine.Enums.Transfers.Controller;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.EntityFrameworkCore.Metadata.Builders;

    public class TransferIconSettingsConfiguration : IEntityTypeConfiguration<TransferIconSettings>
    {
        public void Configure(EntityTypeBuilder<TransferIconSettings> builder)
        {
            builder.ToTable("TransferIconSettings", "settings");
            builder.HasKey(x => x.Id);

            builder.HasData(new TransferIconSettings
            {
                Id = BankTransferIconOption.DefaultCardOk,
                IconUrl = "https://b9prodaccount.blob.core.windows.net/static-documents-container/TransactionIcons/01okAuth.png"
            },

            new TransferIconSettings
            {
                Id = BankTransferIconOption.CardWarning,
                IconUrl = "https://b9prodaccount.blob.core.windows.net/static-documents-container/TransactionIcons/02card.png"
            },

            new TransferIconSettings
            {
                Id = BankTransferIconOption.Atm,
                IconUrl = "https://b9prodaccount.blob.core.windows.net/static-documents-container/TransactionIcons/03atm.png"
            },

            new TransferIconSettings
            {
                Id = BankTransferIconOption.Membership,
                IconUrl = "https://b9prodaccount.blob.core.windows.net/static-documents-container/TransactionIcons/04membership.png"
            },

            new TransferIconSettings
            {
                Id = BankTransferIconOption.Fee,
                IconUrl = "https://b9prodaccount.blob.core.windows.net/static-documents-container/TransactionIcons/05fee.png"
            },

            new TransferIconSettings
            {
                Id = BankTransferIconOption.Advance,
                IconUrl = "https://b9prodaccount.blob.core.windows.net/static-documents-container/TransactionIcons/09moneybag.png"
            },

            new TransferIconSettings
            {
                Id = BankTransferIconOption.Bank,
                IconUrl = "https://b9prodaccount.blob.core.windows.net/static-documents-container/TransactionIcons/07bank.png"
            },

            new TransferIconSettings
            {
                Id = BankTransferIconOption.Person,
                IconUrl = "https://b9prodaccount.blob.core.windows.net/static-documents-container/TransactionIcons/08person.png"
            },

            new TransferIconSettings
            {
                Id = BankTransferIconOption.MoneyBag,
                IconUrl = "https://b9prodaccount.blob.core.windows.net/static-documents-container/TransactionIcons/09moneybag.png"
            },
            new TransferIconSettings
            {
                Id = BankTransferIconOption.Checkmark,
                IconUrl = "https://b9devaccount.blob.core.windows.net/static-documents-container/TransactionIcons/10checkmark.png"
            },
            new TransferIconSettings
            {
                Id = BankTransferIconOption.Cross,
                IconUrl = "https://b9devaccount.blob.core.windows.net/static-documents-container/TransactionIcons/11cross.png"
            }
            );
        }
    }
}
