﻿namespace BNine.Persistence.Configurations.Settings;

using BNine.Domain.Entities.Settings;
using BNine.Persistence.Configurations.Extensions;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

public class WalletBlockCollectionSettingsConfiguration : IEntityTypeConfiguration<WalletBlockCollectionSettings>
{
    public void Configure(EntityTypeBuilder<WalletBlockCollectionSettings> builder)
    {
        builder.MapTable("WalletBlockCollectionSettings", "settings");
    }
}
