﻿namespace BNine.Persistence.Configurations.Settings
{
    using System.Collections.Immutable;
    using BNine.Domain.Entities.Settings;
    using Extensions;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.EntityFrameworkCore.Metadata.Builders;
    using Newtonsoft.Json;

    public class TransferMerchantsSettingsConfiguration : IEntityTypeConfiguration<TransferMerchantsSettings>
    {
        public void Configure(EntityTypeBuilder<TransferMerchantsSettings> builder)
        {
            builder.MapTable("TransferMerchantsSettings", "settings");

            builder.Property(x => x.MerchantDescriptionKeywords).HasConversion(
                x => JsonConvert.SerializeObject(x ?? new object()),
                x => ImmutableSortedSet.Create(JsonConvert.DeserializeObject<string[]>(x ?? string.Empty)));

            builder.Property(tms => tms.MerchantName)
                .IsRequired();

            builder.HasIndex(tms => tms.MerchantName)
                .IsUnique();

            builder.Property(x => x.Priority)
                .HasDefaultValue(0)
                .IsRequired();

            builder.Property(x => x.CreatedAt)
                .HasDefaultValueSql("getdate()");
        }
    }
}
