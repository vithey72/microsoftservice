﻿namespace BNine.Persistence.Configurations.UserQuestionnaire
{
    using Domain.Entities.UserQuestionnaire;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.EntityFrameworkCore.Metadata.Builders;

    public class QuestionQuestionSetConfiguration
        : IEntityTypeConfiguration<QuestionQuestionSet>
    {
        public void Configure(EntityTypeBuilder<QuestionQuestionSet> builder)
        {
            builder.Metadata.SetSchema("questionnaire");
            builder.HasKey(qsr => new { qsr.QuestionId, qsr.QuestionSetId });
        }
    }
}
