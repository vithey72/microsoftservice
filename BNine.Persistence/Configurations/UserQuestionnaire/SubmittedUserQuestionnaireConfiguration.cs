﻿namespace BNine.Persistence.Configurations.UserQuestionnaire
{
    using Domain.Entities.UserQuestionnaire;
    using Extensions;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.EntityFrameworkCore.Metadata.Builders;

    public class SubmittedUserQuestionnaireConfiguration
        : IEntityTypeConfiguration<SubmittedUserQuestionnaire>
    {
        public void Configure(EntityTypeBuilder<SubmittedUserQuestionnaire> builder)
        {
            builder.MapBase("SubmittedUserQuestionnaire", "questionnaire");
        }
    }
}
