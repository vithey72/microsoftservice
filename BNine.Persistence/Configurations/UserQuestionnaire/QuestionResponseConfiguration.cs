﻿namespace BNine.Persistence.Configurations.UserQuestionnaire
{
    using Domain.Entities.UserQuestionnaire;
    using Extensions;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.EntityFrameworkCore.Metadata.Builders;

    public class QuestionResponseConfiguration
        : IEntityTypeConfiguration<QuestionResponse>
    {
        public void Configure(EntityTypeBuilder<QuestionResponse> builder)
        {
            builder.MapBase("QuestionResponses", "questionnaire");

            builder.Property(x => x.Text)
                .HasMaxLength(150)
                .IsRequired(true);

            builder.Property(x => x.QuestionId)
                .IsRequired(true);

            builder.HasMany(sr => sr.SubmittedQuestionResponses)
                .WithOne(x => x.QuestionResponse)
                .OnDelete(DeleteBehavior.NoAction);
        }
    }
}
