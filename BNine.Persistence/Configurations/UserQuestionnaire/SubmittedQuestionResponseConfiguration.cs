﻿namespace BNine.Persistence.Configurations.UserQuestionnaire
{
    using Domain.Entities.UserQuestionnaire;
    using Extensions;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.EntityFrameworkCore.Metadata.Builders;

    public class SubmittedQuestionResponseConfiguration
        : IEntityTypeConfiguration<SubmittedQuestionResponse>
    {
        public void Configure(EntityTypeBuilder<SubmittedQuestionResponse> builder)
        {
            builder.MapBase("SubmittedQuestionResponses", "questionnaire");

            builder.Property(sr => sr.OpenQuestionResponse)
                .IsRequired(false);

            builder.Property(sr => sr.CompletedUserQuestionnaireId)
                .IsRequired();

            builder.Property(sr => sr.QuestionId)
                .IsRequired();

            builder.Property(x => x.CreatedAt)
                .HasDefaultValueSql("getutcdate()");

            builder.HasOne(sr => sr.CompletedUserQuestionnaire)
                .WithMany(sr => sr.Responses);

            builder.HasMany(sr => sr.Responses)
                .WithOne(x => x.SubmittedQuestionResponse)
                .OnDelete(DeleteBehavior.NoAction);
        }
    }
}
