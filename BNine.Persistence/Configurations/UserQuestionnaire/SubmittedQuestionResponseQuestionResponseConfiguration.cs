﻿namespace BNine.Persistence.Configurations.UserQuestionnaire
{
    using Domain.Entities.UserQuestionnaire;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.EntityFrameworkCore.Metadata.Builders;

    public class SubmittedQuestionResponseQuestionResponseConfiguration
        : IEntityTypeConfiguration<SubmittedQuestionResponseQuestionResponse>
    {
        public void Configure(EntityTypeBuilder<SubmittedQuestionResponseQuestionResponse> builder)
        {
            builder.Metadata.SetSchema("questionnaire");
            builder.HasKey(x => new { x.SubmittedQuestionResponseId, x.QuestionResponseId });
            builder.Property(x => x.CreatedAt)
                .HasDefaultValueSql("getutcdate()");
        }
    }
}
