﻿namespace BNine.Persistence.Configurations.UserQuestionnaire
{
    using Domain.Entities.UserQuestionnaire;
    using Extensions;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.EntityFrameworkCore.Metadata.Builders;

    public class QuestionsConfiguration
        : IEntityTypeConfiguration<Question>
    {
        public void Configure(EntityTypeBuilder<Question> builder)
        {
            builder.MapBase("Questions", "questionnaire");

            builder.Property(x => x.Title)
                .HasMaxLength(150)
                .IsRequired(true);

            builder.Property(x => x.QuestionType)
                .IsRequired();

            builder.Property(x => x.Tag)
                .HasMaxLength(50)
                .IsRequired(false);

            builder
                .HasMany(q => q.ClosedResponses)
                .WithOne(qr => qr.Question);

            builder.HasMany(q => q.QuestionSets)
                .WithOne(x => x.Question);
        }
    }
}
