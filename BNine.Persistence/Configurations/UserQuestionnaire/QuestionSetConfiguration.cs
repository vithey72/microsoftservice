﻿namespace BNine.Persistence.Configurations.UserQuestionnaire
{
    using Domain.Entities.UserQuestionnaire;
    using Extensions;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.EntityFrameworkCore.Metadata.Builders;

    public class QuestionSetConfiguration
        : IEntityTypeConfiguration<QuestionSet>
    {
        public void Configure(EntityTypeBuilder<QuestionSet> builder)
        {
            builder.MapBase("QuestionSets", "questionnaire");

            builder.Property(x => x.Name)
                .HasMaxLength(50)
                .IsRequired();

            builder.Property(x => x.Header)
                .HasMaxLength(100)
                .IsRequired(false);

            builder.Property(x => x.HeaderSubtitle)
                .HasMaxLength(1000)
                .IsRequired(false);

            builder.Property(x => x.MainButtonText)
                .HasMaxLength(50)
                .IsRequired(false);

            builder.Property(x => x.AltButtonText)
                .HasMaxLength(50)
                .IsRequired(false);

            builder.HasIndex(x => x.Name)
                .HasDatabaseName("IX_QuestionSet_Name")
                .IsUnique();

            builder.HasMany(q => q.Questions)
                .WithOne(x => x.QuestionSet);
        }
    }
}
