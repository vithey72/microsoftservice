﻿namespace BNine.Persistence.Configurations.CheckCashing
{
    using Domain.Entities.CheckCashing;
    using Extensions;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.EntityFrameworkCore.Metadata.Builders;

    public class CheckCashingTransactionConfiguration : IEntityTypeConfiguration<CheckCashingTransaction>
    {
        public void Configure(EntityTypeBuilder<CheckCashingTransaction> builder)
        {
            builder.MapTable("CheckCashingTransactions");

            builder.HasOne(x => x.User)
                .WithMany()
                .IsRequired(true)
                .OnDelete(DeleteBehavior.NoAction);

            builder.HasOne(x => x.CheckMaker)
                .WithMany()
                .IsRequired(false)
                .OnDelete(DeleteBehavior.NoAction);

            builder.Property(x => x.CreatedDate).HasDefaultValueSql("getutcdate()");

            builder.HasIndex(x => x.AlltrustTransactionId)
                .IsUnique()
                .HasDatabaseName("IX_CheckCashingTransactions_AlltrustTransactionId");

            builder.HasIndex(x => x.PaymentStatus)
                .HasDatabaseName("IX_CheckCashingTransactions_PaymentStatus");

            builder.HasIndex(x => x.Status)
                .HasDatabaseName("IX_CheckCashingTransactions_Status");

            builder.HasIndex(x => x.CreatedDate)
                .HasDatabaseName("IX_CheckCashingTransactions_CreatedDate");

            builder.HasIndex(x => x.StatusDate)
                .HasDatabaseName("IX_CheckCashingTransactions_StatusDate");
        }
    }
}
