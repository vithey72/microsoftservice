﻿namespace BNine.Persistence.Configurations.CheckCashing
{
    using Domain.Entities.CheckCashing;
    using Extensions;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.EntityFrameworkCore.Metadata.Builders;

    public class CheckMakerConfiguration : IEntityTypeConfiguration<CheckMaker>
    {
        public void Configure(EntityTypeBuilder<CheckMaker> builder)
        {
            builder.MapTable("CheckMakers");

            builder.HasIndex(cm => new { cm.MakerRoutingNumber, cm.MakerAccountNumber })
                .IsUnique()
                .HasDatabaseName("IX_CheckMaker_MakerRoutingNumber_MakerAccountNumber");
        }
    }
}
