﻿namespace BNine.Persistence.Configurations.TransfersSecurityOTP;

using Domain.Entities.TransfersSecurityOTPs;
using Extensions;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

public class TransfersSecurityOTPConfiguration : IEntityTypeConfiguration<TransfersSecurityOTP>
{
    public void Configure(EntityTypeBuilder<TransfersSecurityOTP> builder)
    {
        builder.MapTable("TransfersSecurityOTPs", "bank");

        builder.Property(x => x.Value).IsRequired();
        builder.Property(x => x.CreatedDate).IsRequired();
        builder.Property(x => x.OperationType).IsRequired();

        builder.HasOne(x => x.User)
            .WithMany(x => x.TransfersSecurityOTPs)
            .HasForeignKey(x => x.UserId);
    }
}
