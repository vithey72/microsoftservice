﻿namespace BNine.Persistence.Configurations.Banners;

using Domain.Entities.Banners;
using Extensions;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

public class SeenMarketingBannerConfiguration : IEntityTypeConfiguration<SeenMarketingBanner>
{
    public void Configure(EntityTypeBuilder<SeenMarketingBanner> builder)
    {
        builder.MapTable("MarketingBannersSeen", "dbo");

        builder.HasKey(sb => sb.Id);

        builder.HasOne(sb => sb.Banner)
            .WithMany(b => b.SeenBannerInstances)
            .HasForeignKey(sb => sb.BannerId)
            .OnDelete(DeleteBehavior.Cascade);

        builder.HasOne(x => x.User)
            .WithMany(x => x.MarketingBannersSeen)
            .HasForeignKey(x => x.UserId)
            .OnDelete(DeleteBehavior.Cascade);

        builder.Property(sb => sb.BannerName).IsRequired();
    }
}
