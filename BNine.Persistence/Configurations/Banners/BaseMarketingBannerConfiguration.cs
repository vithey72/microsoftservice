﻿namespace BNine.Persistence.Configurations.Banners;

using Domain.Entities.Banners;
using Enums;
using Extensions;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;
using Newtonsoft.Json;

public class BaseMarketingBannerConfiguration : IEntityTypeConfiguration<BaseMarketingBanner>
{
    public void Configure(EntityTypeBuilder<BaseMarketingBanner> builder)
    {
        builder.HasDiscriminator<string>("EntityType")
            .HasValue<InfoPopupBanner>("infopopup");

        builder.MapTable("MarketingBanners", "settings");

        builder.HasKey(sb => sb.Id);

        builder.HasMany(sb => sb.SeenBannerInstances)
            .WithOne(b => b.Banner)
            .HasForeignKey(sb => sb.BannerId);

        builder.Property(sb => sb.Name).IsRequired();
        builder.Property(sb => sb.ButtonText).IsRequired();
        builder.Property(sb => sb.Title).IsRequired();
        builder.Property(sb => sb.Subtitle).IsRequired(false);
        builder.Property(sb => sb.Priority).IsRequired().HasDefaultValue(0);

        builder.Property(sb => sb.ButtonDeeplinkArguments).HasConversion(
            x => JsonConvert.SerializeObject(x),
            x => JsonConvert.DeserializeObject<object>(x));
        builder.Property(sb => sb.ShowCooldownProgressive).HasConversion(
            x => JsonConvert.SerializeObject(x),
            x => JsonConvert.DeserializeObject<TimeSpan[]>(x));
        builder.Property(sb => sb.Groups).HasConversion(
           x => JsonConvert.SerializeObject(x),
        x => JsonConvert.DeserializeObject<Guid[]>(x));

        builder.Property(d => d.ExternalLinkType)
            .HasConversion(new EnumToStringConverter<LinkFollowMode>())
            .HasDefaultValue(LinkFollowMode.Default);

        builder.Property(sb => sb.CreatedAt)
            .IsRequired()
            .HasDefaultValueSql("GETDATE()");
    }
}
