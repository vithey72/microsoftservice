﻿namespace BNine.Persistence.Configurations.Banners;

using Domain.Entities.Banners;
using Enums;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

public class InfoPopupBannerConfiguration : IEntityTypeConfiguration<InfoPopupBanner>
{
    public void Configure(EntityTypeBuilder<InfoPopupBanner> builder)
    {
        builder.Property(u => u.AppScreen)
            .HasConversion<string>()
            .HasMaxLength(50);

        builder.HasData(new List<InfoPopupBanner>
        {
            new InfoPopupBanner
            {
                Id = new Guid("8b8c0da8-7613-4975-a03a-e3158643633a"),
                Name = "sample",
                Title = "Sample infopopup",
                Subtitle = "lorem",
                ButtonText = "OK",
                IsEnabled = false,
                AppScreen = AppScreens.Login,
            }
        });
    }
}
