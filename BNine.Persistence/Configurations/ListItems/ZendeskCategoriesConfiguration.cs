﻿namespace BNine.Persistence.Configurations.ListItems
{
    using BNine.Domain.ListItems;
    using BNine.Persistence.Configurations.Extensions;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.EntityFrameworkCore.Metadata.Builders;

    public class ZendeskCategoriesConfiguration : IEntityTypeConfiguration<ZendeskCategory>
    {
        public void Configure(EntityTypeBuilder<ZendeskCategory> builder)
        {
            builder.MapEnumEntity("ZendeskCategories");
        }
    }
}
