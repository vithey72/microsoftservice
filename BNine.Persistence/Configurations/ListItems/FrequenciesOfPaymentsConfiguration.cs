﻿namespace BNine.Persistence.Configurations.ListItems
{
    using BNine.Domain.ListItems;
    using BNine.Persistence.Configurations.Extensions;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.EntityFrameworkCore.Metadata.Builders;

    public class FrequenciesOfPaymentsConfiguration
        : IEntityTypeConfiguration<FrequencyOfPayments>
    {
        public void Configure(EntityTypeBuilder<FrequencyOfPayments> builder)
        {
            builder.MapEnumEntity("FrequenciesOfPayments");
        }
    }
}
