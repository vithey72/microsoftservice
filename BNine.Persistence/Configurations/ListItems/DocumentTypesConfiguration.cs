﻿namespace BNine.Persistence.Configurations.ListItems
{
    using BNine.Domain.ListItems;
    using BNine.Persistence.Configurations.Extensions;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.EntityFrameworkCore.Metadata.Builders;

    public class DocumentTypesConfiguration
         : IEntityTypeConfiguration<DocumentType>
    {
        public void Configure(EntityTypeBuilder<DocumentType> builder)
        {
            builder.MapEnumEntity("DocumentTypes");
        }
    }
}
