﻿namespace BNine.Persistence.Configurations.ListItems
{
    using BNine.Domain.ListItems;
    using BNine.Persistence.Configurations.Extensions;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.EntityFrameworkCore.Metadata.Builders;

    public class PayMethodsConfiguration
        : IEntityTypeConfiguration<PayMethod>
    {
        public void Configure(EntityTypeBuilder<PayMethod> builder)
        {
            builder.MapEnumEntity("PayMethods");
        }
    }
}
