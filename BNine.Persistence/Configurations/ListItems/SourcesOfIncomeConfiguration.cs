﻿namespace BNine.Persistence.Configurations.ListItems
{
    using BNine.Domain.ListItems;
    using BNine.Persistence.Configurations.Extensions;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.EntityFrameworkCore.Metadata.Builders;

    public class SourcesOfIncomeConfiguration
        : IEntityTypeConfiguration<SourceOfIncome>
    {
        public void Configure(EntityTypeBuilder<SourceOfIncome> builder)
        {
            builder.MapEnumEntity("SourcesOfIncome");
        }
    }
}
