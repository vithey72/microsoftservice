﻿namespace BNine.Persistence.Configurations
{
    using BNine.Domain.Entities;
    using BNine.Persistence.Configurations.Extensions;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.EntityFrameworkCore.Metadata.Builders;

    public class ExternalBankAccountTransfersConfiguration : IEntityTypeConfiguration<ExternalBankAccountTransfer>
    {
        public void Configure(EntityTypeBuilder<ExternalBankAccountTransfer> builder)
        {
            builder.MapTable("ExternalBankAccountTransfers");

            builder.HasOne(x => x.ExternalBankAccount)
                .WithMany(x => x.ExternalBankAccountTransfers)
                .HasForeignKey(x => x.ExternalBankAccountId)
                .OnDelete(DeleteBehavior.Cascade);
        }
    }
}
