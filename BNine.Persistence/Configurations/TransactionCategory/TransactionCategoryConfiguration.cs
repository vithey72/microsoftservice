﻿namespace BNine.Persistence.Configurations;

using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Microsoft.EntityFrameworkCore;
using Domain.Entities;

public class TransactionCategoryConfiguration : IEntityTypeConfiguration<TransactionAnalyticsCategory>
{
    public void Configure(EntityTypeBuilder<TransactionAnalyticsCategory> builder)
    {
        builder.HasKey(x => x.Key)
            .IsClustered();

        builder.HasMany(x => x.ChildCategories)
            .WithOne(x => x.ParentCategory)
            .HasForeignKey(x => x.ParentId);

        builder.SeedTransactionCategoryData();
    }
}
