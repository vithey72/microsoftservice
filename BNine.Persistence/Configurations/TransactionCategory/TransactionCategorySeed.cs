﻿namespace BNine.Persistence.Configurations;

using Domain.Entities;
using Enums.Transfers;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

public static class TransactionCategorySeed
{
    public static void SeedTransactionCategoryData(this EntityTypeBuilder<TransactionAnalyticsCategory> builder)
    {
        //root level categories
        var defaultCategory = new TransactionAnalyticsCategory() { Key = "default" };

        var cardTransactionsCategory = new TransactionAnalyticsCategory()
        {
            Key = "card-transactions",
            Title = "Card Transactions",
            Color = "FF0000",
            IconUrl =
                "https://b9devaccount.blob.core.windows.net/static-documents-container/TransactionAnalyticsIcons/CardTransactionIcon.png",
            ParentId = defaultCategory.Key,
            HintTitle = "Card Transactions",
            HintSubTitle = "If you pay for something with a B9 debit card, all those transactions will be displayed here and categorized for your convenience.",
            HintButtonText = "GOT IT",
            HintIconUrl = "https://b9prodaccount.blob.core.windows.net/static-documents-container/TransactionAnalyticsIcons/cardTransactionsHintIcon.png"
        };

        var transfersCategory = new TransactionAnalyticsCategory()
        {
            Key = "transfers",
            Title = "Transfers",
            Color = "8BC446",
            IconUrl =
                "https://b9devaccount.blob.core.windows.net/static-documents-container/TransactionAnalyticsIcons/TransferIcon.png",
            ParentId = defaultCategory.Key,
            HintTitle = "Transfers",
            HintSubTitle = "Here displays all outgoing transfers from your B9 Account (ACH transfers, transfers to external cards, internal transfers to other B9 members).",
            HintButtonText = "GOT IT",
            HintIconUrl = "https://b9prodaccount.blob.core.windows.net/static-documents-container/TransactionAnalyticsIcons/transafersHintIcon.png"
        };

        var atmCategory = new TransactionAnalyticsCategory()
        {
            Key = "atm",
            Title = "ATM",
            Color = "53ACFF",
            IconUrl =
                "https://b9devaccount.blob.core.windows.net/static-documents-container/TransactionAnalyticsIcons/ATMIcon.png",
            MccCodes = new List<int>() { 6011 },
            ParentId = defaultCategory.Key,
            HintTitle = "ATM",
            HintSubTitle = "All ATM withdrawals will be displayed in this category.",
            HintButtonText = "GOT IT",
            HintIconUrl = "https://b9prodaccount.blob.core.windows.net/static-documents-container/TransactionAnalyticsIcons/atmHintIcon.png"
        };

        builder.HasData(defaultCategory);
        builder.HasData(cardTransactionsCategory);
        builder.HasData(transfersCategory);
        builder.HasData(atmCategory);

        // node level categories
        builder.HasData(
            new TransactionAnalyticsCategory()
            {
                Key = "money-transfers",
                Title = "Mobile Wallet Usage",
                MccCodes = new List<int>() { 4829 },
                ParentId = cardTransactionsCategory.Key
            },
            new TransactionAnalyticsCategory()
            {
                Key = "fastfood",
                Title = "Fast Food",
                MccCodes = new List<int>() { 5814 },
                ParentId = cardTransactionsCategory.Key
            },
            new TransactionAnalyticsCategory()
            {
                Key = "gas-stations",
                Title = "Gas Stations",
                MccCodes = new List<int>() { 5172, 5541, 5542, 5983 },
                ParentId = cardTransactionsCategory.Key
            },
            new TransactionAnalyticsCategory()
            {
                Key = "supermarkets-online-stores",
                Title = "Supermarkets & Groceries",
                MccCodes = new List<int>() { 5297, 5298, 5300, 5411, 5412, 5422, 5441, 5451, 5462, 5499, 5715, 5921 },
                ParentId = cardTransactionsCategory.Key,
                Color = "12B76A",
                IconUrl =
                    "https://b9devaccount.blob.core.windows.net/static-documents-container/TransactionAnalyticsIcons/CardTransactionIcon.png",
            },
            new TransactionAnalyticsCategory()
            {
                Key = "financial-institutions",
                Title = "Financial Institutions",
                MccCodes = new List<int>() { 6012 },
                ParentId = cardTransactionsCategory.Key
            },
            new TransactionAnalyticsCategory()
            {
                Key = "general-merch",
                Title = "Misc. General Merch",
                MccCodes = new List<int>() { 5399, 5732, 5311, 5331, 5941 },
                ParentId = cardTransactionsCategory.Key
            },
            new TransactionAnalyticsCategory()
            {
                Key = "restaurants",
                Title = "Restaurants",
                MccCodes = new List<int>() { 5811, 5812, 5813 },
                ParentId = cardTransactionsCategory.Key
            },
            new TransactionAnalyticsCategory()
            {
                Key = "taxi",
                Title = "Taxis",
                MccCodes = new List<int>() { 4121 },
                ParentId = cardTransactionsCategory.Key
            },
            new TransactionAnalyticsCategory()
            {
                Key = "wallets-crypto",
                Title = "Crypto Wallets",
                MccCodes = new List<int>() { 6051 },
                ParentId = cardTransactionsCategory.Key
            },
            new TransactionAnalyticsCategory()
            {
                Key = "specialty-retail",
                Title = "Specialty Retail",
                MccCodes = new List<int>() { 5999 },
                ParentId = cardTransactionsCategory.Key
            },
            new TransactionAnalyticsCategory()
            {
                Key = "digital-goods",
                Title = "Digital Goods",
                MccCodes = new List<int>() { 5816, 5818 },
                ParentId = cardTransactionsCategory.Key
            },
            new TransactionAnalyticsCategory()
            {
                Key = "discount-stores",
                Title = "Discount Stores",
                MccCodes = new List<int>() { 5310 },
                ParentId = cardTransactionsCategory.Key
            },
            new TransactionAnalyticsCategory()
            {
                Key = "entertainment",
                Title = "Entertainment",
                MccCodes = new List<int>()
                {7911, 7922, 7929, 7932, 7933, 7941, 7991, 7992, 7993, 7994, 7996, 7997, 7998, 7999, 8664},
                ParentId = cardTransactionsCategory.Key
            },
            new TransactionAnalyticsCategory()
            {
                Key = "telecommunications",
                Title = "Telecommunications",
                MccCodes = new List<int>() { 4814, 4812, 4899 },
                ParentId = cardTransactionsCategory.Key
            },
            new TransactionAnalyticsCategory()
            {
                Key = "book-stores",
                Title = "Book Stores",
                MccCodes = new List<int>()
                {2741, 5111, 5192, 5942, 5994},
                ParentId = cardTransactionsCategory.Key
            },
            new TransactionAnalyticsCategory()
            {
                Key = "beauty",
                Title = "Beauty",
                MccCodes = new List<int>() { 5977, 7230, 7297, 7298 },
                ParentId = cardTransactionsCategory.Key
            },
            new TransactionAnalyticsCategory()
            {
                Key = "movies",
                Title = "Movies",
                MccCodes = new List<int>() { 7829, 7832, 7841 },
                ParentId = cardTransactionsCategory.Key
            },
            new TransactionAnalyticsCategory()
            {
                Key = "pharmacies",
                Title = "Pharmacy",
                MccCodes = new List<int>() { 5122, 5292, 5295, 5912 },
                ParentId = cardTransactionsCategory.Key
            },
            new TransactionAnalyticsCategory()
            {
                Key = "auto-services",
                Title = "Auto Services",
                MccCodes = new List<int>() { 5511, 5521, 5531, 5532, 5533, 7542 },
                ParentId = cardTransactionsCategory.Key
            },
            new TransactionAnalyticsCategory()
            {
                Key = "computer-services",
                Title = "Computer Software and Services",
                MccCodes = new List<int>() { 5734, 4816, 7372, 5045 },
                ParentId = cardTransactionsCategory.Key
            },
            new TransactionAnalyticsCategory()
            {
                Key = "insurance",
                Title = "Insurance",
                MccCodes = new List<int>() { 6300 },
                ParentId = cardTransactionsCategory.Key
            },
            new TransactionAnalyticsCategory()
            {
                Key = "clothing-stores",
                Title = "Clothing Stores",
                MccCodes = new List<int>() { 5691, 5651, 5621, 5699, 5661 },
                ParentId = cardTransactionsCategory.Key
            },
            new TransactionAnalyticsCategory()
            {
                Key = "utilities",
                Title = "Utilities",
                MccCodes = new List<int>() { 4900 },
                ParentId = cardTransactionsCategory.Key
            },
            new TransactionAnalyticsCategory()
            {
                Key = "subscriptions",
                Title = "Subscriptions",
                MccCodes = new List<int>() { 5968 },
                ParentId = cardTransactionsCategory.Key
            },
            new TransactionAnalyticsCategory()
            {
                Key = "travel-agencies",
                Title = "Travel",
                MccCodes = new List<int>() { 4722 },
                ParentId = cardTransactionsCategory.Key
            },
            new TransactionAnalyticsCategory()
            {
                Key = "lodging",
                Title = "Lodging",
                MccCodes = new List<int>() { 7011 },
                ParentId = cardTransactionsCategory.Key
            },
            new TransactionAnalyticsCategory()
            {
                Key = "home-stores",
                Title = "Misc. Home Stores",
                MccCodes = new List<int>() { 5719, 5200 },
                ParentId = cardTransactionsCategory.Key
            },
            new TransactionAnalyticsCategory()
            {
                Key = "pet-stores",
                Title = "Pet Stores",
                MccCodes = new List<int>() { 5995 },
                ParentId = cardTransactionsCategory.Key
            },
            new TransactionAnalyticsCategory()
            {
                Key = "airline-tickets",
                Title = "Airline Tickets",
                MccCodes = new List<int>() { 4304, 4415, 4418, 4511, 4582 },
                MccRangeStart = 3000,
                MccRangeEnd = 3350,
                ParentId = cardTransactionsCategory.Key
            },
            new TransactionAnalyticsCategory()
            {
                Key = "others",
                Title = "Others",
                MccCodes = new List<int>() { },
                ParentId = cardTransactionsCategory.Key
            },
            new TransactionAnalyticsCategory()
            {
                Key = "ach-sent",
                Title = "ACH Sent",
                TransferDisplayTypes = new List<TransferDisplayType>() { TransferDisplayType.AchSent },
                ParentId = transfersCategory.Key
            },
            new TransactionAnalyticsCategory()
            {
                Key = "sent-to-external-cards",
                Title = "Sent to External Cards",
                TransferDisplayTypes = new List<TransferDisplayType>() { TransferDisplayType.PushToExternalCard },
                ParentId = transfersCategory.Key
            },
            new TransactionAnalyticsCategory()
            {
                Key = "internal-transfer-sent",
                Title = "Internal Transfer Sent",
                TransferDisplayTypes = new List<TransferDisplayType>() { TransferDisplayType.SentToB9Client },
                ParentId = transfersCategory.Key
            });
    }
}
