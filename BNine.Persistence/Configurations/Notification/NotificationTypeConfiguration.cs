﻿namespace BNine.Persistence.Configurations.Notification
{
    using BNine.Domain.Entities.Notification;
    using BNine.Enums;
    using BNine.Persistence.Configurations.Extensions;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.EntityFrameworkCore.Metadata.Builders;

    public class NotificationTypeConfiguration : IEntityTypeConfiguration<NotificationType>
    {
        public void Configure(EntityTypeBuilder<NotificationType> builder)
        {
            builder.ToTable("NotificationTypes");
            builder.HasKey(x => x.Id);

            builder.Property(x => x.Name).IsRequired().HasMediumMaxLength();

            builder.HasMany(x => x.Notifications)
                 .WithOne(x => x.NotificationType)
                 .HasForeignKey(x => x.Type)
                 .OnDelete(DeleteBehavior.Cascade);

            builder.HasData(new NotificationType
            {
                Id = NotificationTypeEnum.Information,
                Name = NotificationTypeEnum.Information.ToString(),
                Icon = "https://b9devaccount.blob.core.windows.net/static-documents-container/NotificationIcons/information.png"
            },

            new NotificationType
            {
                Id = NotificationTypeEnum.Marketing,
                Name = NotificationTypeEnum.Marketing.ToString(),
                Icon = "https://b9devaccount.blob.core.windows.net/static-documents-container/NotificationIcons/marketing.png"
            },

            new NotificationType
            {
                Id = NotificationTypeEnum.Transaction,
                Name = NotificationTypeEnum.Transaction.ToString(),
                Icon = "https://b9devaccount.blob.core.windows.net/static-documents-container/NotificationIcons/transactions.png"
            },

            new NotificationType
            {
                Id = NotificationTypeEnum.Others,
                Name = NotificationTypeEnum.Others.ToString(),
                Icon = "https://b9devaccount.blob.core.windows.net/static-documents-container/NotificationIcons/others.png"
            });
        }
    }
}
