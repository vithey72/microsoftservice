﻿namespace BNine.Persistence.Configurations.Notification
{
    using BNine.Domain.Entities.Notification;
    using BNine.Enums;
    using Extensions;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.EntityFrameworkCore.Metadata.Builders;

    public class NotificationsConfiguration : IEntityTypeConfiguration<Notification>
    {
        public void Configure(EntityTypeBuilder<Notification> builder)
        {
            builder.MapTable("Notifications");

            builder.Property(x => x.Text).IsRequired();
            builder.Property(x => x.Title).HasMediumMaxLength();

            builder.Property(x => x.Type)
                .IsRequired()
                .HasDefaultValue(NotificationTypeEnum.Others);

            builder.Property(x => x.IsImportant)
                .HasDefaultValue(true);

            builder.HasIndex(g => g.HookId);

            builder.HasOne(x => x.User)
                .WithMany(x => x.Notifications)
                .HasForeignKey(x => x.Id)
                .OnDelete(DeleteBehavior.Cascade);

            builder.HasIndex(x => x.CreatedAt)
                .HasDatabaseName("IX_Notifications_CreatedAt");
        }
    }
}
