﻿namespace BNine.Persistence.Configurations
{
    using BNine.Domain.Entities;
    using BNine.Persistence.Configurations.Extensions;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.EntityFrameworkCore.Metadata.Builders;

    public class LoanSpecialOffersConfiguration : IEntityTypeConfiguration<LoanSpecialOffer>
    {
        public void Configure(EntityTypeBuilder<LoanSpecialOffer> builder)
        {
            builder.MapTable("LoanSpecialOffers");

            builder.Property(x => x.Value).HasColumnType("decimal(18, 6)");

            builder.HasOne(x => x.User)
                .WithMany(x => x.LoanSpecialOffers)
                .HasForeignKey(x => x.UserId)
                .OnDelete(DeleteBehavior.Cascade);

            builder.HasIndex(x => x.UpdatedAt)
                .HasDatabaseName("IX_LoanSpecialOffers_UpdatedAt");
        }
    }
}
