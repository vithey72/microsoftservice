﻿namespace BNine.Persistence.Configurations.LoanAccount
{
    using Domain.Entities;
    using Extensions;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.EntityFrameworkCore.Metadata.Builders;

    public class LoanAccountsConfiguration : IEntityTypeConfiguration<LoanAccount>
    {
        public void Configure(EntityTypeBuilder<LoanAccount> builder)
        {
            builder.MapTable("LoanAccounts", "bank");

            builder.Property(x => x.ExternalId).IsRequired();
            builder.Property(x => x.ProductId).IsRequired();
            builder.Property(x => x.Status).IsRequired();

            builder.HasOne(x => x.User)
                .WithMany(x => x.LoanAccounts)
                .HasForeignKey(x => x.UserId)
                .OnDelete(DeleteBehavior.Cascade);
        }
    }
}
