﻿namespace BNine.Persistence.Configurations.Transfers;

using Domain.Entities.Transfers;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

public class TransferErrorLogConfiguration : IEntityTypeConfiguration<TransferErrorLog>
{
    public void Configure(EntityTypeBuilder<TransferErrorLog> builder)
    {
        builder.HasOne(x => x.User)
            .WithMany(x => x.TransferErrorLogs)
            .HasForeignKey(x => x.UserId);

        builder.Property(x => x.Timestamp)
            .IsRequired(true)
            .ValueGeneratedOnAdd()
            .HasDefaultValueSql("getutcdate()");
    }
}
