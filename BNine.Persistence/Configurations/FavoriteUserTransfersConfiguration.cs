﻿namespace BNine.Persistence.Configurations
{
    using BNine.Domain.Entities.Transfers;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.EntityFrameworkCore.Metadata.Builders;

    public class FavoriteUserTransfersConfiguration : IEntityTypeConfiguration<FavoriteUserTransfer>
    {
        public void Configure(EntityTypeBuilder<FavoriteUserTransfer> builder)
        {
            builder.ToTable("FavoriteUserTransfers", "dbo");

            builder.Property(x => x.CreatedAt)
                .HasDefaultValueSql("GETDATE()")
                .IsRequired();

            builder.Property(x => x.UpdatedAt)
                .HasDefaultValueSql("GETDATE()")
                .IsRequired();

            builder.Property(x => x.UserId)
                .IsRequired();

            builder.Property(x => x.Name)
                .IsRequired();

            builder.Property(x => x.TransferId)
                .IsRequired();

            builder.HasIndex(x => x.TransferId)
                .IsUnique()
                .HasDatabaseName("IX_FavoriteUserTransfers_TransferId");

            builder.HasIndex(x => x.CreditorIdentifier)
                .HasDatabaseName("IX_FavoriteUserTransfers_CreditorIdentifier");

            builder.HasOne(x => x.User)
                .WithMany(x => x.FavoriteUserTransfer)
                .HasForeignKey(x => x.UserId)
                .OnDelete(DeleteBehavior.Cascade);
        }
    }
}
