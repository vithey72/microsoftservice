﻿namespace BNine.Persistence.Configurations.Extensions
{
    using BNine.Domain.Infrastructure;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.EntityFrameworkCore.Metadata.Builders;

    internal static class BaseListItemConfigurationExtension
    {
        public static EntityTypeBuilder<TEntity> MapBase<TEntity>(this EntityTypeBuilder<TEntity> builder, string tableName, string schemaName = "dbo")
            where TEntity : BaseListItem
        {
            builder.ToTable(tableName, schemaName);

            builder.HasKey(x => x.Key);

            return builder;
        }
    }
}
