﻿namespace BNine.Persistence.Configurations.Extensions
{
    using Microsoft.EntityFrameworkCore.Metadata.Builders;

    internal static class PropertyBuilderExtension
    {
        private const int StateCodeLength = 2;
        private const int ZipCodeLength = 12;
        private const int PhoneLength = 16;
        private const int EnumKeyLength = 150;
        private const int MediumLength = 512;
        private const int IpAddressLength = 39;

        public static PropertyBuilder<string> HasMediumMaxLength(this PropertyBuilder<string> propertyBuilder)
        {
            return propertyBuilder.HasMaxLength(MediumLength);
        }

        public static PropertyBuilder<string> HasEnumKeyMaxLength(this PropertyBuilder<string> propertyBuilder)
        {
            return propertyBuilder.HasMaxLength(EnumKeyLength);
        }

        public static PropertyBuilder<string> HasPhoneMaxLength(this PropertyBuilder<string> propertyBuilder)
        {
            return propertyBuilder.HasMaxLength(PhoneLength);
        }

        public static PropertyBuilder<string> HasZipCodeMaxLength(this PropertyBuilder<string> propertyBuilder)
        {
            return propertyBuilder.HasMaxLength(ZipCodeLength);
        }

        public static PropertyBuilder<string> HasStateCodeMaxLength(this PropertyBuilder<string> propertyBuilder)
        {
            return propertyBuilder.HasMaxLength(StateCodeLength);
        }

        public static PropertyBuilder<string> HasIpAddressMaxLength(this PropertyBuilder<string> propertyBuilder)
        {
            // 15 is the max length of an IPv4 address
            // 39 is the max length of an IPv6 address
            // since we can come across IPv6 address, we set the max length to 39
            return propertyBuilder.HasMaxLength(IpAddressLength);
        }
    }
}
