﻿namespace BNine.Persistence.Configurations.Extensions
{
    using Microsoft.EntityFrameworkCore.Migrations;

    internal static class AuditableEntityConfigurationExtension
    {
        public static void AddTemporalTableSupport(this MigrationBuilder builder, string tableName, string schemaName, string historyTableSchema)
        {
            builder.Sql($@"ALTER TABLE [{schemaName}].[{tableName}] ADD 
                SysStartTime datetime2(0) GENERATED ALWAYS AS ROW START HIDDEN NOT NULL,
                SysEndTime datetime2(0) GENERATED ALWAYS AS ROW END HIDDEN NOT NULL,
                PERIOD FOR SYSTEM_TIME (SysStartTime, SysEndTime);");

            builder.Sql($@"ALTER TABLE [{schemaName}].[{tableName}] SET (SYSTEM_VERSIONING = ON (HISTORY_TABLE = [{historyTableSchema}].[{tableName}] ));");
        }

        public static void AddVersioningToExistedTable(this MigrationBuilder builder, string tableName, string schemaName, string historyTableSchema)
        {
            builder.Sql($@"ALTER TABLE [{schemaName}].[{tableName}]
                          ADD SysStartTime DATETIME2(6) GENERATED ALWAYS AS ROW START
                          HIDDEN NOT NULL DEFAULT DATEADD(HOUR, -1, GETDATE()),
                          SysEndTime DATETIME2(6) GENERATED ALWAYS AS ROW END HIDDEN NOT NULL
                          DEFAULT CONVERT(DATETIME2,'9999-12-31 23:59:59.999999'),
                          PERIOD FOR SYSTEM_TIME (SysStartTime, SysEndTime)");

            builder.Sql($@"ALTER TABLE [{schemaName}].[{tableName}] SET (SYSTEM_VERSIONING = ON (HISTORY_TABLE = [{historyTableSchema}].[{tableName}] ));");
        }
    }
}
