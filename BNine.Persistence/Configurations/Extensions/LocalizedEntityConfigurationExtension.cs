﻿namespace BNine.Persistence.Configurations.Extensions
{
    using BNine.Domain.Interfaces;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.EntityFrameworkCore.Metadata.Builders;

    internal static class LocalizedEntityConfigurationExtension
    {
        public static EntityTypeBuilder<TEntity> MapTable<TEntity>(this EntityTypeBuilder<TEntity> builder, string tableName, string schemaName = null)
            where TEntity : class, IBaseEntity
        {
            builder.ToTable(tableName, schemaName);

            builder.HasKey(x => x.Id);
            builder.Property(x => x.Id)
                .ValueGeneratedOnAdd()
                .HasDefaultValueSql("newsequentialid()");

            return builder;
        }

        public static EntityTypeBuilder<TEntity> MapEnumEntity<TEntity>(this EntityTypeBuilder<TEntity> builder, string tableName, string schemaName = null)
            where TEntity : class, IBaseListItem
        {
            builder.ToTable(tableName, schemaName);
            builder.HasKey(x => x.Key);

            builder.Property(x => x.Order).IsRequired();

            builder.OwnsMany(x => x.Value, t =>
            {
                t.ToTable($"{tableName}Values", "translates");
            });

            builder.Property(x => x.Key).IsRequired().HasEnumKeyMaxLength();

            return builder;
        }
    }
}
