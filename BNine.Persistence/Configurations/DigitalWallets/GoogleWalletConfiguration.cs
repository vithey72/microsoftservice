﻿namespace BNine.Persistence.Configurations.DigitalWallets
{
    using BNine.Domain.Entities.DigitalWallet;
    using BNine.Persistence.Configurations.Extensions;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.EntityFrameworkCore.Metadata.Builders;

    public class GoogleWalletConfiguration : IEntityTypeConfiguration<GoogleWallet>
    {
        public void Configure(EntityTypeBuilder<GoogleWallet> builder)
        {
            builder.MapTable("GoogleWallets", "wallet");

            builder.HasOne(x => x.DebitCard)
                .WithOne(x => x.GoogleWallet)
                .HasForeignKey<GoogleWallet>(x => x.DebitCardId)
                .OnDelete(DeleteBehavior.Cascade);
        }
    }
}
