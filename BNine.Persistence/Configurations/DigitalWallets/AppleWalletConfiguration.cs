﻿namespace BNine.Persistence.Configurations.DigitalWallets
{
    using BNine.Domain.Entities.DigitalWallet;
    using BNine.Persistence.Configurations.Extensions;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.EntityFrameworkCore.Metadata.Builders;

    public class AppleWalletConfiguration : IEntityTypeConfiguration<AppleWallet>
    {
        public void Configure(EntityTypeBuilder<AppleWallet> builder)
        {
            builder.MapTable("AppleWallets", "wallet");

            builder.HasOne(x => x.DebitCard)
                .WithOne(x => x.AppleWallet)
                .HasForeignKey<AppleWallet>(x => x.DebitCardId)
                .OnDelete(DeleteBehavior.Cascade);
        }
    }
}
