﻿namespace BNine.Persistence.Configurations.DigitalWallets
{
    using BNine.Domain.Entities.DigitalWallet;
    using BNine.Persistence.Configurations.Extensions;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.EntityFrameworkCore.Metadata.Builders;

    public class SamsungWalletConfiguration : IEntityTypeConfiguration<SamsungWallet>
    {
        public void Configure(EntityTypeBuilder<SamsungWallet> builder)
        {
            builder.MapTable("SamsungWallets", "wallet");

            builder.HasOne(x => x.DebitCard)
                .WithOne(x => x.SamsungWallet)
                .HasForeignKey<SamsungWallet>(x => x.DebitCardId)
                .OnDelete(DeleteBehavior.Cascade);
        }
    }
}
