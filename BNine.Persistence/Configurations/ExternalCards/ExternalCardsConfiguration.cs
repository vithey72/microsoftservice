﻿namespace BNine.Persistence.Configurations.ExternalCards
{
    using BNine.Domain.Entities.ExternalCards;
    using BNine.Persistence.Configurations.Extensions;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.EntityFrameworkCore.Metadata.Builders;

    public class ExternalCardsConfiguration : IEntityTypeConfiguration<ExternalCard>
    {
        public void Configure(EntityTypeBuilder<ExternalCard> builder)
        {
            builder.MapTable("ExternalCards", "bank");

            builder.HasOne(x => x.User)
                .WithMany(x => x.ExternalCards)
                .HasForeignKey(x => x.UserId)
                .OnDelete(DeleteBehavior.Cascade);

            builder.Property(x => x.Name)
                .HasMediumMaxLength();

            builder.HasIndex(x => x.CreatedAt)
                .HasDatabaseName("IX_ExternalCard_CreatedAt");
        }
    }
}
