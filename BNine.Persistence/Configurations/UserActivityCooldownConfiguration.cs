﻿namespace BNine.Persistence.Configurations;

using Domain.Entities;
using Extensions;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

public class UserActivityCooldownConfiguration : IEntityTypeConfiguration<UserActivityCooldown>
{
    public void Configure(EntityTypeBuilder<UserActivityCooldown> builder)
    {
        builder.MapTable("UserActivityCooldowns");

        builder.HasOne(x => x.User)
            .WithMany(x => x.UserActivityCooldowns)
            .HasForeignKey(x => x.UserId)
            .OnDelete(DeleteBehavior.Cascade);

        builder.Property(x => x.ActivityKey)
            .IsRequired()
            .HasMaxLength(100);

        builder.HasIndex(x => new { x.UserId, x.ActivityKey })
            .IsUnique()
            .HasDatabaseName("IX_UserActivityCooldowns_UserId_ActivityKey");

        builder.HasIndex(x => x.ExpiresAt)
            .HasDatabaseName("IX_UserActivityCooldowns_ExpiresAt");
    }
}
