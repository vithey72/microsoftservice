﻿namespace BNine.Persistence.Configurations.PaidUserServices;

using Domain.Entities.PaidUserServices;
using Extensions;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

public class UsedPaidUserServiceConfiguration : IEntityTypeConfiguration<UsedPaidUserService>
{
    public void Configure(EntityTypeBuilder<UsedPaidUserService> builder)
    {

        builder.MapTable("UsedPaidUserServices", "tariff");

        builder.Property(x => x.ServiceId)
            .IsRequired();

        builder.Property(x => x.UserId)
            .IsRequired();

        builder.HasOne(x => x.Service)
            .WithMany(x => x.UsedServices);

        builder.HasOne(x => x.User)
            .WithMany(x => x.UsedPaidUserServices);

        builder.Property(x => x.CreatedAt)
            .IsRequired()
            .HasDefaultValueSql("GETDATE()");

        builder.Property(x => x.ValidTo)
            .IsRequired();
    }
}
