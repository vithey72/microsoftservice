﻿namespace BNine.Persistence.Configurations.PaidUserServices;

using BNine.Constants;
using Domain.Entities.PaidUserServices;
using Extensions;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

public class PaidUserServiceConfiguration : IEntityTypeConfiguration<PaidUserService>
{

    public void Configure(EntityTypeBuilder<PaidUserService> builder)
    {
        builder.MapTable("PaidUserServices", "tariff");

        builder.Property(x => x.Name)
            .IsRequired();

        builder.Property(x => x.ServiceBusName)
            .IsRequired();

        builder.Property(x => x.ChargeTypeId)
            .IsRequired();

        builder.Property(x => x.Description)
            .IsRequired(false);

        builder.HasData(
            new List<PaidUserService>
            {
                new()
                {
                    Id = PaidUserServices.PremiumSupport,
                    Name = PaidUserServices.Names.PremiumSupportName,
                    ServiceBusName = "premium_support",
                    Description = "One time payment for 2 weeks of premium tech support",
                    CreatedAt = new DateTime(2022, 8, 25),
                    ChargeTypeId = 265, // stage value, differs on prod
                    DurationInDays = 14,
                },
                new()
                {
                    Id = PaidUserServices.AdvanceUnfreeze5,
                    Name = PaidUserServices.Names.AdvanceUnfreeze5,
                    ServiceBusName = "extension",
                    Description = "One time payment for 1 use of Unfreeze service",
                    CreatedAt = new DateTime(2022, 8, 30),
                    ChargeTypeId = 332, // stage value, differs on prod
                    DurationInDays = 0,
                },
                new()
                {
                    Id = PaidUserServices.AdvanceUnfreeze10,
                    Name = PaidUserServices.Names.AdvanceUnfreeze10,
                    ServiceBusName = "extension",
                    Description = "One time payment for 1 use of Unfreeze service",
                    CreatedAt = new DateTime(2022, 8, 30),
                    ChargeTypeId = 333, // stage value, differs on prod
                    DurationInDays = 0,
                },
                new()
                {
                    Id = PaidUserServices.AdvanceUnfreeze15,
                    Name = PaidUserServices.Names.AdvanceUnfreeze15,
                    ServiceBusName = "extension",
                    Description = "Ine time payment for 1 use of Unfreeze service",
                    CreatedAt = new DateTime(2022, 8, 30),
                    ChargeTypeId = 334, // stage value, differs on prod
                    DurationInDays = 0,
                },
                new()
                {
                    Id = PaidUserServices.PhysicalCardEmbossing,
                    Name = PaidUserServices.Names.PhysicalCardEmbossing,
                    ServiceBusName = "card_embossing",
                    Description = "One time payment for initial creation and shipping of a physical card",
                    CreatedAt = new DateTime(2022, 10, 11),
                    ChargeTypeId = 336, // stage value, differs on prod
                    DurationInDays = 640_000,
                },
                new()
                {
                    Id = PaidUserServices.PhysicalCardReplacement,
                    Name = PaidUserServices.Names.PhysicalCardReplacement,
                    ServiceBusName = "card_replacement",
                    Description = "Payment for every replacement of physical card after the first one",
                    CreatedAt = new DateTime(2022, 10, 12),
                    ChargeTypeId = 337, // stage value, differs on prod
                    DurationInDays = 640_000,
                },
                new()
                {
                    Id = PaidUserServices.TariffBasicPurchaseOnOnboarding,
                    Name = PaidUserServices.Names.TariffBasicPurchaseOnOnboarding,
                    ServiceBusName = "advance_tariff_on_onboarding",
                    Description = "Purchase of Basic tariff discounted by 1$",
                    CreatedAt = new DateTime(2022, 10, 25),
                    ChargeTypeId = 338, // stage value, differs on prod
                    DurationInDays = 30,
                },
                new()
                {
                    Id = PaidUserServices.AdvanceBoost999,
                    Name = PaidUserServices.Names.AdvanceBoost999,
                    ServiceBusName = "advance_boost",
                    Description = "Increase your one time Advance amount",
                    CreatedAt = new DateTime(2022, 11, 24),
                    ChargeTypeId = 365, // stage value, differs on prod
                    DurationInDays = 14,
                },
                new()
                {
                    Id = PaidUserServices.AdvanceBoost1599,
                    Name = PaidUserServices.Names.AdvanceBoost1599,
                    ServiceBusName = "advance_boost_2",
                    Description = "Increase your one time Advance amount",
                    CreatedAt = new DateTime(2023, 4, 19),
                    ChargeTypeId = 62, // 761 on stage
                    DurationInDays = 14,
                },
                new()
                {
                    Id = PaidUserServices.AdvanceBoost1999,
                    Name = PaidUserServices.Names.AdvanceBoost1999,
                    ServiceBusName = "advance_boost_3",
                    Description = "Increase your one time Advance amount",
                    CreatedAt = new DateTime(2023, 5, 8),
                    ChargeTypeId = 859, // current value is a stage value, on prod env it's 63
                    DurationInDays = 14
                },
                new()
                {
                    Id = PaidUserServices.AfterAdvanceTip2,
                    Name = PaidUserServices.Names.AfterAdvanceTip2,
                    ServiceBusName = "after_advance_tip",
                    Description = "Tip of $2",
                    CreatedAt = new DateTime(2023, 10, 2),
                    ChargeTypeId = 71, // prod value, on stage it's 2014
                    DurationInDays = 14,
                },
                new()
                {
                    Id = PaidUserServices.AfterAdvanceTip3,
                    Name = PaidUserServices.Names.AfterAdvanceTip3,
                    ServiceBusName = "after_advance_tip",
                    Description = "Tip of $3",
                    CreatedAt = new DateTime(2022, 12, 8),
                    ChargeTypeId = 368, // stage value, differs on prod
                    DurationInDays = 14,
                },
                new()
                {
                    Id = PaidUserServices.AfterAdvanceTip5,
                    Name = PaidUserServices.Names.AfterAdvanceTip5,
                    ServiceBusName = "after_advance_tip",
                    Description = "Tip of $5",
                    CreatedAt = new DateTime(2022, 12, 8),
                    ChargeTypeId = 369, // stage value, differs on prod
                    DurationInDays = 14,
                },
                new()
                {
                    Id = PaidUserServices.AfterAdvanceTip6,
                    Name = PaidUserServices.Names.AfterAdvanceTip6,
                    ServiceBusName = "after_advance_tip",
                    Description = "Tip of $6",
                    CreatedAt = new DateTime(2023, 1, 24),
                    ChargeTypeId = 59,
                    DurationInDays = 14,
                },
                new()
                {
                    Id = PaidUserServices.AfterAdvanceTip8,
                    Name = PaidUserServices.Names.AfterAdvanceTip8,
                    ServiceBusName = "after_advance_tip",
                    Description = "Tip of $8",
                    CreatedAt = new DateTime(2022, 12, 8),
                    ChargeTypeId = 370, // stage value, differs on prod
                    DurationInDays = 14,
                },
                new()
                {
                    Id = PaidUserServices.AfterAdvanceTip10,
                    Name = PaidUserServices.Names.AfterAdvanceTip10,
                    ServiceBusName = "after_advance_tip",
                    Description = "Tip of $10",
                    CreatedAt = new DateTime(2022, 12, 8),
                    ChargeTypeId = 371, // stage value, differs on prod
                    DurationInDays = 14,
                },
                new()
                {
                    Id = PaidUserServices.AfterAdvanceTip12,
                    Name = PaidUserServices.Names.AfterAdvanceTip12,
                    ServiceBusName = "after_advance_tip",
                    Description = "Tip of $12",
                    CreatedAt = new DateTime(2023, 10, 19),
                    ChargeTypeId = 72, // prod value, on stage stage is 2080
                    DurationInDays = 14
                },
                new()
                {
                    Id = PaidUserServices.AfterAdvanceTip20,
                    Name = PaidUserServices.Names.AfterAdvanceTip20,
                    ServiceBusName = "after_advance_tip",
                    Description = "Tip of $20",
                    CreatedAt = new DateTime(2023, 10, 19),
                    ChargeTypeId = 73, // prod value, on stage stage is 2081
                    DurationInDays = 14
                },
                new()
                {
                    Id = PaidUserServices.CheckCreditScorePaidService10USD,
                    Name = PaidUserServices.Names.CheckCreditScorePaidService,
                    ServiceBusName = "check_credit_score_paid_service",
                    Description = "Check credit score paid service",
                    CreatedAt = new DateTime(2023, 08, 21),
                    ChargeTypeId = 69,
                    DurationInDays = 14,
                    ObsoletedAt = new DateTime(2023, 09, 15),
                },
                new()
                {
                    Id = PaidUserServices.CheckCreditScorePaidService5USD,
                    Name = PaidUserServices.Names.CheckCreditScorePaidService,
                    ServiceBusName = "check_credit_score_paid_service",
                    Description = "Check credit score paid service",
                    CreatedAt = new DateTime(2023, 09, 15),
                    ChargeTypeId = 70,
                    DurationInDays = 14,
                },
            });
    }
}
