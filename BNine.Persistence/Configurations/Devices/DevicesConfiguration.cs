﻿namespace BNine.Persistence.Configurations.Devices
{
    using Domain.Entities;
    using Extensions;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.EntityFrameworkCore.Metadata.Builders;

    public class DevicesConfiguration: IEntityTypeConfiguration<Device>
    {
        public void Configure(EntityTypeBuilder<Device> builder)
        {
            builder.MapTable("Devices");

            builder.Property(x => x.ExternalDeviceId).IsRequired();

            builder.HasOne(x => x.User)
                .WithMany(x => x.Devices)
                .HasForeignKey(x => x.UserId)
                .OnDelete(DeleteBehavior.Cascade);

            builder.HasIndex(x => x.UpdatedAt)
                .HasDatabaseName("IX_Devices_UpdatedAt");
        }
    }
}
