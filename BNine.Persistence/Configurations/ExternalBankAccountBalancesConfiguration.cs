﻿namespace BNine.Persistence.Configurations
{
    using BNine.Domain.Entities;
    using BNine.Persistence.Configurations.Extensions;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.EntityFrameworkCore.Metadata.Builders;

    public class ExternalBankAccountBalancesConfiguration : IEntityTypeConfiguration<ExternalBankAccountBalance>
    {
        public void Configure(EntityTypeBuilder<ExternalBankAccountBalance> builder)
        {
            builder.MapTable("ExternalBankAccountBalances");

            builder.HasOne(x => x.ExternalBankAccount)
                .WithMany(x => x.ExternalBankAccountBalances)
                .HasForeignKey(x => x.ExternalBankAccountId)
                .OnDelete(DeleteBehavior.Cascade);

            builder.HasIndex(x => x.CreatedAt)
                .HasDatabaseName("IX_ExternalBankAccountBalances_CreatedAt");
        }
    }
}
