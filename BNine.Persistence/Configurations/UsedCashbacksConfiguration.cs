﻿namespace BNine.Persistence.Configurations;

using Domain.Entities.CashbackProgram;
using Extensions;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Newtonsoft.Json;

public class UsedCashbacksConfiguration : IEntityTypeConfiguration<UsedCashback>
{
    public void Configure(EntityTypeBuilder<UsedCashback> builder)
    {
        builder.MapTable("UsedCashbacks");

        builder.HasIndex(x => new { x.UserId, x.Year, x.Month })
            .HasDatabaseName("IX_UsedCashbacks_User_Year_Month")
            .IsUnique();

        builder.HasOne(uc => uc.User)
            .WithMany(u => u.UsedCashbacks)
            .HasForeignKey(x => x.UserId)
            .OnDelete(DeleteBehavior.Cascade);

        builder.HasIndex(uc => uc.EnabledAt)
            .HasDatabaseName("IX_UsedCashbacks_EnabledAt");

        builder.HasIndex(uc => uc.ExpiredAt)
            .HasDatabaseName("IX_UsedCashbacks_ExpiredAt");

        builder.Property(uc => uc.EnabledAt)
            .HasDefaultValueSql("GETDATE()");

        builder.Property(uc => uc.EarnedAmountUpdatedAt)
            .HasDefaultValueSql("GETDATE()");

        builder.Property(uc => uc.EarnedAmount)
            .HasDefaultValue(0M);
        builder.Property(uc => uc.SpentAmount)
            .HasDefaultValue(0M);

        builder.Property(x => x.CashbackCategoriesIds).HasConversion(
            x => JsonConvert.SerializeObject(x ?? System.Array.Empty<Guid>()),
            x => JsonConvert.DeserializeObject<Guid[]>(x ?? string.Empty));

        builder.Property(x => x.CashbackCategoriesNames).HasConversion(
            x => JsonConvert.SerializeObject(x ?? System.Array.Empty<string>()),
            x => JsonConvert.DeserializeObject<string[]>(x ?? string.Empty));
    }
}
