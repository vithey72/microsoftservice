﻿namespace BNine.Persistence.Configurations
{
    using BNine.Domain.Entities;
    using BNine.Persistence.Configurations.Extensions;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.EntityFrameworkCore.Metadata.Builders;

    public class LoanCalculatedLimitsConfiguration : IEntityTypeConfiguration<LoanCalculatedLimit>
    {
        public void Configure(EntityTypeBuilder<LoanCalculatedLimit> builder)
        {
            builder.MapTable("LoanCalculatedLimits");

            builder.Property(r => r.Amount)
               .HasColumnType("decimal(18,2)")
               .IsRequired(true);

            builder.HasOne(x => x.User)
                .WithMany(x => x.LoanCalculatedLimits)
                .HasForeignKey(x => x.UserId)
                .OnDelete(DeleteBehavior.Cascade);
        }
    }
}
