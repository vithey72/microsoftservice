﻿namespace BNine.Persistence.Configurations.BankAccount;

using Domain.Entities.BankAccount;
using Extensions;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

public class CurrentAdditionalAccountsConfiguration : IEntityTypeConfiguration<CurrentAdditionalAccount>
{
    public void Configure(EntityTypeBuilder<CurrentAdditionalAccount> builder)
    {
        builder.MapBase("CurrentAdditionalAccountsInternal", "bank");

        builder.Property(x => x.CreatedAt)
            .IsRequired(true)
            .ValueGeneratedOnAdd()
            .HasDefaultValueSql("getutcdate()");

        builder.Property(x => x.ExternalId)
            .IsRequired(true);

        builder.Property(x => x.ProductId)
            .IsRequired(true);

        builder.HasOne(x => x.User)
            .WithMany(x => x.CurrentAdditionalAccounts)
            .HasForeignKey(x => x.UserId)
            .IsRequired(true)
            .OnDelete(DeleteBehavior.Restrict);
    }
}
