﻿namespace BNine.Persistence.Configurations.BankAccount
{
    using BNine.Domain.Entities.BankAccount;
    using BNine.Persistence.Configurations.Extensions;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.EntityFrameworkCore.Metadata.Builders;

    public class ACHCreditTransfersConfiguration : IEntityTypeConfiguration<ACHCreditTransfer>
    {
        public void Configure(EntityTypeBuilder<ACHCreditTransfer> builder)
        {
            builder.MapBase("ACHCreditTransfers", "bank");

            builder.Property(x => x.CreatedAt)
                .IsRequired(true);

            builder.Property(x => x.UpdatedAt)
                .IsRequired(true)
                .HasDefaultValueSql("GETDATE()");

            builder.Property(x => x.ExternalId)
                .IsRequired(true);

            builder.Property(x => x.Amount)
                .HasColumnType("decimal(18,8)")
                .IsRequired(true);

            builder.HasOne(x => x.User)
                .WithMany()
                .HasForeignKey(x => x.UserId);

            builder.Ignore(x => x.DomainEvents);
        }
    }
}
