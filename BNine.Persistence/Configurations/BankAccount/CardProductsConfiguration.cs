﻿namespace BNine.Persistence.Configurations.BankAccount;

using Domain.Entities.BankAccount;
using Enums.DebitCard;
using Extensions;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

public class CardProductsConfiguration : IEntityTypeConfiguration<CardProduct>
{
    public void Configure(EntityTypeBuilder<CardProduct> builder)
    {
        builder.MapBase("CardProducts", "bank");

        builder.Property(x => x.ExternalId)
            .IsRequired();

        builder.Property(x => x.AssociatedType)
            .IsRequired();

        builder.Property(x => x.Name)
            .IsRequired();

        builder.HasData(new List<CardProduct>
        {
            new()
            {
                Id = new Guid("18adcdbc-0d44-43ff-a2e0-73e0b408e771"),
                ExternalId = 1,
                Name = "Physical Debit Card (w/ Digital First)",
                AssociatedType = CardType.PhysicalDebit,
            },
            new()
            {
                Id = new Guid("89de0b18-3e69-4f05-bbf8-f5d21e874b2b"),
                ExternalId = 4,
                Name = "Virtual Only Debit Card",
                AssociatedType = CardType.VirtualDebit,
            },
            new()
            {
                Id = new Guid("bff92466-f3ca-4cf7-9ffc-169c99c3f6a7"),
                ExternalId = 5,
                Name = "Secure Credit Card",
                AssociatedType = CardType.Credit,
            }
        });
    }
}
