﻿namespace BNine.Persistence.Configurations.BankAccount
{
    using BNine.Domain.Entities.BankAccount;
    using BNine.Persistence.Configurations.Extensions;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.EntityFrameworkCore.Metadata.Builders;

    public class CurrentAccountsConfiguration
        : IEntityTypeConfiguration<CurrentAccount>
    {
        public void Configure(EntityTypeBuilder<CurrentAccount> builder)
        {
            builder.MapBase("CurrentAccount", "bank");

            builder.Property(x => x.CreatedAt)
                .IsRequired(true)
                .ValueGeneratedOnAdd()
                .HasDefaultValueSql("getutcdate()");

            builder.Property(x => x.ExternalId)
                .IsRequired(true);

            builder.Property(x => x.ProductId)
                .IsRequired(true);

            builder.HasOne(x => x.User)
                .WithOne(x => x.CurrentAccount)
                .HasForeignKey<CurrentAccount>(x => x.UserId)
                .IsRequired(true)
                .OnDelete(DeleteBehavior.Restrict);
        }
    }
}
