﻿namespace BNine.Persistence.Configurations
{
    using BNine.Enums.DebitCard;
    using Domain.Entities;
    using Extensions;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.EntityFrameworkCore.Metadata.Builders;

    public class DebitCardsConfiguration : IEntityTypeConfiguration<DebitCard>
    {
        public void Configure(EntityTypeBuilder<DebitCard> builder)
        {
            builder.MapTable("DebitCards", "bank");

            builder.Property(x => x.ExternalId).IsRequired();
            builder.Property(x => x.Status).IsRequired();
            builder.Property(x => x.Type).HasDefaultValue(CardType.PhysicalDebit);

            builder.HasOne(x => x.User)
                .WithMany(x => x.DebitCards)
                .HasForeignKey(x => x.UserId)
                .OnDelete(DeleteBehavior.Cascade);


        }
    }
}
