﻿namespace BNine.Persistence.Configurations
{
    using BNine.Domain.Entities.BonusCodes;
    using Extensions;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.EntityFrameworkCore.Metadata.Builders;

    public class BonusCodeActivationsConfiguration : IEntityTypeConfiguration<BonusCodeActivation>
    {
        public void Configure(EntityTypeBuilder<BonusCodeActivation> builder)
        {
            builder.MapTable("BonusCodeActivations");

            builder.HasOne(x => x.BonusCode)
                .WithMany()
                .HasForeignKey(x => x.BonusCodeId)
                .IsRequired(true)
                .OnDelete(DeleteBehavior.NoAction);

            builder.HasOne(x => x.Receiver)
                .WithOne()
                .HasForeignKey<BonusCodeActivation>(x => x.ReceiverId)
                .IsRequired(true)
                .OnDelete(DeleteBehavior.NoAction);

            builder.HasIndex(x => x.CreatedAt)
                .HasDatabaseName("IX_BonusCodeActivations_CreatedAt");

            builder.HasIndex(x => x.UpdatedAt)
                .HasDatabaseName("IX_BonusCodeActivations_UpdatedAt");
        }
    }
}
