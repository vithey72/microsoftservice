﻿namespace BNine.Persistence.Configurations
{
    using Domain.Entities;
    using Extensions;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.EntityFrameworkCore.Metadata.Builders;

    public class BankAccountsConfiguration : IEntityTypeConfiguration<ExternalBankAccount>
    {
        public void Configure(EntityTypeBuilder<ExternalBankAccount> builder)
        {
            builder.MapTable("ExternalBankAccounts");

            builder.Property(x => x.AccountNumber).IsRequired();
            builder.Property(x => x.RoutingNumber).IsRequired();

            builder.HasOne(x => x.User)
                .WithMany(x => x.BankAccounts)
                .HasForeignKey(x => x.UserId)
                .OnDelete(DeleteBehavior.Cascade);
        }
    }
}
