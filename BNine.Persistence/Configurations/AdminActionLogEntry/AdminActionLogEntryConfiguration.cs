﻿namespace BNine.Persistence.Configurations.AdminActionLogEntry;

using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Microsoft.EntityFrameworkCore;
using BNine.Persistence.Configurations.Extensions;

public class AdminActionLogEntryConfiguration
: IEntityTypeConfiguration<Domain.Entities.AdminActionLogEntry.AdminActionLogEntry>
{
    public void Configure(EntityTypeBuilder<Domain.Entities.AdminActionLogEntry.AdminActionLogEntry> builder)
    {
        builder.MapBase("AdminActionLogEntry", "dbo");

        builder.Property(x => x.UpdateAt)
            .IsRequired(true)
            .ValueGeneratedOnAdd()
            .HasDefaultValueSql("getutcdate()");

        builder.Property(x => x.UpdateByUserId).IsRequired(true);
        builder.Property(x => x.UpdateByUserName).IsRequired(true);
        builder.Property(x => x.Type).IsRequired(true);
        builder.Property(x => x.EntityId).IsRequired(true);
        builder.Property(x => x.EntityName).IsRequired(true);
        builder.Property(x => x.BeforeChanges).IsRequired(true);
        builder.Property(x => x.AfterChanges).IsRequired(true);
    }
}
