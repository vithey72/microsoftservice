﻿namespace BNine.Persistence.Configurations;

using BNine.Domain.Entities;
using BNine.Persistence.Configurations.Extensions;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

public class AdminApiUsersConfiguration
    : IEntityTypeConfiguration<AdminApiUser>
{
    public void Configure(EntityTypeBuilder<AdminApiUser> builder)
    {
        builder.MapBase("AdminApiUsers", "admin");

        builder.Property(x => x.CreatedAt)
            .IsRequired(true)
            .ValueGeneratedOnAdd()
            .HasDefaultValueSql("getutcdate()");

        builder.Property(x => x.Email)
            .HasMaxLength(512)
            .IsRequired();

        builder.Property(x => x.FirstName)
            .HasMediumMaxLength()
            .IsRequired(false);

        builder.Property(x => x.LastName)
            .HasMediumMaxLength()
            .IsRequired(false);

        builder.Property(x => x.Password)
            .IsRequired();

        builder.Property(x => x.RoleId)
           .IsRequired();
    }
}

