﻿namespace BNine.Persistence.Configurations.Administrator
{
    using Microsoft.EntityFrameworkCore;
    using Microsoft.EntityFrameworkCore.Metadata.Builders;
    using Domain.Entities.Administrator;
    using Extensions;

    public class AdministratorConfiguration : IEntityTypeConfiguration<Administrator>
    {
        public void Configure(EntityTypeBuilder<Administrator> builder)
        {
            builder.MapBase("Administrators", "administrators");

            builder.Property(x => x.Name)
                .HasMaxLength(512)
                .IsRequired();

            builder.HasMany(x => x.ExternalIdentities)
                .WithOne();
        }
    }
}
