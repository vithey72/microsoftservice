﻿namespace BNine.Persistence.Configurations.Administrator
{
    using BNine.Domain.Entities.Administrator;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.EntityFrameworkCore.Metadata.Builders;

    public class ExternalIdentityConfiguration : IEntityTypeConfiguration<ExternalIdentity>
    {
        public void Configure(EntityTypeBuilder<ExternalIdentity> builder)
        {
            builder.ToTable("ExternalIdentities", "administrators");

            builder.Property(x => x.Provider)
                .HasMaxLength(512)
                .IsRequired();

            builder.Property(x => x.Id)
                .HasMaxLength(512)
                .IsRequired();

            builder.HasOne(x => x.Administrator)
                .WithMany(x => x.ExternalIdentities)
                .HasForeignKey(x => x.AdministratorId)
                .IsRequired();

            builder.HasKey(x => new { x.Id, x.Provider });
        }
    }
}
