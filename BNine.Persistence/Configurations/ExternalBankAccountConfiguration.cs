﻿namespace BNine.Persistence.Configurations
{
    using BNine.Domain.Entities;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.EntityFrameworkCore.Metadata.Builders;

    public class ExternalBankAccountConfiguration : IEntityTypeConfiguration<ExternalBankAccount>
    {
        public void Configure(EntityTypeBuilder<ExternalBankAccount> builder)
        {
            builder.HasIndex(x => x.UpdatedAt)
                .HasDatabaseName("IX_ExternalBankAccounts_UpdatedAt");
        }
    }
}
