﻿
-- export data from destination db to the csv
-- import into destination database, create temp table [Mbanq_transfer_export]
-- apply this migration script
-- remove temp table and csv

DECLARE @ID int
DECLARE @ClientID int
DECLARE @CreditorSavingsAccountTransactionID int

DECLARE export_table_cursor CURSOR FOR
SELECT [ID],
       [ClientID],
       [CreditorSavingsAccountTransactionID]
FROM [Mbanq_transfer_export]
where [CreditorSavingsAccountTransactionID] != 'NULL'
      and exists
		(
		    select top (1)
		        bank.ACHCreditTransfers.ExternalId
		    from bank.ACHCreditTransfers
		    where bank.ACHCreditTransfers.ExternalId = [Mbanq_transfer_export].ID
		)

BEGIN
    BEGIN TRY
        BEGIN TRANSACTION

        OPEN export_table_cursor
        FETCH NEXT FROM export_table_cursor
        INTO @ID,
             @ClientID,
             @CreditorSavingsAccountTransactionID;

        WHILE @@FETCH_STATUS = 0
        BEGIN
            UPDATE bank.ACHCreditTransfers
            SET SavingsAccountTransferExternalId = @CreditorSavingsAccountTransactionID
            WHERE ExternalId = @ID
                  and bank.ACHCreditTransfers.SavingsAccountTransferExternalId = 0
			
            FETCH NEXT FROM export_table_cursor
            INTO @ID,
                 @ClientID,
                 @CreditorSavingsAccountTransactionID
        END

        COMMIT TRANSACTION

		PRINT 'Script completed successfully.';
    END TRY
    BEGIN CATCH
        -- Rollback the transactions
        PRINT ERROR_MESSAGE();
        PRINT 'Error occured! The changes will be rolled back';

        IF (@@TRANCOUNT > 0)
            ROLLBACK TRANSACTION
    END CATCH
END

CLOSE export_table_cursor
DEALLOCATE export_table_cursor
