﻿namespace BNine.MBanq.Constants
{
    using System;
    using System.Collections.Generic;
    using Enums;

    public static class DocumentTypes
    {
        private static readonly string Passport = "Passport";
        private static readonly string ForeignDriverLicense = "Foreign Driver's License";
        private static readonly string ConsularId = "Consular ID";
        private static readonly string Visa = "Visa";
        private static readonly string Other = "Other";
        private static readonly string SSN = "Social Security Number";
        private static readonly string DriverLicense = "Driver's License";
        private static readonly string StateIdentityCard = "State Identity Card";
        private static readonly string WorkPermitCard = "Work permit (EAD) card";
        private static readonly string PermanentResidentCard = "Permanent Resident Card";
        private static readonly string ForeignPassport = "Foreign Passport";
        private static readonly string ITIN = "Taxpayer Identification Number";

        public static string GetExternalName(DocumentType documentType)
        {
            if (ExternalNames.TryGetValue(documentType, out var value))
            {
                return value;
            }

            throw new ArgumentException("Invalid document type");
        }

        private static Dictionary<DocumentType, string> ExternalNames = new()
        {
            {DocumentType.Passport, Passport},
            {DocumentType.ForeignDriverLicense, ForeignDriverLicense},
            {DocumentType.ConsularId, ConsularId},
            {DocumentType.Visa, Visa},
            {DocumentType.Other, Other},
            {DocumentType.SSN, SSN},
            {DocumentType.DriverLicense, DriverLicense},
            {DocumentType.StateIdentityCard, StateIdentityCard},
            {DocumentType.WorkPermitCard, WorkPermitCard},
            {DocumentType.PermanentResidentCard, PermanentResidentCard},
            {DocumentType.ForeignPassport, ForeignPassport},
            {DocumentType.ITIN, ITIN}
        };
    }
}
