﻿namespace BNine.ResourceLibrary
{
    using Microsoft.Extensions.Localization;

    public interface ISharedResource
    {

    }

    public class SharedResource : ISharedResource
    {
        private readonly IStringLocalizer Localizer;

        public SharedResource(IStringLocalizer<SharedResource> localizer)
        {
            Localizer = localizer;
        }

        public string this[string index]
        {
            get
            {
                return Localizer[index];
            }
        }
    }
}
