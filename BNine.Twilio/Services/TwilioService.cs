﻿namespace BNine.Twilio.Services
{
    using System.Text.RegularExpressions;
    using System.Threading.Tasks;
    using BNine.Application.Interfaces;
    using BNine.Settings;
    using global::Twilio;
    using Microsoft.Extensions.Options;
    using global::Twilio.Rest.Api.V2010.Account;
    using global::Twilio.Types;

    public class TwilioService : ISmsService, ICallService
    {
        private readonly TwilioSettings _configuration;
        // Twiml is a Twilio Markup Language
        private static readonly string TwimlBreak = @"<break strength=""strong""/>";
        private static readonly string TwimlPlaceholder = "{TWIML_TEXT}";
        private static readonly string TwimlTemplate =
            @"<Response><Pause length=""2""></Pause><Say voice=""Polly.Joanna"" loop=""2""><prosody rate=""slow"">{TWIML_TEXT}</prosody></Say></Response>";

        private static readonly Regex OtpCodeRegex = new(@"\d{6,}", RegexOptions.None);
        public int? MaxCallsLongLimit
        {
            get;
        }
        public int? MaxCallsShortLimit
        {
            get;
        }

        public TwilioService(IOptions<TwilioSettings> configuration)
        {
            _configuration = configuration.Value;
            MaxCallsLongLimit = configuration.Value.MaxCallsLongLimit;
            MaxCallsShortLimit = configuration.Value.MaxCallsShortLimit;
            TwilioClient.Init(_configuration.AccountSid, _configuration.AuthToken);
        }

        public async Task SendMessageAsync(string to, string body)
        {
            if (_configuration.IsActive)
            {
                await MessageResource.CreateAsync(
                    new PhoneNumber(to),
                    from: new PhoneNumber(_configuration.SenderPhoneNumber),
                    body: body);
            }
        }

        public async Task CallAsync(string to, string body)
        {

            var formattedBody = OtpCodeRegex.Replace(body, match =>
            {
                var formattedNumber = string.Join($" {TwimlBreak}", match.Value.ToCharArray());
                return formattedNumber;
            });

            if (_configuration.IsActive)
            {

                var finalTwiml = TwimlTemplate.Replace(TwimlPlaceholder, formattedBody);

                await CallResource.CreateAsync(
                    new PhoneNumber(to),
                    from: new PhoneNumber(_configuration.CallerPhoneNumber),
                    twiml: finalTwiml);
            }
        }
    }
}
