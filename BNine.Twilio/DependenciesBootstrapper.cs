﻿namespace BNine.Twilio
{
    using BNine.Application.Interfaces;
    using BNine.Twilio.Services;
    using Microsoft.Extensions.DependencyInjection;

    public static class DependenciesBootstrapper
    {
        public static IServiceCollection AddTwilio(this IServiceCollection services)
        {
            services.AddTransient<ISmsService, TwilioService>();
            services.AddTransient<ICallService, TwilioService>();

            return services;
        }
    }
}
