﻿namespace BNine.ServiceBus;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Azure.Messaging.ServiceBus;
using BNine.Application.Interfaces.ServiceBus;
using BNine.Application.Models.ServiceBus;
using BNine.Settings;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;

public class AzureServiceBus : IServiceBus
{
    private const string dateFormat = "yyyy'-'MM'-'dd'T'HH':'mm':'ss.fffffff";

    private readonly AzureServiceBusSettings _azureServiceBusSettings;
    private readonly ILogger<AzureServiceBus> _logger;
    public readonly ServiceBusClient _serviceBusClient;

    public AzureServiceBus(ILogger<AzureServiceBus> logger, IOptions<AzureServiceBusSettings> options, ServiceBusClient serviceBusClient)
    {
        _azureServiceBusSettings = options?.Value ?? throw new ArgumentNullException(nameof(options));
        ArgumentNullException.ThrowIfNull(logger);
        ArgumentNullException.ThrowIfNull(serviceBusClient);

        _logger = logger;
        _serviceBusClient = serviceBusClient;
    }

    public async Task Publish(IntegrationEvent @event, CancellationToken cancellationToken = default)
    {
        _logger.LogInformation("Start publishing event: {EventName}", @event.EventName);
        if (_azureServiceBusSettings.Disabled)
        {
            return;
        }
        var sender = _serviceBusClient.CreateSender($"{_azureServiceBusSettings.EnvironmentTopicPrefix}{@event.TopicName}");
        try
        {
            var message = new ServiceBusMessage()
            {
                Subject = @event.EventName,
                Body = new BinaryData(@event.Body),
                MessageId = Guid.NewGuid().ToString()
            };

            foreach (var item in @event.ApplicationProperties)
            {
                message.ApplicationProperties.Add(item);
            }

            message.ApplicationProperties.Add("createdAt", DateTime.UtcNow.ToString(dateFormat));

            await sender.SendMessageAsync(message, cancellationToken);

            _logger.LogInformation("Event: {EventName} has been published successfully", @event.EventName);
        }
        catch (Exception ex)
        {
            _logger.LogError(ex, "Publishing event: {EventName} has been failed: {ErrorMessage}", @event.EventName, ex.Message);
        }
        finally
        {
            await sender.DisposeAsync();
        }
    }

    public async Task Publish(IList<IntegrationEvent> events, CancellationToken cancellationToken = default)
    {
        _logger.LogInformation("Start publishing events");
        if (_azureServiceBusSettings.Disabled)
        {
            return;
        }

        if (events.Select(x => x.TopicName).Distinct().Count() != 1)
        {
            throw new Exception("Messages should be sent to the same topic");
        }

        var sender = _serviceBusClient.CreateSender($"{_azureServiceBusSettings.EnvironmentTopicPrefix}{events.First().TopicName}");

        try
        {
            var list = new List<ServiceBusMessage>();
            for (var i = 0; i < events.Count(); i++)
            {
                var message = new ServiceBusMessage
                {
                    Subject = events[i].EventName,
                    Body = new BinaryData(events[i].Body),
                    MessageId = Guid.NewGuid().ToString()
                };

                if (events[i].ApplicationProperties.Any())
                {
                    foreach (var property in events[i].ApplicationProperties)
                    {
                        message.ApplicationProperties.Add(property);
                    }

                }

                message.ApplicationProperties.Add("createdAt", DateTime.UtcNow.ToString(dateFormat));

                list.Add(message);
            }

            await sender.SendMessagesAsync(list, cancellationToken);

            _logger.LogInformation("Events have been published successfully");
        }
        catch (Exception ex)
        {
            _logger.LogError(ex, "Publishing events has been failed");
        }
        finally
        {
            await sender.DisposeAsync();
        }
    }
}
