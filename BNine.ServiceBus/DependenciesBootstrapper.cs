﻿namespace BNine.ServiceBus
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using BNine.Application.Interfaces.ServiceBus;
    using BNine.Common.Attributes;
    using BNine.Settings;
    using MediatR;
    using Microsoft.Extensions.Azure;
    using Microsoft.Extensions.Configuration;
    using Microsoft.Extensions.DependencyInjection;

    public static class DependenciesBootstrapper
    {
        public static IServiceCollection AddAzureServiceBus(this IServiceCollection services, IConfiguration configuration)
        {
            var eventsForServiceBus = ArrangeEventsForServiceBus(services);
            services.AddSingleton(eventsForServiceBus);
            services.AddHostedService<AzureServiceBusReceiver>();
            services.AddTransient<IServiceBus, AzureServiceBus>();
            services.AddAzureClients(builder =>
            {
                builder.AddServiceBusClient(configuration.GetSection(nameof(AzureServiceBusSettings)).Get<AzureServiceBusSettings>().ConnectionString);
            });
            return services;
        }

        private static IEnumerable<Tuple<string, Type>> ArrangeEventsForServiceBus(IServiceCollection services)
        {
            var notificationHandlerType = typeof(INotificationHandler<>);

            var types = services.Where(s => s.ServiceType.IsGenericType && s.ServiceType.GetGenericTypeDefinition() == notificationHandlerType).Select(i => i.ServiceType.GetGenericArguments().First());

            var result = new List<Tuple<string, Type>>();
            foreach (var type in types)
            {
                var attribute = type.GetCustomAttributes(typeof(ServiceBusEventAttribute), true).Length > 0 ?
                    type.GetCustomAttributes(typeof(ServiceBusEventAttribute), true)[0] as ServiceBusEventAttribute : null;
                if (attribute != null)
                {
                    var topicName = attribute.TopicName;
                    result.Add(new Tuple<string, Type>(topicName, type));
                }
            }

            return result;
        }
    }
}
