﻿namespace BNine.ServiceBus
{
    using System;
    using System.Collections.Generic;
    using System.Threading;
    using System.Threading.Tasks;
    using Application.Interfaces;
    using Azure.Messaging.ServiceBus;
    using BNine.Constants;
    using BNine.Settings;
    using Domain.Entities.EventAudit;
    using Domain.Interfaces.EventAudit;
    using MediatR;
    using Microsoft.Extensions.DependencyInjection;
    using Microsoft.Extensions.Hosting;
    using Microsoft.Extensions.Logging;
    using Microsoft.Extensions.Options;
    using Newtonsoft.Json;

    public class AzureServiceBusReceiver : IHostedService
    {
        private ILogger<AzureServiceBusReceiver> Logger
        {
            get;
        }

        public ServiceBusClient ServiceBusClient
        {
            get;
        }

        public IServiceProvider ServiceProvider
        {
            get;
        }

        public IEnumerable<Tuple<string, Type>> EventsForServiceBus
        {
            get;
        }

        public IOptions<AuditEventSettings> AuditEventSettings
        {
            get;
        }

        public List<ServiceBusProcessor> ServiceBusProcessors
        {
            get;
        }

        public AzureServiceBusSettings AzureServiceBusSettings
        {
            get;
            private set;
        }

        public AzureServiceBusReceiver(ILogger<AzureServiceBusReceiver> logger,
            ServiceBusClient serviceBusClient,
            IServiceProvider serviceProvider,
            IEnumerable<Tuple<string, Type>> eventsForServiceBus,
            IOptions<AzureServiceBusSettings> options,
            IOptions<AuditEventSettings> auditEventSettings)
        {
            ServiceBusProcessors = new List<ServiceBusProcessor>();
            Logger = logger;
            ServiceBusClient = serviceBusClient;
            ServiceProvider = serviceProvider;
            EventsForServiceBus = eventsForServiceBus;
            AuditEventSettings = auditEventSettings;
            AzureServiceBusSettings = options.Value;
        }

        public async Task StartAsync(CancellationToken cancellationToken)
        {
            foreach (var serviceBusEvent in EventsForServiceBus)
            {
                var processor = ServiceBusClient.CreateProcessor(
                    $"{AzureServiceBusSettings.EnvironmentTopicPrefix}{serviceBusEvent.Item1}",
                    ServiceBusSubscriptions.Main, new ServiceBusProcessorOptions());

                processor.ProcessMessageAsync += async (ProcessMessageEventArgs args) =>
                {
                    var body = args.Message.Body.ToString();
                    Logger.LogInformation($"Received: {body}");

                    var type = serviceBusEvent.Item2;
                    var command = JsonConvert.DeserializeObject(body, type);
                    var isAzureGuidParsed = Guid.TryParse(args.Message.MessageId, out var eventId);
                    var isAuditRequired = command?.GetType().GetInterface(nameof(IBusEventAuditable)) != null
                                           && AuditEventSettings.Value.IsEnabled && isAzureGuidParsed;
                    if (!isAzureGuidParsed)
                    {
                        Logger.LogError($"Failed to parse MessageId: {args.Message.MessageId}");
                    }


                    using (var scope = ServiceProvider.CreateScope())
                    {
                        #region AuditEvent

                        try
                        {
                            // this is the entry point (1st step) of Audit flow
                            // the expected scenario is when the audit entry is created here
                            // TODO move all the audit logic to the separate service
                            if (isAuditRequired)
                            {

                                if (command is IBusEventAuditable auditableEvent)
                                {
                                    // Set the EventId property if the object is auditable
                                    auditableEvent.EventId = eventId;

                                    // Update the command variable with the modified object
                                    command = auditableEvent;
                                }

                                var eventAudit = new EventAudit
                                {
                                    EventId = eventId, EventName = type.ToString(), RawEventModel = body,
                                };

                                var auditDbContext = scope.ServiceProvider.GetRequiredService<IBNineAuditDbContext>();
                                auditDbContext.EventAuditEntries.Add(eventAudit);
                                await auditDbContext.SaveChangesAsync(cancellationToken);
                            }
                        }
                        catch (Exception e)
                        {
                            Logger.LogError(e, "Failed to create audit entry for event: {EventId}", args.Message.MessageId);
                        }
                        #endregion
                        await scope.ServiceProvider.GetRequiredService<IMediator>().Publish(command, cancellationToken);
                    }
                    await args.CompleteMessageAsync(args.Message);
                };

                processor.ProcessErrorAsync += (ProcessErrorEventArgs args) =>
                {
                    Logger.LogError(args.Exception.ToString());
                    return Task.CompletedTask;
                };

                await processor.StartProcessingAsync(cancellationToken);
                ServiceBusProcessors.Add(processor);
            }
        }

        public async Task StopAsync(CancellationToken cancellationToken)
        {
            Logger.LogInformation("\nStopping the receiver...");

            foreach (var processor in ServiceBusProcessors)
            {
                await processor.StopProcessingAsync(cancellationToken);
            }

            Logger.LogInformation("Stopped receiving messages");
        }
    }
}
