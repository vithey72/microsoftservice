﻿namespace BNine.Settings
{
    public class MBanqSettings : IApplicationSettings
    {
        public string Username
        {
            get; set;
        }

        public string Password
        {
            get; set;
        }

        public string ApiUrl
        {
            get; set;
        }

        public string TenantId
        {
            get; set;
        }

        public string ClientSecret
        {
            get; set;
        }

        public string ClientId
        {
            get; set;
        }

        public DebitCardSettings DebitCardSettings
        {
            get; set;
        }
    }

    public class DebitCardSettings
    {
        public string PinSuccessUrl
        {
            get; set;
        }

        public string PinFailureUrl
        {
            get; set;
        }
    }
}
