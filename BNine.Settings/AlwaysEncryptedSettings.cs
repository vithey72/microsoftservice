﻿namespace BNine.Settings
{
    public class AlwaysEncryptedSettings : IApplicationSettings
    {
        public string AzureADClientId
        {
            get; set;
        }

        public string AzureADTenantId
        {
            get; set;
        }

        public string AzureADClientSecret
        {
            get; set;
        }
    }
}
