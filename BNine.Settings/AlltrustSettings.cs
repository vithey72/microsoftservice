﻿namespace BNine.Settings
{
    public class AlltrustSettings : IApplicationSettings
    {
        public string ApiUrl
        {
            get; set;
        }

        public string ApiKey
        {
            get; set;
        }

        public string Password
        {
            get; set;
        }

        /// <summary>
        /// Guid prefixed with "location_".
        /// </summary>
        public string LocationId
        {
            get; set;
        }

        /// <summary>
        /// Guid prefixed with "business_".
        /// </summary>
        public string BusinessId
        {
            get; set;
        }

        /// <summary>
        /// Whether to check for duplicate identification documents for different users when enrolling them into Alltrust.
        /// This check should be false on production, true on test.
        /// </summary>
        public bool DisableCustomerDuplicatesCheck
        {
            get; set;
        }

        /// <summary>
        /// Whether to allow creating transactions with duplicate data.
        /// This check should be false on production, true on test.
        /// </summary>
        public bool AllowDuplicateChecks
        {
            get; set;
        }

        /// <summary>
        /// Whether to set 'accepted' state for checks as soon as redemption images are uploaded.
        /// </summary>
        public bool AutoAcceptChecksOnRedemption
        {
            get; set;
        }
    }
}
