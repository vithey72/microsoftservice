﻿namespace BNine.Settings
{
    public class SendGridSettings : IApplicationSettings
    {
        public string ApiKey
        {
            get; set;
        }

        public string SenderEmail
        {
            get; set;
        }

        public string SenderName
        {
            get; set;
        }

        public bool IsEmailSendingEnabled
        {
            get; set;
        }

        public string TestEmailAddress
        {
            get; set;
        }

        public string BNineDomain
        {
            get; set;
        }

        public int OperationalGroupId
        {
            get; set;
        }

        public int MarketingGroupId
        {
            get; set;
        }
    }
}
