﻿namespace BNine.Settings.Firebase
{
    using Newtonsoft.Json;

    public class FirebasePrivateKeySettings
    {
        [JsonProperty("type")]
        public string Type
        {
            get; set;
        }

        [JsonProperty("project_id")]
        public string Project_id
        {
            get; set;
        }

        [JsonProperty("private_key_id")]
        public string Private_key_id
        {
            get; set;
        }

        [JsonProperty("private_key")]
        public string Private_key
        {
            get; set;
        }

        [JsonProperty("client_email")]
        public string Client_email
        {
            get; set;
        }

        [JsonProperty("client_id")]
        public string Client_id
        {
            get; set;
        }

        [JsonProperty("auth_uri")]
        public string Auth_uri
        {
            get; set;
        }

        [JsonProperty("token_uri")]
        public string Token_uri
        {
            get; set;
        }

        [JsonProperty("auth_provider_x509_cert_url")]
        public string Auth_provider_x509_cert_url
        {
            get; set;
        }

        [JsonProperty("client_x509_cert_url")]
        public string Client_x509_cert_url
        {
            get; set;
        }
    }
}
