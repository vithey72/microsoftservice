﻿namespace BNine.Settings.Firebase
{
    public class DynamicLinkSettings
    {
        public string Link
        {
            get;
            set;
        }

        public string DomainUriPrefix
        {
            get;
            set;
        }

        public string AndroidPackageName
        {
            get;
            set;
        }

        public string IosBundleId
        {
            get;
            set;
        }
    }
}
