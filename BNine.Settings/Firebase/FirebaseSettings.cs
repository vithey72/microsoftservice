﻿namespace BNine.Settings.Firebase
{
    public class FirebaseSettings : IApplicationSettings
    {
        public string ApiKey
        {
            get;
            set;
        }

        public DynamicLinkSettings DynamicLink
        {
            get;
            set;
        }

        public FirebasePrivateKeySettings PrivateKey
        {
            get;
            set;
        }
    }
}
