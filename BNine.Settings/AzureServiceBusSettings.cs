﻿namespace BNine.Settings
{
    public class AzureServiceBusSettings : IApplicationSettings
    {
        public string ConnectionString
        {
            get; set;
        }

        public bool Disabled
        {
            get; set;
        }

        public string EnvironmentTopicPrefix
        {
            get; set;
        }
    }
}
