﻿namespace BNine.Settings.DwhIntegration
{
    public class DwhIntegrationSettings : IApplicationSettings
    {
        public string ApiUrl
        {
            get; set;
        }

        public string LogAdminActionXFunctionsKey
        {
            get; set;
        }

        public string AdvancesHistoryXFunctionsKey
        {
            get; set;
        }

        public string GetUserLimitsXFunctionsKey
        {
            get; set;
        }

        public string GetUserCashbackInfoXFunctionsKey
        {
            get; set;
        }

        public string AdvanceEmulatorApiUrl
        {
            get;
            set;
        }

        public string AdvanceEmulatorAuthCode
        {
            get;
            set;
        }
    }
}
