﻿namespace BNine.Settings.Blob
{
    public class BlobStorageSettings : IApplicationSettings
    {
        public string ConnectionString
        {
            get;
            set;
        }

        public ClientDocumentsContainer ClientDocumentsContainer
        {
            get;
            set;
        }
    }

    public class ClientDocumentsContainer
    {
        public string Name
        {
            get;
            set;
        }
    }
}
