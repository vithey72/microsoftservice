﻿namespace BNine.Settings
{
    public class TwilioSettings : IApplicationSettings
    {
        public string AccountSid
        {
            get; set;
        }

        public string AuthToken
        {
            get; set;
        }

        public string SenderPhoneNumber
        {
            get; set;
        }

        public string CallerPhoneNumber
        {
            get;
            set;
        }

        public bool IsActive
        {
            get; set;
        }

        public int? MaxCallsLongLimit
        {
            get;
            set;
        }

        public int? MaxCallsShortLimit
        {
            get;
            set;
        }
    }
}
