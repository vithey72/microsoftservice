﻿namespace BNine.Settings
{
    public class DocsAlloySettings : IApplicationSettings
    {
        public string ApiUrl
        {
            get; set;
        }

        public string ApplicationUsersToken
        {
            get; set;
        }

        public string ApplicationUsersSecret
        {
            get; set;
        }

        public string ApplicationDocumentsToken
        {
            get; set;
        }

        public string ApplicationDocumentsSecret
        {
            get; set;
        }
    }
}
