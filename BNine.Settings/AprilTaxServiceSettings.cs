﻿namespace BNine.Settings
{
    using Enums;

    public class AprilTaxServiceSettings : IApplicationSettings
    {
        /// <summary>
        /// Address for communication with April API.
        /// </summary>
        public string ApiUrl
        {
            get;
            set;
        }

        /// <summary>
        /// Address for building URLs for user.
        /// </summary>
        public string ApplicationUrl
        {
            get;
            set;
        }

        /// <summary>
        /// API identifier.
        /// </summary>
        public string PartnerId
        {
            get;
            set;
        }

        /// <summary>
        /// API secret.
        /// </summary>
        public string PartnerSecret
        {
            get;
            set;
        }

        /// <summary>
        /// Determines mode of the application.
        /// </summary>
        public AprilApplicationMode ApplicationType
        {
            get;
            set;
        }

        /// <summary>
        /// Same as above.
        /// </summary>
        public AprilApplicationSubtype ApplicationSubtype
        {
            get;
            set;
        }
    }
}
