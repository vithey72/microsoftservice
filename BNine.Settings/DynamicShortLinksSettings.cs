﻿namespace BNine.Settings
{
    public class DynamicShortLinksSettings : IApplicationSettings
    {
        public string ContinueOnboardingLink
        {
            get;
            set;
        }

        public string SwitchDepositLink
        {
            get;
            set;
        }

    }
}
