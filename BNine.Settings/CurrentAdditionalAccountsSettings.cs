﻿namespace BNine.Settings;

using Enums;

public class CurrentAdditionalAccountsSettings : IApplicationSettings
{
  public List<AccountProduct> AccountProducts { get; set; }
}

public class AccountProduct
{
    public int Id { get; set; }
    public CurrentAdditionalAccountEnum AccountType { get; set; }
}
