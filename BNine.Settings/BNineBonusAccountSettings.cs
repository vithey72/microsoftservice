﻿namespace BNine.Settings
{
    public  class BNineBonusAccountSettings : IApplicationSettings
    {
        public string AccountNumber
        {
            get; set;
        }

        public string RoutingNumber
        {
            get; set;
        }

        public string SavingsAccountId
        {
            get; set;
        }

        public string Name
        {
            get; set;
        }

        public string Address
        {
            get; set;
        }
    }
}
