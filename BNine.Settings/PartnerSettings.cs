﻿namespace BNine.Settings;

using Enums;

public class PartnerSettings : IApplicationSettings
{
    public PartnerApp AppVariant
    {
        get;
        set;
    }

    public bool OverrideByHttpHeader
    {
        get;
        set;
    }
}
