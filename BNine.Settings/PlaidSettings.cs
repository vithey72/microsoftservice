﻿namespace BNine.Settings
{
    public class PlaidSettings : IApplicationSettings
    {
        public string ApiUrl
        {
            get; set;
        }

        public string ClientId
        {
            get; set;
        }

        public string ClientSecret
        {
            get; set;
        }

        public string ClientName
        {
            get; set;
        }

        public string Webhook
        {
            get; set;
        }

        public string AndroidPackageName
        {
            get; set;
        }
    }
}
