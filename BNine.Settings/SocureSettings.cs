﻿namespace BNine.Settings;

public class SocureSettings : IApplicationSettings
{
    /// <summary>
    /// For example https://sandbox.socure.com/.
    /// </summary>
    public string ApiUrl
    {
        get;
        set;
    }

    public string ApiKey
    {
        get; set;
    }

    /// <summary>
    /// Normally https://upload.socure.com/.
    /// </summary>
    public string DocumentsApiUrl
    {
        get;
        set;
    }

    public bool OverrideSocureDecisions
    {
        get;
        set;
    }
}
