﻿namespace BNine.Settings
{
    using System;

    public class ArraySettings : IApplicationSettings
    {
        public string ApiUrl
        {
            get;
            set;
        }

        public Guid ApiKey
        {
            get;
            set;
        }

        public Guid AppKey
        {
            get;
            set;
        }
    }
}
