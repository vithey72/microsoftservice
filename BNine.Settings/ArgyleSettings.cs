﻿namespace BNine.Settings
{
    public class ArgyleSettings : IApplicationSettings
    {
        public string ApiUrl
        {
            get; set;
        }

        public string ClientId
        {
            get; set;
        }

        public string ClientSecret
        {
            get; set;
        }

        public string PluginKey
        {
            get; set;
        }

        public string RoutingNumber
        {
            get; set;
        }
    }
}
