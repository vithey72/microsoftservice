﻿namespace BNine.Settings
{
    public class EmailTemplatesSettings : IApplicationSettings
    {
        public string RegistrationDeniedTemplate
        {
            get;
            set;
        }

        public string RegistrationFinishedTemplate
        {
            get;
            set;
        }

        public string OnboardingNotCompletedFirstTemplate
        {
            get;
            set;
        }

        public string OnboardingNotCompletedSecondTemplate
        {
            get;
            set;
        }

        public string OnboardingNotCompletedThirdTemplate
        {
            get;
            set;
        }

        public string ConnectArgyleProposalFirstTemplate
        {
            get;
            set;
        }

        public string ConfirmEmailUpdateTemplate
        {
            get;
            set;
        }

        public string EmailUpdatedTemplate
        {
            get;
            set;
        }

        public string PhoneUpdatedTemplate
        {
            get;
            set;
        }

        public string DepositEnrolmentTemplate
        {
            get;
            set;
        }

        public string SpendingAccountStatement
        {
            get;
            set;
        }

        public string PasswordChangedTemplate
        {
            get;
            set;
        }

        public string AddressChangedTemplate
        {
            get;
            set;
        }

        public string AccountBlockedTemplate
        {
            get;
            set;
        }

        public string AccountUnblockedTemplate
        {
            get;
            set;
        }

        public string CardBlockedTemplate
        {
            get;
            set;
        }

        public string CardUnblockedTemplate
        {
            get;
            set;
        }

        public string AccountBlockedByUnusualActivity
        {
            get;
            set;
        }

        public string EarlySalaryManualFormTemplate
        {
            get;
            set;
        }

        public string MonthlyPaymentDeclinedTemplate
        {
            get;
            set;
        }

        public string CardShippedTemplate
        {
            get;
            set;
        }
    }
}
