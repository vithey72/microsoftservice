﻿namespace BNine.Settings
{
    public class AppsflyerSettings
    {
        public string S2SUri
        {
            get; set;
        }

        public string DevKey
        {
            get; set;
        }

        public string IOSBundleId
        {
            get; set;
        }

        public string AndroidBundleId
        {
            get; set;
        }
    }
}
