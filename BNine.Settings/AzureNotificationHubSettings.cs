﻿namespace BNine.Settings
{
    public class AzureNotificationHubSettings : IApplicationSettings
    {
        public string ConnectionString
        {
            get; set;
        }

        public string HubName
        {
            get; set;
        }
    }
}
