﻿namespace BNine.DocsAlloy.Services.Base;

using System.Net;
using System.Net.Http.Headers;
using System.Text;
using Common;
using Constants;
using Microsoft.Extensions.Options;
using Models.Responses.Error;
using Settings;

[Obsolete]
public class DocsAlloyBaseService
{
    protected readonly IHttpClientFactory ClientFactory;
    protected readonly DocsAlloySettings Settings;

    protected DocsAlloyBaseService(IOptions<DocsAlloySettings> options, IHttpClientFactory clientFactory)
    {
        Settings = options.Value;
        ClientFactory = clientFactory;
    }

    protected Task<HttpClient> CreateAuthorizedUserWorkflowClient()
    {
        var client = ClientFactory.CreateClient(HttpClientName.DocsAlloy);

        client.DefaultRequestHeaders.Authorization =
            new AuthenticationHeaderValue("Basic",
                Convert.ToBase64String(Encoding.ASCII
                    .GetBytes(Settings.ApplicationUsersToken + ":" + Settings.ApplicationUsersSecret)));

        return Task.FromResult(client);
    }

    protected Task<HttpClient> CreateAuthorizedDocumentVerificationClient()
    {
        var client = ClientFactory.CreateClient(HttpClientName.DocsAlloy);

        client.DefaultRequestHeaders.Authorization =
            new AuthenticationHeaderValue("Basic",
                Convert.ToBase64String(Encoding.ASCII
                    .GetBytes(Settings.ApplicationDocumentsToken + ":" + Settings.ApplicationDocumentsSecret)));

        return Task.FromResult(client);
    }

    protected async Task HandleErrors(HttpResponseMessage response)
    {
        if (response.StatusCode == HttpStatusCode.OK)
        {
            var responseBody = await SerializationHelper.GetResponseContent<ErrorOkResponse>(response);

            var ex = new Exception(responseBody.StatusCode.ToString()) { Data = { { "ErrorResponse", responseBody } } };

            throw ex;
        }

        if (!response.IsSuccessStatusCode)
        {
            if (response.StatusCode == HttpStatusCode.BadRequest || response.StatusCode == HttpStatusCode.Forbidden)
            {
                var responseBody = await SerializationHelper.GetResponseContent<ErrorBadRequestResponse>(response);

                var ex = new Exception(responseBody.StatusCode.ToString())
                {
                    Data = { { "ErrorResponse", responseBody } }
                };

                throw ex;
            }

            throw new Exception(response.ReasonPhrase);
        }
    }
}
