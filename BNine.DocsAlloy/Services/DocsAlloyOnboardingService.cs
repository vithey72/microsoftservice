﻿namespace BNine.DocsAlloy.Services
{
    using System;
    using System.Net.Http;
    using System.Threading.Tasks;
    using Application.Interfaces.Alloy;
    using BNine.Application.Aggregates.Evaluation.Commands;
    using BNine.Application.Aggregates.Evaluation.Models;
    using BNine.Application.Exceptions;
    using BNine.Common;
    using BNine.DocsAlloy.Services.Base;
    using BNine.Settings;
    using Microsoft.Extensions.Options;

    public class DocsAlloyOnboardingService : DocsAlloyBaseService, IDocsAlloyOnboardingService
    {
        private const string EntityTokenHeader = "Alloy-Entity-Token";
        public DocsAlloyOnboardingService(IOptions<DocsAlloySettings> options, IHttpClientFactory clientFactory)
            : base(options, clientFactory)
        {
        }

        public async Task<string> GetEvaluationParameters()
        {
            var client = await CreateAuthorizedUserWorkflowClient();

            var uri = "/v1/parameters";

            var response = await client.GetAsync(uri);

            if (!response.IsSuccessStatusCode)
            {
                throw new Exception($"Alloy: {response.ReasonPhrase}");
            }

            return await response.Content.ReadAsStringAsync();
        }

        public async Task<EvaluationSuccessResponse> EvaluateUser(EvaluateUserCommand command, string entityToken)
        {
            var client = await CreateAuthorizedUserWorkflowClient();

            var uri = "/v1/evaluations";

            var content = SerializationHelper.GetRequestContent(command);

            var request = new HttpRequestMessage(HttpMethod.Post, uri) { Content = content };
            if (entityToken != null)
            {
                request.Headers.Add(EntityTokenHeader, entityToken);
            }

            var response = await client.SendAsync(request);

            try
            {
                await HandleErrors(response);
            }
            catch (Exception ex)
            {
                throw new BadRequestException($"Alloy: {ex.Message}");
            }

            var serializedResponse = await SerializationHelper.GetResponseContent<EvaluationSuccessResponse>(response);

            var responseContent = await response.Content.ReadAsStringAsync();
            serializedResponse.RawResult = responseContent;

            return serializedResponse;
        }
    }
}
