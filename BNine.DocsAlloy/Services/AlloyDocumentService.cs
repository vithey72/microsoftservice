﻿namespace BNine.DocsAlloy.Services
{
    using System;
    using System.Collections.Generic;
    using System.Net.Http;
    using System.Threading.Tasks;
    using Application.Aggregates.Users.Models;
    using Application.Exceptions;
    using Application.Interfaces.Alloy;
    using Base;
    using Common;
    using Microsoft.Extensions.Options;
    using Settings;

    [Obsolete]
    public class AlloyDocumentService : DocsAlloyBaseService, IAlloyDocumentService
    {
        public AlloyDocumentService(IOptions<DocsAlloySettings> options, IHttpClientFactory clientFactory) : base(
            options, clientFactory)
        {
        }

        public async Task<byte[]> GetDocument(string entityToken, string documentToken)
        {
            var client = await CreateAuthorizedUserWorkflowClient();

            var uri = $"/v1/entities/{entityToken}/documents/{documentToken}";

            var request = new HttpRequestMessage(HttpMethod.Get, uri);

            var response = await client.SendAsync(request);

            try
            {
                if (!response.IsSuccessStatusCode)
                {
                    await HandleErrors(response);
                }
            }
            catch (Exception ex)
            {
                throw new BadRequestException($"Alloy: {ex.Message}");
            }

            return await response.Content.ReadAsByteArrayAsync();
        }

        public async Task<IEnumerable<AlloyDocumentDetails>> GetDocuments(string entityToken)
        {
            var client = await CreateAuthorizedUserWorkflowClient();

            var uri = $"/v1/entities/{entityToken}/documents";

            var request = new HttpRequestMessage(HttpMethod.Get, uri);

            var response = await client.SendAsync(request);

            try
            {
                if (!response.IsSuccessStatusCode)
                {
                    await HandleErrors(response);
                }
            }
            catch (Exception ex)
            {
                throw new BadRequestException($"Alloy: {ex.Message}");
            }

            return await SerializationHelper.GetResponseContent<IEnumerable<AlloyDocumentDetails>>(response);
        }
    }
}
