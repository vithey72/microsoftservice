﻿namespace BNine.DocsAlloy
{
    using System;
    using Application.Interfaces.Alloy;
    using BNine.Constants;
    using Microsoft.Extensions.DependencyInjection;
    using Services;
    using Polly;
    using Microsoft.Extensions.Configuration;

    public static class DependenciesBootstrapper
    {
        public static IServiceCollection AddDocsAlloy(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddTransient<IDocsAlloyOnboardingService, DocsAlloyOnboardingService>();
            services.AddTransient<IAlloyDocumentService, AlloyDocumentService>();
            services.AddHttpClient(HttpClientName.DocsAlloy, client =>
            {
                client.BaseAddress = new Uri(configuration["DocsAlloySettings:ApiUrl"]);
            })
            .AddTransientHttpErrorPolicy(p => p.WaitAndRetryAsync(3, (x) => TimeSpan.FromSeconds(x)));

            return services;
        }
    }
}
