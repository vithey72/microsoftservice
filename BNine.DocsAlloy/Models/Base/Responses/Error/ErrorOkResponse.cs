﻿namespace BNine.DocsAlloy.Models.Responses.Error
{
    using System.Collections.Generic;
    using Newtonsoft.Json;

    internal class ErrorOkResponse
    {
        [JsonProperty("status_code")]
        internal int StatusCode
        {
            get; set;
        }

        [JsonProperty("evaluation_token")]
        internal string EvaluationToken
        {
            get; set;
        }

        [JsonProperty("entity_token")]
        internal string EntityToken
        {
            get; set;
        }

        [JsonProperty("required")]
        internal List<Required> Required
        {
            get; set;
        }

    }

    internal class Required
    {
        [JsonProperty("key")]
        internal string Key
        {
            get; set;
        }

        [JsonProperty("type")]
        internal string Type
        {
            get; set;
        }

        [JsonProperty("description")]
        internal string Description
        {
            get; set;
        }

        [JsonProperty("message")]
        internal string Message
        {
            get; set;
        }
    }

}
