﻿namespace BNine.DocsAlloy.Models.Responses.Error
{
    using Newtonsoft.Json;

    internal class ErrorBadRequestResponse
    {
        [JsonProperty("status_code")]
        internal int StatusCode
        {
            get; set;
        }

        [JsonProperty("error")]
        internal Error Error
        {
            get; set;
        }
    }

    internal class Error
    {
        [JsonProperty("minor_code")]
        internal string MinorCode
        {
            get; set;
        }

        [JsonProperty("type")]
        internal string Type
        {
            get; set;
        }

        [JsonProperty("details")]
        internal Details Details
        {
            get; set;
        }
    }

    internal class Details
    {
        [JsonProperty("message")]
        internal string Message
        {
            get; set;
        }

        [JsonProperty("path")]
        internal string Path
        {
            get; set;
        }
    }
}
