﻿namespace BNine.DocsAlloy.Models.Responses
{
    using Newtonsoft.Json;

    internal class EvaluateUserCreatedResponse
    {
        [JsonProperty("status_code")]
        internal string StatusCode
        {
            get; set;
        }

        [JsonProperty("evaluation_token")]
        internal string EvaluationToken
        {
            get; set;
        }

        [JsonProperty("entity_token")]
        internal string EntityToken
        {
            get; set;
        }

        [JsonProperty("summary")]
        internal Summary Summary
        {
            get; set;
        }
    }

    internal class Summary
    {
        [JsonProperty("result")]
        internal string Result
        {
            get; set;
        }

        [JsonProperty("outcome")]
        internal string OutCome
        {
            get; set;
        }
    }
}
