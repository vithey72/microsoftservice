﻿namespace BNine.DocsAlloy.Models.Requests
{
    using Newtonsoft.Json;

    internal class EvaluateUserRequest
    {
        [JsonProperty("phone_number")]
        public string PhoneNumber
        {
            get; set;
        }

        [JsonProperty("name_first")]
        public string FirstName
        {
            get; set;
        }

        [JsonProperty("name_last")]
        public string LastName
        {
            get; set;
        }

        [JsonProperty("email_address")]
        public string Email
        {
            get; set;
        }

        [JsonProperty("birth_date")]
        public string BirthDate
        {
            get; set;
        }

        [JsonProperty("address_line_1")]
        public string Address
        {
            get; set;
        }

        [JsonProperty("address_city")]
        public string City
        {
            get; set;
        }

        [JsonProperty("address_state")]
        public string State
        {
            get; set;
        }

        [JsonProperty("document_ssn")]
        public string SsnNumber
        {
            get; set;
        }

        [JsonProperty("address_postal_code")]
        public string PostalCode
        {
            get; set;
        }

        [JsonProperty("address_country_code")]
        public string CountryCode
        {
            get; set;
        }

        [JsonProperty("ip_address_v4")]
        public string IpAddress
        {
            get; set;
        }

        [JsonProperty("iovation_blackbox")]
        public string IovationBlackBox
        {
            get; set;
        }
    }
}
