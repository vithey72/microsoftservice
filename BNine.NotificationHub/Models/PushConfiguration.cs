﻿namespace BNine.NotificationHub.Models
{
    internal class PushConfiguration
    {
        public class SampleConfiguration
        {
            public string PrimaryConnectionString
            {
                get; set;
            }

            public string HubName
            {
                get; set;
            }

            public string Tag
            {
                get; set;
            }

            public string AppleDeviceId
            {
                get; set;
            }

            public string SendType
            {
                get; set;
            }

            public string AppleGroupId
            {
                get; set;
            }

            public enum Operation
            {
                Broadcast,
                SendByTag,
                SendByDevice
            }
        }
    }
}
