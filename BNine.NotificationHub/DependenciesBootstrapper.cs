﻿namespace BNine.NotificationHub
{
    using BNine.Application.Interfaces;
    using BNine.NotificationHub.Services;
    using Microsoft.Extensions.DependencyInjection;

    public static class DependenciesBootstrapper
    {
        public static IServiceCollection AddNotificationHub(this IServiceCollection services)
        {
            services.AddTransient<IPushNotificationService, PushNotificationService>();

            return services;
        }
    }
}
