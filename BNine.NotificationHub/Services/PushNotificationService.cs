﻿namespace BNine.NotificationHub.Services
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using BNine.Application.Interfaces;
    using BNine.Application.Models.Notifications;
    using BNine.Common;
    using BNine.Enums;
    using BNine.Settings;
    using Microsoft.Azure.NotificationHubs;
    using Microsoft.Azure.NotificationHubs.Messaging;
    using Microsoft.Extensions.Options;
    using Newtonsoft.Json;

    public class PushNotificationService
        : IPushNotificationService
    {
        private NotificationHubClient HubClient
        {
            get;
        }

        public PushNotificationService(IOptions<AzureNotificationHubSettings> options)
        {
            var settings = options.Value;

            HubClient = NotificationHubClient
                .CreateClientFromConnectionString(settings.ConnectionString, settings.HubName);
        }

        /// <summary>
        /// Get registration ID from Azure Notification Hub
        /// </summary>
        public async Task<string> CreateRegistrationId()
        {
            return await HubClient.CreateRegistrationIdAsync();
        }

        /// <summary>
        /// Delete registration ID from Azure Notification Hub
        /// </summary>
        /// <param name="registrationId"></param>
        public async Task DeleteRegistration(string registrationId)
        {
            await HubClient.DeleteRegistrationAsync(registrationId);
        }

        /// <summary>
        /// Register device to receive push notifications.
        /// Registration ID obtained from Azure Notification Hub has to be provided
        /// Then basing on platform (Android, iOS or Windows) specific
        /// handle (token) obtained from Push Notification Service has to be provided
        /// </summary>
        /// <param name="id"></param>
        /// <param name="deviceUpdate"></param>
        /// <returns></returns
        public async Task RegisterForPushNotifications(string id, DeviceRegistration deviceUpdate)
        {
            RegistrationDescription registrationDescription = deviceUpdate.Platform switch
            {
                MobilePlatform.iOs => new AppleRegistrationDescription(deviceUpdate.Handle),
                MobilePlatform.Android => new FcmRegistrationDescription(deviceUpdate.Handle),
                _ => throw new Exception("Platform error")
            };

            registrationDescription.RegistrationId = id;

            if (deviceUpdate.Tags != null)
            {
                registrationDescription.Tags = new HashSet<string>(deviceUpdate.Tags);
            }

            try
            {
                await HubClient.CreateOrUpdateRegistrationAsync(registrationDescription);
            }
            catch (MessagingException ex)
            {
                throw new Exception("Registration failed because of HttpStatusCode.Gone. Please register once again.", ex);
            }
        }

        /// <summary>
        /// Send push notification to both platform (Android and iOS)
        /// </summary>
        /// <param name="notification"></param>
        /// <returns></returns>
        public async Task SendNotification(Application.Models.Notifications.Notification notification)
        {
            foreach (var platform in (IEnumerable<MobilePlatform>)Enum.GetValues(typeof(MobilePlatform)))
            {
                await SendToPlatform(notification, platform);
            }
        }

        /// <summary>
        /// Send push notification to both platform (Android and iOS)
        /// </summary>
        public async Task SendBulkNotificationToSelectedPlatform(BulkNotification notification, MobilePlatform platform)
        {
            await QueueExecutor.ExecuteInQueueAsync(notification.Tags, 20, async (x) =>
            {
                try
                {
                    switch (platform)
                    {
                        case MobilePlatform.iOs:
                            var r = await HubClient.SendAppleNativeNotificationAsync(
                                GetApplePayload(notification.Body, notification.Deeplink,
                                    notification.AppsflyerEventType, notification.Title), x);
                            break;

                        case MobilePlatform.Android:
                            var t = await HubClient.SendFcmNativeNotificationAsync(
                                GetFCMPayload(notification.Body, notification.Deeplink,
                                    notification.AppsflyerEventType, notification.Title), x);
                            break;
                    }
                }
                catch (Exception)
                {
                }
            });
        }

        private async Task SendToPlatform(Application.Models.Notifications.Notification notification, MobilePlatform platform)
        {
            var tags = notification.NotificationTargets
                .Where(x => x.MobilePlatform == platform)
                .Select(x => x.Value)
                .ToList();

            await QueueExecutor.ExecuteInQueueAsync(tags, 20, async (values) =>
            {
                try
                {
                    switch (platform)
                    {
                        case MobilePlatform.iOs:
                            await HubClient.SendAppleNativeNotificationAsync(
                                GetApplePayload(notification.Body, notification.Deeplink,
                                    notification.AppsflyerEventType, notification.Title,
                                    notification.Sub1, notification.IsSilent), values);
                            break;

                        case MobilePlatform.Android:
                            await HubClient.SendFcmNativeNotificationAsync(
                                GetFCMPayload(
                                    notification.Body, notification.Deeplink,
                                    notification.AppsflyerEventType, notification.Title,
                                    notification.Sub1, notification.IsSilent), values);
                            break;
                    }
                }
                catch
                {
                }
            });
        }

        private string GetApplePayload(string content, string deeplink, string appsflyerEventType,
            string title, string sub1 = null, bool isSilent = false)
        {
            if (isSilent)
            {
                return JsonConvert.SerializeObject(new
                {
                    aps = new SilentPushApsObject { ContentAvailable = 1 },
                    deeplink,
                    appsflyerEventType,
                    sub1,
                });
            }
            var jsonPayload = JsonConvert.SerializeObject(new
            {
                aps = new
                {
                    alert = new
                    {
                        title = title,
                        body = content
                    },
                    sound = "default"
                },
                badge = 1,
                deeplink,
                appsflyerEventType,
                sub1,
            });

            return jsonPayload.ToString();
        }

        private string GetFCMPayload(string content, string deeplink, string appsflyerEventType,
            string title, string sub1 = null, bool isSilentPush = false)
        {

            if (isSilentPush)
            {
                return JsonConvert.SerializeObject(
                    new
                    {
                        data = new
                        {
                            title = title,
                            body = content,
                            deeplink,
                            appsflyerEventType,
                            sub1,
                        },
                    });
            }
            return JsonConvert.SerializeObject(
                new
                {
                    notification = new
                    {
                        title = title,
                        body = content,
                    },
                    data = new
                    {
                        deeplink,
                        appsflyerEventType,
                        sub1,
                    }
                });
        }

        private struct SilentPushApsObject
        {
            [JsonProperty("content-available")]
            public int ContentAvailable
            {
                get;
                set;
            }
        }
    }
}
