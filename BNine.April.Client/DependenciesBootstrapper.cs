﻿namespace BNine.April.Client
{
    using System;
    using Application.Interfaces;
    using Constants;
    using Microsoft.Extensions.Configuration;
    using Microsoft.Extensions.DependencyInjection;
    using Services;

    public static class DependenciesBootstrapper
    {
        public static IServiceCollection AddAprilTaxServiceClient(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddTransient<IAprilTaxService, AprilTaxService>();
            services.AddHttpClient(HttpClientName.April, client =>
            {
                client.BaseAddress = new Uri(configuration["AprilTaxServiceSettings:ApiUrl"]);
            });

            return services;
        }
    }
}
