﻿namespace BNine.April.Client.Models;

using Application.Converters;
using Enums;
using Newtonsoft.Json;

internal class GetLoginHashRequest
{
    [JsonProperty("applicationIdentifier")]
    [JsonConverter(typeof(StringEnumMemberConverter<AprilApplicationSubtype>))]
    internal AprilApplicationSubtype ApplicationSubtype
    {
        get;
        set;
    }
}
