﻿namespace BNine.April.Client.Models;

using Newtonsoft.Json;

internal class GetLoginHashResponse
{
    [JsonProperty("hash")]
    internal string Hash
    {
        get;
        set;
    }
}
