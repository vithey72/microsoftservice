﻿namespace BNine.April.Client.Models;

using Newtonsoft.Json;

internal class GetAprilAccessTokenResponse
{
    /// <summary>
    /// User's Bearer token for accessing April.
    /// </summary>
    [JsonProperty("token")]
    internal string Token
    {
        get;
        set;
    } = string.Empty;

    [JsonProperty("status")]
    internal ResponseStatus? Status
    {
        get;
        set;
    }


    internal class ResponseStatus
    {
        [JsonProperty("code")]
        internal string? Code
        {
            get;
            set;
        }

        [JsonProperty("message")]
        internal string? Message
        {
            get;
            set;
        }
    }
}
