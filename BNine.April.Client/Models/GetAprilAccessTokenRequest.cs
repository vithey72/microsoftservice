﻿namespace BNine.April.Client.Models;

using Application.Converters;
using Enums;
using Newtonsoft.Json;

internal class GetAprilAccessTokenRequest
{
    [JsonProperty("taxYear")]
    internal int TaxYear
    {
        get;
        set;
    }

    [JsonProperty("applicationType")]
    [JsonConverter(typeof(StringEnumMemberConverter<AprilApplicationMode>))]
    internal AprilApplicationMode ApplicationMode
    {
        get;
        set;
    }

    /// <summary>
    /// API identifier.
    /// </summary>
    [JsonProperty("partnerId")]
    internal string? PartnerId
    {
        get;
        set;
    }

    /// <summary>
    /// API key.
    /// </summary>
    [JsonProperty("partnerSecret")]
    internal string? PartnerSecret
    {
        get;
        set;
    }

    /// <summary>
    /// User's email.
    /// </summary>
    [JsonProperty("userId")]
    internal string? UserId
    {
        get;
        set;
    }

    /// <summary>
    /// User's GUID in our system.
    /// </summary>
    [JsonProperty("referralId")]
    internal Guid ReferralId
    {
        get;
        set;
    }
}
