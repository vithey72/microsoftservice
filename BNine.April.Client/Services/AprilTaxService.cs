﻿namespace BNine.April.Client.Services;

using System.Net;
using System.Net.Http.Headers;
using Application.Interfaces;
using Common;
using Constants;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Models;
using Settings;

public class AprilTaxService : IAprilTaxService
{
    private readonly ILogger<AprilTaxService> _logger;
    private readonly IHttpClientFactory _clientFactory;
    private readonly AprilTaxServiceSettings _settings;

    public AprilTaxService(
        ILogger<AprilTaxService> logger,
        IHttpClientFactory clientFactory,
        IOptions<AprilTaxServiceSettings> options)
    {
        _logger = logger;
        _clientFactory = clientFactory;
        _settings = options.Value;
    }

    public async Task<string> GetAccessToken(Guid userId, string userEmail, CancellationToken token)
    {
        var uri = "/v1/authentication/authorize";

        var client = CreateAprilClient();
        var body = new GetAprilAccessTokenRequest
        {
            UserId = userEmail,
            TaxYear = 2022,
            ApplicationMode = _settings.ApplicationType,
            PartnerId = _settings.PartnerId,
            PartnerSecret = _settings.PartnerSecret,
            ReferralId = userId,
        };

        var content = SerializationHelper.GetRequestContent(body);

        var response = await client.PostAsync(uri, content, token);

        if (!response.IsSuccessStatusCode)
        {
            await HandleErrors(response);
        }

        var deserializedResponse = await SerializationHelper.GetResponseContent<GetAprilAccessTokenResponse>(response);
        if (deserializedResponse.Status?.Code != "OK")
        {
            await HandleErrors(response, deserializedResponse.Status?.Message);
        }

        return deserializedResponse.Token;
    }

    public async Task<string> GetLoginUrl(string accessToken, CancellationToken token)
    {
        var uri = "/v1/authentication/generateUserPartnerAuthCode";

        var client = CreateAprilClient(accessToken);
        var body = new GetLoginHashRequest
        {
            ApplicationSubtype = _settings.ApplicationSubtype,
        };
        var content = SerializationHelper.GetRequestContent(body);

        var response = await client.PostAsync(uri, content, token);
        if (!response.IsSuccessStatusCode)
        {
            await HandleErrors(response);
        }

        var deserializedResponse = await SerializationHelper.GetResponseContent<GetLoginHashResponse>(response);

        return deserializedResponse.Hash;
    }

    private HttpClient CreateAprilClient(string? accessToken = null)
    {
        var client = _clientFactory.CreateClient(HttpClientName.April);
        if (accessToken != null)
        {
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", accessToken);
        }

        return client;
    }

    private async Task HandleErrors(HttpResponseMessage response, string? message = null)
    {
        var result = message ?? await response.Content.ReadAsStringAsync();
        if (response.StatusCode == HttpStatusCode.Unauthorized)
        {
            throw new UnauthorizedAccessException(result);
        }

        if (response.StatusCode == HttpStatusCode.BadRequest)
        {
            throw new FormatException(result);
        }

        if (response.StatusCode == HttpStatusCode.Conflict)
        {
            return;
        }
        throw new Exception(result);
    }

}
