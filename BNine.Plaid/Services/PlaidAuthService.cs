﻿namespace BNine.Plaid.Services;

using Application.Aggregates.Plaid.Models;
using Application.Interfaces.Plaid;
using Common;
using Constants;
using Microsoft.Extensions.Options;
using Models.Requests;
using Models.Responses;
using Newtonsoft.Json;
using Settings;

public class PlaidAuthService : IPlaidAuthService
{
    protected readonly IHttpClientFactory ClientFactory;
    protected readonly PlaidSettings Settings;

    public PlaidAuthService(IOptions<PlaidSettings> options, IHttpClientFactory clientFactory)
    {
        Settings = options.Value;
        ClientFactory = clientFactory;
    }

    public async Task<string> GetLinkToken(string userId, string androidPackageName = null)
    {
        var client = ClientFactory.CreateClient(HttpClientName.Plaid);

        var uri = "/link/token/create";

        var content = SerializationHelper.GetRequestContent(new LinkTokenRequestModel
        {
            СlientId = Settings.ClientId,
            ClientSecret = Settings.ClientSecret,
            ClientName = Settings.ClientName,
            User = new PlaidUser { ClientUserId = userId },
            Products = new List<string> { "auth", "assets", "transactions" },
            CountryCodes = new List<string> { "US" },
            Language = "en",
            Webhook = Settings.Webhook,
            AndroidPackageName = androidPackageName
        });

        var request = new HttpRequestMessage(HttpMethod.Post, uri) { Content = content };

        var response = await client.SendAsync(request);

        var stringResponse = await response.Content.ReadAsStringAsync();

        response.EnsureSuccessStatusCode();

        var result = JsonConvert.DeserializeObject<LinkTokenResponseModel>(stringResponse);

        return result.LinkToken;
    }

    public async Task<AccessTokenResponseModel> GetAccessToken(string publicToken)
    {
        var client = ClientFactory.CreateClient(HttpClientName.Plaid);

        var uri = "/item/public_token/exchange";

        var content = SerializationHelper.GetRequestContent(new AccessTokenRequestModel
        {
            СlientId = Settings.ClientId, ClientSecret = Settings.ClientSecret, PublicToken = publicToken
        });

        var request = new HttpRequestMessage(HttpMethod.Post, uri) { Content = content };

        var response = await client.SendAsync(request);

        var stringResponse = await response.Content.ReadAsStringAsync();

        response.EnsureSuccessStatusCode();

        var result = JsonConvert.DeserializeObject<AccessTokenResponseModel>(stringResponse);

        return result;
    }

    public async Task<AccountDataResponseModel> GetAccountData(string accessToken)
    {
        var client = ClientFactory.CreateClient(HttpClientName.Plaid);

        var uri = "/auth/get";

        var content = SerializationHelper.GetRequestContent(new AccountDataRequestModel
        {
            СlientId = Settings.ClientId,
            ClientSecret = Settings.ClientSecret,
            AccessToken = accessToken,
        });

        var request = new HttpRequestMessage(HttpMethod.Post, uri) { Content = content };

        var response = await client.SendAsync(request);

        var stringResponse = await response.Content.ReadAsStringAsync();

        response.EnsureSuccessStatusCode();

        var result = JsonConvert.DeserializeObject<AccountDataResponseModel>(stringResponse);

        return result;
    }

    public async Task DisposeAccessToken(string accessToken)
    {
        var client = ClientFactory.CreateClient(HttpClientName.Plaid);

        var uri = "/item/remove";

        var content = SerializationHelper.GetRequestContent(new DisposeAccessTokenRequestModel
        {
            СlientId = Settings.ClientId,
            ClientSecret = Settings.ClientSecret,
            AccessToken = accessToken
        });

        var request = new HttpRequestMessage(HttpMethod.Post, uri) { Content = content };

        var response = await client.SendAsync(request);

        response.EnsureSuccessStatusCode();
    }
}
