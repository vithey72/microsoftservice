﻿namespace BNine.Plaid.Services
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Net.Http;
    using System.Threading.Tasks;
    using Application.Aggregates.Plaid.Models.Transactions;
    using Application.Interfaces.Plaid;
    using Common;
    using Constants;
    using Microsoft.Extensions.Options;
    using Models.Transactions.Requests;
    using Models.Transactions.Response.GetTransactions;
    using Settings;

    public class PlaidTransactionService : IPlaidTransactionService
    {
        private PlaidSettings Settings
        {
            get;
        }

        private IHttpClientFactory ClientFactory
        {
            get;
        }

        public PlaidTransactionService(IOptions<PlaidSettings> options, IHttpClientFactory clientFactory)
        {
            Settings = options.Value;
            ClientFactory = clientFactory;
        }

        public async Task<string> GetRawTransactionsData(string accessToken, IEnumerable<string> accountIds)
        {
            var client = ClientFactory.CreateClient(HttpClientName.Plaid);

            var uri = "/transactions/get";

            var content = SerializationHelper.GetRequestContent(new TransactionsRequestModel
            {
                ClientId = Settings.ClientId,
                ClientSecret = Settings.ClientSecret,
                AccessToken = accessToken,
                StartDate = DateTime.UtcNow.AddMonths(-1).ToString("yyyy-MM-dd"),
                EndDate = DateTime.UtcNow.ToString("yyyy-MM-dd"),
                Options = new TransactionOptions { AccountIds = accountIds }
            });

            var request = new HttpRequestMessage(HttpMethod.Post, uri) { Content = content };

            var response = await client.SendAsync(request);

            var stringResponse = await response.Content.ReadAsStringAsync();

            response.EnsureSuccessStatusCode();

            return stringResponse;
        }

        private TransactionsDataInfo GetTransactionsData(TransactionsData data)
        {
            return new TransactionsDataInfo
            {
                RequestId = data.RequestId,
                TotalTransactions = data.TotalTransactions,
                Item =
                    data.Item == null
                        ? null
                        : new ItemInfo
                        {
                            Webhook = data.Item.Webhook,
                            InstitutionId = data.Item.InstitutionId,
                            ItemId = data.Item.ItemId,
                            UpdateType = data.Item.UpdateType,
                            ConsentExpirationTime = data.Item.ConsentExpirationTime,
                            Error = data.Item.Error == null
                                ? null
                                : new ErrorInfo
                                {
                                    Status = data.Item.Error.Status,
                                    DisplayMessage = data.Item.Error.DisplayMessage,
                                    DocumentationUrl = data.Item.Error.DocumentationUrl,
                                    ErrorCode = data.Item.Error.ErrorCode,
                                    ErrorMessage = data.Item.Error.ErrorMessage,
                                    ErrorType = data.Item.Error.ErrorType,
                                    RequestId = data.Item.Error.RequestId,
                                    SuggestedAction = data.Item.Error.SuggestedAction
                                },
                            AvailableProducts = data.Item.AvailableProducts,
                            BilledProducts = data.Item.BilledProducts
                        },
                Accounts =
                    data.Accounts?.Select(x => new AccountInfo
                    {
                        Mask = x.Mask,
                        Name = x.Name,
                        Subtype = x.Subtype,
                        Type = x.Type,
                        AccountId = x.AccountId,
                        OfficialName = x.OfficialName,
                        VerificationStatus = x.VerificationStatus,
                        Balances = x.Balances == null
                            ? null
                            : new BalanceInfo
                            {
                                Available = x.Balances.Available,
                                Current = x.Balances.Current,
                                Limit = x.Balances.Limit,
                                IsoCurrencyCode = x.Balances.IsoCurrencyCode,
                                UnofficialCurrencyCode = x.Balances.UnofficialCurrencyCode,
                                LastUpdatedDateTime = x.Balances.LastUpdatedDateTime
                            }
                    }),
                Transactions = data.Transactions?.Select(x => new TransactionInfo
                {
                    Amount = x.Amount,
                    Date = x.Date,
                    Name = x.Name,
                    Pending = x.Pending,
                    AccountId = x.AccountId,
                    AccountOwner = x.AccountOwner,
                    AuthorizedDate = x.AuthorizedDate,
                    CategoryId = x.CategoryId,
                    DateTime = x.DateTime,
                    MerchantName = x.MerchantName,
                    PaymentChannel = x.PaymentChannel,
                    TransactionCode = x.TransactionCode,
                    TransactionId = x.TransactionId,
                    AuthorizedDateTime = x.AuthorizedDateTime,
                    IsoCurrencyCode = x.IsoCurrencyCode,
                    PendingTransactionId = x.PendingTransactionId,
                    Categories = x.Categories,
                    UnofficialCurrencyCode = x.UnofficialCurrencyCode,
                    Location =
                        x.Location == null
                            ? null
                            : new LocationInfo
                            {
                                Address = x.Location.Address,
                                City = x.Location.City,
                                Country = x.Location.Country,
                                Latitude = x.Location.Latitude,
                                Longitude = x.Location.Longitude,
                                Region = x.Location.Region,
                                PostalCode = x.Location.PostalCode,
                                StoreNumber = x.Location.StoreNumber
                            },
                    PaymentMeta = x.PaymentMeta == null
                        ? null
                        : new PaymentMetaInfo
                        {
                            Payee = x.PaymentMeta.Payee,
                            Payer = x.PaymentMeta.Payer,
                            Reason = x.PaymentMeta.Reason,
                            PaymentMethod = x.PaymentMeta.PaymentMethod,
                            PaymentProcessor = x.PaymentMeta.PaymentProcessor,
                            PpdId = x.PaymentMeta.PpdId,
                            ReferenceNumber = x.PaymentMeta.Reason,
                            ByOrderOf = x.PaymentMeta.ByOrderOf
                        }
                })
            };
        }
    }
}
