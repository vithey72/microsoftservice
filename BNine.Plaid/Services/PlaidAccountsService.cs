﻿namespace BNine.Plaid.Services
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Net.Http;
    using System.Threading.Tasks;
    using BNine.Application.Interfaces.Plaid;
    using BNine.Application.Models.Plaid;
    using BNine.Common;
    using BNine.Constants;
    using BNine.Plaid.Models.Accounts.Requests;
    using BNine.Plaid.Models.Accounts.Responses;
    using BNine.Settings;
    using Microsoft.Extensions.Logging;
    using Microsoft.Extensions.Options;
    using Newtonsoft.Json;

    public class PlaidAccountsService : IPlaidAccountsService
    {
        private PlaidSettings Settings
        {
            get;
        }


        private IHttpClientFactory ClientFactory
        {
            get;
        }

        private ILogger<PlaidAccountsService> Logger
        {
            get;
        }

        public PlaidAccountsService(ILogger<PlaidAccountsService> logger, IOptions<PlaidSettings> options, IHttpClientFactory clientFactory)
        {
            Settings = options.Value;
            ClientFactory = clientFactory;
            Logger = logger;
        }

        public async Task<BalanceDto> GetBalance(string accessToken, string accountId)
        {
            var client = ClientFactory.CreateClient(HttpClientName.Plaid);

            var uri = "/accounts/balance/get";

            var content = SerializationHelper.GetRequestContent(new GetBalanceModel
            {
                ClientId = Settings.ClientId,
                ClientSecret = Settings.ClientSecret,
                AccessToken = accessToken,
                Options = new GetBalanceOptions { AccountIds = new List<string> { accountId } }
            });

            var request = new HttpRequestMessage(HttpMethod.Post, uri) { Content = content };

            var response = await client.SendAsync(request);

            if (!response.IsSuccessStatusCode)
            {
                throw new System.Exception(response.ReasonPhrase);
            }

            var stringResponse = await response.Content.ReadAsStringAsync();

            var data = JsonConvert.DeserializeObject<GetBalanceResponse>(stringResponse);

            var balances = data.Accounts.FirstOrDefault()?.Balances;

            if (balances == null)
            {
                return null;
            }

            return new BalanceDto
            {
                Currency = balances.IsoCurrencyCode,
                Available = balances.Available,
                Current = balances.Current
            };
        }
    }
}
