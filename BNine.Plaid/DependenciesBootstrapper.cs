﻿namespace BNine.Plaid
{
    using System;
    using Application.Interfaces.Plaid;
    using BNine.Constants;
    using Microsoft.Extensions.Configuration;
    using Microsoft.Extensions.DependencyInjection;
    using Services;
    using Polly;

    public static class DependenciesBootstrapper
    {
        public static IServiceCollection AddPlaid(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddTransient<IPlaidAuthService, PlaidAuthService>();
            services.AddTransient<IPlaidTransactionService, PlaidTransactionService>();
            services.AddTransient<IPlaidAccountsService, PlaidAccountsService>();
            services.AddHttpClient(HttpClientName.Plaid, client =>
            {
                client.BaseAddress = new Uri(configuration["PlaidSettings:ApiUrl"]);
            })
            .AddTransientHttpErrorPolicy(p => p.WaitAndRetryAsync(3, (x) => TimeSpan.FromSeconds(x)));

            return services;
        }
    }
}
