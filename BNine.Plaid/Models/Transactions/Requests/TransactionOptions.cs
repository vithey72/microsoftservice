﻿namespace BNine.Plaid.Models.Transactions.Requests
{
    using System.Collections.Generic;
    using Newtonsoft.Json;

    public class TransactionOptions
    {
        [JsonProperty("account_ids")]
        public IEnumerable<string> AccountIds
        {
            get;
            set;
        }
    }
}
