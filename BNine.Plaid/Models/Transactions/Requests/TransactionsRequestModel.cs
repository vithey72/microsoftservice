﻿namespace BNine.Plaid.Models.Transactions.Requests
{
    using Newtonsoft.Json;

    public class TransactionsRequestModel
    {
        [JsonProperty("client_id")]
        public string ClientId
        {
            get;
            set;
        }

        [JsonProperty("access_token")]
        public string AccessToken
        {
            get;
            set;
        }

        [JsonProperty("secret")]
        public string ClientSecret
        {
            get;
            set;
        }

        [JsonProperty("start_date")]
        public string StartDate
        {
            get;
            set;
        }

        [JsonProperty("end_date")]
        public string EndDate
        {
            get;
            set;
        }

        [JsonProperty("options")]
        public TransactionOptions Options
        {
            get;
            set;
        }
    }
}
