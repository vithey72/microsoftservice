﻿namespace BNine.Plaid.Models.Transactions.Response.GetTransactions
{
    using Newtonsoft.Json;

    public class Balance
    {
        [JsonProperty("available")]
        public decimal? Available
        {
            get;
            set;
        }

        [JsonProperty("current")]
        public decimal Current
        {
            get;
            set;
        }

        [JsonProperty("iso_currency_code")]
        public string IsoCurrencyCode
        {
            get;
            set;
        }

        [JsonProperty("limit")]
        public decimal? Limit
        {
            get;
            set;
        }

        [JsonProperty("unofficial_currency_code")]
        public string UnofficialCurrencyCode
        {
            get;
            set;
        }

        [JsonProperty("last_updated_datetime")]
        public string LastUpdatedDateTime
        {
            get;
            set;
        }
    }
}
