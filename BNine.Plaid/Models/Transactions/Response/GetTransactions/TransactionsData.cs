﻿namespace BNine.Plaid.Models.Transactions.Response.GetTransactions
{
    using System.Collections.Generic;
    using Newtonsoft.Json;

    public class TransactionsData
    {
        [JsonProperty("transactions")]
        public IEnumerable<Transaction> Transactions
        {
            get;
            set;
        }

        [JsonProperty("accounts")]
        public IEnumerable<Account> Accounts
        {
            get;
            set;
        }

        [JsonProperty("total_transactions")]
        public int TotalTransactions
        {
            get;
            set;
        }

        [JsonProperty("item")]
        public Item Item
        {
            get;
            set;
        }

        [JsonProperty("request_id")]
        public string RequestId
        {
            get;
            set;
        }
    }
}
