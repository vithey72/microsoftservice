﻿namespace BNine.Plaid.Models.Transactions.Response.GetTransactions
{
    using Newtonsoft.Json;

    public class Location
    {
        [JsonProperty("address")]
        public string Address
        {
            get;
            set;
        }

        [JsonProperty("city")]
        public string City
        {
            get;
            set;
        }

        [JsonProperty("region")]
        public string Region
        {
            get;
            set;
        }

        [JsonProperty("postal_code")]
        public string PostalCode
        {
            get;
            set;
        }

        [JsonProperty("country")]
        public string Country
        {
            get;
            set;
        }

        [JsonProperty("store_number")]
        public string StoreNumber
        {
            get;
            set;
        }

        [JsonProperty("lat")]
        public float? Latitude
        {
            get;
            set;
        }

        [JsonProperty("lon")]
        public float? Longitude
        {
            get;
            set;
        }
    }
}
