﻿namespace BNine.Plaid.Models.Transactions.Response.GetTransactions
{
    using Newtonsoft.Json;

    public class Account
    {
        [JsonProperty("account_id")]
        public string AccountId
        {
            get;
            set;
        }

        [JsonProperty("mask")]
        public string Mask
        {
            get;
            set;
        }

        [JsonProperty("name")]
        public string Name
        {
            get;
            set;
        }

        [JsonProperty("official_name")]
        public string OfficialName
        {
            get;
            set;
        }

        [JsonProperty("subtype")]
        public string Subtype
        {
            get;
            set;
        }

        [JsonProperty("type")]
        public string Type
        {
            get;
            set;
        }

        [JsonProperty("verification_status")]
        public string VerificationStatus
        {
            get;
            set;
        }

        [JsonProperty("balances")]
        public Balance Balances
        {
            get;
            set;
        }
    }
}
