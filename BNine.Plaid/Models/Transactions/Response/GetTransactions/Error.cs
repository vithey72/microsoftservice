﻿namespace BNine.Plaid.Models.Transactions.Response.GetTransactions
{
    using Newtonsoft.Json;

    public class Error
    {
        [JsonProperty("error_type")]
        public string ErrorType
        {
            get;
            set;
        }

        [JsonProperty("error_code")]
        public string ErrorCode
        {
            get;
            set;
        }

        [JsonProperty("error_message")]
        public string ErrorMessage
        {
            get;
            set;
        }

        [JsonProperty("display_message")]
        public string DisplayMessage
        {
            get;
            set;
        }

        [JsonProperty("request_id")]
        public string RequestId
        {
            get;
            set;
        }

        [JsonProperty("status")]
        public int? Status
        {
            get;
            set;
        }

        [JsonProperty("documentation_url")]
        public string DocumentationUrl
        {
            get;
            set;
        }

        [JsonProperty("suggested_action")]
        public string SuggestedAction
        {
            get;
            set;
        }
    }
}
