﻿namespace BNine.Plaid.Models.Transactions.Response.GetTransactions
{
    using Newtonsoft.Json;

    public class PaymentMeta
    {
        [JsonProperty("reference_number")]
        public string ReferenceNumber
        {
            get;
            set;
        }

        [JsonProperty("ppd_id")]
        public string PpdId
        {
            get;
            set;
        }

        [JsonProperty("payee")]
        public string Payee
        {
            get;
            set;
        }

        [JsonProperty("by_order_of")]
        public string ByOrderOf
        {
            get;
            set;
        }

        [JsonProperty("payer")]
        public string Payer
        {
            get;
            set;
        }

        [JsonProperty("payment_method")]
        public string PaymentMethod
        {
            get;
            set;
        }

        [JsonProperty("payment_processor")]
        public string PaymentProcessor
        {
            get;
            set;
        }

        [JsonProperty("reason")]
        public string Reason
        {
            get;
            set;
        }
    }
}
