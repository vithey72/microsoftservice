﻿namespace BNine.Plaid.Models.Transactions.Response.GetTransactions
{
    using System.Collections.Generic;
    using Newtonsoft.Json;

    public class Item
    {
        [JsonProperty("item_id")]
        public string ItemId
        {
            get;
            set;
        }

        [JsonProperty("institution_id")]
        public string InstitutionId
        {
            get;
            set;
        }

        [JsonProperty("webhook")]
        public string Webhook
        {
            get;
            set;
        }

        [JsonProperty("error")]
        public Error Error
        {
            get;
            set;
        }

        [JsonProperty("available_products")]
        public IEnumerable<string> AvailableProducts
        {
            get;
            set;
        }

        [JsonProperty("billed_products")]
        public IEnumerable<string> BilledProducts
        {
            get;
            set;
        }

        [JsonProperty("consent_expiration_time")]
        public string ConsentExpirationTime
        {
            get;
            set;
        }

        [JsonProperty("update_type")]
        public string UpdateType
        {
            get;
            set;
        }
    }
}
