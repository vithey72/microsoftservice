﻿namespace BNine.Plaid.Models.Transactions.Response.GetTransactions
{
    using System.Collections.Generic;
    using Newtonsoft.Json;

    public class Transaction
    {
        [JsonProperty("account_id")]
        public string AccountId
        {
            get;
            set;
        }

        [JsonProperty("amount")]
        public decimal Amount
        {
            get;
            set;
        }

        [JsonProperty("iso_currency_code")]
        public string IsoCurrencyCode
        {
            get;
            set;
        }

        [JsonProperty("unofficial_currency_code")]
        public string UnofficialCurrencyCode
        {
            get;
            set;
        }

        [JsonProperty("category")]
        public IEnumerable<string> Categories
        {
            get;
            set;
        }

        [JsonProperty("category_id")]
        public string CategoryId
        {
            get;
            set;
        }

        [JsonProperty("date")]
        public string Date
        {
            get;
            set;
        }

        [JsonProperty("datetime")]
        public string DateTime
        {
            get;
            set;
        }

        [JsonProperty("authorized_date")]
        public string AuthorizedDate
        {
            get;
            set;
        }

        [JsonProperty("authorized_datetime")]
        public string AuthorizedDateTime
        {
            get;
            set;
        }

        [JsonProperty("location")]
        public Location Location
        {
            get;
            set;
        }

        [JsonProperty("name")]
        public string Name
        {
            get;
            set;
        }

        [JsonProperty("merchant_name")]
        public string MerchantName
        {
            get;
            set;
        }

        [JsonProperty("payment_meta")]
        public PaymentMeta PaymentMeta
        {
            get;
            set;
        }

        [JsonProperty("payment_channel")]
        public string PaymentChannel
        {
            get;
            set;
        }

        [JsonProperty("pending")]
        public bool Pending
        {
            get;
            set;
        }

        [JsonProperty("pending_transaction_id")]
        public string PendingTransactionId
        {
            get;
            set;
        }

        [JsonProperty("account_owner")]
        public string AccountOwner
        {
            get;
            set;
        }

        [JsonProperty("transaction_id")]
        public string TransactionId
        {
            get;
            set;
        }

        [JsonProperty("transaction_code")]
        public string TransactionCode
        {
            get;
            set;
        }
    }
}
