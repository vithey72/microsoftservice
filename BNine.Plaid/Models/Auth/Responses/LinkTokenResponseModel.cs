﻿namespace BNine.Plaid.Models.Responses
{
    using System;
    using Newtonsoft.Json;

    internal class LinkTokenResponseModel
    {
        [JsonProperty("expiration")]
        internal DateTime Expiration
        {
            get; set;
        }

        [JsonProperty("request_id")]
        internal string RequestId
        {
            get; set;
        }

        [JsonProperty("link_token")]
        internal string LinkToken
        {
            get; set;
        }
    }
}
