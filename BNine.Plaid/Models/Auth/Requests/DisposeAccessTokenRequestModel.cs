﻿namespace BNine.Plaid.Models.Requests
{
    using Newtonsoft.Json;

    public class DisposeAccessTokenRequestModel
    {
        [JsonProperty("client_id")]
        public string СlientId
        {
            get; set;
        }

        [JsonProperty("secret")]
        public string ClientSecret
        {
            get; set;
        }

        [JsonProperty("access_token")]
        public string AccessToken
        {
            get; set;
        }
    }
}
