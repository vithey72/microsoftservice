﻿namespace BNine.Plaid.Models.Requests
{
    using System.Collections.Generic;
    using Newtonsoft.Json;

    internal class LinkTokenRequestModel
    {
        [JsonProperty("client_id")]
        public string СlientId
        {
            get; set;
        }

        [JsonProperty("secret")]
        public string ClientSecret
        {
            get; set;
        }

        [JsonProperty("client_name")]
        public string ClientName
        {
            get; set;
        }

        [JsonProperty("user")]
        public PlaidUser User
        {
            get; set;
        }

        [JsonProperty("products")]
        public List<string> Products
        {
            get; set;
        }

        [JsonProperty("country_codes")]
        public List<string> CountryCodes
        {
            get; set;
        }

        [JsonProperty("language")]
        public string Language
        {
            get; set;
        }

        [JsonProperty("webhook")]
        public string Webhook
        {
            get; set;
        }

        [JsonProperty("android_package_name")]
        public string AndroidPackageName
        {
            get; set;
        }
    }

    internal class PlaidUser
    {
        [JsonProperty("client_user_id")]
        public string ClientUserId
        {
            get; set;
        }
    }
}
