﻿namespace BNine.Plaid.Models.Requests
{
    using Newtonsoft.Json;

    internal class AccessTokenRequestModel
    {
        [JsonProperty("client_id")]
        public string СlientId
        {
            get; set;
        }

        [JsonProperty("secret")]
        public string ClientSecret
        {
            get; set;
        }

        [JsonProperty("public_token")]
        public string PublicToken
        {
            get; set;
        }
    }
}
