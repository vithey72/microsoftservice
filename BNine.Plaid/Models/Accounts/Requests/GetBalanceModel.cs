﻿namespace BNine.Plaid.Models.Accounts.Requests
{
    using System.Collections.Generic;
    using Newtonsoft.Json;

    public class GetBalanceModel
    {
        [JsonProperty("client_id")]
        public string ClientId
        {
            get;
            set;
        }

        [JsonProperty("access_token")]
        public string AccessToken
        {
            get;
            set;
        }

        [JsonProperty("secret")]
        public string ClientSecret
        {
            get;
            set;
        }

        [JsonProperty("options")]
        public GetBalanceOptions Options
        {
            get;
            set;
        }
    }

    public class GetBalanceOptions
    {
        [JsonProperty("account_ids")]
        public IEnumerable<string> AccountIds
        {
            get; set;
        }
    }
}
