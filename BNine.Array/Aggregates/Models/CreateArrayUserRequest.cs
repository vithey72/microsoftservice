﻿namespace BNine.Array.Aggregates.Models
{
    using Newtonsoft.Json;

    public record CreateArrayUserRequest
    {
        [JsonProperty("appKey")]
        public Guid AppKey
        {
            get;
            init;
        }

        [JsonProperty("firstName")]
        public string FirstName
        {
            get;
            init;
        }

        [JsonProperty("lastName")]
        public string LastName
        {
            get;
            init;
        }

        [JsonProperty("ssn")]
        public string Ssn
        {
            get;
            init;
        }

        [JsonProperty("dob")]
        public DateTime DateOfBirth
        {
            get;
            init;
        }

        /// <summary>
        /// The customer's email address.
        /// The address is used as the customer's public username within Array.
        /// </summary>
        [JsonProperty("emailAddress")]
        public string Email
        {
            get;
            init;
        }

        /// <summary>
        /// The customer's phone number (including area code), digits only. For example: "2125550123".
        /// This is the phone number to which an SMS message may be sent during the identity verification process.
        /// </summary>
        [JsonProperty("phoneNumber")]
        public string PhoneNumber
        {
            get;
            init;
        }

        [JsonProperty("address")]
        public UserAddress Address
        {
            get;
            init;
        }

        public class UserAddress
        {
            [JsonProperty("state")]
            public string State
            {
                get;
                init;
            }

            [JsonProperty("street")]
            public string Street
            {
                get;
                init;
            }

            [JsonProperty("city")]
            public string City
            {
                get;
                init;
            }

            [JsonProperty("zip")]
            public string Zip
            {
                get;
                init;
            }
        }
    }
}
