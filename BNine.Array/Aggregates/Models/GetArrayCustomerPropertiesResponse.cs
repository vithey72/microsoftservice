﻿namespace BNine.Array.Aggregates.Models
{
    using Newtonsoft.Json;

    public record GetArrayCustomerPropertiesResponse
    {
        /// <summary>
        /// Unique identifier of user in Array. Is also used as a server-side password.
        /// </summary>
        [JsonProperty("userId")]
        public Guid UserId
        {
            get;
            init;
        }

        /// <summary>
        /// Per-session user token with a TTL of 3  minutes. Is required for all the queries on front, except registration steps.
        /// Null if user has not yet passed KBA-authentication. 
        /// This token is queried every time GET-method associated with this response is called.
        /// </summary>
        [JsonProperty("userToken")]
        public Guid? UserToken
        {
            get;
            init;
        }

        /// <summary>
        /// Code of the credit bureau that user should be using.
        /// </summary>
        [JsonProperty("provider")]
        public string Provider
        {
            get;
            init;
        }


        /// <summary>
        /// Unique key for user. Is used as a server-side password.
        /// </summary>
        [Obsolete("Duplicate of UserId. Should be removed once clients don't use it anymore.")]
        [JsonProperty("clientKey")]
        public Guid ClientKey
        {
            get;
            init;
        }

    }
}
