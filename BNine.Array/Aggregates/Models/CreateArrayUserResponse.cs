﻿namespace BNine.Array.Aggregates.Models
{
    using Newtonsoft.Json;

    public record CreateArrayUserResponse
    {
        [JsonProperty("clientKey")]
        public Guid ClientKey
        {
            get;
            init;
        }

        [JsonProperty("userId")]
        public Guid UserId
        {
            get;
            init;
        }

        [JsonProperty("authToken")]
        public Guid AuthToken
        {
            get;
            init;
        }
    }
}
