﻿namespace BNine.Array.Models
{
    using Newtonsoft.Json;

    internal class UserTokenResult<T>
    {
        [JsonProperty("userToken")]
        public T UserToken
        {
            get; set;
        }
    }
}
