﻿namespace BNine.Array.Models;

using Newtonsoft.Json;

internal class CreateTokenRequest
{
    [JsonProperty("appKey")]
    public Guid AppKey
    {
        get;
        set;
    }

    [JsonProperty("clientKey")]
    public Guid ClientId
    {
        get;
        set;
    }

    [JsonProperty("ttlInMinutes")]
    public int TtlInMinutes
    {
        get;
        set;
    } = 15;
}
