﻿namespace BNine.Array.Models
{
    using Newtonsoft.Json;

    internal class ErrorModel
    {
        [JsonProperty("error")]
        internal string Error
        {
            get;
            set;
        }
    }
}
