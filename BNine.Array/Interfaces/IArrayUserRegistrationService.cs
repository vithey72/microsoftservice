﻿namespace BNine.Array.Interfaces
{
    using Aggregates.Models;

    public interface IArrayUserRegistrationService
    {
        /// <summary>
        /// Enrolls user to Array.
        /// </summary>
        Task<CreateArrayUserResponse> CreateUser(CreateArrayUserRequest request, CancellationToken token);

        /// <summary>
        /// Generates a user token to access array on front, lasts about 15 minutes.
        /// </summary>
        Task<Guid?> CreateUserToken(Guid arrayUserId, CancellationToken cancellationToken);
    }
}
