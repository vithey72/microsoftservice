﻿namespace BNine.Array.Services
{
    using Common;
    using Constants;
    using Microsoft.Extensions.Options;
    using Newtonsoft.Json;
    using Settings;

    public class BaseArrayService
    {
        protected readonly ArraySettings Settings;
        private readonly IHttpClientFactory _clientFactory;

        protected BaseArrayService(IOptions<ArraySettings> options, IHttpClientFactory clientFactory)
        {
            Settings = options.Value;
            _clientFactory = clientFactory;
        }

        protected HttpClient CreateNonAuthClient()
        {
            var client = _clientFactory.CreateClient(HttpClientName.Array);
            return client;
        }

        protected StringContent CreateRequestContent(object body)
        {
            var dateFormatSettings = new JsonSerializerSettings { DateFormatString = "yyyy-MM-dd" };
            var content = SerializationHelper.GetRequestContent(body, dateFormatSettings);
            return content;
        }
    }
}
