﻿namespace BNine.Array.Services
{
    using System;
    using System.Net;
    using System.Threading.Tasks;
    using Aggregates.Models;
    using Common;
    using Interfaces;
    using Microsoft.Extensions.Options;
    using Models;
    using Settings;

    public class ArrayUserAuthorizationService : BaseArrayService, IArrayUserRegistrationService
    {
        private readonly ArraySettings _options;

        public ArrayUserAuthorizationService(
            IOptions<ArraySettings> options,
            IHttpClientFactory clientFactory)
            : base(options, clientFactory)
        {
            _options = options.Value;
        }

        /// <summary>
        /// Enroll user into Array. 
        /// </summary>
        public async Task<CreateArrayUserResponse> CreateUser(CreateArrayUserRequest request, CancellationToken token)
        {
            var client = base.CreateNonAuthClient();
            var content = base.CreateRequestContent(request);
            var result = await client.PostAsync("/api/user/v2", content, token);
            if (!result.IsSuccessStatusCode)
            {
                var errorModel = await GetErrorModel(result);
                throw await HandleErrors(result, errorModel);
            }

            return await SerializationHelper.GetResponseContent<CreateArrayUserResponse>(result);
        }

        /// <summary>
        /// Get a fresh <see cref="GetArrayCustomerPropertiesResponse.UserToken"/>.
        /// Note that in production environment our API server IP address should be whitelisted by Array in order for
        /// "x-credmo-client-token" to work.
        /// </summary>
        public async Task<Guid?> CreateUserToken(Guid arrayUserId, CancellationToken cancellationToken)
        {
            var client = base.CreateNonAuthClient();
            var request = new CreateTokenRequest
            {
                AppKey = _options.AppKey,
                ClientId = arrayUserId,
                TtlInMinutes = 15,
            };
            var content = base.CreateRequestContent(request);
            content.Headers.Add("x-credmo-client-token", _options.ApiKey.ToString());

            var response = await client.PostAsync("api/authenticate/v2/usertoken", content, cancellationToken);
            if (!response.IsSuccessStatusCode)
            {
                var errorModel = await GetErrorModel(response);
                if (response.StatusCode == HttpStatusCode.Unauthorized &&
                    errorModel?.Error == "User has not passed authentication")
                {
                    return null;
                }
                throw await HandleErrors(response, errorModel);
            }

            var responseBody = await SerializationHelper.GetResponseContent<UserTokenResult<Guid>>(response);

            return responseBody.UserToken;
        }

        private async Task<Exception> HandleErrors(HttpResponseMessage response, ErrorModel errorModel = null)
        {
            var errorText = errorModel?.Error ?? await response.Content.ReadAsStringAsync();
            throw response.StatusCode switch
            {
                HttpStatusCode.Unauthorized => new UnauthorizedAccessException(errorText),
                HttpStatusCode.Forbidden => new ArgumentException(errorText, nameof(AlltrustSettings.ApiKey)),
                HttpStatusCode.BadRequest => new FormatException(errorText),
                HttpStatusCode.NotFound => new ArgumentException(errorText, nameof(AlltrustSettings.ApiUrl)),
                _ => new Exception(errorText)
            };
        }

        private static async Task<ErrorModel> GetErrorModel(HttpResponseMessage response) =>
            await SerializationHelper.GetResponseContent<ErrorModel>(response);
    }
}
