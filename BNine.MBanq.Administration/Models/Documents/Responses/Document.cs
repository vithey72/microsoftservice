﻿namespace BNine.MBanq.Administration.Models.Documents.Responses
{
    using Newtonsoft.Json;

    internal class Document
    {
        [JsonProperty("id")]
        internal string Id
        {
            get; set;
        }

        [JsonProperty("fileName")]
        internal string FileName
        {
            get; set;
        }

        [JsonProperty("type")]
        internal string Type
        {
            get; set;
        }
    }
}
