﻿namespace BNine.MBanq.Administration.Models.Documents.Responses
{
    using Newtonsoft.Json;

    public class UploadDocumentResponse
    {
        [JsonProperty("id")]
        internal string Id
        {
            get; set;
        }

        [JsonProperty("resourceIdentifier")]
        internal string ResourceIdentifier
        {
            get; set;
        }
    }
}
