﻿namespace BNine.MBanq.Administration.Models.Identities.Requests
{
    using Newtonsoft.Json;

    internal class UpdateIdentityRequest
    {
        [JsonProperty("documentKey")]
        internal string DocumentKey
        {
            get; set;
        }

        [JsonProperty("documentTypeId")]
        internal int DocumentTypeId
        {
            get; set;
        }

        [JsonProperty("issuedBy")]
        internal string IssuedBy
        {
            get; set;
        }
    }
}
