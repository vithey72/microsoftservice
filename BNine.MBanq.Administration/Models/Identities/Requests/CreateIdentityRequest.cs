﻿namespace BNine.MBanq.Administration.Models.Identities.Requests
{
    using Newtonsoft.Json;

    internal class CreateIdentityRequest
    {
        [JsonProperty("documentKey")]
        internal string DocumentKey
        {
            get; set;
        }

        [JsonProperty("documentTypeId")]
        internal int DocumentTypeId
        {
            get; set;
        }

        [JsonProperty("status")]
        internal string Status
        {
            get; set;
        } = "Active";

        [JsonProperty("issuedBy")]
        internal string IssuedBy
        {
            get; set;
        }
    }
}
