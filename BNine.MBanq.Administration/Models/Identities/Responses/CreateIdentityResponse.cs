﻿namespace BNine.MBanq.Administration.Models.Identities.Responses
{
    using Newtonsoft.Json;

    internal class CreateIdentityResponse
    {
        [JsonProperty("officeId")]
        internal int OfficeId
        {
            get; set;
        }

        [JsonProperty("clientId")]
        internal int ClientId
        {
            get; set;
        }

        [JsonProperty("resourceId")]
        internal int ResourceId
        {
            get; set;
        }
    }
}
