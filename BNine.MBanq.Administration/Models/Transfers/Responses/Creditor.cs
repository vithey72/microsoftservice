﻿namespace BNine.MBanq.Administration.Models.Transfers.Responses
{
    using System.Collections.Generic;
    using Newtonsoft.Json;

    internal class Creditor
    {
        [JsonProperty("identifier")]
        public string Identifier
        {
            get;
            set;
        }

        [JsonProperty("name")]
        public string Name
        {
            get;
            set;
        }

        [JsonProperty("country")]
        public string Country
        {
            get;
            set;
        }

        [JsonProperty("postalCode")]
        public string PostalCode
        {
            get;
            set;
        }

        [JsonProperty("city")]
        public string City
        {
            get;
            set;
        }

        [JsonProperty("address")]
        public IEnumerable<string> Address
        {
            get;
            set;
        }

        [JsonProperty("agent")]
        public Agent Agent
        {
            get;
            set;
        }
    }
}
