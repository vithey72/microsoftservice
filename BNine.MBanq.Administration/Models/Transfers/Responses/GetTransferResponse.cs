﻿namespace BNine.MBanq.Administration.Models.Transfers.Responses
{
    using System;
    using System.Collections.Generic;
    using Newtonsoft.Json;

    internal class GetTransferResponse
    {
        [JsonProperty("id")]
        public int Id
        {
            get; set;
        }

        [JsonProperty("clientId")]
        public int? ClientId
        {
            get; set;
        }

        [JsonProperty("amount")]
        public decimal Amount
        {
            get; set;
        }

        [JsonProperty("currency")]
        public string Currency
        {
            get; set;
        }

        [JsonProperty("correlationId")]
        public Guid CorrelationId
        {
            get; set;
        }

        [JsonProperty("executedAt")]
        public DateTime ExecutedAt
        {
            get; set;
        }

        [JsonProperty("externalId")]
        public string ExternalId
        {
            get; set;
        }

        [JsonProperty("status")]
        public string Status
        {
            get; set;
        }

        [JsonProperty("transactionId")]
        public Guid TransactionId
        {
            get; set;
        }

        [JsonProperty("paymentType")]
        public string TransferType
        {
            get; set;
        }

        [JsonProperty("valueDate")]
        public DateTime ValueDate
        {
            get; set;
        }

        [JsonProperty("type")]
        public string Direction
        {
            get; set;
        }

        [JsonProperty("bookingDate")]
        public DateTime? BookingDate
        {
            get; set;
        }

        [JsonProperty("fileUrl")]
        public string FileUrl
        {
            get; set;
        }

        [JsonProperty("debtor")]
        public Debtor Debtor
        {
            get; set;
        }

        [JsonProperty("creditor")]
        public Creditor Creditor
        {
            get; set;
        }

        [JsonProperty("reference")]
        public IEnumerable<string> Reference { get; set; } = new List<string>();


        [JsonProperty("inOrOut")]
        public string InOrOut
        {
            get; set;
        }

        [JsonProperty("creditorAccountId")]
        public int? CreditorAccountId
        {
            get; set;
        }

        [JsonProperty("creditorSavingsAccountTransactionId")]
        public int? CreditorSavingsAccountTransactionId
        {
            get;
            set;
        }
    }
}
