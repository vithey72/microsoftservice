﻿namespace BNine.MBanq.Administration.Models.Transfers.Responses
{
    using System.Collections.Generic;
    using Newtonsoft.Json;

    internal class Debtor
    {
        [JsonProperty("identifier")]
        public string Identifier
        {
            get;
            set;
        }

        [JsonProperty("name")]
        public string Name
        {
            get;
            set;
        }

        [JsonProperty("country")]
        public string Country
        {
            get;
            set;
        }

        [JsonProperty("address")]
        public IEnumerable<string> Address
        {
            get;
            set;
        }
    }
}
