﻿namespace BNine.MBanq.Administration.Models.Transfers.Responses
{
    using Newtonsoft.Json;

    internal class Agent
    {
        [JsonProperty("identifier")]
        public string Identifier
        {
            get;
            set;
        }

        [JsonProperty("name")]
        public string Name
        {
            get;
            set;
        }
    }
}
