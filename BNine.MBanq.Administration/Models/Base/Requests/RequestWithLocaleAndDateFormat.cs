﻿namespace BNine.MBanq.Administration.Models.Base.Requests
{
    using Newtonsoft.Json;

    internal class RequestWithLocaleAndDateFormat
    {
        [JsonProperty("locale")]
        internal string Locale
        {
            get; set;
        }

        [JsonProperty("dateFormat")]
        internal string DateFormat
        {
            get; set;
        }
    }
}
