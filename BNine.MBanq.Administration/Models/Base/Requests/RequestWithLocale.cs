﻿namespace BNine.MBanq.Administration.Models.Base.Requests
{
    using Newtonsoft.Json;

    internal abstract class RequestWithLocale
    {
        [JsonProperty("locale")]
        internal string Locale
        {
            get; set;
        }
    }
}
