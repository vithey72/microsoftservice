﻿namespace BNine.MBanq.Administration.Models.Base.Responses
{
    using System.Collections.Generic;
    using Newtonsoft.Json;

    internal class ErrorResponse
    {
        [JsonProperty("httpStatusCode")]
        internal int HttpStatusCode
        {
            get; set;
        }

        [JsonProperty("errors")]
        internal IEnumerable<Error> Errors
        {
            get; set;
        } = new List<Error>();
    }

    internal class ErrorIdempotencyResponse : ErrorResponse
    {
        [JsonProperty("path")]
        internal string RequestPath
        {
            get; set;
        }

        [JsonProperty("status")]
        internal int Status
        {
            get; set;
        }

        [JsonProperty("message")]
        internal string Message
        {
            get; set;
        }

        [JsonProperty("timestamp")]
        internal DateTime Timestamp
        {
            get; set;
        }

        [JsonProperty("error")]
        internal string ErrorText
        {
            get; set;
        }
    }

    internal class Error
    {
        [JsonProperty("defaultUserMessage")]
        internal string DefaultUserMessage
        {
            get; set;
        }

        [JsonProperty("userMessageGlobalisationCode")]
        internal string UserMessageGlobalisationCode
        {
            get; set;
        }
    }
}
