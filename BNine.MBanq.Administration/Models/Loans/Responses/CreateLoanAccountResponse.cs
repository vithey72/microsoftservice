﻿namespace BNine.MBanq.Administration.Models.Loans.Responses
{
    using Newtonsoft.Json;

    internal class CreateLoanAccountResponse
    {
        [JsonProperty("officeId")]
        internal int OfficeId
        {
            get; set;
        }

        [JsonProperty("clientId")]
        internal int ClientId
        {
            get; set;
        }

        [JsonProperty("loanId")]
        internal int LoanId
        {
            get; set;
        }

        [JsonProperty("resourceId")]
        internal int ResourceId
        {
            get; set;
        }
    }
}
