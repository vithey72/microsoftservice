﻿namespace BNine.MBanq.Administration.Models.Loans.Responses
{
    using Newtonsoft.Json;

    internal class MakeRepaymentLoanAccountResponse
    {
        [JsonProperty("officeId")]
        internal int OfficeId
        {
            get; set;
        }

        [JsonProperty("clientId")]
        internal int ClientId
        {
            get; set;
        }

        [JsonProperty("loanId")]
        internal int LoanId
        {
            get; set;
        }

        [JsonProperty("resourceId")]
        internal int ResourceId
        {
            get; set;
        }

        [JsonProperty("changes")]
        internal LoanAccountChanges Changes
        {
            get; set;
        }
    }

    internal class LoanAccountChanges
    {
        [JsonProperty("paymentTypeId")]
        internal string PaymentTypeId
        {
            get; set;
        }

        [JsonProperty("transactionAmount")]
        internal string TransactionAmount
        {
            get; set;
        }

        [JsonProperty("transactionDate")]
        internal string TransactionDate
        {
            get; set;
        }

        [JsonProperty("locale")]
        internal string Locale
        {
            get; set;
        }

        [JsonProperty("dateFormat")]
        internal string DateFormat
        {
            get; set;
        }
    }
}
