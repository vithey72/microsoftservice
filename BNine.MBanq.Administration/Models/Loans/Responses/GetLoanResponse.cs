﻿namespace BNine.MBanq.Administration.Models.Loans.Responses
{
    using System.Collections.Generic;
    using Common.Classes;
    using Newtonsoft.Json;

    internal class GetLoanResponse
    {
        [JsonProperty("id")]
        internal int Id
        {
            get; set;
        }

        [JsonProperty("interestRatePerPeriod")]
        internal double InterestRatePerPeriod
        {
            get; set;
        }

        [JsonProperty("summary")]
        internal LoanSummary Summary
        {
            get; set;
        }

        [JsonProperty("status")]
        internal LoanStatus Status
        {
            get; set;
        }

        [JsonProperty("approvedPrincipal")]
        public double ApprovedPrincipal
        {
            get; set;
        }

        [JsonProperty("timeline")]
        public Timeline Timeline
        {
            get; set;
        }
    }

    internal class LoanStatus : IdValue
    {
        [JsonProperty("closedObligationsMet")]
        public bool ClosedObligationsMet
        {
            get; set;
        }
    }

    internal class Timeline
    {
        [JsonProperty("expectedMaturityDate")]
        public IEnumerable<int> ExpectedMaturityDate
        {
            get; set;
        }
    }

    internal class LoanSummary
    {
        [JsonProperty("principalDisbursed")]
        internal double PrincipalDisbursed
        {
            get; set;
        }

        [JsonProperty("principalOutstanding")]
        internal double PrincipalOutstanding
        {
            get; set;
        }

        [JsonProperty("totalOverdue")]
        internal double TotalOverdue
        {
            get; set;
        }

        [JsonProperty("interestPaid")]
        internal double InterestPaid
        {
            get; set;
        }

        [JsonProperty("interestCharged")]
        internal double InterestCharged
        {
            get; set;
        }

        [JsonProperty("feeChargesCharged")]
        internal decimal FeeChargesCharged
        {
            get; set;
        }

        [JsonProperty("feeChargesOutstanding")]
        internal decimal FeeChargesOutstanding
        {
            get; set;
        }
    }
}

