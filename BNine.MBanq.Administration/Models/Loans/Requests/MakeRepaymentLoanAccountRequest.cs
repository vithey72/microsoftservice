﻿namespace BNine.MBanq.Administration.Models.Loans.Requests
{
    using Base.Requests;
    using Newtonsoft.Json;

    internal class MakeRepaymentLoanAccountRequest : RequestWithLocaleAndDateFormat
    {
        [JsonProperty("paymentTypeId")]
        internal int PaymentTypeId
        {
            get; set;
        }

        [JsonProperty("transactionAmount")]
        internal int TransactionAmount
        {
            get; set;
        }

        [JsonProperty("transactionDate")]
        internal string TransactionDate
        {
            get; set;
        }

        [JsonProperty("note")]
        internal string Note
        {
            get; set;
        }
    }
}
