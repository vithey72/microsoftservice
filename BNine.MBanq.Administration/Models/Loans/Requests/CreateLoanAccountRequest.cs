﻿namespace BNine.MBanq.Administration.Models.Loans.Requests
{
    using System.Collections.Generic;
    using Base.Requests;
    using Enums.Loans;
    using Newtonsoft.Json;

    internal class CreateLoanAccountRequest : RequestWithLocaleAndDateFormat
    {
        [JsonProperty("clientId")]
        internal int ClientId
        {
            get; set;
        }

        [JsonProperty("productId")]
        internal int ProductId
        {
            get; set;
        }

        [JsonProperty("disbursementData")]
        internal IEnumerable<DisbursementItemData> DisbursementData
        {
            get; set;
        }

        [JsonProperty("charges")]
        internal IEnumerable<AdditionalCharge> Charges
        {
            get; set;
        }

        [JsonProperty("principal")]
        internal double Principal
        {
            get; set;
        }

        [JsonProperty("loanTermFrequency")]
        internal int LoanTermFrequency
        {
            get; set;
        }

        [JsonProperty("loanTermFrequencyType")]
        internal TermFrequencyType LoanTermFrequencyType
        {
            get; set;
        }

        [JsonProperty("loanType")]
        internal string LoanType
        {
            get; set;
        }

        [JsonProperty("numberOfRepayments")]
        internal int NumberOfRepayments
        {
            get; set;
        }

        [JsonProperty("repaymentEvery")]
        internal int RepaymentEvery
        {
            get; set;
        }

        [JsonProperty("repaymentFrequencyType")]
        internal int RepaymentFrequencyType
        {
            get; set;
        }

        [JsonProperty("interestRatePerPeriod")]
        internal double InterestRatePerPeriod
        {
            get; set;
        }

        [JsonProperty("amortizationType")]
        internal int AmortizationType
        {
            get; set;
        }

        [JsonProperty("interestType")]
        internal int InterestType
        {
            get; set;
        }

        [JsonProperty("interestCalculationPeriodType")]
        internal int InterestCalculationPeriodType
        {
            get; set;
        }

        [JsonProperty("transactionProcessingStrategyId")]
        internal int TransactionProcessingStrategyId
        {
            get; set;
        }

        [JsonProperty("expectedDisbursementDate")]
        internal string ExpectedDisbursementDate
        {
            get; set;
        }

        [JsonProperty("submittedOnDate")]
        internal string SubmittedOnDate
        {
            get; set;
        }

        [JsonProperty("linkAccountId")]
        internal int LinkAccountId
        {
            get; set;
        }

        [JsonProperty("createStandingInstructionAtDisbursement")]
        internal bool CreateStandingInstructionAtDisbursement
        {
            get; set;
        }

        [JsonProperty("maxOutstandingLoanBalance")]
        internal int MaxOutstandingLoanBalance
        {
            get; set;
        }

        [JsonProperty("prepayLoanOnDeposits")]
        internal bool PrepayLoanOnDeposits
        {
            get; set;
        }
    }

    internal class DisbursementItemData
    {
        [JsonProperty("expectedDisbursementDate")]
        internal string ExpectedDisbursementDate
        {
            get; set;
        }

        [JsonProperty("principal")]
        internal int Principal
        {
            get; set;
        }
    }

    internal class AdditionalCharge
    {
        [JsonProperty("chargeId")]
        internal int ChargeId
        {
            get; set;
        }

        [JsonProperty("amount")]
        internal decimal Amount
        {
            get; set;
        }

        [JsonProperty("addToPrincipalAmount")]
        internal bool AddToPrincipalAmount
        {
            get; set;
        }
    }
}
