﻿namespace BNine.MBanq.Administration.Models.Loans.Requests
{
    using Base.Requests;
    using Newtonsoft.Json;

    internal class ApproveLoanAccountRequest : RequestWithLocaleAndDateFormat
    {
        [JsonProperty("approvedOnDate")]
        internal string ApprovedOnDate
        {
            get; set;
        }
    }
}
