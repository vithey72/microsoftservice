﻿namespace BNine.MBanq.Administration.Models.Loans.Requests
{
    using Base.Requests;
    using Newtonsoft.Json;

    internal class DisburseLoanAccountRequest : RequestWithLocaleAndDateFormat
    {
        [JsonProperty("actualDisbursementDate")]
        internal string ActualDisbursementDate
        {
            get; set;
        }

        [JsonProperty("transactionAmount")]
        internal double TransactionAmount
        {
            get; set;
        }
    }
}
