﻿namespace BNine.MBanq.Administration.Models.Clients.Responses
{
    using Newtonsoft.Json;

    internal class GetVerificationStatusResponse
    {
        [JsonProperty("id")]
        internal int Id
        {
            get; set;
        }

        [JsonProperty("accountNo")]
        internal string AccountNo
        {
            get; set;
        }

        [JsonProperty("verificationStatus")]
        internal VerificationStatus VerificationStatus
        {
            get; set;
        }

    }

    internal class VerificationStatus
    {
        [JsonProperty("id")]
        internal int Id
        {
            get; set;
        }

        [JsonProperty("value")]
        internal string Value
        {
            get; set;
        }
    }
}
