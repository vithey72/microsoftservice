﻿namespace BNine.MBanq.Administration.Models.Clients.Responses
{
    using Newtonsoft.Json;

    internal class CreateClientResponse
    {
        [JsonProperty("officeId")]
        internal int OfficeId
        {
            get; set;
        }

        [JsonProperty("clientId")]
        internal int ClientId
        {
            get; set;
        }

        [JsonProperty("resourceId")]
        internal int ResourceId
        {
            get; set;
        }
    }
}
