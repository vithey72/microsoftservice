﻿namespace BNine.MBanq.Administration.Models.Clients.Responses
{
    using Newtonsoft.Json;

    public class GetClientResponse
    {
        [JsonProperty("id")]
        public int Id
        {
            get; set;
        }

        [JsonProperty("clientClassification")]
        public Classification ClientClassification
        {
            get; set;
        }
    }

    public class Classification
    {
        [JsonProperty("id")]
        public int Id
        {
            get; set;
        }

        [JsonProperty("name")]
        public string Name
        {
            get; set;
        }

        [JsonProperty("active")]
        public bool Active
        {
            get; set;
        }

        [JsonProperty("mandatory")]
        public bool Mandatory
        {
            get; set;
        }

        [JsonProperty("systemDefined")]
        public bool SystemDefined
        {
            get; set;
        }
    }
}
