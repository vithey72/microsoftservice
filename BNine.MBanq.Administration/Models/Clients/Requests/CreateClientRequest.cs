﻿namespace BNine.MBanq.Administration.Models.Clients.Requests
{
    using Base.Requests;
    using Newtonsoft.Json;

    internal class CreateClientRequest : RequestWithLocaleAndDateFormat
    {
        [JsonProperty("officeId")]
        internal int OfficeId
        {
            get; set;
        }

        [JsonProperty("firstname")]
        internal string FirstName
        {
            get; set;
        }

        [JsonProperty("lastname")]
        internal string LastName
        {
            get; set;
        }

        [JsonProperty("mobileNo")]
        internal string MobileNo
        {
            get; set;
        }

        [JsonProperty("emailAddress")]
        internal string EmailAddress
        {
            get; set;
        }

        [JsonProperty("dateOfBirth")]
        internal string DateOfBirth
        {
            get; set;
        }

        [JsonProperty("active")]
        internal bool IsActtive
        {
            get; set;
        }
    }
}
