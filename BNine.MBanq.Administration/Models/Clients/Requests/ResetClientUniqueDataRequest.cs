﻿namespace BNine.MBanq.Administration.Models.Clients.Requests
{
    using Newtonsoft.Json;

    internal class ResetClientUniqueDataRequest
    {
        public ResetClientUniqueDataRequest()
        {
            MobileNo = null;
            Email = null;
        }

        [JsonProperty("mobileNo")]
        internal string MobileNo
        {
            get;
        }

        [JsonProperty("emailAddress")]
        internal string Email
        {
            get;
        }
    }
}
