﻿namespace BNine.MBanq.Administration.Models.Clients.Requests
{
    using Newtonsoft.Json;

    internal class UpdateClientRequest
    {
        [JsonProperty("firstname")]
        internal string FirstName
        {
            get; set;
        }

        [JsonProperty("lastname")]
        internal string LastName
        {
            get; set;
        }

        [JsonProperty("mobileNo")]
        internal string MobileNo
        {
            get; set;
        }

        [JsonProperty("emailAddress")]
        internal string EmailAddress
        {
            get; set;
        }

        [JsonProperty("dateFormat")]
        internal string DateFormat
        {
            get; set;
        }

        [JsonProperty("dateOfBirth")]
        internal string DateOfBirth
        {
            get; set;
        }

        [JsonProperty("locale")]
        internal string Locale
        {
            get; set;
        }
    }
}
