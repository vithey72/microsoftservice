﻿namespace BNine.MBanq.Administration.Models.Clients.Requests
{
    using Newtonsoft.Json;

    public class UpdateClientEmailRequest
    {
        [JsonProperty("emailAddress")]
        internal string EmailAddress
        {
            get; set;
        }
    }
}
