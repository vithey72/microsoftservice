﻿namespace BNine.MBanq.Administration.Models.Clients.Requests
{
    using System.Collections.Generic;
    using Newtonsoft.Json;

    internal class LinkClientToUserRequest
    {
        [JsonProperty("authorizations")]
        internal IEnumerable<int> Authorizations
        {
            get; set;
        }
    }
}
