﻿namespace BNine.MBanq.Administration.Models.Users.Requests
{
    using Newtonsoft.Json;

    internal class UpdateClientPhoneNumberRequest
    {
        [JsonProperty("mobileNo")]
        internal string PhoneNumber
        {
            get; set;
        }
    }
}
