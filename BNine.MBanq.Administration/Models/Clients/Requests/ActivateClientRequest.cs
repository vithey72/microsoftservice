﻿namespace BNine.MBanq.Administration.Models.Clients.Requests
{
    using Base.Requests;
    using Newtonsoft.Json;

    internal class ActivateClientRequest : RequestWithLocaleAndDateFormat
    {
        [JsonProperty("activationDate")]
        internal string ActivationDate
        {
            get; set;
        }
    }
}
