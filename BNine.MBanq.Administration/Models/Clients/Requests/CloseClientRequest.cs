﻿namespace BNine.MBanq.Administration.Models.Clients.Requests
{
    using Base.Requests;
    using Newtonsoft.Json;

    internal class CloseClientRequest : RequestWithLocaleAndDateFormat
    {
        [JsonProperty("closureDate")]
        internal string ClosureDate
        {
            get; set;
        }

        [JsonProperty("closureReasonId")]
        internal int ClosureReasonId
        {
            get;
            set;
        }
    }
}
