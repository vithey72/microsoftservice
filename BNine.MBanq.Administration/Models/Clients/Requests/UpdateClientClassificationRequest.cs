﻿namespace BNine.MBanq.Administration.Models.Clients.Requests;

using BNine.Constants;
using Cards.Requests;
using Newtonsoft.Json;

internal class UpdateClientClassificationRequest : MBanqRequestBase
{
    [JsonProperty("expectedApplicableDate")]
    internal string ExpectedApplicableDateString => ExpectedApplicableDate?.ToString(DateFormat.MBanqFormat, DateFormat.GetEnCulture());

    [JsonIgnore]
    internal DateTime? ExpectedApplicableDate
    {
        get;
        set;
    }

    [JsonProperty("classificationId")]
    internal int ClassificationId
    {
        get; set;
    }
}
