﻿namespace BNine.MBanq.Administration.Models.ExternalCards.Responses
{
    using System;
    using Newtonsoft.Json;

    internal class GetPublicKeyResponse
    {
        [JsonProperty("keyID")]
        internal string KeyID
        {
            get; set;
        }

        [JsonProperty("key")]
        internal string Key
        {
            get; set;
        }

        [JsonProperty("expiration")]
        internal DateTime? Expiration
        {
            get; set;
        }
    }
}
