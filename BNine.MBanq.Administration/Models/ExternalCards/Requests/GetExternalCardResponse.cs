﻿namespace BNine.MBanq.Administration.Models.ExternalCards.Requests
{
    using Newtonsoft.Json;

    internal class GetExternalCardResponse
    {
        [JsonProperty("id")]
        internal int Id
        {
            get; set;
        }

        [JsonProperty("lastDigits")]
        internal string LastDigits
        {
            get; set;
        }

        [JsonProperty("expiryDate")]
        internal string ExpiryDate
        {
            get; set;
        }

        [JsonProperty("network")]
        internal string Network
        {
            get; set;
        }

        [JsonProperty("pushEnabled")]
        internal bool PushEnabled
        {
            get; set;
        }

        [JsonProperty("pullEnabled")]
        internal bool PullEnabled
        {
            get; set;
        }

        [JsonProperty("isRegulated")]
        internal bool IsRegulated
        {
            get; set;
        }

        [JsonProperty("countryCode")]
        internal string CountryCode
        {
            get; set;
        }

        [JsonProperty("currencyCode")]
        internal string CurrencyCode
        {
            get; set;
        }

        [JsonProperty("cardType")]
        internal string CardType
        {
            get; set;
        }

        [JsonProperty("fullName")]
        internal string FullName
        {
            get; set;
        }

        [JsonProperty("referenceId")]
        internal string ReferenceId
        {
            get; set;
        }

        [JsonProperty("externalId")]
        internal string ExternalId
        {
            get; set;
        }

        [JsonProperty("deleted")]
        internal bool IsDeleted
        {
            get; set;
        }

    }
}
