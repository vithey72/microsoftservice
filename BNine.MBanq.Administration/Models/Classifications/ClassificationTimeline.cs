﻿namespace BNine.MBanq.Administration.Models.Classifications;

using System;
using Newtonsoft.Json;

internal class ClassificationTimeline
{
    [JsonProperty("clientId")]
    internal int ClientId
    {
        get;
        set;
    }

    [JsonProperty("currentClassificationStartDate")]
    internal DateTime CurrentClassificationStartDate
    {
        get;
        set;
    }


    [JsonProperty("currentClassification")]
    internal Classification CurrentClassification
    {
        get;
        set;
    }

    [JsonProperty("nextPaymentDate")]
    internal DateTime? CurrentClassificationNextPaymentDate

    {
        get;
        set;
    }

    [JsonProperty("upcomingClassification")]
    internal Classification? UpcomingClassification
    {
        get;
        set;
    }

    [JsonProperty("applicableDate")]
    internal DateTime? UpcomingClassificationStartDate
    {
        get;
        set;
    }
}

internal struct Classification
{
    [JsonProperty("id")]
    internal int Id
    {
        get;
        set;
    }

    [JsonProperty("name")]
    internal string Name
    {
        get;
        set;
    }

    [JsonProperty("position")]
    internal int Position
    {
        get;
        set;
    }

    [JsonProperty("description")]
    internal string Description
    {
        get;
        set;
    }

    [JsonProperty("active")]
    internal bool Active
    {
        get;
        set;
    }

    [JsonProperty("mandatory")]
    internal bool Mandatory
    {
        get;
        set;
    }

    [JsonProperty("systemDefined")]
    internal bool SystemDefined
    {
        get;
        set;
    }
}
