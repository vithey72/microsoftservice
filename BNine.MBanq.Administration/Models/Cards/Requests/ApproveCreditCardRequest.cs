﻿namespace BNine.MBanq.Administration.Models.Cards.Requests;

using BNine.Constants;
using Newtonsoft.Json;

internal class ApproveCreditCardRequest : MBanqRequestBase
{
    [JsonProperty("approvedOnDate")]
    internal string ApprovedOndate => DateTime.ToString(DateFormat.MBanqFormat);
}
