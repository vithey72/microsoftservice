﻿namespace BNine.MBanq.Administration.Models.Cards.Requests;

using System;
using BNine.Constants;
using Newtonsoft.Json;

internal class MBanqRequestBase
{
    [JsonIgnore]
    internal DateTime DateTime
    {
        get;
        set;
    }

    [JsonProperty("locale")]
    internal string Locale
    {
        get;
        set;
    } = "en";

    [JsonProperty("dateFormat")]
    internal string DateFormatProperty
    {
        get;
        set;
    } = DateFormat.MBanqFormat;
}
