﻿namespace BNine.MBanq.Administration.Models.Cards.Requests
{
    using Newtonsoft.Json;

    internal class CreateDebitCardRequest
    {
        [JsonProperty("savingsAccountId")]
        internal int SavingsAccountId
        {
            get; set;
        }

        [JsonProperty("productId")]
        internal int ProductId
        {
            get; set;
        }
    }
}
