﻿namespace BNine.MBanq.Administration.Models.Cards.Requests
{
    using Newtonsoft.Json;

    internal class EnableOnlinePaymentsRequest
    {
        [JsonProperty("onlinePaymentEnabled")]
        internal bool OnlinePaymentEnabled
        {
            get; set;
        }
    }
}
