﻿namespace BNine.MBanq.Administration.Models.Cards.Requests;

using BNine.Constants;
using Newtonsoft.Json;

internal class CreateCreditCardRequest : MBanqRequestBase
{
    [JsonProperty("cardProductId")]
    internal int CardProductId
    {
        get; set;
    }

    [JsonProperty("nominalAnnualInterestRate")]
    internal decimal NominalAnnualInterestRate
    {
        get;
        set;
    }

    [JsonProperty("nominalCashAdvanceInterestRate")]
    internal decimal NominalCashAdvanceInterestRate
    {
        get;
        set;
    }

    [JsonProperty("externalId")]
    internal string ExternalId
    {
        get;
        set;
    }

    [JsonProperty("currentAccountId")]
    internal int CurrentAccountId
    {
        get;
        set;
    }

    [JsonProperty("creditLimit")]
    internal int CreditLimit
    {
        get;
        set;
    }

    [JsonProperty("cashLimit")]
    internal int CashLimit
    {
        get;
        set;
    }

    [JsonProperty("submittedOnDate")]
    internal string SubmittedOnDate => DateTime.ToString(DateFormat.MBanqFormat);


    [JsonProperty("clientId")]
    internal int ClientId
    {
        get;
        set;
    }
}
