﻿namespace BNine.MBanq.Administration.Models.Cards.Requests
{
    using System.Collections.Generic;
    using Newtonsoft.Json;

    internal class SetCardLimitsRequest
    {
        [JsonProperty("velocityRules")]
        internal List<SetVelocityRuleModel> VelocityRules
        {
            get; set;
        }
    }
}
