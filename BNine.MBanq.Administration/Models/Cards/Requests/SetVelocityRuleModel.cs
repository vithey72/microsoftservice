﻿namespace BNine.MBanq.Administration.Models.Cards.Requests
{
    using System.Collections.Generic;
    using Newtonsoft.Json;

    internal class SetVelocityRuleModel
    {
        [JsonProperty("controls")]
        internal List<string> Controls
        {
            get; set;
        }

        [JsonProperty("type")]
        internal string Type
        {
            get; set;
        }

        [JsonProperty("timePeriod")]
        internal int TimePeriod
        {
            get; set;
        }

        [JsonProperty("timeUnit")]
        internal string TimeUnit
        {
            get; set;
        }

        [JsonProperty("value")]
        internal double Value
        {
            get; set;
        }
    }
}
