﻿namespace BNine.MBanq.Administration.Models.Cards.Responses
{
    using Newtonsoft.Json;

    public class CardAuthorizationResponse
    {
        [JsonProperty("CardAuthorization")]
        public CardAuthorization CardAuthorization
        {
            get; set;
        }
    }


    public class CardAuthorization
    {
        [JsonProperty("id")]
        public long Id
        {
            get; set;
        }

        [JsonProperty("status")]
        public string Status
        {
            get; set;
        }

        [JsonProperty("card")]
        public CardInfo Card
        {
            get; set;
        }
    }

    public class CardInfo
    {
        [JsonProperty("id")]
        public long Id
        {
            get; set;
        }

        [JsonProperty("primaryAccountNumber")]
        public string PrimaryAccountNumber
        {
            get; set;
        }
    }
}
