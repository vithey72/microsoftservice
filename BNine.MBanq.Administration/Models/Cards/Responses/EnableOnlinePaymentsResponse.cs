﻿namespace BNine.MBanq.Administration.Models.Cards.Responses
{
    using Newtonsoft.Json;

    internal class EnableOnlinePaymentsResponse
    {
        [JsonProperty("resourceId")]
        internal int ResourseId
        {
            get; set;
        }
    }
}
