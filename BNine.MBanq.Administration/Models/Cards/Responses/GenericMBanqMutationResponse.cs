﻿namespace BNine.MBanq.Administration.Models.Cards.Responses
{
    using Newtonsoft.Json;

    internal class GenericMBanqMutationResponse
    {
        [JsonProperty("resourceId")]
        internal int ResourceId
        {
            get; set;
        }
    }
}
