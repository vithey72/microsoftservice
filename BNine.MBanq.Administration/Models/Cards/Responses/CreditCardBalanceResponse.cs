﻿namespace BNine.MBanq.Administration.Models.Cards.Responses;

using Enums.DebitCard;
using Newtonsoft.Json;

internal class CreditCardBalanceResponse
{
    [JsonProperty("cardNumber")]
    internal string CardNumber
    {
        get; set;
    }

    [JsonIgnore]
    internal string Last4Digits => CardNumber != null && CardNumber.Length >= 4
        ? CardNumber[^4..]
        : null;

    [JsonProperty("creditLimit")]
    internal decimal CreditLimit
    {
        get;
        set;
    }

    [JsonProperty("cashLimit")]
    internal decimal CashLimit
    {
        get;
        set;
    }

    [JsonProperty("availableCreditLimit")]
    internal decimal AvailableCreditLimit
    {
        get;
        set;
    }

    [JsonProperty("availableCashLimit")]
    internal decimal AvailableCashLimit
    {
        get;
        set;
    }

    [JsonProperty("amountOverCreditLimit")]
    internal decimal AmountOverCreditLimit
    {
        get;
        set;
    }

    [JsonProperty("unBilledAmount")]
    internal decimal UnBilledAmount
    {
        get;
        set;
    }

    [JsonProperty("currentBalance")]
    internal decimal CurrentBalance
    {
        get;
        set;
    }

    [JsonProperty("lastPaymentAmount")]
    internal decimal LastPaymentAmount
    {
        get;
        set;
    }

    [JsonProperty("cardStatus")]
    internal CardStatus CardStatus
    {
        get;
        set;
    }

    [JsonProperty("lastPaymentDate")]
    internal DateTime LastPaymentDate
    {
        get;
        set;
    }
}
