﻿namespace BNine.MBanq.Administration.Models.Cards.Responses
{
    using System.Collections.Generic;
    using Newtonsoft.Json;

    internal class CardVelocityRulesResponse
    {
        [JsonProperty("velocityRules")]
        internal List<VelocityRule> VelocityRules
        {
            get; set;
        }
    }

    internal class VelocityRule
    {
        [JsonProperty("controls")]
        internal List<string> Controls
        {
            get; set;
        }

        [JsonProperty("type")]
        internal string Type
        {
            get; set;
        }

        [JsonProperty("value")]
        internal double Value
        {
            get; set;
        }

        [JsonProperty("timePeriod")]
        internal int TimePeriod
        {
            get; set;
        }

        [JsonProperty("timeUnit")]
        internal string TimeUnit
        {
            get; set;
        }

        [JsonProperty("version")]
        internal int Version
        {
            get; set;
        }

        [JsonProperty("id")]
        internal int Id
        {
            get; set;
        }
    }
}
