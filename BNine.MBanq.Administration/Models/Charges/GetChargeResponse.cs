﻿namespace BNine.MBanq.Administration.Models.Charges
{
    using Newtonsoft.Json;

    internal class GetChargeResponse
    {
        [JsonProperty("Charge")]
        internal Charge Charge
        {
            get; set;
        }
    }

    internal class Charge
    {
        [JsonProperty("id")]
        internal long Id
        {
            get; set;
        }

        [JsonProperty("amount")]
        internal decimal Amount
        {
            get; set;
        }

        [JsonProperty("minCap")]
        internal decimal? MinCap
        {
            get; set;
        }

        [JsonProperty("name")]
        internal string Name
        {
            get; set;
        }
    }
}
