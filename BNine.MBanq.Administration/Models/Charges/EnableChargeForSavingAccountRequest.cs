﻿namespace BNine.MBanq.Administration.Models.Charges;

using Newtonsoft.Json;

internal class EnableChargeForSavingAccountRequest : PostChargeBaseRequest
{
    [JsonProperty("chargeId")]
    public int ChargeTypeId
    {
        get;
        set;
    }
}
