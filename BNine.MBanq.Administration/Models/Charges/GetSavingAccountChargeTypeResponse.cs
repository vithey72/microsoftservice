﻿namespace BNine.MBanq.Administration.Models.Charges;

using Newtonsoft.Json;

internal class GetSavingAccountChargeTypeResponse
{
    [JsonProperty("id")]
    internal int Id
    {
        get; set;
    }

    [JsonProperty("name")]
    internal string Name
    {
        get; set;
    }

    [JsonProperty("active")]
    internal bool IsActive
    {
        get; set;
    }

    [JsonProperty("amount")]
    internal decimal Amount
    {
        get; set;
    }

    internal class TimeType
    {
        [JsonProperty("id")]
        public int Id
        {
            get;
            set;
        }

        [JsonProperty("code")]
        public string Code
        {
            get;
            set;
        }

        [JsonProperty("value")]
        public string Value
        {
            get;
            set;
        }
    }
}
