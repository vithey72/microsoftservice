﻿namespace BNine.MBanq.Administration.Models.Charges;

using Newtonsoft.Json;

internal class EnableChargeForSavingAccountResponse
{
    [JsonProperty("resourceId")]
    public int ResourceId
    {
        get;
        set;
    }
}
