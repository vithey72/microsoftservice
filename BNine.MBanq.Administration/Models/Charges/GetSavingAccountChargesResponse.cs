﻿namespace BNine.MBanq.Administration.Models.Charges
{
    using Newtonsoft.Json;
    using SavingAccounts.Responses;

    internal class GetSavingAccountChargeResponse
    {
        [JsonProperty("id")]
        internal int Id
        {
            get; set;
        }

        [JsonProperty("chargeId")]
        internal int ChargeId
        {
            get; set;
        }

        [JsonProperty("name")]
        internal string Name
        {
            get; set;
        }

        [JsonProperty("dueDate")]
        internal IEnumerable<int> DueDate
        {
            get; set;
        }

        [JsonProperty("feeOnMonthDay")]
        internal FeeDate FeeOnMonthDay
        {
            get; set;
        }

        [JsonProperty("currency")]
        internal Currency Currency
        {
            get; set;
        }

        [JsonProperty("amount")]
        internal decimal Amount
        {
            get; set;
        }

        [JsonProperty("amountPaid")]
        internal decimal AmountPaid
        {
            get; set;
        }

        [JsonProperty("isActive")]
        internal bool IsActive
        {
            get; set;
        }

        [JsonProperty("totalDeferredChargeAmount")]
        internal decimal TotalDeferredChargeAmount
        {
            get; set;
        }

        [JsonProperty("chargeTimeType")]
        internal TimeType ChargeTimeType
        {
            get; set;
        }

        internal class TimeType
        {
            [JsonProperty("id")]
            public int Id
            {
                get;
                set;
            }

            [JsonProperty("code")]
            public string Code
            {
                get;
                set;
            }

            [JsonProperty("value")]
            public string Value
            {
                get;
                set;
            }
        }
    }

    internal class FeeDate
    {
        [JsonProperty("month")]
        internal int Month
        {
            get; set;
        }

        [JsonProperty("day")]
        internal int Day
        {
            get; set;
        }
    }
}
