﻿namespace BNine.MBanq.Administration.Models.Charges;

using Newtonsoft.Json;

public class PostChargeBaseRequest
{
    [JsonProperty("amount")]
    public decimal Amount
    {
        get;
        set;
    }

    [JsonProperty("dateFormat")]
    public string DateFormat
    {
        get;
        set;
    } = BNine.Constants.DateFormat.MBanqFormat;

    [JsonProperty("dueDate")]
    public string DueDate
    {
        get;
        set;
    }

    [JsonProperty("locale")]
    public string Locale
    {
        get;
        set;
    } = "en";
}
