﻿namespace BNine.MBanq.Administration.Models.SavingAccounts.Requests
{
    using Base.Requests;
    using Newtonsoft.Json;

    internal class CloseSavingAccountRequest : RequestWithLocaleAndDateFormat
    {
        [JsonProperty("closedOnDate")]
        internal string ClosedOnDate
        {
            get;
            set;
        }

        [JsonProperty("paymentTypeId")]
        internal int PaymentTypeId
        {
            get;
            set;
        }

        [JsonProperty("postInterestValidationOnClosure")]
        internal bool PostInterestValidationOnClosure
        {
            get;
            set;
        }

        [JsonProperty("withdrawBalance")]
        internal bool WithdrawBalance
        {
            get;
            set;
        }

        [JsonProperty("note")]
        public string Note
        {
            get;
            set;
        }
    }
}
