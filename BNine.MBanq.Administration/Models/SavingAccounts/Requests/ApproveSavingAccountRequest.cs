﻿namespace BNine.MBanq.Administration.Models.SavingAccounts.Requests
{
    using Base.Requests;
    using Newtonsoft.Json;

    internal class ApproveSavingAccountRequest : RequestWithLocaleAndDateFormat
    {
        [JsonProperty("approvedOnDate")]
        internal string ApprovedOnDate
        {
            get; set;
        }
    }
}
