﻿namespace BNine.MBanq.Administration.Models.SavingAccounts.Requests
{
    using Newtonsoft.Json;

    internal class BlockSavingAccountRequest
    {
        [JsonProperty("reason")]
        internal string Reason
        {
            get;
            set;
        }
    }
}
