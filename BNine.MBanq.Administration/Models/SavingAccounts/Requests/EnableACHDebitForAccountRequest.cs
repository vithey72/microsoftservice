﻿namespace BNine.MBanq.Administration.Models.SavingAccounts.Requests
{
    using Newtonsoft.Json;

    internal class EnableACHDebitForAccountRequest
    {
        [JsonProperty("allowACHDebit")]
        internal bool AllowACHDebit
        {
            get; set;
        } = true;
    }
}
