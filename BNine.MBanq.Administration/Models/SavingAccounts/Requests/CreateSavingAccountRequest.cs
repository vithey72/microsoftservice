﻿namespace BNine.MBanq.Administration.Models.SavingAccounts.Requests
{
    using Base.Requests;
    using Newtonsoft.Json;

    internal class CreateSavingAccountRequest : RequestWithLocaleAndDateFormat
    {
        [JsonProperty("clientId")]
        internal int ClientId
        {
            get; set;
        }

        [JsonProperty("productId")]
        internal int ProductId
        {
            get; set;
        }

        [JsonProperty("submittedOnDate")]
        internal string SubmittedOnDate
        {
            get; set;
        }

        [JsonProperty("lockinPeriodFrequency")]
        internal int LockinPeriodFrequency
        {
            get; set;
        }


        [JsonProperty("lockinPeriodFrequencyType")]
        internal int LockinPeriodFrequencyType
        {
            get; set;
        }
    }
}
