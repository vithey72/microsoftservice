﻿namespace BNine.MBanq.Administration.Models.SavingAccounts.Requests
{
    using Base.Requests;
    using Newtonsoft.Json;

    internal class ActivateSavingAccountRequest : RequestWithLocaleAndDateFormat
    {
        [JsonProperty("activatedOnDate")]
        internal string ActivatedOnDate
        {
            get; set;
        }
    }
}
