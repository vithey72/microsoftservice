﻿namespace BNine.MBanq.Administration.Models.SavingAccounts.Responses
{
    using Newtonsoft.Json;

    internal class Currency
    {
        [JsonProperty("code")]
        internal string Code
        {
            get; set;
        }

        [JsonProperty("name")]
        internal string Name
        {
            get; set;
        }
    }
}
