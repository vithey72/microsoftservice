﻿namespace BNine.MBanq.Administration.Models.SavingAccounts.Responses
{
    using Newtonsoft.Json;

    internal class Status
    {
        [JsonProperty("id")]
        internal int Id
        {
            get; set;
        }

        [JsonProperty("value")]
        internal string Value
        {
            get; set;
        }
    }
}
