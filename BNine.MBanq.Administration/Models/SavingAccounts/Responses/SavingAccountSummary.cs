﻿namespace BNine.MBanq.Administration.Models.SavingAccounts.Responses
{
    using Newtonsoft.Json;

    internal class SavingAccountSummary
    {
        [JsonProperty("currency")]
        internal Currency Currency
        {
            get; set;
        }

        [JsonProperty("availableBalance")]
        internal decimal AvailableBalance
        {
            get; set;
        }

        [JsonProperty("accountBalance")]
        internal decimal AccountBalance
        {
            get; set;
        }

        [JsonProperty("totalDeposits")]
        internal decimal TotalDeposits
        {
            get; set;
        }
    }
}
