﻿namespace BNine.MBanq.Administration.Models.SavingAccounts.Responses
{
    using Newtonsoft.Json;

    internal class CreateSavingAccountResponse
    {
        [JsonProperty("officeId")]
        internal int OfficeId
        {
            get; set;
        }

        [JsonProperty("clientId")]
        internal int ClientId
        {
            get; set;
        }

        [JsonProperty("savingsId")]
        internal int SavingsId
        {
            get; set;
        }

        [JsonProperty("resourceId")]
        internal int ResourceId
        {
            get; set;
        }
    }
}
