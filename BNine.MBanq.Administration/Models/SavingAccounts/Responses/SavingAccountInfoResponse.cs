﻿namespace BNine.MBanq.Administration.Models.SavingAccounts.Responses
{
    using Newtonsoft.Json;

    internal class SavingAccountInfoResponse
    {
        [JsonProperty("id")]
        internal int Id
        {
            get; set;
        }

        [JsonProperty("accountNo")]
        internal string AccountNumber
        {
            get; set;
        }

        [JsonProperty("status")]
        internal Status Status
        {
            get; set;
        }

        [JsonProperty("summary")]
        internal SavingAccountSummary Summary
        {
            get; set;
        }

        [JsonProperty("subStatus")]
        internal SubStatus SubStatus
        {
            get; set;
        }

        [JsonProperty("savingsAmountOnHold")]
        internal double SavingsAmountOnHold
        {
            get; set;
        }

        [JsonProperty("cardRestricted")]
        internal bool IsCardOrderingRestricted
        {
            get; set;
        }
    }

    internal class SubStatus
    {
        [JsonProperty("id")]
        public int Id
        {
            get; set;
        }
    }
}
