﻿namespace BNine.MBanq.Administration.Models.LoanProducts.Responses
{
    using Common.Classes;
    using Newtonsoft.Json;

    internal class GetLoanProductResponse
    {
        [JsonProperty("id")]
        internal int Id
        {
            get; set;
        }

        [JsonProperty("name")]
        internal string Name
        {
            get; set;
        }

        [JsonProperty("shortName")]
        internal string ShortName
        {
            get; set;
        }

        [JsonProperty("description")]
        internal string Description
        {
            get; set;
        }

        [JsonProperty("fundId")]
        internal int FundId
        {
            get; set;
        }

        [JsonProperty("fundName")]
        internal string FundName
        {
            get; set;
        }

        [JsonProperty("useBorrowerCycle")]
        internal bool UseBorrowerCycle
        {
            get; set;
        }

        [JsonProperty("status")]
        internal string Status
        {
            get; set;
        }

        [JsonProperty("principal")]
        internal double Principal
        {
            get; set;
        }

        [JsonProperty("numberOfRepayments")]
        internal int NumberOfRepayments
        {
            get; set;
        }

        [JsonProperty("repaymentEvery")]
        internal int RepaymentEvery
        {
            get; set;
        }

        [JsonProperty("repaymentFrequencyType")]
        internal IdValue RepaymentFrequencyType
        {
            get; set;
        }

        [JsonProperty("interestRatePerPeriod")]
        internal double InterestRatePerPeriod
        {
            get; set;
        }

        [JsonProperty("interestRateFrequencyType")]
        internal IdValue InterestRateFrequencyType
        {
            get; set;
        }

        [JsonProperty("annualInterestRate")]
        internal double AnnualInterestRate
        {
            get; set;
        }

        [JsonProperty("amortizationType")]
        internal IdValue AmortizationType
        {
            get; set;
        }

        [JsonProperty("interestType")]
        internal IdValue InterestType
        {
            get; set;
        }

        [JsonProperty("interestCalculationPeriodType")]
        internal IdValue InterestCalculationPeriodType
        {
            get; set;
        }

        [JsonProperty("transactionProcessingStrategyId")]
        internal int TransactionProcessingStrategyId
        {
            get; set;
        }
    }
}
