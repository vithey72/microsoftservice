﻿namespace BNine.MBanq.Administration.Models.Transactions
{
    using Newtonsoft.Json;

    internal class SavingAccountTransactionResponse
    {
        [JsonProperty("SavingsAccountTransaction")]
        internal SavingsAccountTransaction SavingsAccountTransaction
        {
            get; set;
        }
    }

    internal class SavingsAccountTransaction
    {
        [JsonProperty("id")]
        internal int Id
        {
            get; set;
        }

        [JsonProperty("amount")]
        internal decimal Amount
        {
            get; set;
        }

        [JsonProperty("runningBalance")]
        internal decimal RunningBalance
        {
            get; set;
        }

        [JsonProperty("account")]
        internal SavingAccountInfo Account
        {
            get; set;
        }
    }

    internal class SavingAccountInfo
    {
        [JsonProperty("id")]
        internal int Id
        {
            get; set;
        }
    }
}
