﻿namespace BNine.MBanq.Administration.Models.Templates.Responses
{
    using System.Collections.Generic;
    using Newtonsoft.Json;

    internal class CountryIdOptionsResponse
    {
        [JsonProperty("address")]
        internal Address Address
        {
            get; set;
        }
    }

    internal class Address
    {
        [JsonProperty("countryIdOptions")]
        internal List<CountryIdOptionItem> CountryIdOptions
        {
            get; set;
        }

        [JsonProperty("stateProvinceIdOptions")]
        internal List<CountryIdOptionItem> StateProvinceIdOptions
        {
            get; set;
        }

        [JsonProperty("addressTypeIdOptions")]
        internal List<AddressIdOptionItem> AddressTypeIdOptions
        {
            get; set;
        }
    }

    internal class CountryIdOptionItem
    {
        [JsonProperty("id")]
        internal int Id
        {
            get; set;
        }

        [JsonProperty("name")]
        internal string Name
        {
            get; set;
        }

        [JsonProperty("active")]
        internal bool Active
        {
            get; set;
        }

        [JsonProperty("parentId")]
        public int ParentId
        {
            get; set;
        }
    }

    internal class AddressIdOptionItem
    {
        [JsonProperty("id")]
        internal int Id
        {
            get; set;
        }

        [JsonProperty("name")]
        internal string Name
        {
            get; set;
        }

        [JsonProperty("active")]
        internal bool Active
        {
            get; set;
        }

        [JsonProperty("position")]
        public int Position
        {
            get; set;
        }
    }
}
