﻿namespace BNine.MBanq.Administration.Models.Users.Requests
{
    using Newtonsoft.Json;

    internal class UpdateUserRequest
    {
        [JsonProperty("username")]
        internal string UserName
        {
            get; set;
        }

        [JsonProperty("email")]
        internal string Email
        {
            get; set;
        }

        [JsonProperty("firstname")]
        internal string FirstName
        {
            get; set;
        }

        [JsonProperty("lastname")]
        internal string LastName
        {
            get; set;
        }
    }
}
