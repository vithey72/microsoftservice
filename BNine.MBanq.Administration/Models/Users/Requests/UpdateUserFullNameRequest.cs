﻿namespace BNine.MBanq.Administration.Models.Users.Requests;

using Newtonsoft.Json;

internal class UpdateUserFullNameRequest
{
    [JsonProperty("firstname")]
    internal string FirstName
    {
        get; set;
    }

    [JsonProperty("lastname")]
    internal string LastName
    {
        get; set;
    }
}
