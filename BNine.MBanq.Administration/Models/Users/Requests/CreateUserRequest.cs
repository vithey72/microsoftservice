﻿namespace BNine.MBanq.Administration.Models.Users.Requests
{
    using System.Collections.Generic;
    using Newtonsoft.Json;

    internal class CreateUserRequest
    {
        [JsonProperty("officeId")]
        internal int OfficeId
        {
            get; set;
        }

        [JsonProperty("username")]
        internal string UserName
        {
            get; set;
        }

        [JsonProperty("email")]
        internal string Email
        {
            get; set;
        }

        [JsonProperty("roles")]
        internal IEnumerable<int> Roles
        {
            get; set;
        }

        [JsonProperty("firstname")]
        internal string FirstName
        {
            get; set;
        }

        [JsonProperty("lastname")]
        internal string LastName
        {
            get; set;
        }

        [JsonProperty("sendPasswordToEmail")]
        internal bool SendPasswordToEmail
        {
            get; set;
        }

        [JsonProperty("passwordNeverExpires")]
        internal bool PasswordNeverExpires
        {
            get; set;
        }

        [JsonProperty("isSelfServiceUser")]
        internal bool IsSelfServiceUser
        {
            get; set;
        }

        [JsonProperty("password")]
        internal string Password
        {
            get; set;
        }

        [JsonProperty("repeatPassword")]
        internal string RepeatPassword
        {
            get; set;
        }
    }
}
