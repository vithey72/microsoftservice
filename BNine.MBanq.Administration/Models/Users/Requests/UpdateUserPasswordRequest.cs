﻿namespace BNine.MBanq.Administration.Models.Users.Requests
{
    using Newtonsoft.Json;

    internal class UpdateUserPasswordRequest
    {
        [JsonProperty("firstname")]
        public string FirstName
        {
            get; set;
        }

        [JsonProperty("password")]
        internal string NewPassword
        {
            get; set;
        }

        [JsonProperty("repeatPassword")]
        internal string PasswordConfirmation
        {
            get; set;
        }

    }
}
