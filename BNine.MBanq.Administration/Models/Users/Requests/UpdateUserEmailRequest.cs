﻿namespace BNine.MBanq.Administration.Models.Users.Requests
{
    using Newtonsoft.Json;

    internal class UpdateUserEmailRequest
    {
        [JsonProperty("email")]
        internal string Email
        {
            get; set;
        }
    }
}
