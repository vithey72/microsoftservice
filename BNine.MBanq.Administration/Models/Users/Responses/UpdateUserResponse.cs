﻿namespace BNine.MBanq.Administration.Models.Users.Responses
{
    using Newtonsoft.Json;

    internal class UpdateUserResponse
    {
        [JsonProperty("officeId")]
        internal int OfficeId
        {
            get; set;
        }

        [JsonProperty("resourceId")]
        internal int ResourceId
        {
            get; set;
        }
    }
}
