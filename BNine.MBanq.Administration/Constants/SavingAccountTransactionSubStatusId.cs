﻿namespace BNine.MBanq.Administration.Constants
{
    internal static class SavingAccountTransactionSubStatusId
    {
        internal const int Blocked = 400;
    }
}
