﻿namespace BNine.MBanq.Administration
{
    using Application.Interfaces.Bank.Administration;
    using Application.Services;
    using BNine.MBanq.Administration.Services.Charges;
    using BNine.MBanq.Administration.Services.ExternalCards;
    using BNine.MBanq.Administration.Services.Tansactions;
    using Microsoft.Extensions.DependencyInjection;
    using Services.Cards;
    using Services.Clients;
    using Services.Documents;
    using Services.Identities;
    using Services.LoanProducts;
    using Services.Loans;
    using Services.SavingAccounts;
    using Services.Templates;
    using Services.Transfers;
    using Services.Users;
    using IBankIdentityService = Application.Interfaces.Bank.Administration.IBankIdentityService;
    using IBankInternalCardsService = Application.Interfaces.Bank.Administration.IBankInternalCardsService;
    using IBankLoanAccountsService = Application.Interfaces.Bank.Administration.IBankLoanAccountsService;
    using IBankSavingAccountsService = Application.Interfaces.Bank.Administration.IBankSavingAccountsService;

    public static class DependenciesBootstrapper
    {
        public static IServiceCollection AddMBanqAdministration(this IServiceCollection services)
        {
            services.AddTransient<IBankUsersService, MBanqUsersService>();
            services.AddTransient<IBankClientsService, MBanqClientsService>();
            services.AddTransient<IBankSavingAccountsService, MBanqSavingAccountsService>();
            services.AddTransient<IBankLoanProductsService, MBanqLoanProductsService>();
            services.AddTransient<IBankLoanAccountsService, MBanqLoanAccountsService>();
            services.AddTransient<IBankDocumentsService, MBanqDocumentsService>();
            services.AddTransient<IBankIdentityService, MBanqIdentityService>();
            services.AddTransient<IBankInternalCardsService, MbanqInternalCardsService>();
            services.AddTransient<IBankTemplatesService, MbanqTemplatesService>();
            services.AddTransient<IBankTransferService, MBanqTransfersService>();
            services.AddTransient<IBankExternalCardsService, ExternalCardsService>();
            services.AddTransient<IBankCardAuthorizationsService, MBanqCardAuthorizationsService>();
            services.AddTransient<IBankTransactionsService, MBanqTransactionsService>();
            services.AddTransient<IBankChargesService, MBanqChargesService>();
            services.AddTransient<IBankCurrentAccountsService, BankCurrentAccountsService>();


            return services;
        }
    }
}
