﻿namespace BNine.MBanq.Administration.Services.Tansactions
{
    using System;
    using System.Linq;
    using System.Net.Http;
    using System.Threading.Tasks;
    using BNine.Application.Interfaces.Bank.Administration;
    using BNine.Application.Models.MBanq;
    using BNine.Common;
    using BNine.MBanq.Administration.Models.Transactions;
    using BNine.MBanq.Administration.Services.Base;
    using BNine.MBanq.Infrastructure.Base;
    using BNine.Settings;
    using Microsoft.Extensions.Logging;
    using Microsoft.Extensions.Options;

    public class MBanqTransactionsService : MBanqBaseService, IBankTransactionsService
    {
        public MBanqTransactionsService(IOptions<MBanqSettings> options, IHttpClientFactory clientFactory, ILogger<MBanqTransactionsService> logger) : base(options, clientFactory, logger)
        {
        }

        public async Task<SavingAccountTransactionInfo> GetSavingAccountTransaction(int id)
        {
            var client = CreateAuthorizedClient();

            var uri = $"/graphql";

            var body = new GraphqlRequest
            {
                Query = $"query {{ SavingsAccountTransaction(id: {id}) {{ id amount runningBalance account {{ id }} }} }}"
            };

            var content = SerializationHelper.GetRequestContent(body);

            var response = await client.PostAsync(uri, content);

            await HandleErrors(response);

            var data = await SerializationHelper.GetResponseContent<GraphqlResponse<SavingAccountTransactionResponse>>(response);

            if (data.Errors.Any())
            {
                throw new Exception(data.Errors.ToString());
            }

            return new SavingAccountTransactionInfo
            {
                Id = data.Data.SavingsAccountTransaction.Id,
                AccountId = data.Data.SavingsAccountTransaction.Account.Id,
                Amount = data.Data.SavingsAccountTransaction.Amount,
                RunningBalance = data.Data.SavingsAccountTransaction.RunningBalance
            };
        }
    }
}
