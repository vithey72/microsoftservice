﻿namespace BNine.MBanq.Administration.Services.Clients
{
    using System;
    using System.Collections.Generic;
    using System.Net;
    using System.Net.Http;
    using System.Threading.Tasks;
    using Application.Interfaces.Bank.Administration;
    using Base;
    using BNine.Constants;
    using BNine.MBanq.Administration.Models.Base.Responses;
    using BNine.MBanq.Administration.Models.Users.Requests;
    using Common;
    using Enums;
    using Microsoft.Extensions.Logging;
    using Microsoft.Extensions.Options;
    using Models.Clients.Requests;
    using Models.Clients.Responses;
    using Models.Users.Responses;
    using Settings;

    public class MBanqClientsService
        : MBanqBaseService
        , IBankClientsService
    {
        private const string InsufficientAccountBalance = "error.msg.savingsaccount.insufficient.account.balance.classification.switch";
        private const string InsufficientAccountBalanceDeferred = "error.msg.savingsaccount.insufficient.account.balance.deffered.classification.switch";
        private const int OfficeId = 1;

        public MBanqClientsService(
            IOptions<MBanqSettings> options,
            IHttpClientFactory clientFactory,
            ILogger<MBanqClientsService> logger
            )
            : base(options, clientFactory, logger)
        {
        }

        public async Task<int> CreateClient(string firstName, string lastName, string phone,
            string email, DateTime dateOfBirth, string idempotencyKey = null)
        {
            var client = CreateAuthorizedClient(idempotencyKey);

            var uri = "/v1/clients";

            var body = new CreateClientRequest
            {
                OfficeId = OfficeId,
                FirstName = firstName,
                MobileNo = phone,
                LastName = lastName,
                EmailAddress = email,
                Locale = "en",
                IsActtive = false,
                DateOfBirth = dateOfBirth.ToString(DateFormat.Default),
                DateFormat = DateFormat.Default
            };

            var content = SerializationHelper.GetRequestContent(body);

            var response = await client.PostAsync(uri, content);

            await HandleErrors(response);

            var data = await SerializationHelper.GetResponseContent<CreateClientResponse>(response);

            return data.ClientId;
        }

        public async Task<int> GetClientClassificationId(int clientId)
        {
            var client = CreateAuthorizedClient();

            var uri = $"/v1/clients/{clientId}";

            var response = await client.GetAsync(uri);

            await HandleErrors(response);

            var data = await SerializationHelper.GetResponseContent<GetClientResponse>(response);

            return data.ClientClassification.Id;
        }

        public async Task<string> GetVerificationStatus(int clientId)
        {
            var client = CreateAuthorizedClient();

            var uri = $"/v1/clients/{clientId}/verificationstatus";

            var response = await client.GetAsync(uri);

            await HandleErrors(response);

            var data = await SerializationHelper.GetResponseContent<GetVerificationStatusResponse>(response);

            return data.VerificationStatus.Value;
        }

        public async Task<int> UpdateClient(int clientId, string firstName, string lastName,
            string phone, string email, DateTime dateOfBirth)
        {
            var client = CreateAuthorizedClient();

            var uri = $"/v1/clients/{clientId}";

            var body = new UpdateClientRequest
            {
                FirstName = firstName,
                LastName = lastName,
                EmailAddress = email,
                MobileNo = phone,
                Locale = "en",
                DateOfBirth = dateOfBirth.ToString(DateFormat.Default),
                DateFormat = DateFormat.Default
            };

            var content = SerializationHelper.GetRequestContent(body);

            var response = await client.PutAsync(uri, content);

            await HandleErrors(response);

            var data = await SerializationHelper.GetResponseContent<UpdateClientResponse>(response);

            return data.ClientId;
        }

        public async Task<int> UpdateClientPhoneNumber(int id, string phoneNumber)
        {
            var client = CreateAuthorizedClient();

            var uri = $"/v1/clients/{id}";

            var body = new UpdateClientPhoneNumberRequest
            {
                PhoneNumber = phoneNumber
            };

            var content = SerializationHelper.GetRequestContent(body);

            var response = await client.PutAsync(uri, content);

            await HandleErrors(response);

            var data = await SerializationHelper.GetResponseContent<UpdateClientResponse>(response);

            return data.ResourceId;
        }

        public async Task<int> UpdateClientEmail(int id, string email)
        {
            var client = CreateAuthorizedClient();

            var uri = $"/v1/clients/{id}";

            var body = new UpdateClientEmailRequest
            {
                EmailAddress = email
            };

            var content = SerializationHelper.GetRequestContent(body);

            var response = await client.PutAsync(uri, content);

            await HandleErrors(response);

            var data = await SerializationHelper.GetResponseContent<UpdateClientResponse>(response);

            return data.ResourceId;
        }

        public async Task<int> UpdateClientFullName(int id, string firstName, string lastName)
        {
            var client = CreateAuthorizedClient();

            var uri = $"/v1/clients/{id}";

            var body = new UpdateUserFullNameRequest()
            {
                FirstName = firstName,
                LastName = lastName
            };

            var content = SerializationHelper.GetRequestContent(body);

            var response = await client.PutAsync(uri, content);

            await HandleErrors(response);

            var data = await SerializationHelper.GetResponseContent<UpdateUserResponse>(response);

            return data.ResourceId;
        }

        public async Task<int> UpdateClientClassification(int clientId, int classificationId, DateTime? activationDate = null)
        {
            var client = CreateAuthorizedClient();

            var uri = $"/v1/clients/{clientId}?command=switchClassification";

            var body = new UpdateClientClassificationRequest
            {
                ClassificationId = classificationId,
            };

            if (activationDate != null)
            {
                body.ExpectedApplicableDate = activationDate;
            }

            var content = SerializationHelper.GetRequestContent(body);

            var response = await client.PostAsync(uri, content);

            if (response.StatusCode == HttpStatusCode.Forbidden)
            {
                var responseBody = await SerializationHelper.GetResponseContent<ErrorResponse>(response);

                if (responseBody.Errors.Any(e => InsufficientAccountBalance.Equals(e.UserMessageGlobalisationCode, StringComparison.OrdinalIgnoreCase)
                                                || InsufficientAccountBalanceDeferred.Equals(e.UserMessageGlobalisationCode, StringComparison.OrdinalIgnoreCase)))
                {
                    return -1;
                }
            }

            await HandleErrors(response);

            var data = await SerializationHelper.GetResponseContent<UpdateClientResponse>(response);

            return data.ResourceId;
        }

        public async Task<int> ActivateClient(int clientId)
        {
            var client = CreateAuthorizedClient();

            var uri = $"/v1/clients/{clientId}?command=activate";

            var body = new ActivateClientRequest
            {
                Locale = "en",
                ActivationDate = TimeZoneHelper.UtcToEasternStandartTime(DateTime.UtcNow).ToString(DateFormat.Default),
                DateFormat = DateFormat.Default
            };

            var content = SerializationHelper.GetRequestContent(body);

            var response = await client.PostAsync(uri, content);

            await HandleErrors(response);

            var data = await SerializationHelper.GetResponseContent<ActivateClientResponse>(response);

            return data.ClientId;
        }

        public async Task<int> LinkClientToUser(int clientId, int userId)
        {
            var client = CreateAuthorizedClient();

            var url = $"/v1/clients/{clientId}";

            var body = new LinkClientToUserRequest
            {
                Authorizations = new List<int> { userId }
            };

            var content = SerializationHelper.GetRequestContent(body);

            var response = await client.PutAsync(url, content);

            if (!response.IsSuccessStatusCode)
            {
                throw new Exception("Link client to user error");
            }

            var result = await SerializationHelper.GetResponseContent<LinkClientToUserResponse>(response);

            return result.ClientId;
        }

        public async Task ResetClientUniqueData(int clientId)
        {
            var client = CreateAuthorizedClient();

            var uri = $"/v1/clients/{clientId}";

            var body = new ResetClientUniqueDataRequest();

            var content = SerializationHelper.GetRequestContent(body);

            var updateResponse = await client.PutAsync(uri, content);
            await HandleErrors(updateResponse);
        }

        public async Task CloseClient(int clientId)
        {
            var client = CreateAuthorizedClient();

            var uri = $"/v1/clients/{clientId}?command=close";

            var body = new CloseClientRequest
            {
                Locale = Locale.English,
                ClosureDate = DateTime.UtcNow.ToString(DateFormat.Default),
                DateFormat = DateFormat.Default,
                ClosureReasonId = (int)ClientClosureReason.Other
            };

            var content = SerializationHelper.GetRequestContent(body);

            var response = await client.PostAsync(uri, content);

            await HandleErrors(response);
        }

        public async Task DeleteClient(int clientId)
        {
            var client = CreateAuthorizedClient();

            var uri = $"/v1/clients/{clientId}";

            var deleteResponse = await client.DeleteAsync(uri);
            await HandleErrors(deleteResponse);
        }
    }
}
