﻿namespace BNine.MBanq.Administration.Services.Cards
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Net.Http;
    using System.Threading.Tasks;
    using Application.Aggregates.Cards.Commands.UpdateCardLimits;
    using Application.Aggregates.Cards.Models;
    using Application.Interfaces;
    using Application.Interfaces.Bank.Administration;
    using Base;
    using Common;
    using Enums.DebitCard;
    using Infrastructure.Base;
    using MBanq.Models.Cards.Responses.CardInfo;
    using Microsoft.Extensions.Logging;
    using Microsoft.Extensions.Options;
    using Models.Cards.Requests;
    using Models.Cards.Responses;
    using Settings;

    public class MbanqInternalCardsService
        : MBanqBaseService
        , IBankInternalCardsService
    {
        private const string PurchaseControl = "PURCHASE";
        private const string WithdrawalControl = "WITHDRAWAL";

        private const string SecondsTimeUnit = "SECONDS";
        private const string DaysTimeUnit = "DAYS";
        private const string WeeksTimeUnit = "WEEKS";
        private const string MonthsTimeUnit = "MONTHS";

        private const string VelocityRuleAmountType = "AMOUNT";

        private readonly IBNineDbContext _dbContext;

        public MbanqInternalCardsService(
            IOptions<MBanqSettings> options,
            IHttpClientFactory clientFactory,
            ILogger<MbanqInternalCardsService> logger,
            IBNineDbContext dbContext
            ) : base(options, clientFactory, logger)
        {
            _dbContext = dbContext;
        }

        public async Task<int> CreateInternalCardREST(int clientId, int savingsAccountId, int productId,
            string idempotentKey = null)
        {
            var client = CreateAuthorizedClient(idempotentKey);

            var uri = "/v1/cards";

            var body = new CreateDebitCardRequest
            {
                SavingsAccountId = savingsAccountId,
                ProductId = productId,
            };

            var content = SerializationHelper.GetRequestContent(body);

            var response = await client.PostAsync(uri, content);

            await HandleErrors(response);

            var data = await SerializationHelper.GetResponseContent<GenericMBanqMutationResponse>(response);

            return data.ResourceId;
        }

        public async Task<int> EnableOnlinePayments(int cardId)
        {
            var client = CreateAuthorizedClient();

            var uri = $"/v1/cards/{cardId}?command=update";

            var body = new EnableOnlinePaymentsRequest() { OnlinePaymentEnabled = true };

            var content = SerializationHelper.GetRequestContent(body);

            var response = await client.PutAsync(uri, content);

            await HandleErrors(response);

            var data = await SerializationHelper.GetResponseContent<EnableOnlinePaymentsResponse>(response);

            return data.ResourseId;
        }

        public async Task<CardLimitsResponseModel> GetCardLimits(int cardId)
        {
            var client = CreateAuthorizedClient();

            var uri = $"/v1/cards/{cardId}";

            var response = await client.GetAsync(uri);

            await HandleErrors(response);

            var data = await SerializationHelper.GetResponseContent<CardVelocityRulesResponse>(response);

            var result = new CardLimitsResponseModel
            {
                CardOperations = new CardLimits
                {
                    DailyLimit = GetLimit(data, PurchaseControl, DaysTimeUnit),
                    WeeklyLimit = GetLimit(data, PurchaseControl, WeeksTimeUnit),
                    MonthlyLimit = GetLimit(data, PurchaseControl, MonthsTimeUnit)
                },
                CashWithdrawal = new CardLimits
                {
                    DailyLimit = GetLimit(data, WithdrawalControl, DaysTimeUnit),
                    WeeklyLimit = GetLimit(data, WithdrawalControl, WeeksTimeUnit),
                    MonthlyLimit = GetLimit(data, WithdrawalControl, MonthsTimeUnit)
                }
            };

            return result;
        }

        private decimal? GetLimit(CardVelocityRulesResponse data, string control, string timeUnit)
        {
            var rule = data.VelocityRules.FirstOrDefault(x =>
                x.Controls[0].Equals(control) && x.TimeUnit.Equals(timeUnit));
            if (rule == null)
            {
                return null;
            }

            return (decimal)rule.Value;
        }

        public async Task<int> SetCardLimits(int cardId, UpdateCardLimitsCommand request)
        {
            var client = CreateAuthorizedClient();

            var uri = $"/v1/cards/{cardId}";

            var limitsToUpdate = new List<SetVelocityRuleModel>();

            if (request.CashWithdrawal != null)
            {
                limitsToUpdate.AddRange(new List<SetVelocityRuleModel>
                {
                    GetVelocityRuleModel(WithdrawalControl, SecondsTimeUnit,
                        (int)request.CashWithdrawal.DailyLimit!.Value),
                    GetVelocityRuleModel(WithdrawalControl, DaysTimeUnit,
                        (int)request.CashWithdrawal.DailyLimit!.Value),
                    GetVelocityRuleModel(WithdrawalControl, WeeksTimeUnit,
                        (int)request.CashWithdrawal.WeeklyLimit!.Value),
                    GetVelocityRuleModel(WithdrawalControl, MonthsTimeUnit,
                        (int)request.CashWithdrawal.MonthlyLimit!.Value)
                });
            }

            if (request.CardOperations != null)
            {
                limitsToUpdate.AddRange(new List<SetVelocityRuleModel>
                {
                    GetVelocityRuleModel(PurchaseControl, SecondsTimeUnit,
                        (int)request.CardOperations.DailyLimit!.Value),
                    GetVelocityRuleModel(PurchaseControl, DaysTimeUnit,
                        (int)request.CardOperations.DailyLimit!.Value),
                    GetVelocityRuleModel(PurchaseControl, WeeksTimeUnit,
                        (int)request.CardOperations.WeeklyLimit!.Value),
                    GetVelocityRuleModel(PurchaseControl, MonthsTimeUnit,
                        (int)request.CardOperations.MonthlyLimit!.Value)
                });
            }

            var body = new SetCardLimitsRequest() { VelocityRules = limitsToUpdate };

            var content = SerializationHelper.GetRequestContent(body);

            var response = await client.PutAsync(uri, content);

            await HandleErrors(response);

            var data = await SerializationHelper.GetResponseContent<SetCardLimitsResponse>(response);

            return data.ResourceId;
        }

        public async Task TerminateClientCard(int mbanqCardId)
        {
            var client = CreateAuthorizedClient();

            var uri = $"/v1/cards/{mbanqCardId}?command=terminate";

            var response = await client.PostAsync(uri, null);

            await HandleErrors(response);
        }

        public async Task<CardInfoModel[]> GetAllCards(int externalUserId)
        {
            var client = CreateAuthorizedClient();

            var uri = $"/graphql";

            var body = new GraphqlRequest
            {
                Query =
                    $"{{ Cards(where: {{ account: {{ client: {{ id: {{EQ: {externalUserId}}} }} }} }}) {{ select {{ id status physicalCardActivated onlinePaymentEnabled primaryAccountNumber product {{id}}}}}}}}"
            };

            var content = SerializationHelper.GetRequestContent(body);

            var response = await client.PostAsync(uri, content);

            await HandleErrors(response);

            var data = await SerializationHelper.GetResponseContent<GraphqlResponse<CardInfoResponse>>(response);
            var cardProducts = _dbContext.CardProducts.ToArray();
            return data.Data.Cards.SelectedCards.Select(card => new CardInfoModel
            {
                ExternalId = card.Id,
                Status = ParseCardStatus(card.Status),
                OnlinePaymentEnabled = card.OnlinePaymentEnabled,
                PhysicalCardActivated = card.PhysicalCardActivated,
                PrimaryAccountNumber = card.PrimaryAccountNumber,
                Type = cardProducts
                    .FirstOrDefault(x => x.ExternalId == card.CardProduct.Id)
                    ?.AssociatedType ?? CardType.Unknown,
            }).ToArray();
        }

        private CardStatus ParseCardStatus(string cardStatus) =>
            Enum.TryParse<CardStatus>(cardStatus, true, out var status) ? status : CardStatus.Unknown;

        private SetVelocityRuleModel GetVelocityRuleModel(string control, string timeUnit, int dailyLimitValue)
        {
            return new SetVelocityRuleModel
            {
                Controls = new List<string> { control },
                Type = VelocityRuleAmountType,
                TimePeriod = 1,
                TimeUnit = timeUnit,
                Value = dailyLimitValue
            };
        }
    }
}
