﻿namespace BNine.MBanq.Administration.Services.Cards
{
    using System.Net.Http;
    using System.Threading.Tasks;
    using BNine.Application.Interfaces.Bank.Administration;
    using BNine.MBanq.Administration.Services.Base;
    using BNine.Settings;
    using Microsoft.Extensions.Logging;
    using Microsoft.Extensions.Options;
    using BNine.Common;
    using BNine.MBanq.Infrastructure.Base;
    using BNine.MBanq.Administration.Models.Cards.Responses;
    using System.Linq;
    using BNine.Application.Exceptions;
    using System;
    using BNine.Application.Models.MBanq;

    public class MBanqCardAuthorizationsService : MBanqBaseService, IBankCardAuthorizationsService
    {
        public MBanqCardAuthorizationsService(IOptions<MBanqSettings> options, IHttpClientFactory clientFactory, ILogger<MBanqCardAuthorizationsService> logger) : base(options, clientFactory, logger)
        {
        }

        public async Task<CardAuthorizationInfo> GetCardAuthorization(int id)
        {
            var client = CreateAuthorizedClient();

            var uri = $"/graphql";

            var body = new GraphqlRequest
            {
                Query = $"query {{ CardAuthorization(id: {id}) {{ id status, card {{ id primaryAccountNumber }} }} }}"
            };

            var content = SerializationHelper.GetRequestContent(body);

            var response = await client.PostAsync(uri, content);

            await HandleErrors(response);

            var data = await SerializationHelper.GetResponseContent<GraphqlResponse<CardAuthorizationResponse>>(response);

            if (data.Errors.Any() || data.Data == null)
            {
                if (data.Errors.Any(x => x.Message.Equals("Insufficient account balance.")))
                {
                    throw new ValidationException(nameof(data.Errors), "Insufficient account balance.");
                }

                throw new Exception(data.Errors.ToString());
            }

            return new CardAuthorizationInfo
            {
                Id = data.Data.CardAuthorization.Id,
                Status = data.Data.CardAuthorization.Status,
                Card = new CardDetails
                {
                    Id = data.Data.CardAuthorization.Card.Id,
                    PrimaryAccountNumber = data.Data.CardAuthorization.Card.PrimaryAccountNumber
                }
            };
        }
    }
}
