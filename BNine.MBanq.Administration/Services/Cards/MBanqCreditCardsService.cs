﻿namespace BNine.MBanq.Administration.Services.Cards;

using System.Net.Http;
using System.Threading.Tasks;
using Application.Interfaces.Bank.Administration;
using Base;
using Common;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Models.Cards.Requests;
using Models.Cards.Responses;
using Settings;

public class MBanqCreditCardsService
    : MBanqBaseService
    , IBankCreditCardsService
{
    public MBanqCreditCardsService(
        IOptions<MBanqSettings> options,
        IHttpClientFactory clientFactory,
        ILogger<MBanqCreditCardsService> logger
    )
        : base(options, clientFactory, logger)
    {
    }

    public async Task<(Guid id, int externalId)> CreateCreditCard(int clientId, int currentAccountId, int productId, string idempotencyKey = null)
    {
        var client = base.CreateAuthorizedClient(idempotencyKey);
        var uri = "/v1/creditcards";
        var id = Guid.NewGuid();

        var body = new CreateCreditCardRequest
        {
            ClientId = clientId,
            CurrentAccountId = currentAccountId,
            CardProductId = productId,
            NominalAnnualInterestRate = 0,
            NominalCashAdvanceInterestRate = 2,
            CashLimit = 500,
            CreditLimit = 1000,
            ExternalId = id.ToString(),
            DateTime = DateTime.Now,
        };

        var content = SerializationHelper.GetRequestContent(body);
        var response = await client.PostAsync(uri, content);

        await HandleErrors(response);

        var data = await SerializationHelper.GetResponseContent<GenericMBanqMutationResponse>(response);
        await ApproveCreditCard(data.ResourceId);
        return (id, data.ResourceId);
    }

    public async Task<(string cardNumber, decimal balance)> GetBalance(int cardId)
    {
        var client = base.CreateAuthorizedClient();
        var uri = "/v1/creditcards/{cardId}";

        var response = await client.GetAsync(uri);

        await HandleErrors(response);
        var data = await SerializationHelper.GetResponseContent<CreditCardBalanceResponse>(response);

        return (data.Last4Digits, data.CurrentBalance);
    }

    private async Task<int> ApproveCreditCard(int cardId)
    {
        var client = base.CreateAuthorizedClient();
        var uri = $"/v1/creditcards/{cardId}?command=approve";

        var body = new ApproveCreditCardRequest
        {
            DateTime = DateTime.Now,
        };

        var content = SerializationHelper.GetRequestContent(body);
        var response = await client.PostAsync(uri, content);

        await HandleErrors(response);

        var data = await SerializationHelper.GetResponseContent<GenericMBanqMutationResponse>(response);

        return data.ResourceId;
    }
}
