﻿namespace BNine.MBanq.Administration.Services.ExternalCards
{
    using System;
    using System.Net.Http;
    using System.Threading.Tasks;
    using BNine.Application.Interfaces.Bank.Administration;
    using BNine.Application.Models.MBanq;
    using BNine.Common;
    using BNine.MBanq.Administration.Models.ExternalCards.Requests;
    using BNine.MBanq.Administration.Models.ExternalCards.Responses;
    using BNine.MBanq.Administration.Services.Base;
    using BNine.Settings;
    using Microsoft.Extensions.Logging;
    using Microsoft.Extensions.Options;

    public class ExternalCardsService : MBanqBaseService, IBankExternalCardsService
    {
        public ExternalCardsService(IOptions<MBanqSettings> options, IHttpClientFactory clientFactory, ILogger<ExternalCardsService> logger) : base(options, clientFactory, logger)
        {
        }

        public async Task<string> GetPublicKey()
        {
            var client = CreateAuthorizedClient();

            var uri = $"/v1/externalCards/key";

            var response = await client.GetAsync(uri);

            await HandleErrors(response);

            var data = await SerializationHelper.GetResponseContent<GetPublicKeyResponse>(response);

            if (data == null)
            {
                throw new Exception(await response.Content?.ReadAsStringAsync());
            }

            return data.Key;
        }

        public async Task<ExternalCardDetails> GetExternalCard(int clientId, int cardId)
        {
            var client = CreateAuthorizedClient();

            var uri = $"/v1/clients/{clientId}/externalCards/{cardId}";

            var response = await client.GetAsync(uri);

            await HandleErrors(response);

            var data = await SerializationHelper.GetResponseContent<GetExternalCardResponse>(response);

            if (data == null)
            {
                throw new Exception(await response.Content?.ReadAsStringAsync());
            }

            return new ExternalCardDetails
            {
                Id = data.Id,
                ExpiryDate = data.ExpiryDate,
                LastDigits = data.LastDigits,
                Network = data.Network
            };
        }

        public async Task DeleteCard(int clientId, long externalCardId)
        {
            var client = CreateAuthorizedClient();

            var uri = $"/v1/clients/{clientId}/externalCards/{externalCardId}";

            var response = await client.DeleteAsync(uri);

            await HandleErrors(response);
        }

        public async Task<List<ExternalCardDetails>> GetExternalCards(int clientId)
        {
            var client = CreateAuthorizedClient();

            var uri = $"/v1/clients/{clientId}/externalCards";

            var response = await client.GetAsync(uri);

            await HandleErrors(response);

            var data = await SerializationHelper.GetResponseContent<List<GetExternalCardResponse>>(response);

            if (data == null)
            {
                throw new Exception(await response.Content?.ReadAsStringAsync());
            }

            return data.Select(x => new ExternalCardDetails
            {
                Id = x.Id,
                ExpiryDate = x.ExpiryDate,
                LastDigits = x.LastDigits,
                Network = x.Network,
                IsDeleted = x.IsDeleted
            }).ToList();
        }

        public async Task<List<ExternalCardOperationHistory>> GetExternalCardsHistory(int externalClientId)
        {
            var client = CreateAuthorizedClient();

            var uri = $"/v1/externalcards/operation-history?clientId={externalClientId}";

            var response = await client.GetAsync(uri);

            await HandleErrors(response);

            var data = await SerializationHelper.GetResponseContent<GetExternalCardOperationHistoryResponse>(response);

            return data.PageItems;
        }
    }
}
