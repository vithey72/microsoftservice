﻿namespace BNine.MBanq.Administration.Services.Transfers
{
    using System;
    using System.Linq;
    using System.Net.Http;
    using System.Threading.Tasks;
    using Application.Interfaces.Bank.Administration;
    using Application.Models.MBanq.Transfers;
    using Base;
    using Common;
    using Enums.Transfers;
    using Microsoft.Extensions.Logging;
    using Microsoft.Extensions.Options;
    using Models.Transfers.Responses;
    using Settings;
    using Creditor = Application.Models.MBanq.Transfers.Creditor;
    using Debtor = Application.Models.MBanq.Transfers.Debtor;

    public class MBanqTransfersService : MBanqBaseService, IBankTransferService
    {
        private const string GraphqlUri = "/graphql";

        public MBanqTransfersService(
            IOptions<MBanqSettings> options,
            IHttpClientFactory clientFactory,
            ILogger<MBanqTransfersService> logger) : base(options, clientFactory, logger)
        {
        }

        public async Task<TransferData> GetTransfer(int id)
        {
            var client = CreateAuthorizedClient();
            var uri = $"v1/transfers/{id}";
            var response = await client.GetAsync(uri);

            await HandleErrors(response);

            var data = await SerializationHelper.GetResponseContent<GetTransferResponse>(response);

            return new TransferData
            {
                Id = data.Id,
                ClientExternalId = data.ClientId,
                Amount = data.Amount,
                Created = TimeZoneHelper.EasternStandartTimeToUtc(data.ExecutedAt),
                Status = data.Status,
                Note = data.Reference?.FirstOrDefault(),
                TransferType = Enum.Parse<TransferType>(data.TransferType, true),
                Direction = Enum.Parse<TransferDirection>(data.Direction, true),
                Reference = data.Reference,
                Debtor = new Debtor
                {
                    Identifier = data.Debtor.Identifier,
                    Name = data.Debtor.Name
                },
                Creditor = new Creditor
                {
                    Identifier = data.Creditor.Identifier,
                    Name = data.Creditor.Name,
                    Address = data.Creditor.Address,
                    City = data.Creditor.City,
                    Country = data.Creditor.Country,
                    PostalCode = data.Creditor.PostalCode
                },
                CreditorAccountId = data.CreditorAccountId,
                InOrOut = data.InOrOut,
                CorrelationId = data.CorrelationId,
                SavingsAccountTransferExternalId = data.CreditorSavingsAccountTransactionId.GetValueOrDefault()
            };
        }
    }
}
