﻿namespace BNine.MBanq.Administration.Services.Templates
{
    using System.Linq;
    using System.Net.Http;
    using System.Threading.Tasks;
    using BNine.Application.Aggregates.Users.Models;
    using BNine.Application.Interfaces.Bank.Administration;
    using BNine.Common;
    using BNine.MBanq.Administration.Models.Templates.Responses;
    using BNine.MBanq.Administration.Services.Base;
    using BNine.Settings;
    using Microsoft.Extensions.Logging;
    using Microsoft.Extensions.Options;

    public class MbanqTemplatesService : MBanqBaseService, IBankTemplatesService
    {
        public MbanqTemplatesService(IOptions<MBanqSettings> options, IHttpClientFactory clientFactory, ILogger<MbanqTemplatesService> logger)
            : base(options, clientFactory, logger)
        {
        }

        public async Task<AddressParametersOptions> GetAddressParametersOptions()
        {
            var client = CreateAuthorizedClient();

            var uri = $"/v1/clients/template";

            var response = await client.GetAsync(uri);

            await HandleErrors(response);

            var data = await SerializationHelper.GetResponseContent<CountryIdOptionsResponse>(response);

            var addressParametersOptions = new AddressParametersOptions
            {
                AddressTypeIdOptions = data.Address.AddressTypeIdOptions
                .Where(x => x.Active)
                .Select(x => new AddressTypeIdOption
                {
                    Id = x.Id,
                    Name = x.Name
                })
                .ToList(),
                StateIdOptions = data.Address.StateProvinceIdOptions
                .Where(x => x.Active)
                .Select(x => new CountryIdOption
                {
                    Id = x.Id,
                    Name = x.Name,
                    ParentId = x.ParentId
                })
                .ToList()
            };

            return addressParametersOptions;
        }
    }
}
