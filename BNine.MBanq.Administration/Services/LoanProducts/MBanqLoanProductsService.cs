﻿namespace BNine.MBanq.Administration.Services.LoanProducts
{
    using System;
    using System.Net.Http;
    using System.Threading.Tasks;
    using Application.Interfaces.Bank.Administration;
    using Application.Models.MBanq;
    using Base;
    using Common;
    using Microsoft.Extensions.Logging;
    using Microsoft.Extensions.Options;
    using Models.LoanProducts.Responses;
    using Settings;

    public class MBanqLoanProductsService : MBanqBaseService, IBankLoanProductsService
    {
        public MBanqLoanProductsService(IOptions<MBanqSettings> options, IHttpClientFactory clientFactory, ILogger<MBanqLoanProductsService> logger)
            : base(options, clientFactory, logger)
        {
        }

        public async Task<LoanProduct> GetLoanProduct(int productId)
        {
            var client = CreateAuthorizedClient();

            var uri = $"/v1/loanproducts/{productId}";

            var response = await client.GetAsync(uri);

            await HandleErrors(response);

            var data = await SerializationHelper.GetResponseContent<GetLoanProductResponse>(response);

            if (data == null)
            {
                throw new Exception("Get loan product template error");
            }

            var product = new LoanProduct
            {
                AnnualInterestRate = data.AnnualInterestRate,
                AmortizationType = data.AmortizationType,
                Id = data.Id,
                InterestCalculationPeriodType = data.InterestCalculationPeriodType,
                InterestRateFrequencyType = data.InterestRateFrequencyType,
                InterestRatePerPeriod = data.InterestRatePerPeriod,
                InterestType = data.InterestType,
                NumberOfRepayments = data.NumberOfRepayments,
                Principal = data.Principal,
                RepaymentEvery = data.RepaymentEvery,
                RepaymentFrequencyType = data.RepaymentFrequencyType,
                Status = data.Status,
                TransactionProcessingStrategyId = data.TransactionProcessingStrategyId
            };

            return product;
        }
    }
}
