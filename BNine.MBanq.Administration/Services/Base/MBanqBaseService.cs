﻿namespace BNine.MBanq.Administration.Services.Base
{
    using System;
    using System.Linq;
    using System.Net;
    using System.Net.Http;
    using System.Threading.Tasks;
    using BNine.Application.Exceptions;
    using BNine.Constants;
    using Common;
    using Microsoft.Extensions.Logging;
    using Microsoft.Extensions.Options;
    using Models.Base.Responses;
    using Newtonsoft.Json;
    using Settings;

    public abstract class MBanqBaseService
    {
        private const string IdempotencyKeyHeader = "x-idempotency-key";
        private const string IdempotencyErrorMessage = "This request already processed before.";

        protected MBanqSettings Settings
        {
            get;
        }

        protected IHttpClientFactory ClientFactory
        {
            get;
        }

        protected ILogger Logger
        {
            get;
        }

        protected MBanqBaseService(IOptions<MBanqSettings> options,
            IHttpClientFactory clientFactory, ILogger logger)
        {
            Settings = options.Value;
            ClientFactory = clientFactory;
            Logger = logger;
        }

        protected HttpClient CreateAuthorizedClient(string idempotencyKey = null)
        {
            var client = ClientFactory.CreateClient(HttpClientName.MBanqAuthorized);

            if (idempotencyKey is not null) { client.DefaultRequestHeaders.Add(IdempotencyKeyHeader, idempotencyKey); }

            return client;
        }

        protected async Task HandleErrors(HttpResponseMessage response)
        {
            if (!response.IsSuccessStatusCode)
            {
                // TODO: rewrite

                var result = await response.Content.ReadAsStringAsync();

                Logger.LogError("Mbanq Integration Error response: {result}", result);

                if (response.StatusCode == HttpStatusCode.Unauthorized)
                {
                    throw new UnauthorizedAccessException();
                }

                if (response.StatusCode == HttpStatusCode.BadRequest ||
                    response.StatusCode == HttpStatusCode.Forbidden)
                {
                    var responseBody = await SerializationHelper.GetResponseContent<ErrorIdempotencyResponse>(response);

                    if (responseBody.Errors.Any())
                    {
                        throw new Exception($"MBanq: {responseBody.Errors.FirstOrDefault().DefaultUserMessage}");
                    }

                    if (response.StatusCode == HttpStatusCode.Forbidden
                            && responseBody.Status.Equals(403)
                            && responseBody.ErrorText.Equals("forbidden", StringComparison.InvariantCultureIgnoreCase)
                            && responseBody.Message.Equals(IdempotencyErrorMessage, StringComparison.InvariantCultureIgnoreCase)
                    )
                    {
                        throw new IdempotencyViolationException();
                    }

                    throw new Exception($"MBanq: {JsonConvert.SerializeObject(responseBody)}");

                }

                if (response.StatusCode == HttpStatusCode.NotFound)
                {
                    throw new NotFoundException();
                }

                throw new Exception($"MBanq: {result}");
            }
        }
    }
}
