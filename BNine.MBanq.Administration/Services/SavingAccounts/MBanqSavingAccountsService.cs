﻿namespace BNine.MBanq.Administration.Services.SavingAccounts
{
    using System;
    using System.Net.Http;
    using System.Threading.Tasks;
    using Application.Interfaces.Bank.Administration;
    using Application.Models.MBanq;
    using Base;
    using BNine.Constants;
    using BNine.MBanq.Administration.Constants;
    using Common;
    using Enums;
    using Microsoft.Extensions.Logging;
    using Microsoft.Extensions.Options;
    using Models.SavingAccounts.Requests;
    using Models.SavingAccounts.Responses;
    using Settings;

    public class MBanqSavingAccountsService : MBanqBaseService, IBankSavingAccountsService
    {
        public MBanqSavingAccountsService(IOptions<MBanqSettings> options, IHttpClientFactory clientFactory, ILogger<MBanqSavingAccountsService> logger)
            : base(options, clientFactory, logger)
        {
        }

        public async Task<int> CreateSavingAccount(int clientId, int productId)
        {
            var client = CreateAuthorizedClient();

            var uri = $"/v1/savingsaccounts";

            var body = new CreateSavingAccountRequest
            {
                ClientId = clientId,
                ProductId = productId,
                Locale = "en",
                SubmittedOnDate = TimeZoneHelper.UtcToEasternStandartTime(DateTime.UtcNow).ToString(DateFormat.Default),
                DateFormat = DateFormat.Default,
                LockinPeriodFrequency = 0,
                LockinPeriodFrequencyType = 0
            };

            var content = SerializationHelper.GetRequestContent(body);

            var response = await client.PostAsync(uri, content);

            await HandleErrors(response);

            var data = await SerializationHelper.GetResponseContent<CreateSavingAccountResponse>(response);

            return data.SavingsId;
        }

        public async Task<int> ApproveSavingAccount(int savingId)
        {
            var client = CreateAuthorizedClient();

            var uri = $"/v1/savingsaccounts/{savingId}?command=approve";

            var body = new ApproveSavingAccountRequest
            {
                Locale = "en",
                ApprovedOnDate = TimeZoneHelper.UtcToEasternStandartTime(DateTime.UtcNow).ToString(DateFormat.Default),
                DateFormat = DateFormat.Default
            };

            var content = SerializationHelper.GetRequestContent(body);

            var response = await client.PostAsync(uri, content);

            await HandleErrors(response);

            var data = await SerializationHelper.GetResponseContent<ApproveSavingAccountResponse>(response);

            return data.SavingsId;
        }

        public async Task<int> ActivateSavingAccount(int savingId)
        {
            var client = CreateAuthorizedClient();

            var uri = $"/v1/savingsaccounts/{savingId}?command=activate";

            var body = new ActivateSavingAccountRequest
            {
                Locale = "en",
                ActivatedOnDate = TimeZoneHelper.UtcToEasternStandartTime(DateTime.UtcNow).ToString(DateFormat.Default),
                DateFormat = DateFormat.Default
            };

            var content = SerializationHelper.GetRequestContent(body);

            var response = await client.PostAsync(uri, content);

            await HandleErrors(response);

            var data = await SerializationHelper.GetResponseContent<ActivateSavingAccountResponse>(response);

            return data.SavingsId;
        }

        public async Task<SavingAccountInfo> GetSavingAccountInfo(int savingId)
        {
            var client = CreateAuthorizedClient();

            var uri = $"/v1/savingsaccounts/{savingId}";

            var response = await client.GetAsync(uri);

            await HandleErrors(response);

            var data = await SerializationHelper.GetResponseContent<SavingAccountInfoResponse>(response);

            return new SavingAccountInfo
            {
                Id = data.Id,
                AccountBalance = data.Summary.AccountBalance,
                AvailableBalance = data.Summary.AvailableBalance,
                AccountNumber = data.AccountNumber,
                Currency = data.Summary.Currency.Code,
                Status = data.Status.Value,
                IsBlocked = data.SubStatus.Id == SavingAccountTransactionSubStatusId.Blocked,
                SavingsAmountOnHold = data.SavingsAmountOnHold,
                IsCardOrderingRestricted = data.IsCardOrderingRestricted,
                TotalDeposits = data.Summary.TotalDeposits
            };
        }

        public async Task EnableDebitTransfersForAccount(int savingId)
        {
            var client = CreateAuthorizedClient();

            var uri = $"/v1/savingsaccounts/{savingId}?command=updateTransferOptions";

            var content = SerializationHelper.GetRequestContent(new EnableACHDebitForAccountRequest());

            var request = new HttpRequestMessage(HttpMethod.Post, uri) { Content = content };

            var response = await client.SendAsync(request);

            await HandleErrors(response);
        }

        public async Task CloseSavingAccount(int savingAccountId, string note)
        {
            var client = CreateAuthorizedClient();

            var uri = $"/v1/savingsaccounts/{savingAccountId}?command=close";

            var body = new CloseSavingAccountRequest
            {
                Locale = Locale.English,
                ClosedOnDate = TimeZoneHelper.UtcToEasternStandartTime(DateTime.UtcNow).ToString(DateFormat.Default),
                DateFormat = DateFormat.Default,
                WithdrawBalance = true,
                PaymentTypeId = (int)PaymentType.Cash,
                PostInterestValidationOnClosure = false,
                Note = note
            };

            var content = SerializationHelper.GetRequestContent(body);

            var response = await client.PostAsync(uri, content);

            await HandleErrors(response);
        }

        public async Task BlockSavingAccount(int savingAccountId, string reason)
        {
            var client = CreateAuthorizedClient();

            var uri = $"/v1/savingsaccounts/{savingAccountId}?command=block";

            var body = new BlockSavingAccountRequest
            {
                Reason = reason
            };

            var content = SerializationHelper.GetRequestContent(body);

            var response = await client.PostAsync(uri, content);

            await HandleErrors(response);
        }

        public async Task UnblockSavingAccount(int savingAccountId)
        {
            var client = CreateAuthorizedClient();

            var uri = $"/v1/savingsaccounts/{savingAccountId}?command=unblock";

            var content = SerializationHelper.GetRequestContent(new { });

            var response = await client.PostAsync(uri, content);

            await HandleErrors(response);
        }
    }
}
