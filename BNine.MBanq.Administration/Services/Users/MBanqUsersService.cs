﻿namespace BNine.MBanq.Administration.Services.Users
{
    using System.Collections.Generic;
    using System.Net;
    using System.Net.Http;
    using System.Threading.Tasks;
    using Application.Interfaces.Bank.Administration;
    using Base;
    using BNine.Application.Exceptions.ApiResponse;
    using BNine.Constants;
    using BNine.MBanq.Administration.Models.Base.Responses;
    using Common;
    using Microsoft.Extensions.Logging;
    using Microsoft.Extensions.Options;
    using Models.Users.Requests;
    using Models.Users.Responses;
    using Settings;

    public class MBanqUsersService : MBanqBaseService, IBankUsersService
    {
        private const string PASSWORD_ALREADY_USED = "error.msg.password.already.used";

        public MBanqUsersService(IOptions<MBanqSettings> options, IHttpClientFactory clientFactory, ILogger<MBanqUsersService> logger)
            : base(options, clientFactory, logger)
        {
        }

        public async Task<int> CreateUser(string userName, string firstName, string lastName, string email, string password, int officeId = 1)
        {
            var client = CreateAuthorizedClient();

            var uri = $"/v1/users";

            var body = new CreateUserRequest
            {
                OfficeId = officeId,
                FirstName = firstName,
                LastName = lastName,
                Email = email,
                IsSelfServiceUser = true,
                PasswordNeverExpires = true,
                Roles = new List<int> { 2 },
                SendPasswordToEmail = false,
                UserName = userName,
                Password = password,
                RepeatPassword = password
            };

            var content = SerializationHelper.GetRequestContent(body);

            var response = await client.PostAsync(uri, content);

            await HandleErrors(response);

            var data = await SerializationHelper.GetResponseContent<CreateUserResponse>(response);

            return data.ResourceId;
        }

        public async Task<int> UpdateUser(int id, string userName, string firstName, string lastName, string email)
        {
            var client = CreateAuthorizedClient();

            var uri = $"/v1/users/{id}";

            var body = new UpdateUserRequest
            {
                FirstName = firstName,
                LastName = lastName,
                Email = email,
                UserName = userName
            };

            var content = SerializationHelper.GetRequestContent(body);

            var response = await client.PutAsync(uri, content);

            await HandleErrors(response);

            var data = await SerializationHelper.GetResponseContent<UpdateUserResponse>(response);

            return data.ResourceId;
        }

        public async Task<int> UpdateUser(int id, string firstName, string newPassword, string passwordConfirmation)
        {
            var client = CreateAuthorizedClient();

            var uri = $"/v1/users/{id}";

            var body = new UpdateUserPasswordRequest
            {
                FirstName = firstName,
                NewPassword = newPassword,
                PasswordConfirmation = passwordConfirmation
            };

            var content = SerializationHelper.GetRequestContent(body);

            var response = await client.PutAsync(uri, content);

            if (response.StatusCode == HttpStatusCode.Forbidden)
            {
                var responseBody = await SerializationHelper.GetResponseContent<ErrorResponse>(response);

                if (responseBody.Errors!.Any(e => PASSWORD_ALREADY_USED.Equals(e.UserMessageGlobalisationCode, StringComparison.OrdinalIgnoreCase)))
                {
                    throw new ApiResponseException(ApiResponseErrorCodes.PasswordHasBeenUsed, $"Password has been already used");
                }
            }

            await HandleErrors(response);

            var data = await SerializationHelper.GetResponseContent<UpdateUserResponse>(response);

            return data.ResourceId;
        }

        public async Task<int> UpdateUserEmail(int id, string email)
        {
            var client = CreateAuthorizedClient();

            var uri = $"/v1/users/{id}";

            var body = new UpdateUserEmailRequest
            {
                Email = email
            };

            var content = SerializationHelper.GetRequestContent(body);

            var response = await client.PutAsync(uri, content);

            await HandleErrors(response);

            var data = await SerializationHelper.GetResponseContent<UpdateUserResponse>(response);

            return data.ResourceId;
        }

        public async Task DeleteUser(int userId)
        {
            var client = CreateAuthorizedClient();

            var uri = $"v1/users/{userId}";

            var response = await client.DeleteAsync(uri);

            await HandleErrors(response);
        }
    }
}
