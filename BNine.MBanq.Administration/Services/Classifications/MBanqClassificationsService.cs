﻿namespace BNine.MBanq.Administration.Services.Classifications;

using System.Threading.Tasks;
using Application.Aggregates.TariffPlans.Models;
using Application.Interfaces.Bank.Administration;
using Base;
using BNine.Common;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Models.Classifications;
using Settings;

public class MBanqClassificationsService
    : MBanqBaseService
    , IBankClassificationsService
{
    public MBanqClassificationsService(
        IOptions<MBanqSettings> options,
        IHttpClientFactory clientFactory,
        ILogger<MBanqClassificationsService> logger
        )
        : base(options, clientFactory, logger)
    {
    }

    public async Task<UserClassificationsList> GetUserClassifications(int externalUserId, CancellationToken token)
    {
        var uri = $"/v1/clients/{externalUserId}/classifications";

        var client = CreateAuthorizedClient();

        var response = await client.GetAsync(uri, token);

        await HandleErrors(response);

        var items = await SerializationHelper.GetResponseContent<ClassificationTimeline>(response);

        var currentClassification = new UserClassificationsList.Classification(
            items.CurrentClassification.Id,
            items.CurrentClassification.Name,
            items.CurrentClassificationNextPaymentDate,
            items.CurrentClassificationStartDate);

        UserClassificationsList.Classification? upcomingClassification = items.UpcomingClassification.HasValue
            ? new UserClassificationsList.Classification(
                items.UpcomingClassification.Value.Id,
                items.UpcomingClassification.Value.Name,
                items.UpcomingClassificationStartDate)
            : null;

        return new UserClassificationsList(currentClassification, upcomingClassification);
    }
}
