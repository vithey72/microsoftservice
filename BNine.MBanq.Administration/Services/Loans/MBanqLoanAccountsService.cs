﻿namespace BNine.MBanq.Administration.Services.Loans;

using System;
using System.Linq;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using Application.Aggregates.CreditLines.Queries.GetCreditLineStats;
using Application.Interfaces.Bank.Administration;
using Base;
using BNine.Application.Interfaces;
using BNine.Constants;
using Common;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Models.Loans.Requests;
using Models.Loans.Responses;
using Settings;
using DisbursementData = Models.Loans.Requests.DisbursementItemData;
using LoanSummary = Application.Models.MBanq.LoanSummary;

public class MBanqLoanAccountsService
    : MBanqBaseService
    , IBankLoanAccountsService
{
    private const int LoanProductId = 2;

    private readonly ILoanSettingsProvider _loanSettingsProvider;

    public MBanqLoanAccountsService(
        IOptions<MBanqSettings> options,
        IHttpClientFactory clientFactory,
        ILoanSettingsProvider loanSettingsProvider,
        ILogger<MBanqLoanAccountsService> logger)
        : base(options, clientFactory, logger)
    {
        _loanSettingsProvider = loanSettingsProvider;
    }

    /// <inheritdoc />
    public async Task<int> CreateLoanAccount(
        int clientId,
        int currentAccountExternalId,
        int loanAmount,
        decimal loanBoostExpressFeeAmount,
        CancellationToken cancellationToken)
    {
        var client = CreateAuthorizedClient();

        var uri = "/v1/loans";

        var estNowString = TimeZoneHelper.UtcToEasternStandartTime(DateTime.UtcNow)
            .ToString(DateFormat.Default);

        var body = new CreateLoanAccountRequest
        {
            LoanType = "individual",
            Locale = "en",
            ClientId = clientId,
            DateFormat = DateFormat.Default,
            SubmittedOnDate = estNowString,
            ExpectedDisbursementDate = estNowString,
            Principal = loanAmount,
            ProductId = LoanProductId,
            TransactionProcessingStrategyId = 1,
            NumberOfRepayments = 1,
            RepaymentEvery = LoanHelpers.DurationInDays,
            RepaymentFrequencyType = 0,
            LoanTermFrequency = LoanHelpers.DurationInDays,
            LoanTermFrequencyType = Enums.Loans.TermFrequencyType.Days,
            LinkAccountId = currentAccountExternalId,
            AmortizationType = 1,
            InterestRatePerPeriod = 0,
            InterestCalculationPeriodType = 0,
            InterestType = 0,
            MaxOutstandingLoanBalance = loanAmount,
            PrepayLoanOnDeposits = true,
            CreateStandingInstructionAtDisbursement = true,
            DisbursementData = LoanHelpers.GetLoanDays().Select(x => new DisbursementData
            {
                Principal = loanAmount / (LoanHelpers.DurationInDays + 1),
                ExpectedDisbursementDate = x.ToString(DateFormat.Default),
            })
        };

        if (loanBoostExpressFeeAmount > 0m)
        {
            var settings = await _loanSettingsProvider.GetLoanSettings(cancellationToken);

            body.Charges = new[]
            {
                new AdditionalCharge
                {
                    ChargeId = settings.BoostExpressFeeChargeTypeId,
                    Amount = loanBoostExpressFeeAmount,
                    AddToPrincipalAmount = false,
                }
            };
        };

        var content = SerializationHelper.GetRequestContent(body);

        var response = await client.PostAsync(uri, content);

        await HandleErrors(response);

        var data = await SerializationHelper.GetResponseContent<CreateLoanAccountResponse>(response);

        return data.LoanId;
    }

    /// <inheritdoc />
    public async Task<int> ApproveLoanAccount(int loanId)
    {
        var client = CreateAuthorizedClient();

        var uri = $"/v1/loans/{loanId}?command=approve";

        var body = new ApproveLoanAccountRequest
        {
            Locale = "en",
            ApprovedOnDate = TimeZoneHelper.UtcToEasternStandartTime(DateTime.UtcNow).ToString(DateFormat.Default),
            DateFormat = DateFormat.Default
        };

        var content = SerializationHelper.GetRequestContent(body);

        var response = await client.PostAsync(uri, content);

        await HandleErrors(response);

        var data = await SerializationHelper.GetResponseContent<ApproveLoanAccountResponse>(response);

        return data.LoanId;
    }

    /// <inheritdoc />
    public async Task<int> DisburseLoanAccountToSaving(int loanId, double amount)
    {
        var client = CreateAuthorizedClient();

        var uri = $"/v1/loans/{loanId}?command=disburseToSavings";

        var body = new DisburseLoanAccountRequest
        {
            Locale = "en",
            ActualDisbursementDate = TimeZoneHelper.UtcToEasternStandartTime(DateTime.UtcNow).ToString(DateFormat.Default),
            DateFormat = DateFormat.Default,
            TransactionAmount = amount
        };

        var content = SerializationHelper.GetRequestContent(body);

        var response = await client.PostAsync(uri, content);

        await HandleErrors(response);

        var data = await SerializationHelper.GetResponseContent<DisburseLoanAccountResponse>(response);

        return data.LoanId;
    }

    public async Task<LoanSummary> GetLoan(int loanId)
    {
        var client = CreateAuthorizedClient();

        var uri = $"/v1/loans/{loanId}";

        var response = await client.GetAsync(uri);

        await HandleErrors(response);

        var data = await SerializationHelper.GetResponseContent<GetLoanResponse>(response);

        var summary = new LoanSummary
        {
            Available = data.Summary != null ? data.Summary.PrincipalOutstanding : data.ApprovedPrincipal,
            Total = data.Summary != null ? data.Summary?.PrincipalDisbursed : 0,
            PrincipalOutstandingAmount = data.Summary?.PrincipalOutstanding ?? 0,
            FeeAmount = data.Summary?.FeeChargesCharged ?? 0m,
            FeeOutstandingAmount = data.Summary?.FeeChargesOutstanding ?? 0m,
            ApprovedPrincipal = data.ApprovedPrincipal,
            Id = data.Id,
            InterestRate = data.InterestRatePerPeriod,
            InterestCharged = data.Summary?.InterestCharged,
            InterestPaid = data.Summary?.InterestPaid,
            Status = data.Status.Value,
            StatusId = data.Status.Id,
            ClosedObligationsMet = data.Status.ClosedObligationsMet,
            MaturityDate = DateHelper.GetDate(data.Timeline.ExpectedMaturityDate),
        };

        return summary;
    }
}
