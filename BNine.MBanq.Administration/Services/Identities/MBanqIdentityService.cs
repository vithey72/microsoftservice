﻿namespace BNine.MBanq.Administration.Services.Identities
{
    using System;
    using System.Net.Http;
    using System.Threading.Tasks;
    using Application.Interfaces.Bank.Administration;
    using Application.Models.MBanq;
    using Base;
    using Common;
    using Microsoft.Extensions.Logging;
    using Microsoft.Extensions.Options;
    using Models.Identities.Requests;
    using Models.Identities.Responses;
    using Settings;

    public class MBanqIdentityService : MBanqBaseService, IBankIdentityService
    {
        public MBanqIdentityService(IOptions<MBanqSettings> options, IHttpClientFactory clientFactory, ILogger<MBanqIdentityService> logger)
            : base(options, clientFactory, logger)
        {
        }

        public async Task<int> CreateIdentity(int clientId, int type, string number, string issuedBy)
        {
            var client = CreateAuthorizedClient();

            var uri = $"/v1/clients/{clientId}/identifiers";

            var body = new CreateIdentityRequest
            {
                DocumentKey = number,
                DocumentTypeId = type,
                IssuedBy = issuedBy
            };

            var content = SerializationHelper.GetRequestContent(body);

            var response = await client.PostAsync(uri, content);

            await HandleErrors(response);

            var data = await SerializationHelper.GetResponseContent<CreateIdentityResponse>(response);

            if (data == null)
            {
                throw new Exception("Get loan product template error");
            }

            return data.ResourceId;
        }

        public async Task<int> UpdateIdentity(int clientId, int identityId, int type, string number, string issuedBy)
        {
            var client = CreateAuthorizedClient();

            var uri = $"/v1/clients/{clientId}/identifiers/{identityId}";

            var body = new UpdateIdentityRequest
            {
                DocumentKey = number,
                DocumentTypeId = type,
                IssuedBy = issuedBy
            };

            var content = SerializationHelper.GetRequestContent(body);

            var response = await client.PutAsync(uri, content);

            await HandleErrors(response);

            var data = await SerializationHelper.GetResponseContent<UpdateIdentityResponse>(response);

            if (data == null)
            {
                throw new Exception("Get loan product template error");
            }

            return data.ResourceId;
        }

        public async Task DeleteIdentity(int clientId, int identityId)
        {
            var client = CreateAuthorizedClient();

            var uri = $"/v1/clients/{clientId}/identifiers/{identityId}";

            var response = await client.DeleteAsync(uri);

            await HandleErrors(response);
        }

        /// <summary>
        /// Now useless - MBanq only shows last digits.
        /// </summary>
        public async Task<MbanqIdentityModel> FetchIdentity(int clientId, int identityId)
        {
            var client = CreateAuthorizedClient();

            var uri = $"/v1/clients/{clientId}/identifiers/{identityId}";

            var response = await client.GetAsync(uri);

            await HandleErrors(response);

            return await SerializationHelper.GetResponseContent<MbanqIdentityModel>(response); ;
        }
    }
}
