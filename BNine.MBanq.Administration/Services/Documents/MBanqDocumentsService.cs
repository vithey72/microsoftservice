﻿namespace BNine.MBanq.Administration.Services.Documents
{
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Net.Http;
    using System.Net.Http.Headers;
    using System.Text.RegularExpressions;
    using System.Threading.Tasks;
    using Application.Interfaces.Bank.Administration;
    using Application.Models.MBanq;
    using Base;
    using Common;
    using Microsoft.Extensions.Logging;
    using Microsoft.Extensions.Options;
    using Models.Documents.Responses;
    using Settings;

    public class MBanqDocumentsService : MBanqBaseService, IBankDocumentsService
    {
        public MBanqDocumentsService(IOptions<MBanqSettings> options, IHttpClientFactory clientFactory, ILogger<MBanqDocumentsService> logger)
            : base(options, clientFactory, logger)
        {
        }

        public async Task<string> UploadDocument(byte[] file, string fileName, string contentType, string type, int clientIdentifierId)
        {
            var client = CreateAuthorizedClient();

            var uri = $"/v1/client_identifiers/{clientIdentifierId}/documents";


            var content = new MultipartFormDataContent();

            var fileContent = new StreamContent(new MemoryStream(file));

            fileContent.Headers.ContentType = MediaTypeHeaderValue.Parse(contentType);

            fileName = EscapeNonLetters(fileName);
            content.Add(fileContent, "file", fileName);

            content.Add(new StringContent(fileName), "name");

            content.Add(new StringContent(type), "type");

            var response = await client.PostAsync(uri, content);

            await HandleErrors(response);

            var data = await SerializationHelper.GetResponseContent<UploadDocumentResponse>(response);

            return data.Id;
        }

        public async Task<IEnumerable<DocumentListItem>> GetDocuments(int clientIdentifierId)
        {
            var client = CreateAuthorizedClient();

            var uri = $"/v1/client_identifiers/{clientIdentifierId}/documents";

            var response = await client.GetAsync(uri);

            await HandleErrors(response);

            var data = await SerializationHelper.GetResponseContent<IEnumerable<Document>>(response);

            return data.Select(x => new DocumentListItem { FileName = x.FileName, Id = x.Id, Type = x.Type });
        }

        public async Task<string> DeleteDocument(int clientIdentifierId, string documentId)
        {
            var client = CreateAuthorizedClient();

            var uri = $"/v1/client_identifiers/{clientIdentifierId}/documents/{documentId}";

            var response = await client.DeleteAsync(uri);

            await HandleErrors(response);

            return documentId;
        }

        private string EscapeNonLetters(string fileName)
        {
            var regex = new Regex("[A-z .]{1,}");
            var ms = regex.Matches(fileName);
            var values = ms.Select(m => m.Value);
            var joined = string.Join("_", values);
            return joined;
        }
    }
}
