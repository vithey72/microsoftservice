﻿namespace BNine.MBanq.Administration.Services.Charges;

using System;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Application.Interfaces;
using BNine.Application.Interfaces.Bank.Administration;
using BNine.Application.Models.MBanq;
using BNine.Common;
using BNine.Constants;
using BNine.MBanq.Administration.Models.Charges;
using BNine.MBanq.Administration.Services.Base;
using BNine.MBanq.Infrastructure.Base;
using BNine.Settings;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using IBankSavingAccountsService = Application.Interfaces.Bank.Client.IBankSavingAccountsService;

public class MBanqChargesService : MBanqBaseService, IBankChargesService
{
    private const string VipSupportChargeName = "Vip support charge";

    private readonly string[] SubscriptionChargeTypesNames = new[] { "chargeTimeType.monthlyFee", "chargeTimeType.annualFee" };
    private readonly IBNineDbContext _dbContext;
    private readonly IBankSavingAccountsService _bankSavingAccountsService;

    public MBanqChargesService(
        IOptions<MBanqSettings> options,
        IHttpClientFactory clientFactory,
        ILogger<MBanqChargesService> logger,
        IBNineDbContext dbContext,
        IBankSavingAccountsService bankSavingAccountsService
    )
        : base(options, clientFactory, logger)
    {
        _dbContext = dbContext;
        _bankSavingAccountsService = bankSavingAccountsService;
    }

    public async Task<ChargeDetails> GetChargeDetails(long id)
    {
        var client = CreateAuthorizedClient();

        var uri = $"/graphql";

        var body = new GraphqlRequest
        {
            Query = $"query {{ Charge(id: {id}) {{ id amount name minCap }} }}"
        };

        var content = SerializationHelper.GetRequestContent(body);

        var response = await client.PostAsync(uri, content);

        await HandleErrors(response);

        var data = await SerializationHelper.GetResponseContent<GraphqlResponse<GetChargeResponse>>(response);

        if (data.Errors.Any())
        {
            throw new Exception(data.Errors.ToString());
        }

        return new ChargeDetails
        {
            Amount = data.Data.Charge.Amount,
            Id = data.Data.Charge.Id,
            MinCap = data.Data.Charge.MinCap,
            Name = data.Data.Charge.Name
        };
    }

    public async Task<SavingAccountCharge> GetActiveTariffCharge(int savingsAccountId)
    {
        return await GetActiveCharge(savingsAccountId, SubscriptionChargeTypesNames);
    }

    public async Task<SavingAccountCharge> GetActiveCharge(int savingsAccountId, string[] chargesTypes = null)
    {
        return (await GetSavingAccountCharges(savingsAccountId))
            .FirstOrDefault(
                ch => ch.IsActive &&
                      (chargesTypes == null || chargesTypes.Contains(ch.ChargeTypeName)));
    }

    public async Task<List<SavingAccountCharge>> GetSavingAccountCharges(int savingId)
    {
        var client = CreateAuthorizedClient();

        var uri = $"/v1/savingsaccounts/{savingId}/charges";


        var response = await client.GetAsync(uri);

        await HandleErrors(response);

        var data = await SerializationHelper.GetResponseContent<List<GetSavingAccountChargeResponse>>(response);

        return data.Select(d => new SavingAccountCharge
        {
            Id = d.Id,
            ChargeId = d.ChargeId,
            DueDate = DateHelper.GetDate(d.DueDate),
            Amount = d.Amount,
            AmountPaid = d.AmountPaid,
            IsActive = d.IsActive,
            TotalDeferredChargeAmount = d.TotalDeferredChargeAmount,
            ChargeTypeName = d.ChargeTimeType?.Code,
        }).ToList();
    }

    public async Task<SavingAccountCharge> CreateAndCollectAccountCharge(int accountId, int chargeTypeId, DateTime dueDate, string idempotencyKey = null)
    {
        var chargeInQuestion = await GetChargeType(chargeTypeId);
        if (!await IsSufficientAccountBalance(accountId, chargeInQuestion.Amount))
        {
            return null;
        }

        var client = CreateAuthorizedClient(idempotencyKey);

        var url = $"/v1/savingsaccounts/{accountId}/charges";
        var body = new EnableChargeForSavingAccountRequest
        {
            Amount = chargeInQuestion.Amount,
            ChargeTypeId = chargeTypeId,
            DueDate = DateFormat.GetEstNow().ToString(DateFormat.MBanqFormat, DateFormat.GetEnCulture()),
        };

        var content = SerializationHelper.GetRequestContent(body);
        var response = await client.PostAsync(url, content);
        await HandleErrors(response);

        var data = await SerializationHelper.GetResponseContent<EnableChargeForSavingAccountResponse>(response);
        var chargeId = data.ResourceId;

        var success = await CollectCharge(accountId, chargeId, chargeInQuestion.Amount);
        if (!success)
        {
            await WaiveCharge(accountId, chargeId);
            return null;
        }

        // TODO: query one entity via GET /v1/savingsaccounts/{accountId}/charges/{chargeId}
        return (await GetSavingAccountCharges(accountId))
            .FirstOrDefault(c => c.Id == chargeId);
    }

    private async Task<GetSavingAccountChargeTypeResponse> GetChargeType(int chargeTypeId)
    {
        var client = CreateAuthorizedClient();

        var url = $"/v1/charges/{chargeTypeId}";

        var response = await client.GetAsync(url);

        await HandleErrors(response);

        return await SerializationHelper.GetResponseContent<GetSavingAccountChargeTypeResponse>(response);
    }

    private async Task<bool> CollectCharge(int accountId, int chargeId, decimal amount)
    {
        var client = CreateAuthorizedClient();
        var url = $"/v1/savingsaccounts/{accountId}/charges/{chargeId}?command=paycharge";
        var body = new PostChargeBaseRequest
        {
            Amount = amount,
            DueDate = DateFormat.GetEstNow().ToString(DateFormat.MBanqFormat),
        };
        try
        {
            var content = SerializationHelper.GetRequestContent(body);
            var response = await client.PostAsync(url, content);
            await HandleErrors(response);
            return true;
        }
        catch (Exception ex)
        {
            Logger.LogError(ex, $"Could not paycharge the chargeId = {chargeId} from account {accountId}.");
            return false;
        }
    }

    /// <inheritdoc />
    public async Task WaiveCharge(int accountId, int chargeId)
    {
        var client = CreateAuthorizedClient();
        var url = $"/v1/savingsaccounts/{accountId}/charges/{chargeId}?command=waive";

        var response = await client.PostAsync(url, null);
        await HandleErrors(response);
    }

    private async Task<bool> IsSufficientAccountBalance(int savingsId, decimal chargeCost)
    {
        var savingAccountInfo = await _bankSavingAccountsService.GetSavingAccountInfoAsAdmin(savingsId);
        return chargeCost <= savingAccountInfo.AvailableBalance;
    }
}
