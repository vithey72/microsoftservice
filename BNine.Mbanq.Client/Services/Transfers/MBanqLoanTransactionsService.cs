﻿namespace BNine.MBanq.Client.Services.Transfers
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Net.Http;
    using System.Threading.Tasks;
    using Application.Interfaces.Bank.Client;
    using Application.Models.MBanq;
    using Common;
    using Microsoft.Extensions.Caching.Memory;
    using Microsoft.Extensions.Logging;
    using Microsoft.Extensions.Options;
    using Models.Base;
    using Models.Transfers.Responses;
    using Services.Base;
    using Settings;

    public class MBanqLoanTransactionsService : MBanqBaseService, IBankLoanTransactionsService
    {
        public MBanqLoanTransactionsService(IOptions<MBanqSettings> options, IHttpClientFactory clientFactory, ILogger<MBanqLoanTransactionsService> logger, IMemoryCache memoryCache)
            : base(options, clientFactory, logger, memoryCache)
        {
        }

        public async Task<IEnumerable<LoanTransactionItem>> GetTransactions(int accountId, int start = 1, int limit = 10)
        {
            var client = CreateAuthorizedClient();

            var uri = $"/graphql";

            var body = new GraphqlRequest
            {
                Query = $"{{LoanTransactions(where: {{loan: {{id: {{EQ: {accountId}}}}}}}, page: {{start: {start}, limit: {limit}}}) {{tx: select {{id amount createdAt(orderBy: DESC) typeOf}}}}}}"
            };

            var content = SerializationHelper.GetRequestContent(body);

            var response = await client.PostAsync(uri, content);

            await HandleErrors(response);

            var data = await SerializationHelper.GetResponseContent<GraphqlResponse<LoanTransactions>>(response);


            var result = data.Data.LoanTransactionsItem.Tx.Select(x => new LoanTransactionItem
            {
                Amount = x.Amount,
                Date = x.CreatedAt,
                Id = x.Id,
                Type = x.TypeOf.ToString()
            });

            return result;
        }
    }
}
