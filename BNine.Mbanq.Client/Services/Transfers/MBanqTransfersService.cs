﻿namespace BNine.MBanq.Client.Services.Transfers
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.Net.Http;
    using System.Threading.Tasks;
    using Application.Interfaces.Bank.Client;
    using Application.Models.MBanq;
    using Application.Models.MBanq.Transfers;
    using BNine.Enums;
    using BNine.Enums.Transfers;
    using BNine.MBanq.Client.Models.Transfers.Responses;
    using Common;
    using Microsoft.Extensions.Caching.Memory;
    using Microsoft.Extensions.Logging;
    using Microsoft.Extensions.Options;
    using Models.Base;
    using Newtonsoft.Json;
    using Newtonsoft.Json.Converters;
    using Services.Base;
    using Settings;

    public class MBanqTransfersService : MBanqBaseService, IBankTransfersService
    {
        public MBanqTransfersService(IOptions<MBanqSettings> options, IHttpClientFactory clientFactory, ILogger<MBanqTransfersService> logger, IMemoryCache memoryCache)
            : base(options, clientFactory, logger, memoryCache)
        {
        }

        public async Task<TransferInfo> GetTransfer(int transferId, string accessToken, string ip)
        {
            var uri = $"/graphql";

            var client = CreateSelfServiceClient(accessToken, ip);

            var body = new GraphqlRequest
            {
                Query = $"{{Transfer(id: {transferId}) {{id status createdAt }}}}"
            };

            var content = SerializationHelper.GetRequestContent(body);

            var response = await client.PostAsync(uri, content);

            await HandleErrors(response);

            var data = await SerializationHelper.GetResponseContent<GraphQLResponse<GetTransferResponse>>(response);

            return new TransferInfo
            {
                Id = data.Data.Transfer.Id,
                Status = data.Data.Transfer.Status,
                DateTime = data.Data.Transfer.CreatedAt
            };
        }

        public async Task<GraphQLResponse<CreateAndSubmitTransferResponse>> CreateAndSubmitTransfer(string accessToken, string ip, Transfer transfer)
        {
            HttpClient client;

            if (transfer.Type == TransferDirection.Debit)
            {
                client = CreateAuthorizedClient();
            }
            else
            {
                if (accessToken == "referalBonus")
                {
                    client = CreateAuthorizedClient();
                }
                else
                {
                    client = CreateSelfServiceClient(accessToken, ip);
                }
            }

            var uri = $"/graphql";

            // newline symbol in reference will cause an exception in MBanq
            var reference = transfer.Reference?.Replace("\n", " ").Trim();

            var transferType = JsonConvert.SerializeObject(transfer.Type, Formatting.None, new StringEnumConverter());
            var paymentType = JsonConvert.SerializeObject(transfer.PaymentType, Formatting.None, new StringEnumConverter());

            var body = new GraphqlRequest
            {
                Query = $"mutation {{ createAndSubmitTransfer(input: {{ type: {transferType} currency: \"{transfer.Currency}\" amount: {transfer.Amount.ToString("G", CultureInfo.InvariantCulture)} "
                        + (transfer.PaymentType == TransferType.Internal ? "paymentType: \"INTERNAL\" " : $"paymentType: {paymentType} ")
                        + (!string.IsNullOrEmpty(reference) ? $"reference: \"{reference}\" " : " ")
                        + $" dateFormat: \"{transfer.DateFormat}\" debtor: {{ identifier: \"{transfer.Debtor.Identifier}\" name: \"{transfer.Debtor.Name}\" }} " +
                        $"creditor: {{ identifier: \"{transfer.Creditor.Identifier}\" {GetCreditorAccountTypeString(transfer)} name: \"{transfer.Creditor.Name}\" address: \"{transfer.Creditor.Address}\" }}}}) {{ id }}}}"
            };

            var content = SerializationHelper.GetRequestContent(body);
            var response = await client.PostAsync(uri, content);
            await HandleErrors(response);
            var data = await SerializationHelper.GetResponseContent<GraphQLResponse<CreateAndSubmitTransferResponse>>(response);
            return data;
        }

        /// <summary>
        /// Create and submit check cashing transfer.
        /// Unfortunately, GraphQL cannot be used since request body exceeds maximum.
        /// </summary>
        public async Task<CreateCheckTransferResponse> CreateCheckTransfer(string accessToken, string ip, CheckTransfer transfer)
        {
            var client = CreateAuthorizedClient();
            var dateFormatSettings = new JsonSerializerSettings { Converters = new List<JsonConverter> { new StringEnumConverter() }, };
            var body = SerializationHelper.GetRequestContent(transfer, dateFormatSettings);
            var response = await client.PostAsync("v1/transfers", body);
            if (!response.IsSuccessStatusCode)
            {
                var result = await response.Content.ReadAsStringAsync();
                throw new Exception(result);
            }

            return await SerializationHelper.GetResponseContent<CreateCheckTransferResponse>(response);
        }

        private string GetCreditorAccountTypeString(Transfer transfer)
        {
            if (transfer.PaymentType != TransferType.ACH || transfer.ReceiverAccountType == null)
            {
                return string.Empty;
            }

            return transfer.ReceiverAccountType switch
            {
                AchAccountType.Checking => WithKey("CHECKING"),
                AchAccountType.Savings => WithKey("SAVINGS"),
                _ => throw new ArgumentOutOfRangeException("Unknown account type " + transfer.ReceiverAccountType)
            };

            string WithKey(string content) => $"accountType: {content}";
        }
    }
}
