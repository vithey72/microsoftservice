﻿namespace BNine.MBanq.Client.Services.Transfers;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Application.Aggregates.CheckingAccounts.Models;
using Application.Exceptions.ApiResponse;
using Application.Helpers;
using BNine.Application.Interfaces.Bank.Client;
using BNine.Application.Models;
using BNine.Application.Models.MBanq;
using BNine.Application.Models.MBanq.Transfers;
using BNine.Common;
using BNine.Constants;
using BNine.Enums.Transfers;
using BNine.Enums.Transfers.Controller;
using BNine.MBanq.Client.Models.Base;
using BNine.MBanq.Client.Models.Transfers.Responses;
using BNine.MBanq.Client.Services.Base;
using BNine.Settings;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Counterparty = Models.Transfers.Responses.Counterparty;

public class MBanqSavingsTransactionsService : MBanqBaseService, IBankSavingsTransactionsService
{
    private const string Uri = "/graphql";

    private static readonly string SavingsAccountQuerySelectBase =
        @"{
pages
total
select
{
    id
    amount
    runningBalance
    typeOf
    subTypeOf
    account { accountNumber }
    paymentDetail
    {
        id transactionId
        paymentType { id name }
        cardAuthorization
        {
            card { primaryAccountNumber }
            transactionType
            feeAmount
            merchant
            createdAt
        }
        reference userInputReference
        creditor { identifier name address }
        debtor { identifier name address }
        }
    createdAt
    initiatedAt(orderBy: DESC)
    }
}}";

    private BNineBonusAccountSettings BonusSettings
    {
        get;
    }
    private const string PendingTransactionDefaultDescription = "Pending transaction";

    public MBanqSavingsTransactionsService(
        IOptions<BNineBonusAccountSettings> bonusSettings,
        IOptions<MBanqSettings> options,
        IHttpClientFactory clientFactory,
        ILogger<MBanqSavingsTransactionsService> logger,
        IMemoryCache memoryCache)
        : base(options, clientFactory, logger, memoryCache)
    {
        BonusSettings = bonusSettings.Value;
    }

    public async Task<PaginatedList<SavingsTransactionItem>> GetTransactions(int accountId, int start = 1, int limit = 10,
        DateTime? from = null, DateTime? to = null, IEnumerable<TransactionType> transactionTypes = null,
        string mbanqToken = null, string ip = null, GetFilteredTransfersExpandedFilter newFilter = null)
    {
        var client = CreateSelfServiceClient(mbanqToken, ip);

        var filter = $"{{account: {{id: {{EQ: {accountId}}}}}}}";

        if (from != null)
        {
            filter += $"{{initiatedAt: {{GE: \"{DateHelper.ToGraphqlQueryFormat(from.Value)}\"}}}}";
        }

        if (to != null)
        {
            filter += $"{{initiatedAt: {{LE: \"{DateHelper.ToGraphqlQueryFormat(to.Value)}\"}}}}";
        }

        if (transactionTypes != null)
        {
            if (transactionTypes.Contains(TransactionType.Card))
            {
                filter += $"{{subTypeOf: {{IN: [1, 5]}}}}";
            }
            else
            {
                // NIN & NE don't automatically include null values
                filter += $"{{OR: [{{subTypeOf: {{NIN: [1, 5]}}}} {{ subTypeOf: {{ IS_NULL: true }}}}]}}";
            }
        }

        if (newFilter != null)
        {
            if (newFilter.CompletionState == BankTransferCompletionState.Returned)
            {
                filter += $"{{OR: [{{subTypeOf: {{ EQ: 2 }} }} {{typeOf: {{ EQ: 24 }} }} ]}}";
            }
            else if (newFilter.CompletionState == BankTransferCompletionState.Completed)
            {
                filter += $"{{OR: [{{subTypeOf: {{ NE: 2 }}}} {{ subTypeOf: {{ IS_NULL: true }}}}]}}";
            }
            filter = AddNewSavingsAccountFilters(newFilter, filter);

            if (newFilter.SavingTransactionIds != null)
            {
                var idsString = string.Join(",", newFilter.SavingTransactionIds);
                filter += $"{{id: {{IN: [{idsString}]}}}}";
            }
        }

        var body = new GraphqlRequest
        {
            Query = $"{{SavingsAccountTransactions(where:{{AND:[{filter}]}}, " +
                    $"page: {{start: {start}, limit: {limit}}}) " +
                    SavingsAccountQuerySelectBase,
        };

        var content = SerializationHelper.GetRequestContent(body);

        var response = await client.PostAsync(Uri, content);

        await HandleErrors(response);

        var data = await SerializationHelper.GetResponseContent<GraphqlResponse<SavingsTransactions>>(response);

        var items = data.Data.SavingsAccountTransactions?.Items?.Select(MapFromTransactionWithType) ?? new List<SavingsTransactionItem>();

        var resultTransactions = items
            .Where(x => x != null)
            .OrderByDescending(x => x.CreatedAt)
            .Take(limit).ToList();

        return new PaginatedList<SavingsTransactionItem>(
            items: resultTransactions,
            count: (data.Data.SavingsAccountTransactions?.Total ?? 0) - resultTransactions.Count(x => x == null),
            pageNumber: start,
            pageSize: limit);
    }

    public async Task<PaginatedList<AccountStatementTransactionItem>> GetAccountStatementTransactions(int accountId, int start, int limit, DateTime from, DateTime to, string mbanqToken, string ip, bool asAdmin = false)
    {

        var client = asAdmin ? CreateAuthorizedClient() : this.CreateSelfServiceClient(mbanqToken, ip);

        var uri = $"/graphql";

        var filter = $"{{account: {{id: {{EQ: {accountId}}}}}}}";
        filter += $"{{createdAt: {{GE: \"{DateHelper.ToGraphqlQueryFormat(from)}\"}}}}";
        filter += $"{{createdAt: {{LE: \"{DateHelper.ToGraphqlQueryFormat(to)}\"}}}}";

        var body = new GraphqlRequest
        {
            Query = $"{{SavingsAccountTransactions(where:{{AND:[{filter}]}}, page: {{start: {start}, limit: {limit}}}) {{pages total select {{id(orderBy: DESC) amount createdAt runningBalance typeOf subTypeOf reversed credit debit paymentDetail {{id reference userInputReference cardAuthorization {{transactionType createdAt}} paymentType {{id name}} creditor {{identifier name address}} debtor {{identifier name address}}}}}}}}}}"
        };

        var content = SerializationHelper.GetRequestContent(body);

        var response = await client.PostAsync(uri, content);

        await this.HandleErrors(response);

        var data = await SerializationHelper.GetResponseContent<GraphqlResponse<SavingsTransactions>>(response);

        var items = data.Data.SavingsAccountTransactions?.Items?.Select(this.MapTransactionAccountStatement) ?? new List<AccountStatementTransactionItem>();

        var resultTransactions = items
            .Where(x => x != null)
            .Take(limit).ToList();

        return new PaginatedList<AccountStatementTransactionItem>(
            items: resultTransactions,
            count: (data.Data.SavingsAccountTransactions?.Total ?? 0) - resultTransactions.Count(x => x == null),
            pageNumber: start,
            pageSize: limit);
    }

    public async Task<PaginatedList<PendingTransactionItem>> GetPendingTransactions(int accountId, string mbanqToken, int start = 1, int limit = 10,
    DateTime? from = null, DateTime? to = null, bool? succeeded = null, int? paymentTypeId = null, string ip = null,
    GetFilteredTransfersExpandedFilter newFilter = null)
    {
        var client = CreateSelfServiceClient(mbanqToken, ip);
        var filter = $"{{active:{{EQ:true}}account: {{id: {{EQ: {accountId}}}}}}}";

        if (from != null)
        {
            filter += $"{{createdAt: {{GE: \"{DateHelper.ToGraphqlQueryFormat(from.Value)}\"}}}}";
        }

        if (to != null)
        {
            filter += $"{{createdAt: {{LE: \"{DateHelper.ToGraphqlQueryFormat(to.Value)}\"}}}}";
        }

        if (succeeded != null)
        {
            var value = !succeeded.Value ? "true" : "false";
            filter += $"{{ reversed: {{ EQ: {value}}} }}";
        }

        if (newFilter != null)
        {
            if (newFilter.DirectionInternal == TransferDirection.Credit ||
                newFilter.CardOrAccount == TransactionType.Account ||
                newFilter.SpecialType != null)
            {
                return new PaginatedList<PendingTransactionItem>(new List<PendingTransactionItem>(), 0, start, limit);
            }
            filter = AddNewDateFilters(newFilter, filter);
            filter = AddFilterByCardId(newFilter, filter);
        }

        var body = new GraphqlRequest
        {
            Query =
            @"
            {
                AuthorizeTransactions(
                where:{AND:[" + filter + @"]},
                page: {start: " + start + @", limit: " + limit + @"}) " +
                @"
                {
                    total
                    select
                    {
                        id
                        amount
                        createdAt(orderBy: DESC)
                        typeOf
                        cardAuthorization
                        {
                            card { primaryAccountNumber }
                            transactionType merchant createdAt
                        }
                        transfer
                        {
                            id status
                            paymentType { name }
                        }
                    }
                }
            }"
        };

        var content = SerializationHelper.GetRequestContent(body);

        var response = await client.PostAsync(Uri, content);

        await HandleErrors(response);

        var data = await SerializationHelper.GetResponseContent<GraphqlResponse<PendingTransactions>>(response);
        var authorizeTransactions = data.Data.AuthorizeTransactions.Transactions ?? Enumerable.Empty<PendingTransaction>();

        var items = authorizeTransactions.Select(t => MapFromPendingTransaction(t));

        return new PaginatedList<PendingTransactionItem>(
            items: items.Where(x => x != null).ToList(),
            count: data.Data.AuthorizeTransactions.Total - items.Count(x => x == null),
            pageNumber: start,
            pageSize: limit);
    }

    public async Task<PaginatedList<SavingsTransactionItem>> GetRejectedTransactions(
        int accountId, int start = 1, int limit = 10,
        DateTime? from = null, DateTime? to = null, string mbanqToken = null, string ip = null,
        GetFilteredTransfersExpandedFilter newFilter = null)
    {
        var client = CreateSelfServiceClient(mbanqToken, ip);
        var filter = DetermineBaseFilter(newFilter, accountId);

        if (from != null)
        {
            filter += $"{{createdAt: {{GE: \"{DateHelper.ToGraphqlQueryFormat(from.Value)}\"}}}}";
        }

        if (to != null)
        {
            filter += $"{{createdAt: {{LE: \"{DateHelper.ToGraphqlQueryFormat(to.Value)}\"}}}}";
        }

        if (newFilter != null)
        {
            if (newFilter.DirectionInternal == TransferDirection.Credit ||
                newFilter.CardOrAccount == TransactionType.Account ||
                newFilter.SpecialType != null)
            {
                return new PaginatedList<SavingsTransactionItem>(new List<SavingsTransactionItem>(), 0, start, limit);
            }
            filter = AddNewDateFilters(newFilter, filter);
        }

        var body = new GraphqlRequest
        {
            Query = $"{{CardAuthorizations(where:{{AND:[{filter}]}}, page: {{start: {start}, limit: {limit}}}) {{pages total select{{ createdAt(orderBy: DESC) id amount merchant status}}}}}}"
        };

        var content = SerializationHelper.GetRequestContent(body);

        var response = await client.PostAsync(Uri, content);

        await HandleErrors(response);

        var data = await SerializationHelper.GetResponseContent<GraphqlResponse<RejectedTransactions>>(response);

        var items = data.Data.CardAuthorizations?.Transactions?.Select(MapFromRejectedTransaction).ToList()
                    ?? new List<SavingsTransactionItem>();

        return new PaginatedList<SavingsTransactionItem>(
            items: items,
            count: (data.Data.CardAuthorizations?.Total ?? 0) - items.Count(x => x == null),
            pageNumber: start,
            pageSize: limit);
    }

    public async Task<SavingsTransactionItem> GetTransferByCorrelationId(int accountId, Guid correlationId)
    {
        var client = CreateAuthorizedClient();

        var body = new GraphqlRequest
        {
            Query =
            @"{
                SavingsAccountTransactions(
                where:{
                        AND:
                        [
                            {account:{id:{EQ:" + accountId + @"}}}
                            {paymentDetail: { correlationId:{EQ:""" + correlationId + @""" }}}
                        ]
                      }
                page:{start:1,limit:1})
                {
                    select
                    {
                        id account { id } amount debit credit runningBalance typeOf subTypeOf
                        paymentDetail { id cardAuthorization{ merchant }
                        reference userInputReference paymentType { id } creditor { identifier name address } }
                        createdAt(orderBy:DESC)
                    }
                }
              }"
        };

        var content = SerializationHelper.GetRequestContent(body);
        var response = await client.PostAsync(Uri, content);

        await HandleErrors(response);

        var data = await SerializationHelper.GetResponseContent<GraphqlResponse<SavingsTransactions>>(response);
        var transfer = data.Data.SavingsAccountTransactions.Items.FirstOrDefault();
        if (transfer == null)
        {
            return null;
        }
        return MapFromTransactionWithType(transfer);
    }

    public async Task<SavingsTransactionItem> GetSavingsAccountTransfer(int transferId, string accessToken, string ip)
    {
        var client = CreateSelfServiceClient(accessToken, ip);
        var content = SerializationHelper.GetRequestContent(
            new GraphqlRequest
            {
                Query = "{ SavingsAccountTransactions(where: {AND:[ { id: {EQ: " + transferId + "} } ]} " +
                        "page: {start: 1, limit: 1} ) " +
                        SavingsAccountQuerySelectBase,
            });

        var response = await client.PostAsync(Uri, content);
        await HandleErrors(response);
        var transfers = await SerializationHelper.GetResponseContent<GraphQLResponse<SavingsTransactions>>(response);
        var transfer = transfers.Data.SavingsAccountTransactions.Items.FirstOrDefault();
        if (transfer == null)
        {
            throw new ApiResponseException("transfer-not-found", $"No transfer of type {BankTransferCompletionState.Completed} exists under Id = {transferId}");
        }

        return MapFromTransactionWithType(transfer);
    }

    public async Task<SavingsTransactionItem[]> GetSavingsAccountTransfersArray(int[] transferIds, string accessToken, string ip)
    {
        if (transferIds.Length == 0)
        {
            return Array.Empty<SavingsTransactionItem>();
        }

        var client = CreateSelfServiceClient(accessToken, ip);
        var content = SerializationHelper.GetRequestContent(
            new GraphqlRequest
            {
                Query = "{ SavingsAccountTransactions(where: {AND:[ { id: {IN: [" + string.Join(',', transferIds) + "]} } ]} " +
                        "page: {start: 1, limit: " + transferIds.Length + " } ) " +
                        SavingsAccountQuerySelectBase,
            });

        var response = await client.PostAsync(Uri, content);
        await HandleErrors(response);
        var serializedResponse = await SerializationHelper.GetResponseContent<GraphQLResponse<SavingsTransactions>>(response);
        var transfers = serializedResponse.Data.SavingsAccountTransactions.Items.ToArray();

        return transfers.Select(MapFromTransactionWithType).ToArray();
    }

    public async Task<PendingTransactionItem> GetPendingTransfer(int transferId, string accessToken, string ip)
    {
        var client = CreateSelfServiceClient(accessToken, ip);
        var query =
            new GraphqlRequest
            {
                Query = @"{
	AuthorizeTransactions(where: { AND:
        [{
        OR: [
            { id: {EQ: " + transferId + @" } },
            {transfer: {id: { EQ: " + transferId + @"}}}
            ]},
            { active: {EQ: true}}
        ]
    }
    , page: {start: 1, limit: 1} ) {
		select {
			id
			amount
            createdAt(orderBy: DESC)
            cardAuthorization { card { primaryAccountNumber} createdAt merchant }
            typeOf
            transfer { id status debtor { name identifier } creditor { name identifier } paymentType { name } reference }
            reference
            account {  accountNumber }
		}
	}
}"
            };
        var content = SerializationHelper.GetRequestContent(query);

        var response = await client.PostAsync(Uri, content);
        await HandleErrors(response);
        var transfers = await SerializationHelper.GetResponseContent<GraphQLResponse<PendingTransactions>>(response);
        var transfer = transfers.Data.AuthorizeTransactions.Transactions.FirstOrDefault();
        if (transfer == null)
        {
            throw new ApiResponseException("transfer-not-found", $"No transfer of type {BankTransferCompletionState.Pending} exists under Id = {transferId}");
        }

        return MapFromPendingTransaction(transfer, withSign: false);
    }

    public async Task<SavingsTransactionItem> GetRejectedTransfer(int transferId, string accessToken, string ip)
    {
        var client = CreateSelfServiceClient(accessToken, ip);
        var content = SerializationHelper.GetRequestContent(
            new GraphqlRequest
            {
                Query = "{ CardAuthorizations(where: {AND:[ {id: {EQ: " + transferId + "} } {status: {EQ: REJECTED}}] } " +
                        "page: {start: 1, limit: 1} ) " +
                        "{ select { id amount createdAt(orderBy: DESC) feeAmount merchant " +
                        "card { account { accountNumber } primaryAccountNumber } status transactionType } }}",
            });

        var response = await client.PostAsync(Uri, content);
        await HandleErrors(response);
        var transfers = await SerializationHelper.GetResponseContent<GraphQLResponse<RejectedTransactions>>(response);
        var transfer = transfers.Data.CardAuthorizations.Transactions.FirstOrDefault();
        if (transfer == null)
        {
            throw new ApiResponseException("transfer-not-found", $"No transfer of type {BankTransferCompletionState.Declined} exists under Id = {transferId}");
        }

        return MapFromRejectedTransaction(transfer);
    }


    private SavingsTransactionItem MapFromRejectedTransaction(CardAuthorization transaction) =>
        transaction is null ? null : new SavingsTransactionItem
        {
            Id = transaction.Id,
            Amount = transaction.Amount ?? Decimal.Zero,
            MccCode = transaction.Merchant?.Mcc,
            CreatedAt = TimeZoneHelper.EasternStandartTimeToUtc(transaction.CreatedAt),
            InitiatedAt = TimeZoneHelper.EasternStandartTimeToUtc(transaction.CreatedAt),
            Type = TransactionTypes.REJECTED,
            Direction = TransferDirection.Debit,
            TypeId = 990,
            Debtor = new Debtor { Identifier = transaction.Merchant?.Description },
            CardDigits = transaction.Card?.PrimaryAccountNumber,
            Note = transaction.Merchant?.Description,
        };

    private AccountStatementTransactionItem MapTransactionAccountStatement(Transaction transaction)
    {
        var savingTransaction = MapFromTransactionWithType(transaction);
        return savingTransaction == null ? null : new AccountStatementTransactionItem
        {
            Id = savingTransaction.Id,
            Amount = savingTransaction.Amount,
            CreatedAt = savingTransaction.CreatedAt,
            InitiatedAt = transaction.CreatedAt,
            Type = savingTransaction.Type,
            Direction = savingTransaction.Direction,
            TypeId = savingTransaction.TypeId,
            RunningBalance = savingTransaction.RunningBalance,
            Reversed = savingTransaction.Reversed,
            Reference = transaction.PaymentDetail?.Reference,
            Creditor = savingTransaction.Creditor,
            Debtor = savingTransaction.Debtor,
        };
    }

    private PendingTransactionItem MapFromPendingTransaction(PendingTransaction transaction, bool withSign = true) =>
        transaction == null ? null : new PendingTransactionItem
        {
            Id = transaction.Transfer?.Id ?? transaction.Id, // card auths are queried by root id, AML checks by transfer object id
            Amount = withSign ? transaction.Amount * -1 : transaction.Amount,
            CreatedAt = TimeZoneHelper.EasternStandartTimeToUtc(
                transaction.CardAuthorization?.CreatedAt ?? transaction.CreatedAt),
            Type = transaction.CardAuthorization?.TransactionType ?? String.Empty,
            Merchant = transaction.CardAuthorization?.Merchant?.Description ?? PendingTransactionDefaultDescription,
            CardDigits = transaction.CardAuthorization?.Card?.PrimaryAccountNumber, // only card auths have this
            AccountNumber = transaction.Account?.AccountNumber,
            MccCode = transaction.CardAuthorization?.Merchant?.Mcc,
            Creditor = ConvertCreditor(transaction.Transfer?.Creditor),
            Debtor = ConvertDebtor(transaction.Transfer?.Debtor),
            RawPaymentType = transaction.Transfer?.PaymentType.Name,
            Reference = transaction.Transfer?.Reference,
        };

    private SavingsTransactionItem MapFromTransactionWithType(Transaction transaction)
    {
        var result = MapFromTransaction(transaction);
        if (result != null)
        {
            result.RawType = transaction.TypeOf;
            result.RawSubType = transaction.SubTypeOf;
            result.MccCode = transaction.PaymentDetail?.CardAuthorization?.Merchant?.Mcc;
            if (transaction.PaymentDetail?.CardAuthorization?.TransactionType == TransactionTypes.ATM)
            {
                result.Type = TransactionTypes.ATM;
            }
            else if (transaction.TypeOf == 24) // paycharge reversal
            {
                result.Type = TransfersMappingHelper.InferReturnedChargeName(result.Note);
            }

            result.AccountDigits = transaction.Account?.AccountNumber;
            result.CardDigits = transaction.PaymentDetail?.CardAuthorization?.Card?.PrimaryAccountNumber;
        }
        return result;
    }

    private SavingsTransactionItem MapFromTransaction(Transaction transaction)
    {
        var createdAt = TimeZoneHelper.EasternStandartTimeToUtc(transaction.CreatedAt);
        var initiatedAt = TimeZoneHelper.EasternStandartTimeToUtc(transaction.InitiatedAt);
        if (transaction.TypeOf is 1) // DEPOSIT
        {
            var baseDeposit = new SavingsTransactionItem
            {
                Id = transaction.Id,
                Amount = transaction.Amount!.Value,
                CreatedAt = createdAt,
                InitiatedAt = initiatedAt,
                Reversed = transaction.Reversed,
                RunningBalance = transaction.RunningBalance ?? decimal.Zero,
                Note = transaction.PaymentDetail?.Reference,
                UserNote = transaction.PaymentDetail?.UserInputReference,
                Direction = TransferDirection.Credit
            };

            if (transaction.SubTypeOf is 3) // LOAN DISBURSEMENT
            {
                return baseDeposit with
                {
                    Type = "Advance Disbursement",
                    TypeId = 999
                };
            }

            if (transaction.PaymentDetail?.Debtor?.Identifier != null &&
                transaction.PaymentDetail.Debtor.Identifier.StartsWith("EXTERNALCARD:")) // EXTERNALCARD replenish
            {
                return baseDeposit with
                {
                    Type = TransactionTypes.EXTERNAL_CARD,
                    TypeId = transaction.PaymentDetail.PaymentType.Id,
                    Debtor = new Debtor
                    {
                        Identifier = transaction.PaymentDetail.Debtor.Identifier,
                        Name = transaction.PaymentDetail.Debtor.Name
                    }
                };
            }

            if (transaction.PaymentDetail?.Debtor?.Identifier != null &&
                transaction.PaymentDetail.Debtor.Identifier
                    .Split(":").Last().Equals(BonusSettings.SavingsAccountId)) // REFERRAL BONUS
            {
                return baseDeposit with
                {
                    Type = transaction.PaymentDetail.PaymentType.Name,
                    TypeId = 997,
                    Note = transaction.PaymentDetail.Reference,
                };
            }

            if (transaction.SubTypeOf is 2)
            {
                return baseDeposit with
                {
                    Type = TransactionTypes.SETTLEMENT_RETURN_CREDIT,
                    TypeId = 996,
                    Note = transaction.PaymentDetail?.Reference,
                };
            }

            if (transaction.SubTypeOf is 5)
            {
                return baseDeposit with
                {
                    Type = TransactionTypes.CARD_AUTHORIZE_PAYMENT,
                    TypeId = 995,
                    Note = transaction.PaymentDetail?.Reference,
                };
            }

            return baseDeposit with
            {
                TypeId = transaction.PaymentDetail!.PaymentType.Id,
                Type = transaction.PaymentDetail.PaymentType.Name,
                Reversed = transaction.Reversed,
                Note = transaction.PaymentDetail?.Reference,
                UserNote = transaction.PaymentDetail?.UserInputReference,
                Counterparty = transaction.PaymentDetail == null
                    ? "CASH"
                    : transaction.PaymentDetail.Debtor?.Identifier,
                Debtor = ConvertDebtor(transaction.PaymentDetail?.Debtor),
                Creditor = ConvertCreditor(transaction.PaymentDetail?.Creditor),
            };
        }
        if (transaction.TypeOf is 2) // WITHDRAWAL
        {
            var withdrawalBase = new SavingsTransactionItem
            {
                Id = transaction.Id,
                Amount = transaction.Amount!.Value,
                Reversed = transaction.Reversed,
                CreatedAt = createdAt,
                InitiatedAt = initiatedAt,
                RunningBalance = transaction.RunningBalance ?? decimal.Zero,
                Reference = transaction.PaymentDetail?.Reference,
                Direction = TransferDirection.Debit
            };

            if (transaction.SubTypeOf is 4) // REPAYMENT
            {
                return withdrawalBase with
                {
                    Type = "Advance Repayment",
                    TypeId = 998
                };
            }

            if (transaction.PaymentDetail?.Creditor?.Identifier?.StartsWith("EXTERNALCARD:") == true) // PUSH TO EXTERNAL CARD
            {
                return withdrawalBase with
                {
                    Type = TransactionTypes.EXTERNAL_CARD,
                    TypeId = transaction.PaymentDetail.PaymentType.Id,
                    Creditor = new Creditor
                    {
                        Identifier = transaction.PaymentDetail.Creditor.Identifier,
                        Name = transaction.PaymentDetail.Creditor.Name
                    }
                };
            }

            if (transaction.SubTypeOf is 1) // CARD TRANSACTION
            {
                return withdrawalBase with
                {
                    Type = transaction.PaymentDetail!.Reference,
                    TypeId = transaction.PaymentDetail.PaymentType.Id,
                    Note = transaction.PaymentDetail.Reference,
                };
            }

            return withdrawalBase with
            {
                TypeId = transaction.PaymentDetail!.PaymentType.Id,
                Type = transaction.PaymentDetail.PaymentType.Name,
                Note = transaction.PaymentDetail?.Reference,
                UserNote = transaction.PaymentDetail?.UserInputReference,
                Counterparty = transaction.PaymentDetail == null
                    ? "CASH"
                    : transaction.PaymentDetail.Creditor?.Identifier,
                Debtor = ConvertDebtor(transaction.PaymentDetail?.Debtor),
                Creditor = ConvertCreditor(transaction.PaymentDetail?.Creditor),
            };
        }
        if (transaction.TypeOf is 7 or 5)
        {
            var baseFee = new SavingsTransactionItem
            {
                Id = transaction.Id,
                Amount = transaction.Amount.Value,
                Direction = TransferDirection.Debit,
                CreatedAt = createdAt,
                InitiatedAt = initiatedAt,
                Reversed = transaction.Reversed,
                RunningBalance = transaction.RunningBalance.Value,
                Reference = transaction.PaymentDetail?.Reference,
            };

            if (transaction.SubTypeOf is 6)
            {
                return baseFee with
                {
                    Type = TransactionTypes.DOMESTIC_ATM_WITHDRAWAL_FEE,
                    TypeId = 994
                };
            }

            if (transaction.SubTypeOf is 7)
            {
                return baseFee with
                {
                    Type = TransactionTypes.INTERNATIONAL_ATM_WITHDRAWAL_FEE,
                    TypeId = 993
                };
            }

            if (transaction.SubTypeOf is 8)
            {
                return baseFee with
                {
                    Type = TransactionTypes.INTERNATIONAL_TRANSACTION_FEE,
                    TypeId = 992
                };
            }

            if (transaction.SubTypeOf is 9)
            {
                return baseFee with
                {
                    Type = TransactionTypes.EXTERNAL_CARD_PUSH_TRANSACTION_FEE,
                    TypeId = 989
                };
            }

            if (transaction.SubTypeOf is 10)
            {
                return baseFee with
                {
                    Type = TransactionTypes.EXTERNAL_CARD_PULL_TRANSACTION_FEE,
                    TypeId = 988
                };
            }

            if (transaction.SubTypeOf is 13)
            {
                return baseFee with
                {
                    Type = TransactionTypes.MCC_FEE,
                    TypeId = 991
                };
            }

            var reference = transaction.PaymentDetail?.Reference;
            if (reference is TransactionTypes.VIP_SUPPORT or TransactionTypes.PHYSICAL_CARD_REPLACEMENT_FEE or TransactionTypes.PHYSICAL_CARD_EMBOSSING_FEE)
            {
                return baseFee with
                {
                    Type = reference,
                    TypeId = 991
                };
            }

            if (reference != null && reference.StartsWith(TransactionTypes.ADVANCE_EXTENSION_FEE))
            {
                return baseFee with
                {
                    Type = TransactionTypes.ADVANCE_EXTENSION_FEE,
                    TypeId = 991
                };
            }

            if (reference != null && reference.StartsWith(TransactionTypes.ADVANCE_BOOST_FEE))
            {
                return baseFee with
                {
                    Type = TransactionTypes.ADVANCE_BOOST_FEE,
                    TypeId = 991
                };
            }

            if (reference != null && reference.StartsWith(TransactionTypes.AFTER_ADVANCE_TIP))
            {
                return baseFee with
                {
                    Type = TransactionTypes.AFTER_ADVANCE_TIP,
                    TypeId = 991
                };
            }

            if (reference != null && reference.StartsWith(TransactionTypes.CREDIT_SCORE_FEE))
            {
                return baseFee with
                {
                    Type = TransactionTypes.CREDIT_SCORE_FEE,
                    TypeId = 991
                };
            }

            return baseFee with
            {
                Type = TransactionTypes.PAY_CHARGE,
                TypeId = 991
            };
        }

        if (transaction.TypeOf is 24)
        {
            return new SavingsTransactionItem
            {
                Id = transaction.Id,
                Amount = transaction.Amount.Value,
                Direction = TransferDirection.Credit,
                CreatedAt = createdAt,
                InitiatedAt = initiatedAt,
                Reversed = transaction.Reversed,
                RunningBalance = transaction.RunningBalance.Value,
                Type = TransactionTypes.REVERSED_PAYCHARGE,
                Note = transaction.PaymentDetail?.Reference
            };
        }

        return null;
    }

    private static Debtor ConvertDebtor(Counterparty debtor)
    {
        return debtor == null
            ? null
            : new Debtor
            {
                Identifier = debtor.Identifier,
                Name = debtor.Name,
            };
    }

    private static Creditor ConvertCreditor(Counterparty creditor)
    {
        return creditor == null
            ? null
            : new Creditor
            {
                Identifier = creditor.Identifier,
                Name = creditor.Name,
                Address = creditor.Address,
                City = creditor.City,
                Country = creditor.Country,
                PostalCode = creditor.PostalCode,
            };
    }

    private string AddNewSavingsAccountFilters(GetFilteredTransfersExpandedFilter newFilter, string resultingFilter)
    {
        resultingFilter = AddNewDateFilters(newFilter, resultingFilter);

        if (newFilter.Direction != null)
        {
            if (newFilter.DirectionInternal == TransferDirection.Credit)
            {
                resultingFilter += $"{{ typeOf: {{ IN: [1, 3, 8, 24 ] }} }}";
            }
            else
            {
                resultingFilter += $"{{ typeOf: {{ NIN: [1, 3, 8, 24 ] }} }}";
            }
        }
        else
        {
            if (newFilter.IsOnlyWithdrawal.HasValue && newFilter.IsOnlyWithdrawal.Value)
            {
                resultingFilter += "{ typeOf: {EQ: 2}}";
            }
        }

        if (newFilter.SpecialType != null)
        {
            if (newFilter.SpecialType == B9SpecialBankTransferType.Charge)
            {
                resultingFilter += $"{{ typeOf: {{ EQ: 7 }} }}";
            }
            else if (newFilter.SpecialType == B9SpecialBankTransferType.Advance)
            {
                resultingFilter += $"{{ typeOf: {{ IN: [1, 2] }} }} {{ subTypeOf: {{ IN: [3, 4] }}}}";
            }
            else if (newFilter.SpecialType == B9SpecialBankTransferType.Cashback)
            {
                resultingFilter += "{ typeOf: { EQ: 1 } } { OR:["
                                   + "{ paymentDetail: { reference: {LIKE: \"%"
                                   + TransactionReferenceTexts.CashbackReferenceText + "%\"} } }"
                                   + "{ paymentDetail: { userInputReference: { LIKE: \"%"
                                   + TransactionReferenceTexts.CashbackReferenceText
                                   + "%\"}}}" +
                                   "]}";
            }
        }

        if (newFilter.PaymentTypeNames != null && newFilter.PaymentTypeNames.Any())
        {
            var paymentTypeNamesString = String.Join(", ", newFilter.PaymentTypeNames);
            resultingFilter += $"{{ paymentDetail: {{ paymentType: {{ name: {{ IN: [ {paymentTypeNamesString} ]}}}} }}}}";
        }

        if (newFilter.NotIncludedSavingTransactionIds != null && newFilter.NotIncludedSavingTransactionIds.Any())
        {
            var transactionIdsString = String.Join(",", newFilter.NotIncludedSavingTransactionIds);
            resultingFilter += $"{{ id: {{ NIN:[{transactionIdsString}] }}}}";
        }

        if (newFilter.CardId.HasValue)
        {
            resultingFilter += $"{{paymentDetail: {{cardAuthorization: {{card: {{id: {{EQ:{newFilter.CardId} }}}}}}}}}}";
        }

        if (newFilter.ShowOnlyPotentialIncome)
        {
            resultingFilter += "{ typeOf: { EQ: 1 } } { paymentDetail: { paymentType: { name: { IN: [ \"ACH\", \"VISA\" ] }} }}";
        }

        return resultingFilter;
    }

    private string AddNewDateFilters(GetFilteredTransfersFilter newFilter, string resultingFilter)
    {
        if (newFilter.Amount.MinAmount != null)
        {
            resultingFilter += $"{{amount: {{GE: \"{newFilter.Amount.MinAmount.Value}\" }}}}";
        }

        if (newFilter.Amount.MaxAmount != null)
        {
            resultingFilter += $"{{amount: {{LE: \"{newFilter.Amount.MaxAmount.Value}\"}}}}";
        }

        return resultingFilter;
    }

    private string AddFilterByCardId(GetFilteredTransfersFilter newFilter, string resultingFilter)
    {
        if (newFilter.CardId.HasValue)
        {
            resultingFilter += $"{{cardAuthorization: {{card: {{id: {{EQ:{newFilter.CardId} }}}}}}}}";
        }

        return resultingFilter;
    }

    private string DetermineBaseFilter(GetFilteredTransfersFilter newFilter, int accountId)
    {
        if (newFilter.CardId.HasValue)
        {
            return $"{{ card: {{ id: {{EQ: {newFilter.CardId}}}}}}} {{ status: {{ EQ: REJECTED }} }}";
        }

        return $"{{ card: {{ account:{{ id: {{EQ: {accountId}}}}}}}}} {{ status: {{ EQ: REJECTED }} }}";
    }
}
