﻿namespace BNine.MBanq.Client.Services.ATMs
{
    using System.Globalization;
    using System.Linq;
    using System.Net.Http;
    using System.Threading.Tasks;
    using Application.Interfaces.Bank.Client;
    using BNine.Application.Aggregates.ATMs.Models;
    using BNine.Application.Models;
    using BNine.Common;
    using BNine.MBanq.Client.Models.ATMs.Responses;
    using BNine.MBanq.Client.Models.Base;
    using BNine.MBanq.Client.Services.Base;
    using BNine.Settings;
    using Microsoft.Extensions.Caching.Memory;
    using Microsoft.Extensions.Logging;
    using Microsoft.Extensions.Options;

    public class MbanqATMsService : MBanqBaseService, IBankATMsService
    {
        public MbanqATMsService(IOptions<MBanqSettings> options, IHttpClientFactory clientFactory, ILogger<MbanqATMsService> logger, IMemoryCache memoryCache)
            : base(options, clientFactory, logger, memoryCache)
        {
        }

        public async Task<PaginatedList<ATMInfoModel>> GetATMs(double longitude, double latitude, long radius, int skip = 0, int take = 20)
        {
            var client = CreateAuthorizedClient();
            var uri = $"/graphql";

            var body = new GraphqlRequest
            {
                Query = $"{{ getATM(input: {{latitude: {latitude.ToString("G", CultureInfo.InvariantCulture)} longitude: {longitude.ToString("G", CultureInfo.InvariantCulture)} radius: {radius} }} page: {{start: {skip} limit: {take} }}) {{ total select {{ name isDepositAvailable isAvailable24Hours isHandicappedAccessible locationDescription logoName network location {{ address coordinates }}}}}}}}"
            };

            var content = SerializationHelper.GetRequestContent(body);

            var response = await client.PostAsync(uri, content);

            await HandleErrors(response);

            var data = await SerializationHelper.GetResponseContent<GraphqlResponse<GetATMsResponse>>(response);

            return new PaginatedList<ATMInfoModel>(
                items: data.Data.ATMsInfo.Items.Select(x => new ATMInfoModel
                {
                    Name = x.Name,
                    IsDepositAvailable = x.IsDepositAvailable,
                    IsAvailable24Hours = x.IsAvailable24Hours,
                    IsHandicappedAccessible = x.IsHandicappedAccessible,
                    LocationDescription = x.LocationDescription,
                    LogoName = x.LogoName,
                    Network = x.Network,
                    Location = new Application.Aggregates.ATMs.Models.ATMLocation
                    {
                        Address = new Application.Aggregates.ATMs.Models.ATMAddress
                        {
                            City = x.Location.Address.City,
                            AddressLine1 = x.Location.Address.AddressLine1,
                            AddressLine2 = x.Location.Address.AddressLine2,
                            State = x.Location.Address.State,
                            PostalCode = x.Location.Address.PostalCode,
                            Country = x.Location.Address.Country
                        },
                        Coordinates = new Application.Aggregates.ATMs.Models.ATMCoordinates
                        {
                            Longitude = x.Location.Coordinates.Longitude,
                            Latitude = x.Location.Coordinates.Latitude
                        }
                    }
                }).ToList(),
                count: data.Data.ATMsInfo.Total,
                pageNumber: skip / take + 1,
                pageSize: take);
        }
    }
}
