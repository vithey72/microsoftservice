﻿namespace BNine.MBanq.Client.Services.Loans
{
    using System;
    using System.Globalization;
    using System.Linq;
    using System.Net.Http;
    using System.Threading.Tasks;
    using BNine.Application.Exceptions;
    using BNine.Application.Interfaces.Bank.Client;
    using BNine.Application.Models.MBanq;
    using BNine.Common;
    using BNine.Constants;
    using BNine.MBanq.Client.Models.Base;
    using BNine.MBanq.Client.Models.Loans;
    using BNine.MBanq.Client.Services.Base;
    using BNine.Settings;
    using Microsoft.Extensions.Caching.Memory;
    using Microsoft.Extensions.Logging;
    using Microsoft.Extensions.Options;
    using Newtonsoft.Json;

    public class MBanqLoanAccountsService
        : MBanqBaseService, IBankLoanAccountsService
    {
        public MBanqLoanAccountsService(
            IOptions<MBanqSettings> options,
            IHttpClientFactory clientFactory,
            ILogger<MBanqLoanAccountsService> logger,
            IMemoryCache memoryCache)
            : base(options, clientFactory, logger, memoryCache)
        {
        }

        public async Task MakeRepaymentLoanAccount(string accessToken, string ip, int loanId, decimal amount)
        {
            var client = CreateSelfServiceClient(accessToken, ip);

            var uri = $"/graphql";

            var body = new GraphqlRequest
            {
                Query = $"mutation {{makeLoanRepaymentFromLinkedAccount(loanId: {loanId}, input:{{transactionDate: \"{TimeZoneHelper.UtcToEasternStandartTime(DateTime.UtcNow).ToString(DateFormat.Default)}\" transactionAmount: {amount.ToString("N", CultureInfo.CreateSpecificCulture("en-us"))} dateFormat: \"{DateFormat.Default}\" locale: \"en\"}}) {{loanId}}}}"
            };

            var content = SerializationHelper.GetRequestContent(body);

            var response = await client.PostAsync(uri, content);

            await HandleErrors(response);

            var data = await SerializationHelper.GetResponseContent<GraphqlResponse<MakeRepaymentLoanAccountResponse>>(response);

            if (data.Errors.Any())
            {
                if (data.Errors.Any(x => x.Message.Equals("Insufficient account balance.")))
                {
                    throw new ValidationException(nameof(data.Errors), "Insufficient account balance.");
                }

                Logger.LogError("An error occurred while making a loan repayment: {ex}", JsonConvert.SerializeObject(data.Errors));
                throw new Exception(string.Join(",", data.Errors.Select(e => e.Message)));
            }
        }

        public async Task<LoanAccountDetails> GetLoan(string accessToken, string ip, int loanId)
        {
            var client = CreateSelfServiceClient(accessToken, ip);

            var uri = $"/graphql";

            var body = new GraphqlRequest
            {
                Query = $"query {{ Loan(id: {loanId}) {{ id closed disbursedAmount summary {{ totalOutstanding}} }} }}"
            };

            var content = SerializationHelper.GetRequestContent(body);

            var response = await client.PostAsync(uri, content);

            await HandleErrors(response);

            var data = await SerializationHelper.GetResponseContent<GraphqlResponse<GetLoadResponse>>(response);

            if (data.Errors.Any())
            {
                if (data.Errors.Any(x => x.Message.Equals("Insufficient account balance.")))
                {
                    throw new ValidationException(nameof(data.Errors), "Insufficient account balance.");
                }

                Logger.LogError("An error occurred while getting a loan: {ex}", JsonConvert.SerializeObject(data.Errors));
                throw new Exception(string.Join(",", data.Errors.Select(e => e.Message)));
            }

            return new LoanAccountDetails
            {
                Closed = data.Data.Loan.Closed,
                DisbursedAmount = data.Data.Loan.DisbursedAmount,
                ExternalId = data.Data.Loan.Id,
                OutstandingAmount = data.Data.Loan.Summary.TotalOutstanding
            };
        }
    }
}
