﻿namespace BNine.MBanq.Client.Services.Authorization
{
    using System;
    using System.Net.Http;
    using System.Threading.Tasks;
    using Application.Models.MBanq;
    using Base;
    using BNine.Application.Interfaces.Bank.Client;
    using BNine.Constants;
    using Microsoft.Extensions.Caching.Memory;
    using Microsoft.Extensions.Logging;
    using Microsoft.Extensions.Options;
    using Settings;

    [Obsolete]
    public class MBanqAuthorizationService : MBanqBaseService, IBankAuthorizationService
    {
        public MBanqAuthorizationService(IOptions<MBanqSettings> options, IHttpClientFactory clientFactory, ILogger<MBanqAuthorizationService> logger, IMemoryCache memoryCache)
            : base(options, clientFactory, logger, memoryCache)
        {
        }

        public async Task<AuthorizationTokensResponseModel> GetTokens(string userName, string password)
        {
            var httpClient = ClientFactory.CreateClient(HttpClientName.MBanqAuthorized);

            var tokens = await GetAuthorizationTokens(httpClient, userName, password);

            return tokens;
        }

        public async Task<AuthorizationTokensResponseModel> GetTokensByRefreshToken(string mbanqRefreshToken, string userId)
        {
            var httpClient = ClientFactory.CreateClient(HttpClientName.MBanqAuthorized);

            var tokens = await GetAuthorizationTokensByRefreshToken(httpClient, mbanqRefreshToken, userId);

            return tokens;
        }
    }
}
