﻿namespace BNine.MBanq.Client.Services.ExternalCards
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Net.Http;
    using System.Threading.Tasks;
    using BNine.Application.Interfaces.Bank.Client;
    using BNine.Application.Models.MBanq;
    using BNine.Common;
    using BNine.MBanq.Client.Models.Base;
    using BNine.MBanq.Client.Models.ExternalCards.Responses;
    using BNine.Settings;
    using Microsoft.Extensions.Caching.Memory;
    using Microsoft.Extensions.Logging;
    using Microsoft.Extensions.Options;

    public class MBanqClientExternalCardsService : Services.Base.MBanqBaseService, IBankClientExternalCardsService
    {
        public MBanqClientExternalCardsService(IOptions<MBanqSettings> options,
            IHttpClientFactory clientFactory,
            ILogger<MBanqClientExternalCardsService> logger,
            IMemoryCache memoryCache) :
            base(options, clientFactory, logger, memoryCache)
        {
        }

        public async Task<IEnumerable<ExternalCardListItem>> GetCards(string mbanqToken, string ip, IEnumerable<int> ids)
        {
            if (ids == null || !ids.Any())
            {
                return new List<ExternalCardListItem>();
            }

            var client = CreateSelfServiceClient(mbanqToken, ip);

            var uri = $"/graphql";

            var start = 1;
            var limit = 1;

            var cards = new List<ExternalCardListItem>();

            while (true)
            {
                var idsFilter = $" where:{{ OR:{{ {string.Join(" ", ids.Select(x => $"id: {{ EQ: {x}}}"))} }} }}";

                var body = new GraphqlRequest { Query = $"{{ExternalCards(page:{{start: {start} limit: {limit}}}{idsFilter}){{ pages total select{{ id, details {{ lastDigits }} }} }} }}" };

                var content = SerializationHelper.GetRequestContent(body);

                var response = await client.PostAsync(uri, content);

                await HandleErrors(response);

                var data = await SerializationHelper.GetResponseContent<GraphqlResponse<ExternalCardsResponseContainer>>(response);

                cards.AddRange(data.Data.ExternalCardsRasponse.Select.Select(x => new ExternalCardListItem
                {
                    ExternalId = x.Id,
                    Last4Digits = x.Details.LastDigits.ToString("D4")
                }));

                start += limit;

                if (cards.Count == data.Data.ExternalCardsRasponse.Total)
                {
                    break;
                }
            }

            return cards;
        }

        public async Task<IEnumerable<ExternalCardListItem>> GetAllUserCards(string mbanqToken, string ip)
        {
            var client = CreateSelfServiceClient(mbanqToken, ip);

            var uri = $"/graphql";

            var body = new GraphqlRequest { Query = $"{{ExternalCards(page:{{start: 1 limit: 100}}){{ pages total select{{ id, details {{ lastDigits, networkName }} }} }} }}" };

            var content = SerializationHelper.GetRequestContent(body);

            var response = await client.PostAsync(uri, content);

            await HandleErrors(response);

            var data = await SerializationHelper.GetResponseContent<GraphqlResponse<ExternalCardsResponseContainer>>(response);

            var cards = data.Data.ExternalCardsRasponse.Select.Select(x => new ExternalCardListItem
            {
                ExternalId = x.Id,
                Last4Digits = x.Details.LastDigits.ToString("D4"),
                NetworkName = x.Details.NetworkName
            });

            return cards;
        }

        public async Task DeleteCard(long externalId, int clientId, string ip, string accessToken)
        {
            var client = CreateSelfServiceClient(accessToken, ip);

            var uri = $"/graphql";

            var body = new GraphqlRequest
            {
                Query = $"mutation {{deleteExternalCard(input:{{clientId: {clientId} externalId: {externalId} }}) {{clientId}}}}"
            };

            var content = SerializationHelper.GetRequestContent(body);

            var response = await client.PostAsync(uri, content);

            await HandleErrors(response);

            var data = await SerializationHelper.GetResponseContent<GraphqlResponse<object>>(response);

            if (data?.Errors?.Any() == true)
            {
                throw new Exception(data.Errors.First().Message);
            }
        }
    }
}
