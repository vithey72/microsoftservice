﻿namespace BNine.MBanq.Client.Services.KYC
{
    using System;
    using System.Net.Http;
    using System.Threading.Tasks;
    using Application.Interfaces.Bank.Client;
    using Base;
    using Common;
    using BNine.MBanq.Client.Models.Base;
    using BNine.MBanq.Client.Models.KYC.Responses;
    using BNine.Settings;
    using Microsoft.Extensions.Logging;
    using Microsoft.Extensions.Options;
    using Newtonsoft.Json;
    using System.Linq;
    using Microsoft.Extensions.Caching.Memory;

    public class MBanqKYCService : MBanqBaseService, IBankKYCService
    {
        public MBanqKYCService(IOptions<MBanqSettings> options, IHttpClientFactory clientFactory, ILogger<MBanqKYCService> logger, IMemoryCache memoryCache)
            : base(options, clientFactory, logger, memoryCache)
        {
        }

        public async Task<string> StartKYC(int clientId)
        {
            var client = CreateAuthorizedClient();

            var uri = $"/graphql";

            var body = new GraphqlRequest
            {
                Query = $"mutation {{ initiateClientVerification(id: {clientId})}}"
            };

            var content = SerializationHelper.GetRequestContent(body);

            var response = await client.PostAsync(uri, content);

            var data = await SerializationHelper.GetResponseContent<GraphqlResponse<StartKYCResponse>>(response);

            if (data.Errors.Any())
            {
                throw new Exception(JsonConvert.SerializeObject(data.Errors));
            }

            await HandleErrors(response);

            return data.Data.InitiateClientVerification;
        }
    }
}
