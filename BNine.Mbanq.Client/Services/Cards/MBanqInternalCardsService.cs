﻿namespace BNine.MBanq.Client.Services.Cards
{
    using System;
    using System.Linq;
    using System.Net.Http;
    using System.Threading.Tasks;
    using Application.Aggregates.Cards.Models;
    using Application.Helpers;
    using Application.Interfaces;
    using Application.Interfaces.Bank.Client;
    using BNine.Application.Exceptions;
    using BNine.Enums;
    using BNine.Enums.DebitCard;
    using Common;
    using MBanq.Models.Cards.Responses.CardInfo;
    using Microsoft.Extensions.Caching.Memory;
    using Microsoft.Extensions.Logging;
    using Microsoft.Extensions.Options;
    using Models.Base;
    using Models.Cards.Responses.CardImage;
    using Models.Cards.Responses.GetChangePinUrl;
    using Models.Cards.Responses.PhysicalCardActivation;
    using Models.Cards.Responses.Replace;
    using Models.Cards.Responses.ShippingHistory;
    using Newtonsoft.Json;
    using Services.Base;
    using Settings;
    using CardStatus = BNine.Enums.DebitCard.CardStatus;

    public class MBanqInternalCardsService
        : MBanqBaseService
        , IBankInternalCardsService
    {
        private static readonly string EXCEEDED_REPLACEMENT_LIMIT_KEY = "card.action.reach.limit";
        private static readonly string CARD_STATUS_INVALID = "card.status.invalid";
        private static readonly string CARD_SUB_STATUS_INVALID = "card.sub.status.invalid";

        private readonly IPartnerProviderService _partnerProvider;
        private readonly IBNineDbContext _dbContext;

        public MBanqInternalCardsService(
            IOptions<MBanqSettings> options,
            IHttpClientFactory clientFactory,
            ILogger<MBanqInternalCardsService> logger,
            IMemoryCache memoryCache,
            IPartnerProviderService partnerProvider,
            IBNineDbContext dbContext
            )
            : base(options, clientFactory, logger, memoryCache)
        {
            _partnerProvider = partnerProvider;
            _dbContext = dbContext;
        }

        public async Task<string> GetCardImageUrl(string mbanqToken, string ip, int cardId)
        {
            var client = CreateSelfServiceClient(mbanqToken, ip);

            var uri = $"/graphql";

            var body = new GraphqlRequest { Query = $"{{getCardImageUrl(id:{cardId})}}" };

            var content = SerializationHelper.GetRequestContent(body);

            var response = await client.PostAsync(uri, content);

            await HandleErrors(response);

            var data = await SerializationHelper.GetResponseContent<GraphqlResponse<CardImageUrlResponse>>(response);

            return data.Data.ImageUrl;
        }

        public async Task<ShippingHistoryModel> GetCardShippingHistory(string mbanqToken, string ip, int cardId)
        {
            var client = CreateSelfServiceClient(mbanqToken, ip);

            var uri = $"/graphql";

            var body = new GraphqlRequest
            {
                Query =
                    $"{{ Cards(where: {{ id: {{EQ: {cardId}}} }}) {{ select {{ id shippedOn shippingHistory {{ priority version id shippedBy address status trackingId embossedName orderDate }}}}}}}}"
            };

            var content = SerializationHelper.GetRequestContent(body);

            var response = await client.PostAsync(uri, content);

            await HandleErrors(response);

            var data = await SerializationHelper.GetResponseContent<GraphqlResponse<ShippingHistoryResponse>>(response);

            // Case for virtual cards. Now there is no shipping history for virtual card.
            if (!data.Data.Cards.Select[0].ShippingHistory.Any())
            {
                return new ShippingHistoryModel()
                {
                    Status = CardShippingStatus.IN_TRANSIT,
                    ShippedOn = data.Data.Cards.Select[0].ShippedOn
                };
            }

            var result = new ShippingHistoryModel()
            {
                ShippedBy = data.Data.Cards.Select[0].ShippingHistory[0].ShippedBy,
                Status = Enum.TryParse(data.Data.Cards.Select[0].ShippingHistory[0].Status, true, out CardShippingStatus statusValue) ? statusValue : CardShippingStatus.IN_TRANSIT,
                TrackingId = data.Data.Cards.Select[0].ShippingHistory[0].TrackingId,
                ShippedOn = data.Data.Cards.Select[0].ShippedOn
            };

            return result;
        }

        public async Task<string> ActivatePhysicalCard(string mbanqToken, string ip, int cardId)
        {
            var client = CreateSelfServiceClient(mbanqToken, ip);

            var uri = $"/graphql";

            var body = new GraphqlRequest
            {
                Query =
                    $"mutation {{ activatePhysicalCard(id: {cardId}) {{ id status }}}}"
            };

            var content = SerializationHelper.GetRequestContent(body);

            var response = await client.PostAsync(uri, content);

            await HandleErrors(response);

            var data =
                await SerializationHelper.GetResponseContent<GraphqlResponse<ActivatePhysicalCardResponse>>(response);

            if (data.Errors.Any(x => x.Extensions?.GlobalisationMessageCode == CARD_STATUS_INVALID))
            {
                if (data.Errors.Any(x => x.Message == "Card is not shipped yet, cannot activate physical card"))
                {
                    throw new ValidationException("card",
                        "Card is not shipped yet, cannot activate physical card. " +
                        "If this problem persists for longer than a day, please get in touch with our " +
                        $"Customer Support team at {StringProvider.GetSupportEmail(_partnerProvider)}.");
                }

                throw new ValidationException("card", "Card activation failed. Physical card has already been activated.");
            }

            if (data.Errors.Any())
            {
                throw new Exception(JsonConvert.SerializeObject(data.Errors));
            }

            return data.Data.ActivatePhysicalCard.Status;
        }

        public async Task<int> ReplaceCard(string mbanqToken, string ip, int cardId, string reason)
        {
            var client = CreateSelfServiceClient(mbanqToken, ip);

            var uri = $"/graphql";

            var body = new GraphqlRequest
            {
                Query =
                    $"mutation {{ replaceCard(id: {cardId}, input: {{ replaceReason: {reason} }}) {{ id status }}}}"
            };

            var content = SerializationHelper.GetRequestContent(body);

            var response = await client.PostAsync(uri, content);

            await HandleErrors(response);

            var data = await SerializationHelper.GetResponseContent<GraphqlResponse<ReplaceCardResponse>>(response);

            if (data.Errors.Any(x => x.Extensions?.GlobalisationMessageCode == EXCEEDED_REPLACEMENT_LIMIT_KEY))
            {
                throw new ValidationException("ReplacementCardLimit", "Card order failed. The card production limit has been exhausted.");
            }

            if (data.Errors.Any(x => x.Extensions?.GlobalisationMessageCode == CARD_SUB_STATUS_INVALID))
            {
                if (data.Errors.Any(x => x.Message == "Card is already being replacing"))
                {
                    throw new ValidationException("DamagedCardBeingReplaced", "Card is already in the process of being replaced. Please wait.");
                }
            }

            if (data.Errors.Any())
            {
                throw new Exception(JsonConvert.SerializeObject(data.Errors));
            }

            return data.Data.ReplacedCardStatus.Id;
        }

        public async Task<CardInfoModel> GetCardInfo(string mbanqToken, string ip, int cardId)
        {
            var client = CreateSelfServiceClient(mbanqToken, ip);

            var uri = $"/graphql";

            var body = new GraphqlRequest
            {
                Query = $"{{ Cards(where: {{ id: {{EQ: {cardId}}} }}) {{ select {{ id status physicalCardActivated onlinePaymentEnabled primaryAccountNumber replacedDamageCount }}}}}}"
            };

            var content = SerializationHelper.GetRequestContent(body);

            var response = await client.PostAsync(uri, content);

            await HandleErrors(response);

            var data = await SerializationHelper.GetResponseContent<GraphqlResponse<CardInfoResponse>>(response);

            var card = data.Data.Cards.SelectedCards[0];
            var mbanqCardStatus = ParseCardStatus(card.Status);

            return new CardInfoModel
            {
                ExternalId = card.Id,
                Status = mbanqCardStatus,
                OnlinePaymentEnabled = card.OnlinePaymentEnabled,
                PhysicalCardActivated = card.PhysicalCardActivated,
                PrimaryAccountNumber = card.PrimaryAccountNumber,
                DamagedReasonReplacementsCount = card.ReplacedDamageCount,
            };
        }

        public async Task<CardInfoModel[]> GetAllCards(string mbanqToken, string ip, int externalUserId)
        {
            var client = CreateSelfServiceClient(mbanqToken, ip);

            var uri = $"/graphql";

            var body = new GraphqlRequest
            {
                Query = $"{{ Cards(where: {{ account: {{ client: {{ id: {{EQ: {externalUserId}}} }} }} }}) {{ select {{ id status physicalCardActivated onlinePaymentEnabled primaryAccountNumber product {{id}}}}}}}}"
            };

            var content = SerializationHelper.GetRequestContent(body);

            var response = await client.PostAsync(uri, content);

            await HandleErrors(response);

            var data = await SerializationHelper.GetResponseContent<GraphqlResponse<CardInfoResponse>>(response);

            var cardProducts = _dbContext.CardProducts.ToArray();

            return data.Data.Cards.SelectedCards.Select(card => new CardInfoModel
            {
                ExternalId = card.Id,
                Status = ParseCardStatus(card.Status),
                OnlinePaymentEnabled = card.OnlinePaymentEnabled,
                PhysicalCardActivated = card.PhysicalCardActivated,
                PrimaryAccountNumber = card.PrimaryAccountNumber,
                Type = cardProducts
                    .FirstOrDefault(x => x.ExternalId == card.CardProduct.Id)
                    ?.AssociatedType ?? CardType.Unknown,
            }).ToArray();
        }

        public async Task<int> ChangeCardStatus(string mbanqToken, string ip, int cardId, string command)
        {
            HttpClient client;

            if (command.Equals(HandleCardEventCommand.ACTIVATE.ToString()))
            {
                client = CreateAuthorizedClient();
            }
            else
            {
                client = CreateSelfServiceClient(mbanqToken, ip);
            }

            var uri = $"/graphql";

            var body = new GraphqlRequest
            {
                Query =
                    $"mutation {{ handleCardEvent(id: {cardId}, input: {{ event: {command} }}) {{ id status }}}}"
            };

            var content = SerializationHelper.GetRequestContent(body);

            var response = await client.PostAsync(uri, content);

            await HandleErrors(response);

            var data = await SerializationHelper
                .GetResponseContent<GraphqlResponse<ChangeCardStatusResponse>>(response);

            return Convert.ToInt32(data.Data.HandleCardEvent.Id);
        }

        public async Task<string> GetChangeCardPinUrl(string mbanqToken, string ip, int cardId)
        {

            var client = CreateSelfServiceClient(mbanqToken, ip);

            var uri = $"/graphql";

            var body = new GraphqlRequest
            {
                Query = $"{{ getChangeCardPinUrl(id: {cardId})}}"
            };

            var content = SerializationHelper.GetRequestContent(body);

            var response = await client.PostAsync(uri, content);

            await HandleErrors(response);

            var data =
                await SerializationHelper.GetResponseContent<GraphqlResponse<ChangePinUrlResponse>>(response);

            return data.Data.Url;
        }

        public async Task<int> CountReplacements(string mbanqToken, string ip, int externalUserId)
        {
            var cards = await GetAllCards(
                mbanqToken,
                ip,
                externalUserId);

            var replacedCardsCount = cards.Count(
                c => c.Type == CardType.PhysicalDebit
                     && c.Status == CardStatus.REPLACED);

            var replacedAsDamagedCount = cards
                .Where(c => c.Type == CardType.PhysicalDebit)
                .Sum(c => c.DamagedReasonReplacementsCount);

            return replacedCardsCount + replacedAsDamagedCount;
        }

        private CardStatus ParseCardStatus(string cardStatus) =>
            Enum.TryParse<CardStatus>(cardStatus, true, out var status) ? status : CardStatus.Unknown;
    }
}
