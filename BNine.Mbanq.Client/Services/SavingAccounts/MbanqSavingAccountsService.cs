﻿#nullable enable
namespace BNine.MBanq.Client.Services.SavingAccounts
{
    using System.Net.Http;
    using System.Threading.Tasks;
    using BNine.Application.Interfaces.Bank.Client;
    using BNine.Application.Models.MBanq;
    using BNine.Common;
    using BNine.MBanq.Client.Models.Base;
    using BNine.MBanq.Client.Models.SavingAccounts.Responses;
    using BNine.MBanq.Client.Services.Base;
    using BNine.Settings;
    using Microsoft.Extensions.Caching.Memory;
    using Microsoft.Extensions.Logging;
    using Microsoft.Extensions.Options;

    public class MbanqSavingAccountsService : MBanqBaseService, IBankSavingAccountsService
    {
        public MbanqSavingAccountsService(IOptions<MBanqSettings> options, IHttpClientFactory clientFactory, ILogger<MbanqSavingAccountsService> logger, IMemoryCache memoryCache)
            : base(options, clientFactory, logger, memoryCache)
        {
        }

        public async Task<SavingAccountInfo?> GetSavingAccountInfoSafe(int savingId, string mbanqToken, string ip)
        {
            try
            {
                return await GetSavingAccountInfo(savingId, mbanqToken, ip);
            }
            catch (Exception ex)
            {
                Logger.LogError(ex, $"Failed to get saving account info for {savingId}");
                return null;
            }
        }

        public async Task<SavingAccountInfo> GetSavingAccountInfo(int savingId, string mbanqToken, string ip)
        {
            var client = CreateSelfServiceClient(mbanqToken, ip);
            return await FetchSavingAccountInfo(savingId, client);
        }

        public async Task<SavingAccountInfo> GetSavingAccountInfoAsAdmin(int savingId)
        {
            var client = CreateAuthorizedClient();
            return await FetchSavingAccountInfo(savingId, client);
        }

        public async Task<SavingAccountInfo[]> GetSavingAccounts(string[] savingIds)
        {
            var client = CreateAuthorizedClient();
            var uri = "/graphql";

            var body = new GraphqlRequest
            {
                Query =
                    $"{{ SavingsAccounts(where: {{ id: {{ IN: [{string.Join(',', savingIds)}] }} }}) {{ select {{ id accountNumber withdrawableBalance accountBalance currency {{ code }} }} }} }}"
            };

            var content = SerializationHelper.GetRequestContent(body);

            var response = await client.PostAsync(uri, content);

            await HandleErrors(response);

            var data =
                await SerializationHelper.GetResponseContent<GraphqlResponse<SavingAccountInfoArrayResponse>>(response);

            return data.Data?
                       .Array?
                       .Accounts?
                       .Select(Convert)
                       .Where(x => x != null)
                       .ToArray()
                   ?? System.Array.Empty<SavingAccountInfo>();
        }

        private async Task<SavingAccountInfo> FetchSavingAccountInfo(int savingId, HttpClient client)
        {
            var uri = $"/graphql";

            var body = new GraphqlRequest
            {
                Query =
                    $"{{SavingsAccount(id: {savingId}){{ id accountNumber withdrawableBalance accountBalance currency {{ code }} }}}}"
            };

            var content = SerializationHelper.GetRequestContent(body);

            var response = await client.PostAsync(uri, content);

            await HandleErrors(response);

            var data = await SerializationHelper.GetResponseContent<GraphqlResponse<SavingAccountInfoResponse>>(response);

            return Convert(data?.Data?.SavingsAccount);
        }

        private static SavingAccountInfo Convert(SavingsAccount savingsAccount)
        {
            return new SavingAccountInfo
            {
                Id = savingsAccount.Id,
                AccountNumber = savingsAccount?.AccountNumber,
                AvailableBalance = savingsAccount?.WithdrawableBalance ?? 0,
                AccountBalance = savingsAccount?.AccountBalance ?? 0,
                Currency = savingsAccount?.Currency?.Code
            };
        }
    }
}
