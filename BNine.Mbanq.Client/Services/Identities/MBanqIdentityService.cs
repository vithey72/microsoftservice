﻿namespace BNine.MBanq.Client.Services.Identities
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Net.Http;
    using System.Threading.Tasks;
    using Application.Interfaces.Bank.Client;
    using Application.Models.MBanq;
    using Common;
    using BNine.MBanq.Client.Models.Base;
    using BNine.MBanq.Client.Models.Identities.Responses;
    using BNine.MBanq.Client.Services.Base;
    using BNine.Settings;
    using Microsoft.Extensions.Logging;
    using Microsoft.Extensions.Options;
    using Microsoft.Extensions.Caching.Memory;

    public class MBanqIdentityService : MBanqBaseService, IBankIdentityService
    {
        public MBanqIdentityService(IOptions<MBanqSettings> options, IHttpClientFactory clientFactory, ILogger<MBanqIdentityService> logger, IMemoryCache memoryCache) : base(options, clientFactory, logger, memoryCache)
        {
        }

        public async Task<List<ClientIdentifierType>> GetClientIdentifierDocumentTypes()
        {
            var client = CreateAuthorizedClient();

            var uri = $"/graphql";

            var body = new GraphqlRequest
            {
                Query = $"{{ getClientIdentifierDocumentTypes {{ id label }} }}"
            };

            var content = SerializationHelper.GetRequestContent(body);

            var response = await client.PostAsync(uri, content);

            await HandleErrors(response);

            var identifierTypesResponse = await SerializationHelper.GetResponseContent<GraphqlResponse<GetClientIdentifierDocumentTypesResponse>>(response);

            var identifierTypes = identifierTypesResponse.Data.ClientIdentifierDocumentTypes
                .Select(x => new ClientIdentifierType
                {
                    ExternalId = Convert.ToInt32(x.Id),
                    Name = x.Label
                }).ToList();

            return identifierTypes;
        }
    }
}
