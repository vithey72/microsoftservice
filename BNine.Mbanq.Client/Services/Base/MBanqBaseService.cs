﻿namespace BNine.MBanq.Client.Services.Base
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Net;
    using System.Net.Http;
    using System.Net.Http.Headers;
    using System.Threading.Tasks;
    using Application.Models.MBanq;
    using BNine.Constants;
    using BNine.MBanq.Client.Models.Base;
    using BNine.MBanq.Models.Responses.Error;
    using Common;
    using Microsoft.Extensions.Caching.Memory;
    using Microsoft.Extensions.Logging;
    using Microsoft.Extensions.Options;
    using Settings;

    public abstract class MBanqBaseService
    {
        protected MBanqSettings Settings
        {
            get;
        }
        protected IHttpClientFactory ClientFactory
        {
            get;
        }
        protected ILogger Logger
        {
            get;
        }

        protected IMemoryCache MemoryCache
        {
            get;
        }

        protected MBanqBaseService(IOptions<MBanqSettings> options, IHttpClientFactory clientFactory, ILogger logger, IMemoryCache memoryCache)
        {
            Settings = options.Value;
            ClientFactory = clientFactory;
            Logger = logger;
            MemoryCache = memoryCache;
        }

        protected HttpClient CreateAuthorizedClient()
        {
            var client = ClientFactory.CreateClient(HttpClientName.MBanqAuthorized);

            return client;
        }

        protected HttpClient CreateSelfServiceClient(string token, string ip)
        {
            var client = ClientFactory.CreateClient(HttpClientName.MBanqSelfService);

            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
            if (ip != null)
            {
                client.DefaultRequestHeaders.Add("X-Forwarded-For", ip);
            }
            return client;
        }


        protected async Task<AuthorizationTokensResponseModel> GetAuthorizationTokens(HttpClient client, string userName, string password)
        {

            userName ??= Settings.Username;
            password ??= Settings.Password;

            var nvc = new List<KeyValuePair<string, string>>
            {
                new KeyValuePair<string, string>("username", userName),
                new KeyValuePair<string, string>("password", password),
                new KeyValuePair<string, string>("client_id", Settings.ClientId),
                new KeyValuePair<string, string>("client_secret", Settings.ClientSecret),
                new KeyValuePair<string, string>("grant_type", "password")
            };

            var response = await GetTokens(client, new FormUrlEncodedContent(nvc), userName);
            return new AuthorizationTokensResponseModel
            {
                AccessToken = response.AccessToken,
                RefreshToken = response.RefreshToken
            };
        }

        protected async Task<AuthorizationTokensResponseModel> GetAuthorizationTokensByRefreshToken(HttpClient client, string mbanqRefreshToken, string userId)
        {
            var nvc = new List<KeyValuePair<string, string>>
            {
                new KeyValuePair<string, string>("refresh_token", mbanqRefreshToken),
                new KeyValuePair<string, string>("client_id", Settings.ClientId),
                new KeyValuePair<string, string>("client_secret", Settings.ClientSecret),
                new KeyValuePair<string, string>("grant_type", "refresh_token")
            };

            var response = await GetTokens(client, new FormUrlEncodedContent(nvc), userId);
            return new AuthorizationTokensResponseModel
            {
                AccessToken = response.AccessToken,
                RefreshToken = response.RefreshToken
            };
        }

        protected async Task HandleErrors(HttpResponseMessage response)
        {
            if (!response.IsSuccessStatusCode)
            {
                // TODO: rewrite

                var result = await response.Content.ReadAsStringAsync();

                Logger.LogError("Mbanq Integration Error response: {result}", result);

                if (response.StatusCode == HttpStatusCode.Unauthorized)
                {
                    throw new UnauthorizedAccessException();
                }

                if (response.StatusCode == HttpStatusCode.BadRequest || response.StatusCode == HttpStatusCode.Forbidden)
                {
                    var responseBody = await SerializationHelper.GetResponseContent<ErrorResponse>(response);

                    throw new Exception(responseBody.Errors.FirstOrDefault()?.DefaultUserMessage);
                }

                throw new Exception("Unexpected error");
            }
        }

        private async Task<TokenResponse> GetTokens(HttpClient client, FormUrlEncodedContent content, string userId)
        {
            TokenResponse tokens;
            if (!MemoryCache.TryGetValue(userId, out tokens))
            {

                var req = new HttpRequestMessage(HttpMethod.Post, "/oauth/token") { Content = content };

                var response = await client.SendAsync(req);

                if (!response.IsSuccessStatusCode)
                {
                    Logger.LogError("Invalid login request, mbanq: " + await response.Content.ReadAsStringAsync());
                    throw new Exception("Invalid login request");
                }

                var body = await SerializationHelper.GetResponseContent<TokenResponse>(response);

                tokens = body;

                var cacheEntryOptions = new MemoryCacheEntryOptions()
                    .SetAbsoluteExpiration(TimeSpan.FromSeconds(body.ExpiresIn));

                MemoryCache.Set(userId, tokens, cacheEntryOptions);
            }

            return tokens;
        }
    }
}
