﻿namespace BNine.MBanq.Client.Services.ClientAddress
{
    using System;
    using System.Net.Http;
    using System.Threading.Tasks;
    using Application.Interfaces.Bank.Client;
    using Base;
    using Common;
    using BNine.MBanq.Client.Models.Base;
    using BNine.MBanq.Client.Models.ClinetAddress.Responses;
    using BNine.Settings;
    using Microsoft.Extensions.Logging;
    using Microsoft.Extensions.Options;
    using Newtonsoft.Json;
    using System.Linq;
    using Application.Models.MBanq;
    using Microsoft.Extensions.Caching.Memory;

    public class MBanqClientAddressService : MBanqBaseService, IBankClientAddressService
    {
        public MBanqClientAddressService(IOptions<MBanqSettings> options, IHttpClientFactory clientFactory,
            ILogger<MBanqClientAddressService> logger, IMemoryCache memoryCache)
            : base(options, clientFactory, logger, memoryCache)
        {
        }

        public async Task<int> SetAddress(int clientId, string city, int stateId, string address, string zipCode,
            string unit, int countryId, int addressTypeId)
        {
            var client = CreateAuthorizedClient();

            var uri = $"/graphql";

            var body = new GraphqlRequest
            {
                Query =
                    $"mutation {{ CreateAddress(input:{{ addressLine1: \"{address}\" addressLine2: \"{unit}\" stateOrProvinceId: \"{stateId}\" city: \"{city}\" postalCode: \"{zipCode}\" countryId: {countryId} }}) {{ id }} }}"
            };

            var content = SerializationHelper.GetRequestContent(body);

            var response = await client.PostAsync(uri, content);

            await HandleErrors(response);

            var addressData =
                await SerializationHelper.GetResponseContent<GraphqlResponse<CreateAddressResponse>>(response);

            if (addressData.Errors.Any())
            {
                throw new Exception(JsonConvert.SerializeObject(addressData.Errors));
            }

            var addressId = addressData.Data.CreateAddress.Id;

            body = new GraphqlRequest
            {
                Query =
                    $"mutation {{ CreateClientAddress(input:{{ clientId: {clientId} addressId: {addressId} active: true addressTypeId: {addressTypeId} }}) {{ id }} }}"
            };

            content = SerializationHelper.GetRequestContent(body);

            response = await client.PostAsync(uri, content);

            await HandleErrors(response);

            var clientAddressData =
                await SerializationHelper.GetResponseContent<GraphqlResponse<CreateClientAddressResponse>>(response);

            if (clientAddressData.Errors.Any())
            {
                throw new Exception("Create client address error: " + string.Join(";", clientAddressData.Errors.Select(x => x.Message)));
            }

            return addressId;
        }

        public async Task<int> UpdateAddress(int addressId, string city, int stateId, string address, string zipCode,
            string unit, int countryId)
        {
            var client = CreateAuthorizedClient();

            var uri = $"/graphql";

            var body = new GraphqlRequest
            {
                Query =
                    $"mutation {{ UpdateAddress(id: {addressId}, input:{{ addressLine1: \"{address}\" addressLine2: \"{unit}\" stateOrProvinceId: \"{stateId}\" city: \"{city}\" postalCode: \"{zipCode}\" countryId: {countryId} }}) {{ id }} }}"
            };

            var content = SerializationHelper.GetRequestContent(body);

            var response = await client.PostAsync(uri, content);

            await HandleErrors(response);

            var addressData =
                await SerializationHelper.GetResponseContent<GraphqlResponse<UpdateAddressResponse>>(response);

            if (addressData.Errors.Any())
            {
                var errorMessage = "Update address error, MBanq:";

                try
                {
                    errorMessage += ((List<GraphQlErrorDetails>)addressData.Errors.FirstOrDefault().Extensions.Errors).FirstOrDefault().DeveloperMessage;
                }
                catch (Exception)
                {
                }

                throw new Exception(errorMessage);
            }

            return addressId;
        }

        public async Task<List<ClientAddress>> GetAddresses(int clientId)
        {
            var client = CreateAuthorizedClient();

            var uri = $"/v1/client/{clientId}/addresses";

            var response = await client.GetAsync(uri);

            try
            {
                await HandleErrors(response);

                var addressesList = await SerializationHelper.GetResponseContent<List<ClientAddress>>(response);

                return addressesList;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public async Task<List<ClientAddress>> GetAddresses(int clientId, int addressTypeId)
        {
            var client = CreateAuthorizedClient();

            var uri = $"/v1/client/{clientId}/addresses?type={addressTypeId}";

            var response = await client.GetAsync(uri);

            try
            {
                await HandleErrors(response);

                var addressesList = await SerializationHelper.GetResponseContent<List<ClientAddress>>(response);

                return addressesList;
            }
            catch (Exception ex)
            {
                return null;
            }
        }
    }
}
