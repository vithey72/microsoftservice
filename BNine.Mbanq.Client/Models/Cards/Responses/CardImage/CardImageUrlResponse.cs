﻿namespace BNine.MBanq.Client.Models.Cards.Responses.CardImage
{
    using Newtonsoft.Json;

    public class CardImageUrlResponse
    {
        [JsonProperty("getCardImageUrl")]
        public string ImageUrl
        {
            get;
            set;
        }
    }

}
