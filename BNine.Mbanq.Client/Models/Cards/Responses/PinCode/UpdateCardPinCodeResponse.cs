﻿namespace BNine.MBanq.Client.Models.Cards.Responses.PinCode
{
    using Newtonsoft.Json;

    internal class UpdateCardPinCodeResponse
    {
        [JsonProperty("updateCardPin")]
        internal UpdatedPinCardStatus UpdateCardPin
        {
            get; set;
        }
    }

    internal class UpdatedPinCardStatus
    {
        [JsonProperty("id")]
        internal int Id
        {
            get; set;
        }

        [JsonProperty("status")]
        internal string Status
        {
            get; set;
        }
    }
}
