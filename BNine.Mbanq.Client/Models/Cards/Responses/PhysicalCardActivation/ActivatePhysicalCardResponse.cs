﻿namespace BNine.MBanq.Client.Models.Cards.Responses.PhysicalCardActivation
{
    using Newtonsoft.Json;

    internal class ActivatePhysicalCardResponse
    {
        [JsonProperty("activatePhysicalCard")]
        internal CardStatus ActivatePhysicalCard
        {
            get; set;
        }
    }

    internal class CardStatus
    {
        [JsonProperty("id")]
        internal int Id
        {
            get; set;
        }

        [JsonProperty("status")]
        internal string Status
        {
            get; set;
        }
    }
}
