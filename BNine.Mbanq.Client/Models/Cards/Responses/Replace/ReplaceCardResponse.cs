﻿namespace BNine.MBanq.Client.Models.Cards.Responses.Replace
{
    using Newtonsoft.Json;

    internal class ReplaceCardResponse
    {
        [JsonProperty("replaceCard")]
        internal ReplacedCardStatus ReplacedCardStatus
        {
            get; set;
        }
    }

    internal class ReplacedCardStatus
    {
        [JsonProperty("id")]
        internal int Id
        {
            get; set;
        }

        [JsonProperty("status")]
        internal string Status
        {
            get; set;
        }
    }
}
