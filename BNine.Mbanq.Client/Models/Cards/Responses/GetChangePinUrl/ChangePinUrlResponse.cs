﻿namespace BNine.MBanq.Client.Models.Cards.Responses.GetChangePinUrl
{
    using Newtonsoft.Json;

    public class ChangePinUrlResponse
    {
        [JsonProperty("getChangeCardPinUrl")]
        public string Url
        {
            get;
            set;
        }
    }
}
