﻿namespace BNine.MBanq.Client.Models.Cards.Responses.ShippingHistory
{
    using System.Collections.Generic;
    using Newtonsoft.Json;

    internal class ShippingHistoryResponse
    {
        [JsonProperty("Cards")]
        internal Cards Cards
        {
            get; set;
        }
    }

    internal class Cards
    {
        [JsonProperty("select")]
        internal List<Select> Select
        {
            get; set;
        }
    }

    internal class Select
    {
        [JsonProperty("id")]
        internal int Id
        {
            get; set;
        }

        [JsonProperty("shippedOn")]
        internal DateTime? ShippedOn
        {
            get; set;
        }

        [JsonProperty("shippingHistory")]
        internal List<ShippingHistory> ShippingHistory
        {
            get; set;
        }
    }

    internal class ShippingHistory
    {
        [JsonProperty("shippedBy")]
        internal string ShippedBy
        {
            get; set;
        }

        [JsonProperty("status")]
        internal string Status
        {
            get; set;
        }

        [JsonProperty("trackingId")]
        internal string TrackingId
        {
            get; set;
        }
    }
}
