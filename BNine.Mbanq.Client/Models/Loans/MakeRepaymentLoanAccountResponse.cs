﻿namespace BNine.MBanq.Client.Models.Loans
{
    using Newtonsoft.Json;

    internal class MakeRepaymentLoanAccountResponse
    {
        [JsonProperty("makeLoanRepaymentFromLinkedAccount")]
        internal RepaymentInfo RepaymentInfo
        {
            get; set;
        }
    }

    internal class RepaymentInfo
    {
        [JsonProperty("loanId")]
        internal string loanId
        {
            get; set;
        }
    }
}
