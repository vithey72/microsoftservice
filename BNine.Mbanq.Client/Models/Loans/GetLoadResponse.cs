﻿namespace BNine.MBanq.Client.Models.Loans
{
    using Newtonsoft.Json;

    internal class GetLoadResponse
    {
        [JsonProperty("Loan")]
        internal Loan Loan
        {
            get; set;
        }
    }

    internal class Loan
    {
        [JsonProperty("closed")]
        public bool Closed
        {
            get; set;
        }

        [JsonProperty("id")]
        public int Id
        {
            get; set;
        }

        [JsonProperty("disbursedAmount")]
        public decimal DisbursedAmount
        {
            get; set;
        }

        [JsonProperty("summary")]
        public LoanSummary Summary
        {
            get; set;
        }
    }

    internal class LoanSummary
    {
        [JsonProperty("totalOutstanding")]
        public decimal TotalOutstanding
        {
            get; set;
        }
    }
}
