﻿namespace BNine.MBanq.Client.Models.SavingAccounts.Responses
{
    using Newtonsoft.Json;

    internal class SavingAccountInfoResponse
    {
        [JsonProperty("SavingsAccount")]
        internal SavingsAccount SavingsAccount
        {
            get; set;
        }
    }

    internal class SavingAccountInfoArrayResponse
    {
        [JsonProperty("SavingsAccounts")]
        internal SavingsAccountArray Array
        {
            get; set;
        }
    }

    internal class SavingsAccountArray
    {
        [JsonProperty("select")]
        internal SavingsAccount[] Accounts
        {
            get; set;
        }
    }

    internal class SavingsAccount
    {
        [JsonIgnore]
        internal int Id => Convert.ToInt32(IdSting);

        [JsonProperty("id")]
        internal string IdSting
        {
            get;
            set;
        }

        [JsonProperty("accountNumber")]
        internal string AccountNumber
        {
            get; set;
        }

        [JsonProperty("withdrawableBalance")]
        internal decimal WithdrawableBalance
        {
            get; set;
        }

        [JsonProperty("accountBalance")]
        internal decimal AccountBalance
        {
            get; set;
        }

        [JsonProperty("currency")]
        internal Currency Currency
        {
            get; set;
        }
    }

    internal class Currency
    {
        [JsonProperty("code")]
        internal string Code
        {
            get; set;
        }
    }
}
