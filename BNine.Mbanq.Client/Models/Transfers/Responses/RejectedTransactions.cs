﻿namespace BNine.MBanq.Client.Models.Transfers.Responses
{
    using System.Collections.Generic;
    using Newtonsoft.Json;

    internal class RejectedTransactions
    {
        [JsonProperty("CardAuthorizations")]
        internal RejectedTransactionsResponse CardAuthorizations
        {
            get; set;
        }
    }

    internal class RejectedTransactionsResponse
    {
        [JsonProperty("total")]
        internal int Total
        {
            get; set;
        }

        [JsonProperty("select")]
        internal IEnumerable<CardAuthorization> Transactions
        {
            get; set;
        }
    }
}
