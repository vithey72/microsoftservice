﻿namespace BNine.MBanq.Client.Models.Transfers.Responses
{
    using System;
    using System.Collections.Generic;
    using BNine.MBanq.Client.Enums.Loans;
    using Newtonsoft.Json;

    internal class LoanTransactions
    {
        [JsonProperty("LoanTransactions")]
        internal LoanTransactionsItem LoanTransactionsItem
        {
            get; set;
        }
    }

    internal class LoanTransactionsItem
    {
        [JsonProperty("tx")]
        internal IEnumerable<LoanTx> Tx
        {
            get; set;
        }
    }

    internal class LoanTx
    {
        [JsonProperty("id")]
        internal int Id
        {
            get; set;
        }

        [JsonProperty("amount")]
        internal double Amount
        {
            get; set;
        }

        [JsonProperty("createdAt")]
        internal DateTime CreatedAt
        {
            get; set;
        }

        [JsonProperty("typeOf")]
        internal LoanTransactionType TypeOf
        {
            get; set;
        }
    }
}
