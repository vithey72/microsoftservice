﻿namespace BNine.MBanq.Client.Models.Transfers.Responses
{
    using System;
    using System.Collections.Generic;
    using Newtonsoft.Json;

    internal class SavingsTransactions
    {
        [JsonProperty("SavingsAccountTransactions")]
        internal SavingsAccountTransactions SavingsAccountTransactions
        {
            get;
            set;
        }
    }

    internal class SavingsAccountTransactions
    {
        [JsonProperty("pages")]
        internal int Pages
        {
            get;
            set;
        }

        [JsonProperty("total")]
        internal long Total
        {
            get;
            set;
        }

        [JsonProperty("select")]
        internal IEnumerable<Transaction> Items
        {
            get;
            set;
        }
    }

    internal class Transaction
    {
        [JsonProperty("id")]
        internal int Id
        {
            get;
            set;
        }

        [JsonProperty("typeOf")]
        internal int? TypeOf
        {
            get;
            set;
        }

        [JsonProperty("subTypeOf")]
        internal int? SubTypeOf
        {
            get;
            set;
        }

        [JsonProperty("amount")]
        internal decimal? Amount
        {
            get;
            set;
        }

        [JsonProperty("createdAt")]
        internal DateTime CreatedAt
        {
            get;
            set;
        }

        [JsonProperty("initiatedAt")]
        internal DateTime InitiatedAt
        {
            get; set;
        }

        [JsonProperty("runningBalance")]
        internal decimal? RunningBalance
        {
            get;
            set;
        }

        [JsonProperty("credit")]
        internal bool Credit
        {
            get;
            set;
        }

        [JsonProperty("debit")]
        internal bool Debit
        {
            get;
            set;
        }

        [JsonProperty("reversed")]
        internal bool Reversed
        {
            get;
            set;
        }

        [JsonProperty("paymentDetail")]
        internal PaymentDetail PaymentDetail
        {
            get;
            set;
        }

        [JsonProperty("account")]
        internal Account Account
        {
            get;
            set;
        }
    }

    internal class PaymentDetail
    {
        [JsonProperty("id")]
        internal int Id
        {
            get;
            set;
        }

        [JsonProperty("reference")]
        internal string Reference
        {
            get;
            set;
        }

        [JsonProperty("userInputReference")]
        internal string UserInputReference
        {
            get;
            set;
        }

        [JsonProperty("paymentType")]
        internal PaymentType PaymentType
        {
            get;
            set;
        }

        [JsonProperty("debtor")]
        internal Counterparty Debtor
        {
            get;
            set;
        }

        [JsonProperty("creditor")]
        internal Counterparty Creditor
        {
            get;
            set;
        }

        [JsonProperty("cardAuthorization")]
        internal CardAuthorization CardAuthorization
        {
            get;
            set;
        }
    }

    internal class CardAuthorization
    {
        [JsonProperty("id")]
        internal int Id
        {
            get; set;
        }

        [JsonProperty("amount")]
        internal decimal? Amount
        {
            get; set;
        }

        [JsonProperty("transactionType")]
        internal string TransactionType
        {
            get; set;
        }

        [JsonProperty("merchant")]
        public Merchant Merchant
        {
            get; set;
        }

        [JsonProperty("createdAt")]
        internal DateTime CreatedAt
        {
            get; set;
        }

        [JsonProperty("status")]
        public string Status
        {
            get; set;
        }

        [JsonProperty("card")]
        public Card Card
        {
            get;
            set;
        }

        [JsonProperty("account")]
        public Account Account
        {
            get;
            set;
        }
    }

    internal class Card
    {
        [JsonProperty("primaryAccountNumber")]
        public string PrimaryAccountNumber
        {
            get;
            set;
        }
    }

    internal class Account
    {
        [JsonProperty("accountNumber")]
        public string AccountNumber
        {
            get;
            set;
        }
    }

    internal class Merchant
    {
        [JsonProperty("mcc")]
        internal int Mcc
        {
            get;
            set;
        }

        [JsonProperty("merchantId")]
        internal string MerchantId
        {
            get;
            set;
        }

        [JsonProperty("description")]
        internal string Description
        {
            get;
            set;
        }

        [JsonProperty("postalcode")]
        internal string Postalcode
        {
            get;
            set;
        }

        [JsonProperty("country")]
        internal string Country
        {
            get;
            set;
        }

        [JsonProperty("street")]
        internal string Street
        {
            get;
            set;
        }

        [JsonProperty("state")]
        internal string State
        {
            get;
            set;
        }
    }

    internal class PaymentType
    {
        [JsonProperty("id")]
        internal int Id
        {
            get;
            set;
        }

        [JsonProperty("name")]
        internal string Name
        {
            get;
            set;
        }
    }

    internal class Counterparty
    {
        [JsonProperty("identifier")]
        internal string Identifier
        {
            get;
            set;
        }

        [JsonProperty("name")]
        internal string Name
        {
            get;
            set;
        }

        [JsonProperty("address")]
        internal IEnumerable<string> Address
        {
            get;
            set;
        }

        [JsonProperty("country")]
        internal string Country
        {
            get;
            set;
        }

        [JsonProperty("city")]
        internal string City
        {
            get;
            set;
        }

        [JsonProperty("postalCode")]
        internal string PostalCode
        {
            get;
            set;
        }
    }
}
