﻿namespace BNine.MBanq.Client.Models.Transfers.Responses
{
    using System;
    using System.Collections.Generic;
    using Newtonsoft.Json;

    /// <summary>
    /// Pending transaction response model
    /// </summary>
    internal class PendingTransactions
    {
        [JsonProperty("AuthorizeTransactions")]
        internal AuthorizeTransactions AuthorizeTransactions
        {
            get;
            set;
        }
    }

    /// <summary>
    /// Authorized account transactions
    /// </summary>
    internal class AuthorizeTransactions
    {
        [JsonProperty("total")]
        internal int Total
        {
            get;
            set;
        }

        [JsonProperty("select")]
        internal IEnumerable<PendingTransaction> Transactions
        {
            get;
            set;
        }
    }

    /// <summary>
    /// Pending transaction item
    /// </summary>
    internal class PendingTransaction
    {
        [JsonProperty("id")]
        internal int Id
        {
            get;
            set;
        }

        [JsonProperty("amount")]
        internal decimal Amount
        {
            get;
            set;
        }

        [JsonProperty("createdAt")]
        internal DateTime CreatedAt
        {
            get;
            set;
        }

        [JsonProperty("typeOf")]
        internal string TypeOf
        {
            get;
            set;
        }

        [JsonProperty("cardAuthorization")]
        internal CardAuthorization CardAuthorization
        {
            get;
            set;
        }

        [JsonProperty("transfer")]
        internal PendingTransactionDetails Transfer
        {
            get;
            set;
        }

        [JsonProperty("account")]
        internal PendingTransactionAccount Account
        {
            get;
            set;
        }

        internal class PendingTransactionDetails
        {
            [JsonProperty("id")]
            internal int Id
            {
                get;
                set;
            }

            [JsonProperty("status")]
            internal string Status
            {
                get;
                set;
            }

            [JsonProperty("debtor")]
            internal Counterparty Debtor
            {
                get;
                set;
            }

            [JsonProperty("creditor")]
            internal Counterparty Creditor
            {
                get;
                set;
            }

            [JsonProperty("paymentType")]
            internal PaymentTypeStruct PaymentType
            {
                get;
                set;
            }

            [JsonProperty("reference")]
            internal string[] ReferencesList
            {
                get;
                set;
            } = Array.Empty<string>();

            internal string Reference => ReferencesList?.FirstOrDefault();
        }

        internal class PendingTransactionAccount
        {
            [JsonProperty("accountNumber")]
            public string AccountNumber
            {
                get;
                set;
            }
        }

        internal struct PaymentTypeStruct
        {
            [JsonProperty("name")]
            public string Name
            {
                get;
                set;
            }
        }
    }
}
