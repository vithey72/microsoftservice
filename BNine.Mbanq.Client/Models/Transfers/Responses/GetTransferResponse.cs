﻿namespace BNine.MBanq.Client.Models.Transfers.Responses
{
    using System;
    using Newtonsoft.Json;

    internal class GetTransferResponse
    {
        [JsonProperty("Transfer")]
        public TransferDetails Transfer
        {
            get; set;
        }
    }

    internal class TransferDetails
    {
        [JsonProperty("id")]
        internal int Id
        {
            get; set;
        }

        [JsonProperty("status")]
        internal string Status
        {
            get; set;
        }

        [JsonProperty("createdAt")]
        internal DateTime CreatedAt
        {
            get; set;
        }
    }
}
