﻿namespace BNine.MBanq.Client.Models.ClinetAddress.Responses
{
    using Newtonsoft.Json;

    internal class CreateClientAddressResponse
    {
        [JsonProperty("CreateClientAddress")]
        internal CreateAddress CreateClientAddress
        {
            get; set;
        }
    }

    internal class CreateClientAddress
    {
        [JsonProperty("id")]
        internal int Id
        {
            get; set;
        }
    }
}
