﻿namespace BNine.MBanq.Client.Models.ClinetAddress.Responses;

using Newtonsoft.Json;

public class GraphQlErrorDetails
{
    [JsonProperty("developerMessage")]
    public string DeveloperMessage
    {
        get;
        set;
    }
}
