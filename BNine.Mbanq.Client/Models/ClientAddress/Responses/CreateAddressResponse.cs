﻿namespace BNine.MBanq.Client.Models.ClinetAddress.Responses
{

    using Newtonsoft.Json;

    internal class CreateAddressResponse
    {
        [JsonProperty("CreateAddress")]
        internal CreateAddress CreateAddress
        {
            get; set;
        }
    }

    internal class CreateAddress
    {
        [JsonProperty("id")]
        internal int Id
        {
            get; set;
        }
    }
}
