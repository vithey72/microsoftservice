﻿namespace BNine.MBanq.Client.Models.ClinetAddress.Responses
{
    using Newtonsoft.Json;

    internal class UpdateAddressResponse
    {
        [JsonProperty("UpdateAddress")]
        internal UpdateAddress UpdateAddress
        {
            get; set;
        }
    }

    internal class UpdateAddress
    {
        [JsonProperty("id")]
        internal int Id
        {
            get; set;
        }
    }
}
