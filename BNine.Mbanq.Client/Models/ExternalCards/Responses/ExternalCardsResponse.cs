﻿namespace BNine.MBanq.Client.Models.ExternalCards.Responses
{
    using System.Collections.Generic;
    using Newtonsoft.Json;

    internal class ExternalCardsResponseContainer
    {
        [JsonProperty("ExternalCards")]
        internal ExternalCardsResponse ExternalCardsRasponse
        {
            get; set;
        }
    }

    internal class ExternalCardsResponse
    {
        [JsonProperty("pages")]
        internal int Pages
        {
            get; set;
        }

        [JsonProperty("total")]
        internal int Total
        {
            get; set;
        }

        [JsonProperty("select")]
        internal IEnumerable<Select> Select
        {
            get; set;
        } = new List<Select>();
    }

    internal class Details
    {
        [JsonProperty("lastDigits")]
        internal int LastDigits
        {
            get; set;
        }

        [JsonProperty("networkName")]
        internal string NetworkName
        {
            get; set;
        }
    }

    internal class Select
    {
        [JsonProperty("id")]
        internal int Id
        {
            get; set;
        }

        [JsonProperty("details")]
        internal Details Details
        {
            get; set;
        }
    }
}
