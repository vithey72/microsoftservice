﻿namespace BNine.MBanq.Client.Models.Base
{
    using System.Collections.Generic;
    using Newtonsoft.Json;

    public class GraphqlResponse<T>
    {
        [JsonProperty("data")]
        public T Data
        {
            get; set;
        }

        [JsonProperty("errors")]
        public IEnumerable<GraphqlError> Errors
        {
            get; set;
        } = new List<GraphqlError>();
    }

    public class GraphqlError
    {
        [JsonProperty("message")]
        public string Message
        {
            get; set;
        }

        [JsonProperty("extensions")]
        public Extension Extensions
        {
            get; set;
        }
    }

    public class Extension
    {
        [JsonProperty("code")]
        public string Code
        {
            get; set;
        }

        [JsonProperty("parameterName")]
        public object ParameterName
        {
            get; set;
        }

        [JsonProperty("defaultUserMessageArgs")]
        public List<object> DefaultUserMessageArgs
        {
            get; set;
        }

        [JsonProperty("globalisationMessageCode")]
        public string GlobalisationMessageCode
        {
            get; set;
        }

        [JsonProperty("defaultUserMessage")]
        public string DefaultUserMessage
        {
            get; set;
        }

        [JsonProperty("errors")]
        public object Errors
        {
            get; set;
        }
    }
}
