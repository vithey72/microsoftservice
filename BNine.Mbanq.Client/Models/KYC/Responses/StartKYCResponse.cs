﻿namespace BNine.MBanq.Client.Models.KYC.Responses
{
    using Newtonsoft.Json;

    internal class StartKYCResponse
    {
        [JsonProperty("initiateClientVerification")]
        internal string InitiateClientVerification
        {
            get; set;
        }
    }
}
