﻿namespace BNine.MBanq.Client.Models.ATMs.Responses
{
    using System.Collections.Generic;
    using Newtonsoft.Json;

    internal class GetATMsResponse
    {
        [JsonProperty("getATM")]
        internal ATMSInfo ATMsInfo
        {
            get; set;
        }
    }

    internal class ATMSInfo
    {
        [JsonProperty("select")]
        public IEnumerable<ATMInfo> Items
        {
            get; set;
        }

        [JsonProperty("total")]
        public long Total
        {
            get; set;
        }
    }

    internal class ATMInfo
    {
        [JsonProperty("name")]
        public string Name
        {
            get; set;
        }

        [JsonProperty("isDepositAvailable")]
        public bool IsDepositAvailable
        {
            get; set;
        }

        [JsonProperty("isAvailable24Hours")]
        public bool IsAvailable24Hours
        {
            get; set;
        }

        [JsonProperty("isHandicappedAccessible")]
        public bool IsHandicappedAccessible
        {
            get; set;
        }

        [JsonProperty("locationDescription")]
        public string LocationDescription
        {
            get; set;
        }

        [JsonProperty("logoName")]
        public string LogoName
        {
            get; set;
        }

        [JsonProperty("network")]
        public string Network
        {
            get; set;
        }

        [JsonProperty("location")]
        public ATMLocation Location
        {
            get; set;
        }
    }

    internal class ATMLocation
    {
        [JsonProperty("address")]
        internal ATMAddress Address
        {
            get; set;
        }

        [JsonProperty("coordinates")]
        internal ATMCoordinates Coordinates
        {
            get; set;
        }
    }

    internal class ATMAddress
    {
        [JsonProperty("city")]
        internal string City
        {
            get; set;
        }

        [JsonProperty("line1")]
        internal string AddressLine1
        {
            get; set;
        }

        [JsonProperty("line2")]
        internal string AddressLine2
        {
            get; set;
        }

        [JsonProperty("state")]
        internal string State
        {
            get; set;
        }

        [JsonProperty("postalCode")]
        internal int PostalCode
        {
            get; set;
        }

        [JsonProperty("country")]
        internal string Country
        {
            get; set;
        }
    }

    internal class ATMCoordinates
    {
        [JsonProperty("longitude")]
        internal double Longitude
        {
            get; set;
        }

        [JsonProperty("latitude")]
        internal double Latitude
        {
            get; set;
        }
    }
}
