﻿namespace BNine.MBanq.Client.Models.Identities.Responses
{
    using System.Collections.Generic;
    using Newtonsoft.Json;

    internal class GetClientIdentifierDocumentTypesResponse
    {
        [JsonProperty("getClientIdentifierDocumentTypes")]
        internal List<ClientIdentifierDocumentType> ClientIdentifierDocumentTypes
        {
            get; set;
        }
    }

    internal class ClientIdentifierDocumentType
    {
        [JsonProperty("id")]
        internal string Id
        {
            get; set;
        }

        [JsonProperty("label")]
        internal string Label
        {
            get; set;
        }
    }
}
