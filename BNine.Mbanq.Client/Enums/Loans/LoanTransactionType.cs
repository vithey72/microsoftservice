﻿namespace BNine.MBanq.Client.Enums.Loans
{
    internal enum LoanTransactionType
    {
        Accural = 1,
        Repayment = 2,
        Disbursement = 10
    }
}
