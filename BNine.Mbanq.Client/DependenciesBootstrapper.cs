﻿namespace BNine.MBanq.Client
{
    using BNine.Application.Interfaces.Bank.Client;
    using BNine.MBanq.Client.Services.ATMs;
    using BNine.MBanq.Client.Services.Authorization;
    using BNine.MBanq.Client.Services.ClientAddress;
    using BNine.MBanq.Client.Services.KYC;
    using BNine.MBanq.Client.Services.ExternalCards;
    using BNine.MBanq.Client.Services.Identities;
    using BNine.MBanq.Client.Services.Loans;
    using BNine.MBanq.Client.Services.SavingAccounts;
    using Microsoft.Extensions.DependencyInjection;
    using Services.Cards;
    using Services.Transfers;

    public static class DependenciesBootstrapper
    {
        public static IServiceCollection AddMBanqClient(this IServiceCollection services)
        {
            services.AddTransient<IBankTransfersService, MBanqTransfersService>();
            services.AddTransient<IBankInternalCardsService, MBanqInternalCardsService>();
            services.AddTransient<IBankATMsService, MbanqATMsService>();
            services.AddTransient<IBankAuthorizationService, MBanqAuthorizationService>();
            services.AddTransient<IBankClientAddressService, MBanqClientAddressService>();
            services.AddTransient<IBankKYCService, MBanqKYCService>();
            services.AddTransient<IBankLoanTransactionsService, MBanqLoanTransactionsService>();
            services.AddTransient<IBankLoanAccountsService, MBanqLoanAccountsService>();
            services.AddTransient<IBankSavingsTransactionsService, MBanqSavingsTransactionsService>();
            services.AddTransient<IBankIdentityService, MBanqIdentityService>();
            services.AddTransient<IBankClientExternalCardsService, MBanqClientExternalCardsService>();
            services.AddTransient<IBankSavingAccountsService, MbanqSavingAccountsService>();

            return services;
        }
    }
}
