﻿namespace BNine.Common
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;

    public static class QueueExecutor
    {
        public static async Task ExecuteInQueueAsync<T>(IEnumerable<T> values, int butchSize, Action<IEnumerable<T>> action)
        {
            var i = 0;
            while (values.Skip(i * butchSize).Take(butchSize).Count() > 0)
            {
                await Task.Run(() => action.Invoke(values.Skip(i * butchSize).Take(butchSize)));
                i++;
            }
        }
    }
}
