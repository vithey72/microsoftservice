﻿namespace BNine.Common.Extensions;

#nullable enable

public static class NullableExtensions
{
    public static T NotNull<T>(this T? instanceOrNull, string? name = null)
        where T : class
    {
        if (instanceOrNull == null)
        {
            throw new NullReferenceException($"reference {name ?? ""} is null but must not be");
        }

        return instanceOrNull;
    }

    public static T NotNull<T>(this T? instanceOrNull, string? name = null)
        where T : struct
    {
        if (instanceOrNull == null)
        {
            throw new NullReferenceException($"nullable value {name ?? ""} is null but must not be");
        }

        return instanceOrNull.Value;
    }
}
