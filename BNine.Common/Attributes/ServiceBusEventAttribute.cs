﻿namespace BNine.Common.Attributes
{
    using System;

    public class ServiceBusEventAttribute : Attribute
    {
        public string TopicName { get; set; }
    }
}
