﻿namespace BNine.Common.Classes
{
    using Newtonsoft.Json;

    public class IdValue
    {
        [JsonProperty("id")]
        public int Id
        {
            get; set;
        }

        [JsonProperty("value")]
        public string Value
        {
            get; set;
        }
    }
}
