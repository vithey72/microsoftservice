﻿namespace BNine.Common.Classes
{
    using Newtonsoft.Json;

    public class IdName
    {
        [JsonProperty("id")]
        public int Id
        {
            get; set;
        }

        [JsonProperty("name")]
        public string Name
        {
            get; set;
        }
    }
}
