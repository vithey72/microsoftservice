﻿namespace BNine.Common
{
    using System;

    public static class ArgyleRoutingNumberHelper
    {
        public static string TakeLastNumbers(string value, int count = 2)
        {
            if (string.IsNullOrEmpty(value))
            {
                return null;
            }

            var newString = string.Empty;

            for (var i = value.Length -1; i >= 0; i--)
            {
                if (char.IsNumber(value[i]))
                {
                    newString += value[i];
                }
                else
                {
                    break;
                }

                if (newString.Length == count)
                {
                    break;
                }
            }

            if (newString.Length == 0)
            {
                return string.Empty;
            }

            var charArray = newString.ToCharArray();
            Array.Reverse(charArray);

            return new string(charArray);
        }
    }
}
