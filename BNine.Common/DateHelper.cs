﻿namespace BNine.Common
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.Linq;

    public static class DateHelper
    {
        public static readonly string MonthFirstFormat = "MMM dd, yyyy";
        public static readonly string DateDDMMMYYYYRegex = "^.* (?'date'[0-9]{1,2} [A-Za-z]{3} 20[0-9]{2})$";

        public static DateTime? GetDate(IEnumerable<int> date)
        {
            if (date == null || date.Count() == 0)
            {
                return null;
            }

            var list = date.ToList();

            var result = new DateTime(list[0], list[1], list[2]);

            return result;
        }

        public static DateTime TrimToHour(this DateTime dateTime)
        {
            return new DateTime(dateTime.Year, dateTime.Month, dateTime.Day, dateTime.Hour, 0, 0, 0, dateTime.Kind);
        }

        public static string ToGraphqlQueryFormat(DateTime dateTime) => dateTime.ToString("yyyy'-'MM'-'dd'T'HH':'mm':'ss");

        public static string ToAmericanFormat(DateTime paymentDate) =>
            paymentDate.ToString(MonthFirstFormat, new CultureInfo("en-US"));
    }
}
