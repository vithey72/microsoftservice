﻿namespace BNine.Common
{
    using System;

    // TODO: temporary solution
    public static class TimeZoneHelper
    {
        private static readonly TimeZoneInfo EstTimeZoneInfo = null;

        static TimeZoneHelper()
        {
            try
            {
                EstTimeZoneInfo = TimeZoneInfo.FindSystemTimeZoneById("Eastern Standard Time");
            }
            catch
            {
                EstTimeZoneInfo = TimeZoneInfo.FindSystemTimeZoneById("America/New_York");
            }
        }

        public static DateTime UtcToEasternStandartTime(DateTime timeUtc)
        {
            return TimeZoneInfo.ConvertTimeFromUtc(timeUtc, EstTimeZoneInfo);
        }

        public static DateTime EasternStandartTimeToUtc(DateTime timeEst)
        {
            return TimeZoneInfo.ConvertTimeToUtc(timeEst, EstTimeZoneInfo);
        }
    }
}
