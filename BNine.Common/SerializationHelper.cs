﻿namespace BNine.Common
{
    using System.IO;
    using System.Net.Http;
    using System.Text;
    using System.Threading.Tasks;
    using Newtonsoft.Json;

    public static class SerializationHelper
    {
        public static StringContent GetRequestContent(object obj, JsonSerializerSettings serializerSettings = null)
        {
            var converted = JsonConvert.SerializeObject(obj, serializerSettings);
            return new StringContent(converted, Encoding.UTF8, "application/json");
        }

        public static StringContent GetIgnoredEmptyPropertiesRequestContent(object obj)
        {
            return new StringContent(JsonConvert.SerializeObject(obj, new JsonSerializerSettings
            {
                NullValueHandling = NullValueHandling.Ignore
            }), Encoding.UTF8, "application/json");
        }

        public static async Task<T> GetResponseContent<T>(HttpResponseMessage response)
        {
            var stringResponse = await response.Content.ReadAsStringAsync();

            var result = JsonConvert.DeserializeObject<T>(stringResponse);

            return result;
        }

        public static T Deserialize<T>(byte[] buffer)
        {
            using StreamReader sr = new StreamReader(new MemoryStream(buffer));
            return JsonConvert.DeserializeObject<T>(sr.ReadToEnd());
        }
    }
}
