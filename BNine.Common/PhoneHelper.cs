﻿namespace BNine.Common
{
    using System.Text.RegularExpressions;

    public static class PhoneHelper
    {
        public static bool IsValidPhone(string phone)
        {
            return Regex.IsMatch(phone, @"^\+[0-9]{11}$");
        }
    }
}
