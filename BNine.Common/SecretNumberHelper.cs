﻿namespace BNine.Common
{
    public static class SecretNumberHelper
    {
        public static string GetLastFourDigits(string number)
        {
            if (string.IsNullOrEmpty(number))
            {
                return number;
            }

            return number.Length >= 4 ? $"{number.Substring(number.Length - 4)}" : number;
        }

        public static string GetLast4DigitsWithPrefix(string number, string prefix)
        {
            return prefix + GetLastFourDigits(number);
        }
    }
}
