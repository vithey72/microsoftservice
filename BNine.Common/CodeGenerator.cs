﻿namespace BNine.Common
{
    using System;

    public static class CodeGenerator
    {
        public static string GenerateCode()
        {
            var generator = new Random();

            return generator.Next(0, 999999).ToString("D6");
        }
    }
}
