﻿namespace BNine.Appsflyer.Models.Requests
{
    using System;
    using Newtonsoft.Json;

    public class CreateEventRequest
    {
        [JsonProperty("appsflyer_id")]
        public string AppsflyerId
        {
            get; set;
        }

        [JsonProperty("customer_user_id")]
        public Guid CustomerUserId
        {
            get; set;
        }

        [JsonProperty("eventName")]
        public string EventName
        {
            get; set;
        }
    }
}
