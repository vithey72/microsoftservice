﻿namespace BNine.Appsflyer
{
    using System;
    using System.Net.Http;
    using System.Threading.Tasks;
    using BNine.Application.Interfaces.Appsflyer;
    using BNine.Appsflyer.Models.Requests;
    using BNine.Common;
    using BNine.Constants;
    using BNine.Settings;
    using Enums;
    using Microsoft.Extensions.Logging;
    using Microsoft.Extensions.Options;

    public class AppsflyerServerEventsService : IAppsflyerServerEventsService
    {
        private IHttpClientFactory ClientFactory
        {
            get;
        }
        public ILogger<AppsflyerServerEventsService> Logger
        {
            get;
        }

        private AppsflyerSettings AppsflyerSettings
        {
            get;
        }

        public AppsflyerServerEventsService(IOptions<AppsflyerSettings> options, IHttpClientFactory clientFactory,
            ILogger<AppsflyerServerEventsService> logger)
        {
            ClientFactory = clientFactory;
            Logger = logger;
            AppsflyerSettings = options.Value;
        }

        public async Task SendEvent(string appsflyerId, Guid anonymousAppsflyerId, string eventName, MobilePlatform platform)
        {
            var httpClient = ClientFactory.CreateClient(HttpClientName.AppsflyerS2S);

            var body = new CreateEventRequest
            {
                AppsflyerId = appsflyerId,
                CustomerUserId = anonymousAppsflyerId,
                EventName = eventName
            };

            var content = SerializationHelper.GetRequestContent(body);

            var bundleId = platform == MobilePlatform.iOs
                ? AppsflyerSettings.IOSBundleId
                : AppsflyerSettings.AndroidBundleId;

            var response = await httpClient.PostAsync($"/inappevent/{bundleId}", content);

            response.EnsureSuccessStatusCode();

            Logger.LogInformation("Appsflyer event has been triggered: \"{event}\" for appsflyer Id = {user}, {platform} ({bundleId})", eventName, appsflyerId, platform, bundleId);
        }
    }
}
