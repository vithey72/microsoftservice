﻿namespace BNine.ServiceBus
{
    using System;
    using BNine.Application.Interfaces.Appsflyer;
    using BNine.Appsflyer;
    using BNine.Constants;
    using Microsoft.Extensions.Configuration;
    using Microsoft.Extensions.DependencyInjection;
    using Polly;

    public static class DependenciesBootstrapper
    {
        public static IServiceCollection AddAppsflyer(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddTransient<IAppsflyerServerEventsService, AppsflyerServerEventsService>();

            services.AddHttpClient(HttpClientName.AppsflyerS2S, client =>
            {
                client.BaseAddress = new Uri(configuration["AppsflyerSettings:S2SUri"]);
                client.DefaultRequestHeaders.Add("Authentication", configuration["AppsflyerSettings:DevKey"]);
            })
            .AddTransientHttpErrorPolicy(p => p.WaitAndRetryAsync(3, (x) => TimeSpan.FromSeconds(x)));

            return services;
        }
    }
}
