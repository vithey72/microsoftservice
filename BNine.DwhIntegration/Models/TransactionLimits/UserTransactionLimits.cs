﻿namespace BNine.DwhIntegration.Models.TransactionLimits;

using Application.Aggregates.Users.Models.CardLimits;

public class TransactionLimits : UserCardLimits
{
    public ExternalCard ExternalCard
    {
        get;
        set;
    }

    public ACH ACH
    {
        get;
        set;
    }
}
