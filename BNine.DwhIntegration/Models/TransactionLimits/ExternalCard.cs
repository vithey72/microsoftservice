﻿namespace BNine.DwhIntegration.Models;

public class ExternalCard
{
    public decimal? PushAmountDaily
    {
        get; set;
    }

    public int? PushCountDaily
    {
        get; set;
    }

    public decimal? PushLimitDaily
    {
        get; set;
    }

    public int? PushLimitCountDaily
    {
        get; set;
    }

    public decimal? PushAmountWeekly
    {
        get; set;
    }
    public int? PushCountWeekly
    {
        get; set;
    }

    public decimal? PushLimitWeekly
    {
        get; set;
    }

    public int? PushLimitCountWeekly
    {
        get; set;
    }

    public decimal? PushAmounMonthly
    {
        get; set;
    }

    public int? PushCountMonthly
    {
        get; set;
    }

    public decimal? PushLimitMonthly
    {
        get; set;
    }

    public int? PushLimitCountMonthly
    {
        get; set;
    }

    public decimal? PullAmountDaily
    {
        get; set;
    }

    public int? PullCountDaily
    {
        get; set;
    }

    public decimal? PullLimitDaily
    {
        get; set;
    }

    public int? PullLimitCountDaily
    {
        get; set;
    }

    public decimal? PullAmountWeekly
    {
        get; set;
    }

    public int? PullCountWeekly
    {
        get; set;
    }

    public decimal? PullLimitWeekly
    {
        get; set;
    }

    public int? PullLimitCountWeekly
    {
        get; set;
    }

    public decimal? PullAmounMonthly
    {
        get; set;
    }

    public int? PullCountMonthly
    {
        get; set;
    }

    public decimal? PullLimitMonthly
    {
        get; set;
    }
    public int? PullLimitCountMonthly
    {
        get; set;
    }
}
