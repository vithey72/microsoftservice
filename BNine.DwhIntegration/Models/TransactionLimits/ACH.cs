﻿namespace BNine.DwhIntegration.Models;

public class ACH
{
    public decimal? DepositAmountDaily
    {
        get;
        set;
    }

    public int? DepositCountDaily
    {
        get;
        set;
    }

    public decimal? DepositLimitDaily
    {
        get;
        set;
    }

    public int? DepositLimitCountDaily
    {
        get;
        set;
    }

    public decimal? DepositAmountWeekly
    {
        get;
        set;
    }

    public int? DepositCountWeekly
    {
        get;
        set;
    }

    public decimal? DepositLimitWeekly
    {
        get;
        set;
    }

    public int? DepositLimitCountWeekly
    {
        get;
        set;
    }

    public decimal? DepositAmounMonthly
    {
        get;
        set;
    }

    public int? DepositCountMonthly
    {
        get;
        set;
    }

    public decimal? DepositLimitMonthly
    {
        get;
        set;
    }

    public int? DepositLimitCountMonthly
    {
        get;
        set;
    }

    public decimal? WithdrawlAmountDaily
    {
        get;
        set;
    }

    public int? WithdrawlCountDaily
    {
        get;
        set;
    }

    public decimal? WithdrawlLimitDaily
    {
        get;
        set;
    }

    public int? WithdrawlLimitCountDaily
    {
        get;
        set;
    }

    public decimal? WithdrawlAmountWeekly
    {
        get;
        set;
    }

    public int? WithdrawlCountWeekly
    {
        get;
        set;
    }

    public decimal? WithdrawlLimitWeekly
    {
        get;
        set;
    }

    public int? WithdrawlLimitCountWeekly
    {
        get;
        set;
    }

    public decimal? WithdrawlAmounMonthly
    {
        get;
        set;
    }

    public int? WithdrawlCountMonthly
    {
        get;
        set;
    }

    public decimal? WithdrawlLimitMonthly
    {
        get;
        set;
    }

    public int? WithdrawlLimitCountMonthly
    {
        get;
        set;
    }
}
