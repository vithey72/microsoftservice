﻿namespace BNine.DwhIntegration.Models;

using Newtonsoft.Json;
using Application.Aggregates.Users.Models.UserCashback;
using Application.Mappings;

public class DwhUserCashbackHistory
{
    [JsonProperty("customerName")]
    public string CustomerName
    {
        get;
        set;
    }

    [JsonProperty("cashbackInfo")]
    public DwhCashbackInfo[] CashbackInfo
    {
        get;
        set;
    }
}

public class DwhCashbackInfo : IMapTo<CashbackInfoViewModel>
{
    [JsonProperty("periodStart")]
    public DateTime PeriodStart
    {
        get;
        set;
    }

    [JsonProperty("periodEnd")]
    public DateTime PeriodEnd
    {
        get;
        set;
    }

    [JsonProperty("transactionAmount")]
    public decimal TransactionAmount
    {
        get;
        set;
    }

    [JsonProperty("cashbackAmount")]
    public decimal CashbackAmount
    {
        get;
        set;
    }

    [JsonProperty("cashbackMonth")]
    public string CashbackDate
    {
        get;
        set;
    }
}
