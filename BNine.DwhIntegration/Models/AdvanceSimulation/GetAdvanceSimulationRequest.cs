﻿namespace BNine.DwhIntegration.Models.AdvanceSimulation;

using Application.Aggregates.Users.Models.AdvanceSimulation;
using Newtonsoft.Json;

public class GetAdvanceSimulationRequest
{
    [JsonProperty("userId")]
    public string UserId
    {
        get;
        set;
    }

    [JsonProperty("calculatorState")]
    public DwhAdvanceSimulationRequestParameters CalculatorState
    {
        get;
        set;
    }
}
