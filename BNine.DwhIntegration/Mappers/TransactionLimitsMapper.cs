﻿namespace BNine.DwhIntegration.Mappers;

using Application.Aggregates.Users.Models.TransactionLimits;
using Models.TransactionLimits;

public interface ITransactionLimitsMapper
{
    UserTransactionLimitsViewModel MapToUserTransactionLimits(TransactionLimits input);
}

public class TransactionLimitsMapper : ITransactionLimitsMapper
{
    public UserTransactionLimitsViewModel MapToUserTransactionLimits(TransactionLimits input)
    {
        return new UserTransactionLimitsViewModel()
        {
            UserId = input.UserId,
            ExternalCardCurrentLimits = new ExternalCardLimits()
            {
                Pull = new TransactionLimit()
                {
                    DailyLimit = new TransactionLimitBreakdown()
                    {
                        TransactionCount = input.ExternalCard.PullCountDaily,
                        UsedAmount = input.ExternalCard.PullAmountDaily,
                        LimitAmount = input.ExternalCard.PullLimitDaily,
                        TransactionCountLimit = input.ExternalCard.PullLimitCountDaily,
                    },
                    MonthlyLimit = new TransactionLimitBreakdown()
                    {
                        TransactionCount = input.ExternalCard.PullCountMonthly,
                        UsedAmount = input.ExternalCard.PullAmounMonthly,
                        LimitAmount = input.ExternalCard.PullLimitMonthly,
                        TransactionCountLimit = input.ExternalCard.PullLimitCountMonthly,
                    },
                    WeeklyLimit = new TransactionLimitBreakdown()
                    {
                        TransactionCount = input.ExternalCard.PullCountWeekly,
                        UsedAmount = input.ExternalCard.PullAmountWeekly,
                        LimitAmount = input.ExternalCard.PullLimitWeekly,
                        TransactionCountLimit = input.ExternalCard.PullLimitCountWeekly,
                    }
                },
                Push = new TransactionLimit()
                {
                    DailyLimit = new TransactionLimitBreakdown()
                    {
                        TransactionCount = input.ExternalCard.PushCountDaily,
                        UsedAmount = input.ExternalCard.PushAmountDaily,
                        LimitAmount = input.ExternalCard.PushLimitDaily,
                        TransactionCountLimit = input.ExternalCard.PushLimitCountDaily,
                    },
                    MonthlyLimit = new TransactionLimitBreakdown()
                    {
                        TransactionCount = input.ExternalCard.PushCountMonthly,
                        UsedAmount = input.ExternalCard.PushAmounMonthly,
                        LimitAmount = input.ExternalCard.PushLimitMonthly,
                        TransactionCountLimit = input.ExternalCard.PushLimitCountMonthly,
                    },
                    WeeklyLimit = new TransactionLimitBreakdown()
                    {
                        TransactionCount = input.ExternalCard.PushCountWeekly,
                        UsedAmount = input.ExternalCard.PushAmountWeekly,
                        LimitAmount = input.ExternalCard.PushLimitWeekly,
                        TransactionCountLimit = input.ExternalCard.PushLimitCountWeekly,
                    }
                },
            },
            ACHCurrentLimits = new ACHLimits()
            {
                Deposit = new TransactionLimit()
                {
                    DailyLimit = new TransactionLimitBreakdown()
                    {
                        TransactionCount = input.ACH.DepositCountDaily,
                        UsedAmount = input.ACH.DepositAmountDaily,
                        LimitAmount = input.ACH.DepositLimitDaily,
                        TransactionCountLimit = input.ACH.DepositLimitCountDaily,
                    },
                    MonthlyLimit = new TransactionLimitBreakdown()
                    {
                        TransactionCount = input.ACH.DepositCountMonthly,
                        UsedAmount = input.ACH.DepositAmounMonthly,
                        LimitAmount = input.ACH.DepositLimitMonthly,
                        TransactionCountLimit = input.ACH.DepositLimitCountMonthly,
                    },
                    WeeklyLimit = new TransactionLimitBreakdown()
                    {
                        TransactionCount = input.ACH.DepositCountWeekly,
                        UsedAmount = input.ACH.DepositAmountWeekly,
                        LimitAmount = input.ACH.DepositLimitWeekly,
                        TransactionCountLimit = input.ACH.DepositLimitCountWeekly,
                    }
                },
                Withdrawl = new TransactionLimit()
                {
                    DailyLimit = new TransactionLimitBreakdown()
                    {
                        TransactionCount = input.ACH.WithdrawlCountDaily,
                        UsedAmount = input.ACH.WithdrawlAmountDaily,
                        LimitAmount = input.ACH.WithdrawlLimitDaily,
                        TransactionCountLimit = input.ACH.WithdrawlLimitCountDaily,
                    },
                    MonthlyLimit = new TransactionLimitBreakdown()
                    {
                        TransactionCount = input.ACH.WithdrawlCountMonthly,
                        UsedAmount = input.ACH.WithdrawlAmounMonthly,
                        LimitAmount = input.ACH.WithdrawlLimitMonthly,
                        TransactionCountLimit = input.ACH.WithdrawlLimitCountMonthly,
                    },
                    WeeklyLimit = new TransactionLimitBreakdown()
                    {
                        TransactionCount = input.ACH.WithdrawlCountWeekly,
                        UsedAmount = input.ACH.WithdrawlAmountWeekly,
                        LimitAmount = input.ACH.WithdrawlLimitWeekly,
                        TransactionCountLimit = input.ACH.WithdrawlLimitCountWeekly,
                    }
                },
            }
        };
    }
}
