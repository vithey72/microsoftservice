﻿namespace BNine.DwhIntegration.Services;

using Application.Aggregates.Users.Models.AdvanceSimulation;
using Application.Interfaces.DwhIntegration;
using Common;
using Constants;
using Microsoft.Extensions.Options;
using Models.AdvanceSimulation;
using Settings.DwhIntegration;

public class AdvanceSimulationService : IAdvanceSimulationService
{
    private readonly DwhIntegrationSettings _settings;
    private readonly IHttpClientFactory _httpClientFactory;

    public AdvanceSimulationService(
        IOptions<DwhIntegrationSettings> options,
        IHttpClientFactory clientFactory
    )
    {
        _settings = options.Value;
        _httpClientFactory = clientFactory;
    }

    private HttpClient CreateAuthorizedClient()
    {
        var client = _httpClientFactory.CreateClient(HttpClientName.DwhIntegration);

        return client;
    }

    public async Task<DwhAdvanceSimulationViewModel> GetAdvanceSimulation(Guid userId, DwhAdvanceSimulationRequestParameters requestParameters = null)
    {
        var client = CreateAuthorizedClient();

        var url = $"{_settings.AdvanceEmulatorApiUrl}?code={_settings.AdvanceEmulatorAuthCode}";

        var request = new GetAdvanceSimulationRequest()
        {
            UserId = userId.ToString(),
            CalculatorState = requestParameters
        };

        var content = SerializationHelper.GetRequestContent(request);

        var response = await client.PostAsync(url, content);

        response.EnsureSuccessStatusCode();

        var data = await SerializationHelper.GetResponseContent<DwhAdvanceSimulationViewModel>(response);

        return data;
    }
}
