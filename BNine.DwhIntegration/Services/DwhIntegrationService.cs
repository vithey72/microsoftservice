﻿using BNine.Application.Aggregates.Users.Models.TransactionLimits;
using BNine.Application.Interfaces.DwhIntegration;
using BNine.Application.Models.DwhIntegration.LogAdminAction;
using BNine.Constants;
using BNine.Settings.DwhIntegration;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;

namespace BNine.DwhIntegration.Services;


using Application.Aggregates.Users.Models.CardLimits;
using Application.Aggregates.Users.Models.UserAdvancesHistory;
using Application.Aggregates.Users.Models.UserCashback;
using Application.Aggregates.Users.Queries.GetAdvancesHistory;
using AutoMapper;
using Common;
using Mappers;
using Models;
using Models.TransactionLimits;


public class DwhIntegrationService : IDwhIntegrationService
{
    private readonly IMapper _mapper;

    private DwhIntegrationSettings Settings
    {
        get;
    }
    private IHttpClientFactory ClientFactory
    {
        get;
    }

    private ITransactionLimitsMapper TransactionLimitsMapper
    {
        get;
    }

    public DwhIntegrationService(
        IOptions<DwhIntegrationSettings> options,
        IHttpClientFactory clientFactory,
        ITransactionLimitsMapper transactionLimitsMapper,
        IMapper mapper
    )
    {
        _mapper = mapper;
        Settings = options.Value;
        ClientFactory = clientFactory;
        TransactionLimitsMapper = transactionLimitsMapper;
    }

    private HttpClient CreateAuthorizedClient(string xFunctionsKey)
    {
        var client = ClientFactory.CreateClient(HttpClientName.DwhIntegration);

        client.DefaultRequestHeaders.Add("x-functions-key", xFunctionsKey);

        return client;
    }

    public async Task LogAdminAction(AdminActionLogEntry logEntry)
    {
        var client = CreateAuthorizedClient(Settings.LogAdminActionXFunctionsKey);

        var url = $"{Settings.ApiUrl}/log_admin_action?topicName=b9.admin-log";

        var content = SerializationHelper.GetRequestContent(logEntry);

        var response = await client.PostAsync(url, content);

        response.EnsureSuccessStatusCode();
    }

    public async Task<UserTransactionLimitsViewModel> GetUserTransactionLimits(Guid userId)
    {
        var deserializedResult = await FetchLimitsInternal<TransactionLimits>(userId);

        return TransactionLimitsMapper.MapToUserTransactionLimits(deserializedResult);
    }

    public async Task<UserCardLimitsViewModel> GetUserCardLimits(Guid userId)
    {
        var deserializedResult = await FetchLimitsInternal<UserCardLimits>(userId);

        return _mapper.Map<UserCardLimitsViewModel>(deserializedResult); ;
    }

    public async Task<UserCashbackHistoryViewModel> GetUserCashbackInfo(Guid userId)
    {
        var client = CreateAuthorizedClient(Settings.GetUserCashbackInfoXFunctionsKey);

        var url = $"{Settings.ApiUrl}/cashback_info?userId={userId}";

        var response = await client.GetAsync(url);

        response.EnsureSuccessStatusCode();

        var jsonString = await response.Content.ReadAsStringAsync();

        var deserializedResult = JsonConvert.DeserializeObject<DwhUserCashbackHistory>(jsonString);

        return new UserCashbackHistoryViewModel()
        {
            CashbackInfo = deserializedResult?.CashbackInfo.Select(dwhModel => new CashbackInfoViewModel()
            {
                CashbackAmount = dwhModel.CashbackAmount,
                PeriodStart = dwhModel.PeriodStart,
                TransactionAmount = dwhModel.TransactionAmount,
                CashbackDate = dwhModel.CashbackDate
            })
                .ToArray()
        };
    }

    public async Task<AdvancesHistoryViewModel> GetAdvancesHistory(Guid userId)
    {

        var client = CreateAuthorizedClient(Settings.AdvancesHistoryXFunctionsKey);
        var relativePath = "loan_info";
        var baseUrl = $"{Settings.ApiUrl}/{relativePath}";

        var queryParams = System.Web.HttpUtility.ParseQueryString(string.Empty);
        queryParams.Add("userId", userId.ToString());

        var targetUrl =
            new UriBuilder(baseUrl)
        {
            Query = queryParams.ToString()
        }.ToString();

        var response = await client.GetAsync(targetUrl);

        var jsonString = await response.Content.ReadAsStringAsync();

        var deserializedResult = JsonConvert.DeserializeObject<DwhAdvancesHistoryDto>(jsonString)!;

        var mappedHistoryEntries =
            deserializedResult.AdvancesHistoryEntries.Select(e =>
            _mapper.Map<AdminAdvancesHistoryEntry>(e)).ToList();

        return new AdvancesHistoryViewModel()
        {
            AdvancesHistoryEntries = mappedHistoryEntries
        };

    }

    private async Task<T> FetchLimitsInternal<T>(Guid userId)
        where T : UserCardLimits
    {
        var urlPart = typeof(T) switch
        {
            Type t when t == typeof(TransactionLimits) => "support",
            _ => "backend",
        };

        var client = CreateAuthorizedClient(Settings.GetUserLimitsXFunctionsKey);
        var url = $"{Settings.ApiUrl}/user_limit/{urlPart}?userId={userId}";

        var response = await client.GetAsync(url);
        response.EnsureSuccessStatusCode();

        var jsonString = await response.Content.ReadAsStringAsync();
        var deserializedResult = JsonConvert.DeserializeObject<T>(jsonString);

        return deserializedResult;
    }
}
