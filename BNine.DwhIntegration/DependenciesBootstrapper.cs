﻿using BNine.DwhIntegration.Services;
using Microsoft.Extensions.DependencyInjection;

namespace BNine.DwhIntegration;

using Application.Interfaces.DwhIntegration;
using Mappers;

public static class DependenciesBootstrapper
{
    public static IServiceCollection AddDwhIntegration(this IServiceCollection services)
    {
        services.AddTransient<IDwhIntegrationService, DwhIntegrationService>();
        services.AddTransient<IAdvanceSimulationService, AdvanceSimulationService>();
        services.AddScoped<ITransactionLimitsMapper, TransactionLimitsMapper>();

        return services;
    }
}

