﻿namespace BNine.Enums
{
    using System.Runtime.Serialization;

    public enum Language
    {
        [EnumMember(Value = "en")]
        English = 1,

        [EnumMember(Value = "es")]
        Spanish = 2,
    }
}
