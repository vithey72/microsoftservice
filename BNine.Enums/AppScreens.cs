﻿namespace BNine.Enums;

public enum AppScreens
{
    Unknown = 0,

    Wallet = 1,

    Transfers = 2,

    Login = 3,

    Profile = 4,

    Support = 5,

    Cards = 6,
}
