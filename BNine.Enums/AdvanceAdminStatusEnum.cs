﻿namespace BNine.Enums;

public enum AdvanceAdminStatusEnum
{
    Default = 0,
    Overdue = 1,
    Closed = 2,
    ClosedWithOverdue = 3,
    Active = 4,
    WriteOff = 5

}
