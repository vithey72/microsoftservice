﻿namespace BNine.Enums
{
    public enum WebhookSubscriptionAction
    {
        CREATE,
        UPDATE,
        DELETE,
        COMPLETE_EXTERNAL_KYC
    }
}
