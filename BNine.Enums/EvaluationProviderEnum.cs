﻿namespace BNine.Enums;

public enum EvaluationProviderEnum
{
    /// <summary>
    /// Unexpected type.
    /// </summary>
    Unknown = 0,

    /// <summary>
    /// Legacy.
    /// </summary>
    Alloy = 1,

    /// <summary>
    /// New.
    /// </summary>
    Socure = 2,
}
