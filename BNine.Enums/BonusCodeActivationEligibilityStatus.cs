﻿namespace BNine.Enums
{
    using System.Runtime.Serialization;

    public enum BonusCodeActivationEligibilityStatus
    {
        /// <summary>
        /// Default value.
        /// </summary>
        [EnumMember(Value = "unknown")]
        Unknown = 0,

        /// <summary>
        /// All prerequisites are met.
        /// </summary>
        [EnumMember(Value = "ok")]
        Ok = 1,

        /// <summary>
        /// A bonus code has been activated in the past.
        /// </summary>
        [EnumMember(Value = "alreadyActivated")]
        AlreadyActivated = 2,

        /// <summary>
        /// User has not deposited enough funds into his account.
        /// </summary>
        [EnumMember(Value = "insufficientDeposit")]
        InsufficientDeposit = 3,

        /// <summary>
        /// User has been registered before eligible date.
        /// </summary>
        [EnumMember(Value = "oldRegistration")]
        OldRegistration = 4,
    }
}
