﻿namespace BNine.Enums
{
    using System.Runtime.Serialization;

    public enum UserInfoField
    {
        [EnumMember(Value = "email")]
        EMAIL = 1,

        [EnumMember(Value = "phone")]
        PHONE = 2
    }
}
