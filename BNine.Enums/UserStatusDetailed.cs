﻿// ReSharper disable InconsistentNaming
namespace BNine.Enums;

using System.Runtime.Serialization;

/// <summary>
/// A collection of onboarding steps/statuses, more specified than <see cref="UserStatus"/>.
/// </summary>
public enum UserStatusDetailed
{
    Unknown = 0,

    [EnumMember(Value = "pendingPersonalInformation")]
    PendingPersonalInformation = 10,

    [EnumMember(Value = "pendingAddress")]
    PendingAddress = 20,

    [EnumMember(Value = "pendingSSN")]
    PendingSSN = 30,

    [EnumMember(Value = "pendingDocumentVerification")]
    PendingDocumentVerification = 40,

    [EnumMember(Value = "pendingSelfie")]
    PendingSelfie = 45,

    [EnumMember(Value = "pendingSummary")]
    PendingSummary = 50,

    [EnumMember(Value = "pendingAgreement")]
    PendingAgreement = 60,

    [EnumMember(Value = "pendingSubscriptionPayment")]
    PendingSubscriptionPayment = 70,

    [EnumMember(Value = "active")]
    Active = 80,

    [EnumMember(Value = "declined")]
    Declined = 90,

    [EnumMember(Value = "blocked")]
    Blocked = 100,

    [EnumMember(Value = "pendingClosure")]
    PendingClosure = 105,

    /// <summary>
    /// Closed user status differs from blocked by removing all user resources (cards, account, argyle record, etc.)
    /// </summary>
    [EnumMember(Value = "closed")]
    Closed = 110
}
