﻿namespace BNine.Enums
{
    using System.Runtime.Serialization;

    public enum OTPAction
    {
        [EnumMember(Value = "login")]
        Login = 1,

        [EnumMember(Value = "signup")]
        Signup = 2,

        [EnumMember(Value = "recovery")]
        Recovery = 3,

        [EnumMember(Value = "resetPinCode")]
        ResetPinCode = 4
    }
}
