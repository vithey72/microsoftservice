﻿namespace BNine.Enums;

public enum KycStatusEnum
{
    /// <summary>
    /// Unexpected type.
    /// </summary>
    Unknown = 0,

    /// <summary>
    /// User is ok.
    /// </summary>
    Approved = 1,

    /// <summary>
    /// User is not ok.
    /// </summary>
    Denied = 2,

    /// <summary>
    /// Unused.
    /// </summary>
    ManualReview = 3,

    /// <summary>
    /// Try scanning the document again.
    /// </summary>
    Resubmit = 4,
}
