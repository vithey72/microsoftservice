﻿namespace BNine.Enums;

using System.Runtime.Serialization;

public enum AprilApplicationSubtype
{
    Unknown = 0,

    [EnumMember(Value = "FILING")]
    Filing = 1,

    [EnumMember(Value = "FILING_MINIFLOW")]
    FilingMiniFlow = 2,

    [EnumMember(Value = "FILING_SANDBOX")]
    FilingSandbox = 3,

    [EnumMember(Value = "ESTIMATOR")]
    Estimator = 4,

    [EnumMember(Value = "ESTIMATOR_SANDBOX")]
    EstimatorSandbox = 5,

    [EnumMember(Value = "OPTIMIZER")]
    Optimizer = 6,

    [EnumMember(Value = "OPTIMIZER_SANDBOX")]
    OptimizerSandbox = 7,
}
