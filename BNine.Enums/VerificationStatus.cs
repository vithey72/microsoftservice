﻿namespace BNine.Enums
{
    public enum VerificationStatus
    {
        NOTSTARTED = 0,
        PENDING = 1,
        APPROVED = 2,
        // TODO: ask about real failed status
        FAILED = 3,
        IN_PROGRESS = 4,
    }
}
