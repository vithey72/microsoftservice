﻿namespace BNine.Enums;

using System.Runtime.Serialization;

public enum AprilApplicationMode
{
    Unknown = 0,

    [EnumMember(Value = "FILING")]
    Filing = 1,

    [EnumMember(Value = "ESTIMATOR")]
    Estimator = 2,

    [EnumMember(Value = "OPTIMIZER")]
    Optimizer = 3,
}
