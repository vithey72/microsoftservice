﻿namespace BNine.Enums;

using System.Runtime.Serialization;

public enum AdminRole
{
    [EnumMember(Value = "Admin")]
    Admin = 1,

    [EnumMember(Value = "Agent")]
    Agent = 2,

    [EnumMember(Value = "LightAgent")]
    LightAgent = 3,

    [EnumMember(Value = "Viewer")]
    Viewer = 4
}
