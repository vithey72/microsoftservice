﻿namespace BNine.Enums;

using System.ComponentModel;

public enum BankAccountTypeEnum
{
    [Description("Checking Account")]
    Main = 0,
    [Description("Rewards Account")]
    Reward = 1,
}
