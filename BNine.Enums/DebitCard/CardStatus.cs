﻿namespace BNine.Enums.DebitCard
{
    using System.Runtime.Serialization;

    public enum CardStatus
    {
        /// <summary>
        /// If status could not be parsed.
        /// </summary>
        Unknown = 0,

        [EnumMember(Value = "ordered")]
        ORDERED = 1,

        [EnumMember(Value = "rejected")]
        REJECTED = 2,

        [EnumMember(Value = "active")]
        ACTIVE = 3,

        [EnumMember(Value = "suspended")]
        SUSPENDED = 4,

        [EnumMember(Value = "terminated")]
        TERMINATED = 5,

        [EnumMember(Value = "created")]
        CREATED = 6,

        REPLACED = 7,

        LOST_STOLEN = 8,

        MARK_AS_LOST = 9,
    }
}
