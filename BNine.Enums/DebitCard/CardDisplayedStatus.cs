﻿namespace BNine.Enums.DebitCard
{
    using System.Runtime.Serialization;

    /// <summary>
    /// Represents card's state in our workflow, used by clients for rendering.
    /// </summary>
    public enum CardDisplayedStatus
    {
        /// <summary>
        /// Default value, should not be returned.
        /// </summary>
        [EnumMember(Value = "unknown")]
        Unknown = 0,

        /// <summary>
        /// There isn't a card.
        /// </summary>
        [EnumMember(Value = "noCard")]
        NoCard = 1,

        /// <summary>
        /// Card is being made and sent to customer. It is active but for online operations only.
        /// </summary>
        [EnumMember(Value = "activeOnlineOnly")]
        ActiveOnlineOnly = 2,

        /// <summary>
        /// Card is active and fully usable. User can use it for physical operations and Apple Pay and such.
        /// </summary>
        [EnumMember(Value = "active")]
        Active = 3,

        /// <summary>
        /// Operations by card have been suspended.
        /// </summary>
        [EnumMember(Value = "locked")]
        Locked = 4,

        /// <summary>
        /// MBanq is processing a card reorder request.
        /// </summary>
        [EnumMember(Value = "processingReorder")]
        ProcessingReorder = 5,

        /// <summary>
        /// Replacement card is being made and delivered to the customer. It is active for online operations only.
        /// </summary>
        [EnumMember(Value = "reordered")]
        Reordered = 6,

        /// <summary>
        /// Card could not be physically activated until it would shipped.
        /// </summary>
        [EnumMember(Value = "notActivatable")]
        NotActivatable = 7,

        /// <summary>
        /// Card is created and waiting for the mandatory PIN code set.
        /// </summary>
        [EnumMember(Value = "expectingSetPin")]
        ExpectingSetPin = 8,

    }
}
