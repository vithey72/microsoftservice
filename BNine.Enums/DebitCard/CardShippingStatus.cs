﻿namespace BNine.Enums.DebitCard
{
    using System.Runtime.Serialization;

    public enum CardShippingStatus
    {
        [EnumMember(Value = "returned")]
        RETURNED = 1,

        [EnumMember(Value = "inTransit")]
        IN_TRANSIT = 2,

        [EnumMember(Value = "delivered")]
        DELIVERED = 3,

        [EnumMember(Value = "received")]
        RECEIVED = 4,

        [EnumMember(Value = "activated")]
        ACTIVATED = 5
    }
}
