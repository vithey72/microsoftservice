﻿namespace BNine.Enums.DebitCard;
using System.Runtime.Serialization;

public enum CardType
{
    Unknown = 0,

    [EnumMember(Value = "physicalDebitCard")]
    PhysicalDebit = 1,

    [EnumMember(Value = "virtualDebitCard")]
    VirtualDebit = 2,

    [EnumMember(Value = "creditCard")]
    Credit = 3,
}
