﻿namespace BNine.Enums.Transfers;

/// <summary>
/// Types present in the domain model of our app
/// </summary>
public enum TransferDisplayType
{
    Unknown = 0,

    Atm = 1,

    // Deposits
    AdvanceIssued = 11,
    ReceivedTransferToCard = 12,
    CheckCashing = 13,
    AchReceived = 14,
    ReceivedFromB9Client = 15,
    PullFromExternalCard = 16,
    Rewards = 17,
    DefaultDeposit = 18,

    // Withdrawals
    AdvanceRepayment = 21,
    AchSent = 22,
    SentToB9Client = 23,
    ApprovedCardAuth = 24,
    PushToExternalCard = 25,
    DefaultWithdrawal = 26,

    // Fees
    AtmFee = 31,
    ForeignTransactionFee = 32,
    CardTransferFee = 33,
    ExternalWalletFee = 34,
    MonthlyB9Fee = 35,
    DefaultFee = 36,
    VipSupportFee = 37,
    AdvanceUnfreezeFee = 38,
    PhysicalCardDeliveryFee = 39,
    AnnualFee = 133,
    AdvanceBoostFee = 130,
    AfterAdvanceTip = 131,
    CreditScoreFee = 132,


    // Other
    OtherCardTransaction = 41,
    Declined = 42,
    B9Bonus = 43,
    B9Cashback = 44,
    Returned = 45,
    ReturnedPaycharge = 46,

    OtherDefaultTransaction = 255,
}
