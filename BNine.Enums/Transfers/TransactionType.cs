﻿namespace BNine.Enums.Transfers
{
    using System.Runtime.Serialization;

    public enum TransactionType
    {
        /// <summary>
        /// B9 card.
        /// </summary>
        [EnumMember(Value = "card")]
        Card = 1,

        /// <summary>
        /// Bank account.
        /// </summary>
        [EnumMember(Value = "account")]
        Account = 2,
    }
}
