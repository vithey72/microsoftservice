﻿namespace BNine.Enums.Transfers
{
    using System.Runtime.Serialization;

    public enum TransferDirectionSimplified
    {
        [EnumMember(Value = "incoming")]
        Incoming = 1,

        [EnumMember(Value = "outgoing")]
        Outgoing = 2
    }
}
