﻿namespace BNine.Enums.Transfers.Controller;

public enum TransferCommonDetailsType
{
    Deposit,
    SendMoney
}
