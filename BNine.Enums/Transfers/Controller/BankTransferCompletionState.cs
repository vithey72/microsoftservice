﻿namespace BNine.Enums.Transfers.Controller;

using System.Runtime.Serialization;

public enum BankTransferCompletionState
{
    /// <summary>
    /// Success.
    /// </summary>
    [EnumMember(Value = "completed")]
    Completed = 1,

    /// <summary>
    /// Processing in bank.
    /// </summary>
    [EnumMember(Value = "pending")]
    Pending = 2,

    /// <summary>
    /// Opration declined
    /// </summary>
    [EnumMember(Value = "declined")]
    Declined = 3,

    /// <summary>
    /// Operation was reverted.
    /// </summary>
    [EnumMember(Value = "returned")]
    Returned = 4,
}
