﻿namespace BNine.Enums.Transfers.Controller;

using System.Runtime.Serialization;

/// <summary>
/// Refer to page https://bninecom.atlassian.net/wiki/spaces/MAYF/pages/949714954/US+-+14.1.+Transaction+history
/// </summary>
public enum BankTransferIconOption
{
    [EnumMember(Value = "defaultCardOk")]
    DefaultCardOk = 1,

    [EnumMember(Value = "cardWarning")]
    CardWarning = 2,

    [EnumMember(Value = "atm")]
    Atm = 3,

    [EnumMember(Value = "membership")]
    Membership = 4,

    [EnumMember(Value = "fee")]
    Fee = 5,

    [EnumMember(Value = "advance")]
    Advance = 6,

    [EnumMember(Value = "bank")]
    Bank = 7,

    [EnumMember(Value = "person")]
    Person = 8,

    [EnumMember(Value = "moneyBag")]
    MoneyBag = 9,

    [EnumMember(Value = "checkmark")]
    Checkmark = 10,

    [EnumMember(Value = "cross")]
    Cross = 11,

    [EnumMember(Value = "merchant")]
    Merchant = 255,
}
