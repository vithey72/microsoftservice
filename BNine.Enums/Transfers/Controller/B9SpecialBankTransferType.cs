﻿namespace BNine.Enums.Transfers.Controller;

using System.Runtime.Serialization;

public enum B9SpecialBankTransferType
{
    /// <summary>
    /// Monthly fee charge.
    /// </summary>
    [EnumMember(Value = "charge")]
    Charge = 1,

    /// <summary>
    /// Advance issuing or repaying.
    /// </summary>
    [EnumMember(Value = "advance")]
    Advance = 2,

    /// <summary>
    /// Cashback feature.
    /// </summary>
    [EnumMember(Value = "cashback")]
    Cashback = 3,

    /// <summary>
    /// Transactions that recognized as payroll
    /// </summary>
    [EnumMember(Value = "payroll")]
    Payroll = 4,
}
