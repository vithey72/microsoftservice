﻿namespace BNine.Enums.Transfers.Controller;

public enum PaymentEntity
{
    /// <summary>
    /// "ANY" filter.
    /// </summary>
    Unknown = 0,

    /// <summary>
    /// B9 card.
    /// </summary>
    Card = 1,

    /// <summary>
    /// Bank account.
    /// </summary>
    Account = 2,
}
