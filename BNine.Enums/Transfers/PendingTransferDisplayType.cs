﻿namespace BNine.Enums.Transfers;

/// <summary>
/// Pending transfers present in the domain model of our app.
/// </summary>
public enum PendingTransferDisplayType
{
    Unknown = 0,

    Atm = 1,

    CardAuthorization = 2,

    Internal = 3,

    Ach = 4,

    CheckCashing = 5,

    PushToExternal = 6,
}

public static class PendingTransferDisplayTypeExtensions
{
    public static TransferDisplayType ConvertToTransferDisplayType(this PendingTransferDisplayType value)
    {
        switch (value)
        {
            case PendingTransferDisplayType.Ach:
                return TransferDisplayType.AchSent;
            case PendingTransferDisplayType.Internal:
                return TransferDisplayType.SentToB9Client;
            case PendingTransferDisplayType.PushToExternal:
                return TransferDisplayType.PushToExternalCard;

            default:
                return TransferDisplayType.Unknown;
        }
    }
}
