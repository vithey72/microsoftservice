﻿namespace BNine.Enums.Transfers
{
    using System.Runtime.Serialization;

    public enum TransferDirection
    {
        [EnumMember(Value = "CREDIT")]
        Credit = 1,

        [EnumMember(Value = "DEBIT")]
        Debit = 2
    }
}
