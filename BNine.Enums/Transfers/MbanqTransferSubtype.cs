﻿namespace BNine.Enums.Transfers
{
    public enum MbanqTransferSubtype
    {
        // ReSharper disable InconsistentNaming
        NONE = 0,
        CARD_TRANSACTION = 1,
        SETTLEMENT_RETURN_CREDIT = 2,
        LOAN_DISBURSEMENT = 3,
        LOAN_REPAYMENT = 4,
        CARD_AUTHORIZE_PAYMENT = 5,
        DOMESTIC_ATM_WITHDRAWAL_FEE = 6,
        INTERNATIONAL_ATM_WITHDRAWAL_FEE = 7,
        INTERNATIONAL_TRANSACTION_FEE = 8,
        EXTERNAL_CARD_PUSH_TRANSACTION_FEE = 9,
        EXTERNAL_CARD_PULL_TRANSACTION_FEE = 10,
        PROVISIONAL_CREDIT = 11,
        PROVISIONAL_CREDIT_REVERSAL = 12,
        MCC_CHARGE = 13,
        TRANSFER_FEE = 14,
    }
}
