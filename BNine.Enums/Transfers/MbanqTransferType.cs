﻿namespace BNine.Enums.Transfers
{
    public enum MbanqTransferType
    {
        // ReSharper disable InconsistentNaming
        INVALID = 0,
        DEPOSIT = 1,
        WITHDRAWAL = 2,
        INTEREST_POSTING = 3,
        WITHDRAWAL_FEE = 4,
        ANNUAL_FEE = 5,
        WAIVE_CHARGES = 6,
        PAY_CHARGE = 7,
        DIVIDEND_PAYOUT = 8,
        INITIATE_TRANSFER = 12,
        APPROVE_TRANSFER = 13,
        WITHDRAW_TRANSFER = 14,
        REJECT_TRANSFER = 15,
        WRITTEN_OFF = 16,
        OVERDRAFT_INTEREST = 17,
        WITHHOLD_TAX = 18,
        ESCHEAT = 19,
        AMOUNT_HOLD = 20,
        AMOUNT_RELEASE = 21,
        INTEREST_PAYABLE_ACCRUED = 22,
        OVERDRAFT_INTEREST_RECEIVABLE_ACCRUED = 23,
        PAY_CHARGE_REVERSAL = 24,
    }
}
