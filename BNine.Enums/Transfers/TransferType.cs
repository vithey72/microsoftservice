﻿namespace BNine.Enums.Transfers
{
    using System.Runtime.Serialization;

    public enum TransferType
    {
        [EnumMember(Value = "Internal")]
        Internal = 1,

        [EnumMember(Value = "ACH")]
        ACH = 2,

        [EnumMember(Value = "WIRE")]
        Wire = 3,

        [EnumMember(Value = "Account")]
        Account = 4,

        [EnumMember(Value = "CARD")]
        CARD = 5,

        [EnumMember(Value = "RDC")]
        RDC = 6,
    }
}
