﻿namespace BNine.Enums;

using System.Runtime.Serialization;

public enum EventSourcePlatform
{
    /// <summary>
    /// Default value.
    /// </summary>
    [EnumMember(Value = "unknown")]
    Unknown = 0,

    /// <summary>
    /// Event was generated on backend.
    /// </summary>
    [EnumMember(Value = "backend")]
    Backend = 1,

    /// <summary>
    /// Event was sent via API from iOS.
    /// </summary>
    [EnumMember(Value = "iOS")]
    iOS = 2,

    /// <summary>
    /// Event was sent via API from Android.
    /// </summary>
    [EnumMember(Value = "android")]
    Android = 3,
}
