﻿namespace BNine.Enums
{
    using System.Runtime.Serialization;

    public enum HandleCardEventCommand
    {
        [EnumMember(Value = "Activate")]
        ACTIVATE = 1,

        [EnumMember(Value = "Suspend")]
        SUSPEND = 2,

        [EnumMember(Value = "Renew")]
        RENEW = 3,
    }
}
