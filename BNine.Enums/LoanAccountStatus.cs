﻿namespace BNine.Enums
{
    using System.Runtime.Serialization;

    public enum LoanAccountStatus
    {
        [EnumMember(Value = "active")]
        Active = 1,

        [EnumMember(Value = "closed")]
        Closed = 2
    }
}
