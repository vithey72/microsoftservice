﻿namespace BNine.Enums;

public enum LinkFollowMode
{
    Default = 0,

    WebView = 1,

    BrowserPreview = 2,
}
