﻿namespace BNine.Enums
{
    public enum UserQuestionType
    {
        Unknown = 0,

        OpenEnded = 1,

        ClosedWithSingleChoice = 2,

        ClosedWithMultipleChoices = 3,
    }
}
