﻿namespace BNine.Enums;

public enum MarketingThirdPartyType
{
    April,
    QuickBooks
}
