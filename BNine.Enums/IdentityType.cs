﻿namespace BNine.Enums
{
    using System.Runtime.Serialization;

    public enum IdentityType
    {
        [EnumMember(Value = "ssn")]
        SSN = 1,

        [EnumMember(Value = "itin")]
        ITIN = 2
    }
}
