﻿namespace BNine.Enums;

public enum HowItWorksScreenType
{
    ArgyleSwitched,
    ArgyleNotSwitched,
    CommonVirtualCard
}
