﻿namespace BNine.Enums
{
    using System.Runtime.Serialization;

    public enum RequestProcessingStatus
    {
        [EnumMember(Value = "opened")]
        OPENED = 1,

        [EnumMember(Value = "completed")]
        COMPLETED = 2
    }
}
