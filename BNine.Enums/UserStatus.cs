﻿// ReSharper disable InconsistentNaming
namespace BNine.Enums
{
    using System.Runtime.Serialization;

    public enum UserStatus
    {
        /// <summary>
        /// User was created in DB and is expected to provide their name and email.
        /// </summary>
        [EnumMember(Value = "pendingPersonalInformation")]
        PendingPersonalInformation = 1,

        /// <summary>
        /// User is expected to provide their address.
        /// </summary>
        [EnumMember(Value = "pendingAddress")]
        PendingAddress = 2,

        /// <summary>
        /// User is expected to provide their SSN.
        /// </summary>
        [EnumMember(Value = "pendingSSN")]
        PendingSSN = 3,

        /// <summary>
        /// User is expected to provide their ID or passport.
        /// </summary>
        [EnumMember(Value = "pendingDocumentVerification")]
        PendingDocumentVerification = 4,

        /// <summary>
        /// User is expected to be sent into KYC process with all details entered before.
        /// </summary>
        [EnumMember(Value = "pendingSummary")]
        PendingSummary = 5,

        /// <summary>
        /// User has passed the KYC and is expected to be set up in MBanq.
        /// </summary>
        [EnumMember(Value = "pendingAgreement")]
        PendingAgreement = 6,

        /// <summary>
        /// User was created in MBanq, but still needs to perform some mandatory actions to start using the app.
        /// </summary>
        [EnumMember(Value = "preActivation")]
        PreActivation = 7,

        /// <summary>
        /// User can use the app.
        /// </summary>
        [EnumMember(Value = "active")]
        Active = 8,

        /// <summary>
        /// User did not pass the KYC. All the records except User entity are purged.
        /// They cannot try and register again. 
        /// </summary>
        [EnumMember(Value = "declined")]
        Declined = 9,

        /// <summary>
        /// User is temporarily disabled for security reasons.
        /// </summary>
        [EnumMember(Value = "blocked")]
        Blocked = 10,

        /// <summary>
        /// User is permanently disabled in our system with all the records (except User entity) purged and
        /// external resources removed (MBanq client entity, Argyle connection, etc.). They cannot register again.
        /// </summary>
        [EnumMember(Value = "closed")]
        Closed = 11
    }
}
