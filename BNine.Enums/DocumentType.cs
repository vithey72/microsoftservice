﻿namespace BNine.Enums
{
    using System.Runtime.Serialization;

    public enum DocumentType
    {
        [EnumMember(Value = "passport")]
        Passport = 1,

        [EnumMember(Value = "foreignDriverLicense")]
        ForeignDriverLicense = 2,

        [EnumMember(Value = "consularId")]
        ConsularId = 3,

        [EnumMember(Value = "visa")]
        Visa = 4,

        [EnumMember(Value = "other")]
        Other = 5,

        [EnumMember(Value = "ssn")]
        SSN = 6,

        [EnumMember(Value = "driverLicense")]
        DriverLicense = 7,

        [EnumMember(Value = "identification")]
        StateIdentityCard = 8,

        [EnumMember(Value = "workPermitCard")]
        WorkPermitCard = 9,

        [EnumMember(Value = "permanentResidentCard")]
        PermanentResidentCard = 10,

        [EnumMember(Value = "foreignPassport")]
        ForeignPassport = 11,

        [EnumMember(Value = "itin")]
        ITIN = 12,

        [EnumMember(Value = "commercial-license")]
        CommercialDriverLicense = 13,

        [EnumMember(Value = "drivers-license-permit")]
        DriverLicensePermit = 14,
    }
}
