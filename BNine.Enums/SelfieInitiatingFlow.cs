﻿namespace BNine.Enums;

public enum SelfieInitiatingFlow
{
    ExternalCard,
    Onboarding,
    Unknown
}
