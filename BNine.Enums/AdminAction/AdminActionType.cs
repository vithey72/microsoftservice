﻿namespace BNine.Enums
{
    using System.ComponentModel;
    using System.Runtime.Serialization;

    public enum AdminActionType
    {
        [Description("Change Address")]
        [EnumMember(Value = "changeAddress")]
        ChangeAddress = 1,

        [Description("Change Phone Number")]
        [EnumMember(Value = "changePhoneNumber")]
        ChangePhoneNumber = 2,

        [Description("Change Name")]
        [EnumMember(Value = "changeName")]
        ChangeName = 3,

        [Description("Change Email")]
        [EnumMember(Value = "changeEmail")]
        ChangeEmail = 4,

        [Description("Block Client")]
        [EnumMember(Value = "blockClient")]
        BlockClient = 5,

        [Description("Unblock Client")]
        [EnumMember(Value = "unblockClient")]
        UnblockClient = 6,

        [Description("Delete Argyle Data")]
        [EnumMember(Value = "deleteArgyleData")]
        DeleteArgyleData = 7,

        [Description("Block Advance")]
        [EnumMember(Value = "blockAdvance")]
        BlockAdvance = 8,

        [Description("Close Account")]
        [EnumMember(Value = "closeAccount")]
        CloseAccount = 9
    }
}
