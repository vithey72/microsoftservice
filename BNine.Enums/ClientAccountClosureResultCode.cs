﻿namespace BNine.Enums;

public enum ClientAccountClosureResultCode
{
    Unknown = 0,

    DisbursedAdvance = 1,

    NegativeBalance = 2,

    PositiveBalance = 3,

    PendingTransfersPresent = 4,

    ArgyleConnected = 5,
}
