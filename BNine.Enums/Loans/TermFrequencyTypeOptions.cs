﻿namespace BNine.Enums.Loans
{
    public enum TermFrequencyType
    {
        Days,
        Weeks,
        Months,
        Years
    }
}
