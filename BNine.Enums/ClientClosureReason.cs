﻿namespace BNine.Enums
{
    using System.Runtime.Serialization;

    public enum ClientClosureReason
    {
        [EnumMember(Value = "blacklisted")]
        Blacklisted = 23,

        [EnumMember(Value = "deceased")]
        Deceased = 24,

        [EnumMember(Value = "transferred")]
        Transferred = 25,

        [EnumMember(Value = "left")]
        Left = 26,

        [EnumMember(Value = "other")]
        Other = 27
    }
}
