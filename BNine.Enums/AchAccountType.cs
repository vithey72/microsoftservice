﻿namespace BNine.Enums;

public enum AchAccountType
{
    /// <summary>
    /// Aka Current.
    /// </summary>
    Checking = 0,

    Savings = 1,
}
