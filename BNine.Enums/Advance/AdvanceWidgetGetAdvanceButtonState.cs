﻿namespace BNine.Enums.Advance;

using System.Runtime.Serialization;

public enum AdvanceWidgetGetAdvanceButtonState
{
    [EnumMember(Value = "getAdvanceFallback")]
    Fallback = 0,

    [EnumMember(Value = "getAdvanceWaitingForFirstPaycheck")]
    WaitingForFirstPaycheck = 1,

    [EnumMember(Value = "getAdvanceWaitingForArgyleData")]
    WaitingForArgyleData = 2,

    [EnumMember(Value = "getAdvancePayAllocated")]
    PayAllocated = 3,

    [EnumMember(Value = "getAdvanceCashOut")]
    CashOut = 4,

    [EnumMember(Value = "getAdvanceExtension")]
    Extension = 5,

    [EnumMember(Value = "getAdvanceDelayedBoostInfo")]
    DelayedBoostInfo = 6,

    [EnumMember(Value = "getAdvanceHidden")]
    Hidden = 127,
}
