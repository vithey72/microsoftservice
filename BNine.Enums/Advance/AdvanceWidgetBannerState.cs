﻿namespace BNine.Enums.Advance;

using System.Runtime.Serialization;

public enum AdvanceWidgetBannerState
{
    // No banner
    [EnumMember(Value = "default")]
    Default = 0,

    [EnumMember(Value = "attentionRequiredNewUser")]
    AttentionRequiredNewUser = 10,
    [EnumMember(Value = "attentionRequiredInsufficientDeposit")]
    AttentionRequiredInsufficientDeposit = 11,

    [EnumMember(Value = "waitingForArgyleData")]
    WaitingForArgyleData = 20,

    [EnumMember(Value = "waitingForFirstPaycheck")]
    WaitingForFirstPaycheck = 30,

    [EnumMember(Value = "nextAdvanceMotivation")]
    NextAdvanceMotivation = 40,
    [EnumMember(Value = "nextAdvanceMotivationBeforeDeposit")]
    NextAdvanceMotivationBeforeDeposit = 41,
}
