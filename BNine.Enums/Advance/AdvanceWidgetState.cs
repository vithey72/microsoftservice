﻿namespace BNine.Enums.Advance;

using System.Runtime.Serialization;

public enum AdvanceWidgetState
{
    [EnumMember(Value = "noAdvance")]
    NoAdvance = 0,

    [EnumMember(Value = "advanceAvailable")]
    AdvanceAvailable = 10,

    [EnumMember(Value = "boostAvailable")]
    BoostAvailable = 11,

    [EnumMember(Value = "advanceDisbursed")]
    AdvanceDisbursed = 20,

    [EnumMember(Value = "extensionAvailable")]
    ExtensionAvailable = 21,

    [EnumMember(Value = "repaymentOverdue")]
    RepaymentOverdue = 30,

    [EnumMember(Value = "delayedBoost")]
    DelayedBoost = 40,
}
