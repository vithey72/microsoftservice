﻿namespace BNine.Enums.CheckCashing
{
    using System.Runtime.Serialization;

    public enum CheckType
    {
        Unknown = 0,

        [EnumMember(Value = "payroll")]
        Payroll = 1,

        [EnumMember(Value = "personal")]
        Personal = 2,

        [EnumMember(Value = "insurance")]
        Insurance = 3,

        [EnumMember(Value = "corporate")]
        Corporate = 4,

        [EnumMember(Value = "government")]
        Government = 5,

        [EnumMember(Value = "money_order")]
        MoneyOrder = 6,

        [EnumMember(Value = "other")]
        Other = 7,
    }
}
