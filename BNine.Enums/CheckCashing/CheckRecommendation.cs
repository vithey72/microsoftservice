﻿namespace BNine.Enums.CheckCashing
{
    using System.Runtime.Serialization;

    public enum CheckRecommendation
    {
        Unknown = 0,

        [EnumMember(Value = "accept")]
        Accept = 1,

        [EnumMember(Value = "review")]
        Review = 2,

        [EnumMember(Value = "decline")]
        Decline = 3,
    }
}
