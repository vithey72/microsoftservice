﻿namespace BNine.Enums.CheckCashing
{
    public enum CheckCashingTransactionStatus
    {
        Unknown = 0,
        Created = 10,
        RejectedByAmount = 11,
        UserAbandoned = 12,
        DetailsConfirmedByUser = 30,
        PreApproved = 50,
        ApprovementRequested = 60,
        Approved = 80,
        ApprovementRejected = 81,
        ClearingFailed = 92,
        ClearingSuccess = 100,
        TransactionExpired = 255,
    }
}
