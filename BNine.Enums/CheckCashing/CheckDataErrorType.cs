﻿namespace BNine.Enums.CheckCashing
{
    public enum CheckDataErrorType
    {
        /// <summary>
        /// Default value.
        /// </summary>
        Unknown = 0,

        /// <summary>
        /// Images did not pass the usability check.
        /// </summary>
        AlltrustOcrFail = 1,

        /// <summary>
        /// Amount was parsed as $0.
        /// </summary>
        AmountUnrecognized = 2,

        /// <summary>
        /// MICR line wasn't recognized.
        /// </summary>
        MicrUnrecognized = 3,

        /// <summary>
        /// MICR was probably recognized incorrectly.
        /// </summary>
        AlltrustMicrParseFail = 4,

        /// <summary>
        /// Check caused exception.
        /// </summary>
        Exception = 99,
    }
}
