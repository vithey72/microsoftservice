﻿namespace BNine.Enums.CheckCashing
{
    public enum UserReputationType
    {
        Unknown = 0,

        None = 1,

        EligibleForInstantCashing = 2,

        Bad = 3,
    }
}
