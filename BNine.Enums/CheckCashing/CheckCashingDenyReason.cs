﻿namespace BNine.Enums.CheckCashing
{
    public enum CheckCashingDenyReason
    {
        Unknown = 0,

        /// <summary>
        /// Amount on check is too great.
        /// </summary>
        CheckAmountGreaterThanAcceptable = 1,

        /// <summary>
        /// Amount recognized from check is less than user-passed amount.
        /// </summary>
        CheckAmountMismatch = 2,

        /// <summary>
        /// Alltrust automatically rejected the check.
        /// </summary>
        CheckRejected = 3,

        /// <summary>
        /// Check has already been cashed.
        /// </summary>
        CheckDuplicate = 4,

        /// <summary>
        /// Unexpected behavior encountered.
        /// </summary>
        Exception = 99,
    }
}
