﻿namespace BNine.Enums.CheckCashing
{
    using System.Runtime.Serialization;

    public enum CheckReviewResult
    {
        Unknown = 0,

        [EnumMember(Value = "accepted")]
        Accepted = 1,

        [EnumMember(Value = "cancelled")]
        Cancelled = 2,

        [EnumMember(Value = "preapproved")]
        PreApproved = 3,

        [EnumMember(Value = "review")]
        Review = 4,
    }
}
