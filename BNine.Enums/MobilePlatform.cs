﻿namespace BNine.Enums
{
    using System.Runtime.Serialization;

    public enum MobilePlatform
    {
        [EnumMember(Value = "iOs")]
        iOs = 1,

        [EnumMember(Value = "android")]
        Android = 2
    }
}
