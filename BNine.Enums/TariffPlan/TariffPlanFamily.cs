﻿namespace BNine.Enums.TariffPlan
{
    public enum TariffPlanFamily
    {
        Unknown = 0,

        /// <summary>
        /// Aka Basic.
        /// </summary>
        Advance = 1,

        Premium = 2,
    }
}
