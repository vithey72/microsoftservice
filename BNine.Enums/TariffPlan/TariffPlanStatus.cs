﻿namespace BNine.Enums.TariffPlan
{
    public enum TariffPlanStatus
    {
        Active = 1,
        Inactive,
        Upcoming,
        Overdue
    }
}
