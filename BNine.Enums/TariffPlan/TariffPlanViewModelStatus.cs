﻿namespace BNine.Enums.TariffPlan
{
    public enum TariffPlanViewModelStatus
    {
        Active = 1,
        Inactive,
        Upcoming
    }
}
