﻿namespace BNine.Enums;

public enum PartnerApp
{
    Default = 0,

    MBanqApp = 1,

    Eazy = 2,

    Poetryy = 3,

    USNational = 4,

    Qorbis = 5,

    ManaPacific = 6,

    P2P = 7,

    Booq = 8,

    PayHammer = 9
}
