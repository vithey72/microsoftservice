﻿namespace BNine.Enums
{
    public enum UserBlockAction
    {
        Unblock,
        Block
    }
}
