﻿namespace BNine.Enums;

using System.Runtime.Serialization;

public enum DocumentSide
{
    [EnumMember(Value = "default")]
    Default = 0,

    [EnumMember(Value = "frontSide")]
    FrontSide = 1,

    [EnumMember(Value = "backSide")]
    BackSide = 2
}

