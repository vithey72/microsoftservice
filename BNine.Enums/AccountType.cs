﻿namespace BNine.Enums
{
    public enum AccountType
    {
        Loan = 1,
        Saving = 2
    }
}
