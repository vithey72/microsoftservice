﻿namespace BNine.Enums
{
    using System.Runtime.Serialization;

    public enum ArgyleStatus
    {
        [EnumMember(Value = "unknown")]
        Unknown = 0,

        [EnumMember(Value = "active")]
        Active = 1,
    }
}
