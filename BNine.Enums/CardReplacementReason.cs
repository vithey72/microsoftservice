﻿namespace BNine.Enums
{
    using System.Runtime.Serialization;

    public enum CardReplacementReason
    {
        [EnumMember(Value = "Lost")]
        LOST = 1,

        [EnumMember(Value = "Stolen")]
        STOLEN = 2,

        [EnumMember(Value = "Damaged")]
        DAMAGED = 3,

        [EnumMember(Value = "Card_Not_Received")]
        CARD_NOT_RECEIVED = 4,

        [EnumMember(Value = "Captured_By_Atm")]
        CAPTURED_BY_ATM = 5,
    }
}
