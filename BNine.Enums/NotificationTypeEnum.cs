﻿namespace BNine.Enums
{
    public enum NotificationTypeEnum
    {
        Information = 1,
        Marketing = 2,
        Transaction = 3,
        Others = 4,
    }
}
