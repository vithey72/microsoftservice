﻿namespace BNine.EveryoneApi
{
    using BNine.Application.Interfaces;
    using Microsoft.Extensions.DependencyInjection;

    public static class DependenciesBootstrapper
    {
        public static IServiceCollection AddEveryoneApi(this IServiceCollection services)
        {
            services.AddTransient<IMobilePhoneService, EveryoneApiService>();

            return services;
        }
    }
}
