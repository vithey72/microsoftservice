﻿namespace BNine.EveryoneApi.Models
{
    using Newtonsoft.Json;

    public class Data
    {
        [JsonProperty("expanded_name")]
        public ExpandedName ExpandedName
        {
            get; set;
        }

        [JsonProperty("name")]
        public string Name
        {
            get; set;
        }
    }
}
