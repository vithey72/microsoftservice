﻿namespace BNine.EveryoneApi
{
    using BNine.EveryoneApi.Models;
    using Newtonsoft.Json;

    public class MobileData
    {
        [JsonProperty("data")]
        public Data Data
        {
            get; set;
        }

        [JsonProperty("number")]
        public string Number
        {
            get; set;
        }

        [JsonProperty("status")]
        public bool Status
        {
            get; set;
        }

        [JsonProperty("type")]
        public string Type
        {
            get; set;
        }
    }
}
