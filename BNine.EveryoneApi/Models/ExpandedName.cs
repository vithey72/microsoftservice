﻿namespace BNine.EveryoneApi
{
    using Newtonsoft.Json;

    public class ExpandedName
    {
        [JsonProperty("first")]
        public string First
        {
            get; set;
        }

        [JsonProperty("last")]
        public string Last
        {
            get; set;
        }
    }
}
