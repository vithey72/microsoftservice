﻿namespace BNine.EveryoneApi
{
    using System.Net.Http;
    using System.Threading.Tasks;
    using BNine.Application.Interfaces;
    using BNine.Common;
    using BNine.Constants;
    using BNine.Settings;
    using Microsoft.Extensions.Options;

    public class EveryoneApiService : IMobilePhoneService
    {
        private readonly EveryoneApiSettings settings;

        private readonly IHttpClientFactory httpClientFactory;

        public EveryoneApiService(IOptions<EveryoneApiSettings> options, IHttpClientFactory httpClientFactory)
        {
            settings = options.Value;

            httpClientFactory = httpClientFactory;
        }

        public async Task<Application.Models.EveryoneApi.MobileData> GetData(string phoneNumber)
        {
            var httpClient = httpClientFactory.CreateClient(HttpClientName.EveryoneApi);

            var uri = $"/v1/phone/{phoneNumber}?account_sid={settings.SID}&auth_token={settings.Token}&data=name";

            var response = await httpClient.GetAsync(uri);

            if (response.StatusCode == System.Net.HttpStatusCode.BadRequest)
            {
                throw new System.Exception("Invalid phone number");
            }

            if (!response.IsSuccessStatusCode)
            {
                throw new System.Exception("Phone verification error");
            }

            var body = await SerializationHelper.GetResponseContent<MobileData>(response);

            if (body == null || body.Data == null)
            {
                throw new System.Exception("Phone verification error");
            }

            var result = new Application.Models.EveryoneApi.MobileData
            {
                FirstName = body.Data.ExpandedName?.First,
                LastName = body.Data.ExpandedName?.Last,
                Name = body.Data.Name,
                Phone = body.Number
            };

            return result;
        }
    }
}
