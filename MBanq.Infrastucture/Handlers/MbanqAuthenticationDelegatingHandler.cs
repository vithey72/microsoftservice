﻿namespace BNine.MBanq.Infrastructure.Handlers;

using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading;
using System.Threading.Tasks;
using Common;
using Base;
using Settings;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;

public class MbanqAuthenticationDelegatingHandler : DelegatingHandler
{
    private string _accessToken;
    private readonly ILogger<MbanqAuthenticationDelegatingHandler> _logger;

    protected MBanqSettings Settings
    {
        get;
    }

    protected IConfiguration Configuration
    {
        get;
    }

    public MbanqAuthenticationDelegatingHandler(IOptions<MBanqSettings> options,
        IConfiguration configuration,
        ILogger<MbanqAuthenticationDelegatingHandler> logger)
    {
        Settings = options.Value;
        Configuration = configuration;
        _logger = logger;
    }

    protected override async Task<HttpResponseMessage> SendAsync(HttpRequestMessage request,
        CancellationToken cancellationToken)
    {
        if (_accessToken is null)
        {
            var token = await GetAccessToken(cancellationToken);
            _accessToken = token.AccessToken;
        }

        request.Headers.Authorization = new AuthenticationHeaderValue("Bearer", _accessToken);

        var response = await base.SendAsync(request, cancellationToken);

        if (response.StatusCode == HttpStatusCode.Unauthorized || response.StatusCode == HttpStatusCode.Forbidden)
        {
            var token = await GetAccessToken(cancellationToken);
            _accessToken = token.AccessToken;
            request.Headers.Authorization = new AuthenticationHeaderValue("Bearer", token.AccessToken);
            response = await base.SendAsync(request, cancellationToken);
        }

        return response;
    }

    private async Task<TokenResponse> GetAccessToken(CancellationToken cancellationToken)
    {
        var nvc = new List<KeyValuePair<string, string>>
        {
            new("username", Settings.Username),
            new("password", Settings.Password),
            new("client_id", Settings.ClientId),
            new("client_secret", Settings.ClientSecret),
            new("grant_type", "password")
        };

        var uri = new Uri(new Uri(Configuration["MBanqSettings:ApiUrl"]), "/oauth/token");
        var req = new HttpRequestMessage(HttpMethod.Post, uri) { Content = new FormUrlEncodedContent(nvc) };
        req.Headers.Add("tenantId", Configuration["MBanqSettings:TenantId"]);

        var response = await base.SendAsync(req, cancellationToken);

        if (!response.IsSuccessStatusCode)
        {
            _logger.LogError("Invalid login request, mbanq: "
                             + await response.Content.ReadAsStringAsync(cancellationToken) + "; us: "
            + MaskPasswords(await req.Content.ReadAsStringAsync(cancellationToken), Settings.Password));
            throw new Exception("Invalid login request");
        }

        var body = await SerializationHelper.GetResponseContent<TokenResponse>(response);

        return body;
    }

    private string MaskPasswords(string requestPlaintext, string pass)
    {
        if (string.IsNullOrEmpty(pass) || pass.Length <= 2)
        {
            return requestPlaintext;
        }

        return requestPlaintext.Replace(pass[1..^1], "******");
    }
}
