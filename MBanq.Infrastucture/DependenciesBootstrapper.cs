﻿namespace BNine.MBanq.Infrastructure
{
    using System;
    using BNine.Constants;
    using BNine.MBanq.Infrastructure.Handlers;
    using Microsoft.Extensions.Configuration;
    using Microsoft.Extensions.DependencyInjection;
    using Polly;

    public static class DependenciesBootstrapper
    {
        public static IServiceCollection AddMBanqInfrastructure(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddTransient<MbanqAuthenticationDelegatingHandler>();
            services.AddHttpClient(HttpClientName.MBanqAuthorized, client =>
            {
                client.BaseAddress = new Uri(configuration["MBanqSettings:ApiUrl"]);
                client.DefaultRequestHeaders.Add("tenantId", configuration["MBanqSettings:TenantId"]);
            })
            .AddTransientHttpErrorPolicy(p => p.WaitAndRetryAsync(3, (x) => TimeSpan.FromSeconds(x)))
            .AddHttpMessageHandler<MbanqAuthenticationDelegatingHandler>();

            services.AddHttpClient(HttpClientName.MBanqSelfService, client =>
            {
                client.BaseAddress = new Uri(configuration["MBanqSettings:ApiUrl"]);
                client.DefaultRequestHeaders.Add("tenantId", configuration["MBanqSettings:TenantId"]);
            })
            .AddTransientHttpErrorPolicy(p => p.WaitAndRetryAsync(3, (x) => TimeSpan.FromSeconds(x)));

            return services;
        }
    }
}
