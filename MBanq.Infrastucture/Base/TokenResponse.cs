﻿namespace BNine.MBanq.Infrastructure.Base
{
    using Newtonsoft.Json;

    internal class TokenResponse
    {
        [JsonProperty("access_token")]
        internal string AccessToken
        {
            get; set;
        }
        [JsonProperty("token_type")]
        internal string TokenType
        {
            get; set;
        }
        [JsonProperty("refresh_token")]
        internal string RefreshToken
        {
            get; set;
        }
        [JsonProperty("scope")]
        internal string Scope
        {
            get; set;
        }

        [JsonProperty("expires_in")]
        internal int ExpiresIn
        {
            get; set;
        }
    }
}
