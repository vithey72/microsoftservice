﻿namespace BNine.MBanq.Infrastructure.Base
{
    using Newtonsoft.Json;

    public class GraphqlRequest
    {
        [JsonProperty("query")]
        public string Query
        {
            get; set;
        }
    }
}
