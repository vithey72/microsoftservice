﻿namespace BNine.MBanq.Infrastructure.Base
{
    using System.Collections.Generic;
    using Newtonsoft.Json;

    public class ErrorResponse
    {
        [JsonProperty("httpStatusCode")]
        public int HttpStatusCode
        {
            get; set;
        }

        [JsonProperty("errors")]
        public IEnumerable<Error> Errors
        {
            get; set;
        }
    }

    public class Error
    {
        [JsonProperty("defaultUserMessage")]
        public string DefaultUserMessage
        {
            get; set;
        }
    }
}
