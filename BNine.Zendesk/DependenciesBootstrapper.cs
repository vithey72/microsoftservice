﻿namespace BNine.Zendesk
{
    using System;
    using System.Net.Http.Headers;
    using System.Text;
    using BNine.Application.Interfaces.Zendesk;
    using BNine.Constants;
    using BNine.Zendesk.Services;
    using Microsoft.Extensions.Configuration;
    using Microsoft.Extensions.DependencyInjection;

    public static class DependenciesBootstrapper
    {
        public static IServiceCollection AddZendesk(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddTransient<IZendeskFaqService, ZendeskFaqService>();

            services.AddHttpClient(HttpClientName.Zendesk, client =>
            {
                client.BaseAddress = new Uri(configuration["ZendeskSettings:ApiUrl"]);

                var username = configuration["ZendeskSettings:Username"];
                var password = configuration["ZendeskSettings:Password"];
                var credentials = System.Convert.ToBase64String(Encoding.GetEncoding("ISO-8859-1")
                                               .GetBytes(username + ":" + password));

                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", credentials);
                client.DefaultRequestHeaders
                  .Accept
                  .Add(new MediaTypeWithQualityHeaderValue("application/json"));
            });

            return services;
        }
    }
}
