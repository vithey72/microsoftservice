﻿namespace BNine.Zendesk.Services
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Net.Http;
    using System.Threading.Tasks;
    using BNine.Application.Interfaces.Zendesk;
    using BNine.Application.Models.Zendesk;
    using BNine.Common;
    using BNine.Constants;
    using BNine.Enums;
    using BNine.Zendesk.Models.FAQ.Responses;
    using Microsoft.Extensions.Logging;

    public class ZendeskFaqService : IZendeskFaqService
    {
        private IHttpClientFactory ClientFactory
        {
            get;
        }
        private ILogger Logger
        {
            get;
        }

        public ZendeskFaqService(IHttpClientFactory clientFactory, ILogger<ZendeskFaqService> logger)
        {
            ClientFactory = clientFactory;
            Logger = logger;
        }

        public async Task<FAQSections> GetFaqSections()
        {
            var client = ClientFactory.CreateClient(HttpClientName.Zendesk);

            var sectionsRequestPage = "api/v2/help_center/en-us/sections";

            var sections = new List<FAQSectionInfo>();
            while (!string.IsNullOrEmpty(sectionsRequestPage))
            {
                var sectionsResponse = await client.GetAsync(sectionsRequestPage);

                if (!sectionsResponse.IsSuccessStatusCode)
                {
                    throw new Exception(sectionsResponse.ReasonPhrase);
                }

                var sectionsData = await SerializationHelper.GetResponseContent<GetFaqSectionResponse>(sectionsResponse);

                foreach (var item in sectionsData.Sections)
                {
                    var faqQuestions = await GetAllArticlesInCategory(item.Id, client);

                    sections.Add(new FAQSectionInfo
                    {
                        Id = item.Id,
                        Name = item.Name,
                        Questions = faqQuestions.Select(d => new FAQCategoryInfo
                        {
                            Id = d.Id,
                            Title = d.Title,
                            AnswerPageUrl = d.HtmlUrl
                        }).ToList(),
                    });
                }
                sectionsRequestPage = string.IsNullOrEmpty(sectionsData.NextPage) ? null : sectionsData.NextPage[sectionsData.NextPage.IndexOf("api/v2/help_center")..];
            }

            return new FAQSections
            {
                Sections = sections,
            };
        }

        [Obsolete]
        public async Task<IEnumerable<FAQCategory>> GetFaqArticles(Language language)
        {
            var client = ClientFactory.CreateClient(HttpClientName.Zendesk);

            var categoryId = GetCategoryId(language);
            var faqItems = await GetAllArticlesInCategory(categoryId, client);

            return faqItems.Select(x => new FAQCategory
            {
                Body = x.Body,
                Title = x.Title
            });
        }

        private async Task<List<FaqResponseListItem>> GetAllArticlesInCategory(long categoryId, HttpClient client)
        {
            var faqItems = new List<FaqResponseListItem>();

            var nextPage = $"api/v2/help_center/en-us/sections/{categoryId}/articles";

            while (!string.IsNullOrEmpty(nextPage))
            {
                var response = await client.GetAsync(nextPage);

                if (!response.IsSuccessStatusCode)
                {
                    throw new Exception(response.ReasonPhrase);
                }

                var data = await SerializationHelper.GetResponseContent<GetFaqResponse>(response);

                if (data.Articles.Any())
                {
                    faqItems.AddRange(data.Articles);
                }

                nextPage = string.IsNullOrEmpty(data.NextPage) ? null : data.NextPage[data.NextPage.IndexOf("api/v2/help_center")..];
            }

            return faqItems;
        }

        private long GetCategoryId(Language language)
        {
            return language switch
            {
                Language.Spanish => 360005894797,
                _ => 360005894797
            };
        }
    }
}
