﻿namespace BNine.Zendesk.Models.FAQ.Responses
{
    using Newtonsoft.Json;

    internal class GetFaqSectionResponse : BaseListResponse
    {
        [JsonProperty("sections")]
        internal IEnumerable<FaqResponseSectionItem> Sections
        {
            get; set;
        } = new List<FaqResponseSectionItem>();
    }

    internal class FaqResponseSectionItem
    {
        [JsonProperty("id")]
        internal long Id
        {
            get; set;
        }

        [JsonProperty("name")]
        internal string Name
        {
            get; set;
        }

        [JsonProperty("locale")]
        internal string Locale
        {
            get; set;
        }
    }
}
