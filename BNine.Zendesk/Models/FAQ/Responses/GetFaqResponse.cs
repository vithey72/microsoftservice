﻿namespace BNine.Zendesk.Models.FAQ.Responses
{
    using System.Collections.Generic;
    using Newtonsoft.Json;

    internal class GetFaqResponse : BaseListResponse
    {
        [JsonProperty("articles")]
        internal IEnumerable<FaqResponseListItem> Articles
        {
            get; set;
        } = new List<FaqResponseListItem>();
    }

    internal class FaqResponseListItem
    {
        [JsonProperty("id")]
        internal long Id
        {
            get; set;
        }

        [JsonProperty("html_url")]
        internal string HtmlUrl
        {
            get; set;
        }

        [JsonProperty("draft")]
        internal bool Draft
        {
            get; set;
        }

        [JsonProperty("promoted")]
        internal bool Promoted
        {
            get; set;
        }

        [JsonProperty("section_id")]
        internal long SectionId
        {
            get; set;
        }

        [JsonProperty("name")]
        internal string Name
        {
            get; set;
        }

        [JsonProperty("title")]
        internal string Title
        {
            get; set;
        }

        [JsonProperty("body")]
        internal string Body
        {
            get; set;
        }
    }
}
