﻿namespace BNine.Zendesk.Models
{
    using Newtonsoft.Json;

    internal class BaseListResponse
    {
        [JsonProperty("count")]
        internal int Сount
        {
            get; set;
        }

        [JsonProperty("next_page")]
        internal string NextPage
        {
            get; set;
        }

        [JsonProperty("page")]
        internal int Page
        {
            get; set;
        }

        [JsonProperty("page_count")]
        internal int PageCount
        {
            get; set;
        }

        [JsonProperty("per_page")]
        internal int PerPage
        {
            get; set;
        }

        [JsonProperty("previous_page")]
        internal string PreviousPage
        {
            get; set;
        }

        [JsonProperty("sort_by")]
        internal string SortBy
        {
            get; set;
        }

        [JsonProperty("sort_order")]
        internal string SortOrder
        {
            get; set;
        }
    }
}
