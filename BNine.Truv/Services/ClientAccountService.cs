﻿namespace BNine.Truv.Services;

using System;
using System.Threading;
using System.Threading.Tasks;
using BNine.Application.Interfaces.Truv;
using BNine.Application.Models.Truv;
using BNine.Common;
using BNine.Common.Extensions;
using Newtonsoft.Json;

internal class ClientAccountService : ITruvClientAccountService
{
    private const string RelativeUri = "/v1/users/";

    private readonly IClientFactory _truvClientFactory;

    public ClientAccountService(
        IClientFactory truvClientFactory
        )
    {
        _truvClientFactory = truvClientFactory;
    }

    public async Task<TruvClientAccount> CreateAccount(Guid userId, CancellationToken cancellationToken)
    {
        var userIdString = userId.ToString();
        var requestBody = new CreateUserRequestBody { ExternalId = userIdString };
        var content = SerializationHelper.GetRequestContent(requestBody);

        using var client = await _truvClientFactory.CreateAuthorizedHttpClient();

        var response = await client.PostAsync(RelativeUri, content, cancellationToken);
        response.EnsureSuccessStatusCode();

        var responseString = await response.Content.ReadAsStringAsync(cancellationToken);
        var responseObject = JsonConvert.DeserializeObject<CreateUserResponseBody>(responseString).NotNull();
        var account = ConvertResponseToModel(responseObject);
        return account;
    }

    public async Task DeleteAccount(Guid truvUserId, CancellationToken cancellationToken)
    {
        var truvUserIdString = truvUserId.ToString();
        var uri = $"{RelativeUri}{truvUserIdString}/";

        using var client = await _truvClientFactory.CreateAuthorizedHttpClient();

        var response = await client.DeleteAsync(uri, cancellationToken);
        response.EnsureSuccessStatusCode();
    }

    private TruvClientAccount ConvertResponseToModel(CreateUserResponseBody response)
    {
        var accountId = Guid.Parse(response.Id);

        return new TruvClientAccount
        {
            Id = accountId,
        };
    }

    private class CreateUserRequestBody
    {
        [JsonProperty("external_user_id")]
        public string ExternalId
        {
            get; set;
        }

        //"external_user_id": "FBA7775A-9C3A-ED11-A27C-000D3A338DDF",
        //"first_name": "Egor",
        //"last_name": "Test",
        //"email": "sdfsdf@sdfsdf.com",
        //"phone": "+14155554193",
        //"ssn": "123124799"
    }

    private class CreateUserResponseBody
    {
        [JsonProperty("id")]
        public string Id
        {
            get; set;
        }
    }
}
