﻿namespace BNine.Truv.Services;

using System.Text;
using BNine.Application.Interfaces.Truv;
using BNine.Common;
using BNine.Common.Extensions;
using Newtonsoft.Json;

internal class DataRefreshTaskService : ITruvDataRefreshTaskService
{
    private const string RelativeUri = "/v1/refresh/tasks/";

    private readonly IClientFactory _truvClientFactory;

    public DataRefreshTaskService(
        IClientFactory truvClientFactory
        )
    {
        _truvClientFactory = truvClientFactory;
    }

    public async Task<TruvTaskId> BeginDataRefresh(string accessToken, CancellationToken cancellationToken)
    {
        var content = SerializationHelper.GetRequestContent(new
        {
            access_token = accessToken,
            product_type = "income",
        });

        using var client = await _truvClientFactory.CreateAuthorizedHttpClient();

        var response = await client.PostAsync(RelativeUri, content, cancellationToken);
        response.EnsureSuccessStatusCode();

        var responseString = await response.Content.ReadAsStringAsync();
        var responseObject = JsonConvert.DeserializeObject<DataRefreshResponseBody>(responseString).NotNull();
        return new TruvTaskId(responseObject.task_id);
    }

    public async Task<TruvTaskStatus> GetTaskStatus(TruvTaskId taskId, CancellationToken cancellationToken)
    {
        var taskUri = $"{RelativeUri}{taskId.Value}/";
        using var client = await _truvClientFactory.CreateAuthorizedHttpClient();

        var response = await client.GetAsync(taskUri, cancellationToken);
        response.EnsureSuccessStatusCode();

        var responseString = await response.Content.ReadAsStringAsync();
        var responseObject = JsonConvert.DeserializeObject<StatusResponseBody>(responseString).NotNull();

        var taskStatus = ConvertToStatus(responseObject.status);

        return taskStatus;
    }

    public async Task<TruvTaskStatusUpdate?> ParseTaskStatusUpdateEvent(Stream requestBody)
    {
        var @event = await ReadTaskStatusUpdateEventBody(requestBody);

        if (!IsTaskStatusUpdate(@event))
        {
            return null;
        }

        return GetTaskStatusUpdate(@event);
    }

    private async Task<TaskStatusUpdateEventBodyObject> ReadTaskStatusUpdateEventBody(Stream body)
    {
        using var reader = new StreamReader(body, Encoding.UTF8);

        var bodyString = await reader.ReadToEndAsync();

        var responseObject = JsonConvert.DeserializeObject<TaskStatusUpdateEventBodyObject>(bodyString).NotNull();

        return responseObject;
    }

    private bool IsTaskStatusUpdate(TaskStatusUpdateEventBodyObject @event)
    {
        return @event.event_type == "task-status-updated";
    }

    private TruvTaskStatusUpdate GetTaskStatusUpdate(TaskStatusUpdateEventBodyObject @event)
    {
        return new TruvTaskStatusUpdate(
            new TruvTaskId(@event.task_id),
            Guid.Parse(@event.user_id),
            @event.link_id,
            ConvertToStatus(@event.status),
            @event.updated_at.ToUniversalTime()
            );
    }

    private TruvTaskStatus ConvertToStatus(string statusString)
    {
        var status = statusString switch
        {
            "done" => TruvTaskStatus.Succeeded,
            "login_error"
                or "mfa_error"
                or "config_error"
                or "account_locked"
                or "unable_to_reset"
                or "no_data"
                or "unavailable"
                or "error" => TruvTaskStatus.Failed,
            _ => TruvTaskStatus.Pending,
        };

        return status;
    }

    private class DataRefreshResponseBody
    {
        public string task_id
        {
            get; set;
        } = string.Empty;
    }

    private class StatusResponseBody
    {
        public string id
        {
            get; set;
        } = string.Empty;

        public string status
        {
            get; set;
        } = string.Empty;
    }

    private class TaskStatusUpdateEventBodyObject
    {
        public string event_type
        {
            get; set;
        }

        public string task_id
        {
            get; set;
        }

        public string link_id
        {
            get; set;
        }

        public string user_id
        {
            get; set;
        }

        public string status
        {
            get; set;
        }

        public DateTime updated_at
        {
            get; set;
        }
    }
}
