﻿namespace BNine.Truv.Services;

using BNine.Application.Interfaces;
using BNine.Application.Interfaces.Truv;
using BNine.Common.Extensions;
using BNine.Enums.Transfers.Controller;
using Newtonsoft.Json;

internal class EmploymentProvider : ITruvEmploymentProvider
{
    private readonly IClientFactory _truvClientFactory;
    private readonly ITransferIconsAndNamesProviderService _iconsService;

    public EmploymentProvider(
        IClientFactory truvClientFactory,
        ITransferIconsAndNamesProviderService iconsService
        )
    {
        _truvClientFactory = truvClientFactory;
        _iconsService = iconsService;
    }

    public async Task<TruvEmployment[]> ProvideEmployments(string linkId, CancellationToken cancellationToken)
    {
        var uri = GetRelativeUri(linkId);

        using var client = await _truvClientFactory.CreateAuthorizedHttpClient();

        var response = await client.GetAsync(uri, cancellationToken);
        response.EnsureSuccessStatusCode();

        var responseString = await response.Content.ReadAsStringAsync();
        var responseArray = JsonConvert.DeserializeObject<RetrieveEmploymentResponseBody[]>(responseString).NotNull();

        var defaultIconUri = _iconsService.FindIconUrl(BankTransferIconOption.MoneyBag);
        var account = responseArray.Select(x => ConvertResponseToModel(x, defaultIconUri)).ToArray();
        return account;
    }

    private string GetRelativeUri(string linkIdString) => $"/v1/links/{linkIdString}/employments";

    private TruvEmployment ConvertResponseToModel(RetrieveEmploymentResponseBody response, string defaultIconUri)
    {
        return new TruvEmployment(response.company.name, defaultIconUri);
    }

    private class RetrieveEmploymentResponseBody
    {
        private static readonly Company @default = new();

        public Company company
        {
            get; set;
        } = @default;
    }

    private class Company
    {
        public string name
        {
            get; set;
        } = string.Empty;
    }
}
