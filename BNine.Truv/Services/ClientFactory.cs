﻿namespace BNine.Truv.Services;

using System.Net.Http;
using System.Threading.Tasks;
using BNine.Constants;
using BNine.Settings;
using Microsoft.Extensions.Options;

internal class ClientFactory : IClientFactory
{
    private readonly TruvSettings _settings;
    private readonly IHttpClientFactory _clientFactory;

    public ClientFactory(
        IOptions<TruvSettings> options,
        IHttpClientFactory clientFactory
        )
    {
        _settings = options.Value;
        _clientFactory = clientFactory;
    }

    public Task<HttpClient> CreateAuthorizedHttpClient()
    {
        var client = _clientFactory.CreateClient(HttpClientName.Truv);

        client.DefaultRequestHeaders.Add("X-Access-Secret", _settings.ClientSecret);
        client.DefaultRequestHeaders.Add("X-Access-Client-ID", _settings.ClientId);

        return Task.FromResult(client);
    }
}
