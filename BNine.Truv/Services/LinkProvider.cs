﻿namespace BNine.Truv.Services;

using System.Threading;
using System.Threading.Tasks;
using BNine.Application.Interfaces.Truv;
using BNine.Common;
using BNine.Common.Extensions;
using Newtonsoft.Json;

internal class LinkProvider : ITruvLinkProvider
{
    private const string RelativeUri = "/v1/link-access-tokens/";

    private readonly IClientFactory _truvClientFactory;

    public LinkProvider(
        IClientFactory truvClientFactory
        )
    {
        _truvClientFactory = truvClientFactory;
    }

    public async Task<TruvLinkToken> ProvideLinkToken(string publicToken, CancellationToken cancellationToken)
    {
        var requestBody = new
        {
            public_token = publicToken,
        };
        var content = SerializationHelper.GetRequestContent(requestBody);

        using var client = await _truvClientFactory.CreateAuthorizedHttpClient();

        var response = await client.PostAsync(RelativeUri, content, cancellationToken);
        response.EnsureSuccessStatusCode();

        var responseString = await response.Content.ReadAsStringAsync(cancellationToken);
        var responseObject = JsonConvert.DeserializeObject<RetrieveLinkTokenResponseBody>(responseString).NotNull();
        var account = ConvertResponseToModel(responseObject);
        return account;
    }

    private TruvLinkToken ConvertResponseToModel(RetrieveLinkTokenResponseBody response)
    {
        return new TruvLinkToken(response.link_id, response.access_token);
    }

    private class RetrieveLinkTokenResponseBody
    {
        public string access_token
        {
            get; set;
        } = string.Empty;

        public string link_id
        {
            get; set;
        } = string.Empty;
    }
}
