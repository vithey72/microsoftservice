﻿namespace BNine.Truv.Services;

internal interface IClientFactory
{
    Task<HttpClient> CreateAuthorizedHttpClient();
}
