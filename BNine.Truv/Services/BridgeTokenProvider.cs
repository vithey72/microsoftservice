﻿namespace BNine.Truv.Services;

using BNine.Application.Interfaces;
using BNine.Application.Interfaces.Truv;
using BNine.Common;
using BNine.Common.Extensions;
using BNine.Settings;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;

internal class BridgeTokenProvider : ITruvBridgeTokenProvider
{
    private readonly TimeSpan _bridgeTokenValiditySpan = TimeSpan.FromHours(6);

    private readonly TruvSettings _settings;
    private readonly IClientFactory _truvClientFactory;
    private readonly IDateTimeUtcProviderService _dateTimeProvider;

    public BridgeTokenProvider(
        IOptions<TruvSettings> options,
        IClientFactory truvClientFactory,
        IDateTimeUtcProviderService dateTimeProvider
        )
    {
        _settings = options.Value;
        _truvClientFactory = truvClientFactory;
        _dateTimeProvider = dateTimeProvider;
    }

    public async Task<TruvBridgeToken> GetToken(
        Guid userId,
        string bankAccountNumber,
        CancellationToken cancellationToken)
    {
        var userIdString = userId.GetTruvUserIdString();
        var uri = GetRelativeUri(userIdString);
        var requestBody = new BridgeTokenRequestBody
        {
            Account = new
            {
                action = "create",//update, delete
                account_number = bankAccountNumber,
                routing_number = _settings.RoutingNumber,
                bank_name = "B9",
                account_type = "checking",
            },
        };
        var content = SerializationHelper.GetRequestContent(requestBody);

        using var client = await _truvClientFactory.CreateAuthorizedHttpClient();

        var response = await client.PostAsync(uri, content, cancellationToken);
        response.EnsureSuccessStatusCode();

        var responseString = await response.Content.ReadAsStringAsync();
        var responseObject = JsonConvert.DeserializeObject<BridgeTokenResponseBody>(responseString).NotNull();

        var validTo = _dateTimeProvider.UtcNow().Add(_bridgeTokenValiditySpan);
        return new TruvBridgeToken(responseObject.BridgeToken, validTo);
    }

    private string GetRelativeUri(string userIdString) => $"/v1/users/{userIdString}/tokens/";

    private class BridgeTokenRequestBody
    {
        [JsonProperty("product_type")]
        public string ProductType
        {
            get; set;
        } = "deposit_switch";

        [JsonProperty("allowed_products")]
        public string[] AllowedProducts
        {
            get;
        } = new[] { "income", "deposit_switch" };

        [JsonProperty("account")]
        public object? Account
        {
            get; set;
        }
    }

    private class BridgeTokenResponseBody
    {
        [JsonProperty("bridge_token")]
        public string BridgeToken
        {
            get; set;
        }
    }
}
