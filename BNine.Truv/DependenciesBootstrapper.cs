﻿namespace BNine.Truv;

using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Configuration;
using BNine.Constants;
using Polly;
using BNine.Truv.Services;
using BNine.Application.Interfaces.Truv;

public static class DependenciesBootstrapper
{
    public static IServiceCollection AddTruv(this IServiceCollection services, IConfiguration configuration)
    {
        services.AddTransient<IClientFactory, ClientFactory>();
        services.AddTransient<ITruvClientAccountService, ClientAccountService>();
        services.AddTransient<ITruvBridgeTokenProvider, BridgeTokenProvider>();
        services.AddTransient<ITruvLinkProvider, LinkProvider>();
        services.AddTransient<ITruvDataRefreshTaskService, DataRefreshTaskService>();
        services.AddTransient<ITruvEmploymentProvider, EmploymentProvider>();

        services.AddHttpClient(HttpClientName.Truv, client =>
        {
            client.BaseAddress = new Uri(configuration["TruvSettings:ApiUrl"]);
        })
        .AddTransientHttpErrorPolicy(p => p.WaitAndRetryAsync(3, (x) => TimeSpan.FromSeconds(x)));

        return services;
    }
}
