﻿namespace BNine.Application.Converters
{
    using Extensions;
    using Newtonsoft.Json;
    using Newtonsoft.Json.Converters;

    public class StringEnumMemberConverter<T> : StringEnumConverter
        where T : Enum
    {
        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            if (value == null)
            {
                writer.WriteNull();
                return;
            }

            T e = (T)value;

            var test = e.GetEnumMemberValue();
            if (test != null)
            {
                writer.WriteValue(test);
            }
            else
            {
                writer.WriteValue(value);
            }
        }
    }
}
