﻿namespace BNine.Application.Behaviours
{
    using System.Threading;
    using System.Linq;
    using System.Threading.Tasks;
    using BNine.Application.Interfaces;
    using MediatR;
    using System;
    using System.Collections.Generic;

    public class UserRequestExecutionBehavior<TRequest, TResponse> : IPipelineBehavior<TRequest, TResponse>
        where TRequest : IRequest<TResponse>
    {
        private const int MaxAppLockKeyLength = 32;

        public ICurrentUserService CurrentUserService
        {
            get;
        }
        public IBNineServiceDbContext DbContext
        {
            get;
        }

        public UserRequestExecutionBehavior(ICurrentUserService currentUserService, IBNineServiceDbContext dbContext)
        {
            CurrentUserService = currentUserService;
            DbContext = dbContext;
        }

        private static Dictionary<Type, bool> isCommandOrQuerySequential = new Dictionary<Type, bool>();

        public async Task<TResponse> Handle(TRequest request, CancellationToken cancellationToken, RequestHandlerDelegate<TResponse> next)
        {
            var requestType = typeof(TRequest);
            if (!isCommandOrQuerySequential.ContainsKey(requestType))
            {
                isCommandOrQuerySequential[requestType] = requestType.GetCustomAttributes(false).OfType<SequentialAttribute>().Any();
            }
            if (isCommandOrQuerySequential[requestType])
            {
                var userId = CurrentUserService.UserId;
                var key = userId.HasValue ? userId!.GetHashCode() : request.GetHashCode();
                var lockid = $"{key}/{requestType.Name}";
                await using (await DbContext.CreateTransactionalAppLockAsync(lockid.Length <= MaxAppLockKeyLength ? lockid : lockid.Substring(0, MaxAppLockKeyLength)))
                {
                    return await next();
                }
            }

            return await next();
        }
    }

    /// <summary>
    /// Marks requests that should be executed for a user or request(if no user present) without parallelism
    /// GetHashCode of user id or entire request and request type name are used as a key
    /// </summary>
    public class SequentialAttribute : Attribute
    {

    }
}
