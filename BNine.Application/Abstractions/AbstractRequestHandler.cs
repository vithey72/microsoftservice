﻿namespace BNine.Application.Abstractions
{
    using AutoMapper;
    using BNine.Application.Interfaces;
    using MediatR;

    public abstract class AbstractRequestHandler
    {
        protected IMediator Mediator
        {
            get;
        }

        protected IBNineDbContext DbContext
        {
            get;
        }

        protected IMapper Mapper
        {
            get;
        }

        protected ICurrentUserService CurrentUser
        {
            get;
        }

        public AbstractRequestHandler(
            IMediator mediator,
            IBNineDbContext dbContext,
            IMapper mapper,
            ICurrentUserService currentUser)
        {
            Mediator = mediator;
            DbContext = dbContext;
            Mapper = mapper;
            CurrentUser = currentUser;
        }
    }
}
