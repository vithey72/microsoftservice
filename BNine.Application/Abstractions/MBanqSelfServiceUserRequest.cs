﻿namespace BNine.Application.Abstractions;

using Newtonsoft.Json;

// TODO: inherit in every Query that has same properties
public abstract class MBanqSelfServiceUserRequest
{
    [JsonIgnore]
    public string MbanqAccessToken
    {
        get; set;
    }

    [JsonIgnore]
    public string Ip
    {
        get; set;
    }
}
