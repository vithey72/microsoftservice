﻿namespace BNine.Application.Abstractions;

using Newtonsoft.Json;

public abstract class DebitCardUserRequest : MBanqSelfServiceUserRequest
{
    [JsonProperty("id")]
    public int? CardId
    {
        get; set;
    }
}
