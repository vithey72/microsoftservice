﻿namespace BNine.Application.Mappings;

using Enums;

public static class EnumManualMappings
{
    public static List<CurrentAdditionalAccountEnum> ToCurrentAdditionalAccountEnumList(this List<BankAccountTypeEnum> bankAccountTypeEnums)
    {
        return (from item in bankAccountTypeEnums
            select (CurrentAdditionalAccountEnum?)(item switch
            {
                BankAccountTypeEnum.Reward => CurrentAdditionalAccountEnum.Reward,
                _ => null
            })
            into currentAdditionalAccountEnum
            where currentAdditionalAccountEnum != null
            select currentAdditionalAccountEnum.Value).ToList();
    }

    public static BankAccountTypeEnum ToBankAccountTypeEnum(this CurrentAdditionalAccountEnum currentAdditionalAccountEnum)
    {
        return currentAdditionalAccountEnum switch
        {
            CurrentAdditionalAccountEnum.Reward => BankAccountTypeEnum.Reward,
            _ => throw new ArgumentOutOfRangeException(nameof(currentAdditionalAccountEnum), currentAdditionalAccountEnum,
                "Invalid CurrentAdditionalAccountEnum value")
        };
    }
}
