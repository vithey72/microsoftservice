﻿namespace BNine.Application.Collections
{
    using System.Linq;
    using Enums.DebitCard;

    public static class CardDeliveredStatuses
    {
        private static readonly CardShippingStatus[] Statuses = { CardShippingStatus.RECEIVED, CardShippingStatus.ACTIVATED };

        public static bool Contains(CardShippingStatus status) => Statuses.Contains(status);
    }
}
