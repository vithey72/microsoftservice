﻿namespace BNine.Application.Exceptions.ApiResponse
{
    public class ApiResponseAggregatedException : ApiResponseException
    {
        public ApiResponseAggregatedException(string code, string message) : base(code, message)
        {
        }

        public List<AggregatedError> AggregatedErrors
        {
            get; set;
        }
    }
}
