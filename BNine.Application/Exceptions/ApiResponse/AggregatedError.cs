﻿namespace BNine.Application.Exceptions.ApiResponse
{
    public class AggregatedError
    {
        public string Field
        {
            get; set;
        }

        public string Code
        {
            get; set;
        }

        public string Message
        {
            get; set;
        }
    }
}
