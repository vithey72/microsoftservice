﻿namespace BNine.Application.Exceptions.ApiResponse
{
    public class ApiResponseException : Exception
    {
        public ApiResponseException(string code, string message, string title = null)
        {
            Code = code;
            Message = message;
            Title = title;
        }

        public string Code
        {
            get; set;
        }

        public string Title
        {
            get; set;
        }

        public new string Message
        {
            get; set;
        }

        public string OperationId
        {
            get; set;
        }
    }
}
