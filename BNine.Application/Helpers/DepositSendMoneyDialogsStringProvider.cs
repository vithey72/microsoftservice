﻿namespace BNine.Application.Helpers;
using System.Collections.Immutable;
using Enums;

public static class DepositSendMoneyDialogsStringProvider
{
    private static readonly ImmutableDictionary<Enum, string> DepositWalletInfoIconFileName =
        new Dictionary<Enum, string>()
    {
        { PartnerApp.Default, "deposit_wallet_info_icon_88_88.png" },
        { PartnerApp.Poetryy, "deposit_wallet_info_icon_poetryy_88_88.png" },
        { PartnerApp.Eazy, "deposit_wallet_info_icon_eazy_88_88.png" },
        { PartnerApp.USNational, "deposit_wallet_info_icon_natio_88_88.png"},
        { PartnerApp.Qorbis, "deposit_wallet_info_icon_qorbis_96_96.png"},
        { PartnerApp.ManaPacific, "deposit_wallet_info_icon_mana_96_96.png"},
        { PartnerApp.P2P, "deposit_wallet_info_icon_p2p_96_96.png"},
        { PartnerApp.Booq, "deposit_wallet_info_icon_booq_96_96.png"},
        { PartnerApp.PayHammer, "deposit_wallet_info_icon_payhammer_96_96.png"}

    }.ToImmutableDictionary();

    private static readonly ImmutableDictionary<Enum, string> DepositFromOtherCards = new Dictionary<Enum, string>()
    {
        { PartnerApp.Default, "deposit_from_another_card_icon_130_130.png" },
        { PartnerApp.Qorbis, "deposit_from_another_card_icon_qorbis_108_108.png" },
        { PartnerApp.Booq, "deposit_from_another_card_icon_booq_108_108.png" },
    }.ToImmutableDictionary();

    private static readonly ImmutableDictionary<Enum, string> SendMoneyDomesticAchIconFileName =
        new Dictionary<Enum, string>()
    {
        { PartnerApp.Default, "send_domestic_ach_icon_88_88.png" },
        { PartnerApp.Poetryy, "send_domestic_ach_icon_poetryy_88_88.png" },
        { PartnerApp.Eazy, "send_domestic_ach_icon_eazy_88_88.png" },
        { PartnerApp.USNational, "send_domestic_ach_icon_natio_88_88.png"},
        { PartnerApp.Qorbis, "send_domestic_ach_icon_qorbis_93_84.png" },
        { PartnerApp.ManaPacific, "send_domestic_ach_icon_mana_88_88.png"},
        { PartnerApp.P2P, "send_domestic_ach_icon_p2p_88_88.png"},
        { PartnerApp.Booq, "send_domestic_ach_icon_booq_88_88.png"},
        { PartnerApp.PayHammer, "send_domestic_ach_icon_payhammer_88_88.png"},
    }.ToImmutableDictionary();

    private static readonly ImmutableDictionary<Enum, string> SendMoneyInternalIconFileName =
        new Dictionary<Enum, string>()
    {
        { PartnerApp.Default, "send_internal_icon_88_88.png" },
        { PartnerApp.Poetryy, "send_internal_icon_poetryy_88_88.png" },
        { PartnerApp.Eazy, "send_internal_icon_eazy_94_108.png" },
        { PartnerApp.USNational, "send_internal_icon_natio_88_88.png"},
        { PartnerApp.Qorbis, "send_internal_icon_qorbis_96_97.png" },
        { PartnerApp.ManaPacific, "send_internal_icon_mana_96_97.png" },
        { PartnerApp.P2P, "send_internal_icon_p2p_96_97.png" },
        { PartnerApp.Booq, "send_internal_icon_booq_96_97.png" },
        { PartnerApp.PayHammer, "send_internal_icon_payhammer_96_97.png" },
    }.ToImmutableDictionary();

    private static readonly ImmutableDictionary<Enum, string> SendMoneyToAnotherCard =
        new Dictionary<Enum, string>()
        {
            { PartnerApp.Default, "send_another_card_icon_88_88.png" },
            { PartnerApp.Qorbis, "send_another_card_icon_qorbis_93_93.png" },
            { PartnerApp.Booq, "send_another_card_icon_booq_93_93.png" },
        }.ToImmutableDictionary();

    public static string GetDepositFromOtherCardsIconFileName(PartnerApp partnerApp)
    {
        return DepositFromOtherCards.TryGetValue(partnerApp, out var fileName)
            ? fileName
            : DepositFromOtherCards[PartnerApp.Default];
    }

    public static string GetDepositWalletInfoIconFileName(PartnerApp partnerApp)
    {
        return DepositWalletInfoIconFileName.TryGetValue(partnerApp, out var fileName)
            ? fileName
            : DepositWalletInfoIconFileName[PartnerApp.Default];
    }
    public static string GetSendMoneyDomesticAchIconFileName(PartnerApp partnerApp)
    {
        return SendMoneyDomesticAchIconFileName.TryGetValue(partnerApp, out var fileName)
            ? fileName
            : SendMoneyDomesticAchIconFileName[PartnerApp.Default];
    }
    public static string GetSendMoneyInternalIconFileName(PartnerApp partnerApp)
    {
        return SendMoneyInternalIconFileName.TryGetValue(partnerApp, out var fileName)
            ? fileName
            : SendMoneyInternalIconFileName[PartnerApp.Default];
    }

    public static string GetSendMoneyToAnotherCardIconFileName(PartnerApp partnerApp)
    {
        return SendMoneyToAnotherCard.TryGetValue(partnerApp, out var fileName)
            ? fileName
            : SendMoneyToAnotherCard[PartnerApp.Default];
    }
}
