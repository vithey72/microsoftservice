﻿namespace BNine.Application.Helpers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Enums;

    internal class ClientDocumentsHelper
    {

        public static string GetKey(DocumentType documentType)
        {
            if (DocumentKeys.TryGetValue(documentType, out var value))
            {
                return value;
            }

            throw new ArgumentException("Invalid document type");
        }

        public static DocumentType GetDocumentType(string key)
        {
            var documentType = DocumentKeys
                .Where(x => x.Value.Equals(key))
                .Select(x => (DocumentType?)x.Key)
                .FirstOrDefault();

            if (documentType != null)
            {
                return documentType.Value;
            }

            throw new ArgumentException("Invalid document key");
        }

        private static Dictionary<DocumentType, string> DocumentKeys = new Dictionary<DocumentType, string>
        {
            {DocumentType.Passport, Constants.ClientIdentifierDocumentTypes.Passport},
            {DocumentType.ForeignDriverLicense, Constants.ClientIdentifierDocumentTypes.ForeignDriverLicense},
            {DocumentType.ConsularId, Constants.ClientIdentifierDocumentTypes.ConsularId},
            {DocumentType.Visa, Constants.ClientIdentifierDocumentTypes.Visa},
            {DocumentType.Other, Constants.ClientIdentifierDocumentTypes.Other},
            {DocumentType.SSN, Constants.ClientIdentifierDocumentTypes.SSN},
            {DocumentType.DriverLicense, Constants.ClientIdentifierDocumentTypes.DriverLicense},
            {DocumentType.StateIdentityCard, Constants.ClientIdentifierDocumentTypes.StateIdentityCard},
            {DocumentType.WorkPermitCard, Constants.ClientIdentifierDocumentTypes.WorkPermitCard},
            {DocumentType.PermanentResidentCard, Constants.ClientIdentifierDocumentTypes.PermanentResidentCard},
            {DocumentType.ForeignPassport, Constants.ClientIdentifierDocumentTypes.ForeignPassport},
            {DocumentType.ITIN, Constants.ClientIdentifierDocumentTypes.ITIN},
            {DocumentType.CommercialDriverLicense, Constants.ClientIdentifierDocumentTypes.DriverLicense},
            {DocumentType.DriverLicensePermit, Constants.ClientIdentifierDocumentTypes.DriverLicense}
        };
    }
}
