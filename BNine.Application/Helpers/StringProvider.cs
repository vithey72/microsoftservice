﻿namespace BNine.Application.Helpers;

using System.Globalization;
using Enums;
using Interfaces;

public class StringProvider
{
    private static readonly string BnineSupportEmail = "support@bnine.com";
    private static readonly string MbanqSupportEmail = "support@mbanq.com";
    private static readonly string EazySupportEmail = "support@tryeazy.co";
    private static readonly string PoetryySupportEmail = "info@poetryyfinance.com";
    private static readonly string USNationalSupportEmail = "support@usanational.io";
    private static readonly string QorbisSupportEmail = "support@qorbis.com";
    private static readonly string ManaPacificSupportEmail = "support@mymanapacific.com";
    private static readonly string P2PSupportEmail = "support@ptwop.com";
    private static readonly string BooqSupportEmail = "support@booqfi.com";
    private static readonly string PayHammerSupportEmail = "support@payhammer.com";

    private static readonly string BnineSupportPhone = "+1 (888) 297-5504";
    private static readonly string EazySupportPhone = "+1 (877) 640-7095";
    private static readonly string PoetryySupportPhone = "+1 (888) 683-2990";
    private static readonly string USNationalSupportPhone = "+1 (888) 742-2127";
    private static readonly string QorbisSupportPhone = "+1 (800) 708-8665";
    private static readonly string ManaPacificSupportPhone = "+1 (800) 405 2898";
    private static readonly string P2PSupportPhone = "+1 (888) 383-8042";
    private static readonly string BooqSupportPhone = "+1 (888) 533-5027";
    private static readonly string PayHammerSupportPhone = "+1 (888) 462-2610";

    private static readonly string B9Name = "B9";
    private static readonly string MbanqName = "MBANQ";
    private static readonly string EazyName = "Eazy";
    private static readonly string PoetryyName = "PoetrYY";
    private static readonly string USNationalName = "USA National";
    private static readonly string QorbisName = "QORBIS";
    private static readonly string ManaPacificName = "Mana Pacific";
    private static readonly string P2PName = "P2P";
    private static readonly string BooqName = "Booq";
    private static readonly string PayHammerName = "Payhammer";

    private static readonly string B9SupportAddress = $"845 Market Street, Suite 450 #12, San Francisco 94103";
    private static readonly string PoetryySupportAddress = $"360 Northwest 27th Street, Miami, FL 33134";
    private static readonly string EazySupportAddress = $"1250 Borregas Ave, Sunnyvale, CA 94089";
    private static readonly string USNationalSupportAddress = String.Empty;
    private static readonly string QorbisSupportAddress = String.Empty;
    private static readonly string ManaPacificSupportAddress = String.Empty;
    private static readonly string P2pSupportAddress = String.Empty;
    private static readonly string BooqSupportAddress = String.Empty;
    private static readonly string PayHammerSupportAddress = String.Empty;

    private static readonly string B9LogoFileName = "BNineLogo.JPG";
    private static readonly string MbanqLogoFileName = "mbanq_logo.png";
    private static readonly string PoetryyLogoFileName = "poetryy_logo.png";
    private static readonly string EazyLogoFileName = "eazy_logo.png";
    private static readonly string USNationalLogoFileName = "usnational_logo.png";
    private static readonly string QorbisLogoFileName = "qorbis_logo.png";
    private static readonly string ManaPacificLogoFileName = "manapacific_logo.png";
    private static readonly string P2PLogoFileName = "p2p_logo.png";
    private static readonly string BooqLogoFileName = "booq_logo.png";
    private static readonly string PayHammerLogoFileName = "payhammer_logo.png";

    private static readonly string B9SupportPhoneNumber = "(888) 297-5504";
    private static readonly string PoetryySupportPhoneNumber = "(888) 683-2990";
    private static readonly string EazySupportPhoneNumber = "(877) 640-7095";
    private static readonly string USNationalSupportPhoneNumber = "(888) 742-2127";
    private static readonly string QorbisSupportPhoneNumber = "(800) 708-8665";
    private static readonly string ManaPacificSupportPhoneNumber = "(800) 405 2898";
    private static readonly string P2PSupportPhoneNumber = "(888) 383-8042";
    private static readonly string BooqSupportPhoneNumber = "(888) 533-5027";
    private static readonly string PayHammerSupportPhoneNumber = "(888) 462-2610";

    private static readonly char[] Vowels = { 'a', 'e', 'i', 'o', 'u' };

    public static string GetPartnerName(IPartnerProviderService partnerProvider)
    {
        return partnerProvider.GetPartner() switch
        {
            PartnerApp.MBanqApp => MbanqName,
            PartnerApp.Poetryy => PoetryyName,
            PartnerApp.Eazy => EazyName,
            PartnerApp.USNational => USNationalName,
            PartnerApp.Qorbis => QorbisName,
            PartnerApp.ManaPacific => ManaPacificName,
            PartnerApp.P2P => P2PName,
            PartnerApp.Booq => BooqName,
            PartnerApp.PayHammer => PayHammerName,
            _ => B9Name,
        };
    }

    public static string GetPartnerArticle(IPartnerProviderService partnerProvider)
    {
        var partnerNameFirstLetter = GetPartnerName(partnerProvider).First();
        return IsVowel(partnerNameFirstLetter) ? "an" : "a";
    }

    public static string GetSupportEmail(IPartnerProviderService partnerProvider = null)
    {
        return partnerProvider?.GetPartner() switch
        {
            PartnerApp.MBanqApp => MbanqSupportEmail,
            PartnerApp.Poetryy => PoetryySupportEmail,
            PartnerApp.Eazy => EazySupportEmail,
            PartnerApp.USNational => USNationalSupportEmail,
            PartnerApp.Qorbis => QorbisSupportEmail,
            PartnerApp.ManaPacific => ManaPacificSupportEmail,
            PartnerApp.P2P => P2PSupportEmail,
            PartnerApp.Booq => BooqSupportEmail,
            PartnerApp.PayHammer=> PayHammerSupportEmail,
            _ => BnineSupportEmail,
        };
    }

    public static string GetSupportAddress(IPartnerProviderService partnerProvider)
    {
        return partnerProvider.GetPartner() switch
        {
            PartnerApp.Poetryy => PoetryySupportAddress,
            PartnerApp.Eazy => EazySupportAddress,
            PartnerApp.USNational => USNationalSupportAddress,
            PartnerApp.Qorbis => QorbisSupportAddress,
            PartnerApp.ManaPacific => ManaPacificSupportAddress,
            PartnerApp.P2P => P2pSupportAddress,
            PartnerApp.Booq => BooqSupportAddress,
            PartnerApp.PayHammer => PayHammerSupportAddress,
            _ => B9SupportAddress,
        };
    }

    public static string GetSupportPhone(IPartnerProviderService partnerProvider)
    {
        return partnerProvider?.GetPartner() switch
        {
            PartnerApp.Poetryy => PoetryySupportPhoneNumber,
            PartnerApp.Eazy => EazySupportPhoneNumber,
            PartnerApp.USNational => USNationalSupportPhoneNumber,
            PartnerApp.Qorbis => QorbisSupportPhoneNumber,
            PartnerApp.ManaPacific => ManaPacificSupportPhoneNumber,
            PartnerApp.P2P => P2PSupportPhoneNumber,
            PartnerApp.Booq => BooqSupportPhoneNumber,
            PartnerApp.PayHammer => PayHammerSupportPhoneNumber,
            _ => B9SupportPhoneNumber,
        };
    }

    public static string GetLogoFileName(IPartnerProviderService partnerProvider)
    {
        return partnerProvider.GetPartner() switch
        {
            PartnerApp.MBanqApp => MbanqLogoFileName,
            PartnerApp.Eazy => EazyLogoFileName,
            PartnerApp.Poetryy => PoetryyLogoFileName,
            PartnerApp.USNational => USNationalLogoFileName,
            PartnerApp.Qorbis => QorbisLogoFileName,
            PartnerApp.ManaPacific => ManaPacificLogoFileName,
            PartnerApp.P2P => P2PLogoFileName,
            PartnerApp.Booq => BooqLogoFileName,
            PartnerApp.PayHammer => PayHammerSupportEmail,
            _ => B9LogoFileName,
        };
    }

    public static string GetOTPCodeMessage(string code, IPartnerProviderService partnerProvider)
    {
        return $"{GetPartnerName(partnerProvider)} code is: {code}. It expires in 10 minutes. Never share it.";
    }

    public static string ReplaceCustomizedStrings(string text, IPartnerProviderService partnerProvider)
    {
        if (text == null)
        {
            return null;
        }

        switch (partnerProvider.GetPartner())
        {
            case PartnerApp.MBanqApp:
                text = text.Replace(BnineSupportEmail, MbanqSupportEmail);
                text = text.Replace(B9Name, MbanqName);
                break;
            case PartnerApp.Eazy:
                text = text.Replace(BnineSupportEmail, EazySupportEmail);
                text = text.Replace(B9Name, EazyName);
                break;
            case PartnerApp.Poetryy:
                text = text.Replace(BnineSupportEmail, PoetryySupportEmail);
                text = text.Replace(B9Name, PoetryyName);
                break;
            case PartnerApp.USNational:
                text = text.Replace(BnineSupportEmail, USNationalSupportEmail);
                text = text.Replace(B9Name, USNationalName);
                break;
            case PartnerApp.Qorbis:
                text = text.Replace(BnineSupportEmail, QorbisSupportEmail);
                text = text.Replace(B9Name, QorbisName);
                break;
            case PartnerApp.ManaPacific:
                text = text.Replace(BnineSupportEmail, ManaPacificSupportEmail);
                text = text.Replace(B9Name, ManaPacificName);
                break;
            case PartnerApp.P2P:
                text = text.Replace(BnineSupportEmail, P2PSupportEmail);
                text = text.Replace(B9Name, P2PName);
                break;
            case PartnerApp.Booq:
                text = text.Replace(BnineSupportEmail, BooqSupportEmail);
                text = text.Replace(B9Name, BooqName);
                break;
            case PartnerApp.PayHammer:
                text = text.Replace(BnineSupportEmail, PayHammerSupportEmail);
                text = text.Replace(B9Name, PayHammerName);
                break;
        }

        return text;
    }

    private static bool IsVowel(char firstLetter)
    {
        return Vowels.Contains(char.ToLower(firstLetter, CultureInfo.InvariantCulture));
    }
}
