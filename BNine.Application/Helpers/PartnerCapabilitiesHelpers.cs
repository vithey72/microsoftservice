﻿namespace BNine.Application.Helpers;

using System.Linq;
using Enums;

public static class PartnerCapabilitiesHelpers
{
    private static readonly PartnerApp[] PartnersToIssueVirtualCardsOnOnboarding = { PartnerApp.MBanqApp };
    private static readonly PartnerApp[] PartnersAllowedToIssueVirtualCards = { PartnerApp.Default, PartnerApp.MBanqApp };
    private static readonly PartnerApp[] PartnersRequiringSpecialFreeTariffHandling = { PartnerApp.Default, PartnerApp.MBanqApp };

    public static bool RequiresVirtualCardCreationOnOnboarding(this PartnerApp partner) =>
        PartnersToIssueVirtualCardsOnOnboarding.Contains(partner);

    public static bool CanIssueVirtualCard(this PartnerApp partner) =>
        PartnersAllowedToIssueVirtualCards.Contains(partner);

    public static bool RequiresSpecialFreeTariffHandling(this PartnerApp partner) =>
        PartnersRequiringSpecialFreeTariffHandling.Contains(partner);
}
