﻿#nullable enable
namespace BNine.Application.Helpers;

using Aggregates.CommonModels.Dialog;
using Aggregates.TransferErrorLog.Commands;
using Common;
using Constants;
using Models.MBanq;
using Models.MBanq.Transfers;
using Newtonsoft.Json;

public static class MbanqResponseHelper
{
    public static EitherDialog<GenericSuccessDialog, GenericFailDialog>? GetErrorModelIfAnyErrorsHappened(this
        GraphQLResponse<CreateAndSubmitTransferResponse> result, bool propagateToTitle = true)
    {
        if (result.Data?.Transfer == null)
        {
            string? errorText;
            if (result.Errors != null && result.Errors.Any(x =>
                    x.Extensions.Code.Trim()
                        .Equals(MbanqErrorCodes.InsufficientAccountBalanceKey, StringComparison.OrdinalIgnoreCase)))
            {
                errorText = "Insufficient account balance";
                return new EitherDialog<GenericSuccessDialog, GenericFailDialog>(
                    new GenericFailDialog
                    {
                        ErrorCode = ApiResponseErrorCodes.AccountBalance,
                        Body = propagateToTitle ?
                            new GenericDialogBodyViewModel
                        {
                            Title = errorText,
                            ButtonText = "DONE",
                        } :
                        new GenericDialogBodyViewModel()
                        {
                            Subtitle = errorText,
                            ButtonText = "DONE",
                        },
                    });
            }

            errorText = "Something went wrong";
            return new EitherDialog<GenericSuccessDialog, GenericFailDialog>(
                new GenericFailDialog
                {
                    ErrorCode = ApiResponseErrorCodes.MBanqValidation,
                    Body = new GenericDialogBodyViewModel
                    {
                        Title = errorText,
                        ButtonText = "DONE",
                    },
                });

        }

        return null;
    }

    public static AddTransferErrorLogCommand CreateErrorLogCommand(
        this GraphQLResponse<CreateAndSubmitTransferResponse> result, Transfer transfer)
    {
        return new AddTransferErrorLogCommand()
        {
            TransferNetworkType = transfer.PaymentType,
            TransferDirection = transfer.Type,
            ExceptionHeader = result.Errors?.FirstOrDefault()?.Message ?? "Unknown error",
            MbanqRawErrorPayload = result?.Errors != null ? JsonConvert.SerializeObject(result.Errors) : null
        };
    }

}
