﻿namespace BNine.Application.Helpers
{
    using System.Reflection;
    using Interfaces;
    using iTextSharp.text;
    using iTextSharp.text.pdf;
    using iTextSharp.text.pdf.draw;

    public static class ManualSwitchPdfGenerator
    {
        private const string EvolveBankAddress = "6070 Poplar Ave suite 200, Memphis, TN 38119";
        private const string EvolveBankName = "Evolve Bank and Trust";

        public static byte[] GeneratePdf(
            string beneficiary,
            string accountNumber,
            string routingNumber,
            IPartnerProviderService providerService)
        {
            var leftHeaderText = "B9";
            var rightHeaderText = @"Customer Support
support@bnine.com
";

            var memoryStream = new MemoryStream();

            var document = new Document(PageSize.A4, 30, 30, 30, 30);

            var writer = PdfWriter.GetInstance(document, memoryStream);

            #region FontsSettings

            var leftHeaderFont = FontFactory.GetFont("Times New Roman", 30, Font.BOLD, BaseColor.Black);

            var defaultTextFont = FontFactory.GetFont("Times New Roman", 12, BaseColor.Gray);

            var defaultBoldTextFont = FontFactory.GetFont("Times New Roman", 12, Font.BOLD, BaseColor.Gray);

            var titleFont = FontFactory.GetFont("Times New Roman", 20, Font.BOLD, BaseColor.Black);

            var subTitleFont = FontFactory.GetFont("Times New Roman", 16, Font.BOLD, BaseColor.Black);

            var footerFont = FontFactory.GetFont("Times New Roman", 8, BaseColor.Gray);

            #endregion

            document.AddTitle("Early salary");
            document.Open();

            var checkbox = new PdfPTable(1) { WidthPercentage = 55 };
            checkbox.AddCell(new PdfPCell(new Phrase(" ")));

            #region Headers

            var table0 = new PdfPTable(4) { WidthPercentage = 100, SpacingAfter = 10f };
            table0.SetWidths(new[] { 1f, 1f, 6f, 3f });

            var buildDir = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
            var filePath = buildDir + @"\Images\BNineColoredLogo.jpg";

            var logo = Image.GetInstance(filePath);
            logo.ScaleToFit(100f, 60f);

            var leftHeaderCell1 = new PdfPCell(logo) { Border = 0, VerticalAlignment = Element.ALIGN_TOP };

            var leftHeaderCell2 = new PdfPCell(new Paragraph(leftHeaderText, leftHeaderFont))
            {
                Border = 0,
                PaddingTop = 10f,
            };

            var emptySpacingCell = new PdfPCell { Border = 0 };

            var supportPhoneNumber = StringProvider.GetSupportPhone(providerService);

            var rightHeaderFullText = new Paragraph
            {
                new Phrase(rightHeaderText, defaultTextFont),
                new Phrase(supportPhoneNumber, defaultTextFont)
            };

            var rightHeaderCell = new PdfPCell(rightHeaderFullText)
            {
                Border = 0,
                HorizontalAlignment = Element.ALIGN_LEFT,
                PaddingTop = 15f,
            };

            table0.AddCell(leftHeaderCell1);
            table0.AddCell(leftHeaderCell2);
            table0.AddCell(emptySpacingCell);
            table0.AddCell(rightHeaderCell);

            document.Add(table0);

            #endregion

            #region Title

            var pTitle = new Paragraph("Direct deposit enrollment form", titleFont) { SpacingAfter = 30f };
            document.Add(pTitle);

            #endregion

            #region Account details

            var pTableName = new Paragraph("Account information", subTitleFont) { SpacingAfter = 20f };
            document.Add(pTableName);

            var table1 = new PdfPTable(4) { WidthPercentage = 100, SpacingAfter = 20f };
            table1.SetWidths(new float[] { 1f, 2f, 1.3f, 1.7f });

            var cell11 = new PdfPCell { Border = 0, BorderWidthTop = 1, PaddingBottom = 11f, BorderColor = BaseColor.Gray };
            cell11.AddElement(new Paragraph("NAME", defaultTextFont));


            var cell12 = new PdfPCell { Border = 0, BorderWidthTop = 1, BorderColor = BaseColor.Gray };
            cell12.AddElement(new Paragraph(beneficiary));

            var cell13 = new PdfPCell { Border = 0, BorderWidthTop = 1, BorderColor = BaseColor.Gray };
            cell13.AddElement(new Paragraph("Account Number", defaultTextFont));

            var cell14 = new PdfPCell { Border = 0, BorderWidthTop = 1, BorderColor = BaseColor.Gray };
            cell14.AddElement(new Paragraph(accountNumber));

            table1.AddCell(cell11);
            table1.AddCell(cell12);
            table1.AddCell(cell13);
            table1.AddCell(cell14);

            var cell21 = new PdfPCell { Border = 0, BorderWidthTop = 1, PaddingBottom = 11f, BorderColor = BaseColor.Gray };
            cell21.AddElement(new Paragraph("Bank name", defaultTextFont));

            var cell22 = new PdfPCell { Border = 0, BorderWidthTop = 1, BorderColor = BaseColor.Gray };
            cell22.AddElement(new Paragraph(EvolveBankName));

            var cell23 = new PdfPCell { Border = 0, BorderWidthTop = 1, BorderColor = BaseColor.Gray };
            cell23.AddElement(new Paragraph("Routing Code", defaultTextFont));

            var cell24 = new PdfPCell { Border = 0, BorderWidthTop = 1, BorderColor = BaseColor.Gray };
            cell24.AddElement(new Paragraph(routingNumber));

            table1.AddCell(cell21);
            table1.AddCell(cell22);
            table1.AddCell(cell23);
            table1.AddCell(cell24);

            var cell31 = new PdfPCell { Border = 0, BorderWidthTop = 1, BorderWidthBottom = 1, BorderColor = BaseColor.Gray };
            cell31.AddElement(new Paragraph("Bank Address", defaultTextFont));

            var cell32 = new PdfPCell
            {
                Border = 0,
                BorderWidthTop = 1,
                BorderWidthBottom = 1,
                PaddingBottom = 11f,
                BorderColor = BaseColor.Gray
            };
            cell32.AddElement(new Paragraph(EvolveBankAddress));

            var cell33 = new PdfPCell { Border = 0, BorderWidthTop = 1, BorderWidthBottom = 1, BorderColor = BaseColor.Gray };

            var cell34 = new PdfPCell { Border = 0, BorderWidthTop = 1, BorderWidthBottom = 1, BorderColor = BaseColor.Gray };

            table1.AddCell(cell31);
            table1.AddCell(cell32);
            table1.AddCell(cell33);
            table1.AddCell(cell34);

            document.Add(table1);

            #endregion

            #region Amount

            var pAmount = new Paragraph("Amount", subTitleFont) { SpacingAfter = 10f };
            document.Add(pAmount);

            var table2 = new PdfPTable(6) { WidthPercentage = 100, SpacingAfter = 20f };
            table2.SetWidths(new float[] { 1f, 4f, 1f, 4f, 1f, 4f });

            var checkboxCell = new PdfPCell { Border = 0, PaddingBottom = 11f, PaddingTop = 11f, BorderColor = BaseColor.Gray };
            checkboxCell.AddElement(checkbox);

            var cell2_11 = new PdfPCell { Border = 0, PaddingBottom = 11f, BorderColor = BaseColor.Gray };
            cell2_11.AddElement(new Paragraph("Deposit my entire paycheck", defaultTextFont));

            var pDollarsAmount = new Paragraph
            {
                new Phrase("Amount ($)", defaultTextFont),
                new LineSeparator(1, 62, BaseColor.Gray, Element.ALIGN_RIGHT, -6)
            };


            var cell2_12 = new PdfPCell { Border = 0, PaddingBottom = 11f, BorderColor = BaseColor.Gray };
            cell2_12.AddElement(new Paragraph("Deposit dollars of my paycheck", defaultTextFont));
            cell2_12.AddElement(pDollarsAmount);

            var pPercentAmount = new Paragraph
            {
                new Phrase("Amount (%)", defaultTextFont),
                new LineSeparator(1, 62, BaseColor.Gray, Element.ALIGN_RIGHT, -6)
            };

            var cell2_13 = new PdfPCell { Border = 0, PaddingBottom = 11f, BorderColor = BaseColor.Gray };
            cell2_13.AddElement(new Paragraph("Deposit % of my paycheck", defaultTextFont));
            cell2_13.AddElement(pPercentAmount);

            table2.AddCell(checkboxCell);
            table2.AddCell(cell2_11);
            table2.AddCell(checkboxCell);
            table2.AddCell(cell2_12);
            table2.AddCell(checkboxCell);
            table2.AddCell(cell2_13);

            document.Add(table2);

            #endregion

            #region AuthorizationText

            var authParagrahp = new Paragraph("Authorization", subTitleFont) { SpacingAfter = 10f };
            document.Add(authParagrahp);

            var pAuthorizationTextFirstLine = new Paragraph
            {
                new Phrase("I authorize", defaultTextFont),
                new LineSeparator(1, 89, BaseColor.Gray, Element.ALIGN_RIGHT, -6)
            };
            pAuthorizationTextFirstLine.SpacingAfter = 6f;

            document.Add(pAuthorizationTextFirstLine);

            var pAuthorizationText = new Paragraph("(employer/payer) to initiate credit entries, and, if necessary to initiate any debit entries to correct previous credit errors, to my B9 Account. I understand that this authorization replaces any previous authorization, and will remain in effect until the company named above has received written notification from me of its termination in a reasonable enough time to act."
                , defaultTextFont)
            {
                SpacingAfter = 25f,
            };
            document.Add(pAuthorizationText);

            #endregion

            #region Signature Date

            var table3 = new PdfPTable(2) { WidthPercentage = 100, SpacingAfter = 25f };

            var pSignature = new Paragraph
            {
                new Phrase("Signature", defaultTextFont),
                new LineSeparator(1, 60, BaseColor.Gray, Element.ALIGN_CENTER, -6)
            };

            var cell3_11 = new PdfPCell { Border = 0, PaddingBottom = 11f, BorderColor = BaseColor.Gray };
            cell3_11.AddElement(pSignature);

            var pDate = new Paragraph
            {
                new Phrase("Date", defaultTextFont),
                new LineSeparator(1, 90, BaseColor.Gray, Element.ALIGN_RIGHT, -6)
            };

            var cell3_12 = new PdfPCell { Border = 0, PaddingBottom = 11f, BorderColor = BaseColor.Gray };
            cell3_12.AddElement(pDate);

            table3.AddCell(cell3_11);
            table3.AddCell(cell3_12);

            document.Add(table3);

            #endregion

            #region Footer

            var footerTable = new PdfPTable(1) { WidthPercentage = 100 };

            var footerTableCell1 = new PdfPCell(new Paragraph("B9 is not a bank. Banking Services are provided by Mbanq banking partner, Evolve Bank & Trust, Member FDIC. The B9 Account\u2120 and B9 Visa\u00ae Card are provided by Evolve Bank & Trust, Member FDIC. B9 Account\u2120 is a service mark of B9, Inc., which offers demand deposit accounts among its suite of financial products and services.", footerFont))
            {
                Border = 0,
                HorizontalAlignment = Element.ALIGN_JUSTIFIED
            };

            footerTable.AddCell(footerTableCell1);

            var footerTableCell2 = new PdfPCell(new Paragraph($"All customer data is properly secured in accordance with federal and state regulations and will be retained until all record retention requirements are met. B9 does not share customer data with unaffiliated third parties. Please visit to https://bnine.com/privacy-policy to access our current privacy policy.", footerFont))
            {
                Border = 0,
                HorizontalAlignment = Element.ALIGN_JUSTIFIED
            };

            footerTable.AddCell(footerTableCell2);



            document.Add(footerTable);

            #endregion

            document.Close();

            writer.Close();

            return memoryStream.ToArray();
        }

        public static byte[] GenerateManualEarlySalaryPdf(
            string beneficiary,
            string accountNumber,
            string routingNumber,
            decimal? amountAllocation,
            decimal? percentAllocation,
            string employerName,
            IPartnerProviderService partnerService)
        {

            #region TextConstants

            var leftHeaderText = "B9";
            var rightHeaderText = @"Customer Support
support@bnine.com
";

            #endregion

            var memoryStream = new MemoryStream();

            var document = new Document(PageSize.A4, 30, 30, 30, 30);

            var writer = PdfWriter.GetInstance(document, memoryStream);

            #region FontsSettings

            var leftHeaderFont = FontFactory.GetFont("Times New Roman", 30, Font.BOLD, BaseColor.Black);

            var defaultTextFont = FontFactory.GetFont("Times New Roman", 12, BaseColor.Gray);

            var underlinedTextFont = FontFactory.GetFont("Times New Roman", 12, Font.UNDERLINE, BaseColor.Gray);

            var defaultBoldTextFont = FontFactory.GetFont("Times New Roman", 12, Font.BOLD, BaseColor.Gray);

            var titleFont = FontFactory.GetFont("Times New Roman", 20, Font.BOLD, BaseColor.Black);

            var subTitleFont = FontFactory.GetFont("Times New Roman", 16, Font.BOLD, BaseColor.Black);

            var footerFont = FontFactory.GetFont("Times New Roman", 8, BaseColor.Gray);

            #endregion

            document.AddTitle("Early salary");
            document.Open();

            var checkbox = new PdfPTable(1) { WidthPercentage = 55 };
            checkbox.AddCell(new PdfPCell(new Phrase(" ")));

            #region Headers

            var table0 = new PdfPTable(4) { WidthPercentage = 100, SpacingAfter = 10f };
            table0.SetWidths(new float[] { 1f, 1f, 6f, 3f });

            var buildDir = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
            var filePath = buildDir + @"\Images\BNineColoredLogo.jpg";

            var logo = Image.GetInstance(filePath);
            logo.ScaleToFit(100f, 60f);

            var leftHeaderCell1 = new PdfPCell(logo) { Border = 0, VerticalAlignment = Element.ALIGN_TOP };

            var leftHeaderCell2 = new PdfPCell(new Paragraph(leftHeaderText, leftHeaderFont))
            {
                Border = 0,
                PaddingTop = 10f,
            };

            var emptySpacingCell = new PdfPCell { Border = 0 };

            var supportPhoneNumber = StringProvider.GetSupportPhone(partnerService);

            var rightHeaderFullText = new Paragraph
            {
                new Phrase(rightHeaderText, defaultTextFont),
                new Phrase(supportPhoneNumber, defaultTextFont)
            };

            var rightHeaderCell = new PdfPCell(rightHeaderFullText)
            {
                Border = 0,
                HorizontalAlignment = Element.ALIGN_LEFT,
                PaddingTop = 15f,
            };

            table0.AddCell(leftHeaderCell1);
            table0.AddCell(leftHeaderCell2);
            table0.AddCell(emptySpacingCell);
            table0.AddCell(rightHeaderCell);

            document.Add(table0);

            #endregion

            #region Title

            var pTitle = new Paragraph("Direct deposit enrollment form", titleFont) { SpacingAfter = 30f };
            document.Add(pTitle);

            #endregion

            #region Account details

            var pTableName = new Paragraph("Account information", subTitleFont) { SpacingAfter = 20f };
            document.Add(pTableName);

            var table1 = new PdfPTable(4) { WidthPercentage = 100, SpacingAfter = 20f };
            table1.SetWidths(new float[] { 1f, 2f, 1.3f, 1.7f });

            var cell11 = new PdfPCell { Border = 0, BorderWidthTop = 1, PaddingBottom = 11f, BorderColor = BaseColor.Gray };
            cell11.AddElement(new Paragraph("NAME", defaultTextFont));


            var cell12 = new PdfPCell { Border = 0, BorderWidthTop = 1, BorderColor = BaseColor.Gray };
            cell12.AddElement(new Paragraph(beneficiary));

            var cell13 = new PdfPCell { Border = 0, BorderWidthTop = 1, BorderColor = BaseColor.Gray };
            cell13.AddElement(new Paragraph("Account Number", defaultTextFont));

            var cell14 = new PdfPCell { Border = 0, BorderWidthTop = 1, BorderColor = BaseColor.Gray };
            cell14.AddElement(new Paragraph(accountNumber));

            table1.AddCell(cell11);
            table1.AddCell(cell12);
            table1.AddCell(cell13);
            table1.AddCell(cell14);

            var cell21 = new PdfPCell { Border = 0, BorderWidthTop = 1, PaddingBottom = 11f, BorderColor = BaseColor.Gray };
            cell21.AddElement(new Paragraph("Bank name", defaultTextFont));

            var cell22 = new PdfPCell { Border = 0, BorderWidthTop = 1, BorderColor = BaseColor.Gray };
            cell22.AddElement(new Paragraph(EvolveBankName));

            var cell23 = new PdfPCell { Border = 0, BorderWidthTop = 1, BorderColor = BaseColor.Gray };
            cell23.AddElement(new Paragraph("Routing Code", defaultTextFont));

            var cell24 = new PdfPCell { Border = 0, BorderWidthTop = 1, BorderColor = BaseColor.Gray };
            cell24.AddElement(new Paragraph(routingNumber));

            table1.AddCell(cell21);
            table1.AddCell(cell22);
            table1.AddCell(cell23);
            table1.AddCell(cell24);

            var cell31 = new PdfPCell { Border = 0, BorderWidthTop = 1, BorderWidthBottom = 1, BorderColor = BaseColor.Gray };
            cell31.AddElement(new Paragraph("Bank Address", defaultTextFont));

            var cell32 = new PdfPCell
            {
                Border = 0,
                BorderWidthTop = 1,
                BorderWidthBottom = 1,
                PaddingBottom = 11f,
                BorderColor = BaseColor.Gray,
            };
            cell32.AddElement(new Paragraph(EvolveBankAddress));

            var cell33 = new PdfPCell { Border = 0, BorderWidthTop = 1, BorderWidthBottom = 1, BorderColor = BaseColor.Gray };

            var cell34 = new PdfPCell { Border = 0, BorderWidthTop = 1, BorderWidthBottom = 1, BorderColor = BaseColor.Gray };

            table1.AddCell(cell31);
            table1.AddCell(cell32);
            table1.AddCell(cell33);
            table1.AddCell(cell34);

            document.Add(table1);

            #endregion

            #region Amount

            var pAmount = new Paragraph("Amount", subTitleFont) { SpacingAfter = 10f };
            document.Add(pAmount);

            var table2 = new PdfPTable(6) { WidthPercentage = 100, SpacingAfter = 20f };
            table2.SetWidths(new float[] { 1f, 4f, 1f, 4f, 1f, 4f });

            var checkboxCell = new PdfPCell { Border = 0, PaddingBottom = 11f, PaddingTop = 11f, BorderColor = BaseColor.Gray };
            checkboxCell.AddElement(checkbox);

            var checkboxMarkedCell = new PdfPCell
            {
                Border = 0,
                PaddingBottom = 11f,
                PaddingTop = 11f,
                BorderColor = BaseColor.Gray,
            };

            var checkedLogoDir = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
            var checkedLogoPath = checkedLogoDir + @"\Images\checkmark.png";

            var checkedImage = Image.GetInstance(checkedLogoPath);
            checkedImage.ScaleToFit(20f, 20f);

            var checkedCell = new PdfPCell(checkedImage) { Border = 0, PaddingTop = 11f, VerticalAlignment = Element.ALIGN_TOP };

            var uncheckedLogoDir = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
            var uncheckedLogoPath = uncheckedLogoDir + @"\Images\checkmark-unckecked.png";

            var uncheckedImage = Image.GetInstance(uncheckedLogoPath);
            uncheckedImage.ScaleToFit(20f, 20f);

            var uncheckedCell = new PdfPCell(uncheckedImage) { Border = 0, PaddingTop = 11f, VerticalAlignment = Element.ALIGN_TOP };

            var cell2_11 = new PdfPCell { Border = 0, PaddingBottom = 11f, BorderColor = BaseColor.Gray };
            cell2_11.AddElement(new Paragraph("Deposit my entire paycheck", defaultTextFont));

            var pDollarsAmount = new Paragraph
            {
                new Phrase("Amount ($)", defaultTextFont),
                new Phrase($"  {amountAllocation}", defaultTextFont),
                new LineSeparator(1, 62, BaseColor.Gray, Element.ALIGN_RIGHT, -6)
            };

            var cell2_12 = new PdfPCell { Border = 0, PaddingBottom = 11f, BorderColor = BaseColor.Gray };
            cell2_12.AddElement(new Paragraph("Deposit dollars of my paycheck", defaultTextFont));
            cell2_12.AddElement(pDollarsAmount);

            var percentValue = percentAllocation.HasValue && percentAllocation.Value < 100 ? percentAllocation : null;

            var pPercentAmount = new Paragraph
            {
                new Phrase($"Amount (%)  {percentValue}", defaultTextFont),
                new LineSeparator(1, 62, BaseColor.Gray, Element.ALIGN_RIGHT, -6)
            };

            var cell2_13 = new PdfPCell { Border = 0, PaddingBottom = 11f, BorderColor = BaseColor.Gray };
            cell2_13.AddElement(new Paragraph("Deposit % of my paycheck", defaultTextFont));
            cell2_13.AddElement(pPercentAmount);


            if (percentAllocation.HasValue && percentAllocation == 100)
            {
                table2.AddCell(checkedCell);
            }
            else
            {
                table2.AddCell(uncheckedCell);
            }
            table2.AddCell(cell2_11);
            if (amountAllocation.HasValue)
            {
                table2.AddCell(checkedCell);
            }
            else
            {
                table2.AddCell(uncheckedCell);
            }
            table2.AddCell(cell2_12);
            if (percentAllocation.HasValue && percentAllocation != 100)
            {
                table2.AddCell(checkedCell);
            }
            else
            {
                table2.AddCell(uncheckedCell);
            }
            table2.AddCell(cell2_13);

            document.Add(table2);

            #endregion

            #region AuthorizationText

            var authParagrahp = new Paragraph("Authorization", subTitleFont) { SpacingAfter = 10f };
            document.Add(authParagrahp);

            var employerBlank = "                                                                                   ";
            var pAuthorizationTextFirstLine = new Paragraph
            {
                new Phrase($"I authorize ", defaultTextFont),
                new Phrase($"{employerName ?? employerBlank}", underlinedTextFont),
                new Phrase($" (employer/payer) to initiate credit entries, and, if necessary to initiate any debit entries to correct previous credit errors, to my B9 Account. I understand that this authorization replaces any previous authorization, and will remain in effect until the company named above has received written notification from me of its termination in a reasonable enough time to act.", defaultTextFont),
            };

            document.Add(pAuthorizationTextFirstLine);

            #endregion

            #region Signature Date

            var table3 = new PdfPTable(2) { WidthPercentage = 100, SpacingAfter = 25f };

            var pSignature = new Paragraph
            {
                new Phrase("Signature", defaultTextFont),
                new LineSeparator(1, 60, BaseColor.Gray, Element.ALIGN_CENTER, -6)
            };

            var cell3_11 = new PdfPCell { Border = 0, PaddingBottom = 11f, BorderColor = BaseColor.Gray };
            cell3_11.AddElement(pSignature);

            var pDate = new Paragraph
            {
                new Phrase("Date", defaultTextFont),
                new LineSeparator(1, 90, BaseColor.Gray, Element.ALIGN_RIGHT, -6)
            };

            var cell3_12 = new PdfPCell { Border = 0, PaddingBottom = 11f, BorderColor = BaseColor.Gray };
            cell3_12.AddElement(pDate);

            table3.AddCell(cell3_11);
            table3.AddCell(cell3_12);

            document.Add(table3);

            #endregion

            #region Footer

            var footerTable = new PdfPTable(1) { WidthPercentage = 100 };

            var footerTableCell1 = new PdfPCell(new Paragraph("B9 is not a bank. Banking Services are provided by Mbanq banking partner, Evolve Bank & Trust, Member FDIC. The B9 Account\u2120 and B9 Visa\u00ae Card are provided by Evolve Bank & Trust, Member FDIC. B9 Account\u2120 is a service mark of B9, Inc., which offers demand deposit accounts among its suite of financial products and services.", footerFont))
            {
                Border = 0,
                HorizontalAlignment = Element.ALIGN_JUSTIFIED
            };

            footerTable.AddCell(footerTableCell1);

            var footerTableCell2 = new PdfPCell(new Paragraph($"All customer data is properly secured in accordance with federal and state regulations and will be retained until all record retention requirements are met. B9 does not share customer data with unaffiliated third parties. Please visit to https://bnine.com/privacy-policy to access our current privacy policy.", footerFont))
            {
                Border = 0,
                HorizontalAlignment = Element.ALIGN_JUSTIFIED
            };

            footerTable.AddCell(footerTableCell2);

            document.Add(footerTable);

            #endregion

            document.Close();

            writer.Close();

            return memoryStream.ToArray();
        }
    }
}
