﻿namespace BNine.Application.Helpers;

using System.Globalization;
using BNine.Enums.Transfers;

internal class CurrencyFormattingHelper
{
    public static string AsRegular(decimal amount)
    {
        return amount.ToString("C2", CultureInfo.CreateSpecificCulture("en-us"));
    }

    public static string AsRegularWithSign(decimal amount, TransferDirection direction)
    {
        var sign = direction == TransferDirection.Credit ? "+" : "-";
        return sign + AsRegular(amount);
    }

    public static string AsRounded(decimal amount)
    {
        return amount.ToString("C0", CultureInfo.CreateSpecificCulture("en-us"));
    }

    public static string AsAdaptive(decimal amount)
    {
        return decimal.Floor(amount) == amount ? AsRounded(amount) : AsRegular(amount);
    }

    public static TransferDirection? ConvertDirection(TransferDirectionSimplified? directionSimplified) => directionSimplified switch
    {
        TransferDirectionSimplified.Incoming => TransferDirection.Credit,
        TransferDirectionSimplified.Outgoing => TransferDirection.Debit,
        _ => null,
    };

    public static TransferDirectionSimplified? ConvertDirection(TransferDirection? direction) => direction switch
    {
        TransferDirection.Credit => TransferDirectionSimplified.Incoming,
        TransferDirection.Debit => TransferDirectionSimplified.Outgoing,
        _ => null,
    };

    public static string AsRoundedWithNoTrailingZeroes(decimal amount)
    {
        return amount.ToString("G29", CultureInfo.CreateSpecificCulture("en-us"));
    }
}
