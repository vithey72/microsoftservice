﻿namespace BNine.Application.Helpers
{
    using System.Globalization;
    using Enums;

    public static class LocalizationHelper
    {
        public static Language GetLanguage(CultureInfo cultureInfo)
        {
            if(cultureInfo.Name == "es")
            {
                return Language.Spanish;
            }
            else
            {
                return Language.English;
            }
        }
    }
}
