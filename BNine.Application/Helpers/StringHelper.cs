﻿namespace BNine.Application.Helpers
{
    using System;

    public class StringHelper
    {
        public static string TakeLast(string value, int number)
        {
            if (string.IsNullOrEmpty(value))
            {
                return value;
            }

            if (value.Length < number)
            {
                throw new ArgumentException("String length cannot be less then number");
            }

            return value.Substring(value.Length - number, number);
        }
    }
}
