﻿namespace BNine.Application.Aggregates.CheckingAccounts.Models.TransactionAnalytics;

using CommonModels.Dialog;
using Enums.Transfers;
using Enums.Transfers.Controller;
using Newtonsoft.Json;

public class TransactionAnalyticsListItem
{
    public string Title
    {
        get;
        set;
    }

    public string Subtitle
    {
        get;
        set;
    }

    public string Key
    {
        get;
        set;
    }

    public string IconUrl
    {
        get;
        set;
    }

    public string Color
    {
        get;
        set;
    }

    public decimal TotalAmount
    {
        get;
        set;
    }

    public int TransactionId
    {
        get;
        set;
    }

    public BankTransferCompletionState TransactionStatus
    {
        get;
        set;
    }

    public int TotalOperations
    {
        get;
        set;
    }

    public string TransactionStatusLine
    {
        get;
        set;
    }

    public string AmountString
    {
        get;
        set;
    }

    public string AmountStringColor
    {
        get;
        set;
    }

    public DateTime TimeStampUtc
    {
        get;
        set;
    }

    [JsonProperty("direction")]
    public TransferDirectionSimplified? Direction
    {
        get;
        set;
    }

    public GenericDialogRichBodyViewModel Hint
    {
        get;
        set;
    }

    public string TransactionHintText
    {
        get;
        set;
    }
}
