﻿namespace BNine.Application.Aggregates.CheckingAccounts.Models.TransactionAnalytics;

using Newtonsoft.Json;

public class TransactionAnalyticsFilteringParameters
{
    public string CategoryKey
    {
        get;
        set;
    } = "default";

    /// <summary>
    /// Range from date to date to filter based on.
    /// </summary>
    [JsonProperty("timePeriod")]
    public GetFilteredTransfersFilter.TimePeriodFilter TimePeriod
    {
        get;
        set;
    } = new()
    {
        MinDateUtc = DateTime.UtcNow.AddDays(-30),
        MaxDateUtc = DateTime.UtcNow
    };
}
