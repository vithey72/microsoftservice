﻿namespace BNine.Application.Aggregates.CheckingAccounts.Models.TransactionAnalytics;

using Application.Models;
using CommonModels.Dialog;
using Newtonsoft.Json;

public class TransactionAnalyticsViewModel
{
    public TransactionAnalyticsViewModel()
    {
        Parents = new List<CategoryParent>();
        SubCategories = new List<TransactionAnalyticsListItem>();
    }

    public List<CategoryParent> Parents
    {
        get;
        set;
    }

    public CircleDiagram CircleDiagram
    {
        get;
        set;
    }

    public string Key
    {
        get;
        set;
    }

    public List<TransactionAnalyticsListItem> SubCategories
    {
        get;
        set;
    }

    public PaginatedList<TransactionAnalyticsListItem> TransactionList
    {
        get;
        set;
    }

    public bool IsPeriodSelectionEnabled
    {
        get;
        set;
    }

    /// <summary>
    /// Range from date to date to filter based on.
    /// </summary>
    [JsonProperty("timePeriod")]
    public GetFilteredTransfersFilter.TimePeriodFilter TimePeriod
    {
        get;
        set;
    } = new();

    public string BankAccountLastNumbers
    {
        get;
        set;
    }

    public GenericDialogRichBodyViewModel SpendingOnAccountHint
    {
        get;
        set;
    }

    public string SubCategoriesTitle
    {
        get;
        set;
    }
}
