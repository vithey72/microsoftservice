﻿namespace BNine.Application.Aggregates.CheckingAccounts.Models.TransactionAnalytics;

public class CircleDiagram
{
    public List<CategorySubAmount> SubAmounts
    {
        get;
        set;
    }

    public decimal TotalAmount
    {
        get;
        set;
    }

    public class CategorySubAmount
    {
        public decimal Amount
        {
            get;
            set;
        }

        public string Color
        {
            get;
            set;
        }

        public string CategoryKey
        {
            get;
            set;
        }
    }
}
