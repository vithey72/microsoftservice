﻿namespace BNine.Application.Aggregates.CheckingAccounts.Models.TransactionAnalytics;

public class CategoryParent
{
    public string Title
    {
        get;
        set;
    }

    //Filled only for root level categories
    public string IconUrl
    {
        get;
        set;
    }

    //Filled only for root level categories
    public string Color
    {
        get;
        set;
    }

    public string Key
    {
        get;
        set;
    }
}
