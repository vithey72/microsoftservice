﻿namespace BNine.Application.Aggregates.CheckingAccounts.Models
{
    using BNine.Enums.Transfers;

    public class TransactionModel
    {
        public int Id
        {
            get; set;
        }

        public string Type
        {
            get; set;
        }

        public TransferDirection Direction
        {
            get; set;
        }

        public decimal Amount
        {
            get; set;
        }

        public DateTime PostingDate
        {
            get; set;
        }

        public DateTime TransactionDate
        {
            get; set;
        }

        public decimal RunningBalance
        {
            get; set;
        }

        public string Reference
        {
            get; set;
        }

        public string CreditorName
        {
            get; set;
        }

        public string DebtorName
        {
            get; set;
        }
    }
}
