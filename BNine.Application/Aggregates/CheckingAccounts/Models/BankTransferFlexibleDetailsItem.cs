﻿namespace BNine.Application.Aggregates.CheckingAccounts.Models;

using Newtonsoft.Json;

public class BankTransferFlexibleDetailsItem
{
    [JsonProperty("title")]
    public string Title
    {
        get;
        set;
    }

    [JsonProperty("value")]
    public string Value
    {
        get;
        set;
    }
}
