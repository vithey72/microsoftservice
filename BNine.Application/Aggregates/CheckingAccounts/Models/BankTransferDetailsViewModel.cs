﻿namespace BNine.Application.Aggregates.CheckingAccounts.Models;

using CommonModels.Dialog;
using Enums.Transfers;
using Newtonsoft.Json;

public class BankTransferDetailsViewModel
{
    /// <summary>
    /// Unique ID of transfer in the bank.
    /// </summary>
    [JsonProperty("id")]
    public int Id
    {
        get;
        set;
    }

    /// <summary>
    /// Completed, Pending, etc.
    /// </summary>
    [JsonProperty("header")]
    public string Header
    {
        get;
        set;
    }

    [JsonProperty("dropDownList")]
    public DropDownListWithBulletPoints DropDownList
    {
        get;
        set;
    }

    /// <summary>
    /// Title and subtitle of the details page.
    /// </summary>
    [JsonProperty("pageTitle")]
    public GenericDialogHeaderViewModel PageTitle
    {
        get;
        set;
    } = new();

    /// <summary>
    /// Line that states who received or sent money (depending on transfer direction).
    /// </summary>
    [JsonProperty("debtorOrCreditorName")]
    public string DebtorOrCreditorName
    {
        get;
        set;
    }

    [JsonProperty("timeStampUtc")]
    public DateTime TimeStampUtc
    {
        get;
        set;
    }

    /// <summary>
    /// Url of the icon in the center of the screen to display to user.
    /// </summary>
    [JsonProperty("iconUrl")]
    public string IconUrl
    {
        get;
        set;
    }

    /// <summary>
    /// String representation of amount. No sign (for now).
    /// </summary>
    [JsonProperty("amount")]
    public string Amount
    {
        get;
        set;
    }

    /// <summary>
    /// ACH transfer, Paid to B9 member, B9 Card Charge, etc.
    /// </summary>
    [JsonProperty("operationTypeHeader")]
    public string OperationTypeHeader
    {
        get;
        set;
    }

    /// <summary>
    /// To show or not to show
    /// </summary>
    [JsonProperty("isRepeatable")]
    public bool IsRepeatable
    {
        get;
        set;
    }

    /// <summary>
    /// If not null, then there is a favorite transfer saved based on this transfer.
    /// </summary>
    [JsonProperty("favoriteTransferId")]
    public Guid? FavoriteTransferId
    {
        get;
        set;
    }

    /// <summary>
    /// Whether user marked this particular transfer as potential income.
    /// Null means non applicable (button not shown).
    /// </summary>
    [JsonProperty("markedAsPotentialIncome")]
    public bool? MarkedAsPotentialIncome
    {
        get;
        set;
    }

    /// <summary>
    /// The rest of the details at the bottom of the screen.
    /// </summary>
    [JsonProperty("details")]
    public List<BankTransferFlexibleDetailsItem> Details
    {
        get;
        set;
    }

    /// <summary>
    /// For debug purposes.
    /// </summary>
    [JsonProperty("mcc")]
    public int? Mcc
    {
        get;
        set;
    }

    [JsonIgnore]
    public TransferDisplayType SavingsTransferType
    {
        get;
        set;
    }

    [JsonIgnore]
    public PendingTransferDisplayType PendingTransferType
    {
        get;
        set;
    }

    /// <summary>
    /// For debug purposes.
    /// </summary>
    [JsonProperty("displayType")]
    public string DisplayType => SavingsTransferType != TransferDisplayType.Unknown
        ? SavingsTransferType.ToString()
        : PendingTransferType.ToString();
}
