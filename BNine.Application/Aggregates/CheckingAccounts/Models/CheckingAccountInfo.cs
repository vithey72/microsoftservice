﻿namespace BNine.Application.Aggregates.CheckingAccounts.Models
{
    using Newtonsoft.Json;

    public class CheckingAccountInfo
    {
        [JsonProperty("accountNumber")]
        public string AccountNumber
        {
            get; set;
        }

        [JsonProperty("routingNumber")]
        public string RoutingNumber
        {
            get; set;
        }

        [JsonProperty("ownerFirstname")]
        public string OwnerFirstName
        {
            get; set;
        }

        [JsonProperty("ownerLastname")]
        public string OwnerLastName
        {
            get; set;
        }

        [JsonProperty("ownerFullAddress")]
        public CheckingAccountOwnerAddress OwnerAddress
        {
            get; set;
        }

        [JsonProperty("bankAddress")]
        public string BankAddress
        {
            get; set;
        }
    }
}
