﻿namespace BNine.Application.Aggregates.CheckingAccounts.Models;

using System;
using Enums.Transfers;
using Enums.Transfers.Controller;
using Helpers;
using Newtonsoft.Json;

public class GetFilteredTransfersFilter
{
    [JsonProperty("completionState")]
    public BankTransferCompletionState? CompletionState
    {
        get;
        set;
    }

    /// <summary>
    /// B9 specific type worth mentioning.
    /// </summary>
    [JsonProperty("specialType")]
    public B9SpecialBankTransferType? SpecialType
    {
        get;
        set;
    }

    /// <summary>
    /// Range from date to date to filter based on.
    /// </summary>
    [JsonProperty("timePeriod")]
    public TimePeriodFilter TimePeriod
    {
        get;
        set;
    } = new();

    /// <summary>
    /// Amount filter
    /// </summary>
    [JsonProperty("amount")]
    public AmountFilter Amount
    {
        get;
        set;
    } = new();

    /// <summary>
    /// Incoming, outgoing or null if both
    /// </summary>
    [JsonProperty("direction")]
    public TransferDirectionSimplified? Direction
    {
        get;
        set;
    }

    /// <summary>
    /// Debit, credit or null if both.</summary>
    [JsonIgnore]
    public TransferDirection? DirectionInternal => CurrencyFormattingHelper.ConvertDirection(Direction);

    /// <summary>
    /// Card or account or both.
    /// </summary>
    [JsonProperty("cardOrAccount")]
    public TransactionType? CardOrAccount
    {
        get;
        set;
    }

    /// <summary>
    /// Range of nullable $ amounts.
    /// </summary>
    public class AmountFilter
    {
        /// <summary>
        /// 0.00 if null.
        /// </summary>
        [JsonProperty("minAmount")]
        public decimal? MinAmount
        {
            get;
            set;
        }

        /// <summary>
        /// Infinity if null.
        /// </summary>
        [JsonProperty("maxAmount")]
        public decimal? MaxAmount
        {
            get;
            set;
        }
    }

    /// <summary>
    /// Range of nullable dates.
    /// </summary>
    public class TimePeriodFilter
    {
        /// <summary>
        /// Null means beginning of time.
        /// </summary>
        [JsonProperty("minDateUtc")]
        public DateTime? MinDateUtc
        {
            get;
            set;
        }

        /// <summary>
        /// Null means end of time.
        /// </summary>
        [JsonProperty("maxDateUtc")]
        public DateTime? MaxDateUtc
        {
            get;
            set;
        }
    }

    /// <summary>
    /// Card ID at Mbanq
    /// </summary>
    [JsonProperty("cardId")]
    public int? CardId
    {
        get;
        set;
    }
}
