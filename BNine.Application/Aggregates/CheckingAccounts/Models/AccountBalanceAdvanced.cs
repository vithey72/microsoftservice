﻿namespace BNine.Application.Aggregates.CheckingAccounts.Models;

using Enums;

public class AccountBalanceAdvanced : BalanceBase
{
    public AccountData AccountData
    {
        get;
        set;
    } = new AccountData();
}

public class AccountData
{

    public BankAccountTypeEnum AccountType
    {
        get;
        set;
    }

    public string Title
    {
        get;
        set;
    }

    public string Subtitle
    {
        get;
        set;
    }
}
