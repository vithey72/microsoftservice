﻿namespace BNine.Application.Aggregates.CheckingAccounts.Models;

public class GetFilteredTransfersPagingParameters
{
    public int StartPage
    {
        get;
        set;
    } = 1;

    public int PageSize
    {
        get;
        set;
    } = 30;
}
