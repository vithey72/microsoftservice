﻿namespace BNine.Application.Aggregates.CheckingAccounts.Models
{
    using Newtonsoft.Json;

    public abstract class BalanceBase
    {
        // TODO: think about enum
        [JsonProperty("currency")]
        public string Currency
        {
            get; set;
        }

        [JsonProperty("availableBalance")]
        public decimal AvailableBalance
        {
            get; set;
        }


    }
}
