﻿namespace BNine.Application.Aggregates.CheckingAccounts.Models
{
    using BNine.Enums.Transfers;
    using Newtonsoft.Json;

    public class SavingsTransaction
    {
        [JsonProperty("id")]
        public int Id
        {
            get; set;
        }

        [JsonProperty("type")]
        public string Type
        {
            get; set;
        }

        [JsonProperty("succeeded")]
        public bool Succeeded
        {
            get; set;
        }

        [JsonProperty("amount")]
        public decimal Amount
        {
            get; set;
        }

        [JsonProperty("date")]
        public DateTime Date
        {
            get; set;
        }

        [JsonProperty("direction")]
        public TransferDirection Direction
        {
            get; set;
        }

        [JsonProperty("runningBalance")]
        public decimal RunningBalance
        {
            get; set;
        }

        [JsonProperty("counterparty")]
        public string Counterparty
        {
            get; set;
        }

        [JsonProperty("recipientName")]
        public string RecipientName
        {
            get; set;
        }

        [JsonProperty("debtorName")]
        public string DebtorName
        {
            get; set;
        }

        [JsonProperty("address")]
        public string Address
        {
            get; set;
        }

        [JsonProperty("toRoutingNumber")]
        public string ToRoutingNumber
        {
            get; set;
        }

        [JsonProperty("to")]
        public string To
        {
            get; set;
        }

        [JsonProperty("from")]
        public string From
        {
            get; set;
        }

        [JsonProperty("note")]
        public string Note
        {
            get; set;
        }
    }
}
