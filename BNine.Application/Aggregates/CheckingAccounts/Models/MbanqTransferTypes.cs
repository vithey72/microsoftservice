﻿namespace BNine.Application.Aggregates.CheckingAccounts.Models;

public static class MbanqTransferTypes
{
#pragma warning disable IDE0055
    private static readonly Dictionary<int, string> Types = new()
    {
        { 0, "INVALID" },
        { 1, "DEPOSIT" },
        { 2, "WITHDRAWAL" },
        { 3, "INTEREST_POSTING" },
        { 4, "WITHDRAWAL_FEE" },
        { 5, "ANNUAL_FEE" },
        { 6, "WAIVE_CHARGES" },
        { 7, "PAY_CHARGE" },
        { 8, "DIVIDEND_PAYOUT" },
        { 12, "INITIATE_TRANSFER" },
        { 13, "APPROVE_TRANSFER" },
        { 14, "WITHDRAW_TRANSFER" },
        { 15, "REJECT_TRANSFER" },
        { 16, "WRITTEN_OFF" },
        { 17, "OVERDRAFT_INTEREST" },
        { 18, "WITHHOLD_TAX" },
        { 19, "ESCHEAT" },
        { 20, "AMOUNT_HOLD" },
        { 21, "AMOUNT_RELEASE" },
        { 22, "INTEREST_PAYABLE_ACCRUED" },
        { 23, "OVERDRAFT_INTEREST_RECEIVABLE_ACCRUED" },
        { 24, "PAY_CHARGE_REVERSAL"}
    };

    private static readonly Dictionary<int, string> SubTypes = new()
    {
        { 0, "NONE" },
        { 1, "CARD_TRANSACTION" },
        { 2, "SETTLEMENT_RETURN_CREDIT" },
        { 3, "LOAN_DISBURSEMENT" },
        { 4, "LOAN_REPAYMENT" },
        { 5, "CARD_AUTHORIZE_PAYMENT" },
        { 6, "DOMESTIC_ATM_WITHDRAWAL_FEE" },
        { 7, "INTERNATIONAL_ATM_WITHDRAWAL_FEE" },
        { 8, "INTERNATIONAL_TRANSACTION_FEE" },
        { 9, "EXTERNAL_CARD_PUSH_TRANSACTION_FEE" },
        { 10, "EXTERNAL_CARD_PULL_TRANSACTION_FEE" },
        { 11, "PROVISIONAL_CREDIT" },
        { 12, "PROVISIONAL_CREDIT_REVERSAL" },
        { 13, "MCC_CHARGE" },
        { 14, "TRANSFER_FEE" }
    };
#pragma warning restore IDE0055

    public static string GetTypeName(int? typeId)
    {
        return FindElementOrFirst(Types, typeId);
    }

    public static string GetSubtypeName(int? subtypeId)
    {
        return FindElementOrFirst(SubTypes, subtypeId);
    }

    private static string FindElementOrFirst(Dictionary<int, string> dictionary, int? typeId)
    {
        var kvPair = dictionary.FirstOrDefault(t => t.Key == typeId);
        if (string.IsNullOrEmpty(kvPair.Value))
        {
            kvPair = dictionary.First();
        }

        return kvPair.Value;
    }
}
