﻿namespace BNine.Application.Aggregates.CheckingAccounts.Models;

using BNine.Application.Aggregates.CommonModels.Buttons;

public class AccountBalanceAdvancedWithButton : AccountBalanceAdvanced
{
    public ButtonWithIconExtendedViewModel Button
    {
        get;
        set;
    }
}
