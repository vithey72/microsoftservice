﻿namespace BNine.Application.Aggregates.CheckingAccounts.Models;

using Enums.Transfers;
using Enums.Transfers.Controller;
using Newtonsoft.Json;

public class BankTransferViewModel
{
    [JsonProperty("id")]
    public int Id
    {
        get;
        set;
    }

    /// <summary>
    /// Mbanq type.
    /// </summary>
    [JsonProperty("rawType")]
    public int? RawType
    {
        get;
        set;
    }

    /// <summary>
    /// Mbanq type name.
    /// </summary>
    [JsonProperty("rawTypeName")]
    public string RawTypeName => MbanqTransferTypes.GetTypeName(RawType);

    /// <summary>
    /// Mbanq subtype.
    /// </summary>
    [JsonProperty("rawSubType")]
    public int? RawSubType
    {
        get;
        set;
    }

    /// <summary>
    /// Mbanq subtype.
    /// </summary>
    [JsonProperty("rawSubTypeName")]
    public string RawSubTypeName => MbanqTransferTypes.GetSubtypeName(RawSubType);

    [JsonProperty("title")]
    public string Title
    {
        get;
        set;
    }

    [JsonProperty("subtitle")]
    public string Subtitle
    {
        get;
        set;
    }

    [JsonProperty("amount")]
    public string Amount
    {
        get;
        set;
    }

    [JsonProperty("amountStringColor")]
    public string AmountStringColor
    {
        get;
        set;
    }

    [JsonProperty("direction")]
    public TransferDirectionSimplified? AmountDirection
    {
        get;
        set;
    }

    [JsonProperty("status")]
    public BankTransferCompletionState Status
    {
        get;
        set;
    }

    [JsonProperty("statusLine")]
    public string StatusLine
    {
        get;
        set;
    }

    [JsonProperty("specialStatus")]
    public B9SpecialBankTransferType? SpecialStatus
    {
        get;
        set;
    }

    /// <summary>
    /// Either a standard icon or a merchant specific icon.
    /// </summary>
    [JsonProperty("iconUrl")]
    public string IconUrl
    {
        get;
        set;
    }

    [JsonProperty("timeStampUtc")]
    public DateTime TimeStampUtc
    {
        get;
        set;
    }

    //TODO: We need to add an intermediary model that has all of the fields and which can be mapped onto this one.
    //Then add a new query and handler or something that is used both in new and old code.
    //Using only from TransactionAnalytics request, should be refactored
    [JsonIgnore]
    public int? MccCode
    {
        get;
        set;
    }

    [JsonIgnore]
    public TransferDisplayType SavingsTransferType
    {
        get;
        set;
    }

    [JsonIgnore]
    public PendingTransferDisplayType PendingTransferType
    {
        get;
        set;
    }

    /// <summary>
    /// For debug purposes.
    /// </summary>
    [JsonProperty("displayType")]
    public string DisplayType => SavingsTransferType != TransferDisplayType.Unknown
        ? SavingsTransferType.ToString()
        : PendingTransferType.ToString();

    [JsonIgnore]
    public decimal AmountSum
    {
        get;
        set;
    }

    public string HintText
    {
        get;
        set;
    }
}
