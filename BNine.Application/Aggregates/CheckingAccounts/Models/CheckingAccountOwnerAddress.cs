﻿namespace BNine.Application.Aggregates.CheckingAccounts.Models
{
    using Newtonsoft.Json;

    public class CheckingAccountOwnerAddress
    {
        [JsonProperty("ownerZipCode")]
        public string ZipCode
        {
            get; set;
        }

        [JsonProperty("ownerState")]
        public string State
        {
            get; set;
        }

        [JsonProperty("ownerCity")]
        public string City
        {

            get; set;
        }

        [JsonProperty("ownerAddress")]
        public string Address
        {
            get; set;
        }

        [JsonProperty("ownerUnit")]
        public string Unit
        {
            get; set;
        }
    }
}
