﻿namespace BNine.Application.Aggregates.CheckingAccounts.Models
{
    using Newtonsoft.Json;

    public class GetFilteredTransfersExpandedFilter : GetFilteredTransfersFilter
    {
        [JsonProperty("paymentTypeNames")]
        public List<string> PaymentTypeNames
        {
            get;
            set;
        }

        [JsonProperty("isOnlyWithdrawal")]
        public bool? IsOnlyWithdrawal
        {
            get;
            set;
        }

        [JsonIgnore]
        public List<int> NotIncludedSavingTransactionIds
        {
            get;
            set;
        }

        [JsonIgnore]
        public List<int> SavingTransactionIds
        {
            get;
            set;
        }

        [JsonProperty("showOnlyPotentialIncome")]
        public bool ShowOnlyPotentialIncome
        {
            get;
            set;
        }
    }
}
