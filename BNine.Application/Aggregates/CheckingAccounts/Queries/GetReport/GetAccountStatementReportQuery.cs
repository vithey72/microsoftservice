﻿namespace BNine.Application.Aggregates.CheckingAccounts.Queries.GetReport
{
    using System;
    using Abstractions;
    using MediatR;

    public class GetAccountStatementReportQuery : MBanqSelfServiceUserRequest, IRequest<Unit>
    {
        public DateTime? StartingDate
        {
            get; set;
        }

        public DateTime? EndingDate
        {
            get; set;
        }

        public string Email
        {
            get; set;
        }
    }
}
