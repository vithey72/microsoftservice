﻿namespace BNine.Application.Aggregates.CheckingAccounts.Queries.GetReport
{

    using System.Threading;
    using System.Threading.Tasks;
    using AutoMapper;
    using Emails.Commands.Documents.SpendingAccountStatement;
    using Exceptions;
    using Interfaces;
    using Interfaces.Bank.Client;
    using MediatR;
    using Microsoft.EntityFrameworkCore;
    using IBankSavingAccountsService = Interfaces.Bank.Administration.IBankSavingAccountsService;

    public class GetAccountStatementReportQueryHandler
        : AccountStatementBaseQueryHandler, IRequestHandler<GetAccountStatementReportQuery, Unit>
    {

        public GetAccountStatementReportQueryHandler(
            IMediator mediator,
            IBNineDbContext dbContext,
            IMapper mapper,
            ICurrentUserService currentUser,
            IBankSavingAccountsService bankSavingsAccountsService,
            IBankSavingsTransactionsService bankSavingsTransactionsService,
            IPartnerProviderService partnerProviderService
            )
            : base(mediator, dbContext, mapper, currentUser,
                bankSavingsTransactionsService,
                bankSavingsAccountsService,
                partnerProviderService
                )
        {

        }

        public async Task<Unit> Handle(GetAccountStatementReportQuery request, CancellationToken cancellationToken)
        {
            var user = await DbContext.Users
                .Include(x => x.CurrentAccount)
                .Include(x => x.CurrentAdditionalAccounts)
                .AsNoTracking()
                .FirstOrDefaultAsync(x => x.Id == CurrentUser.UserId, cancellationToken);

            if (user?.CurrentAccount == null)
            {
                throw new NotFoundException(nameof(user.CurrentAccount), CurrentUser.UserId);
            }

            var statementInput = await GetStatementInput(user, request.StartingDate, request.EndingDate);

            var attachment = GetPdfAttachment(user, statementInput);

            await Mediator.Send(new SpendingAccountStatementEmailCommand(request.Email, user.FirstName, attachment), cancellationToken);

            return Unit.Value;
        }
    }
}
