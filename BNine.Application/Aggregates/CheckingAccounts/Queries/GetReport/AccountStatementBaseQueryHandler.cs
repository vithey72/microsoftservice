﻿namespace BNine.Application.Aggregates.CheckingAccounts.Queries.GetReport
{
    using System.Globalization;
    using Abstractions;
    using Application.Models.Emails.EmailsWithTemplate;
    using Application.Models.MBanq;
    using AutoMapper;
    using Domain.Entities.User;
    using Enums;
    using Helpers;
    using Interfaces;
    using Interfaces.Bank.Client;
    using MediatR;
    using Models;
    using IBankSavingAccountsService = Interfaces.Bank.Administration.IBankSavingAccountsService;

    public class AccountStatementBaseQueryHandler
        : AbstractRequestHandler
    {
        private readonly IBankSavingsTransactionsService _bankSavingsTransactionsService;
        private readonly IBankSavingAccountsService _bankSavingsAccountsService;
        private readonly IPartnerProviderService _partnerProvider;

        protected AccountStatementBaseQueryHandler(
            IMediator mediator,
            IBNineDbContext dbContext,
            IMapper mapper,
            ICurrentUserService currentUser,
            IBankSavingsTransactionsService bankSavingsTransactionsService,
            IBankSavingAccountsService bankSavingsAccountsService,
            IPartnerProviderService partnerProviderService
            )
            : base(mediator, dbContext, mapper, currentUser)
        {
            _bankSavingsTransactionsService = bankSavingsTransactionsService;
            _bankSavingsAccountsService = bankSavingsAccountsService;
            _partnerProvider = partnerProviderService;
        }

        protected async Task<StatementPdfGenerator.StatementInput> GetStatementInput(User user,
            DateTime? startingDate, DateTime? endingDate)
        {
            ValidateUser(user);
            var (reportMonthStartingDate, reportMonthEndingDate) = GetMonthDates(startingDate, endingDate);
            var rewardAccount = user.CurrentAdditionalAccounts.FirstOrDefault(x =>
                x.AccountType == CurrentAdditionalAccountEnum.Reward);

            var mainReportTransactions = await GetAccountTransactions(user.CurrentAccount.ExternalId,
                reportMonthStartingDate, reportMonthEndingDate);

            var rewardReportTransactions = await GetAccountTransactions(rewardAccount?.ExternalId,
                reportMonthStartingDate, reportMonthEndingDate);

            var accountInfo = await _bankSavingsAccountsService
                .GetSavingAccountInfo(user.CurrentAccount.ExternalId);

            var mainAccountEmptyListCaseBalance = await GetEmptyListCaseBalance(mainReportTransactions,
                user.CurrentAccount.ExternalId, reportMonthStartingDate);
            var rewardAccountEmptyListCaseBalance = await GetEmptyListCaseBalance(rewardReportTransactions,
                rewardAccount?.ExternalId, reportMonthStartingDate);

            return new StatementPdfGenerator.StatementInput()
            {
                UserName = user.FirstName + " " + user.LastName,
                Address = $"{user.Address.AddressLine}, {user.Address.City}, {user.Address.State} {user.Address.ZipCode}",
                AccountNumber = accountInfo.AccountNumber,
                StartingDate = reportMonthStartingDate,
                EndingDate = reportMonthEndingDate,
                MainAccountCalculusItems = new StatementPdfGenerator.AccountCalculusItems()
                {
                    AccountTransactions = mainReportTransactions,
                    AccountEmptyListCaseBalance= mainAccountEmptyListCaseBalance
                },
                RewardAccountCalculusItems = new StatementPdfGenerator.AccountCalculusItems()
                {
                    AccountTransactions = rewardReportTransactions,
                    AccountEmptyListCaseBalance = rewardAccountEmptyListCaseBalance
                },
                IsRewardAccountPresent = rewardAccount != null
            };
        }

        protected EmailAttachmentData GetPdfAttachment(User user, StatementPdfGenerator.StatementInput input)
        {
            var pdfReportName = GetPdfName(user, input.StartingDate);
            var pdfReport = StatementPdfGenerator.GenerateAccountStatementPdfReport(input,
                _partnerProvider);

            var attachment = new EmailAttachmentData($"{pdfReportName}.pdf", pdfReport);
            return attachment;
        }

        private (DateTime monthStartingDate, DateTime monthEndingDate) GetMonthDates(DateTime? startingDate, DateTime? endingDate)
        {
            var date = DateTime.UtcNow.AddMonths(-1);

            var reportMonthStartingDate = new DateTime(date.Year, date.Month, 1).Date;

            var reportMonthEndingDate = reportMonthStartingDate.Date.AddMonths(1).AddSeconds(-1);

            if (startingDate != null)
            {
                reportMonthStartingDate = startingDate.Value;
            }

            if (endingDate != null)
            {
                reportMonthEndingDate = endingDate.Value;
            }

            return (reportMonthStartingDate, reportMonthEndingDate);
        }

        private string GetPdfName(User user, DateTime reportMonthStartingDate)
        {
            var partnerName = StringProvider.GetPartnerName(_partnerProvider);
            return $"{user.FirstName}_{user.LastName}_" +
                   $"{reportMonthStartingDate.ToString("MMMM", new CultureInfo("en-US"))}_" +
                   $"{reportMonthStartingDate.Year}_{partnerName}Statement";
        }

        private static TransactionModel ToTransactionModel(AccountStatementTransactionItem x)
        {
            return new TransactionModel
            {
                Id = x.Id,
                Type = x.Type,
                Amount = x.Amount,
                PostingDate = x.InitiatedAt,
                TransactionDate = x.CreatedAt,
                Direction = x.Direction,
                RunningBalance = x.RunningBalance,
                Reference = x.Reference,
                CreditorName = x.Creditor?.Name,
                DebtorName = x.Debtor?.Name
            };
        }


        private async Task<decimal> GetEmptyListCaseBalance(List<TransactionModel> reportTransactions,
            int? accountExternalId, DateTime reportMonthStartingDate)
        {
            var emptyListCaseBalance = 0M;
            if(accountExternalId == null)
            {
                return emptyListCaseBalance;
            }
            if (reportTransactions.Count == 0)
            {
                emptyListCaseBalance = await GetEmptyListCaseBalance(null, accountExternalId.Value, reportMonthStartingDate);
            }

            return emptyListCaseBalance;
        }

        private async Task<decimal> GetEmptyListCaseBalance(string ip, int accountExternalId, DateTime reportMonthStartingDate)
        {
            var closestTransaction = (await _bankSavingsTransactionsService
                    .GetAccountStatementTransactions(
                        accountExternalId, 1, 10000,
                        DateTime.MinValue, reportMonthStartingDate,
                        CurrentUser.UserMbanqToken, ip,
                        asAdmin: true))
                .Items
                .FirstOrDefault();
            return closestTransaction?.RunningBalance ?? 0M;
        }

        private async Task<List<TransactionModel>> GetAccountTransactions(int? accountId,
            DateTime reportMonthStartingDate, DateTime reportMonthEndingDate)
        {

            if(accountId == null)
            {
                return new List<TransactionModel>();
            }
            var transactions = await _bankSavingsTransactionsService
                .GetAccountStatementTransactions(
                    accountId.Value, 1, 10000,
                    reportMonthStartingDate, reportMonthEndingDate,
                    null, null, asAdmin: true);

            var reportTransactions = transactions.Items
                .Select(ToTransactionModel)
                .ToList();

            return reportTransactions;
        }

        private void ValidateUser(User user)
        {
            if (user.CurrentAdditionalAccounts == null)
            {
                throw new ArgumentException($"{nameof(user.CurrentAdditionalAccounts)} is not provided for the passed down user");
            }
        }
    }
}
