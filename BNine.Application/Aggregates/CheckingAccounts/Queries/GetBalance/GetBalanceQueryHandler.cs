﻿namespace BNine.Application.Aggregates.CheckingAccounts.Queries.GetBalance
{
    using System.Linq;
    using System.Threading;
    using System.Threading.Tasks;
    using Abstractions;
    using AutoMapper;
    using Exceptions;
    using Interfaces;
    using Interfaces.Bank.Client;
    using MediatR;
    using Microsoft.EntityFrameworkCore;
    using Models;

    public class GetBalanceQueryHandler
        : AbstractRequestHandler, IRequestHandler<GetBalanceQuery, AccountBalance>
    {
        private IBankSavingAccountsService BankAccountService
        {
            get;
        }

        public GetBalanceQueryHandler(
            IMediator mediator,
            IBNineDbContext dbContext,
            IMapper mapper,
            ICurrentUserService currentUser,
            IBankSavingAccountsService bankAccountService) : base(mediator, dbContext, mapper, currentUser)
        {
            BankAccountService = bankAccountService;
        }

        public async Task<AccountBalance> Handle(GetBalanceQuery request, CancellationToken cancellationToken)
        {
            var account = await DbContext.Users
                .Include(x => x.CurrentAccount)
                .Where(x => x.Id == CurrentUser.UserId)
                .Select(x => new { x.CurrentAccount.ExternalId })
                .FirstOrDefaultAsync(cancellationToken);

            if (account == null)
            {
                throw new NotFoundException();
            }

            var info = await BankAccountService.GetSavingAccountInfo(account.ExternalId, CurrentUser.UserMbanqToken, request.Ip);

            return new AccountBalance()
            {
                Currency = info.Currency,
                AvailableBalance = info.AvailableBalance
            };
        }
    }
}
