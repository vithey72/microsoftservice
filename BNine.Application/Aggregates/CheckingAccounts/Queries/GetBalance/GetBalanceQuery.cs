﻿namespace BNine.Application.Aggregates.CheckingAccounts.Queries.GetBalance
{
    using MediatR;
    using Models;
    using Newtonsoft.Json;

    public class GetBalanceQuery
        : IRequest<AccountBalance>
    {
        [JsonIgnore]
        public string Ip
        {
            get; set;
        }
    }
}
