﻿namespace BNine.Application.Aggregates.CheckingAccounts.Queries.GetBalance.Models;

using Enums;

public class GetBalanceAdvancedRequest
{
    public List<BankAccountTypeEnum> AccountsToGet
    {
        get;
        set;
    }

}
