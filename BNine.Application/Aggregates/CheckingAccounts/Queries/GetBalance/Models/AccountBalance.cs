﻿namespace BNine.Application.Aggregates.CheckingAccounts.Queries.GetBalance.Models;

using CheckingAccounts.Models;

[Obsolete("Will be deprecated after client force upgrade from 2.44.0")]
public class AccountBalance : BalanceBase
{
}
