﻿#nullable enable
namespace BNine.Application.Aggregates.CheckingAccounts.Queries.GetBalance;

using CheckingAccounts.Models;
using Common;
using Configuration.Queries.GetConfiguration;
using Constants;
using Enums;
using Exceptions;
using Extensions;
using Interfaces;
using Interfaces.Bank.Client;
using Mappings;
using MediatR;
using Microsoft.EntityFrameworkCore;

public class GetBalanceAdvancedQueryHandler : IRequestHandler<GetBalanceAdvancedQuery, List<AccountBalanceAdvanced>>
{
    private readonly IBNineDbContext _dbContext;
    private readonly IBankSavingAccountsService _bankAccountService;
    private readonly IMediator _mediator;

    public GetBalanceAdvancedQueryHandler(IBNineDbContext dbContext,
        IBankSavingAccountsService bankAccountService,
        IMediator mediator)
    {
        _dbContext = dbContext;
        _bankAccountService = bankAccountService;
        _mediator = mediator;
    }

    public async Task<List<AccountBalanceAdvanced>> Handle(GetBalanceAdvancedQuery query,
        CancellationToken cancellationToken)
    {
        var userAccounts = await _dbContext.Users
            .Include(x => x.CurrentAccount)
            .Include(x => x.CurrentAdditionalAccounts)
            .Where(x => x.Id == query.UserId)
            .Select(x => new { MainAccExternalId = x.CurrentAccount.ExternalId, x.CurrentAdditionalAccounts })
            .FirstOrDefaultAsync(cancellationToken);


        if (userAccounts == null)
        {
            throw new NotFoundException($"User with Id {query.UserId} was not found");
        }

        var configuration = await _mediator.Send(new GetConfigurationQuery(), cancellationToken);
        var areAdditionalAccountsRequired =
            query.Request.AccountsToGet.Any(x => x != BankAccountTypeEnum.Main) &&
            configuration.HasFeatureEnabled(FeaturesNames.Features.CurrentAdditionalAccountsInternal);

        var result = new List<AccountBalanceAdvanced>();

        if (query.Request.AccountsToGet.Contains(BankAccountTypeEnum.Main))
        {
            var mainAccount = userAccounts.MainAccExternalId;
            var mainAccInfo = await _bankAccountService.GetSavingAccountInfoSafe(mainAccount,
                query.MbanqToken, query.Ip);

            if (mainAccInfo != null)
            {
                result.Add(new AccountBalanceAdvanced()
                {
                    Currency = mainAccInfo.Currency,
                    AvailableBalance = mainAccInfo.AvailableBalance,
                    AccountData = new AccountData()
                    {
                        AccountType = BankAccountTypeEnum.Main,
                        Title = string.Concat(BankAccountTypeEnum.Main.GetEnumDescriptionValue(),
                            SecretNumberHelper.GetLast4DigitsWithPrefix(mainAccInfo.AccountNumber, " ....")),
                        Subtitle = "Available balance"
                    }
                });
            }
        }

        if (areAdditionalAccountsRequired)
        {
            var requestedAdditionalAccounts =
                query.Request.AccountsToGet.ToCurrentAdditionalAccountEnumList();

            var additionalAccountsToReturn =
                userAccounts.CurrentAdditionalAccounts
                    .Where(ca => requestedAdditionalAccounts.Contains(ca.AccountType))
                    .Select(x =>
                        (Id: x.ExternalId, Type: x.AccountType)).ToList();

            foreach (var (id, type) in additionalAccountsToReturn)
            {
                var info = await _bankAccountService.GetSavingAccountInfoSafe(id, query.MbanqToken, query.Ip);

                if (info == null)
                {
                    continue;
                }

                var accountType = type.ToBankAccountTypeEnum();

                result.Add(new AccountBalanceAdvanced()
                {
                    Currency = info.Currency,
                    AvailableBalance = info.AvailableBalance,
                    AccountData = new AccountData()
                    {
                        AccountType = accountType,
                        Title = accountType.GetEnumDescriptionValue(),
                        Subtitle = "Available balance"
                    }
                });

            }
        }

        return result;
    }

}
