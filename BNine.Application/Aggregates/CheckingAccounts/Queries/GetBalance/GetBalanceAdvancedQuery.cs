﻿namespace BNine.Application.Aggregates.CheckingAccounts.Queries.GetBalance;

using CheckingAccounts.Models;
using MediatR;
using Models;


public record GetBalanceAdvancedQuery(GetBalanceAdvancedRequest Request, string Ip, Guid UserId, string MbanqToken)
    : IRequest<List<AccountBalanceAdvanced>>;

