﻿namespace BNine.Application.Aggregates.CheckingAccounts.Queries.GetFilteredTransfers;
using Abstractions;
using Application.Models;
using Application.Models.MBanq;
using Application.Models.MBanq.Transfers;
using AutoMapper;
using Common;
using Constants;
using Enums;
using Enums.Transfers;
using Enums.Transfers.Controller;
using Extensions;
using Helpers;
using Interfaces;
using Interfaces.Bank.Client;
using Interfaces.TransactionHistory;
using LoanSettings.Queries.GetACHPrincipalKeywords;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Models;
using Services;

public class GetFilteredTransfersQueryHandler
    : AbstractRequestHandler,
      IRequestHandler<GetFilteredTransfersQuery, PaginatedFilteredList<BankTransferViewModel, GetFilteredTransfersFilter>>
{
    private const string GrayAmountStringColor = "808080";

    private readonly IBankSavingsTransactionsService _bankSavingsTransactionsService;
    private readonly IBankClientExternalCardsService _externalCardsService;
    private readonly ITransfersMappingService _transfersMappingService;
    private readonly IBankSavingAccountsService _accountsService;
    private readonly ICardsAndAccountsDigitsProviderService _cardsAndAccountsDigitsProviderService;
    private readonly IPartnerProviderService _partnerProvider;

    public GetFilteredTransfersQueryHandler(
        IMediator mediator,
        IBNineDbContext dbContext,
        IMapper mapper,
        ICurrentUserService currentUser,
        IBankSavingsTransactionsService bankSavingsTransactionsService,
        IBankClientExternalCardsService externalCardsService,
        ITransfersMappingService transfersMappingService,
        IBankSavingAccountsService accountsService,
        ICardsAndAccountsDigitsProviderService cardsAndAccountsDigitsProviderService,
        IPartnerProviderService partnerProvider
    )

        : base(mediator, dbContext, mapper, currentUser)
    {
        _bankSavingsTransactionsService = bankSavingsTransactionsService;
        _externalCardsService = externalCardsService;
        _transfersMappingService = transfersMappingService;
        _accountsService = accountsService;
        _cardsAndAccountsDigitsProviderService = cardsAndAccountsDigitsProviderService;
        _partnerProvider = partnerProvider;
    }

    public async Task<PaginatedFilteredList<BankTransferViewModel, GetFilteredTransfersFilter>> Handle(GetFilteredTransfersQuery request, CancellationToken cancellationToken)
    {
        // for same paging behavior as legacy
        request.Paging.StartPage += 1;

        await FillRequestParameters(request);

        var accountAndCardDetails = await _cardsAndAccountsDigitsProviderService.GetAccountAndCardDetails(request, cancellationToken);

        var baseModelsList = new List<TransactionItemBase>();
        var resultingViewModels = new List<BankTransferViewModel>();
        var totalCount = 0L;

        // MBanq operates in EST dates
        var startDate = request.Filter?.TimePeriod?.MinDateUtc == null
            ? (DateTime?)null
            : TimeZoneHelper.UtcToEasternStandartTime(request.Filter.TimePeriod.MinDateUtc!.Value);
        var endDate = request.Filter?.TimePeriod?.MaxDateUtc == null
            ? (DateTime?)null
            : TimeZoneHelper.UtcToEasternStandartTime(request.Filter.TimePeriod.MaxDateUtc!.Value);

        switch (request.Filter?.CompletionState)
        {
            case null:
                {
                    var savingTransactions = await GetSavingsTransactions(request, accountAndCardDetails.AccountExternalId, startTime: startDate, endTime: endDate);
                    baseModelsList.AddRange(savingTransactions.Items);
                    var externalCards = (await FetchCards(savingTransactions.Items, request.Ip))
                        .ToDictionary(x => x.ExternalId, x => x.Last4Digits);
                    var senderAccounts = (await FetchAccounts(savingTransactions.Items))
                        .ToDictionary(x => x.Key, y => y.Value);

                    var (minConcatDate, maxConcatDate) = GetDatesForConcatenation(savingTransactions, startDate, endDate, request.Paging.StartPage == 1);
                    var pendingTransactions = await GetPendingTransactions(
                        request, accountAndCardDetails.AccountExternalId, minDate: startDate, maxDate: endDate, overrideCount: 1000, overridePage: 1);
                    pendingTransactions = RemoveTransfersOutOfPageRange(pendingTransactions, minConcatDate, maxConcatDate);
                    baseModelsList.AddRange(pendingTransactions.Items);

                    var rejectedTransactions = await GetRejectedTransactions(
                        request, accountAndCardDetails.AccountExternalId, minDate: startDate, maxDate: endDate, overrideCount: 1000, overridePage: 1);
                    rejectedTransactions = RemoveTransfersOutOfPageRange(rejectedTransactions, minConcatDate, maxConcatDate);
                    baseModelsList.AddRange(rejectedTransactions.Items);

                    resultingViewModels = await BaseModelsToViewModels(request, baseModelsList, accountAndCardDetails.CardLastDigitsText, accountAndCardDetails.AccountLastDigitsText, externalCards, senderAccounts);
                    totalCount = savingTransactions.TotalCount + pendingTransactions.TotalCount + rejectedTransactions.TotalCount;
                    break;
                }
            case BankTransferCompletionState.Completed:
            case BankTransferCompletionState.Returned:
                {
                    var savingTransactions = await GetSavingsTransactions(request, accountAndCardDetails.AccountExternalId, startTime: startDate, endTime: endDate);
                    var externalCards = (await FetchCards(savingTransactions.Items, request.Ip))
                        .ToDictionary(x => x.ExternalId, x => x.Last4Digits);
                    var senderAccounts = (await FetchAccounts(savingTransactions.Items))
                        .ToDictionary(x => x.Key, y => y.Value);
                    baseModelsList.AddRange(savingTransactions.Items);
                    resultingViewModels = await BaseModelsToViewModels(request, baseModelsList, accountAndCardDetails.CardLastDigitsText, accountAndCardDetails.AccountLastDigitsText, externalCards, senderAccounts);
                    totalCount = savingTransactions.TotalCount;
                    break;
                }
            case BankTransferCompletionState.Pending:
                {
                    var pendingTransactions = await GetPendingTransactions(request, accountAndCardDetails.AccountExternalId, minDate: startDate, maxDate: endDate);
                    baseModelsList.AddRange(pendingTransactions.Items);
                    resultingViewModels = await BaseModelsToViewModels(request, baseModelsList, accountAndCardDetails.CardLastDigitsText, accountAndCardDetails.AccountLastDigitsText);
                    totalCount = pendingTransactions.TotalCount;
                    break;
                }
            case BankTransferCompletionState.Declined:
                {
                    var rejectedTransactions = await GetRejectedTransactions(request, accountAndCardDetails.AccountExternalId, minDate: startDate, maxDate: endDate);
                    baseModelsList.AddRange(rejectedTransactions.Items);
                    resultingViewModels = await BaseModelsToViewModels(request, baseModelsList, accountAndCardDetails.CardLastDigitsText, accountAndCardDetails.AccountLastDigitsText);
                    totalCount = rejectedTransactions.TotalCount;
                    break;
                }
            default:
                throw new NotImplementedException("Unexpected completion state in Filter: " + request.Filter?.CompletionState);
        }

        var orderedTransactionVMs = resultingViewModels
            .OrderByDescending(vm => vm.TimeStampUtc)
            .ToArray();

        return new PaginatedFilteredList<BankTransferViewModel, GetFilteredTransfersFilter>
        (
            orderedTransactionVMs,
            request.Filter,
            totalCount,
            request.Paging.StartPage,
            request.Paging.PageSize
        );
    }

    private async Task FillRequestParameters(GetFilteredTransfersQuery request)
    {
        if (request.Filter?.ShowOnlyPotentialIncome ?? false)
        {
            await ExcludeAlreadyMarkedTransfers(request.Filter);
            await ExcludeRecognizedPayrollTransfers(request.Filter);
        }

        if (request.Filter?.SpecialType == B9SpecialBankTransferType.Payroll)
        {
            var payrollAchKeywords = await Mediator.Send(new GetACHPrincipalKeywordsQuery());

            var transactionIds =
                await DbContext.ACHCreditTransfers.GetPayrollExternalTransactionIds(CurrentUser.UserId.Value,
                    payrollAchKeywords.ToArray());

            request.Filter.SavingTransactionIds = transactionIds;
        }
    }

    private async Task<List<BankTransferViewModel>> BaseModelsToViewModels(GetFilteredTransfersQuery request,
        List<TransactionItemBase> baseModelsList,
        string cardLastDigitsText,
        string accountLastDigitsText,
        Dictionary<long, string> externalCards = default, Dictionary<int, string> accountNumbers = default)
    {
        var models = new List<BankTransferViewModel>();

        foreach (var baseModel in baseModelsList)
        {
            models.Add(await ToViewModel(
                baseModel, cardLastDigitsText, accountLastDigitsText,
                baseModel.CardDigits, externalCards, accountNumbers));
        }

        return models;
    }

    private PaginatedList<T> RemoveTransfersOutOfPageRange<T>(PaginatedList<T> pendingTransactions, DateTime minConcatDate, DateTime maxConcatDate)
        where T : TransactionItemBase
    {
        pendingTransactions.Items = pendingTransactions.Items
            .Where(t => t.CreatedAt < maxConcatDate && t.CreatedAt > minConcatDate)
            .ToList();
        return pendingTransactions;
    }



    private async Task<PaginatedList<SavingsTransactionItem>> GetSavingsTransactions(GetFilteredTransfersQuery request,
        int accountExternalId,
        DateTime? startTime = null, DateTime? endTime = null)
    {
        return await _bankSavingsTransactionsService.GetTransactions(
             accountExternalId,
             request.Paging.StartPage, request.Paging.PageSize,
             startTime, endTime,
             request.Filter.CardOrAccount == null ? null : new[] { request.Filter.CardOrAccount.Value },
             request.MbanqAccessToken, request.Ip,
             request.Filter);
    }

    private async Task<PaginatedList<SavingsTransactionItem>> GetRejectedTransactions(GetFilteredTransfersQuery request,
        int accountExternalId,
        DateTime? minDate = null, DateTime? maxDate = null, int? overrideCount = null, int? overridePage = null)
    {
        return await _bankSavingsTransactionsService.GetRejectedTransactions(
            accountExternalId,
            overridePage ?? request.Paging.StartPage, overrideCount ?? request.Paging.PageSize,
            minDate, maxDate,
            request.MbanqAccessToken, request.Ip,
            request.Filter);
    }

    private async Task<PaginatedList<PendingTransactionItem>> GetPendingTransactions(GetFilteredTransfersQuery request,
        int accountExternalId,
        DateTime? minDate = null, DateTime? maxDate = null, int? overrideCount = null, int? overridePage = null)
    {
        return await _bankSavingsTransactionsService.GetPendingTransactions(
            accountExternalId,
            request.MbanqAccessToken,
            overridePage ?? request.Paging.StartPage, overrideCount ?? request.Paging.PageSize,
            minDate, maxDate,
            null, null,
            request.Ip,
            request.Filter);
    }

    private static (DateTime minDate, DateTime maxDate) GetDatesForConcatenation(PaginatedList<SavingsTransactionItem> transactions,
        DateTime? startDate, DateTime? endDate, bool firstPage)
    {
        DateTime minDate = DateTime.MinValue;
        DateTime maxDate = DateTime.MaxValue;
        if (transactions.Items.Count > 0)
        {
            minDate = transactions.Items.Min(tr => tr.CreatedAt);
            maxDate = transactions.Items.Max(tr => tr.CreatedAt);
            if (minDate == maxDate)
            {
                minDate = DateTime.MinValue;
            }

            if (firstPage)
            {
                maxDate = DateTime.MaxValue;
            }
        }

        if (startDate != null && startDate > minDate)
        {
            minDate = startDate.Value;
        }

        if (endDate != null && endDate < maxDate)
        {
            maxDate = endDate.Value;
        }

        return (minDate, maxDate);
    }

    private async Task<BankTransferViewModel> ToViewModel(TransactionItemBase transactionItem, string requestCardLastDigitsText, string accountLastDigitsText,
        string transactionCardLastDigitsMbanqText,
        Dictionary<long, string> externalCards = default, Dictionary<int, string> accountNumbers = default)
    {
        var cardLastDigitsText = $"{StringProvider.GetPartnerName(_partnerProvider)} Card *";
        if (!string.IsNullOrEmpty(transactionCardLastDigitsMbanqText))
        {
            //MBanq card digits contains own text in addition to last 4 card digits
            cardLastDigitsText += transactionCardLastDigitsMbanqText.Substring(transactionCardLastDigitsMbanqText.Length - 4);
        }
        else
        {
            cardLastDigitsText = requestCardLastDigitsText;
        }

        if (transactionItem is SavingsTransactionItem transaction)
        {
            var displayType = transaction.TransferDisplayType;
            var savingsTransactionVm = BuildBaseModel(transaction, displayType);

            savingsTransactionVm.SpecialStatus = InferSpecialStatus(displayType);
            var merchant = _transfersMappingService.InferMerchant(transaction.Note);

            savingsTransactionVm.IconUrl = !string.IsNullOrEmpty(merchant?.Name)
                ? merchant.PictureUrl
                : await _transfersMappingService.InferIconUrl(displayType.InferIconOption(), transaction.MccCode, transaction.Counterparty);

            (savingsTransactionVm.Title, savingsTransactionVm.Subtitle) =
                InferTitles(displayType, transaction, cardLastDigitsText, accountLastDigitsText, externalCards, accountNumbers);

            var feePeriodicDisplayName = _partnerProvider.GetPartner() != PartnerApp.Qorbis
                ? "Monthly Card and Account Fee"
                : "Annual Membership Fee";

            savingsTransactionVm.HintText = displayType == TransferDisplayType.MonthlyB9Fee
                ? TransfersMappingHelper.GetMonthlyFeeTextWithPeriod(transaction.Reference, isDetailsScreenFormat: false, feePeriodicDisplayName, _partnerProvider.GetPartner() == PartnerApp.Qorbis ? 12 : 1)
                : null;

            savingsTransactionVm.SavingsTransferType = displayType;

            return savingsTransactionVm;
        }
        if (transactionItem is PendingTransactionItem pendingTransaction)
        {
            var pendingTransactionVm = new BankTransferViewModel
            {
                Id = pendingTransaction.Id,
                Amount = CurrencyFormattingHelper.AsRegular(Math.Abs(pendingTransaction.Amount)),
                TimeStampUtc = pendingTransaction.CreatedAt,
                Status = BankTransferCompletionState.Pending,
                StatusLine = "Processing",
                AmountDirection = TransferDirectionSimplified.Outgoing,
                MccCode = pendingTransaction.MccCode,
                AmountSum = pendingTransaction.Amount,
                PendingTransferType = pendingTransaction.PaymentType,
                HintText = "Why is this Processing?\nWe're still waiting for the final amount to come through"
            };

            if (pendingTransaction.Type == TransactionTypes.ATM)
            {
                pendingTransactionVm.IconUrl = await _transfersMappingService.InferIconUrl(BankTransferIconOption.Atm, pendingTransaction.MccCode);
                pendingTransactionVm.Title = "ATM";
                pendingTransactionVm.Subtitle = cardLastDigitsText;
                return pendingTransactionVm;
            }
            else
            {
                var merchant = _transfersMappingService.InferMerchant(pendingTransaction.Merchant);
                if (!string.IsNullOrEmpty(merchant?.Name))
                {
                    pendingTransactionVm.IconUrl = merchant.PictureUrl;
                    pendingTransactionVm.Title = merchant.Name;
                }
                else
                {
                    pendingTransactionVm.IconUrl = await _transfersMappingService.InferIconUrl(BankTransferIconOption.DefaultCardOk, pendingTransaction.MccCode);
                    pendingTransactionVm.Title = TransfersMappingHelper.ShortenMerchant(pendingTransaction.Merchant);
                }

                pendingTransactionVm.Subtitle = cardLastDigitsText;
                return pendingTransactionVm;
            }
        }

        throw new NotImplementedException($"Unknown type of transaction: {transactionItem.GetType()}");
    }

    private BankTransferViewModel BuildBaseModel(SavingsTransactionItem transaction, TransferDisplayType displayType)
    {
        var (status, stateLine) = InferStatusLine(transaction, displayType);

        var amount = CurrencyFormattingHelper.AsRegular(transaction.Amount);

        if (transaction.Direction == TransferDirection.Credit)
        {
            amount = CurrencyFormattingHelper.AsRegularWithSign(transaction.Amount, transaction.Direction);
        }

        var amountDirection = CurrencyFormattingHelper.ConvertDirection(transaction.Direction);

        var result = new BankTransferViewModel
        {
            Id = transaction.Id,
            Amount = amount,
            AmountStringColor = transaction.Type == TransactionTypes.REJECTED
                ? GrayAmountStringColor
                : TransfersMappingHelper.GetTransactionHistoryAmountStringColor(amountDirection),
            AmountDirection = amountDirection,
            RawType = transaction.RawType,
            RawSubType = transaction.RawSubType,
            TimeStampUtc = transaction.InitiatedAt,
            Status = status,
            MccCode = transaction.MccCode,
            SavingsTransferType = displayType,
            AmountSum = transaction.Amount
        };

        return result;
    }

    private B9SpecialBankTransferType? InferSpecialStatus(TransferDisplayType displayType)
    {
        return displayType switch
        {
            TransferDisplayType.MonthlyB9Fee => B9SpecialBankTransferType.Charge,
            TransferDisplayType.AdvanceIssued or TransferDisplayType.AdvanceRepayment => B9SpecialBankTransferType.Advance,
            TransferDisplayType.B9Cashback => B9SpecialBankTransferType.Cashback,
            _ => null
        };
    }

    private (BankTransferCompletionState state, string stateLine) InferStatusLine(SavingsTransactionItem transaction, TransferDisplayType displayType)
    {
        if (displayType is TransferDisplayType.Returned or TransferDisplayType.ReturnedPaycharge)
        {
            return (BankTransferCompletionState.Returned, nameof(BankTransferCompletionState.Returned));
        }
        if (transaction.Type == TransactionTypes.REJECTED)
        {
            return (BankTransferCompletionState.Declined, nameof(BankTransferCompletionState.Declined));
        }

        if (displayType is TransferDisplayType.MonthlyB9Fee or TransferDisplayType.AtmFee or
            TransferDisplayType.ForeignTransactionFee or TransferDisplayType.CardTransferFee or
            TransferDisplayType.ExternalWalletFee or TransferDisplayType.VipSupportFee or
            TransferDisplayType.AdvanceUnfreezeFee or TransferDisplayType.PhysicalCardDeliveryFee or
            TransferDisplayType.AdvanceBoostFee or TransferDisplayType.AfterAdvanceTip)
        {
            return (BankTransferCompletionState.Completed, $"Charged by {StringProvider.GetPartnerName(_partnerProvider)}");
        }

        return (BankTransferCompletionState.Completed, nameof(BankTransferCompletionState.Completed));
    }

    private (string Title, string Subtitle) InferTitles(TransferDisplayType displayType, SavingsTransactionItem transaction,
        string cardLastDigitsText, string accountLastDigitsText, Dictionary<long, string> externalCards, Dictionary<int, string> accountNumbers)
    {
        return displayType switch
        {
            TransferDisplayType.Atm => ("ATM", cardLastDigitsText),

            TransferDisplayType.AdvanceIssued => ("Advance Issued", accountLastDigitsText),
            TransferDisplayType.ReceivedTransferToCard => (transaction.Note, cardLastDigitsText),
            TransferDisplayType.CheckCashing => ("Replenishment by check", accountLastDigitsText),
            TransferDisplayType.AchReceived => (Coalesce(transaction.Debtor?.Name, "ACH Transfer"), $"Account *{GetAchSenderAccountDigits(transaction.Debtor)}"),
            TransferDisplayType.ReceivedFromB9Client => (transaction.Debtor?.Name, ExtractSenderAccountNumber(accountNumbers, transaction.Counterparty)),
            TransferDisplayType.PullFromExternalCard => ("Transfer From Card", ExtractExternalCardNumber(externalCards, transaction.Debtor?.Identifier)),
            TransferDisplayType.Returned => (_transfersMappingService.InferMerchant(transaction.Note).Name ?? TransfersMappingHelper.ShortenMerchant(transaction.Note), cardLastDigitsText),
            TransferDisplayType.DefaultDeposit => ("Deposit", accountLastDigitsText),

            TransferDisplayType.ApprovedCardAuth => (_transfersMappingService.InferMerchant(transaction.Note).Name ?? TransfersMappingHelper.ShortenMerchant(transaction.Note), cardLastDigitsText),
            TransferDisplayType.AdvanceRepayment => ("Advance Repayment", accountLastDigitsText),
            TransferDisplayType.AchSent => (Coalesce(transaction.Creditor?.Name, "ACH Transfer"), SecretNumberHelper.GetLast4DigitsWithPrefix(_transfersMappingService.ExtractAchRecipientNumbers(transaction).Account, "Account *")),
            TransferDisplayType.SentToB9Client => (transaction.Creditor?.Name, accountLastDigitsText),
            TransferDisplayType.Rewards => ("B9 Rewards", accountLastDigitsText),
            TransferDisplayType.PushToExternalCard => ("Transfer To Card", ExtractExternalCardNumber(externalCards, transaction.Creditor?.Identifier)),
            TransferDisplayType.DefaultWithdrawal => ("Withdrawal", accountLastDigitsText),

            TransferDisplayType.MonthlyB9Fee => (_partnerProvider.GetPartner() is PartnerApp.Qorbis ? "Annual Membership Fee" : "Monthly Fee", accountLastDigitsText),
            TransferDisplayType.AnnualFee => ("Annual Membership Fee", accountLastDigitsText),
            TransferDisplayType.VipSupportFee => ("Premium Support Fee", accountLastDigitsText),
            TransferDisplayType.AdvanceUnfreezeFee => ("Advance Unfreeze Fee", accountLastDigitsText),
            TransferDisplayType.AdvanceBoostFee => ("Express Fee", accountLastDigitsText),
            TransferDisplayType.AfterAdvanceTip => ("Tip", accountLastDigitsText),
            TransferDisplayType.CreditScoreFee => ("Credit Score Fee", accountLastDigitsText),
            TransferDisplayType.PhysicalCardDeliveryFee => ("Physical Card Delivery Fee", accountLastDigitsText),
            TransferDisplayType.AtmFee => ("ATM Fee", accountLastDigitsText),
            TransferDisplayType.ForeignTransactionFee => ("Foreign Transaction Fee", accountLastDigitsText),
            TransferDisplayType.CardTransferFee => ("Card Transfer Fee", accountLastDigitsText),
            TransferDisplayType.ExternalWalletFee => ("External Wallet Fee", accountLastDigitsText),
            TransferDisplayType.DefaultFee => ("Fee", accountLastDigitsText),

            TransferDisplayType.OtherCardTransaction => (TransfersMappingHelper.ShortenMerchant(transaction.Note), cardLastDigitsText),
            TransferDisplayType.Declined => ("Card Declined", cardLastDigitsText),
            TransferDisplayType.B9Bonus => ($"{StringProvider.GetPartnerName(_partnerProvider)} Bonus", accountLastDigitsText),
            TransferDisplayType.B9Cashback => ($"{StringProvider.GetPartnerName(_partnerProvider)} Cashback", cardLastDigitsText),
            TransferDisplayType.ReturnedPaycharge => (TransfersMappingHelper.InferReturnedChargeName(transaction.Note), accountLastDigitsText),

            TransferDisplayType.OtherDefaultTransaction => (transaction.Note, "Unrecognized Transfer Type"),

            _ => throw new NotImplementedException("Unknown display type: " + displayType)
        };
    }

    private async Task<List<ExternalCardListItem>> FetchCards(List<SavingsTransactionItem> transactions, string ipAddress)
    {
        var cardsIds = transactions
            .Where(x => !string.IsNullOrEmpty(x.Debtor?.Identifier) && x.Debtor.Identifier.StartsWith("EXTERNALCARD:"))
            .Select(x => x.Debtor.Identifier[13..])
            .ToList();

        cardsIds.AddRange(transactions
            .Where(x => !string.IsNullOrEmpty(x.Creditor?.Identifier) && x.Creditor.Identifier.StartsWith("EXTERNALCARD:"))
            .Select(x => x.Creditor.Identifier[13..])
            .ToList());

        if (cardsIds == null || !cardsIds.Any())
        {
            return new List<ExternalCardListItem>();
        }

        var cards = await _externalCardsService
            .GetCards(CurrentUser.UserMbanqToken, ipAddress, cardsIds.Distinct().Select(x => int.Parse(x)));

        return cards.ToList();
    }

    private async Task<List<KeyValuePair<int, string>>> FetchAccounts(List<SavingsTransactionItem> transactions)
    {
        var accountIds = transactions
            .Where(x => !string.IsNullOrEmpty(x.Counterparty) && x.Counterparty.StartsWith("ID:"))
            .Select(x => x.Counterparty.Split(':').LastOrDefault())
            .Distinct()
            .ToArray();

        var accounts = await _accountsService.GetSavingAccounts(accountIds);
        return accounts.Select(x => new KeyValuePair<int, string>(x.Id, x.AccountNumber)).ToList();
    }

    private string ExtractExternalCardNumber(Dictionary<long, string> externalCards, string identifier)
    {
        if (externalCards != null && identifier != null)
        {
            var cardIdString = identifier.Split(":").LastOrDefault();
            if (long.TryParse(cardIdString, out var cardId))
            {
                if (externalCards.ContainsKey(cardId))
                {
                    return "Card *" + externalCards[cardId];
                }

                return "Deleted Card";
            }
        }

        return "External Card";
    }

    private string ExtractSenderAccountNumber(Dictionary<int, string> accounts, string counterparty)
    {
        if (!string.IsNullOrEmpty(counterparty) &&
            int.TryParse(counterparty.Split(':').LastOrDefault(), out var intId))
        {
            if (accounts.ContainsKey(intId))
            {
                return SecretNumberHelper.GetLast4DigitsWithPrefix(accounts[intId], "Account *");
            }
        }

        return $"From {StringProvider.GetPartnerName(_partnerProvider)} Client";
    }

    private string GetAchSenderAccountDigits(Debtor transactionDebtor)
    {
        var digits = transactionDebtor?.Identifier?.Split('/').LastOrDefault();
        return SecretNumberHelper.GetLastFourDigits(digits);
    }

    private string Coalesce(string name, string backupName) => string.IsNullOrEmpty(name) ? backupName : name;

    private async Task ExcludeAlreadyMarkedTransfers(GetFilteredTransfersExpandedFilter filter)
    {
        filter.CompletionState = BankTransferCompletionState.Completed;

        var markedTransfers = await DbContext.PotentialAchIncomeTransfers
            .Where(x => x.UserId == CurrentUser.UserId)
            .Select(x => x.TransferExternalId)
            .ToListAsync();

        filter.NotIncludedSavingTransactionIds ??= new List<int>();
        filter.NotIncludedSavingTransactionIds.AddRange(markedTransfers);
    }

    private async Task ExcludeRecognizedPayrollTransfers(GetFilteredTransfersExpandedFilter filter)
    {
        var payrollAchKeywords = await Mediator.Send(new GetACHPrincipalKeywordsQuery());
        var recognizedIncomeTransferIds = await DbContext
            .ACHCreditTransfers
            .GetPayrollExternalTransactionIds(
                CurrentUser.UserId!.Value,
                payrollAchKeywords.ToArray());

        filter.NotIncludedSavingTransactionIds ??= new List<int>();
        filter.NotIncludedSavingTransactionIds.AddRange(recognizedIncomeTransferIds);
    }
}
