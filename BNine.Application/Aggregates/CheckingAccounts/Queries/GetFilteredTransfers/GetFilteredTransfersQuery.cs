﻿namespace BNine.Application.Aggregates.CheckingAccounts.Queries.GetFilteredTransfers;

using Abstractions;
using Application.Models;
using MediatR;
using Models;
using Newtonsoft.Json;


public class GetFilteredTransfersQuery
    : MBanqSelfServiceUserRequest,
      IRequest<PaginatedFilteredList<BankTransferViewModel, GetFilteredTransfersFilter>>
{
    [JsonProperty("filter")]
    public GetFilteredTransfersExpandedFilter Filter
    {
        get;
        set;
    } = new();

    [JsonProperty("paging")]
    public GetFilteredTransfersPagingParameters Paging
    {
        get;
        set;
    } = new();
}
