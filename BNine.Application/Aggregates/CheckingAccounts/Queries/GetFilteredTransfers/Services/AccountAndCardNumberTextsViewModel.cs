﻿namespace BNine.Application.Aggregates.CheckingAccounts.Queries.GetFilteredTransfers.Services;

public class AccountAndCardNumberTextsViewModel
{
    public int AccountExternalId
    {
        get;
        set;
    }

    public string CardLastDigitsText
    {
        get;
        set;
    }

    public string AccountLastDigitsText
    {
        get;
        set;
    }
}
