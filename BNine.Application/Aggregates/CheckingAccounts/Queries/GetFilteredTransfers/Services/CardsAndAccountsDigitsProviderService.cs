﻿namespace BNine.Application.Aggregates.CheckingAccounts.Queries.GetFilteredTransfers;

using Exceptions;
using Interfaces;
using Interfaces.TransactionHistory;
using Microsoft.EntityFrameworkCore;
using Services;

public class CardsAndAccountsDigitsProviderService : ICardsAndAccountsDigitsProviderService
{
    private readonly ITransfersMappingService _transfersMappingService;
    private readonly IBNineDbContext _bNineDbContext;
    private readonly ICurrentUserService _currentUserService;

    public CardsAndAccountsDigitsProviderService(ITransfersMappingService transfersMappingService,
        IBNineDbContext bNineDbContext,
        ICurrentUserService currentUserService)
    {
        _transfersMappingService = transfersMappingService;
        _bNineDbContext = bNineDbContext;
        _currentUserService = currentUserService;
    }

    public async Task<AccountAndCardNumberTextsViewModel> GetAccountAndCardDetails(GetFilteredTransfersQuery request,
        CancellationToken cancellationToken)
    {
        var account = await _bNineDbContext.Users
            .Include(x => x.CurrentAccount)
            .Include(x => x.DebitCards)
            .Where(x => x.Id == _currentUserService.UserId)
            .Select(x => new
            {
                x.CurrentAccount.ExternalId,
                DebitCard = request.Filter.CardId.HasValue
                    ? x.DebitCards.FirstOrDefault(dc => dc.ExternalId == request.Filter.CardId)
                    : x.DebitCards.FirstOrDefault(dc => dc.TerminatedAt == null)
            }).FirstOrDefaultAsync(cancellationToken);

        if (account is null)
        {
            throw new NotFoundException();
        }

        var (cardLastDigitsText, accountLastDigitsText) =
            await _transfersMappingService.GetAccountAndCardNumberTexts(account.ExternalId, account.DebitCard,
                request.MbanqAccessToken, request.Ip);

        var details = new AccountAndCardNumberTextsViewModel()
        {
            AccountExternalId = account.ExternalId,
            CardLastDigitsText = cardLastDigitsText,
            AccountLastDigitsText = accountLastDigitsText
        };

        return details;
    }
}
