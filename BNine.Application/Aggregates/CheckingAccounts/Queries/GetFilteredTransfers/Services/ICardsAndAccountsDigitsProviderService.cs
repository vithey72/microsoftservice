﻿namespace BNine.Application.Aggregates.CheckingAccounts.Queries.GetFilteredTransfers.Services;

public interface ICardsAndAccountsDigitsProviderService
{
    Task<AccountAndCardNumberTextsViewModel> GetAccountAndCardDetails(
        GetFilteredTransfersQuery request, CancellationToken cancellationToken);
}
