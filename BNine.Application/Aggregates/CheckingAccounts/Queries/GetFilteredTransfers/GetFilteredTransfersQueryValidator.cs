﻿namespace BNine.Application.Aggregates.CheckingAccounts.Queries.GetFilteredTransfers;

using FluentValidation;

public class GetFilteredTransfersQueryValidator : AbstractValidator<GetFilteredTransfersQuery>
{
    public GetFilteredTransfersQueryValidator()
    {
        RuleFor(x => x.Filter.TimePeriod.MinDateUtc)
            .Must(dt => dt == null || dt.Value < DateTime.Now)
            .WithMessage("Starting filter date must be in the past");

        RuleFor(x => x.Filter.TimePeriod)
            .Must(p => p.MaxDateUtc == null || p.MinDateUtc == null || p.MaxDateUtc > p.MinDateUtc)
            .WithMessage("Ending filter date must be greater than starting date");

        RuleFor(x => x.Filter.Amount)
            .Must(amt => amt.MinAmount == null || amt.MaxAmount == null || amt.MinAmount <= amt.MaxAmount)
            .WithMessage("Maximum amount filter must be greater than minimum amount filter");

        RuleFor(x => x.Filter.Amount.MinAmount)
            .Must(amt => amt == null || amt >= 0)
            .WithMessage("Minimum amount filter must be zero or greater");

        RuleFor(x => x.Filter.Amount.MaxAmount)
            .Must(amt => amt == null || amt > 0)
            .WithMessage("Maximum amount filter must be positive");
    }
}
