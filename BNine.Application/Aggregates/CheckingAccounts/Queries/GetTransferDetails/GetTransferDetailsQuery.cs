﻿namespace BNine.Application.Aggregates.CheckingAccounts.Queries.GetTransferDetails
{
    using Abstractions;
    using Enums.Transfers.Controller;
    using MediatR;
    using Models;

    public class GetTransferDetailsQuery : MBanqSelfServiceUserRequest, IRequest<BankTransferDetailsViewModel>
    {
        public int TransferId
        {
            get;
            set;
        }

        public BankTransferCompletionState Type
        {
            get;
            set;
        }
    }
}
