﻿#pragma warning disable IDE0009 // Member access should be qualified.
namespace BNine.Application.Aggregates.CheckingAccounts.Queries.GetTransferDetails
{
    using System;
    using System.Threading.Tasks;
    using Abstractions;
    using Application.Models.MBanq;
    using AutoMapper;
    using Common;
    using CommonModels.DialogElements;
    using Configuration.Queries.GetConfiguration;
    using Constants;
    using Enums;
    using Enums.Transfers;
    using Enums.Transfers.Controller;
    using Extensions;
    using Helpers;
    using Interfaces;
    using Interfaces.Bank.Client;
    using Interfaces.TransactionHistory;
    using LoanSettings.Queries.GetACHPrincipalKeywords;
    using MediatR;
    using Microsoft.EntityFrameworkCore;
    using Models;

    using IBankLoanAccountsAdministrationService = Interfaces.Bank.Administration.IBankLoanAccountsService;

    public class GetTransferDetailsQueryHandler
        : AbstractRequestHandler
        , IRequestHandler<GetTransferDetailsQuery, BankTransferDetailsViewModel>
    {
        private const string DeletedCard = "Deleted Card";
        private readonly (string, string) _atmWithdrawalTitle = ("ATM Withdrawal", string.Empty);
        private readonly (string, string) _achTransferTitle = ("ACH money transfer", string.Empty);

        private (string, string) InternalTransferSentTitle => ("Send Money", $"Instant free transfer to another {StringProvider.GetPartnerName(_partnerProvider)} member.");

        private readonly IBankSavingsTransactionsService _transfersService;
        private readonly IBankClientExternalCardsService _externalCardsService;
        private readonly ITransferIconsAndNamesProviderService _iconsAndNamesProvider;
        private readonly ITransfersMappingService _transfersMappingService;
        private readonly IBankSavingAccountsService _accountsService;
        private readonly IPartnerProviderService _partnerProvider;
        private readonly IBankLoanAccountsAdministrationService _loanAccountsService;

        public GetTransferDetailsQueryHandler(
            IMediator mediator,
            IBNineDbContext dbContext,
            IMapper mapper,
            ICurrentUserService currentUser,
            IBankSavingsTransactionsService transfersService,
            IBankClientExternalCardsService externalCardsService,
            ITransferIconsAndNamesProviderService iconsAndNamesProvider,
            ITransfersMappingService transfersMappingService,
            IBankSavingAccountsService accountsService,
            IPartnerProviderService partnerProvider,
            IBankLoanAccountsAdministrationService loanAccountsService
        )
            : base(mediator, dbContext, mapper, currentUser)
        {
            _transfersService = transfersService;
            _externalCardsService = externalCardsService;
            _iconsAndNamesProvider = iconsAndNamesProvider;
            _transfersMappingService = transfersMappingService;
            _accountsService = accountsService;
            _partnerProvider = partnerProvider;
            _loanAccountsService = loanAccountsService;
        }

        public async Task<BankTransferDetailsViewModel> Handle(GetTransferDetailsQuery request, CancellationToken cancellationToken)
        {
            var result = (request.Type) switch
            {
                BankTransferCompletionState.Completed or BankTransferCompletionState.Returned =>
                    await ConvertToUniversal(await _transfersService.GetSavingsAccountTransfer(
                        request.TransferId,
                        request.MbanqAccessToken,
                        request.Ip), request.Type, request.MbanqAccessToken, request.Ip),
                BankTransferCompletionState.Pending =>
                    await ConvertToUniversal(await _transfersService.GetPendingTransfer(
                    request.TransferId,
                    request.MbanqAccessToken,
                    request.Ip), request.Type),
                BankTransferCompletionState.Declined or BankTransferCompletionState.Returned =>
                    await ConvertToUniversal(await _transfersService.GetRejectedTransfer(
                    request.TransferId,
                    request.MbanqAccessToken,
                    request.Ip), request.Type, request.MbanqAccessToken, request.Ip),
                _ => throw new NotImplementedException("Unknown transfer type: " + request.Type),
            };

            return result;
        }

        private async Task<BankTransferDetailsViewModel> ConvertToUniversal(SavingsTransactionItem savingsTransaction, BankTransferCompletionState completion,
            string mbanqToken, string ip)
        {
            var viewModel = BuildBaseModel(savingsTransaction, completion);
            var displayType = savingsTransaction.TransferDisplayType;
            viewModel.Amount = CurrencyFormattingHelper.AsRegular(savingsTransaction.Amount);

            var merchant = _transfersMappingService.InferMerchant(savingsTransaction.Note);
            if (!string.IsNullOrEmpty(merchant?.Name))
            {
                viewModel.IconUrl = merchant.PictureUrl;
            }
            else
            {
                viewModel.IconUrl = await _transfersMappingService.InferIconUrl(
                    displayType.InferIconOption(),
                    savingsTransaction.MccCode,
                    savingsTransaction.Counterparty);
            }

            viewModel.Mcc = savingsTransaction.MccCode;
            viewModel.SavingsTransferType = savingsTransaction.TransferDisplayType;

            if (savingsTransaction.Type == TransactionTypes.REJECTED)
            {
                (viewModel.OperationTypeHeader, viewModel.PageTitle.Title, viewModel.PageTitle.Subtitle) =
                    ($"{CardNumberText(savingsTransaction.CardDigits)} Charge".ToUpper(), "Send money", $"Instant free transfer to another {StringProvider.GetPartnerName(_partnerProvider)} member.");
            }
            else
            {
                (viewModel.OperationTypeHeader, viewModel.PageTitle.Title, viewModel.PageTitle.Subtitle) =
                    await InferTypeHeader(displayType, savingsTransaction.CardDigits, savingsTransaction.Reference);
            }

            if (!string.IsNullOrEmpty(merchant.Name))
            {
                viewModel.DebtorOrCreditorName = merchant.Name;
            }
            else
            {
                viewModel.DebtorOrCreditorName = InferDebtorOrCreditorName(savingsTransaction, displayType, viewModel);
            }

            AddBaseDetails(savingsTransaction, viewModel);
            if (displayType is TransferDisplayType.SentToB9Client)
            {
                viewModel.IsRepeatable = true;
                viewModel.FavoriteTransferId = await GetFavoriteTransactionId(savingsTransaction.Id);
                await AddOutgoingInternalTransferDetails(savingsTransaction, viewModel);
            }
            else if (displayType is TransferDisplayType.ReceivedFromB9Client)
            {
                await AddIncomingInternalTransferDetails(savingsTransaction, viewModel);
            }
            else if (displayType is TransferDisplayType.AdvanceRepayment)
            {
                AddAdvanceRepaymentDetails(savingsTransaction, viewModel, incoming: false);
            }
            else if (displayType is TransferDisplayType.AdvanceIssued)
            {
                AddAdvanceRepaymentDetails(savingsTransaction, viewModel, incoming: true);
            }
            else if (displayType is TransferDisplayType.AchSent)
            {
                viewModel.IsRepeatable = true;
                viewModel.FavoriteTransferId = await GetFavoriteTransactionId(savingsTransaction.Id);
                AddAchTransferSentDetails(savingsTransaction, viewModel);
            }
            else if (displayType is TransferDisplayType.AchReceived)
            {
                AddAchTransferReceivedDetails(savingsTransaction, viewModel);
                await SetMarkedAsPotentialIncomeFlag(viewModel);
            }
            else if (displayType is TransferDisplayType.MonthlyB9Fee or TransferDisplayType.ExternalWalletFee or TransferDisplayType.CardTransferFee
                     or TransferDisplayType.ForeignTransactionFee or TransferDisplayType.AtmFee or TransferDisplayType.VipSupportFee or TransferDisplayType.AdvanceUnfreezeFee
                     or TransferDisplayType.DefaultFee or TransferDisplayType.PhysicalCardDeliveryFee or TransferDisplayType.AdvanceBoostFee
                     or TransferDisplayType.CreditScoreFee)
            {
                AddFeeDetails(savingsTransaction, viewModel);
            }
            else if (displayType is TransferDisplayType.PullFromExternalCard)
            {
                await AddPullFromCardDetails(savingsTransaction, viewModel, mbanqToken, ip);
            }
            else if (displayType is TransferDisplayType.PushToExternalCard)
            {
                string externalCardNumber = null;
                var cardId = savingsTransaction.Creditor.Identifier.Split(':').LastOrDefault();
                if (int.TryParse(cardId, out var cardInt))
                {
                    externalCardNumber = await GetExternalCardNumber(mbanqToken, ip, cardInt);
                    if (externalCardNumber != DeletedCard)
                    {
                        viewModel.IsRepeatable = true;
                        viewModel.FavoriteTransferId = await GetFavoriteTransactionId(savingsTransaction.Id);
                    }
                }
                AddPushToCardDetails(savingsTransaction, viewModel, externalCardNumber);
            }
            else if (displayType is TransferDisplayType.B9Cashback or TransferDisplayType.B9Bonus or TransferDisplayType.AfterAdvanceTip)
            {
                AddIncentiveDetails(savingsTransaction, viewModel);
            }
            else if (displayType is TransferDisplayType.Declined)
            {
                AddDeclineDetails(savingsTransaction, viewModel);
            }
            else if (displayType is TransferDisplayType.ReturnedPaycharge)
            {
                AddReturnedChargeDetails(savingsTransaction, viewModel);
            }
            else if (displayType is TransferDisplayType.ReceivedTransferToCard)
            {
                AddSavingsTransactionDetails(savingsTransaction, viewModel);
                await SetMarkedAsPotentialIncomeFlag(viewModel);
            }
            else if (displayType is TransferDisplayType.Rewards)
            {
                AddRewardsTransactionDetails(savingsTransaction, viewModel);
            }
            else
            {
                AddSavingsTransactionDetails(savingsTransaction, viewModel);
            }

            return viewModel;
        }

        private async Task<BankTransferDetailsViewModel> ConvertToUniversal(PendingTransactionItem pendingTransaction, BankTransferCompletionState type)
        {
            var response = BuildBaseModel(pendingTransaction, type);
            response.Amount = CurrencyFormattingHelper.AsRegular(pendingTransaction.Amount);
            response.Mcc = pendingTransaction.MccCode;
            response.PendingTransferType = pendingTransaction.PaymentType;

            response.DebtorOrCreditorName = InferPendingTransferDebtorOrCreditorName(
                pendingTransaction, pendingTransaction.PaymentType, response);

            (response.OperationTypeHeader, response.PageTitle.Title, response.PageTitle.Subtitle)
                = InferPendingTransferTitle(pendingTransaction.PaymentType, pendingTransaction.CardDigits);

            AddBaseDetails(pendingTransaction, response);

            if (pendingTransaction.PaymentType == PendingTransferDisplayType.Atm)
            {
                response.IconUrl = _iconsAndNamesProvider.FindIconUrl(BankTransferIconOption.Atm);
                AddAtmPendingDetails(pendingTransaction, response);
            }
            else if (pendingTransaction.PaymentType == PendingTransferDisplayType.Internal)
            {
                response.IconUrl = await _transfersMappingService.InferIconUrl(
                    BankTransferIconOption.Person,
                    pendingTransaction.MccCode,
                    pendingTransaction.Creditor.Identifier);
                await AddPendingInternalTransferDetails(pendingTransaction, response);
            }
            // TODO: ACH case
            else
            {
                response.IconUrl = _iconsAndNamesProvider.FindMccIconUrl(pendingTransaction.MccCode);
                AddPendingTransactionDetails(pendingTransaction, response);
            }

            return response;
        }

        private BankTransferDetailsViewModel BuildBaseModel(TransactionItemBase baseTransaction, BankTransferCompletionState type)
        {
            var model = new BankTransferDetailsViewModel
            {
                Header = GetTransferDetailsPageHeder(type),
                Id = baseTransaction.Id,
                TimeStampUtc = baseTransaction.CreatedAt,
                Details = new(),
            };

            if (type is BankTransferCompletionState.Pending)
            {
                model.DropDownList = BuildDropDownList();
            }

            return model;

        }

        private static string GetTransferDetailsPageHeder(BankTransferCompletionState type)
        {
            if (type == BankTransferCompletionState.Pending)
            {
                return "Processing";
            }
            return type.ToString();
        }

        private async Task<(string opType, string title, string subtitle)> InferTypeHeader(
            TransferDisplayType displayType,
            string cardDigits,
            string reference)
        {
            var periodicFeeDisplayName = _partnerProvider.GetPartner() != PartnerApp.Qorbis
                ? "Monthly Card and Account Fee" : "Annual Membership Fee";

            var partnerName = StringProvider.GetPartnerName(_partnerProvider);

            var operationType = displayType switch
            {
                TransferDisplayType.Atm => $"{CardNumberText(cardDigits)} ATM Withdrawal",

                TransferDisplayType.AdvanceIssued => "Advance Pay",
                TransferDisplayType.ReceivedTransferToCard => "Card Transfer Received",
                TransferDisplayType.AchReceived => "ACH Received",
                TransferDisplayType.ReceivedFromB9Client => $"Received from {StringProvider.GetPartnerName(_partnerProvider)} Member",
                TransferDisplayType.CheckCashing => "Check Cashing",
                TransferDisplayType.PullFromExternalCard => "Transfer from Card",
                TransferDisplayType.DefaultDeposit => "Deposit",

                TransferDisplayType.AdvanceRepayment => await GetAdvanceRepaymentOperationTypeHeader(reference),
                TransferDisplayType.AchSent => "ACH Sent",
                TransferDisplayType.MonthlyB9Fee => TransfersMappingHelper.GetMonthlyFeeTextWithPeriod(reference, isDetailsScreenFormat: true, periodicFeeName: periodicFeeDisplayName, _partnerProvider.GetPartner() == PartnerApp.Qorbis ? 12 : 1)
                                                    ?? periodicFeeDisplayName,
                TransferDisplayType.AnnualFee => periodicFeeDisplayName,
                TransferDisplayType.VipSupportFee => "Premium Support Fee",
                TransferDisplayType.AdvanceUnfreezeFee => "Advance Unfreeze Fee",
                TransferDisplayType.AdvanceBoostFee => "Express Fee",
                TransferDisplayType.AfterAdvanceTip => "Tip",
                TransferDisplayType.CreditScoreFee => "Credit Score Fee",
                TransferDisplayType.PhysicalCardDeliveryFee => "Physical Card Delivery Fee",
                TransferDisplayType.SentToB9Client => $"Paid to {StringProvider.GetPartnerName(_partnerProvider)} Member",
                TransferDisplayType.PushToExternalCard => "Transfer to Card",
                TransferDisplayType.ApprovedCardAuth => $"{CardNumberText(cardDigits)} Charge",
                TransferDisplayType.DefaultWithdrawal => "Withdrawal",

                TransferDisplayType.AtmFee => "ATM Fee",
                TransferDisplayType.ForeignTransactionFee => "Foreign Transaction Fee",
                TransferDisplayType.CardTransferFee => "Card Transfer Fee",
                TransferDisplayType.ExternalWalletFee => "External Wallet Fee",
                TransferDisplayType.DefaultFee => "Fee",

                TransferDisplayType.OtherCardTransaction or
                TransferDisplayType.Declined => $"{CardNumberText(cardDigits)} Charge",
                TransferDisplayType.B9Bonus => $"{StringProvider.GetPartnerName(_partnerProvider)} Bonus",
                TransferDisplayType.B9Cashback => $"{StringProvider.GetPartnerName(_partnerProvider)} Cashback",
                TransferDisplayType.Returned => $"{CardNumberText(cardDigits)} Payment Reversed",
                TransferDisplayType.ReturnedPaycharge => "Reversed Fee",
                TransferDisplayType.Rewards => $"{partnerName} Rewards",

                TransferDisplayType.OtherDefaultTransaction => "Transfer ???",

                _ => throw new NotImplementedException("Unknown display type: " + displayType)
            };

            var (title, subtitle) = displayType switch
            {
                TransferDisplayType.AchSent or TransferDisplayType.AchReceived => _achTransferTitle,
                TransferDisplayType.Atm => _atmWithdrawalTitle,
                TransferDisplayType.MonthlyB9Fee => (periodicFeeDisplayName, string.Empty),
                TransferDisplayType.PushToExternalCard => ("Transfer to Card", "Instant transfer to external Card"),
                TransferDisplayType.PullFromExternalCard => ("Transfer from Card", $"Instant transfer to {StringProvider.GetPartnerName(_partnerProvider)} Card."),
                TransferDisplayType.SentToB9Client => InternalTransferSentTitle,
                TransferDisplayType.ReceivedFromB9Client =>
                    ($"Transfer from {StringProvider.GetPartnerName(_partnerProvider)} Member",
                    $"Instant free transfer from another {StringProvider.GetPartnerName(_partnerProvider)} member."),
                _ => ("Transaction", string.Empty),
            };
            return (operationType, title, subtitle);
        }

        private async Task<string> GetAdvanceRepaymentOperationTypeHeader(string reference)
        {
            if (TransfersMappingHelper.TryGetLoanAccountExternalId(reference, out var loanExternalId))
            {
                var loan = await _loanAccountsService.GetLoan(loanExternalId);
                var hasBoost = loan is { FeeAmount: > 0m };

                if (hasBoost)
                {
                    var advanceAmount = Convert.ToDecimal(loan.Total);
                    return "Advance Repayment:\n" +
                        $"Advance {CurrencyFormattingHelper.AsRegular(advanceAmount)} and Express fee {CurrencyFormattingHelper.AsRegular(loan.FeeAmount)}";
                }
            }

            return "Advance Repayment";
        }

        private string InferDebtorOrCreditorName(SavingsTransactionItem savingsTransaction, TransferDisplayType displayType,
            BankTransferDetailsViewModel viewModel)
        {
            var debtor = savingsTransaction.Debtor?.Name;
            var creditor = savingsTransaction.Creditor?.Name;

            return displayType switch
            {
                TransferDisplayType.AchSent => creditor,
                TransferDisplayType.SentToB9Client => creditor,
                TransferDisplayType.AchReceived => debtor,
                TransferDisplayType.ReceivedFromB9Client => debtor,
                TransferDisplayType.PushToExternalCard or TransferDisplayType.PullFromExternalCard => savingsTransaction.CardDigits,
                _ => viewModel.OperationTypeHeader,
            };
        }

        private (string operationType, string Title, string Subtitle) InferPendingTransferTitle(
            PendingTransferDisplayType displayType, string cardDigits)
        {
            var operationType = displayType switch
            {
                PendingTransferDisplayType.Ach => "ACH Sent",
                PendingTransferDisplayType.Internal => $"Paid to {StringProvider.GetPartnerName(_partnerProvider)} Member",
                _ => $"{CardNumberText(cardDigits)} Charge",
            };

            var (title, subtitle) = displayType switch
            {
                PendingTransferDisplayType.Ach => _achTransferTitle,
                PendingTransferDisplayType.Atm => _atmWithdrawalTitle,
                PendingTransferDisplayType.Internal => InternalTransferSentTitle,
                // TODO: more cases from business
                _ => ("Pending Transaction", null),
            };
            return (operationType, title, subtitle);
        }

        private string InferPendingTransferDebtorOrCreditorName(PendingTransactionItem pendingTransaction,
            PendingTransferDisplayType displayType, BankTransferDetailsViewModel viewModel)
        {
            var merchant = _transfersMappingService.InferMerchant(pendingTransaction.Merchant);
            var creditor = pendingTransaction.Creditor?.Name;

            return displayType switch
            {
                PendingTransferDisplayType.Ach or PendingTransferDisplayType.Internal => creditor,
                PendingTransferDisplayType.CardAuthorization or PendingTransferDisplayType.Atm => merchant.Name,
                _ => "Pending Transaction",
            };
        }

        private async Task AddIncomingInternalTransferDetails(SavingsTransactionItem savingsTransaction, BankTransferDetailsViewModel viewModel)
        {
            var (fullName, phone) = await _transfersMappingService.ExtractB9ClientPhoneAndName(savingsTransaction.Counterparty);

            var externalId = savingsTransaction.Counterparty?.Split(':').LastOrDefault();
            if (int.TryParse(externalId, out var accountExternalId))
            {
                var acc = await _accountsService.GetSavingAccountInfoAsAdmin(accountExternalId);
                viewModel.Details.Add(CreateProperty($"From {StringProvider.GetPartnerName(_partnerProvider)} Account", acc.AccountNumber));
            }
            AddPropertyIfValueNotNull(viewModel.Details, "From Phone Number", phone);
            AddPropertyIfValueNotNull(viewModel.Details, $"Account in {StringProvider.GetPartnerName(_partnerProvider)}", fullName);
            AddPropertyIfValueNotNull(viewModel.Details, "Your comment", savingsTransaction.UserNote ?? savingsTransaction.Note);
            viewModel.Details.Add(CreateProperty("Amount", CurrencyFormattingHelper.AsRegular(savingsTransaction.Amount)));
        }

        private async Task AddOutgoingInternalTransferDetails(SavingsTransactionItem savingsTransaction, BankTransferDetailsViewModel viewModel)
        {
            var (fullName, phone) = await _transfersMappingService.ExtractB9ClientPhoneAndName(savingsTransaction.Counterparty);
            viewModel.Details.Add(CreateProperty($"From {StringProvider.GetPartnerName(_partnerProvider)} Account", savingsTransaction.AccountDigits));
            AddPropertyIfValueNotNull(viewModel.Details, "From Phone Number", phone);
            AddPropertyIfValueNotNull(viewModel.Details, $"Account in {StringProvider.GetPartnerName(_partnerProvider)}", fullName);
            AddPropertyIfValueNotNull(viewModel.Details, "Your comment", savingsTransaction.UserNote ?? savingsTransaction.Note);
            viewModel.Details.Add(CreateProperty("Amount", CurrencyFormattingHelper.AsRegular(savingsTransaction.Amount)));
        }

        private void AddAdvanceRepaymentDetails(SavingsTransactionItem savingsTransaction, BankTransferDetailsViewModel viewModel, bool incoming)
        {
            viewModel.Details.Add(CreateProperty((incoming ? "To" : "From") + $" {StringProvider.GetPartnerName(_partnerProvider)} Account", savingsTransaction.AccountDigits));
            viewModel.Details.Add(CreateProperty("Amount", CurrencyFormattingHelper.AsRegular(savingsTransaction.Amount)));
        }

        private void AddAchTransferSentDetails(SavingsTransactionItem savingsTransaction, BankTransferDetailsViewModel viewModel)
        {
            var (recipientAccountNumber, recipientRoutingNumber) = _transfersMappingService.ExtractAchRecipientNumbers(savingsTransaction);
            viewModel.Details.Add(CreateProperty("To Routing #", recipientRoutingNumber));
            viewModel.Details.Add(CreateProperty("To Account #", recipientAccountNumber));
            viewModel.Details.Add(CreateProperty("Recipient's full name", savingsTransaction.Creditor?.Name));
            AddPropertyIfValueNotNull(viewModel.Details, "Recipient's address", savingsTransaction.Creditor?.Address?.FirstOrDefault());
            AddPropertyIfValueNotNull(viewModel.Details, "Your comment", savingsTransaction.UserNote ?? savingsTransaction.Note);
            viewModel.Details.Add(CreateProperty("Amount", CurrencyFormattingHelper.AsRegular(savingsTransaction.Amount)));
        }

        private void AddAchTransferReceivedDetails(SavingsTransactionItem savingsTransaction, BankTransferDetailsViewModel viewModel)
        {
            var (recipientAccountNumber, recipientRoutingNumber) = _transfersMappingService.ExtractAchRecipientNumbers(savingsTransaction);
            viewModel.Details.Add(CreateProperty("From Routing #", recipientRoutingNumber));
            viewModel.Details.Add(CreateProperty("From Account #", recipientAccountNumber));
            viewModel.Details.Add(CreateProperty("Sender's full name", savingsTransaction.Debtor?.Name));
            AddPropertyIfValueNotNull(viewModel.Details, "Comment", savingsTransaction.UserNote ?? savingsTransaction.Note);
            viewModel.Details.Add(CreateProperty("Amount", CurrencyFormattingHelper.AsRegular(savingsTransaction.Amount)));
        }

        private void AddFeeDetails(SavingsTransactionItem savingsTransaction, BankTransferDetailsViewModel viewModel)
        {
            viewModel.Details.Add(CreateProperty($"From {StringProvider.GetPartnerName(_partnerProvider)} Account", savingsTransaction.AccountDigits));
            viewModel.Details.Add(CreateProperty("Amount", CurrencyFormattingHelper.AsRegular(savingsTransaction.Amount)));
        }

        private void AddReturnedChargeDetails(SavingsTransactionItem savingsTransaction, BankTransferDetailsViewModel viewModel)
        {
            viewModel.Details.Add(CreateProperty($"To {StringProvider.GetPartnerName(_partnerProvider)} Account", savingsTransaction.AccountDigits));
            viewModel.Details.Add(CreateProperty("Amount", CurrencyFormattingHelper.AsRegular(savingsTransaction.Amount)));
            viewModel.Details.Add(CreateProperty("Operation", TransfersMappingHelper.InferReturnedChargeName(savingsTransaction.Note)));
        }

        private async Task AddPullFromCardDetails(SavingsTransactionItem savingsTransaction, BankTransferDetailsViewModel viewModel,
            string mbanqAccessToken, string ip)
        {
            var cardId = savingsTransaction.Debtor.Identifier.Split(':').LastOrDefault();
            if (int.TryParse(cardId, out var cardInt))
            {
                var cardNumber = await GetExternalCardNumber(mbanqAccessToken, ip, cardInt);
                viewModel.Details.Add(CreateProperty("From Card", cardNumber));
            }
            viewModel.Details.Add(CreateProperty($"To {StringProvider.GetPartnerName(_partnerProvider)} Account", savingsTransaction.AccountDigits));
            viewModel.Details.Add(CreateProperty("Amount", CurrencyFormattingHelper.AsRegular(savingsTransaction.Amount)));
        }

        private void AddPushToCardDetails(SavingsTransactionItem savingsTransaction, BankTransferDetailsViewModel viewModel,
            string cardNumber)
        {
            if (cardNumber != null)
            {
                viewModel.Details.Add(CreateProperty("To Card", cardNumber));
            }
            viewModel.Details.Add(CreateProperty($"From {StringProvider.GetPartnerName(_partnerProvider)} Account", savingsTransaction.AccountDigits));
            viewModel.Details.Add(CreateProperty("Amount", CurrencyFormattingHelper.AsRegular(savingsTransaction.Amount)));
        }

        private void AddAtmPendingDetails(PendingTransactionItem pendingTransaction, BankTransferDetailsViewModel viewModel)
        {
            viewModel.Details.Add(CreateProperty(
                $"From {StringProvider.GetPartnerName(_partnerProvider)} Card", SecretNumberHelper.GetLast4DigitsWithPrefix(pendingTransaction.CardDigits, "**** ")));
            viewModel.Details.Add(CreateProperty("Amount", CurrencyFormattingHelper.AsRegular(pendingTransaction.Amount)));
        }

        private async Task AddPendingInternalTransferDetails(PendingTransactionItem pendingTransaction, BankTransferDetailsViewModel viewModel)
        {
            var (fullName, phone) = await _transfersMappingService.ExtractB9ClientPhoneAndName(pendingTransaction.Creditor.Identifier);
            viewModel.Details.Add(CreateProperty($"From {StringProvider.GetPartnerName(_partnerProvider)} Account", pendingTransaction.AccountNumber));
            AddPropertyIfValueNotNull(viewModel.Details, "From Phone Number", phone);
            AddPropertyIfValueNotNull(viewModel.Details, $"Account in {StringProvider.GetPartnerName(_partnerProvider)}", fullName);
            AddPropertyIfValueNotNull(viewModel.Details, "Your comment", pendingTransaction.Reference);
            viewModel.Details.Add(CreateProperty("Amount", CurrencyFormattingHelper.AsRegular(pendingTransaction.Amount)));
        }

        private void AddDeclineDetails(SavingsTransactionItem savingsTransaction, BankTransferDetailsViewModel viewModel)
        {
            viewModel.Details.Add(CreateProperty("Operation ID", savingsTransaction.Id.ToString()));
            viewModel.Details.Add(CreateProperty(
                $"From {StringProvider.GetPartnerName(_partnerProvider)} Card", SecretNumberHelper.GetLast4DigitsWithPrefix(savingsTransaction.CardDigits, "**** ")));
            AddPropertyIfValueNotNull(viewModel.Details, "Merchant", TransfersMappingHelper.ShortenMerchant(savingsTransaction.Note));
            viewModel.Details.Add(CreateProperty("Amount", CurrencyFormattingHelper.AsRegular(savingsTransaction.Amount)));
        }

        private void AddSavingsTransactionDetails(SavingsTransactionItem savingsTransaction,
            BankTransferDetailsViewModel viewModel)
        {
            var fromOrTo = savingsTransaction.Direction == TransferDirection.Credit ? "To" : "From";

            if (!string.IsNullOrEmpty(savingsTransaction.CardDigits))
            {
                viewModel.Details.Add(CreateProperty(fromOrTo + $" {StringProvider.GetPartnerName(_partnerProvider)} Card",
                    SecretNumberHelper.GetLast4DigitsWithPrefix(savingsTransaction.CardDigits, "**** ")));
            }
            else
            {
                viewModel.Details.Add(CreateProperty(fromOrTo + " Account", savingsTransaction.AccountDigits));
            }
            AddPropertyIfValueNotNull(viewModel.Details, "Merchant", TransfersMappingHelper.ShortenMerchant(savingsTransaction.Note));
            viewModel.Details.Add(CreateProperty("Amount", CurrencyFormattingHelper.AsRegular(savingsTransaction.Amount)));

            // TODO: Fee amt?
        }

        private void AddPendingTransactionDetails(PendingTransactionItem pendingTransaction, BankTransferDetailsViewModel viewModel)
        {
            viewModel.Details.Add(CreateProperty($"From {StringProvider.GetPartnerName(_partnerProvider)} Card",
                SecretNumberHelper.GetLast4DigitsWithPrefix(pendingTransaction.CardDigits, "**** ")));

            AddPropertyIfValueNotNull(viewModel.Details, "Merchant",
                TransfersMappingHelper.ShortenMerchant(pendingTransaction.Merchant));

            viewModel.Details.Add(CreateProperty("Amount", CurrencyFormattingHelper.AsRegular(pendingTransaction.Amount)));
        }

        private void AddIncentiveDetails(SavingsTransactionItem savingsTransaction, BankTransferDetailsViewModel viewModel)
        {
            viewModel.Details.Add(CreateProperty("To Account", savingsTransaction.AccountDigits));
            viewModel.Details.Add(CreateProperty("Amount", CurrencyFormattingHelper.AsRegular(savingsTransaction.Amount)));
        }

        private void AddRewardsTransactionDetails(SavingsTransactionItem savingsTransaction,
            BankTransferDetailsViewModel viewModel)
        {
            viewModel.Details.Add(CreateProperty("Amount", CurrencyFormattingHelper.AsRegular(savingsTransaction.Amount)));
        }

        private void AddBaseDetails(TransactionItemBase transaction, BankTransferDetailsViewModel viewModel)
        {
            viewModel.Details.Add(CreateProperty("Operation ID", transaction.Id.ToString()));
        }

        private async Task<string> GetExternalCardNumber(string mbanqAccessToken, string ip, int cardInt)
        {
            var externalCardList = await _externalCardsService.GetCards(mbanqAccessToken, ip, new[] { cardInt });
            var firstCard = externalCardList.FirstOrDefault();
            var cardNumber = firstCard != null ? ("**** " + firstCard.Last4Digits) : DeletedCard;
            return cardNumber;
        }

        private static BankTransferFlexibleDetailsItem CreateProperty(string title, string value) =>
            new()
            {
                Title = title,
                Value = value,
            };

        private void AddPropertyIfValueNotNull(List<BankTransferFlexibleDetailsItem> detailsList, string title, string value, bool canCopy = false)
        {
            if (value == null) { return; }
            detailsList.Add(CreateProperty(title, value));
        }

        private async Task<Guid?> GetFavoriteTransactionId(int transactionId)
        {
            var savedTransfer = await DbContext
                .FavoriteUserTransfers
                .FirstOrDefaultAsync(t =>
                    t.UserId == CurrentUser.UserId &&
                    t.TransferId == transactionId);

            return savedTransfer?.Id;
        }

        private async Task SetMarkedAsPotentialIncomeFlag(BankTransferDetailsViewModel viewModel)
        {
            var config = await Mediator.Send(new GetConfigurationQuery());

            if (config.HasFeatureEnabled(FeaturesNames.Features.IncomeAndTaxesScreen))
            {
                var payrollAchKeywords = await Mediator.Send(new GetACHPrincipalKeywordsQuery());
                var recognizedIncomeTransferIds = await DbContext
                    .ACHCreditTransfers
                    .GetPayrollExternalTransactionIds(
                        CurrentUser.UserId!.Value,
                        payrollAchKeywords.ToArray());

                if (!recognizedIncomeTransferIds.Contains(viewModel.Id))
                {
                    viewModel.MarkedAsPotentialIncome = await DbContext
                        .PotentialAchIncomeTransfers
                        .AnyAsync(x => x.UserId == CurrentUser.UserId &&
                                       x.TransferExternalId == viewModel.Id);
                }
            }
        }

        private DropDownListWithBulletPoints BuildDropDownList()
        {
            var partner = _partnerProvider.GetPartner();
            var availableBalance = new BulletPointViewModel("Your available balance has been changed to reflect this transaction")
                .WithKnownType(BulletPointTypeEnum.BulletPoint);
            var amountChanges = new BulletPointViewModel("The amount may change when transaction is finalized. For example, " +
                                                         "a restaurant may not include the gratuity that is included in the final complete transaction")
                .WithKnownType(BulletPointTypeEnum.BulletPoint);
            var atmFee = new BulletPointViewModel("ATM withdrawal may include $2.50 fee which will be waived for eligible Premium users")
                .WithKnownType(BulletPointTypeEnum.BulletPoint);
            var finalAmounts = new BulletPointViewModel("Final amounts may take up to 7 business days to post to your account")
                .WithKnownType(BulletPointTypeEnum.BulletPoint);
            var suspect = new BulletPointViewModel("If you suspect fraud please contact us immediately")
                .WithKnownType(BulletPointTypeEnum.BulletPoint);

            var dropDownList = new DropDownListWithBulletPoints()
            {
                Title = "Processing transaction, amount may be changed",
                ImageUrl = "https://b9prodaccount.blob.core.windows.net/static-documents-container/Misc/drop_down_list_clock_img_64_64.png",
                BulletPoints = new List<BulletPointViewModel>()
                {
                    availableBalance,
                    amountChanges,
                    atmFee,
                    finalAmounts,
                    suspect
                }
            };

            if (partner is not (PartnerApp.Default or PartnerApp.MBanqApp))
            {
                dropDownList.BulletPoints.Remove(atmFee);
            }

            return dropDownList;
        }

        private string CardNumberText(string cardDigits) =>
            $"{StringProvider.GetPartnerName(_partnerProvider)} Card {SecretNumberHelper.GetLast4DigitsWithPrefix(cardDigits, " * ")} ";

    }
}
