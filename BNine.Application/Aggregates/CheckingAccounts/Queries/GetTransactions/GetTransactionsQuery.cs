﻿namespace BNine.Application.Aggregates.CheckingAccounts.Queries.GetTransactions
{
    using System.Collections.Generic;
    using Application.Models;
    using Enums.Transfers;
    using MediatR;
    using Models;
    using Newtonsoft.Json;

    public class GetTransactionsQuery : IRequest<PaginatedList<SavingsTransaction>>
    {
        public int Skip
        {
            get; set;
        } = 0;

        public int Take
        {
            get; set;
        } = 20;

        public IEnumerable<TransactionType> TransactionTypes
        {
            get;
            set;
        }

        [JsonIgnore]
        public string IpAddress
        {
            get; set;
        }

        [JsonIgnore]
        public string MbanqAccessToken
        {
            get;
            set;
        }
    }
}
