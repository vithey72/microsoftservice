﻿namespace BNine.Application.Aggregates.CheckingAccounts.Queries.GetTransactions
{
    using System.Collections.Generic;
    using System.Threading;
    using System.Threading.Tasks;
    using AutoMapper;
    using BNine.Application.Abstractions;
    using BNine.Application.Aggregates.CheckingAccounts.Models;
    using BNine.Application.Exceptions;
    using BNine.Application.Interfaces;
    using BNine.Application.Interfaces.Bank.Client;
    using BNine.Application.Models;
    using BNine.Application.Models.MBanq;
    using BNine.Constants;
    using BNine.Enums.Transfers;
    using MediatR;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.Extensions.Logging;

    public class GetTransactionsQueryHandler
        : AbstractRequestHandler
        , IRequestHandler<GetTransactionsQuery, PaginatedList<SavingsTransaction>>
    {
        private readonly IBankSavingAccountsService _bankAccountService;
        private readonly IBankSavingsTransactionsService _bankSavingsTransactionsService;
        private readonly IBankClientExternalCardsService _bankClientExternalCardsService;
        private readonly ILogger<GetTransactionsQueryHandler> _logger;

        public GetTransactionsQueryHandler(
            IBankClientExternalCardsService bankClientExternalCardsService,
            IBankSavingAccountsService bankAccountService,
            IMediator mediator,
            IBNineDbContext dbContext,
            IMapper mapper,
            ICurrentUserService currentUser,
            ILogger<GetTransactionsQueryHandler> logger,
            IBankSavingsTransactionsService bankSavingsTransactionsService)
            : base(mediator, dbContext, mapper, currentUser)
        {
            _bankAccountService = bankAccountService;
            _bankSavingsTransactionsService = bankSavingsTransactionsService;
            _bankClientExternalCardsService = bankClientExternalCardsService;
            _logger = logger;
        }

        public async Task<PaginatedList<SavingsTransaction>> Handle(GetTransactionsQuery request, CancellationToken cancellationToken)
        {
            var account = await DbContext.Users
                .Include(x => x.CurrentAccount)
                .Where(x => x.Id == CurrentUser.UserId)
                .Select(x => new { x.CurrentAccount.ExternalId })
                .FirstOrDefaultAsync(cancellationToken);

            if (account is null)
            {
                throw new NotFoundException();
            }

            var transactions = await _bankSavingsTransactionsService.GetTransactions(account.ExternalId, request.Skip + 1, request.Take, null, null, request.TransactionTypes, request.MbanqAccessToken, request.IpAddress);

            var transactionsList = new List<SavingsTransaction>();
            if (transactions.TotalCount == 0)
            {
                return new PaginatedList<SavingsTransaction>(transactionsList, transactions.TotalCount, transactions.PageNumber, request.Take);
            }

            var savingAccount = await _bankAccountService.GetSavingAccountInfo(account.ExternalId, request.MbanqAccessToken, request.IpAddress);

            var ids = transactions.Items
                .Where(x => x.TypeId == 1 && !string.IsNullOrEmpty(x.Counterparty))
                .Select(x => int.Parse(x.Counterparty.Split(":").Last()))
                .ToList();

            var phones = await DbContext.Users
                .Include(x => x.CurrentAccount)
                .Where(x => ids.Contains(x.CurrentAccount.ExternalId))
                .Select(x => new KeyValuePair<int, string>(x.CurrentAccount.ExternalId, x.Phone))
                .ToListAsync(cancellationToken);

            var cardsIds = transactions.Items
                .Where(x => !string.IsNullOrEmpty(x.Debtor?.Identifier) && x.Debtor.Identifier.StartsWith("EXTERNALCARD:"))
                .Select(x => x.Debtor.Identifier[13..])
                .ToList();

            cardsIds.AddRange(transactions.Items
                .Where(x => !string.IsNullOrEmpty(x.Creditor?.Identifier) && x.Creditor.Identifier.StartsWith("EXTERNALCARD:"))
                .Select(x => x.Creditor.Identifier[13..])
                .ToList());

            var cards = await FetchCards(cardsIds.Distinct(), request.IpAddress);

            foreach (var transaction in transactions.Items)
            {
                try
                {
                    var savingTransaction = GetSavingTransactionDetails(transaction);

                    // visually fixing MBanq incident 05.05.2022
                    if (transaction?.Note == "Reversal of duplicate ACH deposit")
                    {
                        savingTransaction.Type = TransactionTypes.B9_CARD;
                        savingTransaction.To = "Error Deposit Reversal";
                    }

                    if (transaction.TypeId == 1) //INTERNAL //TODO: store codes in enum
                    {
                        if (transaction.Note != null && transaction.Note.EndsWith("Reversed."))
                        {
                            savingTransaction.Type = TransactionTypes.REVERSED_TRANSACTION;
                        }
                        else
                        {
                            var phone = phones.FirstOrDefault(y =>
                                y.Key == int.Parse(transaction.Counterparty.Split(":").Last()));

                            savingTransaction.Counterparty = phone is object ? phone.Value : "";
                            savingTransaction.Type = TransactionTypes.MONEY_TRANSFER;
                        }
                        savingTransaction.RecipientName = transaction.Creditor?.Name;
                        savingTransaction.DebtorName = transaction.Debtor?.Name;
                        savingTransaction.To = savingTransaction.Counterparty;
                        savingTransaction.From = savingAccount.AccountNumber;
                        savingTransaction.Note = transaction.Note;
                    }

                    else if (transaction.TypeId == 999)
                    {
                        savingTransaction.Type = TransactionTypes.ADVANCE;
                    }

                    else if (transaction.TypeId == 998)
                    {
                        savingTransaction.Type = TransactionTypes.REPAYMENT;
                    }

                    else if (transaction.TypeId == 997)
                    {
                        savingTransaction.Type = TransactionTypes.B9_BONUS;
                        savingTransaction.From = "B9 Organization";
                    }

                    else if (transaction.TypeId == 996)
                    {
                        savingTransaction.To = savingAccount.AccountNumber;
                        savingTransaction.From = null;
                    }

                    else if (transaction.TypeId == 995)
                    {
                        savingTransaction.To = savingAccount.AccountNumber;
                        savingTransaction.From = null;
                    }

                    else if (transaction.TypeId == 994)
                    {
                        savingTransaction.To = null;
                        savingTransaction.From = savingAccount.AccountNumber;
                    }

                    else if (transaction.TypeId == 993)
                    {
                        savingTransaction.To = null;
                        savingTransaction.From = savingAccount.AccountNumber;
                    }

                    else if (transaction.TypeId == 992)
                    {
                        savingTransaction.To = null;
                        savingTransaction.From = savingAccount.AccountNumber;
                    }

                    else if (transaction.TypeId == 991)
                    {
                        savingTransaction.To = null;
                        savingTransaction.From = savingAccount.AccountNumber;
                    }

                    else if (transaction.TypeId == 990)
                    {
                        savingTransaction.To = null;
                        savingTransaction.From = transaction.Debtor.Identifier;
                    }

                    else if (transaction.TypeId == 7 || transaction.TypeId == 19)
                    {
                        var isCredit = transaction.Direction == TransferDirection.Credit;
                        savingTransaction.Type = TransactionTypes.B9_CARD;
                        savingTransaction.To = isCredit ? null : transaction.Type;
                        savingTransaction.From = isCredit ? transaction.Type : null;
                    }

                    else if ((transaction.TypeId == 105 || transaction.TypeId == 52) && transaction.Type != null && transaction.Type.Equals(TransactionTypes.EXTERNAL_CARD))
                    {
                        var isCredit = transaction.Direction == TransferDirection.Credit;
                        savingTransaction.Type = isCredit ? TransactionTypes.TOP_UP_WALLET : TransactionTypes.PUSH_TO_EXTERNAL_CARD;
                        savingTransaction.To = isCredit ? savingAccount.AccountNumber : GetCardNumber(transaction.Creditor.Identifier, cards);
                        savingTransaction.From = isCredit ? GetCardNumber(transaction.Debtor.Identifier, cards) : savingAccount.AccountNumber;
                    }

                    // Check cashing
                    else if (transaction.TypeId == 116)
                    {
                        savingTransaction.RecipientName = transaction.Creditor?.Name;
                        savingTransaction.Address = ExtractCreditorAddress(transaction);
                        savingTransaction.To = savingAccount.AccountNumber;
                    }

                    else if (transaction.TypeId != 6) //other(ACH)
                    {
                        var numbers = transaction.Counterparty?.Split('/');
                        savingTransaction.Counterparty = numbers?.LastOrDefault();
                        savingTransaction.RecipientName = transaction.Creditor?.Name;
                        savingTransaction.DebtorName = transaction.Debtor?.Name;
                        savingTransaction.Address = ExtractCreditorAddress(transaction);
                        savingTransaction.To = savingTransaction.Counterparty;
                        savingTransaction.From = savingAccount.AccountNumber;
                        savingTransaction.ToRoutingNumber = numbers?.ElementAtOrDefault(2);
                        savingTransaction.Note = transaction.Note;
                    }

                    transactionsList.Add(savingTransaction);
                }
                catch (Exception ex)
                {
                    _logger.LogWarning(ex, "Failed to convert Mbanq transfer Id = {TransactionId} for user {CurrentUserId}. " +
                                               "Date: {TransactionDate}. Transaction type: {TransactionTypeId} ({TransactionType}).",
                                               transaction.Id,
                                               CurrentUser.UserId,
                                               transaction.CreatedAt,
                                               transaction.TypeId,
                                               transaction.Type);
                }
            }

            return new PaginatedList<SavingsTransaction>(transactionsList, transactions.TotalCount, transactions.PageNumber, request.Take);
        }

        private string GetCardNumber(string identifier, IEnumerable<ExternalCardListItem> cards)
        {
            return cards.Where(x => x.ExternalId.ToString() == identifier[13..]).FirstOrDefault()?.Last4Digits;
        }

        private async Task<IEnumerable<ExternalCardListItem>> FetchCards(IEnumerable<string> cardsIds, string ipAddress)
        {
            if (cardsIds == null || !cardsIds.Any())
            {
                return new List<ExternalCardListItem>();
            }

            var cards = await _bankClientExternalCardsService.GetCards(CurrentUser.UserMbanqToken, ipAddress, cardsIds.Select(x => int.Parse(x)));

            return cards;
        }

        private SavingsTransaction GetSavingTransactionDetails(SavingsTransactionItem transaction)
        {
            return new SavingsTransaction
            {
                Id = transaction.Id,
                Type = transaction.Type,
                Date = transaction.CreatedAt,
                Succeeded = !transaction.Reversed,
                RunningBalance = transaction.RunningBalance,
                Amount = transaction.Amount,
                Direction = transaction.Direction,
                Counterparty = transaction.Counterparty
            };
        }

        private static string ExtractCreditorAddress(SavingsTransactionItem transaction) => transaction.Creditor?.Address?.FirstOrDefault();
    }
}
