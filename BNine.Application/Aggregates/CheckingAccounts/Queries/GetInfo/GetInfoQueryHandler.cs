﻿namespace BNine.Application.Aggregates.CheckingAccounts.Queries.GetInfo
{
    using System.Threading;
    using System.Threading.Tasks;
    using AutoMapper;
    using BNine.Application.Abstractions;
    using BNine.Application.Aggregates.CheckingAccounts.Models;
    using BNine.Application.Exceptions;
    using BNine.Application.Interfaces;
    using BNine.Constants;
    using Interfaces.Bank.Administration;
    using MediatR;
    using Microsoft.EntityFrameworkCore;

    public class GetInfoQueryHandler
        : AbstractRequestHandler, IRequestHandler<GetInfoQuery, CheckingAccountInfo>
    {
        private readonly IBankSavingAccountsService BankAccountService;

        public GetInfoQueryHandler(IMediator mediator, IBNineDbContext dbContext, IMapper mapper, ICurrentUserService currentUser, IBankSavingAccountsService bankAccountService)
            : base(mediator, dbContext, mapper, currentUser)
        {
            BankAccountService = bankAccountService;
        }

        public async Task<CheckingAccountInfo> Handle(GetInfoQuery request, CancellationToken cancellationToken)
        {
            var user = await DbContext.Users
                .Include(x => x.CurrentAccount)
                .AsNoTracking()
                .FirstOrDefaultAsync(x => x.Id == CurrentUser.UserId, cancellationToken);

            if (user?.CurrentAccount == null)
            {
                throw new NotFoundException(nameof(user.CurrentAccount), CurrentUser.UserId);
            }

            var info = await BankAccountService.GetSavingAccountInfo(user.CurrentAccount.ExternalId);

            return new CheckingAccountInfo
            {
                AccountNumber = info.AccountNumber,
                RoutingNumber = BankInformation.RoutingNumber,
                OwnerFirstName = user.FirstName,
                OwnerLastName = user.LastName,
                OwnerAddress = new CheckingAccountOwnerAddress
                {
                    ZipCode = user.Address.ZipCode,
                    State = user.Address.State,
                    City = user.Address.City,
                    Address = user.Address.AddressLine,
                    Unit = user.Address.Unit
                },
                BankAddress = BankInformation.BankAddress
            };
        }
    }
}
