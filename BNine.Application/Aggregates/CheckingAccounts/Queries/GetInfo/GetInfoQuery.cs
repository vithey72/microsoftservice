﻿namespace BNine.Application.Aggregates.CheckingAccounts.Queries.GetInfo
{
    using BNine.Application.Aggregates.CheckingAccounts.Models;
    using MediatR;

    public class GetInfoQuery : IRequest<CheckingAccountInfo>
    {
    }
}
