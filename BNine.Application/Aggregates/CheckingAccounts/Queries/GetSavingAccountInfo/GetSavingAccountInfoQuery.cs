﻿namespace BNine.Application.Aggregates.CheckingAccounts.Queries.GetSavingAccountInfo
{
    using System;
    using MediatR;
    using Newtonsoft.Json;

    public class GetSavingAccountInfoQuery : IRequest<SavingAccountInfo>
    {
        public GetSavingAccountInfoQuery(Guid userId)
        {
            UserId = userId;
        }

        public Guid UserId
        {
            get; set;
        }
    }

    public class SavingAccountInfo
    {
        [JsonProperty("availableBalance")]
        public decimal AvailableBalance
        {
            get; set;
        }

        [JsonProperty("accountBalance")]
        public decimal AccountBalance
        {
            get; set;
        }

        [JsonProperty("isBlocked")]
        public bool IsBlocked
        {
            get; set;
        }
    }
}
