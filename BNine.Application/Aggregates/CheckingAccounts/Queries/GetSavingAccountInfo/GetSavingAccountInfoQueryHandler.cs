﻿namespace BNine.Application.Aggregates.CheckingAccounts.Queries.GetSavingAccountInfo
{
    using System.Linq;
    using System.Threading;
    using System.Threading.Tasks;
    using AutoMapper;
    using BNine.Application.Abstractions;
    using BNine.Application.Exceptions;
    using BNine.Application.Interfaces;
    using BNine.Application.Interfaces.Bank.Administration;
    using BNine.Domain.Entities.BankAccount;
    using MediatR;
    using Microsoft.EntityFrameworkCore;

    public class GetSavingAccountInfoQueryHandler
        : AbstractRequestHandler, IRequestHandler<GetSavingAccountInfoQuery, SavingAccountInfo>
    {
        private IBankSavingAccountsService BankSavingAccountsService
        {
            get;
        }

        public GetSavingAccountInfoQueryHandler(IBankSavingAccountsService bankSavingAccountsService, IMediator mediator, IBNineDbContext dbContext, IMapper mapper, ICurrentUserService currentUser) : base(mediator, dbContext, mapper, currentUser)
        {
            BankSavingAccountsService = bankSavingAccountsService;
        }

        public async Task<SavingAccountInfo> Handle(GetSavingAccountInfoQuery request, CancellationToken cancellationToken)
        {
            var savingAccount = await DbContext.Users
                .Include(x => x.CurrentAccount)
                .Where(x => x.Id == request.UserId)
                .FirstOrDefaultAsync(cancellationToken);

            if (savingAccount?.CurrentAccount?.ExternalId == null)
            {
                throw new NotFoundException(nameof(CurrentAccount), new
                {
                    userId = request.UserId
                });
            }

            var externalSavingAccount = await BankSavingAccountsService.GetSavingAccountInfo(savingAccount.CurrentAccount.ExternalId);

            if (externalSavingAccount == null)
            {
                throw new NotFoundException(nameof(CurrentAccount), new
                {
                    userId = request.UserId
                });
            }

            return new SavingAccountInfo
            {
                AvailableBalance = externalSavingAccount.AvailableBalance,
                AccountBalance = externalSavingAccount.AccountBalance,
                IsBlocked = externalSavingAccount.IsBlocked
            };
        }
    }
}
