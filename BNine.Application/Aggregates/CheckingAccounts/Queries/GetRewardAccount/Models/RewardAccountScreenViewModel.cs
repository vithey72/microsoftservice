﻿namespace BNine.Application.Aggregates.CheckingAccounts.Queries.GetRewardAccount.Models;

using CheckingAccounts.Models;
using CommonModels.Buttons;
using CommonModels.DialogElements;

public class RewardAccountScreenViewModel
{
    public AccountBalanceAdvancedWithButton Balance
    {
        get;
        set;
    }

    public RewardsHistory RewardsHistory
    {
        get;
        set;
    }

    public List<ButtonWithIconViewModel> ReferralPrograms
    {
        get;
        set;
    }

    public ButtonWithIconViewModel Cashback
    {
        get;
        set;
    }

    public About About
    {
        get;
        set;
    }

}

public class RewardsHistory
{
    public SectionViewModelWithImageUrl Header
    {
        get;
        set;
    }

    public RewardsHistoryBody Body
    {
        get;
        set;
    }

    public class RewardsHistoryBody
    {
        public RewardsHistoryBodyItem CurrentMonth
        {
            get;
            set;
        }

        public RewardsHistoryBodyItem Lifetime
        {
            get;
            set;
        }

        public ButtonViewModel Button
        {
            get;
            set;
        }

        public class RewardsHistoryBodyItem
        {
            public string Currency
            {
                get; set;
            }

            public decimal Amount
            {
                get; set;
            }

            public string Title
            {
                get;
                set;
            }
        }
    }

}

public class About
{
    public SectionViewModelWithImageUrl Header
    {
        get;
        set;
    }

    public SectionViewModelWithBulletPoints Body
    {
        get;
        set;
    }

}
