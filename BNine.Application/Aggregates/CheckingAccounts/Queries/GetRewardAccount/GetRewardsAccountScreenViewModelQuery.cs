﻿namespace BNine.Application.Aggregates.CheckingAccounts.Queries.GetRewardAccount;

using MediatR;
using Models;

public record GetRewardsAccountScreenViewModelQuery(string Ip, Guid UserId, string MbanqToken) : IRequest<RewardAccountScreenViewModel>;

