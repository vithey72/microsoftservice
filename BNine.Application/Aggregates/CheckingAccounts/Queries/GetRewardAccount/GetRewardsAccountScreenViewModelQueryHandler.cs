﻿namespace BNine.Application.Aggregates.CheckingAccounts.Queries.GetRewardAccount;

using CheckingAccounts.Models;
using Common;
using CommonModels.Buttons;
using CommonModels.DialogElements;
using Configuration.Queries.GetConfiguration;
using Constants;
using Enums;
using Enums.Transfers;
using Exceptions;
using GetBalance;
using GetBalance.Models;
using Interfaces;
using Interfaces.Bank.Administration;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Models;
using Newtonsoft.Json;

public class GetRewardsAccountScreenViewModelQueryHandler : IRequestHandler<GetRewardsAccountScreenViewModelQuery, RewardAccountScreenViewModel>
{
    private readonly IMediator _mediator;
    private readonly IBankSavingAccountsService _bankSavingAccountsService;
    private readonly Interfaces.Bank.Client.IBankSavingsTransactionsService _bankSavingsTransactionsService;
    private readonly IBNineDbContext _dbContext;
    private readonly IDateTimeUtcProviderService _dateTimeProviderService;

    public GetRewardsAccountScreenViewModelQueryHandler(
        IMediator mediator,
        IBankSavingAccountsService bankSavingAccountsService,
        Interfaces.Bank.Client.IBankSavingsTransactionsService  bankSavingsTransactionsService,
        IBNineDbContext dbContext,
        IDateTimeUtcProviderService dateTimeProviderService
    )
    {
        _mediator = mediator;
        _bankSavingAccountsService = bankSavingAccountsService;
        _bankSavingsTransactionsService = bankSavingsTransactionsService;
        _dbContext = dbContext;
        _dateTimeProviderService = dateTimeProviderService;
    }


    public async Task<RewardAccountScreenViewModel> Handle(GetRewardsAccountScreenViewModelQuery request,
        CancellationToken cancellationToken)
    {

        var isFeatureEnabled = (await _mediator.Send(new GetConfigurationQuery(), cancellationToken))
            .HasFeatureEnabled(FeaturesNames.Features.CurrentAdditionalAccountsExternal);

        var rewardAccount =
            (await _dbContext.Users.Include(u => u.CurrentAdditionalAccounts)
                .Where(u => u.Id == request.UserId)
                .Select(u => u.CurrentAdditionalAccounts)
                .FirstOrDefaultAsync(cancellationToken))
            .FirstOrDefault(c => c.AccountType == CurrentAdditionalAccountEnum.Reward);

        if (rewardAccount == null)
        {
            throw new NotFoundException($"Reward account for user with Id {request.UserId} was not found");
        }

        var rewardAccBalance = await GetRewardAccountBalance(request, cancellationToken);

        var resp = new RewardAccountScreenViewModel()
        {
            Balance = rewardAccBalance,
            RewardsHistory = new RewardsHistory()
            {
                Header = new SectionViewModelWithImageUrl()
                {
                    Title = "Rewards Earned History",
                    ImageUrl = "https://b9prodaccount.blob.core.windows.net/static-documents-container/Misc/rewards_earned_history_icon.png"
                },
                Body = await GetRewardsHistoryBody(request, rewardAccount.ExternalId, isFeatureEnabled)
            },
            ReferralPrograms = new List<ButtonWithIconViewModel>()
            {
               new ButtonWithIconViewModel(Title: "Invite\na friend", Subtitle: "$5 for you \n$5 for your friend",
                   IconUrl: "https://b9prodaccount.blob.core.windows.net/static-documents-container/Misc/rewards_invite_a_friend_icon.png")
                   .WithDeeplink(DeepLinkRoutes.Bonus),
               new ButtonWithIconViewModel(Title:"My Rewards\nActivation", Subtitle:"Activate your\nreward instantly!",
                   IconUrl:"https://b9prodaccount.blob.core.windows.net/static-documents-container/Misc/rewards_my_rewards_activation_icon.png")
                   .WithDeeplink(DeepLinkRoutes.RewardsActivation),
            },
            Cashback = new ButtonWithIconViewModel("Cashback", "Manage your\ncategories for\nmaximum cashback",
                "https://b9prodaccount.blob.core.windows.net/static-documents-container/Misc/rewards_cashback_icon.png")
                .WithDeeplink(DeepLinkRoutes.CashbackProgram),
            About = new About()
            {
                Header = new SectionViewModelWithImageUrl()
                {
                    Title = "About the B9 Rewards Program",
                    ImageUrl = "https://b9prodaccount.blob.core.windows.net/static-documents-container/Misc/rewards_about_b9_rewards_icon.png"
                },
                Body = new SectionViewModelWithBulletPoints()
                {
                    Title = "The B9 Rewards program allows you to earn rewards and rebate fees on a variety of services and products.",
                    Subtitle = "Your Rewards account will include all of the above plus the ability to transfer your accumulated funds to your main account at any time.",
                    ImageUrl = "https://b9prodaccount.blob.core.windows.net/static-documents-container/Misc/rewards_about_b9_rewards_body_icon.png",
                    BulletPoints = new List<BulletPointViewModelWithColor>()
                    {
                        new BulletPointViewModelWithColor("Cash Withdrawal Fees")
                            .WithKnownType(BulletPointTypeEnum.BulletPoint).WithHexColor(BulletPointElementColor.Black),
                        new BulletPointViewModelWithColor("Subscription Fees")
                            .WithKnownType(BulletPointTypeEnum.BulletPoint).WithHexColor(BulletPointElementColor.Black),
                        new BulletPointViewModelWithColor("Cashback")
                            .WithKnownType(BulletPointTypeEnum.BulletPoint).WithHexColor(BulletPointElementColor.Black),
                        new BulletPointViewModelWithColor("Friend Invite Bonuses")
                            .WithKnownType(BulletPointTypeEnum.BulletPoint).WithHexColor(BulletPointElementColor.Black),
                    }
                }
            }

        };

        return resp;
    }

    private async Task<AccountBalanceAdvancedWithButton> GetRewardAccountBalance(
        GetRewardsAccountScreenViewModelQuery request,
        CancellationToken cancellationToken)
    {
        var rewardAccBalanceQuery =
            new GetBalanceAdvancedQuery(
            new GetBalanceAdvancedRequest()
            {
                AccountsToGet =
                    new List<BankAccountTypeEnum>() { BankAccountTypeEnum.Reward }
            },
            request.Ip,
            request.UserId,
            request.MbanqToken);

        var rewardAccBalanceResponse = await _mediator.Send(rewardAccBalanceQuery, cancellationToken);

        if (rewardAccBalanceResponse.Count != 1)
        {
            throw new ArgumentException($"Reward account balance query returned the wrong number of accounts for {request.UserId}");
        }

        var balanceSection = ConvertFromBase(rewardAccBalanceResponse.Single());

        balanceSection.Button = new ButtonWithIconExtendedViewModel("Redeem for cash", null,
            "https://b9prodaccount.blob.core.windows.net/static-documents-container/Misc/redeem_button_arrow_up_icon.png",
            ButtonStyleEnum.Solid).WithDeeplink(DeepLinkRoutes.RewardsRedeem);

        return balanceSection;
    }

    private async Task<RewardsHistory.RewardsHistoryBody> GetRewardsHistoryBody(GetRewardsAccountScreenViewModelQuery request, int savingId, bool isFeatureEnabled = false)
    {
        var now = _dateTimeProviderService.UtcNow();
        var savingAcc = await _bankSavingAccountsService.GetSavingAccountInfo(savingId);
        var endDate = TimeZoneHelper.UtcToEasternStandartTime(now);
        var lastMonthTotal = await GetLastMonthTotal(savingId, request.MbanqToken, request.Ip,  now, endDate);
        var buttonArgs = isFeatureEnabled ? new { Filter = "rewards" } : null;
        return new RewardsHistory.RewardsHistoryBody()
        {
            CurrentMonth = new RewardsHistory.RewardsHistoryBody.RewardsHistoryBodyItem(){ Amount = lastMonthTotal, Title  = "Current month", Currency = savingAcc.Currency},
            Lifetime = new RewardsHistory.RewardsHistoryBody.RewardsHistoryBodyItem(){ Amount = savingAcc.TotalDeposits, Title  = "Total (lifetime)", Currency = savingAcc.Currency},
            Button = new ButtonViewModel("Show my B9 Rewards history", ButtonStyleEnum.Solid)
                .WithDeeplink(DeepLinkRoutes.TransfersHistory, buttonArgs)
        };
    }

    private async Task<decimal> GetLastMonthTotal(int savingId, string mbanqToken, string ip, DateTime now, DateTime endDate)
    {
        var lastMonthDeposits = 0m;
        var page = 1;
        // Mbanq sets no upper bound for how many transactions can be queried at once.
        // I think this number should be enough to get all transactions for a month and not to visit the next page
        // to reduce network round-trips.
        var limit = 1000;
        bool hasMore;
        do
        {
            var resp = await _bankSavingsTransactionsService
                .GetTransactions(savingId, start: page, limit: limit, from: new DateTime(now.Year, now.Month, 1), to: endDate,
                    mbanqToken: mbanqToken, ip: ip);
            lastMonthDeposits += resp.Items.Where(st => st.Direction == TransferDirection.Credit).Sum(i => i.Amount);
            hasMore = resp.HasNextPage;
            page+=1;

        } while(hasMore);

        return lastMonthDeposits;
    }

    private static AccountBalanceAdvancedWithButton ConvertFromBase(AccountBalanceAdvanced balance)
    {
        var rewardsAccBalanceResult = JsonConvert.SerializeObject(balance);
        return JsonConvert.DeserializeObject<AccountBalanceAdvancedWithButton>(rewardsAccBalanceResult);
    }
}
