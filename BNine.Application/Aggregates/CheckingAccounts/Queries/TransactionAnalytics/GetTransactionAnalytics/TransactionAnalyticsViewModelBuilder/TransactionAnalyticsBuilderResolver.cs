﻿namespace BNine.Application.Aggregates.CheckingAccounts.Queries.TransactionAnalytics;

using Domain.Entities;
using Interfaces;
using Interfaces.TransactionHistory;
using MediatR;

public class TransactionAnalyticsBuilderResolver : ITransactionAnalyticsBuilderResolver
{
    private readonly IMediator _mediator;
    private readonly IBNineDbContext _bNineDbContext;
    private readonly ICurrentUserService _currentUserService;
    private readonly ITransfersMappingService _transfersMappingService;
    private readonly IPartnerProviderService _partnerProviderService;

    public TransactionAnalyticsBuilderResolver(
        IMediator mediator, IBNineDbContext bNineDbContext,
        ICurrentUserService currentUserService,
        ITransfersMappingService transfersMappingService,
        IPartnerProviderService partnerProviderService)
    {
        _mediator = mediator;
        _bNineDbContext = bNineDbContext;
        _currentUserService = currentUserService;
        _transfersMappingService = transfersMappingService;
        _partnerProviderService = partnerProviderService;
    }

    public TransactionAnalyticsBuilderBase ResolveTransactionAnalyticsBuilder(
        TransactionAnalyticsCategory selectedCategory, GetTransactionAnalyticsQuery request)
    {
        // Low level subcategory level, no nesting level, contains transaction list,showing on the last screen of the analytics
        if (selectedCategory.Key == TransactionCategoryConstants.OthersCategoryKey ||
            ((selectedCategory.MccCodes?.Any() == true || selectedCategory.TransferDisplayTypes?.Any() == true)
            && selectedCategory.ChildCategories?.Any() == false))
        {
            return new TerminalCategoryTransactionAnalyticsBuilder(request, _mediator, _bNineDbContext, _currentUserService, _transfersMappingService);
        }

        // Subcategory level, 1 nesting level, showing on the second screen of the analytics
        if (selectedCategory.ParentCategory != null && selectedCategory.ChildCategories?.Any() == true)
        {
            return new NodeCategoryTransactionAnalyticsBuilder(request, _mediator, _bNineDbContext, _currentUserService, _transfersMappingService);
        }

        // Root level category, 2 nesting levels, showing on the first screen of the analytics, key is "default"
        if (selectedCategory.ParentCategory == null && selectedCategory.ChildCategories?.Any() == true)
        {
            return new RootCategoryTransactionAnalyticsBuilder(request, _mediator, _bNineDbContext, _currentUserService, _transfersMappingService, _partnerProviderService);
        }

        throw new ArgumentException("Category is not supported");
    }
}
