﻿namespace BNine.Application.Aggregates.CheckingAccounts.Queries.TransactionAnalytics;

using Domain.Entities;
using Enums.Transfers;
using Interfaces;
using Interfaces.TransactionHistory;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Models;
using Models.TransactionAnalytics;

public class NodeCategoryTransactionAnalyticsBuilder : TransactionAnalyticsBuilderBase
{
    private readonly GetTransactionAnalyticsQuery _request;

    public NodeCategoryTransactionAnalyticsBuilder(GetTransactionAnalyticsQuery request,
        IMediator mediator, IBNineDbContext bNineDbContext,
        ICurrentUserService currentUserService,
        ITransfersMappingService transfersMappingService) : base(mediator,
        bNineDbContext, currentUserService, transfersMappingService)
    {
        _request = request;
    }

    // Colors for 10 subcategories to be displayed on Subcategories screen
    private static readonly string[] CardTransactionsSubcategoriesColors = new string[]
    {
        "FF0000", "FAE24C", "F5C142", "ED8934", "EF723E", "EA3323", "B42525", "C85425", "CF55B4",
    };

    private static readonly string OthersSubcategoryColor = "EA38C3";

    private static readonly string[] TransfersSubcategoriesColors = new string[]
    {
        "8BC446", "377D22", "B0ED4F"
    };

    protected override async Task BuildSubcategories(TransactionAnalyticsCategory selectedCategory,
        GetTransactionAnalyticsQuery getTransactionAnalyticsQuery)
    {
        var transactionList = await GetParentCategoryTransactions(selectedCategory);

        var processedTransactionsIds = new List<int>();

        foreach (var childCategory in selectedCategory.ChildCategories)
        {
            List<BankTransferViewModel> categoryTransactions;
            if (selectedCategory.Key == TransactionCategoryConstants.TransfersCategoryKey)
            {
                categoryTransactions =
                    transactionList.Where(x =>
                            childCategory.TransferDisplayTypes.Contains(x.SavingsTransferType)
                            || childCategory.TransferDisplayTypes.Contains(x.PendingTransferType.ConvertToTransferDisplayType()))
                        .ToList();
            }
            else
            {
                categoryTransactions =
                    transactionList.Where(x => x.MccCode.HasValue && childCategory.MccCodes.Contains(x.MccCode.Value))
                        .ToList();
            }

            var viewItem = new TransactionAnalyticsListItem()
            {
                Key = childCategory.Key,
                IconUrl = selectedCategory.IconUrl,
                Title = childCategory.Title,
                Subtitle = $"{categoryTransactions.Count} operations",
                TotalAmount = categoryTransactions.Sum(x => Math.Abs(x.AmountSum)),
                TotalOperations = categoryTransactions.Count
            };

            if (viewItem.TotalOperations > 0)
            {
                ResultObject.SubCategories.Add(viewItem);
            }

            processedTransactionsIds.AddRange(categoryTransactions.Select(x => x.Id));
        }

        if (selectedCategory.Key == TransactionCategoryConstants.CardTransactionsCategoryKey)
        {
            AddOthersSubcategory(selectedCategory, transactionList, processedTransactionsIds);
        }
    }

    private async Task<List<BankTransferViewModel>> GetParentCategoryTransactions(TransactionAnalyticsCategory selectedCategory)
    {
        List<BankTransferViewModel> transactionList;
        switch (selectedCategory.Key)
        {
            case TransactionCategoryConstants.TransfersCategoryKey:
                {
                    var transferDisplayTypes = selectedCategory.ChildCategories.SelectMany(x => x.TransferDisplayTypes).ToList();

                    transactionList = await base.GetTransactionList(_request, transferDisplayTypes);
                    break;
                }
            case TransactionCategoryConstants.CardTransactionsCategoryKey:
                {
                    // Ignore ATM Mcc codes
                    var atmCategory =
                        await BNineDbContext.TransactionCategories.FirstOrDefaultAsync(x => x.Key == TransactionCategoryConstants.AtmCategoryKey);

                    var fullTransactionList = await base.GetTransactionList(_request);

                    transactionList = fullTransactionList
                        .Where(x => x.MccCode.HasValue && !atmCategory.MccCodes.Contains(x.MccCode.Value))
                        .ToList();
                    break;
                }
            case TransactionCategoryConstants.AtmCategoryKey:
                {
                    var fullTransactionList = await base.GetTransactionList(_request);
                    transactionList = fullTransactionList
                        .Where(x => x.MccCode.HasValue && !selectedCategory.MccCodes.Contains(x.MccCode.Value)).ToList();
                    break;
                }
            default:
                throw new Exception($"Root category key: {selectedCategory.Key} is not supported");
        }

        return transactionList;
    }

    /// <summary>
    /// Only applicable for Card Transactions
    /// Filled by transactions with Mcc codes that unknown to the B9 codes list
    /// </summary>
    private void AddOthersSubcategory(TransactionAnalyticsCategory selectedCategory, List<BankTransferViewModel> transactionList,
        List<int> processedTransactionsIds)
    {
        var nonProceededTransactions = transactionList.Where(x =>
            !processedTransactionsIds.Contains(x.Id)).ToList();

        var otherCategoryTotalOperations = nonProceededTransactions.Count;

        if (otherCategoryTotalOperations > 0)
        {
            var othersCategory = selectedCategory.ChildCategories.FirstOrDefault(x =>
                x.Key == TransactionCategoryConstants.OthersCategoryKey);

            ResultObject.SubCategories.Add(new TransactionAnalyticsListItem()
            {
                Key = othersCategory.Key,
                Title = othersCategory.Title,
                Subtitle = $"{otherCategoryTotalOperations} operations",
                IconUrl = selectedCategory.IconUrl,
                TotalAmount = nonProceededTransactions.Sum(x => Math.Abs(x.AmountSum)),
                TotalOperations = otherCategoryTotalOperations
            });
        }
    }

    protected override void GroupAndSortSubcategories(TransactionAnalyticsCategory selectedCategory)
    {
        ResultObject.SubCategories = ResultObject.SubCategories.Where(x => x.TotalOperations > 0).ToList();

        var orderedSubcategories = ResultObject.SubCategories
            .OrderBy(x => x.Key == TransactionCategoryConstants.OthersCategoryKey ? 1 : 0)
            .ThenByDescending(x => x.TotalAmount);

        var first9Categories = orderedSubcategories.Take(9).ToList();

        foreach (var category in first9Categories)
        {
            var index = first9Categories.IndexOf(category);

            if (selectedCategory.Key == TransactionCategoryConstants.TransfersCategoryKey)
            {
                if (index <= TransfersSubcategoriesColors.Length - 1)
                {
                    category.Color = TransfersSubcategoriesColors[first9Categories.IndexOf(category)];
                }
            }
            else
            {
                category.Color = CardTransactionsSubcategoriesColors[first9Categories.IndexOf(category)];
            }
        }

        var theRestCategories = orderedSubcategories.Skip(9);

        foreach (var category in theRestCategories)
        {
            category.Color = OthersSubcategoryColor;
        }

        var resultSubcategories = new List<TransactionAnalyticsListItem>();

        resultSubcategories.AddRange(first9Categories);
        resultSubcategories.AddRange(theRestCategories);

        ResultObject.SubCategories = resultSubcategories;
    }

    protected override Task BuildSelectedCategory(TransactionAnalyticsCategory selectedCategory, GetTransactionAnalyticsQuery request)
    {
        if (selectedCategory.Key == TransactionCategoryConstants.CardTransactionsCategoryKey)
        {
            ResultObject.SubCategoriesTitle = "Top spending categories";
        }

        return base.BuildSelectedCategory(selectedCategory, request);
    }
}
