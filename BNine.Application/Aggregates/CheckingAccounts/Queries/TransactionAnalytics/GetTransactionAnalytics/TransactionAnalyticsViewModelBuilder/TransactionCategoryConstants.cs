﻿namespace BNine.Application.Aggregates.CheckingAccounts.Queries.TransactionAnalytics;

public static class TransactionCategoryConstants
{
    public const string CardTransactionsCategoryKey = "card-transactions";
    public const string TransfersCategoryKey = "transfers";
    public const string AtmCategoryKey = "atm";
    public const string DefaultCategoryKey = "default";
    public const string OthersCategoryKey = "others";

    public const char OthersCategoryKeySeparator = '_';
}
