﻿namespace BNine.Application.Aggregates.CheckingAccounts.Queries.TransactionAnalytics;

using Application.Models;
using Domain.Entities;
using Enums.Transfers;
using Interfaces;
using Interfaces.TransactionHistory;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Models;
using Models.TransactionAnalytics;

public class TerminalCategoryTransactionAnalyticsBuilder : TransactionAnalyticsBuilderBase
{
    private readonly GetTransactionAnalyticsQuery _request;

    private const string IncomingTransactionColor = "000000";
    private const string OutgoingTransactionColor = "14273A";

    public TerminalCategoryTransactionAnalyticsBuilder(GetTransactionAnalyticsQuery request,
        IMediator mediator, IBNineDbContext bNineDbContext,
        ICurrentUserService currentUserService,
        ITransfersMappingService transfersMappingService) : base(mediator,
        bNineDbContext, currentUserService, transfersMappingService)
    {
        _request = request;
    }

    protected override async Task BuildSubcategories(TransactionAnalyticsCategory selectedCategory,
        GetTransactionAnalyticsQuery getTransactionAnalyticsQuery)
    {
        var transactionList = await GetTransactionList(selectedCategory);

        transactionList = transactionList.OrderByDescending(x => x.TimeStampUtc);

        foreach (var transaction in transactionList)
        {
            var viewItem = MapTransactionToViewModel(transaction);

            ResultObject.SubCategories.Add(viewItem);
        }
    }

    private static TransactionAnalyticsListItem MapTransactionToViewModel(BankTransferViewModel transaction)
    {
        //Transaction amount should be negative
        var amountSum = transaction.AmountSum > 0 ? transaction.AmountSum * -1 : transaction.AmountSum;

        //Leaves only two digits after the dot
        var amountString = $"${Math.Abs(transaction.AmountSum):0.00}";

        var viewItem = new TransactionAnalyticsListItem()
        {
            TransactionId = transaction.Id,
            TransactionStatusLine = transaction.StatusLine,
            TransactionStatus = transaction.Status,
            IconUrl = transaction.IconUrl,
            Subtitle = transaction.Subtitle,
            Title = transaction.Title,
            TotalAmount = amountSum,
            AmountString = amountString,
            Direction = transaction.AmountDirection,
            AmountStringColor = transaction.AmountDirection == TransferDirectionSimplified.Incoming
                ? IncomingTransactionColor
                : OutgoingTransactionColor,
            TimeStampUtc = transaction.TimeStampUtc,
            TransactionHintText = transaction.HintText
        };
        return viewItem;
    }

    private async Task<IEnumerable<BankTransferViewModel>> GetTransactionList(TransactionAnalyticsCategory selectedCategory)
    {
        IEnumerable<BankTransferViewModel> transactionList;

        if (selectedCategory.Key == TransactionCategoryConstants.OthersCategoryKey)
        {
            var fullTransactionList = await base.GetTransactionList(_request);

            // Ignore card transactions Mcc codes
            var cardTransactionsCategory = await BNineDbContext.TransactionCategories
                .Include(x => x.ChildCategories)
                .FirstOrDefaultAsync(x =>
                x.Key == TransactionCategoryConstants.CardTransactionsCategoryKey);

            var cardTransactionsMccCodes = cardTransactionsCategory.ChildCategories.SelectMany(x => x.MccCodes).ToArray();

            // Ignore ATM Mcc codes
            var atmCategory = await BNineDbContext.TransactionCategories.FirstOrDefaultAsync(x =>
                    x.Key == TransactionCategoryConstants.AtmCategoryKey);

            //Get all transactions other than card transactions
            transactionList = fullTransactionList.Where(x =>
                x.MccCode.HasValue && !cardTransactionsMccCodes.Contains(x.MccCode.Value) &&
                !atmCategory.MccCodes.Contains(x.MccCode.Value));

            return transactionList;
        }

        switch (selectedCategory.ParentCategory.Key)
        {
            case TransactionCategoryConstants.TransfersCategoryKey:
                transactionList = await base.GetTransactionList(_request, selectedCategory.TransferDisplayTypes);
                break;
            default:
                {
                    var fullTransactionList = await base.GetTransactionList(_request);

                    transactionList = fullTransactionList.Where(x =>
                        x.MccCode.HasValue && selectedCategory.MccCodes.Contains(x.MccCode.Value));
                    break;
                }
        }

        return transactionList;
    }

    public override async Task<TransactionAnalyticsViewModel> Build(TransactionAnalyticsCategory selectedCategory,
        GetTransactionAnalyticsQuery request)
    {
        var result = await base.Build(selectedCategory, request);

        FillTransactionListWithPagination(request);

        return result;
    }

    protected override void BuildCircleDiagram(TransactionAnalyticsCategory selectedCategory)
    {
        var totalAmount = ResultObject.SubCategories.Sum(x => Math.Abs(x.TotalAmount));

        ResultObject.CircleDiagram = new CircleDiagram()
        {
            SubAmounts = new List<CircleDiagram.CategorySubAmount>()
            {
                new CircleDiagram.CategorySubAmount()
                {
                    Amount = totalAmount,
                    Color = selectedCategory.Color
                }
            },
            TotalAmount = totalAmount
        };
    }

    private void FillTransactionListWithPagination(GetTransactionAnalyticsQuery request)
    {
        var subCategories = ResultObject.SubCategories
            .Skip((request.Paging.StartPage - 1) * request.Paging.PageSize)
            .Take(request.Paging.PageSize).ToList();

        var paginatedList = new PaginatedList<TransactionAnalyticsListItem>(subCategories, ResultObject.SubCategories.Count, request.Paging.StartPage,
                request.Paging.PageSize);

        ResultObject.SubCategories = null;

        ResultObject.TransactionList = paginatedList;
    }

    protected override void GroupAndSortSubcategories(TransactionAnalyticsCategory selectedCategory)
    {

    }
}
