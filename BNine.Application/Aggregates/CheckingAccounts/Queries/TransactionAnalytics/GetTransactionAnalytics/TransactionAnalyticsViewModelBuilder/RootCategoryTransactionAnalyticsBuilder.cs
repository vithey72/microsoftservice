﻿namespace BNine.Application.Aggregates.CheckingAccounts.Queries.TransactionAnalytics;

using System.Text.RegularExpressions;
using BNine.Application.Helpers;
using BNine.Enums;
using CommonModels.Buttons;
using CommonModels.Dialog;
using CommonModels.Text;
using Domain.Entities;
using Enums.Transfers;
using Interfaces;
using Interfaces.TransactionHistory;
using MediatR;
using Models;
using Models.TransactionAnalytics;

public class RootCategoryTransactionAnalyticsBuilder : TransactionAnalyticsBuilderBase
{
    private readonly IPartnerProviderService _partnerProviderService;
    private readonly GetTransactionAnalyticsQuery _request;

    public RootCategoryTransactionAnalyticsBuilder(GetTransactionAnalyticsQuery request,
        IMediator mediator, IBNineDbContext bNineDbContext,
        ICurrentUserService currentUserService,
        ITransfersMappingService transfersMappingService, IPartnerProviderService partnerProviderService) : base(mediator,
        bNineDbContext, currentUserService, transfersMappingService)
    {
        _partnerProviderService = partnerProviderService;
        _request = request;
    }

    protected override async Task BuildSubcategories(TransactionAnalyticsCategory selectedCategory,
        GetTransactionAnalyticsQuery getTransactionAnalyticsQuery)
    {
        var partnerName = StringProvider.GetPartnerName(_partnerProviderService);
        var transactionList = await base.GetTransactionList(_request);

        foreach (var childCategory in selectedCategory.ChildCategories)
        {
            var categoryTransactions = GetChildCategoryTransactions(selectedCategory, childCategory, transactionList);

            var viewItem = new TransactionAnalyticsListItem()
            {
                Key = childCategory.Key,
                IconUrl = childCategory.IconUrl,
                Color = childCategory.Color,
                Title = childCategory.Title,
                Subtitle = $"{categoryTransactions.Count} operations",
                TotalAmount = categoryTransactions.Sum(x => Math.Abs(x.AmountSum)),
                TotalOperations = categoryTransactions.Count,
                Hint = BuildHint(childCategory)
            };

            ResultObject.SubCategories.Add(viewItem);
        }
    }

    private GenericDialogRichBodyViewModel BuildHint(TransactionAnalyticsCategory childCategory)
    {
        var partnerName = StringProvider.GetPartnerName(_partnerProviderService);
        var partner = _partnerProviderService.GetPartner();
        if (partner is PartnerApp.Default)
        {
            return new GenericDialogRichBodyViewModel()
            {
                Title = childCategory.Title,
                Buttons = new[] { new ButtonViewModel(childCategory.HintButtonText, ButtonStyleEnum.Solid) },
                Subtitle = new LinkedText(childCategory.HintSubTitle),
                ImageUrl = childCategory.HintIconUrl
            };
        }
        else
        {
            return new GenericDialogRichBodyViewModel()
            {
                Title = childCategory.Title,
                Buttons = new[] { new ButtonViewModel(childCategory.HintButtonText, ButtonStyleEnum.Solid) },
                Subtitle = new LinkedText(Regex.Replace(childCategory.HintSubTitle, @"\bB9\b", partnerName)),
                ImageUrl = childCategory.HintIconUrl
            };
        }
    }

    private List<BankTransferViewModel> GetChildCategoryTransactions(TransactionAnalyticsCategory selectedCategory,
        TransactionAnalyticsCategory childCategory, List<BankTransferViewModel> transactionList)
    {
        List<BankTransferViewModel> categoryTransactions;

        switch (childCategory.Key)
        {
            case TransactionCategoryConstants.TransfersCategoryKey:
                {
                    var childCategoryPaymentTypeIds = childCategory.ChildCategories.SelectMany(x => x.TransferDisplayTypes)
                        .Select(x => x).ToList();

                    categoryTransactions =
                        transactionList.Where(x => childCategoryPaymentTypeIds.Contains(x.SavingsTransferType)
                                                   || childCategoryPaymentTypeIds.Contains(x.PendingTransferType.ConvertToTransferDisplayType()))
                            .ToList();
                    break;
                }
            case TransactionCategoryConstants.AtmCategoryKey:
                {
                    //ATM has own mcc codes and no children
                    var childCategoryMccCodes = childCategory.MccCodes.Select(x => x).ToList();

                    categoryTransactions =
                        transactionList.Where(x => x.MccCode.HasValue && childCategoryMccCodes.Contains(x.MccCode.Value))
                            .ToList();
                    break;
                }
            case TransactionCategoryConstants.CardTransactionsCategoryKey:
                {
                    var atmCategory = selectedCategory.ChildCategories.FirstOrDefault(x => x.Key == TransactionCategoryConstants.AtmCategoryKey);

                    // Ignore ATM Mcc codes, all other transactions with Mcc code are card transactions
                    categoryTransactions = transactionList
                        .Where(x => x.MccCode.HasValue && !atmCategory.MccCodes.Contains(x.MccCode.Value)).ToList();
                    break;
                }
            default:
                throw new Exception($"Root category key: {childCategory.Key} is not supported");
        }

        return categoryTransactions;
    }

    protected override void GroupAndSortSubcategories(TransactionAnalyticsCategory selectedCategory)
    {
        var subcategories = new List<TransactionAnalyticsListItem>
        {
            ResultObject.SubCategories.FirstOrDefault(x => x.Key == TransactionCategoryConstants.CardTransactionsCategoryKey),
            ResultObject.SubCategories.FirstOrDefault(x => x.Key == TransactionCategoryConstants.TransfersCategoryKey),
            ResultObject.SubCategories.FirstOrDefault(x => x.Key == TransactionCategoryConstants.AtmCategoryKey)
        };

        ResultObject.SubCategories = subcategories;
    }

    protected override Task BuildSelectedCategory(TransactionAnalyticsCategory selectedCategory, GetTransactionAnalyticsQuery request)
    {
        ResultObject.SubCategoriesTitle = "Top categories";

        return base.BuildSelectedCategory(selectedCategory, request);
    }
}
