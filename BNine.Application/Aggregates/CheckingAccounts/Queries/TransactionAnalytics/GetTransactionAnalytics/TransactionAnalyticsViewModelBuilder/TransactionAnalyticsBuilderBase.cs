﻿namespace BNine.Application.Aggregates.CheckingAccounts.Queries.TransactionAnalytics;

using CommonModels.Buttons;
using CommonModels.Dialog;
using CommonModels.Text;
using Domain.Entities;
using Enums.Transfers;
using Enums.Transfers.Controller;
using GetFilteredTransfers;
using Interfaces;
using Interfaces.TransactionHistory;
using MediatR;
using Models;
using Models.TransactionAnalytics;

public abstract class TransactionAnalyticsBuilderBase
{
    protected IMediator Mediator
    {
        get;
    }

    protected IBNineDbContext BNineDbContext
    {
        get;
    }

    protected ICurrentUserService CurrentUser
    {
        get;
        set;
    }

    protected ITransfersMappingService TransfersMappingService
    {
        get;
        set;
    }

    protected TransactionAnalyticsBuilderBase(
        IMediator mediator,
        IBNineDbContext bNineDbContext,
        ICurrentUserService currentUserService,
        ITransfersMappingService transfersMappingService)
    {
        Mediator = mediator;
        BNineDbContext = bNineDbContext;
        CurrentUser = currentUserService;
        TransfersMappingService = transfersMappingService;

        ResultObject = new TransactionAnalyticsViewModel();
    }

    protected TransactionAnalyticsViewModel ResultObject;

    public virtual async Task<TransactionAnalyticsViewModel> Build(TransactionAnalyticsCategory selectedCategory,
        GetTransactionAnalyticsQuery request)
    {
        await BuildSubcategories(selectedCategory, request);

        await BuildSelectedCategory(selectedCategory, request);

        GroupAndSortSubcategories(selectedCategory);

        BuildParents(selectedCategory);

        BuildCircleDiagram(selectedCategory);

        MapDataFromRequest(request);

        return ResultObject;
    }

    protected abstract Task BuildSubcategories(TransactionAnalyticsCategory selectedCategory,
        GetTransactionAnalyticsQuery getTransactionAnalyticsQuery);

    protected abstract void GroupAndSortSubcategories(TransactionAnalyticsCategory selectedCategory);

    protected virtual void MapDataFromRequest(GetTransactionAnalyticsQuery request)
    {
        ResultObject.TimePeriod = request.FilteringParameters.TimePeriod;
        ResultObject.Key = request.FilteringParameters.CategoryKey;
        ResultObject.IsPeriodSelectionEnabled = true;
    }

    protected async Task<List<BankTransferViewModel>> GetTransactionList(GetTransactionAnalyticsQuery request,
        List<TransferDisplayType>? transferDisplayTypes = null)
    {
        var pendingTransactions = await Mediator.Send(new GetFilteredTransfersQuery()
        {
            Ip = request.Ip,
            MbanqAccessToken = request.MbanqAccessToken,
            Paging = new GetFilteredTransfersPagingParameters() { PageSize = 99999, StartPage = 0 },
            Filter = new GetFilteredTransfersExpandedFilter()
            {
                Direction = TransferDirectionSimplified.Outgoing,
                CompletionState = BankTransferCompletionState.Pending,
                TimePeriod = request.FilteringParameters.TimePeriod
            },
        });

        var completedTransactions = await Mediator.Send(new GetFilteredTransfersQuery()
        {
            Ip = request.Ip,
            MbanqAccessToken = request.MbanqAccessToken,
            Paging = new GetFilteredTransfersPagingParameters() { PageSize = 99999, StartPage = 0 },
            Filter = new GetFilteredTransfersExpandedFilter()
            {
                Direction = TransferDirectionSimplified.Outgoing,
                CompletionState = BankTransferCompletionState.Completed,
                TimePeriod = request.FilteringParameters.TimePeriod
            }
        });

        List<BankTransferViewModel> baseModelList = new List<BankTransferViewModel>();

        baseModelList.AddRange(completedTransactions.Items);
        baseModelList.AddRange(pendingTransactions.Items);

        var filteredBaseModelList = baseModelList.Where(transaction =>
        {
            if (transferDisplayTypes != null)
            {
                return transferDisplayTypes.Contains(transaction.SavingsTransferType)
                       || transferDisplayTypes.Contains(transaction.PendingTransferType.ConvertToTransferDisplayType());
            }

            return true;
        }).ToList();

        return filteredBaseModelList;
    }

    protected virtual async Task BuildSelectedCategory(TransactionAnalyticsCategory selectedCategory, GetTransactionAnalyticsQuery request)
    {
        var userId = CurrentUser.UserId.HasValue ? CurrentUser.UserId.Value: throw new UnauthorizedAccessException();
        var accountLastDigits = await TransfersMappingService.GetAccountLastDigits(userId, request.MbanqAccessToken, request.Ip);

        ResultObject.BankAccountLastNumbers = accountLastDigits;
        ResultObject.Key = selectedCategory.Key;
        ResultObject.SpendingOnAccountHint = new GenericDialogRichBodyViewModel()
        {
            Title = "Spending",
            Buttons = new[] { new ButtonViewModel("GOT IT", ButtonStyleEnum.Solid) },
            Subtitle = new LinkedText("Here you'll find your spending displayed in various categories. Track your monthly spending by category to help boost your savings and avoid surprises."),
            ImageUrl = "https://b9prodaccount.blob.core.windows.net/static-documents-container/TransactionAnalyticsIcons/spendingHintIcon.png"
        };
    }

    protected virtual void BuildCircleDiagram(TransactionAnalyticsCategory selectedCategory)
    {
        var subcategories =
            ResultObject.SubCategories.Where(x => x.Key != TransactionCategoryConstants.DefaultCategoryKey);

        var first9Categories = subcategories.Take(9).ToList();

        var theRestCategories = subcategories.Skip(9);

        ResultObject.CircleDiagram = new CircleDiagram()
        {
            SubAmounts = first9Categories.Select(category => new CircleDiagram.CategorySubAmount()
            {
                Amount = category.TotalAmount,
                Color = category.Color,
                CategoryKey = category.Key
            }).ToList()
        };

        if (theRestCategories.Any())
        {
            ResultObject.CircleDiagram.SubAmounts.Add(new CircleDiagram.CategorySubAmount()
            {
                Amount = theRestCategories.Sum(x => x.TotalAmount),
                Color = theRestCategories.FirstOrDefault().Color
            });
        }

        ResultObject.CircleDiagram.TotalAmount = ResultObject.CircleDiagram.SubAmounts.Sum(x => x.Amount);
    }

    protected virtual void BuildParents(TransactionAnalyticsCategory selectedCategory)
    {
        var parentsList = new List<TransactionAnalyticsCategory>();

        // collect domain parent categories
        if (selectedCategory.ParentCategory != null)
        {
            if (selectedCategory.ParentCategory.ParentCategory != null)
            {
                parentsList.Add(selectedCategory.ParentCategory.ParentCategory);
            }

            parentsList.Add(selectedCategory.ParentCategory);
        }

        // add current category to the list
        parentsList.Add(selectedCategory);

        foreach (var parent in parentsList)
        {
            if (parent.Key == TransactionCategoryConstants.DefaultCategoryKey)
            {
                continue;
            }

            ResultObject.Parents.Add(new CategoryParent()
            {
                Key = parent.Key,
                Title = parent.Title,
                IconUrl = parent.IconUrl,
                Color = parent.Color
            });
        }
    }

}
