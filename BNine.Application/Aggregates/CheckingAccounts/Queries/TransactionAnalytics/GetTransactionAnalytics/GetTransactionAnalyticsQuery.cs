﻿namespace BNine.Application.Aggregates.CheckingAccounts.Queries.TransactionAnalytics;

using Abstractions;
using MediatR;
using Models;
using Models.TransactionAnalytics;
using Newtonsoft.Json;

public class GetTransactionAnalyticsQuery :
    MBanqSelfServiceUserRequest,
    IRequest<TransactionAnalyticsViewModel>
{
    [JsonProperty("filter")]
    public TransactionAnalyticsFilteringParameters FilteringParameters
    {
        get;
        set;
    } = new();

    [JsonProperty("paging")]
    public GetFilteredTransfersPagingParameters Paging
    {
        get;
        set;
    } = new();
}
