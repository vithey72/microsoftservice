﻿namespace BNine.Application.Aggregates.CheckingAccounts.Queries.TransactionAnalytics;

using Domain.Entities;

public interface ITransactionAnalyticsBuilderResolver
{
    TransactionAnalyticsBuilderBase ResolveTransactionAnalyticsBuilder(TransactionAnalyticsCategory selectedCategory,
        GetTransactionAnalyticsQuery request);
}
