﻿namespace BNine.Application.Aggregates.CheckingAccounts.Queries.TransactionAnalytics;

using Abstractions;
using AutoMapper;
using Exceptions;
using Interfaces;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Models.TransactionAnalytics;

public class GetTransactionAnalyticsQueryHandler : AbstractRequestHandler,
    IRequestHandler<GetTransactionAnalyticsQuery, TransactionAnalyticsViewModel>
{
    private readonly ITransactionAnalyticsBuilderResolver _transactionAnalyticsBuilderResolver;

    public GetTransactionAnalyticsQueryHandler(
        IMediator mediator,
        IBNineDbContext dbContext,
        IMapper mapper,
        ICurrentUserService currentUser,
        ITransactionAnalyticsBuilderResolver transactionAnalyticsBuilderResolver) : base(mediator, dbContext, mapper,
        currentUser)
    {
        _transactionAnalyticsBuilderResolver = transactionAnalyticsBuilderResolver;
    }

    public async Task<TransactionAnalyticsViewModel> Handle(
        GetTransactionAnalyticsQuery request, CancellationToken cancellationToken)
    {
        var selectedCategory = await DbContext.TransactionCategories
            .Include(x => x.ParentCategory)
            .ThenInclude(x => x.ParentCategory)
            .Include(x => x.ChildCategories)
            .ThenInclude(x => x.ChildCategories)
            .FirstOrDefaultAsync(x => x.Key == request.FilteringParameters.CategoryKey, cancellationToken);

        if (selectedCategory is null)
        {
            throw new NotFoundException($"Category with key: {request.FilteringParameters.CategoryKey} not found.");
        }

        var builder = _transactionAnalyticsBuilderResolver.ResolveTransactionAnalyticsBuilder(selectedCategory, request);

        var result = await builder.Build(selectedCategory, request);

        return result;
    }
}
