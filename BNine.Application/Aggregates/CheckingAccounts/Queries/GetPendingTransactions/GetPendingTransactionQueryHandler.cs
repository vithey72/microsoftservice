﻿namespace BNine.Application.Aggregates.CheckingAccounts.Queries.GetPendingTransactions
{
    using System.Linq;
    using System.Threading;
    using System.Threading.Tasks;
    using Abstractions;
    using Application.Models;
    using Application.Models.MBanq;
    using AutoMapper;
    using Exceptions;
    using Interfaces;
    using Interfaces.Bank.Client;
    using MediatR;
    using Microsoft.EntityFrameworkCore;

    public class GetPendingTransactionQueryHandler : AbstractRequestHandler,
        IRequestHandler<GetPendingTransactionsQuery, PaginatedList<PendingTransactionItem>>
    {
        private IBankSavingAccountsService BankAccountService
        {
            get;
        }

        private IBankSavingsTransactionsService BankSavingsTransactionsService
        {
            get;
        }

        public GetPendingTransactionQueryHandler(IMediator mediator, IBNineDbContext dbContext, IMapper mapper,
            ICurrentUserService currentUser, IBankSavingAccountsService bankAccountService, IBankSavingsTransactionsService bankSavingsTransactionsService) : base(mediator, dbContext, mapper, currentUser)
        {
            BankAccountService = bankAccountService;
            BankSavingsTransactionsService = bankSavingsTransactionsService;
        }

        public async Task<PaginatedList<PendingTransactionItem>> Handle(GetPendingTransactionsQuery request,
            CancellationToken cancellationToken)
        {
            var account = await DbContext.Users
                .Include(x => x.CurrentAccount)
                .Where(x => x.Id == CurrentUser.UserId)
                .Select(x => new { x.CurrentAccount.ExternalId })
                .FirstOrDefaultAsync(cancellationToken: cancellationToken);

            if (account == null)
            {
                throw new NotFoundException();
            }

            var transactions = await BankSavingsTransactionsService.GetPendingTransactions(account.ExternalId, request.MbanqAccessToken,  request.Skip + 1, request.Take, null, null, null, null, request.IpAddress);

            return transactions;
        }
    }
}
