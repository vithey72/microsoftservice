﻿namespace BNine.Application.Aggregates.CheckingAccounts.Queries.GetPendingTransactions
{
    using Application.Models;
    using Application.Models.MBanq;
    using MediatR;
    using Newtonsoft.Json;

    public class GetPendingTransactionsQuery : IRequest<PaginatedList<PendingTransactionItem>>
    {
        public int Skip
        {
            get; set;
        } = 0;

        public int Take
        {
            get; set;
        } = 20;

        [JsonIgnore]
        public string IpAddress
        {
            get; set;
        }

        [JsonIgnore]
        public string MbanqAccessToken
        {
            get;
            set;
        }
    }
}
