﻿namespace BNine.Application.Aggregates.CheckingAccounts.Queries.GetAccountStatementAsAdmin
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading;
    using System.Threading.Tasks;
    using Application.Models.Emails.EmailsWithTemplate;
    using AutoMapper;
    using Interfaces;
    using Interfaces.Bank.Client;
    using MediatR;
    using Microsoft.EntityFrameworkCore;
    using IBankSavingAccountsService = Interfaces.Bank.Administration.IBankSavingAccountsService;

    public class DownloadAccountStatementAsAdminQueryHandler
        : AccountStatementAdminQueryHandlerBase, IRequestHandler<DownloadAccountStatementAsAdminQuery, EmailAttachmentData>
    {
        public DownloadAccountStatementAsAdminQueryHandler(
            IMediator mediator,
            IBNineDbContext dbContext,
            IMapper mapper,
            ICurrentUserService currentUser,
            IBankSavingAccountsService bankSavingsAccountsService,
            IBankSavingsTransactionsService bankSavingsTransactionsService,
            IPartnerProviderService partnerProviderService
            )
            : base(mediator, dbContext, mapper, currentUser, bankSavingsAccountsService,
                bankSavingsTransactionsService, partnerProviderService)
        {
        }

        public async Task<EmailAttachmentData> Handle(DownloadAccountStatementAsAdminQuery request, CancellationToken cancellationToken)
        {
            var user = await DbContext.Users
                .Where(x => x.Id == request.UserId)
                .Include(x => x.CurrentAccount)
                .Include(x => x.CurrentAdditionalAccounts)
                .AsNoTracking()
                .FirstOrDefaultAsync(cancellationToken);

            if (user?.CurrentAccount == null)
            {
                throw new KeyNotFoundException("User or CurrentAccount not found.");
            }

            var attachment = await base.GenerateAccountStatementPdf(user, request.StartingDate, request.EndingDate);

            return attachment;
        }
    }
}
