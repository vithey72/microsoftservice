﻿namespace BNine.Application.Aggregates.CheckingAccounts.Queries.GetAccountStatementAsAdmin;

using Application.Models.Emails.EmailsWithTemplate;
using MediatR;

public record DownloadAccountStatementAsAdminQuery : AccountStatementAsAdminQueryBase, IRequest<EmailAttachmentData>
{ }
