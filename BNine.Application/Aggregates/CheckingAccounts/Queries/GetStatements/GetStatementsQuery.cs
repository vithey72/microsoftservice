namespace BNine.Application.Aggregates.CheckingAccounts.Queries.GetStatements
{
    using System;
    using MediatR;
    using Newtonsoft.Json;

    public class GetStatementsQuery : IRequest<Unit>
    {
        [JsonProperty("startingDate")]
        public DateTime? StartingDate
        {
            get; set;
        }

        [JsonProperty("endingDate")]
        public DateTime? EndingDate
        {
            get; set;
        }

        [JsonProperty("privateKey")]
        public string PrivateKey
        {
            get;
            set;
        }
    }
}
