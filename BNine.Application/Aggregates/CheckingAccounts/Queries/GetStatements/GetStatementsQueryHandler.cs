﻿namespace BNine.Application.Aggregates.CheckingAccounts.Queries.GetReport
{

    using System.Linq;
    using System.Threading;
    using System.Threading.Tasks;
    using AutoMapper;
    using BNine.Application.Aggregates.CheckingAccounts.Queries.GetStatements;
    using BNine.Enums;
    using Emails.Commands.Documents.SpendingAccountStatement;
    using Interfaces;
    using Interfaces.Bank.Client;
    using MediatR;
    using Microsoft.EntityFrameworkCore;
    using IBankSavingAccountsService = Interfaces.Bank.Administration.IBankSavingAccountsService;


    public class GetStatementsQueryHandler
        : AccountStatementBaseQueryHandler, IRequestHandler<GetStatementsQuery, Unit>
    {

        public GetStatementsQueryHandler(
            IMediator mediator,
            IBNineDbContext dbContext,
            IMapper mapper,
            ICurrentUserService currentUser,
            IBankSavingAccountsService bankSavingsAccountsService,
            IBankSavingsTransactionsService bankSavingsTransactionsService,
            IPartnerProviderService partnerProviderService
            )
            : base(mediator, dbContext, mapper, currentUser, bankSavingsTransactionsService,
                bankSavingsAccountsService, partnerProviderService)
        {
        }

        public async Task<Unit> Handle(GetStatementsQuery request, CancellationToken cancellationToken)
        {
            var users = await DbContext.Users
                .Where(x => x.Status != UserStatus.Active && x.Status != UserStatus.Declined)
                .Where(x => x.CurrentAccount != null)
                .Include(x => x.CurrentAccount)
                .Include(x => x.CurrentAdditionalAccounts)
                .AsNoTracking()
                .ToListAsync(cancellationToken);

            foreach (var user in users.TakeWhile(_ => !cancellationToken.IsCancellationRequested))
            {
                var statementInput = await GetStatementInput(user, request.StartingDate, request.EndingDate);
                if (statementInput.MainAccountCalculusItems.AccountTransactions.Count == 0)
                {
                    continue;
                }
                var attachment = GetPdfAttachment(user, statementInput);
                await Mediator.Send(new SpendingAccountStatementEmailCommand(user.Email, user.FirstName, attachment),
                    cancellationToken);
            }

            return Unit.Value;
        }
    }
}
