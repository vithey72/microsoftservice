﻿namespace BNine.Application.Aggregates.CheckingAccounts.Queries.GetRepeatTransactionProperties;

using Abstractions;
using Application.Models.MBanq.Transfers;
using MediatR;

public class GetRepeatTransactionPropertiesQuery
    : MBanqSelfServiceUserRequest,
      IRequest<GetRepeatTransactionPropertiesResponse>
{
    public int TransactionId
    {
        get;
        set;
    }
}
