﻿namespace BNine.Application.Aggregates.CheckingAccounts.Queries.GetRepeatTransactionProperties;


using Abstractions;
using Application.Models.MBanq.Transfers;
using AutoMapper;
using Constants;
using Enums.Transfers;
using Exceptions.ApiResponse;
using Interfaces;
using Interfaces.Bank.Client;
using Interfaces.TransactionHistory;
using MediatR;

public class GetRepeatTransactionPropertiesQueryHandler
    : AbstractRequestHandler,
      IRequestHandler<GetRepeatTransactionPropertiesQuery, GetRepeatTransactionPropertiesResponse>
{
    private readonly IBankSavingsTransactionsService _bankTransfersService;
    private readonly IBankClientExternalCardsService _externalCardsService;
    private readonly ITransfersMappingService _transfersMappingService;

    public GetRepeatTransactionPropertiesQueryHandler(
        IMediator mediator,
        IBNineDbContext dbContext,
        IMapper mapper,
        ICurrentUserService currentUser,
        IBankSavingsTransactionsService bankTransfersService,
        IBankClientExternalCardsService externalCardsService,
        ITransfersMappingService transfersMappingService
        )
        : base(mediator, dbContext, mapper, currentUser)
    {
        _bankTransfersService = bankTransfersService;
        _externalCardsService = externalCardsService;
        _transfersMappingService = transfersMappingService;
    }

    public async Task<GetRepeatTransactionPropertiesResponse> Handle(
        GetRepeatTransactionPropertiesQuery request, CancellationToken cancellationToken)
    {
        var savingsTransaction = await _bankTransfersService.GetSavingsAccountTransfer(
            request.TransactionId,
            request.MbanqAccessToken,
            request.Ip);

        var internalType = savingsTransaction.TransferDisplayType;
        switch (internalType)
        {
            case TransferDisplayType.AchSent:
                {
                    var creditor = savingsTransaction.Creditor;
                    var (accountNumber, routingNumber) = _transfersMappingService.ExtractAchRecipientNumbers(savingsTransaction);
                    var achRepeat = new GetRepeatTransactionPropertiesResponse.AchRepeatTransferDetails
                    {
                        AccountNumber = accountNumber,
                        RoutingNumber = routingNumber,
                        RecipientFullName = creditor.Name,
                        Address = creditor.Address.FirstOrDefault(),
                    };
                    return new GetRepeatTransactionPropertiesResponse()
                    {
                        Type = GetRepeatTransactionPropertiesResponse.RepeatTransactionType.Ach,
                        Amount = savingsTransaction.Amount,
                        Ach = achRepeat,
                    };
                }
            case TransferDisplayType.SentToB9Client:
                var (name, phone) = await _transfersMappingService.ExtractB9ClientPhoneAndName(savingsTransaction.Counterparty);
                return new GetRepeatTransactionPropertiesResponse
                {
                    Type = GetRepeatTransactionPropertiesResponse.RepeatTransactionType.Internal,
                    Amount = savingsTransaction.Amount,
                    Internal = new GetRepeatTransactionPropertiesResponse.InternalRepeatTransferDetails
                    {
                        RecipientFullName = name,
                        RecipientPhoneNumber = phone,
                    },
                };
            case TransferDisplayType.PushToExternalCard:
                var cardId = savingsTransaction.Creditor?.Identifier.Split(":").LastOrDefault();

                var externalCardList = await _externalCardsService.GetCards(
                    request.MbanqAccessToken, request.Ip,
                    new[] { Convert.ToInt32(cardId) });
                var externalCardEntity = externalCardList.FirstOrDefault();

                if (externalCardEntity == null)
                {
                    throw new ApiResponseException(ApiResponseErrorCodes.ExternalCardWasRemoved,
                        "Cannot repeat this transaction since the external card associated with it has been removed from Cards list");
                }
                return new GetRepeatTransactionPropertiesResponse
                {
                    Type = GetRepeatTransactionPropertiesResponse.RepeatTransactionType.PushToExternalCard,
                    Amount = savingsTransaction.Amount,
                    PushToExternalCard = new() { ExternalCardId = externalCardEntity.ExternalId },
                };
            default:
                throw new NotImplementedException($"Unknown type of transaction for repeat operation of type {savingsTransaction.RawType} sybtype {savingsTransaction.RawSubType}");
        }
    }
}
