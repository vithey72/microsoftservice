﻿namespace BNine.Application.Aggregates.CheckingAccounts.Queries.GetAccountStatementAsAdmin;

using MediatR;
using Newtonsoft.Json;

public record GetAccountStatementAsAdminQuery : AccountStatementAsAdminQueryBase, IRequest<Unit>
{
    [JsonProperty("sendToEmail")]
    public string SendToEmail
    {
        get; set;
    }
}
