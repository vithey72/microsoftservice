﻿namespace BNine.Application.Aggregates.CheckingAccounts.Queries.GetAccountStatementAsAdmin;

using System.Threading.Tasks;
using Application.Models.Emails.EmailsWithTemplate;
using AutoMapper;
using Domain.Entities.User;
using GetReport;
using Interfaces;
using Interfaces.Bank.Client;
using MediatR;
using IBankSavingAccountsService = Interfaces.Bank.Administration.IBankSavingAccountsService;

public class AccountStatementAdminQueryHandlerBase : AccountStatementBaseQueryHandler
{

    protected AccountStatementAdminQueryHandlerBase(
        IMediator mediator,
        IBNineDbContext dbContext,
        IMapper mapper,
        ICurrentUserService currentUser,
        IBankSavingAccountsService bankSavingsAccountsService,
        IBankSavingsTransactionsService bankSavingsTransactionsService,
        IPartnerProviderService partnerProviderService
        )
        : base(mediator,
        dbContext, mapper, currentUser, bankSavingsTransactionsService, bankSavingsAccountsService, partnerProviderService)
    {
    }

    protected async Task<EmailAttachmentData> GenerateAccountStatementPdf(User user, DateTime? startingDate,
        DateTime? endingDate)
    {
        var statementInput = await GetStatementInput(user, startingDate, endingDate);
        var attachment = GetPdfAttachment(user, statementInput);
        return attachment;
    }
}
