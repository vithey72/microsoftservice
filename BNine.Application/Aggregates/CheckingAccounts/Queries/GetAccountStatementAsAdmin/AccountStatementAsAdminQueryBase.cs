﻿namespace BNine.Application.Aggregates.CheckingAccounts.Queries.GetAccountStatementAsAdmin;

using Newtonsoft.Json;

public record AccountStatementAsAdminQueryBase
{
    [JsonProperty("userId")]
    public Guid UserId
    {
        get; set;
    }

    [JsonProperty("startingDate")]
    public DateTime? StartingDate
    {
        get; set;
    }

    [JsonProperty("endingDate")]
    public DateTime? EndingDate
    {
        get; set;
    }
}
