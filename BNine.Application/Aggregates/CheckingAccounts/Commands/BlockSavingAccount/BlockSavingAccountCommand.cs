﻿namespace BNine.Application.Aggregates.CheckingAccounts.Commands.BlockSavingAccount
{
    using System;
    using MediatR;
    using Newtonsoft.Json;

    public class BlockSavingAccountCommand : IRequest<Unit>
    {
        [JsonIgnore]
        public Guid UserId
        {
            get; set;
        }

        [JsonProperty("reasonId")]
        public Guid ReasonId
        {
            get; set;
        }
    }
}
