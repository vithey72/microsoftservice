﻿namespace BNine.Application.Aggregates.CheckingAccounts.Commands.BlockSavingAccount
{
    using System.Linq;
    using System.Threading;
    using System.Threading.Tasks;
    using AutoMapper;
    using BNine.Application.Abstractions;
    using BNine.Application.Exceptions;
    using BNine.Application.Interfaces;
    using BNine.Application.Interfaces.Bank.Administration;
    using BNine.Domain.Entities.BankAccount;
    using BNine.Domain.Entities.User;
    using MediatR;
    using Microsoft.EntityFrameworkCore;

    public class BlockSavingAccountCommandHandler
        : AbstractRequestHandler, IRequestHandler<BlockSavingAccountCommand, Unit>
    {
        private IBankSavingAccountsService BankSavingAccountsService
        {
            get;
        }

        public BlockSavingAccountCommandHandler(IBankSavingAccountsService bankSavingAccountsService, IMediator mediator, IBNineDbContext dbContext, IMapper mapper, ICurrentUserService currentUser) : base(mediator, dbContext, mapper, currentUser)
        {
            BankSavingAccountsService = bankSavingAccountsService;
        }

        public async Task<Unit> Handle(BlockSavingAccountCommand request, CancellationToken cancellationToken)
        {
            var savingAccount = await DbContext.Users
                .Where(x => x.Id == request.UserId)
                .Select(x => new { x.CurrentAccount.ExternalId })
                .FirstOrDefaultAsync(cancellationToken);

            if (savingAccount == null)
            {
                throw new NotFoundException(nameof(CurrentAccount), new
                {
                    userId = request.UserId
                });
            }

            var accountInfo = await BankSavingAccountsService.GetSavingAccountInfo(savingAccount.ExternalId);

            if (accountInfo.AvailableBalance != 0)
            {
                throw new ValidationException(nameof(request), "Cannot block account with positive or negative balance");
            }

            var loanAccount = DbContext.LoanAccounts
                .Where(x => x.UserId == request.UserId)
                .Where(x => !x.DeletedAt.HasValue)
                .Where(x => x.Status == Enums.LoanAccountStatus.Active)
                .FirstOrDefault();

            if (loanAccount != null)
            {
                throw new ValidationException(nameof(request), "Cannot block account with active loan");
            }

            var blockReason = DbContext.UserBlockReasons
                .Where(x => x.Id == request.ReasonId)
                .FirstOrDefault();

            if (blockReason == null)
            {
                throw new NotFoundException(nameof(BlockUserReason), request.ReasonId);
            }

            await BankSavingAccountsService.BlockSavingAccount(savingAccount.ExternalId, blockReason.Value);

            return Unit.Value;
        }
    }
}
