﻿namespace BNine.Application.Aggregates.CheckingAccounts.Commands.UnblockSavingAccount
{
    using System.Linq;
    using System.Threading;
    using System.Threading.Tasks;
    using AutoMapper;
    using BNine.Application.Abstractions;
    using BNine.Application.Exceptions;
    using BNine.Application.Interfaces;
    using BNine.Application.Interfaces.Bank.Administration;
    using BNine.Domain.Entities.BankAccount;
    using MediatR;
    using Microsoft.EntityFrameworkCore;

    public class UnblockSavingAccountCommandHandler
        : AbstractRequestHandler, IRequestHandler<UnblockSavingAccountCommand, Unit>
    {
        private IBankSavingAccountsService BankSavingAccountsService
        {
            get;
        }

        public UnblockSavingAccountCommandHandler(IBankSavingAccountsService bankSavingAccountsService, IMediator mediator, IBNineDbContext dbContext, IMapper mapper, ICurrentUserService currentUser) : base(mediator, dbContext, mapper, currentUser)
        {
            BankSavingAccountsService = bankSavingAccountsService;
        }

        public async Task<Unit> Handle(UnblockSavingAccountCommand request, CancellationToken cancellationToken)
        {
            var savingAccount = await DbContext.Users
                .Where(x => x.Id == request.UserId)
                .Select(x => new { x.CurrentAccount.ExternalId })
                .FirstOrDefaultAsync(cancellationToken);

            if (savingAccount == null)
            {
                throw new NotFoundException(nameof(CurrentAccount), new
                {
                    userId = request.UserId
                });
            }

            await BankSavingAccountsService.UnblockSavingAccount(savingAccount.ExternalId);

            return Unit.Value;
        }
    }
}
