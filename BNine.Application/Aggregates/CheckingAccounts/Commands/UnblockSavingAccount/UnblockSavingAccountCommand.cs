﻿namespace BNine.Application.Aggregates.CheckingAccounts.Commands.UnblockSavingAccount
{
    using System;
    using MediatR;
    using Newtonsoft.Json;

    public class UnblockSavingAccountCommand : IRequest<Unit>
    {
        public UnblockSavingAccountCommand(Guid userId)
        {
            UserId = userId;
        }

        [JsonIgnore]
        public Guid UserId
        {
            get; set;
        }
    }
}
