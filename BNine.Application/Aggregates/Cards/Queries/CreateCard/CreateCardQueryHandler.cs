﻿namespace BNine.Application.Aggregates.Cards.Queries.CreateCard;

using System;
using Abstractions;
using AutoMapper;
using BNine.Application.Helpers;
using BNine.Enums.DebitCard;
using Domain.Entities;
using Enums;
using Exceptions;
using Interfaces;
using Interfaces.Bank.Administration;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Models;
using IBankInternalCardsService = Interfaces.Bank.Administration.IBankInternalCardsService;

public class CreateCardQueryHandler
    : AbstractRequestHandler, IRequestHandler<CreateCardQuery, CreateCardPhysicalDialogViewModel>
{
    private readonly IPaidUserServicesService _paidServices;
    private readonly IPartnerProviderService _partnerProvider;
    private readonly IBankInternalCardsService _bankInternalCardsService;

    public CreateCardQueryHandler(
        IMediator mediator,
        IBNineDbContext dbContext,
        IMapper mapper,
        ICurrentUserService currentUser,
        IBankInternalCardsService bankInternalCardsService,
        IPaidUserServicesService paidServices,
        IPartnerProviderService partnerProvider
    )
        : base(mediator, dbContext, mapper, currentUser)
    {
        _paidServices = paidServices;
        _bankInternalCardsService = bankInternalCardsService;
        _partnerProvider = partnerProvider;
    }

    public async Task<CreateCardPhysicalDialogViewModel> Handle(CreateCardQuery request, CancellationToken cancellationToken)
    {
        var partner = _partnerProvider.GetPartner();
        if (request.CardType == CardType.VirtualDebit && !partner.CanIssueVirtualCard())
        {
            throw new InvalidOperationException($"Virtual card product is disabled for this partner: {partner}");
        }

        var user = DbContext.Users
            .Include(x => x.DebitCards)
            .Include(x => x.CurrentAccount)
            .First(x => x.Id == CurrentUser.UserId && x.Status == UserStatus.Active);

        if (request.CardType == CardType.VirtualDebit)
        {
            await CreateVirtualCard(user, cancellationToken);
            return default;
        }

        return new CreateCardPhysicalDialogViewModel
        {
            Header = "Order a physical card?",
            Subtitle = "Your physical card will arrive to your registered address in 5-10 business days.",
            ButtonText = "CONFIRM",
            BackButtonText = "BACK",
        };
    }

    private async Task CreateVirtualCard(Domain.Entities.User.User user, CancellationToken cancellationToken)
    {
        if (user.DebitCards
            .Any(x => x.Type == CardType.VirtualDebit && x.Status < CardStatus.REPLACED))
        {
            throw new ValidationException("card", "User already has a virtual debit card");
        }

        var virtualCardProduct = await DbContext
            .CardProducts
            .SingleAsync(x => x.AssociatedType == CardType.VirtualDebit, cancellationToken);

        var externalCardId = await _bankInternalCardsService
            .CreateInternalCardREST(
                user.ExternalClientId!.Value,
                user.CurrentAccount.ExternalId,
                virtualCardProduct.ExternalId);
        await _bankInternalCardsService.EnableOnlinePayments(externalCardId);

        user.DebitCards.Add(new DebitCard
        {
            Status = CardStatus.ACTIVE,
            ShippingStatus = CardShippingStatus.ACTIVATED,
            ExternalId = externalCardId,
            Type = CardType.VirtualDebit,
            CreatedAt = DateTime.Now,
        });
        await DbContext.SaveChangesAsync(CancellationToken.None);
    }
}
