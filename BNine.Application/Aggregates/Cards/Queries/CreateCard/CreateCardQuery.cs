﻿namespace BNine.Application.Aggregates.Cards.Queries.CreateCard;

using Behaviours;
using BNine.Enums.DebitCard;
using MediatR;
using Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

[Sequential]
public class CreateCardQuery : IRequest<CreateCardPhysicalDialogViewModel>
{
    [JsonConverter(typeof(StringEnumConverter))]
    [JsonProperty("type")]
    public CardType CardType
    {
        get;
        set;
    }

    [JsonIgnore]
    public string Ip
    {
        get;
        set;
    }
}
