﻿namespace BNine.Application.Aggregates.Cards.Queries.GetDebitCardReplacementDetails;

using Abstractions;
using MediatR;
using Models;

public class GetDebitCardReplacementDetailsQuery
    : MBanqSelfServiceUserRequest
    , IRequest<DialogWithCancelButtonViewModel>
{
}
