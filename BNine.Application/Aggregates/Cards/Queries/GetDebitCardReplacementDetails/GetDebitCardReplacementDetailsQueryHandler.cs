﻿namespace BNine.Application.Aggregates.Cards.Queries.GetDebitCardReplacementDetails;

using Abstractions;
using AutoMapper;
using Constants;
using Helpers;
using Interfaces;
using Interfaces.Bank.Administration;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Models;
using IBankInternalCardsService = Interfaces.Bank.Client.IBankInternalCardsService;

//Currently there is no fees for physical card reissue
//https://bninecom.atlassian.net/browse/B9-3640
[Obsolete]
public class GetDebitCardReplacementDetailsQueryHandler
    : AbstractRequestHandler
    , IRequestHandler<GetDebitCardReplacementDetailsQuery, DialogWithCancelButtonViewModel>
{
    private readonly IBankInternalCardsService _cardsService;
    private readonly IPaidUserServicesService _paidUserServices;

    public GetDebitCardReplacementDetailsQueryHandler(
        IMediator mediator,
        IBNineDbContext dbContext,
        IMapper mapper,
        ICurrentUserService currentUser,
        IBankInternalCardsService cardsService,
        IPaidUserServicesService paidUserServices
    )
        : base(mediator, dbContext, mapper, currentUser)
    {
        _cardsService = cardsService;
        _paidUserServices = paidUserServices;
    }

    public async Task<DialogWithCancelButtonViewModel> Handle(GetDebitCardReplacementDetailsQuery request, CancellationToken token)
    {
        var user = await DbContext.Users
            .FirstAsync(u => u.Id == CurrentUser.UserId, token);

        var replacements = await _cardsService.CountReplacements(
            request.MbanqAccessToken,
            request.Ip,
            user.ExternalClientId!.Value);

        if (replacements == 0)
        {
            return null;
        }

        var replacementCost = await _paidUserServices.GetServiceCost(PaidUserServices.PhysicalCardReplacement);
        var costFormatted = CurrencyFormattingHelper.AsRegular(replacementCost);
        return new DialogWithCancelButtonViewModel
        {
            Header = "Order a new\nphysical card?",
            Subtitle = $"Having a physical card issued & delivered will cost {costFormatted}. Your physical card will arrive to your registered address in 5-10 business days.",
            ButtonText = $"CONFIRM & PAY {costFormatted}",
            BackButtonText = "CLOSE",
        };
    }
}
