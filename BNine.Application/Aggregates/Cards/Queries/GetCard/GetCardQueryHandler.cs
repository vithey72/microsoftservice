﻿namespace BNine.Application.Aggregates.Cards.Queries.GetCard
{
    using System.Linq;
    using System.Threading;
    using System.Threading.Tasks;
    using AutoMapper;
    using BNine.Application.Abstractions;
    using BNine.Application.Aggregates.Cards.Models;
    using BNine.Application.Interfaces;
    using BNine.Application.Interfaces.Bank.Client;
    using BNine.Common;
    using MediatR;
    using Microsoft.EntityFrameworkCore;

    public class GetCardQueryHandler
        : AbstractRequestHandler, IRequestHandler<GetCardQuery, CardInfoPay>
    {
        private IBankSavingAccountsService BankSavingAccountsService
        {
            get;
        }

        private IBankInternalCardsService BankInternalCardsService
        {
            get;
        }

        public GetCardQueryHandler(
            IMediator mediator,
            IBNineDbContext dbContext,
            IMapper mapper,
            ICurrentUserService currentUser,
            IBankSavingAccountsService bankSavingAccountsService,
            IBankInternalCardsService bankInternalCardsService)
            : base(mediator, dbContext, mapper, currentUser)
        {
            BankSavingAccountsService = bankSavingAccountsService;
            BankInternalCardsService = bankInternalCardsService;
        }

        public async Task<CardInfoPay> Handle(GetCardQuery request, CancellationToken cancellationToken)
        {
            var user = await DbContext.Users
                .Include(u => u.CurrentAccount)
                .Include(u => u.DebitCards)
                .FirstOrDefaultAsync(u => u.Id == CurrentUser.UserId, cancellationToken);

            var newestCard = request.CardId.HasValue ?
                user!.DebitCards
                    .Where(dc => dc.ExternalId == request.CardId.Value)
                    .MaxBy(dc => dc.CreatedAt)
                : user!.DebitCards
                    .MaxBy(dc => dc.CreatedAt);

            var accountInfo = await BankSavingAccountsService.GetSavingAccountInfo(
                user.CurrentAccount.ExternalId,
                request.MbanqAccessToken,
                request.Ip);

            var cardInfo = new CardInfoPay
            {
                SavingAccountNumber = accountInfo.AccountNumber,
            };

            if (newestCard == null)
            {
                return cardInfo;
            }

            var cardExternalDetails = await BankInternalCardsService.GetCardInfo(
                request.MbanqAccessToken,
                request.Ip,
                newestCard.ExternalId);

            cardInfo.Id = newestCard.ExternalId;
            cardInfo.Last4Numbers = SecretNumberHelper.GetLastFourDigits(cardExternalDetails.PrimaryAccountNumber);

            return cardInfo;
        }
    }
}
