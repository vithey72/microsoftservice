﻿namespace BNine.Application.Aggregates.Cards.Queries.GetCard;

using Abstractions;
using Models;
using MediatR;

public class GetCardQuery : DebitCardUserRequest, IRequest<CardInfoPay>
{
}
