﻿namespace BNine.Application.Aggregates.Cards.Queries.GetCardShippingStatus;

using Abstractions;
using Models;
using MediatR;

public class GetCardShippingStatusQuery : DebitCardUserRequest, IRequest<ShippingHistoryModel>
{
}
