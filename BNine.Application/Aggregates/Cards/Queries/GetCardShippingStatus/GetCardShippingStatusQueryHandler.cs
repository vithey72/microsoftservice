﻿namespace BNine.Application.Aggregates.Cards.Queries.GetCardShippingStatus
{
    using System.Threading;
    using System.Threading.Tasks;
    using Abstractions;
    using AutoMapper;
    using Domain.Entities;
    using Enums.DebitCard;
    using Exceptions;
    using GetMostRecentCard;
    using Interfaces;
    using Interfaces.Bank.Client;
    using MediatR;
    using Models;

    public class GetCardShippingStatusQueryHandler
        : AbstractRequestHandler, IRequestHandler<GetCardShippingStatusQuery, ShippingHistoryModel>
    {
        private IBankInternalCardsService BankInternalCardsService
        {
            get;
        }

        public GetCardShippingStatusQueryHandler(
            IMediator mediator,
            IBNineDbContext dbContext,
            IMapper mapper,
            ICurrentUserService currentUser,
            IBankInternalCardsService bankInternalCardsService)
            : base(mediator, dbContext, mapper, currentUser)
        {
            BankInternalCardsService = bankInternalCardsService;
        }

        public async Task<ShippingHistoryModel> Handle(GetCardShippingStatusQuery request, CancellationToken cancellationToken)
        {
            var card = await Mediator.Send(new GetMostRecentDebitCardQuery(CardType.PhysicalDebit, request.CardId));

            if (card == null)
            {
                throw new NotFoundException(nameof(DebitCard), CurrentUser.UserId);
            }

            var shippingHistory = await BankInternalCardsService.GetCardShippingHistory(request.MbanqAccessToken, request.Ip, card.ExternalId);
            if (shippingHistory.Status == card.ShippingStatus)
            {
                return shippingHistory;
            }

            card.ShippingStatus = shippingHistory.Status;
            await DbContext.SaveChangesAsync(cancellationToken);

            return shippingHistory;
        }
    }
}
