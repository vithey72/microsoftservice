﻿namespace BNine.Application.Aggregates.Cards.Queries.GetCreditCardStatus;

using MediatR;
using Models;

public record GetCreditCardStatusQuery : IRequest<CreditCardStatusViewModel>;
