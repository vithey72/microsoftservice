﻿namespace BNine.Application.Aggregates.Cards.Queries.GetCreditCardStatus;

using AutoMapper;
using MediatR;
using Abstractions;
using Enums.DebitCard;
using Models;
using Interfaces;
using Interfaces.Bank.Administration;
using Microsoft.EntityFrameworkCore;

public class GetCreditCardStatusQueryHandler
    : AbstractRequestHandler
    , IRequestHandler<GetCreditCardStatusQuery, CreditCardStatusViewModel>
{
    private readonly IBankCreditCardsService _creditCardsService;

    public GetCreditCardStatusQueryHandler(
        IMediator mediator,
        IBNineDbContext dbContext,
        IMapper mapper,
        ICurrentUserService currentUser,
        IBankCreditCardsService creditCardsService
        )
        : base(mediator, dbContext, mapper, currentUser)
    {
        _creditCardsService = creditCardsService;
    }

    public async Task<CreditCardStatusViewModel> Handle(GetCreditCardStatusQuery request, CancellationToken cancellationToken)
    {
        var creditCard = await DbContext.DebitCards
            .Where(c => c.UserId == CurrentUser.UserId && c.Type == CardType.Credit)
            .ToListAsync(cancellationToken);
        var activeCard = creditCard.FirstOrDefault(c => c.Status is CardStatus.ACTIVE or CardStatus.SUSPENDED);

        if (activeCard != null)
        {
            var creditCardStats = await _creditCardsService.GetBalance(activeCard.ExternalId);
            return new CreditCardStatusViewModel
            {
                Id = activeCard.ExternalId,
                Status = CardDisplayedStatus.Active, // TODO: project status to viewmodel status
                Balance = creditCardStats.balance,
                LastDigits = creditCardStats.cardNumber,
            };
        }

        return new CreditCardStatusViewModel
        {
            Status = CardDisplayedStatus.NoCard,
        };
    }
}
