﻿namespace BNine.Application.Aggregates.Cards.Queries.GetCardLimitsObselete
{
    using BNine.Application.Aggregates.Cards.Models;
    using MediatR;

    [Obsolete]
    public class GetCardLimitsQueryObselete : DebitCardBaseModel, IRequest<CardLimitsResponseModel>
    {
    }
}
