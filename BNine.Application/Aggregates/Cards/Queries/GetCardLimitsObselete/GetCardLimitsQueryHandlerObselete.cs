﻿namespace BNine.Application.Aggregates.Cards.Queries.GetCardLimitsObselete
{
    using System.Threading;
    using System.Threading.Tasks;
    using Abstractions;
    using AutoMapper;
    using BNine.Application.Aggregates.CardProduct.Queries.GetMaximumCardLimits;
    using Enums.DebitCard;
    using Exceptions;
    using GetMostRecentCard;
    using Interfaces;
    using Interfaces.Bank.Administration;
    using MediatR;
    using Models;

    [Obsolete(
        $"A new implementation added {nameof(BNine.Application.Aggregates.Cards.Queries.GetCardLimitsObsolete.GetCardLimitsQueryHandler)}")]
    public class GetCardLimitsQueryHandlerObselete
        : AbstractRequestHandler, IRequestHandler<GetCardLimitsQueryObselete, CardLimitsResponseModel>
    {
        private IBankInternalCardsService BankInternalCardsService
        {
            get;
        }

        public GetCardLimitsQueryHandlerObselete(
            IMediator mediator,
            IBNineDbContext dbContext,
            IMapper mapper,
            ICurrentUserService currentUser,
            IBankInternalCardsService bankInternalCardsService)
            : base(mediator, dbContext, mapper, currentUser)
        {
            BankInternalCardsService = bankInternalCardsService;
        }

        public async Task<CardLimitsResponseModel> Handle(GetCardLimitsQueryObselete request,
            CancellationToken cancellationToken)
        {
            var mockResult = await Mediator.Send(new GetMaximumCardLimitsQuery());

            return mockResult;

            // Currently, this functionality shall be disabled because it doesn't work at Mbanq side.
            var card = await Mediator.Send(new GetMostRecentDebitCardQuery(request.CardId));
            CardLimitsResponseModel result = null;

            if (card == null)
            {
                throw new NotFoundException("There is no physical debit card for current user", CurrentUser.UserId);
            }

            if (card.Type == CardType.PhysicalDebit)
            {
                result = await BankInternalCardsService.GetCardLimits(card.ExternalId);
            }
            else
            {
                throw new NotFoundException("There is no limits for a virtual debit card");
            }

            return result;
        }
    }
}
