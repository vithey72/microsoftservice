﻿namespace BNine.Application.Aggregates.Cards.Queries.GetMostRecentCard;

using Abstractions;
using Enums.DebitCard;
using Domain.Entities;
using MediatR;

public class GetMostRecentDebitCardQuery : DebitCardUserRequest, IRequest<DebitCard>
{
    public GetMostRecentDebitCardQuery(CardType? type = null) => CardType = type;

    public GetMostRecentDebitCardQuery(int? cardId = null) => CardId = cardId;

    public GetMostRecentDebitCardQuery(CardType? type = null, int? cardId = null)
    {
        CardType = type;
        CardId = cardId;
    }

    public CardType? CardType
    {
        get; set;
    }
}
