﻿namespace BNine.Application.Aggregates.Cards.Queries.GetMostRecentCard
{
    using System.Linq;
    using System.Threading;
    using System.Threading.Tasks;
    using AutoMapper;
    using BNine.Application.Abstractions;
    using BNine.Application.Interfaces;
    using BNine.Domain.Entities;
    using Enums.DebitCard;
    using MediatR;
    using Microsoft.EntityFrameworkCore;

    public class GetMostRecentDebitCardQueryHandler
        : AbstractRequestHandler, IRequestHandler<GetMostRecentDebitCardQuery, DebitCard>
    {
        public GetMostRecentDebitCardQueryHandler(
            IMediator mediator,
            IBNineDbContext dbContext,
            IMapper mapper,
            ICurrentUserService currentUser)
            : base(mediator, dbContext, mapper, currentUser)
        {
        }

        public async Task<DebitCard> Handle(GetMostRecentDebitCardQuery request, CancellationToken cancellationToken)
        {
            var allowedStatuses = new[] { CardStatus.ACTIVE, CardStatus.SUSPENDED, CardStatus.ORDERED, CardStatus.CREATED };

            var queryDb = DbContext.DebitCards
                .Where(x => x.UserId == CurrentUser.UserId)
                .Where(x => allowedStatuses.Contains(x.Status));

            if (request.CardId.HasValue)
            {
                queryDb = queryDb.Where(x => x.ExternalId == request.CardId.Value);
            }
            else if (request.CardType.HasValue)
            {
                queryDb = queryDb.Where(x => x.Type == request.CardType.Value);
            }

            var card = await queryDb
                .OrderByDescending(x => x.ExternalId)
                .FirstOrDefaultAsync(cancellationToken);

            return card;
        }
    }
}
