﻿namespace BNine.Application.Aggregates.Cards.Queries.GetWalletsInfo
{
    using Newtonsoft.Json;

    public class WalletsInfo
    {
        [JsonProperty("isApplePayConnected")]
        public bool IsApplePayConnected
        {
            get; set;
        }

        [JsonProperty("isGooglePayConnected")]
        public bool IsGooglePayConnected
        {
            get; set;
        }

        [JsonProperty("isSamsungPayConnected")]
        public bool IsSamsungPayConnected
        {
            get; set;
        }
    }
}
