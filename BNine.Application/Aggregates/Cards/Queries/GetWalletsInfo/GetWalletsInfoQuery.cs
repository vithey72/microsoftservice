﻿namespace BNine.Application.Aggregates.Cards.Queries.GetWalletsInfo
{
    using BNine.Application.Aggregates.Cards.Models;
    using MediatR;

    public class GetWalletsInfoQuery : DebitCardBaseModel, IRequest<WalletsInfo>
    {
    }
}
