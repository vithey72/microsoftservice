﻿namespace BNine.Application.Aggregates.Cards.Queries.GetWalletsInfo
{
    using System.Linq;
    using System.Threading;
    using System.Threading.Tasks;
    using AutoMapper;
    using BNine.Application.Abstractions;
    using BNine.Application.Interfaces;
    using Enums.DebitCard;
    using MediatR;
    using Microsoft.EntityFrameworkCore;

    public class GetWalletsInfoQueryHandler
        : AbstractRequestHandler, IRequestHandler<GetWalletsInfoQuery, WalletsInfo>
    {
        public GetWalletsInfoQueryHandler(IMediator mediator, IBNineDbContext dbContext, IMapper mapper, ICurrentUserService currentUser) : base(mediator, dbContext, mapper, currentUser)
        {
        }

        public async Task<WalletsInfo> Handle(GetWalletsInfoQuery request, CancellationToken cancellationToken)
        {
            var cardQuery = DbContext.DebitCards
                 .Where(x => x.UserId == CurrentUser.UserId)
                 .Where(x => x.Status == CardStatus.ACTIVE);

            if (request.CardId.HasValue)
            {
                cardQuery = cardQuery.Where(x => x.ExternalId == request.CardId);
            }

            var card = await cardQuery
                 .Include(x => x.GoogleWallet)
                 .Include(x => x.AppleWallet)
                 .Include(x => x.SamsungWallet)
                 .FirstOrDefaultAsync(cancellationToken);

            if (card == null)
            {
                return null;
            }

            return new WalletsInfo
            {
                IsApplePayConnected = card.AppleWallet != null && !card.AppleWallet.DeletedAt.HasValue,
                IsGooglePayConnected = card.GoogleWallet != null && !card.GoogleWallet.DeletedAt.HasValue,
                IsSamsungPayConnected = card.SamsungWallet != null && !card.SamsungWallet.DeletedAt.HasValue
            };
        }
    }
}
