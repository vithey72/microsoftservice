﻿namespace BNine.Application.Aggregates.Cards.Queries.GetVirtualDebitCardReplacementDetails;

using Abstractions;
using MediatR;
using Models;

public class GetVirtualDebitCardReplacementDetailsQuery
    : MBanqSelfServiceUserRequest
    , IRequest<DialogWithCancelButtonViewModel>
{
}
