﻿namespace BNine.Application.Aggregates.Cards.Queries.GetVirtualDebitCardReplacementDetails;

using Abstractions;
using AutoMapper;
using Interfaces;
using MediatR;
using Models;

public class GetVirtualDebitCardReplacementDetailsQueryHandler
    : AbstractRequestHandler
        , IRequestHandler<GetVirtualDebitCardReplacementDetailsQuery, DialogWithCancelButtonViewModel>
{
    public GetVirtualDebitCardReplacementDetailsQueryHandler(
        IMediator mediator,
        IBNineDbContext dbContext,
        IMapper mapper,
        ICurrentUserService currentUser
    )
        : base(mediator, dbContext, mapper, currentUser)
    {
    }

    public async Task<DialogWithCancelButtonViewModel> Handle(GetVirtualDebitCardReplacementDetailsQuery request,
        CancellationToken token)
    {
        return new DialogWithCancelButtonViewModel
        {
            Header = "Issue a new virtual card",
            Subtitle = $"Your current card will be terminated. \nYou will get your new credentials instantly.",
            ButtonText = $"CONFIRM",
            BackButtonText = "CLOSE",
        };
    }
}
