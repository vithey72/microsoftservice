﻿namespace BNine.Application.Aggregates.Cards.Queries.GetCardImage;

using Abstractions;
using Enums.DebitCard;
using MediatR;
using Models;

public class GetCardImageQuery : DebitCardUserRequest, IRequest<CardImage>
{
    public CardType? CardType
    {
        get; set;
    }
}
