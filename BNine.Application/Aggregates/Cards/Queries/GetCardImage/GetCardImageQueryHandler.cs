﻿namespace BNine.Application.Aggregates.Cards.Queries.GetCardImage
{
    using System.Threading;
    using System.Threading.Tasks;
    using Abstractions;
    using AutoMapper;
    using Domain.Entities;
    using Exceptions;
    using GetMostRecentCard;
    using Interfaces;
    using Interfaces.Bank.Client;
    using MediatR;
    using Models;

    public class GetCardImageQueryHandler
        : AbstractRequestHandler, IRequestHandler<GetCardImageQuery, CardImage>
    {
        private IBankInternalCardsService BankInternalCardsService
        {
            get;
        }

        public GetCardImageQueryHandler(
            IMediator mediator,
            IBNineDbContext dbContext,
            IMapper mapper,
            ICurrentUserService currentUser,
            IBankInternalCardsService bankInternalCardsService)
            : base(mediator, dbContext, mapper, currentUser)
        {
            BankInternalCardsService = bankInternalCardsService;
        }

        public async Task<CardImage> Handle(GetCardImageQuery request, CancellationToken cancellationToken)
        {
            var card = await Mediator.Send(new GetMostRecentDebitCardQuery(request.CardId));

            if (card == null)
            {
                throw new NotFoundException(nameof(DebitCard), CurrentUser.UserId);
            }

            var imageLink = await BankInternalCardsService.GetCardImageUrl(request.MbanqAccessToken, request.Ip, card.ExternalId);

            return new CardImage
            {
                ImageLink = imageLink
            };
        }
    }
}
