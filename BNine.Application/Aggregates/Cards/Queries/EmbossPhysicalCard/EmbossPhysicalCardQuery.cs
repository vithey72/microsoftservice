﻿namespace BNine.Application.Aggregates.Cards.Queries.EmbossPhysicalCard;

using Abstractions;
using Behaviours;
using CommonModels.Dialog;
using MediatR;

[Sequential]
public class EmbossPhysicalCardQuery
    : MBanqSelfServiceUserRequest
    , IRequest<GenericSuccessDialog>
{
}
