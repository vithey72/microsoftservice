﻿namespace BNine.Application.Aggregates.Cards.Queries.EmbossPhysicalCard;

using System.Collections.Immutable;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Abstractions;
using AutoMapper;
using BNine.Application.Aggregates.Cards.Commands.SyncCardStatuses;
using CommonModels.Dialog;
using Domain.Entities;
using Domain.Entities.User;
using Enums.DebitCard;
using Exceptions;
using Helpers;
using Interfaces;
using Interfaces.Bank.Administration;
using MediatR;
using Microsoft.EntityFrameworkCore;

public class EmbossPhysicalCardQueryHandler
    : AbstractRequestHandler
    , IRequestHandler<EmbossPhysicalCardQuery, GenericSuccessDialog>
{
    private static readonly ImmutableHashSet<CardStatus> _cardStatusesThatAllowReissue = new[]
    {
        CardStatus.TERMINATED,
        CardStatus.REPLACED,
        CardStatus.LOST_STOLEN,
        CardStatus.MARK_AS_LOST,
    }.ToImmutableHashSet();

    private readonly IBankInternalCardsService _bankInternalCardsService;
    private readonly IPartnerProviderService _partnerProvider;

    public EmbossPhysicalCardQueryHandler(
        IMediator mediator,
        IBNineDbContext dbContext,
        IMapper mapper,
        ICurrentUserService currentUser,
        IBankInternalCardsService bankInternalCardsService,
        IPartnerProviderService partnerProvider
        )
        : base(mediator, dbContext, mapper, currentUser)
    {
        _bankInternalCardsService = bankInternalCardsService;
        _partnerProvider = partnerProvider;
    }

    public async Task<GenericSuccessDialog> Handle(EmbossPhysicalCardQuery request, CancellationToken token)
    {
        var user = await DbContext.Users
            .Include(u => u.CurrentAccount)
            .Include(u => u.DebitCards)
            .FirstOrDefaultAsync(x => x.Id == CurrentUser.UserId, token);

        if (user == null)
        {
            throw new NotFoundException($"The user with Id = {CurrentUser.UserId} does not exist.");
        }

        await SyncCardStatuses(user.Id, token);

        EnsureUserHasNoPhysicalCards(user);

        await CreatePhysicalCard(request, user, token);

        return new GenericSuccessDialog
        {
            Header = new GenericDialogHeaderViewModel
            {
                Title = $"{StringProvider.GetPartnerName(_partnerProvider)} Debit Card",
            },
            Body = new GenericDialogBodyViewModel
            {
                Title = $"Congrats!\nYou have successfully processed your physical {StringProvider.GetPartnerName(_partnerProvider)} debit card.\n" +
                         "Until you get it you can use it as a virtual card to make purchases.",
                Subtitle = "You'll get your physical card within 5-10 business days. " +
                           "When you receive your card, don't forget to activate it in the app.",
                ButtonText = "DONE",
            }
        };
    }

    private Task SyncCardStatuses(Guid userId, CancellationToken cancellationToken)
    {
        return Mediator.Send(new SyncDebitCardStatusesCommand
        {
            UserId = userId
        }, cancellationToken);
    }

    private static void EnsureUserHasNoPhysicalCards(User user)
    {
        if (user.DebitCards
            .Any(x => x.Type == CardType.PhysicalDebit && !_cardStatusesThatAllowReissue.Contains(x.Status)))
        {
            throw new ValidationException("card", "You already have a physical debit card");
        }
    }

    private async Task CreatePhysicalCard(EmbossPhysicalCardQuery request, User user, CancellationToken cancellationToken)
    {
        var physicalCardProduct =
            await DbContext
                .CardProducts
                .SingleAsync(x => x.AssociatedType == CardType.PhysicalDebit);

        var externalCardId = await _bankInternalCardsService
            .CreateInternalCardREST(
                user.ExternalClientId!.Value,
                user.CurrentAccount.ExternalId,
                physicalCardProduct.ExternalId);
        await _bankInternalCardsService.EnableOnlinePayments(externalCardId);

        user.DebitCards.Add(new DebitCard
        {
            Status = CardStatus.ACTIVE,
            ShippingStatus = CardShippingStatus.IN_TRANSIT,
            ExternalId = externalCardId
        });
        await DbContext.SaveChangesAsync(cancellationToken);
    }
}
