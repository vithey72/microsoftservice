namespace BNine.Application.Aggregates.Cards.Queries.GetCardStatus
{
    using BNine.Application.Abstractions;
    using BNine.Application.Aggregates.Cards.Models;
    using MediatR;

    public class GetAllCardsStatusQuery : MBanqSelfServiceUserRequest, IRequest<List<CardStatusModel>>
    {
    }
}
