﻿namespace BNine.Application.Aggregates.Cards.Queries.GetCardStatus;

using Abstractions;
using AutoMapper;
using Configuration.Queries.GetConfiguration;
using Constants;
using Domain.Entities;
using Enums.DebitCard;
using Interfaces;
using Interfaces.Bank.Client;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Models;

#nullable enable

public class GetCardStatusBaseHandler : AbstractRequestHandler
{
    public const string YourDebitCardWillArrive = "Your debit card will arrive to your registered address";

    private readonly IBankInternalCardsService _bankInternalCardsService;
    private readonly IConfiguration _configuration;

    public GetCardStatusBaseHandler(
        IMediator mediator,
        IBNineDbContext dbContext,
        IMapper mapper,
        ICurrentUserService currentUser,
        IBankInternalCardsService bankInternalCardsService,
        IConfiguration configuration
        )
        : base(mediator, dbContext, mapper, currentUser)
    {
        _bankInternalCardsService = bankInternalCardsService;
        _configuration = configuration;
    }

    protected async Task<CardStatusModel?> ToCardViewModel(MBanqSelfServiceUserRequest request, DebitCard? card)
    {
        if (card is null)
        {
            return null;
        }

        if (card.Type == CardType.PhysicalDebit)
        {
            return await ToPhysicalDebitCardViewModel(request, card, CancellationToken.None);
        }

        if (card.Type == CardType.VirtualDebit)
        {
            return await ToVirtualDebitCardViewModel(request, card, CancellationToken.None);
        }

        return null;
    }

    protected async Task<CardStatusModel> ToPhysicalDebitCardViewModel(
        MBanqSelfServiceUserRequest request,
        DebitCard card,
        CancellationToken cancellationToken)
    {
        var cardInfo = await _bankInternalCardsService.GetCardInfo(
            request.MbanqAccessToken,
            request.Ip,
            card.ExternalId);

        var shippingDetails = await _bankInternalCardsService.GetCardShippingHistory(
            request.MbanqAccessToken,
            request.Ip,
            card.ExternalId);

        var isFirstCard = (await DbContext
            .DebitCards
            .CountAsync(dc => dc.UserId == CurrentUser.UserId && dc.Type == CardType.PhysicalDebit, cancellationToken)) == 1;

        var returnCard = ConstructPhysicalCardViewModel(cardInfo, shippingDetails, card, isFirstCard);

        return returnCard;
    }

    protected async Task<CardStatusModel> ToVirtualDebitCardViewModel(
        MBanqSelfServiceUserRequest request,
        DebitCard card,
        CancellationToken cancellationToken)
    {
        var cardInfo = await _bankInternalCardsService.GetCardInfo(
            request.MbanqAccessToken,
            request.Ip,
            card.ExternalId);

        var configuration = await Mediator.Send(new GetConfigurationQuery(), cancellationToken);
        var isFeatureEnabled = configuration.HasFeatureEnabled(FeaturesNames.Features.NeedPinForVirtualCard);

        return ConstructVirtualCardViewModel(cardInfo, card, isFeatureEnabled);
    }

    private CardStatusModel ConstructPhysicalCardViewModel(
        CardInfoModel cardInfo, ShippingHistoryModel shippingHistory, DebitCard card, bool isFirstCard)
    {
        var shippedOn = shippingHistory.ShippedOn ?? card.CreatedAt;
        var status = (cardInfo.Status, shippingHistory.Status, isFirstCard, cardInfo.PhysicalCardActivated) switch
        {
            _ when shippedOn.HasValue && DateTime.Now.Subtract(shippedOn.Value) < GetShippingCooldown()
                => CardDisplayedStatus.NotActivatable,
            (CardStatus.ACTIVE, _, true, false) => CardDisplayedStatus.ActiveOnlineOnly,
            (CardStatus.ACTIVE, CardShippingStatus.ACTIVATED, _, _) => CardDisplayedStatus.Active,
            (CardStatus.ACTIVE, _, _, true) => CardDisplayedStatus.Active,
            (CardStatus.ACTIVE, _, false, false) => CardDisplayedStatus.Reordered,
            (CardStatus.TERMINATED, CardShippingStatus.IN_TRANSIT, _, _) when card.ReplacedByUserRequest ?? false
                => CardDisplayedStatus.ProcessingReorder,
            (CardStatus.CREATED, _, false, _) => CardDisplayedStatus.ProcessingReorder,
            (CardStatus.SUSPENDED, _, _, _) => CardDisplayedStatus.Locked,
            _ => CardDisplayedStatus.Unknown,
        };

        return new CardStatusModel
        {
            CardId = card.ExternalId,
            CardType = CardType.PhysicalDebit,
            Status = status,
            PrimaryAccountNumber = cardInfo.PrimaryAccountNumber
        };
    }

    private CardStatusModel ConstructVirtualCardViewModel(CardInfoModel cardModel, DebitCard card, bool isExpectingSetPinStatusEnabled)
    {
        var status = cardModel.Status switch
        {
            CardStatus.ACTIVE when !card.IsPinSet && isExpectingSetPinStatusEnabled => CardDisplayedStatus.ExpectingSetPin,
            CardStatus.ACTIVE => CardDisplayedStatus.Active,
            CardStatus.CREATED => CardDisplayedStatus.Reordered,
            CardStatus.TERMINATED => CardDisplayedStatus.Unknown,
            CardStatus.SUSPENDED => CardDisplayedStatus.Locked,
            _ => CardDisplayedStatus.Unknown,
        };

        return new CardStatusModel
        {
            CardId = cardModel.ExternalId,
            CardType = CardType.VirtualDebit,
            Status = status,
            PrimaryAccountNumber = cardModel.PrimaryAccountNumber
        };
    }

    private TimeSpan GetShippingCooldown()
    {
        try
        {
            return TimeSpan.Parse(_configuration["CardShippingCooldown"]);
        }
        catch
        {
            return TimeSpan.FromHours(24);
        }
    }
    
    protected CardStatusModel NoPhysicalCard()
    {
        var cardStatusModel = CreateNoPhysicalCardModel();

        return cardStatusModel;
    }

    protected async Task<CardStatusModel> TerminatedPhysicalCard(
        MBanqSelfServiceUserRequest request,
        DebitCard card,
        CancellationToken cancellationToken)
    {
        var cardStatusModel = CreateNoPhysicalCardModel();

        await ApplyTerminatedCardInfo(cardStatusModel, request, card, cancellationToken);

        return cardStatusModel;
    }

    private CardStatusModel CreateNoPhysicalCardModel()
    {
        return new CardStatusModel
        {
            Status = CardDisplayedStatus.NoCard,
            NoCardMessageHeader = YourDebitCardWillArrive,
            CardType = CardType.PhysicalDebit,
            IsTerminated = false,
            CardIssueWidgetType = null,
        };
    }

    private async Task ApplyTerminatedCardInfo(
        CardStatusModel cardStatusModel,
        MBanqSelfServiceUserRequest request,
        DebitCard card,
        CancellationToken cancellationToken)
    {
        var cardInfo = await _bankInternalCardsService.GetCardInfo(
            request.MbanqAccessToken,
            request.Ip,
            card.ExternalId);

        cardStatusModel.CardId = card.ExternalId;
        cardStatusModel.IsTerminated = true;
        cardStatusModel.PrimaryAccountNumber = cardInfo.PrimaryAccountNumber;
    }
}
