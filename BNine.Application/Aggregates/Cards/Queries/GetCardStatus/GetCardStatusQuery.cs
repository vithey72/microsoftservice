﻿namespace BNine.Application.Aggregates.Cards.Queries.GetCardStatus
{
    using BNine.Application.Abstractions;
    using BNine.Application.Aggregates.Cards.Models;
    using MediatR;

    public class GetCardStatusQuery : MBanqSelfServiceUserRequest, IRequest<CardStatusModel>
    {
        public int? CardId
        {
            get; set;
        }
    }
}
