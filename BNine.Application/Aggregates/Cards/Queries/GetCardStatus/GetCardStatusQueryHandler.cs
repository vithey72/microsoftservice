﻿namespace BNine.Application.Aggregates.Cards.Queries.GetCardStatus
{
    using System.Threading;
    using System.Threading.Tasks;
    using AutoMapper;
    using Commands.SyncCardStatuses;
    using Models;
    using Interfaces;
    using Enums.DebitCard;
    using GetMostRecentCard;
    using Interfaces.Bank.Client;
    using MediatR;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.Extensions.Configuration;

    public class GetCardStatusQueryHandler
        : GetCardStatusBaseHandler
        , IRequestHandler<GetCardStatusQuery, CardStatusModel>
    {

        public GetCardStatusQueryHandler(
            IMediator mediator,
            IBNineDbContext dbContext,
            IMapper mapper,
            ICurrentUserService currentUser,
            IBankInternalCardsService bankInternalCardsService,
            IConfiguration configuration
            )
            : base(mediator, dbContext, mapper, currentUser, bankInternalCardsService, configuration)
        {
        }

        public async Task<CardStatusModel> Handle(GetCardStatusQuery request, CancellationToken token)
        {
            await Mediator.Send(new SyncDebitCardStatusesCommand
            {
                UserId = CurrentUser.UserId.Value
            }, token);

            var cardCount = await DbContext.DebitCards
                .CountAsync(dc => dc.UserId == CurrentUser.UserId, token);
            if (cardCount == 0)
            {
                return NoPhysicalCard();
            }

            var card = await Mediator.Send(new GetMostRecentDebitCardQuery(null, request.CardId), token);

            // old clients only
            if (card == null)
            {
                return new CardStatusModel
                {
                    Status = CardDisplayedStatus.Unknown,
                };
            }

            return await base.ToCardViewModel(request, card);
        }
    }
}
