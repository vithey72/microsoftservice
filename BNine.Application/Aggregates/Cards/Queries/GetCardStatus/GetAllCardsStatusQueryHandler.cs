﻿namespace BNine.Application.Aggregates.Cards.Queries.GetCardStatus;

using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using Commands.SyncCardStatuses;
using Models;
using Interfaces;
using Enums.DebitCard;
using GetMostRecentCard;
using Interfaces.Bank.Client;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using BNine.Domain.Entities;

using IBankSavingAccountsService = Interfaces.Bank.Administration.IBankSavingAccountsService;

#nullable enable

public class GetAllCardsStatusQueryHandler
    : GetCardStatusBaseHandler, IRequestHandler<GetAllCardsStatusQuery, List<CardStatusModel>>
{
    private readonly IBankClientAddressService _bankClientAddressService;
    private readonly IBankSavingAccountsService _bankSavingAccountsService;

    public GetAllCardsStatusQueryHandler(
        IMediator mediator,
        IBNineDbContext dbContext,
        IMapper mapper,
        ICurrentUserService currentUser,
        IBankInternalCardsService bankInternalCardsService,
        IBankClientAddressService bankClientAddressService,
        IBankSavingAccountsService bankSavingAccountsService,
        IConfiguration configuration)
        : base(mediator, dbContext, mapper, currentUser, bankInternalCardsService, configuration)
    {
        _bankClientAddressService = bankClientAddressService;
        _bankSavingAccountsService = bankSavingAccountsService;
    }

    public async Task<List<CardStatusModel>> Handle(GetAllCardsStatusQuery request, CancellationToken cancellationToken)
    {
        var userId = CurrentUser.UserId!.Value;

        await SyncCardStatuses(userId, cancellationToken);

        var cardsToReturn = new List<CardStatusModel>();
        CardStatusModel? noCardEntry = null;

        var cardCount = await DbContext.DebitCards.CountAsync(dc => dc.UserId == userId, cancellationToken);
        if (cardCount > 0)
        {
            var physicalCard = await TryGetMostRecentPhysicalDebitCard(userId, cancellationToken);
            var virtualCard = await TryGetMostRecentVirtualDebitCard(cancellationToken);

            if (physicalCard is not null)
            {
                if (IsInActiveState(physicalCard))
                {
                    var cardToAdd = await ToPhysicalDebitCardViewModel(request, physicalCard, cancellationToken);
                    cardsToReturn.Add(cardToAdd);
                }
                else if (IsInTerminatedState(physicalCard))
                {
                    noCardEntry = await TerminatedPhysicalCard(request, physicalCard, cancellationToken);
                }
                else
                {
                    noCardEntry = NoPhysicalCard();
                }
            }
            else
            {
                noCardEntry = NoPhysicalCard();
            }

            if (virtualCard is not null)
            {
                var cardToAdd = await ToVirtualDebitCardViewModel(request, virtualCard, cancellationToken);
                cardsToReturn.Add(cardToAdd);
            }
        }
        else
        {
            noCardEntry = NoPhysicalCard();
        }

        if (noCardEntry is not null)
        {
            await ApplyCardIssueWidgetType(noCardEntry, userId);
            cardsToReturn.Add(noCardEntry);
        }

        return cardsToReturn;
    }

    private static bool IsInActiveState(DebitCard debitCard)
    {
        return activeCardStatuses.Contains(debitCard.Status);
    }

    private static bool IsInTerminatedState(DebitCard debitCard)
    {
        return terminatedCardStatuses.Contains(debitCard.Status);
    }

    private static readonly CardStatus[] activeCardStatuses = new[]
    {
        CardStatus.ORDERED,
        CardStatus.ACTIVE,
        CardStatus.SUSPENDED,
        CardStatus.CREATED
    };

    private static readonly CardStatus[] terminatedCardStatuses = new[]
    {
        CardStatus.TERMINATED,
        CardStatus.REJECTED,
        CardStatus.REPLACED,
        CardStatus.LOST_STOLEN,
        CardStatus.MARK_AS_LOST
    };

    private Task SyncCardStatuses(Guid userId, CancellationToken cancellationToken)
    {
        return Mediator.Send(new SyncDebitCardStatusesCommand
        {
            UserId = userId
        }, cancellationToken);
    }

    private async Task<DebitCard?> TryGetMostRecentPhysicalDebitCard(Guid userId, CancellationToken cancellationToken)
    {
        var queryDb = DbContext.DebitCards
            .Where(x => x.UserId == userId)
            .Where(x => x.Type == CardType.PhysicalDebit)
            .OrderByDescending(x => activeCardStatuses.Contains(x.Status))
            .ThenByDescending(x => x.ExternalId);

        var card = await queryDb
            .FirstOrDefaultAsync(cancellationToken);

        return card;
    }

    private Task<DebitCard?> TryGetMostRecentVirtualDebitCard(CancellationToken cancellationToken)
    {
        return Mediator.Send(new GetMostRecentDebitCardQuery(CardType.VirtualDebit), cancellationToken);
    }

    private async Task ApplyCardIssueWidgetType(
        CardStatusModel noCardEntry,
        Guid userId)
    {
        noCardEntry.CardIssueWidgetType = CardIssueWidgetType.ContactSupport;

        var user = await DbContext.Users.Include(x => x.CurrentAccount).FirstOrDefaultAsync(x => x.Id == userId);

        if (user?.ExternalClientId == null)
        {
            return;
        }

        var addresses = await _bankClientAddressService.GetAddresses(user.ExternalClientId.Value);
        if (addresses.Count == 0)
        {
            return;
        }
        if (addresses.All(x => !x.IsActive))
        {
            return;
        }

        var account = await _bankSavingAccountsService.GetSavingAccountInfo(
            user.CurrentAccount.ExternalId);

        if (account.IsCardOrderingRestricted)
        {
            return;
        }

        noCardEntry.CardIssueWidgetType = CardIssueWidgetType.IssueNewCard;
    }
}
