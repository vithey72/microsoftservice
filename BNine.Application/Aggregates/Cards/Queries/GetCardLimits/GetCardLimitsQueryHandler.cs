﻿namespace BNine.Application.Aggregates.Cards.Queries.GetCardLimitsObsolete
{
    using System.Threading;
    using System.Threading.Tasks;
    using Abstractions;
    using AutoMapper;
    using Exceptions;
    using GetCardLimits;
    using Interfaces;
    using Interfaces.Bank.Administration;
    using Interfaces.DwhIntegration;
    using MediatR;
    using Users.Models.CardLimits;

    public class GetCardLimitsQueryHandler
        : AbstractRequestHandler, IRequestHandler<GetCardLimitsQuery, UserCardLimitsViewModel>
    {
        private IBankInternalCardsService BankInternalCardsService
        {
            get;
        }

        public GetCardLimitsQueryHandler(
            IMediator mediator,
            IBNineDbContext dbContext,
            IMapper mapper,
            ICurrentUserService currentUser,
            IDwhIntegrationService dwhIntegrationService)
            : base(mediator, dbContext, mapper, currentUser)
        {
            DwhIntegrationService = dwhIntegrationService;
        }

        private IDwhIntegrationService DwhIntegrationService
        {
            get;
        }

        public async Task<UserCardLimitsViewModel> Handle(GetCardLimitsQuery request,
            CancellationToken cancellationToken)
        {
            var userTransactionLimits = await DwhIntegrationService.GetUserCardLimits(CurrentUser.UserId.Value);

            if (userTransactionLimits == null)
            {
                throw new NotFoundException("Card limits not found for current user", CurrentUser.UserId);
            }

            return userTransactionLimits;
        }
    }
}
