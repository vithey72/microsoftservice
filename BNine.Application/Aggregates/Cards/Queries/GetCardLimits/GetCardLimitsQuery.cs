﻿namespace BNine.Application.Aggregates.Cards.Queries.GetCardLimits
{
    using MediatR;
    using Models;
    using Users.Models.CardLimits;

    public class GetCardLimitsQuery : DebitCardBaseModel, IRequest<UserCardLimitsViewModel>
    {
    }
}
