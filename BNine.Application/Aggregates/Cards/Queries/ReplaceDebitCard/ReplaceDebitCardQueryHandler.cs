﻿namespace BNine.Application.Aggregates.Cards.Queries.ReplaceDebitCard
{
    using System.Threading;
    using System.Threading.Tasks;
    using AutoMapper;
    using BNine.Application.Abstractions;
    using BNine.Application.Interfaces;
    using BNine.Constants;
    using BNine.Domain.Entities.User;
    using BNine.Enums;
    using BNine.ResourceLibrary;
    using Commands.UnblockCard;
    using CommonModels.Dialog;
    using Enums.DebitCard;
    using Exceptions;
    using Helpers;
    using Interfaces.Bank.Administration;
    using MediatR;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.Extensions.Localization;
    using Microsoft.Extensions.Logging;
    using Queries.GetMostRecentCard;
    using IBankInternalCardsService = Interfaces.Bank.Client.IBankInternalCardsService;
    using IBankSavingAccountsService = Interfaces.Bank.Client.IBankSavingAccountsService;

    public class ReplaceDebitCardQueryHandler
        : ReplaceDebitCardCommandHandlerBase, IRequestHandler<ReplaceDebitCardQuery, EitherDialog<GenericSuccessDialog, GenericFailDialog>>
    {
        private const int MaxDamagedReplacementsPerCard = 3;
        private const int PhysicalCardReplacementLimit = 3;

        private readonly IPaidUserServicesService _paidUserServices;
        private readonly IBankSavingAccountsService _savingAccountsService;
        private readonly IPartnerProviderService _partnerProvider;

        private IBankInternalCardsService BankInternalCardsService
        {
            get;
        }

        private INotificationUsersService NotificationUsersService
        {
            get;
        }

        private IStringLocalizer<SharedResource> SharedLocalizer
        {
            get;
        }

        public ReplaceDebitCardQueryHandler(
            IStringLocalizer<SharedResource> sharedLocalizer,
            IMediator mediator,
            IBNineDbContext dbContext,
            IMapper mapper,
            ICurrentUserService currentUser,
            INotificationUsersService notificationUsersService,
            IBankInternalCardsService bankInternalCardsService,
            IPaidUserServicesService paidUserServices,
            IBankSavingAccountsService savingAccountsService,
            ILogger<ReplaceDebitCardQueryHandler> logger,
            IPartnerProviderService partnerProvider
        )
            : base(
                mediator,
                dbContext,
                mapper,
                currentUser,
                sharedLocalizer,
                notificationUsersService,
                logger,
                partnerProvider
                )
        {
            _paidUserServices = paidUserServices;
            _savingAccountsService = savingAccountsService;
            _partnerProvider = partnerProvider;
            BankInternalCardsService = bankInternalCardsService;
            NotificationUsersService = notificationUsersService;
            SharedLocalizer = sharedLocalizer;
        }

        public async Task<EitherDialog<GenericSuccessDialog, GenericFailDialog>> Handle(ReplaceDebitCardQuery request, CancellationToken cancellationToken)
        {
            // because damaged reissue flow doesn't work that well: https://bninecom.atlassian.net/browse/B9-3326
            if (request.ReplacementReason == CardReplacementReason.DAMAGED)
            {
                request.ReplacementReason = CardReplacementReason.LOST;
            }

            var card = await Mediator.Send(new GetMostRecentDebitCardQuery(CardType.PhysicalDebit, request.CardId), cancellationToken);
            var user = await DbContext.Users
                .Include(u => u.Devices)
                .Include(u => u.CurrentAccount)
                .FirstOrDefaultAsync(u => u.Id == CurrentUser.UserId.Value, cancellationToken);

            if (card == null)
            {
                throw new ValidationException("request", "There is no physical card associated with this client.");
            }

            var cardStatus = await BankInternalCardsService.GetCardInfo(
                    request.MbanqAccessToken,
                    request.Ip,
                    card.ExternalId);

            await CheckForReplacementLimit(CurrentUser.UserId.Value, CardType.PhysicalDebit, PhysicalCardReplacementLimit);
            if (FailResultDialog?.IsSuccess == false)
            {
                return FailResultDialog;
            }

            if (cardStatus.DamagedReasonReplacementsCount >= MaxDamagedReplacementsPerCard)
            {
                throw new ValidationException(
                    "request",
                    "Card order failed. Damaged card replacement limit has been exhausted. " +
                    $"Please get in touch with our Customer Support team at {StringProvider.GetSupportEmail(_partnerProvider)}.");
            }

            if (cardStatus.Status == CardStatus.ACTIVE)
            {
                await BankInternalCardsService.ChangeCardStatus(
                    request.MbanqAccessToken,
                    request.Ip,
                    card.ExternalId,
                    HandleCardEventCommand.SUSPEND.ToString());
            }

            int newCardId;
            try
            {
                newCardId = await BankInternalCardsService.ReplaceCard(
                    request.MbanqAccessToken,
                    request.Ip,
                    card.ExternalId,
                    request.ReplacementReason.ToString());
                await _paidUserServices.MarkServiceAsUsed(PaidUserServices.PhysicalCardReplacement);
            }
            catch (ValidationException ex)
            {
                base.HandleMBanqExceptions(ex);

                throw;
            }

            if (newCardId != card.ExternalId)
            {
                base.ReplaceDebitCardInB9Database(cancellationToken, card, newCardId);
            }
            else if (request.ReplacementReason == CardReplacementReason.DAMAGED)
            {
                await Mediator.Send(new UnblockCardCommand
                {
                    Ip = request.Ip,
                    MbanqAccessToken = request.MbanqAccessToken,
                });
            }

            await base.NotifyUser(request, user);

            return new EitherDialog<GenericSuccessDialog, GenericFailDialog>(
                new GenericSuccessDialog
                {
                    Header = new GenericDialogHeaderViewModel
                    {
                        Title = "Get your card",
                        Subtitle = "Physical card arrives within 5-10 business days",
                    },
                    Body = new GenericDialogBodyViewModel
                    {
                        Subtitle = $"Your new card will be ready for use in a few hours. \nAccess it any time from the {StringProvider.GetPartnerName(_partnerProvider)} App. \nYour physical card will arrive soon.",
                        ButtonText = "DONE",
                    }
                });
        }

        private async Task<EitherDialog<GenericSuccessDialog, GenericFailDialog>> ChargeUserIfNecessary(User user, MBanqSelfServiceUserRequest request)
        {
            var replacementsCount = await BankInternalCardsService.CountReplacements(
                request.MbanqAccessToken,
                request.Ip,
                user.ExternalClientId!.Value);
            var replacementServiceId = PaidUserServices.PhysicalCardReplacement;

            // only first replacement is free
            if (replacementsCount > 0)
            {
                var accountStatus = await _savingAccountsService.GetSavingAccountInfo(
                    user!.CurrentAccount.ExternalId,
                    request.MbanqAccessToken,
                    request.Ip);

                var serviceCost = await _paidUserServices.GetServiceCost(replacementServiceId);
                if (serviceCost > accountStatus.AvailableBalance)
                {
                    return new EitherDialog<GenericSuccessDialog, GenericFailDialog>(
                        new GenericFailDialog
                        {
                            ErrorCode = ApiResponseErrorCodes.InsufficientFunds,
                            Body = new GenericDialogBodyViewModel
                            {
                                Title = "Insufficient funds",
                                Subtitle = "To order a physical card you need to have at least " +
                                           $"{CurrencyFormattingHelper.AsRegular(serviceCost)} in your account",
                                ButtonText = "ADD MONEY",
                            },
                        });
                }
                if (!await _paidUserServices.ServiceIsAlreadyPurchased(replacementServiceId))
                {
                    await _paidUserServices.EnableService(replacementServiceId);
                }
            }

            return null;
        }
    }
}
