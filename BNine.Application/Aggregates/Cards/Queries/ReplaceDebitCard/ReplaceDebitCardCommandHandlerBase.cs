﻿namespace BNine.Application.Aggregates.Cards.Queries.ReplaceDebitCard;

using Abstractions;
using AutoMapper;
using CommonModels.Dialog;
using Constants;
using Domain.Entities;
using Domain.Entities.User;
using Enums;
using Enums.DebitCard;
using Exceptions;
using Helpers;
using Interfaces;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Localization;
using Microsoft.Extensions.Logging;
using ResourceLibrary;

public abstract class ReplaceDebitCardCommandHandlerBase : AbstractRequestHandler
{
    private readonly IPartnerProviderService _partnerProvider;

    private IStringLocalizer<SharedResource> SharedLocalizer
    {
        get;
    }

    private INotificationUsersService NotificationUsersService
    {
        get;
    }

    private ILogger Logger
    {
        get;
    }

    protected EitherDialog<GenericSuccessDialog, GenericFailDialog> FailResultDialog
    {
        get;
        set;
    }

    protected ReplaceDebitCardCommandHandlerBase(
        IMediator mediator,
        IBNineDbContext dbContext,
        IMapper mapper,
        ICurrentUserService currentUser,
        IStringLocalizer<SharedResource> sharedLocalizer,
        INotificationUsersService notificationUsersService,
        ILogger logger,
        IPartnerProviderService partnerProvider
        )
        : base(mediator,
        dbContext, mapper, currentUser)
    {
        _partnerProvider = partnerProvider;
        SharedLocalizer = sharedLocalizer;
        NotificationUsersService = notificationUsersService;
        Logger = logger;
    }

    protected async Task ReplaceDebitCardInB9Database(CancellationToken cancellationToken, DebitCard oldCard,
        int newCardId)
    {
        oldCard.Status = CardStatus.TERMINATED;
        oldCard.TerminatedAt = DateTime.UtcNow;
        oldCard.ReplacedByUserRequest = true;

        var newCard = new DebitCard
        {
            ExternalId = newCardId,
            Status = CardStatus.ACTIVE,
            ShippingStatus = CardShippingStatus.IN_TRANSIT,
            UserId = CurrentUser.UserId!.Value
        };

        await DbContext.DebitCards.AddAsync(newCard, cancellationToken);
        await DbContext.SaveChangesAsync(cancellationToken);
    }

    protected async Task NotifyUser(DebitCardUserRequest command, User user)
    {
        var notificationText = $"Your {StringProvider.GetPartnerName(_partnerProvider)} card has been successfully reissued.";

        try
        {
            await NotificationUsersService.SendNotification(user.Id, notificationText,
                user.Settings.IsNotificationsEnabled, NotificationTypeEnum.Information, true, user.Devices, null,
                null,
                StringProvider.ReplaceCustomizedStrings(PushNotificationCategory.YourB9CardHasBeenSuccessfullyReissued, _partnerProvider));
        }
        catch (Exception ex)
        {
            Logger.LogError("Error occured during Send Notification to user, details: " + ex.Message);
        }
    }

    protected void HandleMBanqExceptions(ValidationException ex)
    {
        if (ex.ErrorCodes.ContainsKey("ReplacementCardLimit"))
        {
            throw new ValidationException("request", SharedLocalizer["replacementCardLimit"].Value);
        }

        if (ex.ErrorCodes.ContainsKey("DamagedCardBeingReplaced"))
        {
            RethrowWithSameMessage(ex);
        }
    }

    /// <summary>
    /// Fill Fail dialog in case of error
    /// </summary>
    /// <param name="cardType"></param>
    /// <param name="replacementLimit"></param>
    protected async Task CheckForReplacementLimit(Guid userId, CardType cardType, int replacementLimit)
    {
        var replacedCards = await DbContext.DebitCards
            .Where(x => x.Status == CardStatus.REPLACED && x.Type == cardType && x.UserId == userId)
            .CountAsync();

        if (replacedCards >= replacementLimit)
        {
            FailResultDialog = new EitherDialog<GenericSuccessDialog, GenericFailDialog>(new GenericFailDialog()
            {
                ErrorCode = ApiResponseErrorCodes.VirtualCardReplacementLimit,
                Body = new GenericDialogBodyViewModel
                {
                    Title = "Something went wrong",
                    Subtitle = @"Please contact our support team at " + StringProvider.GetSupportEmail(_partnerProvider),
                    ButtonText = "DONE",
                },
            });
        }
    }

    private static void RethrowWithSameMessage(ValidationException ex)
    {
        throw new ValidationException("request", ex.Errors.FirstOrDefault().Value.FirstOrDefault());
    }
}
