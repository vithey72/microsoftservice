﻿namespace BNine.Application.Aggregates.Cards.Queries.ReplaceDebitCard;

using Abstractions;
using Behaviours;
using CommonModels.Dialog;
using Enums;
using MediatR;
using Newtonsoft.Json;

[Sequential]
public class ReplaceDebitCardQuery
    : DebitCardUserRequest
    , IRequest<EitherDialog<GenericSuccessDialog, GenericFailDialog>>
{
    [JsonProperty("reason")]
    public CardReplacementReason ReplacementReason
    {
        get; set;
    }
}
