﻿namespace BNine.Application.Aggregates.Cards.Models
{
    using BNine.Common;
    using Enums.DebitCard;
    using Newtonsoft.Json;
    using Newtonsoft.Json.Converters;

    public class CardStatusModel : DebitCardBaseModel
    {
        [JsonConverter(typeof(StringEnumConverter))]
        [JsonProperty("status")]
        public CardDisplayedStatus Status
        {
            get; set;
        }

        [JsonProperty("primaryAccountNumber")]
        public string PrimaryAccountNumber
        {
            get;
            set;
        }

        [JsonProperty("noCardMessageHeader")]
        public string NoCardMessageHeader
        {
            get;
            set;
        }

        [JsonProperty("cardType")]
        [JsonConverter(typeof(StringEnumConverter))]
        public CardType CardType
        {
            get; set;
        }

        [JsonProperty("last4Digits")]
        public string Last4Digits => SecretNumberHelper.GetLastFourDigits(PrimaryAccountNumber);

        [JsonProperty("shippedOn")]
        public DateTime? ShippedOn
        {
            get; set;
        }

        /// <summary>
        /// Value makes sense only in a case of <see cref="Status"/> field state <see cref="CardDisplayedStatus.NoCard"/>
        /// </summary>
        [JsonProperty("cardIssueWidgetType")]
        [JsonConverter(typeof(StringEnumConverter))]
        public CardIssueWidgetType? CardIssueWidgetType
        {
            get; set;
        }

        /// <summary>
        /// Extension for <see cref="Status"/> field state <see cref="CardDisplayedStatus.NoCard"/>
        /// </summary>
        [JsonProperty("isTerminated")]
        public bool IsTerminated
        {
            get; set;
        }
    }
}
