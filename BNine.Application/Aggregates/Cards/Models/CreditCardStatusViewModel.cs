﻿namespace BNine.Application.Aggregates.Cards.Models;

using Enums.DebitCard;

public class CreditCardStatusViewModel
{
    public int? Id
    {
        get;
        set;
    }

    public CardDisplayedStatus Status
    {
        get;
        set;
    }

    public string LastDigits
    {
        get;
        set;
    }

    public decimal? Balance
    {
        get;
        set;
    }
}
