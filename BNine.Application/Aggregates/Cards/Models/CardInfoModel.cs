﻿namespace BNine.Application.Aggregates.Cards.Models
{
    using Enums.DebitCard;

    public class CardInfoModel
    {
        public int ExternalId
        {
            get;
            set;
        }

        public CardStatus Status
        {
            get;
            set;
        }

        public bool OnlinePaymentEnabled
        {
            get;
            set;
        }

        public bool PhysicalCardActivated
        {
            get;
            set;
        }

        public string PrimaryAccountNumber
        {
            get;
            set;
        }

        public int DamagedReasonReplacementsCount
        {
            get;
            set;
        }

        public CardType Type
        {
            get;
            set;
        }
    }
}
