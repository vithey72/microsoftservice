﻿namespace BNine.Application.Aggregates.Cards.Models;

using CommonModels.Dialog;

public class DialogWithCancelButtonViewModel : GenericDialogBasicViewModel
{
    public string BackButtonText
    {
        get;
        set;
    }
}
