﻿namespace BNine.Application.Aggregates.Cards.Models
{
    using Newtonsoft.Json;

    public class ActivationStatusResponseModel
    {
        [JsonProperty("isActivated")]
        public bool IsActivated
        {
            get; set;
        }
    }
}
