﻿namespace BNine.Application.Aggregates.Cards.Models
{
    using Newtonsoft.Json;

    public abstract class DebitCardBaseModel
    {
        [JsonProperty("id")]
        public int? CardId
        {
            get; set;
        }
    }
}
