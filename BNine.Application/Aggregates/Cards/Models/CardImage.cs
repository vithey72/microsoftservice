﻿namespace BNine.Application.Aggregates.Cards.Models
{
    using Newtonsoft.Json;

    public class CardImage
    {
        [JsonProperty("imageLink")]
        public string ImageLink
        {
            get;
            set;
        }
    }
}
