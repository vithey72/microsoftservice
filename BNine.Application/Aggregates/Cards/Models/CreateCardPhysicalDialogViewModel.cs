﻿namespace BNine.Application.Aggregates.Cards.Models;

using CommonModels.Dialog;

public class CreateCardPhysicalDialogViewModel : GenericDialogBasicViewModel
{
    public string BackButtonText
    {
        get;
        set;
    }
}
