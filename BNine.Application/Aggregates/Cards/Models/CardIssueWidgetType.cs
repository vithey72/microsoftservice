﻿namespace BNine.Application.Aggregates.Cards.Models;

using System.Runtime.Serialization;

public enum CardIssueWidgetType
{
    [EnumMember(Value = "issueNewCard")]
    IssueNewCard = 0,

    [EnumMember(Value = "contactSupport")]
    ContactSupport = 1,
}
