﻿namespace BNine.Application.Aggregates.Cards.Models
{
    using Newtonsoft.Json;

    public class CardInfoPay
    {
        [JsonProperty("id")]
        public int Id
        {
            get; set;
        }

        [JsonProperty("last4Numbers")]
        public string Last4Numbers
        {
            get;
            set;
        } = string.Empty;

        [JsonProperty("savingAccountNumber")]
        public string SavingAccountNumber
        {
            get; set;
        }
    }
}
