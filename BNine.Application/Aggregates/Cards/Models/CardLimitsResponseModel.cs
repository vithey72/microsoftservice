﻿namespace BNine.Application.Aggregates.Cards.Models
{
    using Newtonsoft.Json;

    public class CardLimitsResponseModel
    {
        [JsonProperty("cardOperations")]
        public CardLimits CardOperations
        {
            get; set;
        }

        [JsonProperty("cashWithdrawal")]
        public CardLimits CashWithdrawal
        {
            get; set;
        }
    }
}
