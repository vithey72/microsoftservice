﻿namespace BNine.Application.Aggregates.Cards.Models
{
    using Newtonsoft.Json;

    public class PinCodeWebView
    {
        [JsonProperty("url")]
        public string Url
        {
            get;
            set;
        }
    }
}
