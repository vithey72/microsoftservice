﻿namespace BNine.Application.Aggregates.Cards.Models;

using Enums.DebitCard;

public record BlockCardResponse(CardDisplayedStatus Status);
