﻿namespace BNine.Application.Aggregates.Cards.Models
{
    using Enums.DebitCard;
    using Newtonsoft.Json;
    using Newtonsoft.Json.Converters;

    public class ShippingHistoryModel
    {
        [JsonProperty("shippedBy")]
        public string ShippedBy
        {
            get; set;
        }

        [JsonConverter(typeof(StringEnumConverter))]
        [JsonProperty("status")]
        public CardShippingStatus Status
        {
            get; set;
        }

        [JsonProperty("trackingId")]
        public string TrackingId
        {
            get; set;
        }

        [JsonProperty("shippedOn")]
        public DateTime? ShippedOn
        {
            get; set;
        }
    }
}
