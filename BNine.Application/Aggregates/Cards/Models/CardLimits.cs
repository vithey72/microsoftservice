﻿namespace BNine.Application.Aggregates.Cards.Models
{
    using Newtonsoft.Json;

    public class CardLimits
    {
        [JsonProperty("dailyLimit")]
        public decimal? DailyLimit
        {
            get; set;
        }

        [JsonProperty("weeklyLimit")]
        public decimal? WeeklyLimit
        {
            get; set;
        }

        [JsonProperty("monthlyLimit")]
        public decimal? MonthlyLimit
        {
            get; set;
        }
    }
}
