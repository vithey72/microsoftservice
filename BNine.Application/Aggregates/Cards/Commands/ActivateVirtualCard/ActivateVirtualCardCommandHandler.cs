﻿namespace BNine.Application.Aggregates.Cards.Commands.ActivateVirtualCard
{
    using System;
    using System.Threading;
    using System.Threading.Tasks;
    using Abstractions;
    using AutoMapper;
    using CommonModels.Dialog;
    using Domain.Entities;
    using Enums.DebitCard;
    using Exceptions;
    using Interfaces;
    using Interfaces.Bank.Client;
    using MediatR;
    using Queries.GetMostRecentCard;

    public class ActivateVirtualCardCommandHandler
        : AbstractRequestHandler, IRequestHandler<ActivateVirtualCardCommand, EitherDialog<GenericSuccessDialog, GenericFailDialog>>
    {
        private IBankInternalCardsService BankInternalCardsService
        {
            get;
        }

        public ActivateVirtualCardCommandHandler(
            IMediator mediator,
            IBNineDbContext dbContext,
            IMapper mapper,
            ICurrentUserService currentUser,
            IBankInternalCardsService bankInternalCardsService)
            : base(mediator, dbContext, mapper, currentUser)
        {
            BankInternalCardsService = bankInternalCardsService;
        }

        public EitherDialog<GenericSuccessDialog, GenericFailDialog> FailResultDialog
        {
            get;
            set;
        }

        public async Task<EitherDialog<GenericSuccessDialog, GenericFailDialog>> Handle(
            ActivateVirtualCardCommand request,
            CancellationToken cancellationToken)
        {
            var card = await Mediator.Send(new GetMostRecentDebitCardQuery(CardType.VirtualDebit, request.CardId));

            if (card == null)
            {
                throw new NotFoundException(nameof(DebitCard), CurrentUser.UserId);
            }

            if (card.Status != CardStatus.ACTIVE)
            {
                throw new Exception($"Card activation failed, MBanq returned card with {card.Status} status.");
            }

            if (!card.IsPinSet)
            {
                return new EitherDialog<GenericSuccessDialog, GenericFailDialog>(new GenericFailDialog()
                {
                    Header = new GenericDialogHeaderViewModel() { Title = "Activate", Subtitle = null },
                    Body = new GenericDialogBodyViewModel()
                    {
                        Title = "Fail", Subtitle = "PIN code should be set.", ButtonText = "DONE"
                    }
                });
            }

            card.Status = CardStatus.ACTIVE;
            card.ShippingStatus = CardShippingStatus.ACTIVATED;
            card.ActivationDate = DateTime.UtcNow;

            await DbContext.SaveChangesAsync(cancellationToken);

            var successDialog = new EitherDialog<GenericSuccessDialog, GenericFailDialog>(
                new GenericSuccessDialog()
                {
                    Header = new GenericDialogHeaderViewModel() { Title = "Activate", Subtitle = null },
                    Body = new GenericDialogBodyViewModel()
                    {
                        Title = "Success", Subtitle = "Your card will be activated in few minutes.", ButtonText = "DONE"
                    }
                });

            return FailResultDialog ?? successDialog;
        }
    }
}
