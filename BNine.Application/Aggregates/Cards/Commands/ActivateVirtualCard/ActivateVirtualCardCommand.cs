﻿namespace BNine.Application.Aggregates.Cards.Commands.ActivateVirtualCard
{
    using BNine.Application.Aggregates.Cards.Models;
    using CommonModels.Dialog;
    using MediatR;
    using Newtonsoft.Json;

    public class ActivateVirtualCardCommand : DebitCardBaseModel, IRequest<EitherDialog<GenericSuccessDialog, GenericFailDialog>>
    {
        [JsonIgnore]
        public string MbanqAccessToken
        {
            get; set;
        }

        [JsonIgnore]
        public string Ip
        {
            get; set;
        }
    }
}
