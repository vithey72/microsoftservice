﻿namespace BNine.Application.Aggregates.Cards.Commands.UpdateCardLimits
{
    using BNine.ResourceLibrary;
    using FluentValidation;
    using Microsoft.Extensions.Localization;
    using Models;

    public class UpdateCardLimitsCommandValidator : AbstractValidator<UpdateCardLimitsCommand>
    {
        private IStringLocalizer<SharedResource> SharedLocalizer
        {
            get;
        }

        public UpdateCardLimitsCommandValidator(
            IStringLocalizer<SharedResource> sharedLocalizer
            )
        {
            SharedLocalizer = sharedLocalizer;

            RuleFor(x => x.CardOperations)
                .Must(IsValidLimits)
                .WithMessage(SharedLocalizer["IncorrectEnteredDataValidationErrorMessage"].Value);

            RuleFor(x => x.CashWithdrawal)
                .Must(IsValidLimits)
                .WithMessage(SharedLocalizer["IncorrectEnteredDataValidationErrorMessage"].Value);
        }

        private bool IsValidLimits(CardLimits limits)
        {
            if (limits == null)
            {
                return true;
            }

            return limits.DailyLimit >= 0 && limits.WeeklyLimit >= 0 && limits.MonthlyLimit >= 0
                   && limits.DailyLimit <= limits.WeeklyLimit
                   && limits.WeeklyLimit <= limits.MonthlyLimit;
        }
    }
}
