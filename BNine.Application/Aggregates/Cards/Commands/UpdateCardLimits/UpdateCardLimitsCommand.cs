﻿namespace BNine.Application.Aggregates.Cards.Commands.UpdateCardLimits
{
    using BNine.Application.Aggregates.Cards.Models;
    using MediatR;
    using Newtonsoft.Json;

    public class UpdateCardLimitsCommand : DebitCardBaseModel, IRequest<Unit>
    {
        [JsonProperty("cardOperations")]
        public CardLimits CardOperations
        {
            get; set;
        }

        [JsonProperty("cashWithdrawal")]
        public CardLimits CashWithdrawal
        {
            get; set;
        }
    }
}
