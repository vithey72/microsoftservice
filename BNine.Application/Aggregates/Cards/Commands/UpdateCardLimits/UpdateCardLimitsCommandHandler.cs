﻿namespace BNine.Application.Aggregates.Cards.Commands.UpdateCardLimits
{
    using System.Threading;
    using System.Threading.Tasks;
    using Abstractions;
    using AutoMapper;
    using Enums.DebitCard;
    using Exceptions;
    using Interfaces;
    using Interfaces.Bank.Administration;
    using MediatR;
    using Queries.GetMostRecentCard;

    public class UpdateCardLimitsCommandHandler
        : AbstractRequestHandler, IRequestHandler<UpdateCardLimitsCommand, Unit>
    {
        private IBankInternalCardsService BankInternalCardsService
        {
            get;
        }

        public UpdateCardLimitsCommandHandler(
            IMediator mediator,
            IBNineDbContext dbContext,
            IMapper mapper,
            ICurrentUserService currentUser,
            IBankInternalCardsService bankInternalCardsService)
            : base(mediator, dbContext, mapper, currentUser)
        {
            BankInternalCardsService = bankInternalCardsService;
        }

        public async Task<Unit> Handle(UpdateCardLimitsCommand request, CancellationToken cancellationToken)
        {
            return Unit.Value;

            // TODO: Currently, this functionality shall be disabled because it doesn't work at Mbanq side.
            var card = await Mediator.Send(new GetMostRecentDebitCardQuery(request.CardId));

            if (card == null)
            {
                throw new NotFoundException("There is no physical debit card for current user", CurrentUser.UserId);
            }

            if (card.Type == CardType.PhysicalDebit)
            {
                await BankInternalCardsService.SetCardLimits(card.ExternalId, request);
            }
            else
            {
                throw new NotFoundException("There is no limits for a virtual debit card");
            }

            return Unit.Value;
        }
    }
}
