﻿namespace BNine.Application.Aggregates.Cards.Commands.SyncCardStatuses
{
    using MediatR;

    public class SyncDebitCardStatusesCommand : IRequest
    {
        public Guid UserId
        {
            get;
            set;
        }
    }
}
