﻿namespace BNine.Application.Aggregates.Cards.Commands.SyncCardStatuses
{
    using Abstractions;
    using AutoMapper;
    using Domain.Entities;
    using Enums.DebitCard;
    using Interfaces;
    using MediatR;
    using Microsoft.EntityFrameworkCore;
    using Models;
    using IBankInternalCardsService = BNine.Application.Interfaces.Bank.Administration.IBankInternalCardsService;

    public class SyncDebitCardStatusesCommandHandler :
        AbstractRequestHandler,
        IRequestHandler<SyncDebitCardStatusesCommand>
    {
        private readonly IBankInternalCardsService _debitCardsService;

        public SyncDebitCardStatusesCommandHandler(
            IMediator mediator,
            IBNineDbContext dbContext,
            IMapper mapper,
            IBankInternalCardsService debitCardsService,
            ICurrentUserService currentUser) : base(mediator, dbContext, mapper, currentUser)
        {
            _debitCardsService = debitCardsService;
        }

        public async Task<Unit> Handle(SyncDebitCardStatusesCommand request, CancellationToken cancellationToken)
        {
            var userId = DbContext.Users.FirstOrDefault(u => u.Id == request.UserId)!.ExternalClientId;

            var cards = await _debitCardsService.GetAllCards(userId.Value);

            foreach (var card in cards)
            {
                await UpdateIfNecessary(card, request);
            }

            await DbContext.SaveChangesAsync(cancellationToken);

            return Unit.Value;
        }

        private async Task UpdateIfNecessary(CardInfoModel card, SyncDebitCardStatusesCommand request)
        {
            var dbCardEntity = await DbContext.DebitCards.FirstOrDefaultAsync(dc => dc.ExternalId == card.ExternalId);
            if (dbCardEntity == null)
            {
                var newCardEntity = new DebitCard
                {
                    UserId = request.UserId,
                    CreatedAt = DateTime.Now,
                    ExternalId = card.ExternalId,
                    Status = card.Status,
                    ShippingStatus = card.Type == CardType.VirtualDebit ? CardShippingStatus.ACTIVATED : CardShippingStatus.IN_TRANSIT,
                    Type = card.Type
                };
                await DbContext.DebitCards.AddAsync(newCardEntity);
                return;
            }

            if (dbCardEntity.Type != card.Type)
            {
                dbCardEntity.Type = card.Type;
            }

            if (dbCardEntity.Status != card.Status)
            {
                dbCardEntity.Status = card.Status;
                switch (card.Status)
                {
                    case CardStatus.REPLACED or CardStatus.TERMINATED or CardStatus.LOST_STOLEN or CardStatus.MARK_AS_LOST:
                        dbCardEntity.TerminatedAt ??= DateTime.Now;
                        break;
                    case CardStatus.ORDERED or CardStatus.CREATED or CardStatus.ACTIVE:
                        dbCardEntity.CreatedAt ??= DateTime.Now;
                        break;
                    case CardStatus.SUSPENDED:
                        dbCardEntity.SuspendedAt ??= DateTime.Now;
                        break;
                }
            }
        }
    }
}
