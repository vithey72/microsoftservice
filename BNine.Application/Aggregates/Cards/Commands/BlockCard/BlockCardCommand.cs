﻿namespace BNine.Application.Aggregates.Cards.Commands.BlockCard;

using Abstractions;
using MediatR;
using Models;

public class BlockCardCommand : DebitCardUserRequest, IRequest<BlockCardResponse>
{
}
