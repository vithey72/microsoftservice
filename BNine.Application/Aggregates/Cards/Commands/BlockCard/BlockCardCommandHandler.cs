﻿namespace BNine.Application.Aggregates.Cards.Commands.BlockCard
{
    using System;
    using System.Threading;
    using System.Threading.Tasks;
    using AutoMapper;
    using BNine.Constants;
    using Common;
    using Domain.Entities;
    using Domain.Entities.User;
    using Emails.Commands.Card.Blocked;
    using Enums;
    using Enums.DebitCard;
    using Exceptions;
    using Helpers;
    using Interfaces;
    using Interfaces.Bank.Client;
    using MediatR;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.Extensions.Configuration;
    using Microsoft.Extensions.Logging;
    using Models;
    using Queries.GetCardStatus;
    using Queries.GetMostRecentCard;

    public class BlockCardCommandHandler
        : GetCardStatusBaseHandler
        , IRequestHandler<BlockCardCommand, BlockCardResponse>
    {

        private readonly ISmsService _smsService;
        private readonly INotificationUsersService _notificationUsersService;
        private readonly IBankInternalCardsService _bankInternalCardsService;
        private readonly IPartnerProviderService _partnerProvider;
        private readonly ILogger<BlockCardCommandHandler> _logger;

        public BlockCardCommandHandler(
            ISmsService smsService,
            INotificationUsersService notificationUsersService,
            IMediator mediator,
            IBNineDbContext dbContext,
            IMapper mapper,
            ICurrentUserService currentUser,
            IConfiguration configuration,
            IBankInternalCardsService bankInternalCardsService,
            IPartnerProviderService partnerProvider,
            ILogger<BlockCardCommandHandler> logger
            )
            : base(mediator, dbContext, mapper, currentUser, bankInternalCardsService, configuration)
        {
            _partnerProvider = partnerProvider;
            _logger = logger;
            _smsService = smsService;
            _notificationUsersService = notificationUsersService;
            _bankInternalCardsService = bankInternalCardsService;
        }

        public async Task<BlockCardResponse> Handle(BlockCardCommand request, CancellationToken cancellationToken)
        {
            var card = await Mediator.Send(new GetMostRecentDebitCardQuery(null, request.CardId));
            var user = await DbContext.Users
                .Include(u => u.Devices)
                .FirstOrDefaultAsync(u => u.Id == CurrentUser.UserId.Value, cancellationToken);

            if (card == null)
            {
                throw new NotFoundException(nameof(DebitCard), CurrentUser.UserId);
            }

            var cardInfo =
                await _bankInternalCardsService.GetCardInfo(request.MbanqAccessToken, request.Ip, card.ExternalId);

            // unless we move card to active status, we can't suspend operations
            if (cardInfo.Status == CardStatus.ORDERED)
            {
                await _bankInternalCardsService.ChangeCardStatus(
                    request.MbanqAccessToken,
                    request.Ip,
                    card.ExternalId,
                    HandleCardEventCommand.ACTIVATE.ToString());
            }

            await _bankInternalCardsService.ChangeCardStatus(
                request.MbanqAccessToken,
                request.Ip,
                card.ExternalId,
                HandleCardEventCommand.SUSPEND.ToString());

            card.Status = CardStatus.SUSPENDED;
            card.SuspendedAt = DateTime.UtcNow;
            await DbContext.SaveChangesAsync(cancellationToken);

            await NotifyUser(request, card, user);

            var cardViewModel = await base.ToCardViewModel(request, card);
            return new BlockCardResponse(cardViewModel.Status);
        }

        private async Task NotifyUser(BlockCardCommand command, DebitCard card, User user)
        {
            var cardInfo =
                await _bankInternalCardsService.GetCardInfo(command.MbanqAccessToken, command.Ip, card.ExternalId);

            await Mediator.Send(new CardBlockedEmailCommand(card.User.Email, card.User.FirstName,
                cardInfo.PrimaryAccountNumber));

            var notificationText =
                $"Your {StringProvider.GetPartnerName(_partnerProvider)} Card " +
                $"{SecretNumberHelper.GetLastFourDigits(cardInfo.PrimaryAccountNumber)} has been locked. " +
                $"To re-activate, call us {StringProvider.GetSupportPhone(_partnerProvider)}. " +
                $"The {StringProvider.GetPartnerName(_partnerProvider)} Team";

            try
            {
                await _notificationUsersService.SendNotification(user.Id, notificationText,
                    user.Settings.IsNotificationsEnabled, NotificationTypeEnum.Information, true, user.Devices, null,
                    null, PushNotificationCategory.YourB9CardHasBeenBlocked);
            }
            catch (Exception ex)
            {
                _logger.LogError($"Error when sending push notification: {ex.Message}", ex);
            }

            try
            {
                await _smsService.SendMessageAsync(user.Phone, notificationText);
            }
            catch (Exception ex)
            {
                _logger.LogError($"Error when sending sms: {ex.Message}", ex);
            }
        }
    }
}
