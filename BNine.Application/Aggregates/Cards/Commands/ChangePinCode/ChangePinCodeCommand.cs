﻿namespace BNine.Application.Aggregates.Cards.Commands.ChangePinCode;

using Abstractions;
using MediatR;
using Models;
using Newtonsoft.Json;

public class ChangePinCodeCommand : DebitCardUserRequest, IRequest<PinCodeWebView>
{
    [JsonIgnore]
    public string SuccessUrl
    {
        get;
        set;
    }

    [JsonIgnore]
    public string FailureUrl
    {
        get;
        set;
    }
}
