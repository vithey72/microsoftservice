﻿namespace BNine.Application.Aggregates.Cards.Commands.ChangePinCode
{
    using System.Threading;
    using System.Threading.Tasks;
    using Abstractions;
    using AutoMapper;
    using Domain.Entities;
    using Exceptions;
    using Interfaces;
    using Interfaces.Bank.Client;
    using MediatR;
    using Models;
    using Queries.GetMostRecentCard;

    public class ChangePinCodeCommandHandler :
        AbstractRequestHandler,
        IRequestHandler<ChangePinCodeCommand, PinCodeWebView>
    {
        private IBankInternalCardsService BankInternalCardsService
        {
            get;
        }

        public ChangePinCodeCommandHandler(
            IBankInternalCardsService bankInternalCardsService,
            IMediator mediator,
            IBNineDbContext dbContext,
            IMapper mapper,
            ICurrentUserService currentUser) : base(mediator, dbContext, mapper, currentUser)
        {
            BankInternalCardsService = bankInternalCardsService;
        }

        public async Task<PinCodeWebView> Handle(ChangePinCodeCommand request, CancellationToken cancellationToken)
        {
            var card = await Mediator.Send(new GetMostRecentDebitCardQuery(request.CardId));

            if (card == null)
            {
                throw new NotFoundException(nameof(DebitCard), CurrentUser.UserId);
            }

            var url = await BankInternalCardsService.GetChangeCardPinUrl(
                request.MbanqAccessToken,
                request.Ip,
                card.ExternalId);

            var urlWithParams = $"{url}?r={request.SuccessUrl}&f={request.FailureUrl}";

            return new PinCodeWebView
            {
                Url = urlWithParams
            };
        }
    }
}
