﻿namespace BNine.Application.Aggregates.Cards.Commands.UpdatePinCodeChanged;

using MediatR;

public class UpdatePinCodeSuccessfullyChangedCommand : IRequest<Unit>
{
    public int ExternalCardId
    {
        get;
        set;
    }
}
