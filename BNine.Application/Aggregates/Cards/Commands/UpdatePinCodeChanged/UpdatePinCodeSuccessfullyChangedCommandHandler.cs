﻿namespace BNine.Application.Aggregates.Cards.Commands.UpdatePinCodeChanged;

using Abstractions;
using AutoMapper;
using Exceptions;
using Interfaces;
using MediatR;
using Microsoft.EntityFrameworkCore;

public class UpdatePinCodeSuccessfullyChangedCommandHandler : AbstractRequestHandler,
    IRequestHandler<UpdatePinCodeSuccessfullyChangedCommand, Unit>
{
    public UpdatePinCodeSuccessfullyChangedCommandHandler(IMediator mediator, IBNineDbContext dbContext, IMapper mapper,
        ICurrentUserService currentUser) : base(mediator, dbContext, mapper, currentUser)
    {
    }

    public async Task<Unit> Handle(UpdatePinCodeSuccessfullyChangedCommand request, CancellationToken cancellationToken)
    {
        var card = await DbContext.DebitCards.FirstOrDefaultAsync(x => x.ExternalId == request.ExternalCardId);

        if (card == null)
        {
            throw new NotFoundException($"Debit card with external id: {request.ExternalCardId} not found");
        }

        card.IsPinSet = true;

        await DbContext.SaveChangesAsync(cancellationToken);

        return Unit.Value;
    }
}
