﻿namespace BNine.Application.Aggregates.Cards.Commands.ActivateCard
{
    using BNine.Application.Aggregates.Cards.Models;
    using MediatR;
    using Newtonsoft.Json;

    public class ActivateCardCommand : DebitCardBaseModel, IRequest<ActivationStatusResponseModel>
    {
        [JsonIgnore]
        public string MbanqAccessToken
        {
            get; set;
        }

        [JsonIgnore]
        public string Ip
        {
            get; set;
        }
    }
}
