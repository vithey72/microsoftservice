﻿namespace BNine.Application.Aggregates.Cards.Commands.ActivateCard
{
    using System;
    using System.Threading;
    using System.Threading.Tasks;
    using Abstractions;
    using AutoMapper;
    using Domain.Entities;
    using Enums.DebitCard;
    using Exceptions;
    using Interfaces;
    using Interfaces.Bank.Client;
    using MediatR;
    using Models;
    using Queries.GetMostRecentCard;

    public class ActivateCardCommandHandler
        : AbstractRequestHandler, IRequestHandler<ActivateCardCommand, ActivationStatusResponseModel>
    {
        private IBankInternalCardsService BankInternalCardsService
        {
            get;
        }

        public ActivateCardCommandHandler(
            IMediator mediator,
            IBNineDbContext dbContext,
            IMapper mapper,
            ICurrentUserService currentUser,
            IBankInternalCardsService bankInternalCardsService)
            : base(mediator, dbContext, mapper, currentUser)
        {
            BankInternalCardsService = bankInternalCardsService;
        }

        public async Task<ActivationStatusResponseModel> Handle(ActivateCardCommand request, CancellationToken cancellationToken)
        {
            var card = await Mediator.Send(new GetMostRecentDebitCardQuery(CardType.PhysicalDebit, request.CardId));

            if (card == null)
            {
                throw new NotFoundException(nameof(DebitCard), CurrentUser.UserId);
            }

            var status = await BankInternalCardsService.ActivatePhysicalCard(
                request.MbanqAccessToken,
                request.Ip,
                request.CardId ?? card.ExternalId);

            var activationSuccess = false;
            if (status.Equals("ACTIVE"))
            {
                activationSuccess = true;

                card.Status = CardStatus.ACTIVE;
                card.ShippingStatus = CardShippingStatus.ACTIVATED;
                card.ActivationDate = DateTime.UtcNow;

                await DbContext.SaveChangesAsync(cancellationToken);
            }

            return new ActivationStatusResponseModel
            {
                IsActivated = activationSuccess,
            };
        }
    }
}
