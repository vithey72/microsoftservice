﻿namespace BNine.Application.Aggregates.Cards.Commands.Verify4LastDigits
{
    using System.Text.RegularExpressions;
    using FluentValidation;

    public class Verify4LastDigitsCommandValidator : AbstractValidator<Verify4LastDigitsCommand>
    {
        public Verify4LastDigitsCommandValidator()
        {
            RuleFor(x => x.CardId).NotEmpty();
            RuleFor(x => x.Last4Numbers)
                .NotEmpty()
                .Must(IsValidLast4Numbers)
                .WithMessage("Invalid last 4 digits");
        }

        private bool IsValidLast4Numbers(string last4Numbers)
        {
            return Regex.IsMatch(last4Numbers, @"^[0-9]{4}$");
        }
    }
}
