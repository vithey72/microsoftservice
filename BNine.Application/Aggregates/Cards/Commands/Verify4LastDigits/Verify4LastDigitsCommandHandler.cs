﻿namespace BNine.Application.Aggregates.Cards.Commands.Verify4LastDigits
{
    using System.Threading;
    using System.Threading.Tasks;
    using AutoMapper;
    using BNine.Application.Abstractions;
    using BNine.Application.Exceptions.ApiResponse;
    using BNine.Application.Interfaces;
    using BNine.Application.Interfaces.Bank.Client;
    using BNine.Common;
    using BNine.Constants;
    using MediatR;

    public class Verify4LastDigitsCommandHandler : AbstractRequestHandler, IRequestHandler<Verify4LastDigitsCommand, Unit>
    {
        private IBankInternalCardsService BankInternalCardsService
        {
            get;
        }

        public Verify4LastDigitsCommandHandler(IMediator mediator, IBNineDbContext dbContext, IMapper mapper,
            ICurrentUserService currentUser, IBankInternalCardsService bankInternalCardsService) : base(mediator, dbContext, mapper, currentUser)
        {
            BankInternalCardsService = bankInternalCardsService;
        }

        public async Task<Unit> Handle(Verify4LastDigitsCommand request, CancellationToken cancellationToken)
        {
            var cardExternalDetails = await BankInternalCardsService.GetCardInfo(
                request.MbanqAccessToken,
                request.Ip,
                request.CardId);

            var last4Numbers = SecretNumberHelper.GetLastFourDigits(cardExternalDetails.PrimaryAccountNumber);

            if (last4Numbers != request.Last4Numbers)
            {
                throw new ApiResponseException(ApiResponseErrorCodes.Last4Digits, "Card details are incorrect");
            }

            return Unit.Value;
        }
    }
}
