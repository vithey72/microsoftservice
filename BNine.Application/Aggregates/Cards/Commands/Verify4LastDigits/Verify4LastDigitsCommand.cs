﻿namespace BNine.Application.Aggregates.Cards.Commands.Verify4LastDigits
{
    using BNine.Application.Abstractions;
    using MediatR;

    public class Verify4LastDigitsCommand : MBanqSelfServiceUserRequest, IRequest
    {
        public int CardId
        {
            get; set;
        }

        public string Last4Numbers
        {
            get; set;
        }
    }
}
