﻿namespace BNine.Application.Aggregates.Cards.Commands.UnblockCard
{
    using System.Threading;
    using System.Threading.Tasks;
    using AutoMapper;
    using BNine.Application.Helpers;
    using BNine.Constants;
    using Common;
    using Domain.Entities;
    using Domain.Entities.User;
    using Emails.Commands.Card.Unblocked;
    using Enums;
    using Enums.DebitCard;
    using Exceptions;
    using Interfaces;
    using Interfaces.Bank.Client;
    using MediatR;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.Extensions.Configuration;
    using Microsoft.Extensions.Logging;
    using Models;
    using Queries.GetCardStatus;
    using Queries.GetMostRecentCard;

    public class UnblockCardCommandHandler
        : GetCardStatusBaseHandler
        , IRequestHandler<UnblockCardCommand, BlockCardResponse>
    {
        private readonly ISmsService _smsService;
        private readonly INotificationUsersService _notificationUsersService;
        private readonly IBankInternalCardsService _bankInternalCardsService;
        private readonly IPartnerProviderService _partnerProvider;
        private readonly ILogger<UnblockCardCommandHandler> _logger;

        public UnblockCardCommandHandler(
            ISmsService smsService,
            INotificationUsersService notificationUsersService,
            IMediator mediator,
            IBNineDbContext dbContext,
            IMapper mapper,
            ICurrentUserService currentUser,
            IConfiguration configuration,
            IBankInternalCardsService bankInternalCardsService,
            IPartnerProviderService partnerProvider,
            ILogger<UnblockCardCommandHandler> logger
            )
            : base(mediator, dbContext, mapper, currentUser, bankInternalCardsService, configuration)
        {
            _smsService = smsService;
            _notificationUsersService = notificationUsersService;
            _bankInternalCardsService = bankInternalCardsService;
            _partnerProvider = partnerProvider;
            _logger = logger;
        }

        public async Task<BlockCardResponse> Handle(UnblockCardCommand request, CancellationToken cancellationToken)
        {
            var card = await Mediator.Send(new GetMostRecentDebitCardQuery(null, request.CardId));
            var user = await DbContext.Users
                .Include(u => u.Devices)
                .FirstOrDefaultAsync(u => u.Id == CurrentUser.UserId.Value, cancellationToken);

            if (card == null)
            {
                throw new NotFoundException(nameof(DebitCard), CurrentUser.UserId);
            }

            var updatedCardId = await _bankInternalCardsService.ChangeCardStatus(
                    request.MbanqAccessToken,
                    request.Ip,
                    request.CardId ?? card.ExternalId,
                    HandleCardEventCommand.ACTIVATE.ToString());

            card.Status = CardStatus.ACTIVE;
            await DbContext.SaveChangesAsync(cancellationToken);

            await NotifyUser(request, card, user);

            var cardViewModel = await base.ToCardViewModel(request, card);
            return new BlockCardResponse(cardViewModel.Status);
        }

        private async Task NotifyUser(UnblockCardCommand command, DebitCard card, User user)
        {
            var cardInfo = await _bankInternalCardsService.GetCardInfo(command.MbanqAccessToken, command.Ip, card.ExternalId);

            await Mediator.Send(new CardUnblockedEmailCommand(card.User.Email, card.User.FirstName, cardInfo.PrimaryAccountNumber));

            var notificationText = $"Your {StringProvider.GetPartnerName(_partnerProvider)} " +
                                   $"Card {SecretNumberHelper.GetLastFourDigits(cardInfo.PrimaryAccountNumber)} " +
                                   $"successfully unlocked. The {StringProvider.GetPartnerName(_partnerProvider)} Team";

            try
            {
                await _notificationUsersService.SendNotification(user.Id, notificationText, user.Settings.IsNotificationsEnabled,
                    NotificationTypeEnum.Information, true, user.Devices, null, null,
                    StringProvider.ReplaceCustomizedStrings(PushNotificationCategory.YourB9CardSuccessfullyUnlocked, _partnerProvider));
            }
            catch (Exception ex)
            {
                _logger.LogError($"Error when sending push notification: {ex.Message}", ex);
            }

            try
            {
                await _smsService.SendMessageAsync(user.Phone, notificationText);
            }
            catch (Exception ex)
            {
                _logger.LogError($"Error when sending sms: {ex.Message}", ex);
            }
        }
    }
}
