﻿namespace BNine.Application.Aggregates.Cards.Commands.UnblockCard;

using Abstractions;
using MediatR;
using Models;

public class UnblockCardCommand : DebitCardUserRequest, IRequest<BlockCardResponse>
{
}
