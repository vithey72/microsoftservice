﻿namespace BNine.Application.Aggregates.Cards.Commands.SaveCardEmbossedDate
{
    using MediatR;

    public class SaveCardEmbossedDateCommand : IRequest
    {
        public int CardExternalId
        {
            get;
            set;
        }
    }
}
