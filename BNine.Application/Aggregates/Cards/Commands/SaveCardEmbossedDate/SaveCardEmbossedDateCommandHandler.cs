﻿namespace BNine.Application.Aggregates.Cards.Commands.SaveCardEmbossedDate
{
    using Abstractions;
    using AutoMapper;
    using Interfaces;
    using MediatR;
    using Microsoft.EntityFrameworkCore;

    public class SaveCardEmbossedDateCommandHandler :
        AbstractRequestHandler,
        IRequestHandler<SaveCardEmbossedDateCommand>
    {
        public SaveCardEmbossedDateCommandHandler(
            IMediator mediator,
            IBNineDbContext dbContext,
            IMapper mapper,
            ICurrentUserService currentUser) : base(mediator, dbContext, mapper, currentUser)
        {
        }

        public async Task<Unit> Handle(SaveCardEmbossedDateCommand request, CancellationToken cancellationToken)
        {
            var entity = await DbContext
                .DebitCards
                .FirstOrDefaultAsync(
                    dc => dc.ExternalId == request.CardExternalId,
                    cancellationToken);

            if (entity != null)
            {
                entity.EmbossedAt = DateTime.Now;
                await DbContext.SaveChangesAsync(cancellationToken);
            }

            return Unit.Value;
        }
    }
}
