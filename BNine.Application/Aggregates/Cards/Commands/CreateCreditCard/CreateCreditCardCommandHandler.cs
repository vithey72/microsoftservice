﻿namespace BNine.Application.Aggregates.Cards.Commands.CreateCreditCard;

using Abstractions;
using AutoMapper;
using Configuration.Queries.GetConfiguration;
using Constants;
using Domain.Entities;
using Enums.DebitCard;
using Interfaces;
using Interfaces.Bank.Administration;
using MediatR;
using Microsoft.EntityFrameworkCore;

public class CreateCreditCardCommandHandler
    : AbstractRequestHandler
    , IRequestHandler<CreateCreditCardCommand, Unit>
{
    private readonly IBankCreditCardsService _creditCardsService;

    public CreateCreditCardCommandHandler(
        IMediator mediator,
        IBNineDbContext dbContext,
        IMapper mapper,
        ICurrentUserService currentUser,
        IBankCreditCardsService creditCardsService
        )
        : base(mediator, dbContext, mapper, currentUser)
    {
        _creditCardsService = creditCardsService;
    }

    public async Task<Unit> Handle(CreateCreditCardCommand request, CancellationToken cancellationToken)
    {
        var configuration = await Mediator.Send(new GetConfigurationQuery(), cancellationToken);
        if (!configuration.HasFeatureEnabled(FeaturesNames.Features.CreditCardInternalTest))
        {
            throw new AccessViolationException(
                $"User does not have access to {FeaturesNames.Features.CreditCardInternalTest} feature.");
        }

        var user = await DbContext
            .Users
            .Include(u => u.CurrentAccount)
            .SingleAsync(u => u.Id == CurrentUser.UserId, cancellationToken);
        var creditCardProduct = await DbContext
                .CardProducts
                .SingleAsync(p => p.AssociatedType == CardType.Credit, cancellationToken);

        var (id, externalId) = await _creditCardsService.CreateCreditCard(
            user.ExternalClientId!.Value,
            user.CurrentAccount.ExternalId,
            creditCardProduct.ExternalId);

        var newCardEntity = new DebitCard
        {
            Id = id,
            ExternalId = externalId,
            Status = CardStatus.ACTIVE,
            UserId = user.Id,
            CreatedAt = DateTime.UtcNow,
            Type = CardType.Credit,
            ShippingStatus = CardShippingStatus.IN_TRANSIT,
        };

        await DbContext.DebitCards.AddAsync(newCardEntity, CancellationToken.None);
        await DbContext.SaveChangesAsync(CancellationToken.None);
        return Unit.Value;
    }
}
