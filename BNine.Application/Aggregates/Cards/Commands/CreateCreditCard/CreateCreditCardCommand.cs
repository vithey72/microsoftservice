﻿namespace BNine.Application.Aggregates.Cards.Commands.CreateCreditCard;

using Behaviours;
using MediatR;

[Sequential]
public class CreateCreditCardCommand : IRequest<Unit>
{
}
