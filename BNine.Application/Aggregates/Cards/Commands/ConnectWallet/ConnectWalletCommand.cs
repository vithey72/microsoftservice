﻿namespace BNine.Application.Aggregates.Cards.Commands.ConnectWallet
{
    using BNine.Application.Aggregates.Cards.Models;
    using BNine.Enums;
    using MediatR;

    public class ConnectWalletCommand : DebitCardBaseModel, IRequest<Unit>
    {
        public ConnectWalletCommand(DigitalWalletType walletType)
        {
            WalletType = walletType;
        }

        public DigitalWalletType WalletType
        {
            get;
        }
    }
}
