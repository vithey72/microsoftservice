﻿namespace BNine.Application.Aggregates.Cards.Commands.ConnectWallet
{
    using System;
    using System.Linq;
    using System.Threading;
    using System.Threading.Tasks;
    using AutoMapper;
    using BNine.Application.Abstractions;
    using BNine.Application.Exceptions;
    using BNine.Application.Interfaces;
    using BNine.Domain.Entities;
    using Enums.DebitCard;
    using MediatR;
    using Microsoft.EntityFrameworkCore;

    public class ConnectWalletCommandHandler
        : AbstractRequestHandler, IRequestHandler<ConnectWalletCommand, Unit>
    {
        public ConnectWalletCommandHandler(IMediator mediator, IBNineDbContext dbContext, IMapper mapper, ICurrentUserService currentUser) : base(mediator, dbContext, mapper, currentUser)
        {
        }

        public async Task<Unit> Handle(ConnectWalletCommand request, CancellationToken cancellationToken)
        {
            var cardQuery = DbContext.DebitCards
                .Include(x => x.SamsungWallet)
                .Include(x => x.AppleWallet)
                .Include(x => x.GoogleWallet)
                .Where(x => x.UserId == CurrentUser.UserId)
                .Where(x => x.Status == CardStatus.ACTIVE);

            if (request.CardId.HasValue)
            {
                cardQuery = cardQuery.Where(x => x.ExternalId == request.CardId);
            }

            var card = cardQuery.FirstOrDefault();

            if (card == null)
            {
                throw new NotFoundException(nameof(DebitCard), CurrentUser.UserId);
            }

            switch (request.WalletType)
            {
                case Enums.DigitalWalletType.Apple:
                    if (card.AppleWallet == null)
                    {
                        card.AppleWallet = new Domain.Entities.DigitalWallet.AppleWallet();
                    }
                    else
                    {
                        card.AppleWallet.DeletedAt = null;
                        card.AppleWallet.UpdatedAt = DateTime.UtcNow;
                    }
                    break;

                case Enums.DigitalWalletType.Google:
                    if (card.GoogleWallet == null)
                    {
                        card.GoogleWallet = new Domain.Entities.DigitalWallet.GoogleWallet();
                    }
                    else
                    {
                        card.GoogleWallet.DeletedAt = null;
                        card.GoogleWallet.UpdatedAt = DateTime.UtcNow;
                    }
                    break;

                case Enums.DigitalWalletType.Samsung:
                    if (card.SamsungWallet == null)
                    {
                        card.SamsungWallet = new Domain.Entities.DigitalWallet.SamsungWallet();
                    }
                    else
                    {
                        card.SamsungWallet.DeletedAt = null;
                        card.SamsungWallet.UpdatedAt = DateTime.UtcNow;
                    }
                    break;
            }

            await DbContext.SaveChangesAsync(cancellationToken);

            return Unit.Value;
        }
    }
}
