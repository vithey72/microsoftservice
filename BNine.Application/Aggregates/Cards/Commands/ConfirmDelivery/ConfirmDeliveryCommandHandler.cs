﻿namespace BNine.Application.Aggregates.Cards.Commands.ConfirmDelivery
{
    using System.Threading;
    using System.Threading.Tasks;
    using AutoMapper;
    using BNine.Application.Abstractions;
    using BNine.Application.Interfaces;
    using Enums.DebitCard;
    using Exceptions;
    using MediatR;
    using Queries.GetMostRecentCard;

    public class ConfirmDeliveryCommandHandler
        : AbstractRequestHandler, IRequestHandler<ConfirmDeliveryCommand, Unit>
    {
        public ConfirmDeliveryCommandHandler(
            IMediator mediator,
            IBNineDbContext dbContext,
            IMapper mapper,
            ICurrentUserService currentUser)
            : base(mediator, dbContext, mapper, currentUser)
        {
        }

        public async Task<Unit> Handle(ConfirmDeliveryCommand request, CancellationToken cancellationToken)
        {
            var card = await Mediator.Send(new GetMostRecentDebitCardQuery(CardType.PhysicalDebit));

            if (card == null)
            {
                throw new NotFoundException("User hasn't debit card in Ordered status", CurrentUser.UserId);
            }

            card.ShippingStatus = CardShippingStatus.RECEIVED;

            await DbContext.SaveChangesAsync(cancellationToken);

            return Unit.Value;
        }
    }
}
