﻿namespace BNine.Application.Aggregates.Cards.Commands.ReplaceVirtualDebitCard;

using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using BNine.Application.Interfaces;
using BNine.Enums;
using BNine.ResourceLibrary;
using CommonModels.Dialog;
using Enums.DebitCard;
using Exceptions;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Localization;
using Microsoft.Extensions.Logging;
using Queries.GetMostRecentCard;
using Queries.ReplaceDebitCard;
using IBankInternalCardsService = Interfaces.Bank.Client.IBankInternalCardsService;

public class ReplaceVirtualDebitCardCommandHandler
    : ReplaceDebitCardCommandHandlerBase,
        IRequestHandler<ReplaceVirtualDebitCardCommand, EitherDialog<GenericSuccessDialog, GenericFailDialog>>
{
    private const int VirtualCardReplacementLimit = 10;

    private readonly IBankInternalCardsService _bankInternalCardsService;

    public ReplaceVirtualDebitCardCommandHandler(
        IStringLocalizer<SharedResource> sharedLocalizer,
        IMediator mediator,
        IBNineDbContext dbContext,
        IMapper mapper,
        ICurrentUserService currentUser,
        INotificationUsersService notificationUsersService,
        IBankInternalCardsService bankInternalCardsService,
        ILogger<ReplaceVirtualDebitCardCommandHandler> logger,
        IPartnerProviderService partnerProvider
    )
        : base(
            mediator,
            dbContext,
            mapper,
            currentUser,
            sharedLocalizer,
            notificationUsersService,
            logger,
            partnerProvider
            )
    {
        _bankInternalCardsService = bankInternalCardsService;
    }

    public async Task<EitherDialog<GenericSuccessDialog, GenericFailDialog>> Handle(
        ReplaceVirtualDebitCardCommand request, CancellationToken cancellationToken)
    {
        var card = await Mediator.Send(new GetMostRecentDebitCardQuery(CardType.VirtualDebit, request.CardId),
            cancellationToken);
        var user = await DbContext.Users
            .Include(u => u.Devices)
            .Include(u => u.CurrentAccount)
            .FirstOrDefaultAsync(u => u.Id == CurrentUser.UserId.Value, cancellationToken);

        if (card == null)
        {
            throw new ValidationException("request", "There is no virtual card associated with this client.");
        }

        await CheckForReplacementLimit(CurrentUser.UserId.Value, CardType.VirtualDebit, VirtualCardReplacementLimit);
        if (FailResultDialog?.IsSuccess == false)
        {
            return FailResultDialog;
        }

        var cardStatus = await _bankInternalCardsService.GetCardInfo(
            request.MbanqAccessToken,
            request.Ip,
            card.ExternalId);

        if (cardStatus.Status == CardStatus.ACTIVE)
        {
            await _bankInternalCardsService.ChangeCardStatus(
                request.MbanqAccessToken,
                request.Ip,
                card.ExternalId,
                HandleCardEventCommand.SUSPEND.ToString());
        }

        int newCardId;
        try
        {
            newCardId = await _bankInternalCardsService.ReplaceCard(
                request.MbanqAccessToken,
                request.Ip,
                card.ExternalId,
                CardReplacementReason.STOLEN.ToString());
        }
        catch (ValidationException ex)
        {
            base.HandleMBanqExceptions(ex);

            throw;
        }

        if (newCardId != card.ExternalId)
        {
            await base.ReplaceDebitCardInB9Database(cancellationToken, card, newCardId);
        }

        await base.NotifyUser(request, user);

        var successResponse = new EitherDialog<GenericSuccessDialog, GenericFailDialog>(
            new GenericSuccessDialog
            {
                Header = new GenericDialogHeaderViewModel { Title = "New Virtual Card" },
                Body = new GenericDialogBodyViewModel
                {
                    Subtitle = "Your new virtual card is ready to use.",
                    ButtonText = "DONE",
                    Title = "Success"
                }
            });

        return FailResultDialog ?? successResponse;
    }
}
