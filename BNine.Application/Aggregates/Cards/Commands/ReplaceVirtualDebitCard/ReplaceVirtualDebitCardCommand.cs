﻿namespace BNine.Application.Aggregates.Cards.Commands.ReplaceVirtualDebitCard;

using Abstractions;
using Behaviours;
using CommonModels.Dialog;
using MediatR;

[Sequential]
public class ReplaceVirtualDebitCardCommand
    : DebitCardUserRequest
    , IRequest<EitherDialog<GenericSuccessDialog, GenericFailDialog>>
{
}
