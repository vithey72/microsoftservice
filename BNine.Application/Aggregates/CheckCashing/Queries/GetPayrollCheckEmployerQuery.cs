﻿namespace BNine.Application.Aggregates.CheckCashing.Queries
{
    using System;
    using MediatR;

    public class GetPayrollCheckEmployerQuery : IRequest<string>
    {
        public Guid TransactionId
        {
            get; set;
        }
    }
}
