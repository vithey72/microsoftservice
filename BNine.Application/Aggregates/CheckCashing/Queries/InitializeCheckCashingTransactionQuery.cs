﻿namespace BNine.Application.Aggregates.CheckCashing.Queries
{
    using Models;
    using MediatR;
    using Models.Transaction;

    public class InitializeCheckCashingTransactionQuery : IRequest<(CheckCashingTransactionViewModel Transaction, CheckCashingInitializationFailedResponse FailResponse)>
    {
        public CheckScans CheckScans
        {
            get; set;
        }
    }
}
