﻿namespace BNine.Application.Aggregates.CheckCashing.Queries
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading;
    using System.Threading.Tasks;
    using Abstractions;
    using Application.Models.MBanq.Transfers;
    using AutoMapper;
    using BNine.Application.Exceptions;
    using BNine.Application.Interfaces.Bank.Administration;
    using BNine.Application.Interfaces.Bank.Client;
    using BNine.Application.Models.MBanq;
    using BNine.Domain.Entities.BankAccount;
    using BNine.Domain.Entities.User;
    using BNine.Enums.Transfers;
    using BNine.Settings;
    using Configuration.Queries.GetConfiguration;
    using Constants;
    using Domain.Entities.CheckCashing;
    using Enums.CheckCashing;
    using Interfaces;
    using Interfaces.Alltrust;
    using MediatR;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.Extensions.Options;
    using Models;
    using Models.Transaction;
    using IBankSavingAccountsService = Interfaces.Bank.Administration.IBankSavingAccountsService;

    public class SubmitRedemptionImagesQueryHandler
        : AbstractRequestHandler
        , IRequestHandler<SubmitRedemptionImagesQuery, SubmitRedemptionImagesResponse>
    {
        public AlltrustSettings Settings
        {
            get;
        }

        public IAlltrustCheckApprovementService AlltrustService
        {
            get;
        }

        public IBankTransactionsService BankTransactionsService
        {
            get;
        }

        private IBankTransfersService BankTransfersService
        {
            get;
        }

        public IBankSavingAccountsService BankSavingAccountsService
        {
            get;
        }

        public SubmitRedemptionImagesQueryHandler(
            IMediator mediator,
            IBNineDbContext dbContext,
            IMapper mapper,
            ICurrentUserService currentUser,
            IOptions<AlltrustSettings> options,
            IAlltrustCheckApprovementService alltrustService,
            IBankTransactionsService bankTransactionsService,
            IBankTransfersService bankTransfersService,
            IBankSavingAccountsService bankSavingAccountsService)
            : base(mediator, dbContext, mapper, currentUser)
        {
            Settings = options.Value;
            AlltrustService = alltrustService;
            BankTransactionsService = bankTransactionsService;
            BankTransfersService = bankTransfersService;
            BankSavingAccountsService = bankSavingAccountsService;
        }

        public async Task<SubmitRedemptionImagesResponse> Handle(SubmitRedemptionImagesQuery request, CancellationToken cancellationToken)
        {
            var transaction = await DbContext
                .CheckCashingTransactions
                .FirstOrDefaultAsync(
                    x => x.Id == request.TransactionId,
                    cancellationToken);

            if (transaction == null)
            {
                throw new KeyNotFoundException($"Transaction with Id = {request.TransactionId} does not exist.");
            }

            if (transaction.Status != CheckCashingTransactionStatus.DetailsConfirmedByUser)
            {
                throw new InvalidOperationException(
                    $"Invalid stage: {transaction.Status}, expected {CheckCashingTransactionStatus.DetailsConfirmedByUser}");
            }

            var redemptionRequest = GetRedemptionRequest(request);
            await AlltrustService.RedeemCheck(transaction.AlltrustTransactionId, redemptionRequest, cancellationToken);

            var alltrustTransaction = await AlltrustService.GetTransaction(transaction.AlltrustTransactionId, cancellationToken);
            var (reputation, instantCashingAmount) = await CalculateUserReputation(alltrustTransaction, transaction.UserId, cancellationToken);
            var viewModel = CheckCashingTransactionViewModel.Build(transaction, alltrustTransaction);

            if (Settings.AutoAcceptChecksOnRedemption)
            {
                await AlltrustService.SetTransactionResult(transaction.AlltrustTransactionId, CheckReviewResult.Accepted, cancellationToken);

                await SubmitCheckForClearing(request, transaction, alltrustTransaction, instantCashingAmount, cancellationToken);
                transaction.Status = CheckCashingTransactionStatus.Approved;
                if (instantCashingAmount != 0)
                {
                    transaction.PaymentStatus = instantCashingAmount >= transaction.AmountTotal
                        ? CheckCashingPaymentStatus.InstantPaidInFull
                        : CheckCashingPaymentStatus.InstantFundsPaid;
                }
            }
            else
            {
                transaction.Status = CheckCashingTransactionStatus.ApprovementRequested;
            }
            transaction.StatusDate = DateTime.Now;
            await DbContext.SaveChangesAsync(cancellationToken);


            return new SubmitRedemptionImagesResponse
            {
                Transaction = viewModel,
                UserReputation = reputation,
                InstantCashingAmount = instantCashingAmount,
            };
        }

        private static CheckRedemptionRequest GetRedemptionRequest(SubmitRedemptionImagesQuery request)
        {
            return new CheckRedemptionRequest
            {
                CheckImages = new CheckRedemptionRequest.RedemptionImages
                {
                    Back = new CheckRedemptionRequest.TypedImage
                    {
                        Image = request.BackImage,
                        ImageType = "jpeg",
                        ImageDate = DateTime.Now,
                    },
                    Front = new CheckRedemptionRequest.TypedImage
                    {
                        Image = request.FrontImage,
                        ImageType = "jpeg",
                        ImageDate = DateTime.Now,
                    },
                },
                CustomerImage = new CheckRedemptionRequest.TypedImage { Image = "", ImageType = "jpeg" },
            };
        }

        /// <summary>
        /// If user has at least one successful cashing and the maker is known, then fetch instant amount from CheckCashing feature. Otherwise it's $0.
        /// If user has bounced checks, instant amount is always $0. 
        /// </summary>
        private async Task<(UserReputationType reputation, decimal instantCashingAmount)> CalculateUserReputation(
            AlltrustTransaction transaction, Guid userId, CancellationToken cancellationToken)
        {
            var alltrustMaker = transaction.Check.Maker;
            var maker = await DbContext.CheckMakers
                .FirstOrDefaultAsync(x =>
                    x.MakerRoutingNumber == alltrustMaker.AbaNumber &&
                    x.MakerAccountNumber == alltrustMaker.AccountNumber,
                    cancellationToken);

            if (maker == null)
            {
                return (UserReputationType.None, 0);
            }

            var badStatuses = new[] { CheckCashingTransactionStatus.ClearingFailed };

            var clearedChecksCount = await DbContext
                .CheckCashingTransactions
                .CountAsync(
                    cct => cct.UserId == userId &&
                           cct.Status == CheckCashingTransactionStatus.ClearingSuccess &&
                           cct.CheckMakerId == maker.Id,
                    cancellationToken);

            var failedTransactionsCount = await DbContext
                .CheckCashingTransactions
                .CountAsync(cct => cct.UserId == userId &&
                                   badStatuses.Contains(cct.Status) &&
                                   cct.CheckMakerId == maker.Id,
                    cancellationToken);

            if (failedTransactionsCount != 0)
            {
                return (UserReputationType.Bad, 0);
            }

            if (clearedChecksCount >= 1)
            {
                var configuration = await Mediator.Send(new GetConfigurationQuery());
                decimal allowedInstantCashingAmount = 0;
                if (configuration.PropertyExists(FeaturesNames.Features.CheckCashing, "InstantAllocation"))
                {
                    allowedInstantCashingAmount = (decimal)(configuration.Features[FeaturesNames.Features.CheckCashing] as dynamic).InstantAllocation;
                }

                return (UserReputationType.EligibleForInstantCashing, allowedInstantCashingAmount);
            }

            return (UserReputationType.None, 0);
        }

        public async Task SubmitCheckForClearing(SubmitRedemptionImagesQuery request,
            Domain.Entities.CheckCashing.CheckCashingTransaction transaction, AlltrustTransaction alltrustTransaction,
            decimal instantCashingAmount, CancellationToken cancellationToken)
        {
            var user = await DbContext.Users
                .Include(u => u.CurrentAccount)
                .Where(x => x.Id == CurrentUser.UserId)
                .Select(x => new
                {
                    x.ExternalClientId,
                    x.FirstName,
                    x.LastName,
                    AccountId = x.CurrentAccount.ExternalId
                }).FirstOrDefaultAsync(cancellationToken);

            if (user == null)
            {
                throw new NotFoundException(nameof(User), CurrentUser.UserId);
            }

            if (user.AccountId == 0)
            {
                throw new NotFoundException(nameof(CurrentAccount), CurrentUser.UserId);
            }

            var accountInfo = await BankSavingAccountsService.GetSavingAccountInfo(user.AccountId);
            var micrParseResult = await AlltrustService.ParseMicr(alltrustTransaction.Check.RawMicr, cancellationToken);
            var depositImages = await AlltrustService.GetDepositCheckImages(alltrustTransaction.Check.Id, cancellationToken);

            var check = alltrustTransaction.Check;
            var maker = check.Maker;
            var transfer = new CheckTransfer
            {
                Type = TransferDirection.Debit,
                PaymentType = TransferType.RDC,
                Creditor =
                    new Counterparty { Identifier = $"ACCOUNT:{accountInfo.AccountNumber}", Name = check.PayeeName, },
                Debtor = new Counterparty { Identifier = $"RDC", },
                Amount = transaction.AmountRequested / 100m,
                InstantFunds = Math.Min(instantCashingAmount, transaction.AmountRequested / 100m),
                ExternalId = transaction.Id.ToString(),
                TransferDetails =
                    new CheckTransfer.CheckTransferDetails
                    {
                        CheckFrontImage = "data:image/image/png;base64," + check.DisplayCheckImages.FrontImage,
                        CheckBackImage = "data:image/image/png;base64," + check.DisplayCheckImages.BackImage,
                    },
                Reference = new List<string> { "Replenishment by check" },
                RawPaymentDetails = new CheckTransfer.CheckTransferRawPaymentDetails
                {
                    Amount = transaction.AmountRequested / 100m,
                    RawMicr = check.RawMicr,
                    PayeeName = check.PayeeName,
                    PayerName = maker.Name,
                    PayerAddress = ComposeAddress(maker),
                    CheckDate = check.CheckDate,
                    CheckNumber = check.CheckNumber,
                    FrontProcessedImage = depositImages.FrontImage,
                    RearProcessedImage = depositImages.BackImage,
                    Status = null,
                    Error = null,
                    Kind = check.Kind,
                    Micr = new CheckTransfer.CheckTransferRawPaymentDetails.MicrType
                    {
                        RawMicr = check.RawMicr,
                        ParseError = "",
                        StatusMessage = "",
                        ContainsUnrecognizedSymbols = micrParseResult.ContainsUnrecognizedSymbols,
                        CheckType = micrParseResult.CheckType.ToString(),
                        AuxOnus = micrParseResult.AuxOnus,
                        Onus = micrParseResult.Onus,
                        TransitNumber = maker.AbaNumber,
                        AccountNumber = maker.AccountNumber,
                        CheckNumber = check.CheckNumber,
                        Amount = check.Amount / 100m,
                        Special = true
                    },
                    Maker = new CheckTransfer.Maker
                    {
                        Id = maker.Id,
                        AbaNumber = maker.AbaNumber,
                        AccountNumber = maker.AccountNumber,
                        Status = maker.Status,
                        Kind = maker.Kind,
                        Name = maker.Name,
                        Address1 = maker.Address1,
                        Address2 = maker.Address2,
                        City = maker.City,
                        State = maker.State,
                        Zip = maker.Zip,
                        Phone = maker.Phone,
                        AltPhone = maker.AltPhone,
                        Notes = maker.Notes,
                    }
                },
            };
            await BankTransfersService.CreateCheckTransfer(
                request.MbanqAccessToken,
                request.Ip,
                transfer);
        }

        private string ComposeAddress(Maker maker)
        {
            return string.Join(", ", new[]
            {
                maker.City,
                maker.State,
                maker.Address1,
                maker.Address2
            });
        }
    }
}
