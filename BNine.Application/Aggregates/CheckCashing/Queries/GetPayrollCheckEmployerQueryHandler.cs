﻿namespace BNine.Application.Aggregates.CheckCashing.Queries
{
    using System.Threading;
    using System.Threading.Tasks;
    using Abstractions;
    using AutoMapper;
    using Enums.CheckCashing;
    using Interfaces;
    using Interfaces.Alltrust;
    using MediatR;
    using Microsoft.EntityFrameworkCore;

    public class GetPayrollCheckEmployerQueryHandler
        : AbstractRequestHandler,
        IRequestHandler<GetPayrollCheckEmployerQuery, string>
    {
        public IAlltrustCheckApprovementService AlltrustService
        {
            get;
        }

        public GetPayrollCheckEmployerQueryHandler(
            IMediator mediator,
            IBNineDbContext dbContext,
            IMapper mapper,
            IAlltrustCheckApprovementService alltrustService,
            ICurrentUserService currentUser)
            : base(mediator, dbContext, mapper, currentUser)
        {
            AlltrustService = alltrustService;
        }

        public async Task<string> Handle(GetPayrollCheckEmployerQuery request, CancellationToken cancellationToken)
        {
            var transaction = await DbContext
                .CheckCashingTransactions
                .FirstOrDefaultAsync(x => x.Id == request.TransactionId, cancellationToken);


            var alltrustTransaction = await AlltrustService.GetTransaction(transaction.AlltrustTransactionId, cancellationToken);

            if (alltrustTransaction.Check.Kind != CheckType.Payroll)
            {
                return null;
            }

            var user = await DbContext
                .Users
                .Include(u => u.AlltrustCustomerSettings)
                .ThenInclude(acs => acs.CurrentEmployer)
                .FirstOrDefaultAsync(u => u.Id == transaction.UserId, cancellationToken);

            var alltrustMaker = alltrustTransaction.Check.Maker;
            var currentEmployer = user?.AlltrustCustomerSettings?.CurrentEmployer;
            if (currentEmployer != null)
            {
                // it's the same employer as before
                if (currentEmployer.MakerAccountNumber == alltrustMaker.AccountNumber &&
                    currentEmployer.MakerRoutingNumber == alltrustMaker.AbaNumber)
                {
                    transaction.CheckMakerId = currentEmployer.Id;
                    await DbContext.SaveChangesAsync(cancellationToken);
                    return null;
                }
            }

            return alltrustMaker.Name;
        }
    }
}
