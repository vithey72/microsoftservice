﻿namespace BNine.Application.Aggregates.CheckCashing.Queries
{
    using MediatR;
    using Models.Transaction;

    public class GetTransactionInProgressQuery : IRequest<CheckCashingTransactionViewModel>
    {
    }
}
