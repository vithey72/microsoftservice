﻿namespace BNine.Application.Aggregates.CheckCashing.Queries
{
    using System;
    using System.Threading;
    using System.Threading.Tasks;
    using AutoMapper;
    using BNine.Application.Abstractions;
    using BNine.Application.Aggregates.CheckCashing.Models;
    using BNine.Application.Interfaces;
    using BNine.Application.Interfaces.Alltrust;
    using BNine.Settings;
    using Domain.Entities.CheckCashing;
    using Enums.CheckCashing;
    using MediatR;
    using Microsoft.Extensions.Logging;
    using Microsoft.Extensions.Options;
    using Models.Transaction;

    public class InitializeCheckCashingTransactionQueryHandler
        : AbstractRequestHandler
        , IRequestHandler<InitializeCheckCashingTransactionQuery,
            (CheckCashingTransactionViewModel Transaction, CheckCashingInitializationFailedResponse FailResponse)>
    {
        private readonly ICheckProblemsLoggingService _checkProblemsLoggingService;

        public IAlltrustCheckApprovementService AlltrustService
        {
            get;
        }

        public AlltrustSettings Settings
        {
            get;
        }

        public ILogger<InitializeCheckCashingTransactionQueryHandler> Logger
        {
            get;
        }

        public InitializeCheckCashingTransactionQueryHandler(
            IMediator mediator,
            IBNineDbContext dbContext,
            IMapper mapper,
            ICurrentUserService currentUser,
            IOptions<AlltrustSettings> options,
            ILogger<InitializeCheckCashingTransactionQueryHandler> logger,
            ICheckProblemsLoggingService checkProblemsLoggingService,
            IAlltrustCheckApprovementService alltrustService)
            : base(mediator, dbContext, mapper, currentUser)
        {
            _checkProblemsLoggingService = checkProblemsLoggingService;
            AlltrustService = alltrustService;
            Settings = options.Value;
            Logger = logger;
        }

        public async Task<(CheckCashingTransactionViewModel Transaction, CheckCashingInitializationFailedResponse FailResponse)> Handle(
            InitializeCheckCashingTransactionQuery request, CancellationToken cancellationToken)
        {
            var result = await BuildTransactionFromScans(request, cancellationToken);
            if (result.Transaction != null)
            {
                var vm = await SaveTransactionToDb(result.Transaction);
                return (vm, default);
            }
            return (null, result.FailResponse);
        }

        private async Task<(AlltrustTransaction Transaction, CheckCashingInitializationFailedResponse FailResponse)> BuildTransactionFromScans(InitializeCheckCashingTransactionQuery request, CancellationToken ct)
        {
            try
            {
                var customerId = await Mediator.Send(new GetAlltrustCustomerIdQuery(), ct);

                var ocrResult = await AlltrustService.OcrCheck(request.CheckScans, ct);
                if (ocrResult.Usability != "OK")
                {
                    return (null, await _checkProblemsLoggingService.DenyAsOcrFail(request, ocrResult));
                }
                if (ocrResult.Amount == 0)
                {
                    return (null, await _checkProblemsLoggingService.DenyAsAmountRecognitionFail(request, ocrResult));
                }
                if (string.IsNullOrEmpty(ocrResult.Micr))
                {
                    return (null, await _checkProblemsLoggingService.DenyAsMicrRecognitionFail(request, ocrResult));
                }

                var micrParseResult = await AlltrustService.ParseMicr(ocrResult.Micr, ct);
                if (micrParseResult.ParseError || micrParseResult.ContainsUnrecognizedSymbols)
                {
                    return (null, await _checkProblemsLoggingService.DenyAsMicrParseFail(request, ocrResult, micrParseResult));
                }

                var maker = await SelectBestMakerEntry(micrParseResult, ocrResult, ct);

                var decisionRequest = CreateCheckDecisionRequest(customerId, ocrResult, micrParseResult, maker);
                var decision = await AlltrustService.GetCheckDecision(decisionRequest, ct);

                var postResult = await PostTransaction(
                    request.CheckScans,
                    maker,
                    ocrResult,
                    micrParseResult,
                    customerId,
                    decision,
                    ct);
                return postResult;
            }
            catch (Exception ex)
            {
                var problemMessage = $"Error initializing check cashing transaction in Alltrust. ";
                var problemMessageWithBlobName = await _checkProblemsLoggingService.SaveOperationDataToStorage("exception", problemMessage, request);
                Logger.LogInformation(ex, problemMessageWithBlobName);
                return (null, new CheckCashingInitializationFailedResponse(CheckDataErrorType.Exception));
            }
        }

        /// <summary>
        /// Select from parsed maker and exisiting makers an entry with best filled fields.
        /// </summary>
        private async Task<Maker> SelectBestMakerEntry(MicrParseResult micrParseResult, CheckOcrResult ocrResult, CancellationToken ct)
        {
            var parsedMaker = CreateNewMaker(micrParseResult, ocrResult);
            var makers = await AlltrustService.GetMakers(micrParseResult.TransitNumber, micrParseResult.AccountNumber, ct);
            var bestMaker = parsedMaker;
            foreach (var maker in makers)
            {
                if (maker.GetDataQualityScore() >= bestMaker.GetDataQualityScore())
                {
                    bestMaker = maker;
                }
            }

            return bestMaker;
        }

        private CheckDecisionRequest CreateCheckDecisionRequest(string customerId, CheckOcrResult ocrResult, MicrParseResult micrParseResult, Maker maker)
        {
            var decisionRequest = new CheckDecisionRequest
            {
                Amount = ocrResult.Amount,
                BusinessId = Settings.BusinessId,
                LocationId = Settings.LocationId,
                CustomerId = customerId,
                CheckDate = ocrResult.CheckDate,
                CheckNumber = micrParseResult.CheckNumber,
                Maker = maker,
                HandWritten = ocrResult.Handwritten,
                OcrAmountMatched = true,
            };
            return decisionRequest;
        }

        /// <summary>
        /// No need to add maker manually, it will be added to Alltrust makers list automatically on POST transaction.
        /// </summary>
        private static Maker CreateNewMaker(MicrParseResult micrParseResult, CheckOcrResult ocrResult)
        {
            return new Maker
            {
                AbaNumber = micrParseResult.TransitNumber,
                AccountNumber = micrParseResult.AccountNumber,
                Name = ocrResult.MakerName ?? string.Empty,
                Address1 = ocrResult.Address,
                City = ocrResult.City,
                State = ocrResult.State,
                Zip = ocrResult.Zip,
                Kind = ocrResult.CheckType,
                Status = "neutral",
                Notes = Array.Empty<string>(),
            };
        }

        private async Task<(AlltrustTransaction Transaction, CheckCashingInitializationFailedResponse FailReason)> PostTransaction(
            CheckScans scans, Maker maker, CheckOcrResult ocrResult,
            MicrParseResult micrParseResult, string customerId, CheckDecisionResponse decision,
            CancellationToken ct)
        {
            var transaction = new AlltrustTransaction
            {
                Id = string.Empty,
                Check = new Check
                {
                    Id = string.Empty,
                    DisplayCheckImages = new CheckImages
                    {
                        FrontImage = scans.FrontImage,
                        BackImage = scans.BackImage,
                        FrontImageType = "jpeg",
                        BackImageType = "jpeg",
                    },
                    DepositCheckImages = new CheckImages
                    {
                        FrontImage = scans.FrontImageProcessed,
                        BackImage = scans.BackImageProcessed,
                        FrontImageType = "tiff",
                        BackImageType = "tiff",
                        ImageDate = DateTime.Now,
                    },
                    Maker = maker,
                    Amount = ocrResult.Amount,
                    CheckDate = ocrResult.CheckDate,
                    PayeeName = ocrResult.PayeeName,
                    Kind = ocrResult.CheckType,
                    CheckNumber = micrParseResult.CheckNumber,
                    RawMicr = micrParseResult.RawMicr,
                    IqaResult = ocrResult.IqaFailReasons,
                    UsabilityResult = ocrResult.UsabilityFailReasons,
                    Notes = Array.Empty<string>(),
                },
                CustomerId = customerId,
                LocationId = Settings.LocationId,
                Recommendation = decision.Recommendation,
                Result = CheckReviewResult.PreApproved,
                Created = DateTime.Now,
                AllowDuplicates = Settings.AllowDuplicateChecks,
            };

            var result = await AlltrustService.InitializeTransaction(transaction, ct);
            if (result == null)
            {
                // duplicate found
                return (transaction, null);
            }
            return (result, default);
        }

        private async Task<CheckCashingTransactionViewModel> SaveTransactionToDb(AlltrustTransaction alltrustTransaction)
        {
            var now = DateTime.Now;
            var transaction = new CheckCashingTransaction
            {
                Id = Guid.NewGuid(),
                UserId = CurrentUser.UserId!.Value,
                AlltrustTransactionId = alltrustTransaction.Id,
                AmountTotal = alltrustTransaction.Check.Amount,
                AmountRequested = alltrustTransaction.Check.Amount,
                Status = Enums.CheckCashing.CheckCashingTransactionStatus.Created,
                PaymentStatus = CheckCashingPaymentStatus.NotPaid,
                CreatedDate = now,
                StatusDate = now,
            };
            var addResult = await DbContext.CheckCashingTransactions.AddAsync(transaction);
            var entity = addResult.Entity;
            await DbContext.SaveChangesAsync(CancellationToken.None);
            return CheckCashingTransactionViewModel.Build(entity, alltrustTransaction);
        }
    }
}
