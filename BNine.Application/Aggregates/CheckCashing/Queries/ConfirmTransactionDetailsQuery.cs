﻿namespace BNine.Application.Aggregates.CheckCashing.Queries
{
    using MediatR;
    using Models;
    using Models.Transaction;

    public class ConfirmTransactionDetailsQuery : IRequest<(CheckCashingTransactionViewModel Transaction, CheckCashingDeniedResponse DenyResponse)>
    {
        public CheckCashingTransactionViewModel Transaction
        {
            get; set;
        }
    }
}
