﻿namespace BNine.Application.Aggregates.CheckCashing.Queries
{
    using System;
    using Abstractions;
    using MediatR;
    using Models;
    using Newtonsoft.Json;

    public class SubmitRedemptionImagesQuery : MBanqSelfServiceUserRequest, IRequest<SubmitRedemptionImagesResponse>
    {
        [JsonIgnore]
        public Guid TransactionId
        {
            get; set;
        }

        /// <summary>
        /// JPEG of the voided front of the check in base64 format.
        /// </summary>
        [JsonProperty("frontImage")]
        public string FrontImage
        {
            get; set;
        }

        /// <summary>
        /// JPEG of the voided back of the check.
        /// </summary>
        [JsonProperty("backImage")]
        public string BackImage
        {
            get; set;
        }
    }
}
