﻿namespace BNine.Application.Aggregates.CheckCashing.Queries
{
    using System.Linq;
    using System.Threading;
    using System.Threading.Tasks;
    using AutoMapper;
    using BNine.Application.Abstractions;
    using BNine.Application.Interfaces;
    using BNine.Application.Interfaces.Alltrust;
    using BNine.Enums.CheckCashing;
    using MediatR;
    using Microsoft.EntityFrameworkCore;
    using Models.Transaction;

    public class GetTransactionInProgressQueryHandler
        : AbstractRequestHandler
        , IRequestHandler<GetTransactionInProgressQuery, CheckCashingTransactionViewModel>
    {
        private static readonly CheckCashingTransactionStatus[] terminalTransactionStates = new[]
        {
            CheckCashingTransactionStatus.UserAbandoned,
            CheckCashingTransactionStatus.RejectedByAmount,
            CheckCashingTransactionStatus.ApprovementRejected,
            CheckCashingTransactionStatus.ClearingFailed,
        };

        public IAlltrustCheckApprovementService AlltrustService
        {
            get;
        }

        public GetTransactionInProgressQueryHandler(
            IMediator mediator,
            IBNineDbContext dbContext,
            IMapper mapper,
            ICurrentUserService currentUser,
            IAlltrustCheckApprovementService alltrustService)
            : base(mediator, dbContext, mapper, currentUser)
        {
            AlltrustService = alltrustService;
        }

        public async Task<CheckCashingTransactionViewModel> Handle(GetTransactionInProgressQuery request, CancellationToken cancellationToken)
        {
            // TODO: mark old transactions as expired
            var transaction = await DbContext
                .CheckCashingTransactions
                .FirstOrDefaultAsync(cct =>
                        cct.UserId == CurrentUser.UserId &&
                        !terminalTransactionStates.Contains(cct.Status),
                    cancellationToken);

            if (transaction == null)
            {
                return null;
            }

            var alltrustTransaction = await AlltrustService.GetTransaction(transaction.AlltrustTransactionId, cancellationToken);
            return CheckCashingTransactionViewModel.Build(transaction, alltrustTransaction);
        }
    }
}
