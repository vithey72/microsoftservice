﻿namespace BNine.Application.Aggregates.CheckCashing.Queries
{
    using System;
    using System.Collections.Generic;
    using System.Threading;
    using System.Threading.Tasks;
    using Abstractions;
    using AutoMapper;
    using BNine.Settings;
    using Configuration.Queries.GetConfiguration;
    using Constants;
    using Enums.CheckCashing;
    using Interfaces;
    using Interfaces.Alltrust;
    using MediatR;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.Extensions.Logging;
    using Microsoft.Extensions.Options;
    using Models;
    using Models.Transaction;

    public class ConfirmTransactionDetailsQueryHandler
        : AbstractRequestHandler
        , IRequestHandler<ConfirmTransactionDetailsQuery, (CheckCashingTransactionViewModel Transaction, CheckCashingDeniedResponse DenyReason)>
    {
        private readonly ICheckProblemsLoggingService _checkProblemsLoggingService;

        private ILogger<InitializeCheckCashingTransactionQueryHandler> Logger
        {
            get;
        }

        private IAlltrustCheckApprovementService AlltrustService
        {
            get;
        }

        private AlltrustSettings Settings
        {
            get;
        }

        public ConfirmTransactionDetailsQueryHandler(
            IMediator mediator,
            IBNineDbContext dbContext,
            IMapper mapper,
            ICurrentUserService currentUser,
            IOptions<AlltrustSettings> options,
            ILogger<InitializeCheckCashingTransactionQueryHandler> logger,
            ICheckProblemsLoggingService checkProblemsLoggingService,
            IAlltrustCheckApprovementService alltrustService)
            : base(mediator, dbContext, mapper, currentUser)
        {
            _checkProblemsLoggingService = checkProblemsLoggingService;
            AlltrustService = alltrustService;
            Settings = options.Value;
            Logger = logger;
        }

        public async Task<(CheckCashingTransactionViewModel Transaction, CheckCashingDeniedResponse DenyReason)> Handle(ConfirmTransactionDetailsQuery request, CancellationToken cancellationToken)
        {
            var existingTransaction = await DbContext
                .CheckCashingTransactions
                .Include(cct => cct.User.AlltrustCustomerSettings)
                .AsNoTracking()
                .FirstOrDefaultAsync(
                    cct => cct.Id == request.Transaction.Id,
                    cancellationToken);

            if (existingTransaction == null)
            {
                throw new KeyNotFoundException($"Transaction with Id = {request.Transaction.Id} was not found");
            }
            if (existingTransaction.Status != CheckCashingTransactionStatus.Created)
            {
                throw new InvalidOperationException($"Expected transaction in state {nameof(CheckCashingTransactionStatus.Created)}");
            }

            if (request.Transaction.UserSetAmount > existingTransaction.AmountTotal)
            {
                return (null, new CheckCashingDeniedResponse { Reason = CheckCashingDenyReason.CheckAmountMismatch });
            }

            var existingAlltrustTransaction = await AlltrustService.GetTransaction(existingTransaction.AlltrustTransactionId, cancellationToken);
            if (existingAlltrustTransaction == null)
            {
                return (null, new CheckCashingDeniedResponse { Reason = CheckCashingDenyReason.CheckDuplicate });
            }

            var maxAmount = await GetMaxAmount();
            if (existingAlltrustTransaction.Check.Amount / 100m > maxAmount)
            {
                return (null, new CheckCashingDeniedResponse
                {
                    Reason = CheckCashingDenyReason.CheckAmountGreaterThanAcceptable,
                    MaxCheckAmount = maxAmount,
                });
            }

            if (!Settings.AllowDuplicateChecks)
            {
                var duplicatesCheckResult = await AlltrustService.FindCheckDuplicates(
                    existingAlltrustTransaction.Check.Maker.AbaNumber,
                    existingAlltrustTransaction.Check.Maker.AccountNumber,
                    existingAlltrustTransaction.Check.CheckNumber,
                    request.Transaction.CheckDate,
                    cancellationToken);

                if (duplicatesCheckResult.IsDuplicate && !duplicatesCheckResult.MultipleDuplicates &&
                    duplicatesCheckResult.DuplicateResult != CheckReviewResult.Cancelled)
                {
                    return (null, await _checkProblemsLoggingService.DenyAsDuplicate(existingAlltrustTransaction, duplicatesCheckResult));
                }
            }

            var decisionRequest = CheckDecisionRequest.FromTransaction(
                existingTransaction.User.AlltrustCustomerSettings.AlltrustCustomerId, existingAlltrustTransaction, existingAlltrustTransaction.Check.Maker, Settings);
            var decision = await AlltrustService.GetCheckDecision(decisionRequest, cancellationToken);
            // TODO: save decision to alltrust
            if (decision.Recommendation != CheckRecommendation.Accept)
            {
                return (null, await _checkProblemsLoggingService.DenyByAlltrustDecisioning(existingAlltrustTransaction, decision));
            }

            existingAlltrustTransaction.Recommendation = decision.Recommendation;

            var updateMaker = existingAlltrustTransaction.Check.Maker.Name != request.Transaction.MakerName;
            existingAlltrustTransaction.Check.Maker.Name = request.Transaction.MakerName;
            existingAlltrustTransaction.Check.PayeeName = request.Transaction.PayeeName;
            existingAlltrustTransaction.Check.CheckDate = request.Transaction.CheckDate;
            existingAlltrustTransaction.Check.Kind = request.Transaction.CheckType;

            var updatedTransaction = await AlltrustService.UpdateCheck(existingAlltrustTransaction, updateMaker, cancellationToken);

            var transactionEntity = await DbContext
                .CheckCashingTransactions
                .FirstOrDefaultAsync(
                    cct => cct.Id == request.Transaction.Id,
                    cancellationToken);
            transactionEntity.AmountRequested = request.Transaction.UserSetAmount;
            transactionEntity.Status = CheckCashingTransactionStatus.DetailsConfirmedByUser;
            transactionEntity.StatusDate = DateTime.Now;
            await DbContext.SaveChangesAsync(cancellationToken);

            var successMessage = $"Succesful check init for user {CurrentUser.UserId}. TransactionId: {existingAlltrustTransaction.Id}. ";
            var successMessageWithBlobName = await _checkProblemsLoggingService.SaveOperationDataToStorage("ok", successMessage, request, success: true);
            Logger.LogInformation(successMessageWithBlobName);

            return (CheckCashingTransactionViewModel.Build(transactionEntity, updatedTransaction), null);
        }

        private async Task<decimal> GetMaxAmount()
        {
            var config = await Mediator.Send(new GetConfigurationQuery());
            var fallbackMaxAmount = 1000.00m;
            if (config.PropertyExists(FeaturesNames.Features.CheckCashing, FeaturesNames.Settings.CheckCashing.MaxAmount))
            {
                return (decimal)(config.Features[FeaturesNames.Features.CheckCashing] as dynamic).MaxAmount;
            }
            return fallbackMaxAmount;
        }
    }
}
