﻿namespace BNine.Application.Aggregates.CheckCashing.Queries
{
    using System.Collections.Generic;
    using System.Threading;
    using System.Threading.Tasks;
    using Abstractions;
    using AutoMapper;
    using Interfaces;
    using Interfaces.Alltrust;
    using MediatR;
    using Microsoft.EntityFrameworkCore;

    public class GetAlltrustCustomerIdQueryHandler
        : AbstractRequestHandler, IRequestHandler<GetAlltrustCustomerIdQuery, string>
    {
        public IAlltrustCustomerEnrollmentService AlltrustService
        {
            get;
        }

        public GetAlltrustCustomerIdQueryHandler(
            IMediator mediator,
            IBNineDbContext dbContext,
            IMapper mapper,
            ICurrentUserService currentUser,
            IAlltrustCustomerEnrollmentService alltrustService) : base(mediator, dbContext, mapper, currentUser)
        {
            AlltrustService = alltrustService;
        }

        /// <summary>
        /// Returns CustomerId and enrolls the user as an Alltrust customer if necessary.
        /// </summary>
        public async Task<string> Handle(GetAlltrustCustomerIdQuery request, CancellationToken cancellationToken)
        {
            var user = await DbContext.Users
                .Include(x => x.Address)
                .Include(x => x.Document)
                .Include(x => x.AlltrustCustomerSettings)
                .FirstOrDefaultAsync(x => x.Id == CurrentUser.UserId, cancellationToken);

            if (user == null)
            {
                throw new KeyNotFoundException($"User with Id = {CurrentUser.UserId} does not exist.");
            }

            if (user.AlltrustCustomerSettings?.AlltrustCustomerId != null)
            {
                return user.AlltrustCustomerSettings.AlltrustCustomerId;
            }

            if (user.Address == null)
            {
                throw new KeyNotFoundException($"Cannot enroll user with Id = {CurrentUser.UserId}: user address is not filled.");
            }
            if (user.Document == null)
            {
                throw new KeyNotFoundException($"Cannot enroll user with Id = {CurrentUser.UserId}: user does not have identification documents.");
            }
            await AlltrustService.EnrollCustomer(user, cancellationToken);
            await DbContext.SaveChangesAsync(cancellationToken);

            return user.AlltrustCustomerSettings!.AlltrustCustomerId;
        }
    }
}
