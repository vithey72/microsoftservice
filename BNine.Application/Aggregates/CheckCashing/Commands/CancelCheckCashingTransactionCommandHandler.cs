﻿namespace BNine.Application.Aggregates.CheckCashing.Commands
{
    using System;
    using System.Linq;
    using System.Threading;
    using System.Threading.Tasks;
    using Abstractions;
    using AutoMapper;
    using Interfaces;
    using Interfaces.Alltrust;
    using MediatR;

    public class CancelCheckCashingTransactionCommandHandler
        : AbstractRequestHandler
        , IRequestHandler<CancelCheckCashingTransactionCommand, Unit>
    {
        public IAlltrustCheckApprovementService AlltrustService
        {
            get;
        }

        public CancelCheckCashingTransactionCommandHandler(
            IMediator mediator,
            IBNineDbContext dbContext,
            IMapper mapper,
            ICurrentUserService currentUser,
            IAlltrustCheckApprovementService alltrustService)
            : base(mediator, dbContext, mapper, currentUser)
        {
            AlltrustService = alltrustService;
        }

        public async Task<Unit> Handle(CancelCheckCashingTransactionCommand request, CancellationToken cancellationToken)
        {
            var transaction = DbContext
                .CheckCashingTransactions
                .FirstOrDefault(cct => cct.Id == request.TransactionId);

            await AlltrustService.CancelTransaction(transaction.AlltrustTransactionId, CancellationToken.None);

            transaction.Status = Enums.CheckCashing.CheckCashingTransactionStatus.UserAbandoned;
            transaction.StatusDate = DateTime.Now;
            await DbContext.SaveChangesAsync(cancellationToken);

            return Unit.Value;
        }
    }
}
