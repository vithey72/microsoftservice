﻿namespace BNine.Application.Aggregates.CheckCashing.Commands
{
    using System;
    using System.Collections.Generic;
    using System.Threading;
    using System.Threading.Tasks;
    using Abstractions;
    using AutoMapper;
    using Domain.Entities.CheckCashing;
    using Enums.CheckCashing;
    using Exceptions;
    using Interfaces;
    using MediatR;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.Extensions.Logging;

    public class MarkCheckClearingCompleteCommandHandler
        : AbstractRequestHandler
        , IRequestHandler<MarkCheckClearingCompleteCommand, Unit>
    {
        private readonly ILogger<MarkCheckClearingCompleteCommandHandler> Logger;

        public MarkCheckClearingCompleteCommandHandler(
            IMediator mediator,
            IBNineDbContext dbContext,
            IMapper mapper,
            ILogger<MarkCheckClearingCompleteCommandHandler> logger,
            ICurrentUserService currentUser)
            : base(mediator, dbContext, mapper, currentUser)
        {
            Logger = logger;
        }

        public async Task<Unit> Handle(MarkCheckClearingCompleteCommand request, CancellationToken cancellationToken)
        {
            try
            {
                var transaction = await DbContext.CheckCashingTransactions
                    .FirstOrDefaultAsync(t => t.Id == request.TransactionId, cancellationToken);

                if (transaction == null)
                {
                    throw new KeyNotFoundException($"Check cashing transaction with id = {request.TransactionId} does not exist");
                }

                if (transaction.Status != CheckCashingTransactionStatus.Approved)
                {
                    throw new BadRequestException(
                        $"Incorrect transaction state: {transaction.Status}, expected {CheckCashingTransactionStatus.Approved}");
                }

                if (request.Success)
                {
                    transaction.Status = CheckCashingTransactionStatus.ClearingSuccess;
                    transaction.PaymentStatus = transaction.PaymentStatus == CheckCashingPaymentStatus.InstantFundsPaid
                        ? transaction.PaymentStatus = CheckCashingPaymentStatus.PaidRestOfFunds
                        : transaction.PaymentStatus = CheckCashingPaymentStatus.PaidInFull;
                }
                else
                {
                    transaction.Status = CheckCashingTransactionStatus.ClearingFailed;
                    transaction.PaymentStatus = transaction.PaymentStatus == CheckCashingPaymentStatus.InstantFundsPaid
                        ? transaction.PaymentStatus = CheckCashingPaymentStatus.InstantPaymentReverted
                        : transaction.PaymentStatus = CheckCashingPaymentStatus.NotPaid;
                }

                transaction.StatusDate = DateTime.Now;

                await DbContext.SaveChangesAsync(cancellationToken);
            }
            catch (Exception exception)
            {
                // swallow exception so that webhook response is 200.
                Logger.LogError(exception, "Problem marking check as cleared");
            }

            return Unit.Value;
        }
    }
}
