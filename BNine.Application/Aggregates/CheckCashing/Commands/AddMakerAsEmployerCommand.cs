﻿namespace BNine.Application.Aggregates.CheckCashing.Commands
{
    using System;
    using MediatR;

    public class AddMakerAsEmployerCommand : IRequest<Unit>
    {
        public Guid TransactionId
        {
            get; set;
        }
    }
}
