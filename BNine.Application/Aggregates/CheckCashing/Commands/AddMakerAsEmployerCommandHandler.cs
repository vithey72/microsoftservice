﻿namespace BNine.Application.Aggregates.CheckCashing.Commands
{
    using System.Collections.Generic;
    using System.Threading;
    using System.Threading.Tasks;
    using Abstractions;
    using AutoMapper;
    using Domain.Entities.CheckCashing;
    using Interfaces;
    using Interfaces.Alltrust;
    using MediatR;
    using Microsoft.EntityFrameworkCore;
    using Models;

    public class AddMakerAsEmployerCommandHandler
        : AbstractRequestHandler,
          IRequestHandler<AddMakerAsEmployerCommand, Unit>
    {
        public IAlltrustCheckApprovementService AlltrustService
        {
            get;
        }

        public AddMakerAsEmployerCommandHandler(
            IMediator mediator,
            IBNineDbContext dbContext,
            IMapper mapper,
            IAlltrustCheckApprovementService alltrustService,
            ICurrentUserService currentUser)
            : base(mediator, dbContext, mapper, currentUser)
        {
            AlltrustService = alltrustService;
        }

        public async Task<Unit> Handle(AddMakerAsEmployerCommand request, CancellationToken cancellationToken)
        {
            var transaction = await DbContext
                .CheckCashingTransactions
                .FirstOrDefaultAsync(x => x.Id == request.TransactionId, cancellationToken);

            if (transaction == null)
            {
                throw new KeyNotFoundException($"Transaction with Id = {request.TransactionId} does not exist.");
            }

            var alltrustTransaction = await AlltrustService.GetTransaction(transaction.AlltrustTransactionId, cancellationToken);
            var alltrustMaker = alltrustTransaction.Check.Maker;

            var makerEntity = await GetOrCreateMakerEntity(cancellationToken, alltrustMaker);

            var user = await DbContext
                .Users
                .Include(x => x.AlltrustCustomerSettings)
                .FirstOrDefaultAsync(x => x.Id == transaction.UserId, cancellationToken);

            transaction.CheckMakerId = makerEntity.Id;
            user.AlltrustCustomerSettings.CurrentEmployerId = makerEntity.Id;
            user.AlltrustCustomerSettings.UpdatedAt = DateTime.Now;

            await DbContext.SaveChangesAsync(cancellationToken);

            return Unit.Value;
        }

        private async Task<CheckMaker> GetOrCreateMakerEntity(CancellationToken cancellationToken, Maker alltrustMaker)
        {
            var existingMaker = await DbContext
                .CheckMakers
                .FirstOrDefaultAsync(cm =>
                        cm.MakerRoutingNumber == alltrustMaker.AbaNumber &&
                        cm.MakerAccountNumber == alltrustMaker.AccountNumber,
                    cancellationToken);

            if (existingMaker == null)
            {
                var checkMaker = new CheckMaker
                {
                    MakerRoutingNumber = alltrustMaker.AbaNumber,
                    MakerAccountNumber = alltrustMaker.AccountNumber,
                    MakerName = alltrustMaker.Name,
                };
                var result = await DbContext.CheckMakers.AddAsync(checkMaker, cancellationToken);
                await DbContext.SaveChangesAsync(cancellationToken);
                return result.Entity;
            }

            return existingMaker;
        }
    }
}
