﻿namespace BNine.Application.Aggregates.CheckCashing.Commands
{
    using System;
    using MediatR;

    public class MarkCheckClearingCompleteCommand : IRequest<Unit>
    {
        public MarkCheckClearingCompleteCommand(Guid transactionId, bool success)
        {
            TransactionId = transactionId;
            Success = success;
        }
        public Guid TransactionId
        {
            get;
        }

        public bool Success
        {
            get;
        }
    }
}
