﻿namespace BNine.Application.Aggregates.CheckCashing.Commands
{
    using System;
    using MediatR;

    public class CancelCheckCashingTransactionCommand : IRequest<Unit>
    {
        public Guid TransactionId
        {
            get; set;
        }
    }
}
