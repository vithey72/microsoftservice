﻿namespace BNine.Application.Aggregates.CheckCashing.Models
{
    using Enums.CheckCashing;
    using Newtonsoft.Json;

    public class CheckCashingDeniedResponse
    {
        public CheckCashingDeniedResponse()
        {
        }

        /// <summary>
        /// Type of problem with the check.
        /// </summary>
        [JsonProperty("reason")]
        public CheckCashingDenyReason Reason
        {
            get; set;
        }

        /// <summary>
        /// If problem is of type <see cref="CheckCashingDenyReason.CheckAmountGreaterThanAcceptable"/>, this field contains the maximum accepted amount.
        /// </summary>
        [JsonProperty("maxCheckAmount")]
        public decimal MaxCheckAmount
        {
            get; set;
        }
    }
}
