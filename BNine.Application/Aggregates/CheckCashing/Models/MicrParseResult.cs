﻿namespace BNine.Application.Aggregates.CheckCashing.Models
{
    using Enums.CheckCashing;
    using Newtonsoft.Json;

    public class MicrParseResult
    {
        [JsonProperty("raw_micr")]
        public string RawMicr
        {
            get; set;
        }

        [JsonProperty("parse_error")]
        public bool ParseError
        {
            get; set;
        }

        [JsonProperty("status_message")]
        public string StatusMessage
        {
            get; set;
        }

        [JsonProperty("contains_unrecognized_symbols")]
        public bool ContainsUnrecognizedSymbols
        {
            get; set;
        }

        [JsonProperty("check_type")]
        public CheckType CheckType
        {
            get; set;
        }

        [JsonProperty("aux_onus")]
        public string AuxOnus
        {
            get; set;
        }

        /// <summary>
        /// Aka routing number, aka ABA, the indentifier of the bank.
        /// </summary>
        [JsonProperty("transit_number")]
        public string TransitNumber
        {
            get; set;
        }

        [JsonProperty("onus")]
        public string Onus
        {
            get; set;
        }

        /// <summary>
        /// Payer's account number in bank.
        /// </summary>
        [JsonProperty("account_number")]
        public string AccountNumber
        {
            get; set;
        }

        [JsonProperty("check_number")]
        public string CheckNumber
        {
            get; set;
        }

        [JsonProperty("special")]
        public bool Special
        {
            get; set;
        }
    }
}
