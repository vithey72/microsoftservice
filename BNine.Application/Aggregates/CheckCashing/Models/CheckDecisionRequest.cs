﻿namespace BNine.Application.Aggregates.CheckCashing.Models
{
    using System;
    using BNine.Settings;
    using Newtonsoft.Json;
    using Transaction;

    public class CheckDecisionRequest
    {
        public static CheckDecisionRequest FromTransaction(string customerId, AlltrustTransaction transaction, Maker maker, AlltrustSettings settings)
        {
            var decisionRequest = new CheckDecisionRequest
            {
                Amount = transaction.Check.Amount,
                BusinessId = settings.BusinessId,
                LocationId = settings.LocationId,
                CustomerId = customerId,
                CheckDate = transaction.Check.CheckDate,
                CheckNumber = transaction.Check.CheckNumber,
                Maker = maker,
                HandWritten = transaction.Check.HandKeyed,
                OcrAmountMatched = true,
            };
            return decisionRequest;
        }

        [JsonProperty("business_id")]
        public string BusinessId
        {
            get; set;
        }

        [JsonProperty("location_id")]
        public string LocationId
        {
            get; set;
        }

        [JsonProperty("customer_id")]
        public string CustomerId
        {
            get; set;
        }

        [JsonProperty("maker")]
        public Maker Maker
        {
            get; set;
        }

        [JsonProperty("amount")]
        public int Amount
        {
            get; set;
        }

        [JsonProperty("check_number")]
        public string CheckNumber
        {
            get; set;
        }

        [JsonProperty("check_date")]
        public DateTime CheckDate
        {
            get; set;
        }

        [JsonProperty("hand_written")]
        public bool HandWritten
        {
            get; set;
        }

        [JsonProperty("hand_keyed")]
        public bool HandKeyed
        {
            get; set;
        }

        [JsonProperty("ocr_amount_matched")]
        public bool OcrAmountMatched
        {
            get; set;
        }
    }
}
