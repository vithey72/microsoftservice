﻿using Newtonsoft.Json;

namespace BNine.Application.Aggregates.CheckCashing.Models
{
    public class CheckImages
    {
        [JsonProperty("front_image")]
        public string FrontImage
        {
            get; set;
        }

        /// <summary>
        /// jpg or tiff.
        /// </summary>
        [JsonProperty("front_image_type")]
        public string FrontImageType
        {
            get; set;
        }

        [JsonProperty("back_image")]
        public string BackImage
        {
            get; set;
        }

        /// <summary>
        /// jpg or tiff.
        /// </summary>
        [JsonProperty("back_image_type")]
        public string BackImageType
        {
            get; set;
        }

        [JsonProperty("image_date")]
        public DateTime ImageDate
        {
            get; set;
        }
    }
}
