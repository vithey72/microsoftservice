﻿namespace BNine.Application.Aggregates.CheckCashing.Models
{
    using System;
    using Newtonsoft.Json;

    public class CheckRedemptionRequest
    {
        [JsonProperty("check_images")]
        public RedemptionImages CheckImages
        {
            get; set;
        }

        /// <summary>
        /// Customer selfie. Not used.
        /// </summary>
        [JsonProperty("customer_image")]
        public TypedImage CustomerImage
        {
            get; set;
        }

        public class RedemptionImages
        {
            [JsonProperty("front")]
            public TypedImage Front
            {
                get; set;
            }

            [JsonProperty("back")]
            public TypedImage Back
            {
                get; set;
            }
        }

        public class TypedImage
        {
            [JsonProperty("image")]
            public string Image
            {
                get; set;
            }

            [JsonProperty("image_type")]
            public string ImageType
            {
                get; set;
            }

            [JsonProperty("image_date")]
            public DateTime ImageDate
            {
                get; set;
            }
        }
    }
}
