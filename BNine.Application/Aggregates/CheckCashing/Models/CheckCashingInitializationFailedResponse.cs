﻿namespace BNine.Application.Aggregates.CheckCashing.Models
{
    using Enums.CheckCashing;
    using Newtonsoft.Json;

    public class CheckCashingInitializationFailedResponse
    {
        public CheckCashingInitializationFailedResponse(CheckDataErrorType reason)
        {
            Reason = reason;
        }

        /// <summary>
        /// Type of check recognition problem.
        /// </summary>
        [JsonProperty("reason")]
        public CheckDataErrorType Reason
        {
            get; set;
        }
    }
}
