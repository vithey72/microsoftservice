﻿namespace BNine.Application.Aggregates.CheckCashing.Models
{
    using System;
    using BNine.Enums.CheckCashing;
    using Newtonsoft.Json;
    using Newtonsoft.Json.Converters;

    public class Maker
    {
        [JsonProperty("id")]
        public string Id
        {
            get; set;
        }

        /// <summary>
        /// Aka routing number.
        /// </summary>
        [JsonProperty("aba_number")]
        public string AbaNumber
        {
            get; set;
        }

        [JsonProperty("account_number")]
        public string AccountNumber
        {
            get; set;
        }

        [JsonProperty("status")]
        public string Status
        {
            get; set;
        }

        [JsonProperty("kind")]
        [JsonConverter(typeof(StringEnumConverter))]
        public CheckType Kind
        {
            get; set;
        }

        [JsonProperty("name")]
        public string Name
        {
            get; set;
        }

        [JsonProperty("address1")]
        public string Address1
        {
            get; set;
        }

        [JsonProperty("address2")]
        public string Address2
        {
            get; set;
        }

        [JsonProperty("city")]
        public string City
        {
            get; set;
        }

        [JsonProperty("state")]
        public string State
        {
            get; set;
        }

        [JsonProperty("zip")]
        public string Zip
        {
            get; set;
        }

        [JsonProperty("phone")]
        public string Phone
        {
            get; set;
        }

        [JsonProperty("alt_phone")]
        public string AltPhone
        {
            get; set;
        }

        [JsonProperty("created")]
        public DateTime CreatedAt
        {
            get; set;
        }

        [JsonProperty("updated")]
        public DateTime UpdatedAt
        {
            get; set;
        }

        [JsonProperty("notes")]
        public string[] Notes
        {
            get; set;
        }

        [JsonProperty("excluded")]
        public bool Excluded
        {
            get; set;
        }

        public int GetDataQualityScore()
        {
            var score = 0;
            if (Excluded) { return score; }

            if (!string.IsNullOrEmpty(Name)) { score += 10; }
            if (Kind != CheckType.Other && Kind != CheckType.Unknown) { score += 10; }

            if (!string.IsNullOrEmpty(Address1)) { score += 1; }
            if (!string.IsNullOrEmpty(Address2)) { score += 1; }
            if (!string.IsNullOrEmpty(City)) { score += 1; }
            if (!string.IsNullOrEmpty(Zip)) { score += 1; }
            if (!string.IsNullOrEmpty(Phone)) { score += 1; }
            if (!string.IsNullOrEmpty(AltPhone)) { score += 1; }

            // prefer makers created automatically to user input
            if (UpdatedAt != default) { score += 5; }

            return score;
        }
    }
}
