﻿namespace BNine.Application.Aggregates.CheckCashing.Models
{
    using Enums.CheckCashing;
    using Newtonsoft.Json;
    using Transaction;

    public class SubmitRedemptionImagesResponse
    {
        [JsonProperty("transaction")]
        public CheckCashingTransactionViewModel Transaction
        {
            get; set;
        }

        [JsonProperty("userReputation")]
        public UserReputationType UserReputation
        {
            get; set;
        }


        [JsonProperty("instantCashingAmount")]
        public decimal InstantCashingAmount
        {
            get; set;
        }

        /// <summary>
        /// Name of employer. If not null then user should be asked if they want to save this employer.
        /// </summary>
        [JsonProperty("employerName")]
        public string EmployerName
        {
            get;
            set;
        }
    }
}
