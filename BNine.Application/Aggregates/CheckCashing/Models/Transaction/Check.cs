﻿namespace BNine.Application.Aggregates.CheckCashing.Models.Transaction
{
    using System;
    using Enums.CheckCashing;
    using Newtonsoft.Json;
    using Newtonsoft.Json.Converters;

    public class Check
    {
        [JsonProperty("id")]
        public string Id
        {
            get; set;
        }

        [JsonProperty("maker")]
        public Maker Maker
        {
            get; set;
        }

        /// <summary>
        /// Amount in USD, 101 = 1.01$.
        /// </summary>
        [JsonProperty("amount")]
        public int Amount
        {
            get; set;
        }

        [JsonProperty("check_number")]
        public string CheckNumber
        {
            get; set;
        }

        [JsonProperty("check_date")]
        public DateTime CheckDate
        {
            get; set;
        }

        [JsonProperty("kind")]
        [JsonConverter(typeof(StringEnumConverter))]
        public CheckType Kind
        {
            get; set;
        }

        [JsonProperty("raw_micr")]
        public string RawMicr
        {
            get; set;
        }

        [JsonProperty("hand_keyed")]
        public bool HandKeyed
        {
            get; set;
        }

        [JsonProperty("iqa_result")]
        public string[] IqaResult
        {
            get; set;
        }

        [JsonProperty("display_check_images")]
        public CheckImages DisplayCheckImages
        {
            get; set;
        }

        [JsonProperty("deposit_check_images")]
        public CheckImages DepositCheckImages
        {
            get; set;
        }

        [JsonProperty("payee_name")]
        public string PayeeName
        {
            get; set;
        }

        [JsonProperty("payee_is_business")]
        public bool PayeeIsBusiness
        {
            get; set;
        }

        [JsonProperty("usability_result")]
        public string[] UsabilityResult
        {
            get; set;
        }

        [JsonProperty("notes")]
        public string[] Notes
        {
            get; set;
        }
    }
}
