﻿namespace BNine.Application.Aggregates.CheckCashing.Models.Transaction
{
    using System;
    using Enums.CheckCashing;
    using Newtonsoft.Json;

    public class CheckOcrResult
    {
        /// <summary>
        /// Machine-readable line from the front of the check that contains routing number, account number and check number.
        /// </summary>
        [JsonProperty("micr")]
        public string Micr
        {
            get; set;
        }

        [JsonProperty("amount")]
        public int Amount
        {
            get; set;
        }

        [JsonProperty("check_date")]
        public DateTime CheckDate
        {
            get; set;
        }

        [JsonProperty("maker_name1")]
        public string MakerName
        {
            get; set;
        }

        [JsonProperty("address")]
        public string Address
        {
            get; set;
        }

        [JsonProperty("city")]
        public string City
        {
            get; set;
        }

        [JsonProperty("state")]
        public string State
        {
            get; set;
        }

        [JsonProperty("zip")]
        public string Zip
        {
            get; set;
        }
        [JsonProperty("payeeName")]
        public string PayeeName
        {
            get; set;
        }

        [JsonProperty("handwritten")]
        public bool Handwritten
        {
            get; set;
        }

        [JsonProperty("image_quality")]
        public string ImageQuality
        {
            get; set;
        }

        /// <summary>
        /// List of problems with images quality.
        /// </summary>
        [JsonProperty("iqa_fail_reasons")]
        public string[] IqaFailReasons
        {
            get; set;
        }

        /// <summary>
        /// Should be "OK" to accept check.
        /// </summary>
        [JsonProperty("usability")]
        public string Usability
        {
            get; set;
        }

        [JsonProperty("usability_fail_reasons")]
        public string[] UsabilityFailReasons
        {
            get; set;
        }

        [JsonProperty("check_type")]
        public CheckType CheckType
        {
            get; set;
        }
    }
}
