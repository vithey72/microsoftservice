﻿namespace BNine.Application.Aggregates.CheckCashing.Models.Transaction
{
    using Newtonsoft.Json;

    public class CheckScans
    {
        /// <summary>
        /// JPEG of the front of the check in base64 format.
        /// </summary>
        [JsonProperty("frontImage")]
        public string FrontImage
        {
            get; set;
        }

        /// <summary>
        /// JPEG of the back of the check.
        /// </summary>
        [JsonProperty("backImage")]
        public string BackImage
        {
            get; set;
        }

        /// <summary>
        /// TIFF of the front of the check in base64 format, Check21 compliant.
        /// </summary>
        [JsonProperty("frontImageProcessed")]
        public string FrontImageProcessed
        {
            get; set;
        }

        /// <summary>
        /// TIFF of the back of the check.
        /// </summary>
        [JsonProperty("backImageProcessed")]
        public string BackImageProcessed
        {
            get; set;
        }
    }
}
