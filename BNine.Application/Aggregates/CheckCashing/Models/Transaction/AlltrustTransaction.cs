﻿namespace BNine.Application.Aggregates.CheckCashing.Models.Transaction
{
    using System;
    using Enums.CheckCashing;
    using Newtonsoft.Json;
    using Newtonsoft.Json.Converters;

    public class AlltrustTransaction
    {
        [JsonProperty("id")]
        public string Id
        {
            get; set;
        }

        [JsonProperty("check")]
        public Check Check
        {
            get; set;
        }

        /// <summary>
        /// Alltrust customer id (customer_{guid}).
        /// </summary>
        [JsonProperty("customer_id")]
        public string CustomerId
        {
            get; set;
        }

        [JsonProperty("customer_name")]
        public string CustomerName
        {
            get; set;
        }

        [JsonProperty("location_id")]
        public string LocationId
        {
            get; set;
        }

        [JsonProperty("location_name")]
        public string LocationName
        {
            get; set;
        }

        [JsonProperty("recommendation")]
        [JsonConverter(typeof(StringEnumConverter))]
        public CheckRecommendation Recommendation
        {
            get; set;
        }

        [JsonProperty("result")]
        [JsonConverter(typeof(StringEnumConverter))]
        public CheckReviewResult Result
        {
            get; set;
        }

        [JsonProperty("current_status")]
        [JsonConverter(typeof(StringEnumConverter))]
        public CheckReviewResult? CurrentStatus
        {
            get; set;
        }

        [JsonProperty("fee_amount")]
        public int FeeAmount
        {
            get; set;
        }

        [JsonProperty("merchant_user_name")]
        public string MerchantUserName
        {
            get; set;
        } = "bnine.com";

        [JsonProperty("created")]
        public DateTime Created
        {
            get; set;
        }

        [JsonProperty("deposited")]
        public bool Deposited
        {
            get; set;
        }

        [JsonProperty("extra_info")]
        public TransactionExtraInfo ExtraInfo
        {
            get; set;
        }

        [JsonProperty("allow_duplicates")]
        public bool AllowDuplicates
        {
            get; set;
        }

        public class TransactionExtraInfo
        {
            [JsonProperty("established_customer")]
            public bool IsEstabilishedCustomer
            {
                get; set;
            }

            [JsonProperty("established_maker")]
            public bool IsEstabilishedMaker
            {
                get; set;
            }
        }
    }

}
