﻿namespace BNine.Application.Aggregates.CheckCashing.Models.Transaction
{
    using System;
    using Domain.Entities.CheckCashing;
    using Enums.CheckCashing;
    using Mappings;
    using Newtonsoft.Json;

    public class CheckCashingTransactionViewModel : IMapFrom<CheckCashingTransaction>
    {
        [JsonProperty("id")]
        public Guid Id
        {
            get; set;
        }

        [JsonProperty("frontImage")]
        public string FrontImage
        {
            get; set;
        }

        [JsonProperty("backImage")]
        public string BackImage
        {
            get; set;
        }

        [JsonProperty("makerName")]
        public string MakerName
        {
            get; set;
        }

        [JsonProperty("payeeName")]
        public string PayeeName
        {
            get; set;
        }

        [JsonProperty("ocrAmount")]
        public int OcrAmount
        {
            get; set;
        }

        [JsonProperty("userSetAmount")]
        public int UserSetAmount
        {
            get; set;
        }

        [JsonProperty("checkDate")]
        public DateTime CheckDate
        {
            get; set;
        }

        [JsonProperty("checkType")]
        public CheckType CheckType
        {
            get; set;
        }

        [JsonProperty("routingNumber")]
        public string RoutingNumber
        {
            get; set;
        }

        [JsonProperty("accountNumber")]
        public string AccountNumber
        {
            get; set;
        }

        [JsonProperty("checkNumber")]
        public string CheckNumber
        {
            get; set;
        }

        [JsonProperty("status")]
        public CheckCashingTransactionStatus Status
        {
            get; set;
        }

        [JsonProperty("statusDate")]
        public DateTime StatusDate
        {
            get; set;
        }

        public static CheckCashingTransactionViewModel Build(CheckCashingTransaction transactionEntity, AlltrustTransaction alltrustTransaction)
        {
            return new CheckCashingTransactionViewModel
            {
                Id = transactionEntity.Id,
                UserSetAmount = transactionEntity.AmountRequested,
                Status = transactionEntity.Status,
                StatusDate = transactionEntity.StatusDate,
                FrontImage = alltrustTransaction.Check.DisplayCheckImages.FrontImage,
                BackImage = alltrustTransaction.Check.DisplayCheckImages.BackImage,
                MakerName = alltrustTransaction.Check.Maker.Name,
                PayeeName = alltrustTransaction.Check.PayeeName,
                OcrAmount = alltrustTransaction.Check.Amount,
                RoutingNumber = alltrustTransaction.Check.Maker.AbaNumber,
                AccountNumber = alltrustTransaction.Check.Maker.AccountNumber,
                CheckNumber = alltrustTransaction.Check.CheckNumber,
                CheckDate = alltrustTransaction.Check.CheckDate,
                CheckType = alltrustTransaction.Check.Kind,
            };
        }
    }
}
