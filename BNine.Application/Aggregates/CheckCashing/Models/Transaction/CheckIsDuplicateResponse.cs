﻿namespace BNine.Application.Aggregates.CheckCashing.Models.Transaction
{
    using Enums.CheckCashing;
    using Newtonsoft.Json;

    public class CheckIsDuplicateResponse
    {
        [JsonProperty("is_duplicate")]
        public bool IsDuplicate
        {
            get; set;
        }

        [JsonProperty("duplicate_check_result")]
        public CheckReviewResult DuplicateResult
        {
            get; set;
        }

        [JsonProperty("multiple_seen")]
        public bool MultipleDuplicates
        {
            get; set;
        }
    }
}
