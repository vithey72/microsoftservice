﻿namespace BNine.Application.Aggregates.CheckCashing.Models
{
    using Enums.CheckCashing;
    using Newtonsoft.Json;

    public class CheckDecisionResponse
    {
        [JsonProperty("score")]
        public int Score
        {
            get; set;
        }

        [JsonProperty("auto_result")]
        public string AutoResult
        {
            get; set;
        }

        [JsonProperty("recommendation")]
        public CheckRecommendation Recommendation
        {
            get; set;
        }

        [JsonProperty("score_based_recommendation")]
        public CheckRecommendation ScoreBasedRecommendation
        {
            get; set;
        }

        // CBA to add this model.
        ////public DecisionDetails Details
        ////{
        ////    get; set;
        ////}

        /// <summary>
        /// 'decision_{guid}'.
        /// </summary>
        [JsonProperty("decision_id")]
        public string DecisionId
        {
            get; set;
        }

        /// <summary>
        /// Lowest possible score to accept. Configured in Alltrust admin interface.
        /// </summary>
        [JsonProperty("low_range")]
        public int LowRange
        {
            get; set;
        }

        /// <summary>
        /// Highest possible score to decline. Configured in Alltrust admin interface.
        /// </summary>
        [JsonProperty("high_range")]
        public int HighRange
        {
            get; set;
        }
    }
}
