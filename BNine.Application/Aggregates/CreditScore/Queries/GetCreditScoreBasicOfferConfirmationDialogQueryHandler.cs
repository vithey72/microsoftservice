﻿namespace BNine.Application.Aggregates.CreditScore.Queries;

using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using BNine.Application.Abstractions;
using BNine.Application.Aggregates.CommonModels.Dialog;
using BNine.Application.Interfaces;
using MediatR;

public class GetCreditScoreBasicOfferConfirmationDialogQueryHandler : AbstractRequestHandler
    , IRequestHandler<GetCreditScoreBasicOfferConfirmationDialogQuery, GenericConfirmationDialogViewModel>
{
    public GetCreditScoreBasicOfferConfirmationDialogQueryHandler(
        IMediator mediator,
        IBNineDbContext dbContext,
        IMapper mapper,
        ICurrentUserService currentUser)
        : base(mediator, dbContext, mapper, currentUser)
    {
    }

    public Task<GenericConfirmationDialogViewModel> Handle(GetCreditScoreBasicOfferConfirmationDialogQuery request, CancellationToken cancellationToken)
    {
        var response = new GenericConfirmationDialogViewModel
        {
            Header = "Get My Credit Score",
            ConfirmationButtonText = "YES, CONTINUE",
            CancelButtonText = "NO, BACK",
            Features = new[]
            {
                new GenericFeatureViewModel
                {
                    Text = "$5 for 14 days",
                    IconType = "tag",
                }
            }
        };

        return Task.FromResult(response);
    }
}
