﻿namespace BNine.Application.Aggregates.CreditScore.Queries;

using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using BNine.Application.Abstractions;
using BNine.Application.Aggregates.CommonModels.Dialog;
using BNine.Application.Aggregates.CreditScore.Models;
using BNine.Application.Interfaces;
using BNine.Domain.Entities.PaidUserServices;
using BNine.Domain.Entities.User;
using BNine.Enums.TariffPlan;
using MediatR;
using Microsoft.EntityFrameworkCore;
using IBankSavingAccountsService = Interfaces.Bank.Client.IBankSavingAccountsService;

public class ActivateCheckCreditScorePaidServiceQueryHandler : AbstractRequestHandler
    , IRequestHandler<ActivateCheckCreditScorePaidServiceQuery, GenericActivateServiceResultDialogViewModel>
{
    private const decimal CreditScorePaidServiceCost = 5m;

    private readonly ICheckCreditScorePaidService _creditScorePaidService;
    private readonly ITariffPlanService _tariffPlanService;
    private readonly IBankSavingAccountsService _bankSavingAccountsService;

    public ActivateCheckCreditScorePaidServiceQueryHandler(
        IMediator mediator,
        IBNineDbContext dbContext,
        IMapper mapper,
        ICurrentUserService currentUser,
        ICheckCreditScorePaidService creditScorePaidService,
        IBankSavingAccountsService bankSavingAccountsService,
        ITariffPlanService tariffPlanService
        )
        : base(mediator, dbContext, mapper, currentUser)
    {
        _creditScorePaidService = creditScorePaidService;
        _tariffPlanService = tariffPlanService;
        _bankSavingAccountsService = bankSavingAccountsService;
    }

    public async Task<GenericActivateServiceResultDialogViewModel> Handle(
        ActivateCheckCreditScorePaidServiceQuery request,
        CancellationToken cancellationToken)
    {
        var user = await GetUserWithAccount(cancellationToken);
        await CheckUserHasBasicTariff(user, cancellationToken);

        var activePaidService = await TryGetActivePaidService(user, cancellationToken);

        if (activePaidService is null)
        {
            if (!await DoesUserHaveSufficientFunds(request, user))
            {
                return CreateInsufficientFundsFault();
            }

            activePaidService = await _creditScorePaidService.Enable(user.Id, null, cancellationToken);

            if (activePaidService is null)
            {
                return CreateGenericFault();
            }
        }

        return CreateSuccess(activePaidService);
    }

    private async Task<User> GetUserWithAccount(CancellationToken cancellationToken)
    {
        var user = await DbContext
            .Users
            .Include(u => u.CurrentAccount)
            .FirstOrDefaultAsync(u => u.Id == CurrentUser.UserId!.Value, cancellationToken);

        if (user == null)
        {
            throw new KeyNotFoundException("User was not present in DB");
        }

        return user;
    }

    private async Task CheckUserHasBasicTariff(User user, CancellationToken cancellationToken)
    {
        var currentTariffFamily = await _tariffPlanService.GetCurrentTariffPlanFamily(user.Id, cancellationToken);
        if (currentTariffFamily != TariffPlanFamily.Advance)
        {
            throw new InvalidOperationException($"Cannot purchase check credit score paid service. User {user.Id} already has Premium tariff.");
        }
    }

    private async Task<UsedPaidUserService> TryGetActivePaidService(User user, CancellationToken cancellationToken)
    {
        if (!await _creditScorePaidService.IsAlreadyPurchased(user.Id, cancellationToken))
        {
            return null;
        }

        return await _creditScorePaidService.GetEnabledForUser(user.Id, cancellationToken);
    }

    private async Task<bool> DoesUserHaveSufficientFunds(ActivateCheckCreditScorePaidServiceQuery request, User user)
    {
        var savingsAccount = await _bankSavingAccountsService.GetSavingAccountInfo(
            user.CurrentAccount.ExternalId,
            request.MbanqAccessToken,
            request.Ip);

        return savingsAccount.AvailableBalance >= CreditScorePaidServiceCost;
    }

    private GenericActivateServiceResultDialogViewModel CreateSuccess(UsedPaidUserService activeService)
    {
        return new GenericActivateServiceResultDialogViewModel
        {
            Header = "Congrats",
            Subtitle = $"Your Credit Score will be available to you \nthrough {activeService.ValidTo:MMMM d, yyyy}",
            ButtonText = "OK",
            IsEnabled = true,
        };
    }

    private GenericActivateServiceResultDialogViewModel CreateInsufficientFundsFault()
    {
        return new GenericActivateServiceResultDialogViewModel
        {
            IsEnabled = false,
            Header = "Insufficient funds",
            Subtitle = $"To buy your Credit Score you'll need to have at least ${CreditScorePaidServiceCost} in your B9 Account",
            ButtonText = "ADD MONEY",
        };
    }

    private GenericActivateServiceResultDialogViewModel CreateGenericFault()
    {
        return CreateInsufficientFundsFault();
    }
}
