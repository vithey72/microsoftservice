﻿namespace BNine.Application.Aggregates.CreditScore.Queries;

using BNine.Application.Aggregates.CommonModels;
using BNine.Application.Aggregates.CreditScore.Models;
using MediatR;

public class GetCreditScoreBasicOfferInfoScreenQuery : IRequest<OptionViewModelResult<CreditScoreBasicOfferInfoScreenViewModel>>
{
}
