﻿namespace BNine.Application.Aggregates.CreditScore.Queries;

using BNine.Application.Aggregates.CommonModels.Dialog;
using MediatR;

public class GetCreditScoreBasicOfferConfirmationDialogQuery : IRequest<GenericConfirmationDialogViewModel>
{
}
