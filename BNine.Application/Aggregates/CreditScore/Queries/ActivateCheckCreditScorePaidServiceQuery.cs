﻿namespace BNine.Application.Aggregates.CreditScore.Queries;

using BNine.Application.Abstractions;
using BNine.Application.Aggregates.CommonModels.Dialog;
using MediatR;

public class ActivateCheckCreditScorePaidServiceQuery : MBanqSelfServiceUserRequest, IRequest<GenericActivateServiceResultDialogViewModel>
{
}
