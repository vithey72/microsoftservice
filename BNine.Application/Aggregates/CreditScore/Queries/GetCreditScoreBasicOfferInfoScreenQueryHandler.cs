﻿namespace BNine.Application.Aggregates.CreditScore.Queries;

using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using BNine.Application.Abstractions;
using BNine.Application.Aggregates.CommonModels;
using BNine.Application.Aggregates.CreditScore.Models;
using BNine.Application.Interfaces;
using BNine.Domain.Entities.PaidUserServices;
using BNine.Enums.TariffPlan;
using MediatR;

public class GetCreditScoreBasicOfferInfoScreenQueryHandler : AbstractRequestHandler
    , IRequestHandler<GetCreditScoreBasicOfferInfoScreenQuery, OptionViewModelResult<CreditScoreBasicOfferInfoScreenViewModel>>
{
    private readonly ICheckCreditScorePaidService _creditScorePaidService;
    private readonly ITariffPlanService _tariffPlanService;
    private readonly ICreditScoreBasicOfferInfoScreenViewModelFactory _viewModelFactory;

    public GetCreditScoreBasicOfferInfoScreenQueryHandler(
        IMediator mediator,
        IBNineDbContext dbContext,
        IMapper mapper,
        ICurrentUserService currentUser,
        ICheckCreditScorePaidService creditScorePaidService,
        ITariffPlanService tariffPlanService,
        ICreditScoreBasicOfferInfoScreenViewModelFactory viewModelFactory
    ) : base(mediator, dbContext, mapper, currentUser)
    {
        _creditScorePaidService = creditScorePaidService;
        _tariffPlanService = tariffPlanService;
        _viewModelFactory = viewModelFactory;
    }

    public async Task<OptionViewModelResult<CreditScoreBasicOfferInfoScreenViewModel>> Handle(
        GetCreditScoreBasicOfferInfoScreenQuery request,
        CancellationToken cancellationToken
        )
    {
        var planType = await _tariffPlanService.GetCurrentTariffPlanFamily(CurrentUser.UserId.Value, cancellationToken);
        if (planType == TariffPlanFamily.Premium)
        {
            return OptionViewModelResult<CreditScoreBasicOfferInfoScreenViewModel>.Empty;
        }

        var activePaidService = await TryGetActivePaidService(cancellationToken);

        var viewModel = activePaidService is not null ?
            _viewModelFactory.GetScreenForActiveService(activePaidService) :
            _viewModelFactory.GetScreenForInactiveService();

        return new(viewModel);
    }

    private async Task<UsedPaidUserService> TryGetActivePaidService(CancellationToken cancellationToken)
    {
        if (!await _creditScorePaidService.IsAlreadyPurchased(CurrentUser.UserId.Value, cancellationToken))
        {
            return null;
        }

        return await _creditScorePaidService.GetEnabledForUser(CurrentUser.UserId.Value, cancellationToken);
    }
}
