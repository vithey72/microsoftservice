﻿namespace BNine.Application.Aggregates.CreditScore.Models;

using BNine.Application.Aggregates.CommonModels.Dialog;

public class CreditScoreBasicOfferInfoScreenViewModel
{
    public GenericDialogHeaderViewModel Header
    {
        get;
        set;
    }

    public CreditScoreBasicOfferInfoScreenBodyViewModel Body
    {
        get;
        set;
    }
}
