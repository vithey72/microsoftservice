﻿namespace BNine.Application.Aggregates.CreditScore.Models;

using BNine.Domain.Entities.PaidUserServices;

#nullable enable

public interface ICreditScoreBasicOfferInfoScreenViewModelFactory
{
    CreditScoreBasicOfferInfoScreenViewModel GetScreenForInactiveService();
    CreditScoreBasicOfferInfoScreenViewModel GetScreenForActiveService(UsedPaidUserService purchasedService);
}
