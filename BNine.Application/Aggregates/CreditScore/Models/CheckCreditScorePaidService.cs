﻿namespace BNine.Application.Aggregates.CreditScore.Models;

using BNine.Application.Aggregates.PaidService;
using BNine.Application.Interfaces.Bank.Administration;
using BNine.Constants;

public class CheckCreditScorePaidService : GenericChangingChargePaidUserService, ICheckCreditScorePaidService
{
    public CheckCreditScorePaidService(IPaidUserServicesService paidUserServicesService)
        : base(
            new ObsoleteCheckCreditScorePaidService(paidUserServicesService),
            new NewCheckCreditScorePaidService(paidUserServicesService))
    {
    }
}

public class ObsoleteCheckCreditScorePaidService : GenericPaidUserService, ICheckCreditScorePaidService
{
    public ObsoleteCheckCreditScorePaidService(IPaidUserServicesService paidUserServicesService)
        : base(PaidUserServices.CheckCreditScorePaidService10USD, paidUserServicesService)
    {
    }
}

public class NewCheckCreditScorePaidService : GenericPaidUserService, ICheckCreditScorePaidService
{
    public NewCheckCreditScorePaidService(IPaidUserServicesService paidUserServicesService)
        : base(PaidUserServices.CheckCreditScorePaidService5USD, paidUserServicesService)
    {
    }
}
