﻿namespace BNine.Application.Aggregates.CreditScore.Models;

using BNine.Application.Aggregates.CommonModels.Buttons;
using BNine.Application.Aggregates.CommonModels.Dialog;
using BNine.Application.Aggregates.CommonModels.DialogElements;
using BNine.Application.Aggregates.CommonModels.Text;
using BNine.Constants;
using BNine.Domain.Entities.PaidUserServices;

#nullable enable

public class CreditScoreBasicOfferInfoScreenViewModelFactory : ICreditScoreBasicOfferInfoScreenViewModelFactory
{
    private const string BaseContainerUrl =
        "https://b9prodaccount.blob.core.windows.net/static-documents-container/Misc/";
    private const string CheckScorePaidServiceImageFileName = "credit-score-basic-offer-info-screen-logo-v1.png";
    private const string CreditScoreBasicAvailableTextFormat = "MMM d, yyyy";

    public CreditScoreBasicOfferInfoScreenViewModel GetScreenForInactiveService()
    {
        var model = GetCommonFlowModel();

        model.Body.Buttons = new[]
        {
            GetSwitchToPremiumButton(false),
            new ButtonViewModel("PAY $5 & ACTIVATE FOR 14 DAYS", ButtonStyleEnum.Solid)
                .EmitsUserEvent("pay-button", null, null),
        };

        return model;
    }

    public CreditScoreBasicOfferInfoScreenViewModel GetScreenForActiveService(UsedPaidUserService activeService)
    {
        var model = GetCommonFlowModel();

        model.Body.Buttons = new[]
        {
            GetSwitchToPremiumButton(true),
            new ButtonViewModel("GO TO CREDIT SCORE", ButtonStyleEnum.Solid)
                .WithDeeplink(DeepLinkRoutes.CreditScore)
                .EmitsUserEvent("credit-score-button", null, null),
        };

        model.Body.CreditScoreBasicAvailableText = $"Credit Score available until {activeService.ValidTo.ToString(CreditScoreBasicAvailableTextFormat)}";

        return model;
    }

    private CreditScoreBasicOfferInfoScreenViewModel GetCommonFlowModel()
    {
        var model = new CreditScoreBasicOfferInfoScreenViewModel
        {
            Header = new GenericDialogHeaderViewModel
            {
                Title = "Credit Score"
            },
            Body = new CreditScoreBasicOfferInfoScreenBodyViewModel
            {
                Title = "Know Your Credit Score with B9",
                ImageUrl = string.Concat(BaseContainerUrl, CheckScorePaidServiceImageFileName),
                BulletPoints = new[]
                {
                    new BulletPointViewModel("Credit Score").WithKnownType(BulletPointTypeEnum.GreenCheckmarkImage),
                    new BulletPointViewModel("Credit Report").WithKnownType(BulletPointTypeEnum.GreenCheckmarkImage),
                },
                Subtitle = "Knowing your current score plus, getting a report on it can help guide you towards better credit.\nAnd since we use soft pulls your credit score will not be impacted.",
                Footnote = new LinkedText("The B9 Credit Score service is available on the B9 Basic Plan for an additional fee of $5 for 14 days of use and is included for free in the B9 Premium plan."),
            },
        };

        return model;
    }

    private ButtonViewModel GetSwitchToPremiumButton(bool isActive)
    {
        var button = new ButtonViewModel("SWITCH TO PREMIUM", ButtonStyleEnum.Solid)
            .WithDeeplink(DeepLinkRoutes.PremiumTariffPlan).WithColor(ThematicColor.PremiumColor)
            .EmitsUserEvent("premium-button", null, new
            {
                isActive
            });

        return button;
    }
}
