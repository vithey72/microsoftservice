﻿namespace BNine.Application.Aggregates.CreditScore.Models;

using BNine.Application.Aggregates.CommonModels.Buttons;
using BNine.Application.Aggregates.CommonModels.DialogElements;
using BNine.Application.Aggregates.CommonModels.Text;

public class CreditScoreBasicOfferInfoScreenBodyViewModel
{
    public string Title
    {
        get;
        set;
    }

    public string ImageUrl
    {
        get;
        set;
    }

    public BulletPointViewModel[] BulletPoints
    {
        get;
        set;
    }

    public string Subtitle
    {
        get;
        set;
    }

    public LinkedText Footnote
    {
        get;
        set;
    }

    public string CreditScoreBasicAvailableText
    {
        get;
        set;
    }

    public ButtonViewModel[] Buttons
    {
        get;
        set;
    }
}
