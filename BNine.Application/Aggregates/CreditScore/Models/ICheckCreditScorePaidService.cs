﻿namespace BNine.Application.Aggregates.CreditScore.Models;

using BNine.Application.Aggregates.PaidService;

public interface ICheckCreditScorePaidService : IChangingChargePaidUserServicePattern
{
}
