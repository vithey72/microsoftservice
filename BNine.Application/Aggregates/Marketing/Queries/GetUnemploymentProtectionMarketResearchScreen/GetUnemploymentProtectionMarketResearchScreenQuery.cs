﻿namespace BNine.Application.Aggregates.Marketing.Queries.GetUnemploymentProtectionMarketResearchScreen;

using CommonModels.Dialog;
using MediatR;

public record GetUnemploymentProtectionMarketResearchScreenQuery : IRequest<GenericDialogV2>;
