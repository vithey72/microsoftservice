﻿namespace BNine.Application.Aggregates.Marketing.Queries.GetUnemploymentProtectionMarketResearchScreen;

using System.Threading.Tasks;
using Abstractions;
using AutoMapper;
using CommonModels.Buttons;
using CommonModels.Dialog;
using CommonModels.Text;
using Interfaces;
using MediatR;

public class GetUnemploymentProtectionMarketResearchScreenQueryHandler
    : AbstractRequestHandler
    , IRequestHandler<GetUnemploymentProtectionMarketResearchScreenQuery, GenericDialogV2>
{
    public GetUnemploymentProtectionMarketResearchScreenQueryHandler(
        IMediator mediator,
        IBNineDbContext dbContext,
        IMapper mapper,
        ICurrentUserService currentUser
        ) : base(mediator, dbContext, mapper, currentUser)
    {
    }

    public Task<GenericDialogV2> Handle(GetUnemploymentProtectionMarketResearchScreenQuery request, CancellationToken cancellationToken)
    {
        return Task.FromResult(new GenericDialogV2
        {
            Header = new GenericDialogHeaderViewModel
            {
                Title = "Unemployment Protection",
                Subtitle = "To keep your B9 Advance limit safe",
            },
            Body = new GenericDialogRichBodyViewModel
            {
                ImageUrl = "https://b9prodaccount.blob.core.windows.net/static-documents-container/Misc/Marketing/unemployment_protection_screen.png",
                Title = "Would you be interested in our Unemployment Protection Product?",
                Subtitle = new LinkedText(
                    "In case you fall behind on payments it would allow you to lock in your most current B9 Advance " +
                    "for three months and would run you $74.99. It's not available yet but will be soon."),
                Buttons = new[]
                {
                    new ButtonViewModel("I'M INTERESTED", ButtonStyleEnum.Solid)
                        .EmitsUserEvent("interested-button", null, null),
                    new ButtonViewModel("NO THANKS", ButtonStyleEnum.Bordered)
                        .EmitsUserEvent("no-interested-button", null, null),
                },
            }
        });
    }
}
