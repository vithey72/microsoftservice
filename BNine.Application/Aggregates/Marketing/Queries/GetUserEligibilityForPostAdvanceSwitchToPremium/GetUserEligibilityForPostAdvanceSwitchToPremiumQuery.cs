﻿namespace BNine.Application.Aggregates.Marketing.Queries.GetUserEligibilityForPostAdvanceSwitchToPremium;

using BNine.Application.Abstractions;
using MediatR;

public class GetUserEligibilityForPostAdvanceSwitchToPremiumQuery : MBanqSelfServiceUserRequest, IRequest<bool>
{
}
