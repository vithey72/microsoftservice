﻿namespace BNine.Application.Aggregates.Marketing.Queries.GetUserEligibilityForPostAdvanceSwitchToPremium;

using System.Threading.Tasks;
using Abstractions;
using AutoMapper;
using Configuration.Queries.GetConfiguration;
using Constants;
using Domain.Constants;
using Enums;
using Enums.TariffPlan;
using Interfaces;
using Interfaces.Bank.Client;
using MediatR;
using Microsoft.EntityFrameworkCore;

internal class GetUserEligibilityForPostAdvanceSwitchToPremiumQueryHandler
    : AbstractRequestHandler
    , IRequestHandler<GetUserEligibilityForPostAdvanceSwitchToPremiumQuery, bool>
{
    private readonly ITariffPlanService _tariffPlanService;
    private readonly IUserActivitiesCooldownService _cooldownService;
    private readonly IBankLoanAccountsService _loanAccountsService;

    public GetUserEligibilityForPostAdvanceSwitchToPremiumQueryHandler(
        IMediator mediator,
        IBNineDbContext dbContext,
        IMapper mapper,
        ICurrentUserService currentUser,
        ITariffPlanService tariffPlanService,
        IUserActivitiesCooldownService cooldownService,
        IBankLoanAccountsService loanAccountsService
    ) : base(mediator, dbContext, mapper, currentUser)
    {
        _tariffPlanService = tariffPlanService;
        _cooldownService = cooldownService;
        _loanAccountsService = loanAccountsService;
    }

    public async Task<bool> Handle(GetUserEligibilityForPostAdvanceSwitchToPremiumQuery request, CancellationToken cancellationToken)
    {
        var features = await Mediator.Send(
            new GetConfigurationQuery(CurrentUser.UserId!.Value), cancellationToken);

        if (!features.HasFeatureEnabled(FeaturesNames.Features.SwitchToPremiumAfterAdvance))
        {
            return false;
        }

        var userIsOnCooldown = await _cooldownService
            .IsOnCooldown(CurrentUser.UserId!.Value, CooldownActivitiesNames.Marketing.SwitchToPremiumPitch, cancellationToken);

        if (userIsOnCooldown)
        {
            return false;
        }

        var tariffFamily = await _tariffPlanService.GetCurrentTariffPlanFamily(CurrentUser.UserId!.Value, cancellationToken);

        if (tariffFamily != TariffPlanFamily.Advance)
        {
            return false;
        }

        var user = await DbContext.Users
            .Include(u => u.LoanAccounts)
            .SingleAsync(u => u.Id == CurrentUser.UserId, cancellationToken);

        var activeLoanEntity = user.LoanAccounts
            .OrderByDescending(l => l.CreatedAt)
            .FirstOrDefault(l => l.Status == LoanAccountStatus.Active);

        if (activeLoanEntity == null)
        {
            return false;
        }

        var activeLoan = await _loanAccountsService.GetLoan(
            request.MbanqAccessToken,
            request.Ip,
            activeLoanEntity.ExternalId);

        return activeLoan.DisbursedAmount >= 100;
    }
}
