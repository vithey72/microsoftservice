﻿namespace BNine.Application.Aggregates.Marketing.Queries.GetPostAdvanceDisbursementPremiumSwitchOfferQuery;

using MediatR;
using Model;

public record GetPostAdvanceDisbursementPremiumSwitchOfferQuery : IRequest<PostAdvancePremiumSwitchOfferViewModel>;
