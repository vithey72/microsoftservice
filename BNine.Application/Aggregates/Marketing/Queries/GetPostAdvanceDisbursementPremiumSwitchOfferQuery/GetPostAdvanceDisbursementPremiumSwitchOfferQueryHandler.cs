﻿namespace BNine.Application.Aggregates.Marketing.Queries.GetPostAdvanceDisbursementPremiumSwitchOfferQuery;

using System.Threading.Tasks;
using Abstractions;
using AutoMapper;
using CommonModels.Buttons;
using CommonModels.Dialog;
using CommonModels.Text;
using Constants;
using Interfaces;
using MediatR;
using Model;

public class GetPostAdvanceDisbursementPremiumSwitchOfferQueryHandler
    : AbstractRequestHandler
    , IRequestHandler<GetPostAdvanceDisbursementPremiumSwitchOfferQuery, PostAdvancePremiumSwitchOfferViewModel>
{
    public GetPostAdvanceDisbursementPremiumSwitchOfferQueryHandler(
        IMediator mediator,
        IBNineDbContext dbContext,
        IMapper mapper,
        ICurrentUserService currentUser
        )
        : base(mediator, dbContext, mapper, currentUser)
    {
    }

    public async Task<PostAdvancePremiumSwitchOfferViewModel> Handle(GetPostAdvanceDisbursementPremiumSwitchOfferQuery request,
        CancellationToken cancellationToken)
    {
        return new PostAdvancePremiumSwitchOfferViewModel
        {
            Header = new GenericDialogHeaderViewModel { Title = "B9 Advance limit reached", Subtitle = null, },
            Body = new GenericDialogRichBodyViewModel
            {
                Title = "You’ve maxed out your\nB9 Advance limit",
                Subtitle = new LinkedText("Switch to B9 Premium\nfor advance limits up to $500"),
                Buttons = new[]
                {
                    new ButtonViewModel("SWITCH TO PREMIUM", ButtonStyleEnum.Solid)
                        .WithDeeplink(DeepLinkRoutes.PremiumTariffPlan)
                        .EmitsUserEvent("switch-button", null, null),
                    new ButtonViewModel("NO THANKS", ButtonStyleEnum.Bordered)
                        .EmitsUserEvent("skip-button", null, null),
                },
                ImageUrl =
                    "https://b9prodaccount.blob.core.windows.net/static-documents-container/Misc/Marketing/premium_pitch_speedometer.png",
            },
            PremiumPitch = new PostAdvancePremiumSwitchOfferViewModel.PremiumPitchObject(),
        };
    }
}
