﻿namespace BNine.Application.Aggregates.Marketing.Model;

using BNine.Application.Aggregates.CommonModels.Dialog;
using CommonModels.DialogElements;

public class PostAdvancePremiumSwitchOfferViewModel : GenericDialogV2
{
    public PremiumPitchObject PremiumPitch
    {
        get;
        set;
    }

    public class PremiumPitchObject
    {
        public BulletPointViewModel[] BulletPoints
        {
            get;
            set;
        } = {
                new BulletPointViewModel("Credit Report").WithKnownType(BulletPointTypeEnum.CircledCheckmark),
                new BulletPointViewModel("Premium Support").WithKnownType(BulletPointTypeEnum.CircledCheckmark),
                new BulletPointViewModel("Credit Score").WithKnownType(BulletPointTypeEnum.CircledCheckmark),
                new BulletPointViewModel("2 Free ATM Withdrawals").WithKnownType(BulletPointTypeEnum.CircledCheckmark),
            };

        public string GreyText
        {
            get;
            set;
        } = "Free with B9 Premium";
    }
}
