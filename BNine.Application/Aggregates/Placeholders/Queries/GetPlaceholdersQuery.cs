﻿namespace BNine.Application.Aggregates.Placeholders.Queries;

using System;
using MediatR;
using Models;

public record GetPlaceholdersQuery(Guid UserId) : IRequest<PlaceholdersModel>, IRequest<Unit>;
