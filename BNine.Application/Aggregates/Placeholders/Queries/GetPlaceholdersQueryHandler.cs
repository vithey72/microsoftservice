﻿namespace BNine.Application.Aggregates.Placeholders.Queries;

using System.Threading.Tasks;
using Abstractions;
using AutoMapper;
using Interfaces;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Models;

public class GetPlaceholdersQueryHandler
    : AbstractRequestHandler
    , IRequestHandler<GetPlaceholdersQuery, PlaceholdersModel>
{
    public GetPlaceholdersQueryHandler(
        IMediator mediator,
        IBNineDbContext dbContext,
        IMapper mapper,
        ICurrentUserService currentUser
        )
        : base(mediator, dbContext, mapper, currentUser)
    {
    }

    public async Task<PlaceholdersModel> Handle(GetPlaceholdersQuery request, CancellationToken cancellationToken)
    {
        var dictionary = await DbContext
            .UserPlaceholderTexts
            .Where(ph => ph.UserId == request.UserId)
            .Select(
                x => new KeyValuePair<string, string>(x.Key, x.Value))
            .ToArrayAsync(cancellationToken);

        return new PlaceholdersModel
        {
            UserId = request.UserId,
            Placeholders = dictionary,
        };
    }
}
