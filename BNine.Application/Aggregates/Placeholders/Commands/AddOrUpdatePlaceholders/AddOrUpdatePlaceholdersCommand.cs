﻿namespace BNine.Application.Aggregates.Placeholders.Commands.AddOrUpdatePlaceholders;

using MediatR;
using Models;

public record AddOrUpdatePlaceholdersCommand(PlaceholdersModel Model) : IRequest<Unit>;
