﻿namespace BNine.Application.Aggregates.Placeholders.Commands.AddOrUpdatePlaceholders;

using Abstractions;
using AutoMapper;
using Domain.Entities;
using Interfaces;
using MediatR;
using Microsoft.EntityFrameworkCore;

public class AddOrUpdatePlaceholdersCommandHandler
    : AbstractRequestHandler
    , IRequestHandler<AddOrUpdatePlaceholdersCommand, Unit>
{
    public AddOrUpdatePlaceholdersCommandHandler(
        IMediator mediator,
        IBNineDbContext dbContext,
        IMapper mapper,
        ICurrentUserService currentUser
        )
        : base(mediator, dbContext, mapper, currentUser)
    {
    }

    public async Task<Unit> Handle(AddOrUpdatePlaceholdersCommand request, CancellationToken cancellationToken)
    {
        var uniqueEntriesCount = request
            .Model
            .Placeholders
            .Select(x => x.Key)
            .Distinct()
            .Count();

        if (uniqueEntriesCount != request.Model.Placeholders.Length)
        {
            throw new InvalidOperationException("The request contains duplicate keys. Please make sure all keys within request are unique.");
        }

        foreach (var entry in request.Model.Placeholders)
        {
            var existingEntity = await DbContext
                .UserPlaceholderTexts
                .FirstOrDefaultAsync(x =>
                    x.UserId == request.Model.UserId &&
                    x.Key == entry.Key, cancellationToken);

            if (existingEntity != null)
            {
                existingEntity.Value = entry.Value;
                existingEntity.UpdatedAt = DateTime.UtcNow;
            }
            else
            {
                var newEntity = new UserPlaceholderText
                {
                    UserId = request.Model.UserId,
                    Key = entry.Key,
                    Value = entry.Value,
                    CreatedAt = DateTime.UtcNow,
                };
                DbContext.UserPlaceholderTexts.Add(newEntity);
            }
        }

        await DbContext.SaveChangesAsync(cancellationToken);

        return Unit.Value;
    }
}
