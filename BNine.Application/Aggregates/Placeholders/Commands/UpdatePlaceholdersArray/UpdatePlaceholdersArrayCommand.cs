﻿namespace BNine.Application.Aggregates.Placeholders.Commands.UpdatePlaceholdersArray;

using BNine.Application.Aggregates.Placeholders.Models;
using MediatR;

public record UpdatePlaceholdersArrayCommand(PlaceholdersModel[] Models) : IRequest<Unit>;
