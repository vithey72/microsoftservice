﻿namespace BNine.Application.Aggregates.Placeholders.Commands.UpdatePlaceholdersArray;

using AutoMapper;
using BNine.Application.Abstractions;
using BNine.Application.Interfaces;
using BNine.Domain.Entities;
using MediatR;
using Microsoft.EntityFrameworkCore;

public class UpdatePlaceholdersArrayCommandHandler
    : AbstractRequestHandler
    , IRequestHandler<UpdatePlaceholdersArrayCommand, Unit>
{
    public UpdatePlaceholdersArrayCommandHandler(
        IMediator mediator,
        IBNineDbContext dbContext,
        IMapper mapper,
        ICurrentUserService currentUser
        )
        : base(mediator, dbContext, mapper, currentUser)
    {
    }

    public async Task<Unit> Handle(UpdatePlaceholdersArrayCommand request, CancellationToken cancellationToken)
    {
        var uniqueUsersCount = request
            .Models
            .Select(x => x.UserId)
            .Distinct()
            .Count();

        if (uniqueUsersCount != request.Models.Length)
        {
            throw new InvalidOperationException("The request contains duplicate users." +
                                                " Please make sure all users within request are unique.");
        }

        foreach (var model in request.Models)
        {
            var uniqueEntriesCount = model
                .Placeholders
                .Select(x => x.Key)
                .Distinct()
                .Count();

            if (uniqueEntriesCount != model.Placeholders.Length)
            {
                throw new InvalidOperationException($"The request contains duplicate keys for user {model.UserId}." +
                                                    " Please make sure all keys within user object are unique.");
            }
        }

        foreach (var user in request.Models)
        {
            foreach (var entry in user.Placeholders)
            {
                var existingEntity = await DbContext
                    .UserPlaceholderTexts
                    .FirstOrDefaultAsync(x =>
                        x.UserId == user.UserId &&
                        x.Key == entry.Key, cancellationToken);

                if (existingEntity != null)
                {
                    existingEntity.Value = entry.Value;
                    existingEntity.UpdatedAt = DateTime.UtcNow;
                }
                else
                {
                    var newEntity = new UserPlaceholderText
                    {
                        UserId = user.UserId, Key = entry.Key, Value = entry.Value, CreatedAt = DateTime.UtcNow,
                    };
                    DbContext.UserPlaceholderTexts.Add(newEntity);
                }
            }
        }

        await DbContext.SaveChangesAsync(cancellationToken);
        return Unit.Value;
    }
}
