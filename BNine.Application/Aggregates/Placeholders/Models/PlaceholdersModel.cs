﻿namespace BNine.Application.Aggregates.Placeholders.Models;

using System;
using System.Collections.Generic;

public struct PlaceholdersModel
{
    public Guid UserId
    {
        get;
        set;
    }

    public KeyValuePair<string, string>[] Placeholders
    {
        get;
        set;
    }
}
