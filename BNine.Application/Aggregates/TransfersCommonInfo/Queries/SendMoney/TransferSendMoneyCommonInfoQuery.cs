﻿namespace BNine.Application.Aggregates.TransfersCommonInfo.Queries.SendMoney;

using MediatR;
using Models;

public record TransferSendMoneyCommonInfoQuery : IRequest<TransfersCommonInfoViewModel>;

