﻿namespace BNine.Application.Aggregates.TransfersCommonInfo.Queries.SendMoney;

using Abstractions;
using AutoMapper;
using CommonModels.Buttons;
using CommonModels.Dialog;
using Constants;
using Enums;
using ExternalCards.Queries.GetExternalCardFees;
using Helpers;
using Interfaces;
using MediatR;
using Models;

public class TransferSendMoneyCommonInfoQueryHandler : AbstractRequestHandler,
    IRequestHandler<TransferSendMoneyCommonInfoQuery, TransfersCommonInfoViewModel>
{
    private readonly IPartnerProviderService _partnerProviderService;

    public TransferSendMoneyCommonInfoQueryHandler(
        IMediator mediator,
        IBNineDbContext dbContext,
        IMapper mapper,
        ICurrentUserService currentUser,
        IPartnerProviderService partnerProviderService) :
        base(mediator, dbContext, mapper, currentUser)
    {
        _partnerProviderService = partnerProviderService;
    }

    public async Task<TransfersCommonInfoViewModel> Handle(TransferSendMoneyCommonInfoQuery request,
        CancellationToken cancellationToken)
    {
        var body = new GenericDialogButtonWithIconViewModel()
        {
            Title = "Send money",
            ButtonsWithIcon = await GenerateButtonsWithIcons(),
            Buttons = new[]
            {
                new ButtonViewModel("DONE", ButtonStyleEnum.Solid)
            }
        };
        return new TransfersCommonInfoViewModel() { Body = body };
    }

    private async Task<ButtonWithIconViewModel[]> GenerateButtonsWithIcons()
    {
        var partner = _partnerProviderService.GetPartner();
        List<ButtonWithIconViewModel> buttonsGenerated = new List<ButtonWithIconViewModel>
        {
            new ButtonWithIconViewModel("Domestic Transfer (ACH)",
                    "1-3 days. No Fee.",
                    string.Concat("https://b9prodaccount.blob.core.windows.net/static-documents-container/Misc/",
                        DepositSendMoneyDialogsStringProvider.GetSendMoneyDomesticAchIconFileName(partner)))
                .WithDeeplink(DeepLinkRoutes.AchTransfer),
            new ButtonWithIconViewModel($"Send Money to {StringProvider.GetPartnerArticle(_partnerProviderService)} {StringProvider.GetPartnerName(_partnerProviderService)} Member",
                    $"Within minutes. No Fee.",
                    string.Concat("https://b9prodaccount.blob.core.windows.net/static-documents-container/Misc/",
                        DepositSendMoneyDialogsStringProvider.GetSendMoneyInternalIconFileName(partner)))
                .WithDeeplink(DeepLinkRoutes.InternalTransfer)
        };

        if (partner is PartnerApp.Default)
        {
            var sendToAnotherValues = await Mediator.Send(new GetExternalCardFeeQuery());
            buttonsGenerated.Add(new ButtonWithIconViewModel("Send Money to Another Card",
                    $"Within minutes. Fee: {CurrencyFormattingHelper.AsRoundedWithNoTrailingZeroes(sendToAnotherValues.PushToExternalCardMinFeePercent)}% " +
                    $"(minimum ${CurrencyFormattingHelper.AsRoundedWithNoTrailingZeroes(sendToAnotherValues.PushToExternalCardMinFeeAmount)}).   \u26A1",
                    string.Concat("https://b9prodaccount.blob.core.windows.net/static-documents-container/Misc/",
                        DepositSendMoneyDialogsStringProvider.GetSendMoneyToAnotherCardIconFileName(partner)))
                .WithDeeplink(DeepLinkRoutes.PushTransaction));
        }
        if (partner is PartnerApp.Qorbis)
        {
            buttonsGenerated.Add(new ButtonWithIconViewModel("Send Money to Another Card",
                    $"Within minutes. No fee.   \u26A1",
                    string.Concat("https://b9prodaccount.blob.core.windows.net/static-documents-container/Misc/",
                        DepositSendMoneyDialogsStringProvider.GetSendMoneyToAnotherCardIconFileName(partner)))
                .WithDeeplink(DeepLinkRoutes.PushTransaction));
        }
        if (partner is PartnerApp.Booq)
        {
            buttonsGenerated.Add(new ButtonWithIconViewModel("Send Money to Another Card",
                    $"Within minutes. Fee: 1$   \u26A1",
                    string.Concat("https://b9prodaccount.blob.core.windows.net/static-documents-container/Misc/",
                        DepositSendMoneyDialogsStringProvider.GetSendMoneyToAnotherCardIconFileName(partner)))
                .WithDeeplink(DeepLinkRoutes.PushTransaction));
        }

        return buttonsGenerated.ToArray();
    }

}
