﻿namespace BNine.Application.Aggregates.TransfersCommonInfo.Queries.Deposit;

using BNine.Application.Aggregates.TransfersCommonInfo.Models;
using MediatR;

public record TransferDepositCommonInfoQuery : IRequest<TransfersCommonInfoViewModel>;

