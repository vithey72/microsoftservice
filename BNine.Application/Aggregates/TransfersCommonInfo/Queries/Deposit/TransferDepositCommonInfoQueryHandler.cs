﻿namespace BNine.Application.Aggregates.TransfersCommonInfo.Queries.Deposit;

using AutoMapper;
using BNine.Application.Abstractions;
using BNine.Application.Aggregates.TransfersCommonInfo.Models;
using BNine.Application.Interfaces;
using CommonModels.Buttons;
using CommonModels.Dialog;
using Constants;
using Enums;
using ExternalCards.Queries.GetExternalCardFees;
using Helpers;
using MediatR;

public class TransferDepositCommonInfoQueryHandler : AbstractRequestHandler, IRequestHandler<TransferDepositCommonInfoQuery, TransfersCommonInfoViewModel>
{
    private readonly IPartnerProviderService _partnerProviderService;

    public TransferDepositCommonInfoQueryHandler(
        IMediator mediator,
        IBNineDbContext dbContext,
        IMapper mapper,
        ICurrentUserService currentUser,
        IPartnerProviderService partnerProviderService)
        : base(mediator, dbContext, mapper, currentUser)
    {
        _partnerProviderService = partnerProviderService;
    }
    public async Task<TransfersCommonInfoViewModel> Handle(TransferDepositCommonInfoQuery request, CancellationToken cancellationToken)
    {
        var body = new GenericDialogButtonWithIconViewModel()
        {
            Title = "Deposit",
            ButtonsWithIcon = await GenerateButtonsWithIcons(),
            Buttons = new[]
            {
                new ButtonViewModel("DONE", ButtonStyleEnum.Solid)
            }
        };
        return new TransfersCommonInfoViewModel() { Body = body };
    }

    private async Task<ButtonWithIconViewModel[]> GenerateButtonsWithIcons()
    {
        var partner = _partnerProviderService.GetPartner();
        List<ButtonWithIconViewModel> buttonsGenerated = new List<ButtonWithIconViewModel>();

        if (partner == PartnerApp.Default)
        {
            buttonsGenerated.Add(
                new ButtonWithIconViewModel("Switch your Direct Deposit",
                        "As soon as your employer pays you. No Fee.",
                        "https://b9prodaccount.blob.core.windows.net/static-documents-container/Misc/deposit_direct_icon_88_88.png")
                    .WithDeeplink(DeepLinkRoutes.SwitchDeposit));
        }

        if (partner is PartnerApp.Default)
        {
            var addFromOtherValues = await Mediator.Send(new GetExternalCardFeeQuery());
            buttonsGenerated.Add(new ButtonWithIconViewModel("Add Money from Other Cards",
                    $"Within minutes. Fee: {CurrencyFormattingHelper.AsRoundedWithNoTrailingZeroes(addFromOtherValues.PullFromExternalCardMinFeePercent)}%" +
                    $" (minimum ${CurrencyFormattingHelper.AsRoundedWithNoTrailingZeroes(addFromOtherValues.PullFromExternalCardMinFeeAmount)}).    \u26A1",
                    string.Concat("https://b9prodaccount.blob.core.windows.net/static-documents-container/Misc/",
                        DepositSendMoneyDialogsStringProvider.GetDepositFromOtherCardsIconFileName(partner)))
                .WithDeeplink(DeepLinkRoutes.PullTransaction));
        }
        if (partner is PartnerApp.Qorbis)
        {
            buttonsGenerated.Add(new ButtonWithIconViewModel("Add Money from Other Cards",
                    $"Within minutes. No fee.    \u26A1",
                    string.Concat("https://b9prodaccount.blob.core.windows.net/static-documents-container/Misc/",
                        DepositSendMoneyDialogsStringProvider.GetDepositFromOtherCardsIconFileName(partner)))
                .WithDeeplink(DeepLinkRoutes.PullTransaction));
        }
        if (partner is PartnerApp.Booq)
        {
            buttonsGenerated.Add(new ButtonWithIconViewModel("Add Money from Other Cards",
                    $"Within minutes. Fee: 1$    \u26A1",
                    string.Concat("https://b9prodaccount.blob.core.windows.net/static-documents-container/Misc/",
                        DepositSendMoneyDialogsStringProvider.GetDepositFromOtherCardsIconFileName(partner)))
                .WithDeeplink(DeepLinkRoutes.PullTransaction));
        }

        buttonsGenerated.Add(
            new ButtonWithIconViewModel("Add Money from Other Bank Accounts",
                    "1-3 days, depends on your bank. No Fee.",
                    string.Concat("https://b9prodaccount.blob.core.windows.net/static-documents-container/Misc/",
                        DepositSendMoneyDialogsStringProvider.GetDepositWalletInfoIconFileName(partner)))
                .WithDeeplink(DeepLinkRoutes.WalletInfo));

        return buttonsGenerated.ToArray();
    }
}
