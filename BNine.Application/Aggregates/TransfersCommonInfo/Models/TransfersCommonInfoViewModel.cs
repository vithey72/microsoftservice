﻿namespace BNine.Application.Aggregates.TransfersCommonInfo.Models;

using CommonModels.Dialog;

public class TransfersCommonInfoViewModel
{
    public GenericDialogButtonWithIconViewModel Body
    {
        get;
        set;
    }
}
