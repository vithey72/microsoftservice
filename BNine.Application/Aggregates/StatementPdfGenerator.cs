﻿namespace BNine.Application.Aggregates;

using System.Globalization;
using System.Reflection;
using CheckingAccounts.Models;
using Common;
using Constants;
using Enums;
using Enums.Transfers;
using Helpers;
using Interfaces;
using iTextSharp.text;
using iTextSharp.text.pdf;

public static class StatementPdfGenerator
{
    private static readonly string RightHeaderTextLine1 = "Member Services" + Environment.NewLine;

    private static readonly Font DefaultBlackTextFont = FontFactory.GetFont("SF Pro Display", 10, BaseColor.Black);
    private static readonly Font BoldBlackTextFont = FontFactory.GetFont("SF Pro Display", 10, Font.BOLD, BaseColor.Black);
    private static readonly Font DefaultGrayTextFont = FontFactory.GetFont("SF Pro Display", 10, BaseColor.Gray);
    private static readonly Font TitleFont = FontFactory.GetFont("SF Pro Display", 18, Font.BOLD, BaseColor.Black);
    private static readonly Font SubTitleFont = FontFactory.GetFont("SF Pro Display", 14, Font.BOLD, BaseColor.Black);
    private static readonly Font SmallGrayFont = FontFactory.GetFont("SF Pro Text", 8, BaseColor.Gray);
    private static readonly Font LargeBlackFont = FontFactory.GetFont("SF Pro Display", 24, Font.BOLD, BaseColor.Black);

    private static CultureInfo Culture => new("en-US");

    public static byte[] GenerateAccountStatementPdfReport(
        StatementInput input,
        IPartnerProviderService partnerProvider)
    {
        var memoryStream = new MemoryStream();
        var document = new Document(PageSize.A4, 40, 40, 30, 30);
        var writer = PdfWriter.GetInstance(document, memoryStream);

        document.AddTitle("Transactions Report");
        document.Open();

        AppendHeader(document, partnerProvider);
        AppendTitle(input.UserName, input.Address, input.AccountNumber, document);
        AppendDescription(input.StartingDate, input.EndingDate, document);
        AppendSummary(input, document);
        AppendTransactionsTable(input.MainAccountCalculusItems.AccountTransactions, document);
        AppendImportantInformation(document, partnerProvider);

        document.Close();
        writer.Close();
        return memoryStream.ToArray();
    }

    private static void AppendHeader(Document document, IPartnerProviderService partnerProvider)
    {
        var appDirectory = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
        var largeText = partnerProvider.GetPartner() == PartnerApp.Default ? StringProvider.GetPartnerName(partnerProvider) : "";
        var (logoOption, supportEmail, supportPhoneNumber) =
            (
                LogoFileName: StringProvider.GetLogoFileName(partnerProvider),
                SupportEmail: StringProvider.GetSupportEmail(partnerProvider),
                SupportPhoneNumber: StringProvider.GetSupportPhone(partnerProvider) + Environment.NewLine
            );
        var logoPath = $@"{appDirectory}/Images/{logoOption}";

        var logo = Image.GetInstance(logoPath);
        logo.ScaleToFit(100f, 60f);

        var leftHeaderCell = new PdfPCell(logo) { Border = 0, VerticalAlignment = Element.ALIGN_TOP, };

        var middleHeaderCell = new PdfPCell(new Paragraph(largeText, LargeBlackFont))
        {
            PaddingTop = 15f,
            Border = 0,
        };

        var rightHeaderFullText = new Paragraph
        {
            new Phrase(RightHeaderTextLine1, DefaultGrayTextFont),
            new Phrase(supportPhoneNumber, DefaultBlackTextFont),
            new Phrase(supportEmail, DefaultBlackTextFont)
        };

        var rightHeaderCell = new PdfPCell(rightHeaderFullText)
        {
            Border = 0,
            HorizontalAlignment = Element.ALIGN_RIGHT,
            PaddingTop = 15f
        };

        var headerTable = new PdfPTable(3)
        {
            WidthPercentage = 100,
            SpacingAfter = 20f,
        };
        headerTable.SetWidths(new[] { 1f, 7f, 3f });


        headerTable.AddCell(leftHeaderCell);
        headerTable.AddCell(middleHeaderCell);
        headerTable.AddCell(rightHeaderCell);

        document.Add(headerTable);
    }

    private static void AppendTitle(string userName, string address, string accountNumber, Document document)
    {
        var pTitle = new Paragraph(@"Spending account statement", TitleFont)
        {
            SpacingAfter = 15f,
        };
        document.Add(pTitle);

        var clientInfoTable = new PdfPTable(2)
        {
            WidthPercentage = 100,
            SpacingAfter = 20f,
        };
        clientInfoTable.SetWidths(new float[] { 2f, 8f });

        AddCellToTable(clientInfoTable, "Beneficiary", DefaultGrayTextFont);
        AddCellToTable(clientInfoTable, userName, DefaultBlackTextFont);

        AddCellToTable(clientInfoTable, "Address", DefaultGrayTextFont);
        AddCellToTable(clientInfoTable, address, DefaultBlackTextFont);

        AddCellToTable(clientInfoTable, "Account number", DefaultGrayTextFont);
        AddCellToTable(clientInfoTable, accountNumber, DefaultBlackTextFont);

        document.Add(clientInfoTable);
    }

    private static void AppendDescription(DateTime startingDate, DateTime endingDate, Document document)
    {
        var statementPeriodLabel = new Paragraph("Statement Period:", SubTitleFont) { SpacingAfter = 8f };
        document.Add(statementPeriodLabel);

        var startMonth = GetMonthName(startingDate);
        var endMonth = GetMonthName(endingDate);
        var statementPeriodHolder = new Paragraph(
            $"{startMonth} {startingDate.Year} " +
            $"({startMonth} {startingDate.Day}, {startingDate.Year} - {endMonth} {endingDate.Day}, {endingDate.Year})",
            DefaultBlackTextFont)
        {
            SpacingAfter = 30f
        };
        document.Add(statementPeriodHolder);
    }

    private static void AppendSummary(StatementInput input, Document document)
    {
        var summaryLabel = new Paragraph("Summary", SubTitleFont) { SpacingAfter = 15f };
        document.Add(summaryLabel);

        var balanceTable = new PdfPTable(2) { WidthPercentage = 100, SpacingAfter = 20f };
        balanceTable.SetWidths(new[] { 2f, 5f });

        var (mainAccountStartingBalance, mainAccountEndingBalance) =
            GetBalancePeakValues(input.MainAccountCalculusItems.AccountTransactions,
                input.MainAccountCalculusItems.AccountEmptyListCaseBalance);


        AddCellToTable(balanceTable, $"Balance on {GetMonthName(input.StartingDate)} {input.StartingDate.Day}, {input.StartingDate.Year}", DefaultGrayTextFont);
        AddCellToTable(balanceTable, $"{CurrencyFormattingHelper.AsRegular(mainAccountStartingBalance)}", DefaultBlackTextFont);
        AddCellToTable(balanceTable, $"Balance on {GetMonthName(input.EndingDate)} {input.EndingDate.Day}, {input.EndingDate.Year}", DefaultGrayTextFont);
        AddCellToTable(balanceTable, $"{CurrencyFormattingHelper.AsRegular(mainAccountEndingBalance)}", DefaultBlackTextFont);

        if (input.IsRewardAccountPresent)
        {
            var (rewardAccountStartingBalance, rewardAccountEndingBalance) =
                GetBalancePeakValues(input.RewardAccountCalculusItems.AccountTransactions,
                    input.RewardAccountCalculusItems.AccountEmptyListCaseBalance);

            AddCellToTable(balanceTable, $"Reward Account balance on {GetMonthName(input.StartingDate)} {input.StartingDate.Day}, {input.StartingDate.Year}", DefaultGrayTextFont);
            AddCellToTable(balanceTable, $"{CurrencyFormattingHelper.AsRegular(rewardAccountStartingBalance)}", DefaultBlackTextFont);
            AddCellToTable(balanceTable, $"Reward Account balance on {GetMonthName(input.EndingDate)} {input.EndingDate.Day}, {input.EndingDate.Year}", DefaultGrayTextFont);
            AddCellToTable(balanceTable, $"{CurrencyFormattingHelper.AsRegular(rewardAccountEndingBalance)}", DefaultBlackTextFont);
        }

        document.Add(balanceTable);
    }

    private static void AppendTransactionsTable(List<TransactionModel> transactions, Document document)
    {
        var transactionsLabel = new Paragraph("Transactions", SubTitleFont) { SpacingAfter = 15f };
        document.Add(transactionsLabel);

        if (transactions.Count != 0)
        {
            var transactionsTable = new PdfPTable(5) { WidthPercentage = 100, SpacingAfter = 20f };

            AddCellToTable(transactionsTable, "TRANSACTION DATE", SmallGrayFont);
            AddCellToTable(transactionsTable, "POSTING DATE", SmallGrayFont);
            AddCellToTable(transactionsTable, "DESCRIPTION", SmallGrayFont);
            AddCellToTable(transactionsTable, "AMOUNT", SmallGrayFont);
            AddCellToTable(transactionsTable, "BALANCE", SmallGrayFont);

            foreach (var transaction in transactions)
            {
                var descriptionText = InferTransactionDescriptionText(transaction);
                AddCellToTable(transactionsTable, FormatDate(transaction.TransactionDate), DefaultBlackTextFont);
                AddCellToTable(transactionsTable, FormatDate(transaction.PostingDate), DefaultBlackTextFont);
                AddCellToTable(transactionsTable, descriptionText, DefaultBlackTextFont);
                AddCellToTable(transactionsTable, $"{CurrencyFormattingHelper.AsRegularWithSign(transaction.Amount, transaction.Direction)}", BoldBlackTextFont);
                AddCellToTable(transactionsTable, $"{CurrencyFormattingHelper.AsRegular(transaction.RunningBalance)}", DefaultBlackTextFont);
            }

            document.Add(transactionsTable);
        }
        else
        {
            var noTransactionsPlaceholder = new Paragraph("You don't have transactions in this period")
            {
                SpacingAfter = 30f
            };
            document.Add(noTransactionsPlaceholder);
        }

        string FormatDate(DateTime date)
        {
            return TimeZoneHelper.UtcToEasternStandartTime(date).ToString(DateFormat.MonthFirst, Culture);
        }
    }

    private static void AppendImportantInformation(Document document, IPartnerProviderService partnerProvider)
    {
        var pageTitle = new Paragraph(@"IMPORTANT INFORMATION:", SubTitleFont)
        {
            SpacingAfter = 5f
        };
        document.Add(pageTitle);

        var subTitle = new Paragraph(@"Bank Deposit Accounts", SubTitleFont)
        {
            SpacingAfter = 5f
        };
        document.Add(subTitle);

        var howToContact = new Paragraph
        {
            SpacingAfter = 10f
        };
        howToContact.Add(new Chunk(
            "How to contact us", BoldBlackTextFont));
        howToContact.Add(new Chunk(
            " \u2013 You may contact us via email or phone number listed on the front of this statement.",
            DefaultGrayTextFont));
        document.Add(howToContact);

        var encourageToUpdateContacts = new Paragraph(
            "Updating your contact information - We encourage you to keep your contact information up to date. " +
            "This includes address, email, and phone number. If your information has changed, the easiest way to update it is by contacting us via email.",
            DefaultGrayTextFont)
        {
            SpacingAfter = 10f
        };
        document.Add(encourageToUpdateContacts);

        var depositAgreements = new Paragraph(
            "Deposit agreement - When you opened your account, you received a deposit agreement and fee schedule and agreed that your account would be governed " +
            "by the terms of these documents, as we may amend them from time to time. These documents are part of the contract for your deposit account " +
            "and govern all transactions relating to your account, including all deposits and withdrawals. Copies of both the deposit agreement and fee schedule " +
            "which contain the current version of the terms and conditions of your account relationship may be obtained by contacting us.",
            DefaultGrayTextFont)
        {
            SpacingAfter = 10f
        };
        document.Add(depositAgreements);

        var electronicTransfers = new Paragraph(
            "Electronic transfers: In case of errors or questions about your electronic transfers - If you think your statement or receipt is wrong " +
            "or need more information about an electronic transfer on the statement or receipt, email us at the email address listed on the front of this statement " +
            "as soon as you can. We must hear from you no later than 60 days after we sent you the FIRST statement on which the error or problem appeared.",
            DefaultGrayTextFont)
        {
            SpacingAfter = 10f
        };
        document.Add(electronicTransfers);

        var bullet1 = new Paragraph(
            "  • Tell us your name and account number.",
            DefaultGrayTextFont)
        {
            SpacingAfter = 5f
        };
        document.Add(bullet1);

        var bullet2 = new Paragraph(
            "  • Describe the error or transfer you are unsure about and explain as clearly as you can " +
            "why you believe there is an error or why you need more information.",
            DefaultGrayTextFont)
        {
            SpacingAfter = 5f
        };
        document.Add(bullet2);

        var bullet3 = new Paragraph(
            "  • Tell us the dollar amount of the suspected error.",
            DefaultGrayTextFont)
        {
            SpacingAfter = 10f
        };
        document.Add(bullet3);

        var consumerAccounts = new Paragraph(
            "For consumer accounts used primarily for personal, family or household purposes, we will investigate your complaint and will correct any error promptly. " +
            "If we take more than 10 business days to do this, we will provisionally credit your account for the amount you think is in error, so that you will " +
            "have use of the money during the time it will take to complete our investigation.",
            DefaultGrayTextFont)
        {
            SpacingAfter = 10f
        };
        document.Add(consumerAccounts);

        var otherAccounts = new Paragraph(
            "For other accounts, we investigate, and if we find we have made an error, we credit your account at the conclusion of our investigation.",
            DefaultGrayTextFont)
        {
            SpacingAfter = 10f
        };
        document.Add(otherAccounts);

        var otherProblems = new Paragraph(
            "Reporting other problems - You must examine your statement carefully and promptly. You are in the best position to discover errors " +
            "and unauthorized transactions on your account. If you fail to notify us in writing of suspected problems or an unauthorized transaction " +
            "within the time period specified in the deposit agreement (which periods are no more than 60 days after we make the statement available to you), " +
            "we are not liable to you and you agree to not make a claim against us, for the problems or unauthorized transactions.",
            DefaultGrayTextFont)
        {
            SpacingAfter = 10f
        };
        document.Add(otherProblems);
    }

    private static (decimal StartingBalance, decimal Endingbalance) GetBalancePeakValues(
        List<TransactionModel> transactions, decimal emptyListCaseBalance)
    {
        var startingBalance = 0M;
        var endingBalance = 0M;

        if (transactions.Count != 0)
        {
            var earliestTransaction = transactions.Last();
            startingBalance = earliestTransaction.Direction == TransferDirection.Credit
                ? earliestTransaction.RunningBalance - earliestTransaction.Amount
                : earliestTransaction.RunningBalance + earliestTransaction.Amount;

            var latestTransaction = transactions.OrderByDescending(t => t.PostingDate).First();
            endingBalance = latestTransaction.RunningBalance;
        }
        else
        {
            startingBalance = endingBalance = emptyListCaseBalance;
        }

        return (startingBalance, endingBalance);
    }

    private static string GetMonthName(DateTime date) => date.ToString("MMMM", Culture);

    private static void AddCellToTable(PdfPTable table, string text, Font font)
    {
        var cell = new PdfPCell(new Paragraph(text, font))
        {
            BorderWidthLeft = 0,
            BorderWidthRight = 0,
            BorderColorTop = BaseColor.Gray,
            BorderColorBottom = BaseColor.Gray,
            PaddingBottom = 5f,
        };
        table.AddCell(cell);
    }

    private static string InferTransactionDescriptionText(TransactionModel transaction)
    {
        string descriptionText;
        if (transaction.Type == TransactionTypes.INTERNAL)
        {
            const string prefix = "Internal Transfer";
            descriptionText =
                IsRewardTransaction(transaction) ?
                    string.Concat(prefix, " ","(Transfer from Your B9 Reward)") :
                    string.Concat(prefix, " ",
                        transaction.Reference != null ? $"({transaction.Reference})" : "");
        }
        else
        {
            descriptionText = transaction.Type;
        }

        return descriptionText;
    }

    private static bool IsRewardTransaction(TransactionModel transaction)
    {
        var isReward = transaction.Type == TransactionTypes.INTERNAL && transaction.Direction == TransferDirection.Credit &&
               ((transaction.CreditorName != null && transaction.DebtorName != null) &&
                (transaction.CreditorName == transaction.DebtorName));
        return isReward;
    }


    public class StatementInput
    {
        public string UserName { get; init; }
        public string Address { get; init; }
        public string AccountNumber { get; init; }
        public DateTime StartingDate { get; init; }
        public DateTime EndingDate { get; init; }
        public AccountCalculusItems MainAccountCalculusItems { get; init; }
        public AccountCalculusItems RewardAccountCalculusItems { get; init; }
        public bool IsRewardAccountPresent { get; init; }

    }

    public class AccountCalculusItems
    {
        public List<TransactionModel> AccountTransactions { get; init; }
        public decimal AccountEmptyListCaseBalance { get; init; }
    }
}


