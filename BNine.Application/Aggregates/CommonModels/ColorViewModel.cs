﻿namespace BNine.Application.Aggregates.CommonModels;

using AutoMapper;
using BNine.Application.Mappings;

public record ColorViewModel(string ARGB) : IMapFrom<string>
{
    public ColorViewModel() : this("00000000")
    {
    }

    public void Mapping(Profile profile) => profile.CreateMap<string, ColorViewModel>()
        .ForMember(dest => dest.ARGB, opt => opt.MapFrom(s => Parse(s)));

    private string Parse(string input)
    {
        return input;
    }
}
