﻿namespace BNine.Application.Aggregates.CommonModels;

public class OptionViewModelResult<T> where T : class
{
    public static readonly OptionViewModelResult<T> Empty = new();

    private OptionViewModelResult()
    {
    }

    public OptionViewModelResult(T viewModel)
    {
        HasContent = true;
        ViewModel = viewModel;
    }

    public bool HasContent
    {
        get;
    }

    public T ViewModel
    {
        get;
    }
}

