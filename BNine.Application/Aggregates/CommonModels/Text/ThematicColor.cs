﻿namespace BNine.Application.Aggregates.CommonModels.Text;

using System.Runtime.Serialization;

public enum ThematicColor
{
    [EnumMember(Value = "none")]
    None = 0,

    [EnumMember(Value = "brandPrimary")]
    BrandPrimary = 1,

    [EnumMember(Value = "brandAlternative")]
    BrandAlternative = 2,

    [EnumMember(Value = "premiumColor")]
    PremiumColor = 3,
}
