﻿namespace BNine.Application.Aggregates.CommonModels.Text;

using System.Runtime.Serialization;

public enum TypographicalEmphasis
{
    [EnumMember(Value = "none")]
    None = 0,

    [EnumMember(Value = "bold")]
    Bold,

    /// <summary>
    /// Megabold.
    /// </summary>
    [EnumMember(Value = "black")]
    Black,

    [EnumMember(Value = "semiBold")]
    SemiBold,

    [EnumMember(Value = "italic")]
    Italic,

    [EnumMember(Value = "boldItalic")]
    BoldItalic,

    [EnumMember(Value = "underlined")]
    Underlined,

    [EnumMember(Value = "strikeThrough")]
    StrikeThrough,
}
