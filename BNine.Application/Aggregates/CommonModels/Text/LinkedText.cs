﻿namespace BNine.Application.Aggregates.CommonModels.Text;

using DialogElements;

public record LinkedText(string Text, EmbeddedUrlViewModel[] Urls = null);
