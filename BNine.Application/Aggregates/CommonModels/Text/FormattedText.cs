﻿namespace BNine.Application.Aggregates.CommonModels.Text;

public record FormattedText(string Text, FormattedTextFragmentViewModel[] Formats = null);
