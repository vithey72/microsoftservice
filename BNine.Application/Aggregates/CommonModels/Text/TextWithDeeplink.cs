﻿namespace BNine.Application.Aggregates.CommonModels.Text;

using DialogElements;

public record TextWithDeeplink(string Text, DeeplinkEmbeddedUrlViewModel[] Urls = null);

