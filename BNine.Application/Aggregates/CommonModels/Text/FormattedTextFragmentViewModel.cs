﻿namespace BNine.Application.Aggregates.CommonModels.Text;

public record FormattedTextFragmentViewModel(string Placeholder, string Text, TypographicalEmphasis Emphasis = TypographicalEmphasis.None,
    ThematicColor Color = ThematicColor.None, decimal FontSizeMultiplier = 1, ColorViewModel HexColor = null);
