﻿namespace BNine.Application.Aggregates.CommonModels.DialogElements;

using System.Runtime.Serialization;

public enum BulletPointTypeEnum
{
    Unknown = 0,

    [EnumMember(Value = "greenCheckmarkImage")]
    GreenCheckmarkImage = 1,

    [EnumMember(Value = "bulletPoint")]
    BulletPoint = 2,

    [EnumMember(Value = "bookmark")]
    Bookmark = 3,

    [EnumMember(Value = "clock")]
    Clock = 4,

    [EnumMember(Value = "circledCheckmark")]
    CircledCheckmark = 5,

    [EnumMember(Value = "circledWarning")]
    CircledWarning = 6,

    [EnumMember(Value = "emoji")]
    Emoji = 250,

    [EnumMember(Value = "custom")]
    Custom = 255,
}
