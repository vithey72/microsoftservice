﻿namespace BNine.Application.Aggregates.CommonModels.DialogElements;

using Text;

public record struct DeeplinkedBulletPointViewModel(TextWithDeeplink Text, string Header)
{
    public DeeplinkedBulletPointViewModel WithKnownType(BulletPointTypeEnum type) => this with { Type = type, ImageSource = null };

    public DeeplinkedBulletPointViewModel WithCustom(string imageUrl) => this with { Type = BulletPointTypeEnum.Custom, ImageSource = imageUrl };

    public BulletPointTypeEnum Type
    {
        get;
        set;
    } = BulletPointTypeEnum.Unknown;

    public string ImageSource
    {
        get;
        set;
    } = null;
}
