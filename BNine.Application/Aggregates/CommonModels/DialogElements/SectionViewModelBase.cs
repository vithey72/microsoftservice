﻿namespace BNine.Application.Aggregates.CommonModels.DialogElements;

using Newtonsoft.Json;

public abstract class SectionViewModelBase
{
    [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
    public string Title
    {
        get;
        set;
    }

    [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
    public string Subtitle
    {
        get;
        set;
    }
}
