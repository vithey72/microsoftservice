﻿namespace BNine.Application.Aggregates.CommonModels.DialogElements;

public record BulletPointViewModelWithColor(string Text) : BulletPointViewModel(Text)
{
    public string Color
    {
        get;
        set;
    }

    public override BulletPointViewModelWithColor WithKnownType(BulletPointTypeEnum type) =>
        this with { Type = type, ImageSource = null };

    public BulletPointViewModelWithColor WithHexColor(string hexColor) => this with { Color = hexColor };

}
