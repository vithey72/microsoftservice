﻿namespace BNine.Application.Aggregates.CommonModels.DialogElements;

using Newtonsoft.Json;

public class SectionViewModelWithImageUrl : SectionViewModelBase
{
    [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
    public string ImageUrl
    {
        get;
        set;
    }
}
