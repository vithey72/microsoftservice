﻿namespace BNine.Application.Aggregates.CommonModels.DialogElements;

public static class BulletPointElementColor
{
    public static string Black = "#14273A";
    public static string White = "#FFFFFF";
}
