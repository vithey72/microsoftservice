﻿namespace BNine.Application.Aggregates.CommonModels.DialogElements;

public class SectionViewModelWithBulletPoints : SectionViewModelWithImageUrl
{
    public List<BulletPointViewModelWithColor> BulletPoints
    {
        get;
        set;
    }
}
