﻿namespace BNine.Application.Aggregates.CommonModels.DialogElements;

public record class EventTrackingObject(UserEventInstructionsObject UserEventInstructions, string AppsflyerEventName);

public record class UserEventInstructionsObject(string ElementName, object Payload);
