﻿namespace BNine.Application.Aggregates.CommonModels.DialogElements;

public record BulletPointViewModel(string Text)
{
    public virtual BulletPointViewModel WithKnownType(BulletPointTypeEnum type) => this with { Type = type, ImageSource = null };

    public BulletPointViewModel WithEmoji(string emoji) => this with { Type = BulletPointTypeEnum.Emoji, ImageSource = emoji };

    public BulletPointViewModel WithCustom(string imageUrl) => this with { Type = BulletPointTypeEnum.Custom, ImageSource = imageUrl };

    public BulletPointTypeEnum Type
    {
        get;
        set;
    } = BulletPointTypeEnum.Unknown;

    public string ImageSource
    {
        get;
        set;
    } = null;
}
