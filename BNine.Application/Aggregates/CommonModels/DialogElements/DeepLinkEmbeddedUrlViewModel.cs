﻿namespace BNine.Application.Aggregates.CommonModels.DialogElements;

public record struct DeeplinkEmbeddedUrlViewModel(string Placeholder, string Text)
{
    public DeeplinkEmbeddedUrlViewModel WithDeeplink(string deeplink, object arguments = null) => this with
    {
        Deeplink = deeplink,
        DeeplinkObject = new DeeplinkObject(deeplink, arguments),
    };

    [Obsolete("Replaced by DeeplinkObject")]
    public string Deeplink
    {
        get;
        set;
    } = null;

    public DeeplinkObject DeeplinkObject
    {
        get;
        set;
    } = null;
}
