﻿namespace BNine.Application.Aggregates.CommonModels.DialogElements;

/// <summary>
/// Object encapsulating deeplink and its properties.
/// </summary>
/// <param name="Deeplink">Deeplink address.</param>
/// <param name="Arguments">Dictionary of parameters to be passed with deeplink.</param>
public record class DeeplinkObject(string Deeplink, object Arguments);
