﻿namespace BNine.Application.Aggregates.CommonModels.DialogElements;

public class SectionViewModelBaseGeneric<TTitle, TSubtitle>
    where TTitle : class
    where TSubtitle : class
{
    public TTitle Title
    {
        get;
        set;
    }

    public TSubtitle Subtitle
    {
        get;
        set;
    }

    public string ImageUrl
    {
        get;
        set;
    }
}
