﻿namespace BNine.Application.Aggregates.CommonModels.DialogElements;

using Dialog;

public class HintWithDialogViewModel : GenericDialogRichBodyViewModel
{
    public EventTrackingObject EventTracking
    {
        get;
        set;
    }
}
