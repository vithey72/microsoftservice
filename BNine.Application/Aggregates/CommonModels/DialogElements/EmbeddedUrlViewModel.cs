﻿namespace BNine.Application.Aggregates.CommonModels.DialogElements;

using Enums;

public record struct EmbeddedUrlViewModel(string Placeholder, string Text, string Url, LinkFollowMode Type = LinkFollowMode.Default);
