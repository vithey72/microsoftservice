﻿namespace BNine.Application.Aggregates.CommonModels.Static;

using Buttons;
using Constants;
using Dialog;
using Text;

public static class ModalWindows
{
    public static readonly GenericDialogRichBodyViewModel AddExternalCardModalWindow = new()
    {
        Title = "Oops!",
        Subtitle = new LinkedText("Debit card should be added as reserve repayment method"),
        Buttons = new[]
        {
            new ButtonViewModel("Cancel", ButtonStyleEnum.Unknown),
            new ButtonViewModel("Add card", ButtonStyleEnum.Unknown)
                .WithDeeplink(DeepLinkRoutes.PushTransaction)
        }
    };
}
