﻿namespace BNine.Application.Aggregates.CommonModels.Static;

public static class DialogTexts
{
    public static class Headers
    {
        public const string EarlyPaydayTitle = "Early Payday";
        public const string EarlyPaydaySub = "Get paid earlier with B9 Advance";
    }
}
