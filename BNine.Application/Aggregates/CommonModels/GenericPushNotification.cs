﻿namespace BNine.Application.Aggregates.CommonModels;

using Enums;
using Newtonsoft.Json;

public class GenericPushNotification
{

    [JsonProperty("title")]
    public string Title
    {
        get;
        set;
    }

    [JsonProperty("message")]
    public string Message
    {
        get;
        set;
    }

    [JsonProperty("category")]
    public string Category
    {
        get;
        set;
    }

    [JsonProperty("deeplink")]
    public string Deeplink
    {
        get;
        set;
    }

    [JsonProperty("appsflyerEventType")]
    public string AppsflyerEventType
    {
        get;
        set;
    }

    [JsonProperty("messageType")]
    public string MessageType
    {
        get;
        set;
    }

    [JsonProperty("pushType")]
    public NotificationTypeEnum NotificationType
    {
        get;
        set;
    }

    [JsonProperty("params")]
    public Dictionary<string, string> Parameters
    {
        get;
        set;
    }
}
