﻿namespace BNine.Application.Aggregates.CommonModels;

using CreditLines.Models;
using Domain.Entities.PaidUserServices;

public class UsedPaidServiceBusMessage
{
    public UsedPaidServiceBusMessage(UsedPaidUserService paidUserService)
    {
        Id = paidUserService.Id;
        UserId = paidUserService.UserId;
        ValidTo = paidUserService.User.AdvanceStats.ExtensionExpiresAt ?? DateTime.Now;
        CreatedAt = paidUserService.CreatedAt;

        var service = paidUserService.Service;
        if (service == null)
        {
            throw new NullReferenceException("Entity does not reference parent Paid Service");
        }

        Service = new UsedExtensionServiceBusMessage.PaidServiceBusMessage
        {
            Id = service.Id,
            Name = service.Name,
            BusName = service.ServiceBusName,
            DurationInDays = service.DurationInDays,
        };
    }

    public Guid Id
    {
        get;
        set;
    }

    public Guid UserId
    {
        get;
        set;
    }

    public PaidServiceBusMessage Service
    {
        get;
        set;
    }

    public DateTime ValidTo
    {
        get;
        set;
    }

    public string ValidToDate => ValidTo.ToString(BNine.Constants.DateFormat.DwhDateFormat);

    public DateTime CreatedAt
    {
        get;
        set;
    }

    public class PaidServiceBusMessage
    {
        public Guid Id
        {
            get;
            set;
        }

        public string Name
        {
            get;
            set;
        }

        public string BusName
        {
            get;
            set;
        }

        public int DurationInDays
        {
            get;
            set;
        }
    }
}
