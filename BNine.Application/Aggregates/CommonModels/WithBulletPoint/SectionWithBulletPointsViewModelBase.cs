﻿namespace BNine.Application.Aggregates.CommonModels.WithBulletPoint;

using DialogElements;

public abstract class SectionWithBulletPointsViewModelBase
{
    public string Title
    {
        get;
        set;
    }
    public string ImageUrl
    {
        get;
        set;
    }
    public List<BulletPointViewModel> BulletPoints
    {
        get;
        set;
    }
}
