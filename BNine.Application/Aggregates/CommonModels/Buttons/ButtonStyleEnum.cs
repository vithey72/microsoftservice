﻿namespace BNine.Application.Aggregates.CommonModels.Buttons;

using System.Runtime.Serialization;

public enum ButtonStyleEnum
{
    Unknown = 0,

    [EnumMember(Value = "solid")]
    Solid = 1,

    [EnumMember(Value = "bordered")]
    Bordered = 2,

    [EnumMember(Value = "row")]
    Row = 3
}
