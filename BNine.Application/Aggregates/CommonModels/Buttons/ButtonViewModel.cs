﻿namespace BNine.Application.Aggregates.CommonModels.Buttons;

using BNine.Application.Aggregates.CommonModels.Text;
using DialogElements;
using Enums;

public record struct ButtonViewModel(string Text, ButtonStyleEnum Style)
{
    public ButtonViewModel WithUrl(string url, LinkFollowMode type = LinkFollowMode.Default) => this with
    {
        Url = url,
        Type = type,
    };

    public ButtonViewModel WithDeeplink(string deeplink, object arguments = null) => this with
    {
        DeeplinkObject = new DeeplinkObject(deeplink, arguments),
        Url = null,
    };

    public ButtonViewModel EmitsUserEvent(string elementName, string appsflyerEvent, object payload)
    {
        if (elementName != null)
        {
            EventTracking = new EventTrackingObject(
                new UserEventInstructionsObject(elementName, payload), appsflyerEvent);
        }
        else
        {
            EventTracking = new EventTrackingObject(null, appsflyerEvent);
        }

        return this;
    }

    public ButtonViewModel Disable()
    {
        IsDisabled = true;
        return this;
    }

    public ButtonViewModel WithColor(ThematicColor color)
    {
        Color = color;
        return this;
    }

    public string Url
    {
        get;
        set;
    } = null;

    public DeeplinkObject DeeplinkObject
    {
        get;
        set;
    } = null;

    public LinkFollowMode Type
    {
        get;
        set;
    } = LinkFollowMode.Default;

    public EventTrackingObject EventTracking
    {
        get;
        set;
    } = null;

    public bool IsDisabled
    {
        get;
        set;
    } = false;

    public ThematicColor Color
    {
        get;
        set;
    } = ThematicColor.BrandPrimary;
}
