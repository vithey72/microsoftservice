﻿namespace BNine.Application.Aggregates.CommonModels.Buttons;

using DialogElements;

public record ButtonWithIconExtendedViewModel(string Title, string Subtitle, string IconUrl, ButtonStyleEnum Style) :
    ButtonWithIconViewModel(Title, Subtitle, IconUrl)
{
    public override ButtonWithIconExtendedViewModel WithDeeplink(string deeplink, object arguments = null) => this with
    {
        DeeplinkObject = new DeeplinkObject(deeplink, arguments),
    };
}

