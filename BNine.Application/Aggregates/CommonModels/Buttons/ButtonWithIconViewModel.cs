﻿namespace BNine.Application.Aggregates.CommonModels.Buttons;

using DialogElements;

public record ButtonWithIconViewModel(string Title, string Subtitle, string IconUrl)
{
    public DeeplinkObject DeeplinkObject
    {
        get;
        set;
    } = null;

    public EventTrackingObject EventTracking
    {
        get;
        set;
    } = null;
    public virtual ButtonWithIconViewModel WithDeeplink(string deeplink, object arguments = null) => this with
    {
        DeeplinkObject = new DeeplinkObject(deeplink, arguments),
    };

    public ButtonWithIconViewModel EmitsUserEvent(string elementName, string appsflyerEvent, object payload)
    {
        if (elementName != null)
        {
            EventTracking = new EventTrackingObject(
                new UserEventInstructionsObject(elementName, payload), appsflyerEvent);
        }
        else
        {
            EventTracking = new EventTrackingObject(null, appsflyerEvent);
        }

        return this;
    }

}


