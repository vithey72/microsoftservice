﻿namespace BNine.Application.Aggregates.CommonModels.Buttons;

using Dialog;
using DialogElements;
using Enums;

public record ButtonWithRichDialogViewModel
    (string Text, ButtonStyleEnum Style, GenericDialogRichBodyViewModel TooltipDialog)

{
    public ButtonWithRichDialogViewModel WithUrl(string url, LinkFollowMode type = LinkFollowMode.Default) => this with
    {
        Url = url,
        Type = type,
    };

    public ButtonWithRichDialogViewModel WithDeeplink(string deeplink, object arguments = null) => this with
    {
        DeeplinkObject = new DeeplinkObject(deeplink, arguments),
        Url = null,
    };

    public ButtonWithRichDialogViewModel EmitsUserEvent(string elementName, string appsflyerEvent, object payload)
    {
        EventTracking = new EventTrackingObject(
            new UserEventInstructionsObject(elementName, payload), appsflyerEvent);
        return this;
    }
    public string Url
    {
        get;
        set;
    } = null;
    public DeeplinkObject DeeplinkObject
    {
        get;
        set;
    } = null;
    public LinkFollowMode Type
    {
        get;
        set;
    } = LinkFollowMode.Default;
    public EventTrackingObject EventTracking
    {
        get;
        set;
    } = null;

};

