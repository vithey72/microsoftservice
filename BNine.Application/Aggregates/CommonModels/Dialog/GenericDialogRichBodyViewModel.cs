﻿namespace BNine.Application.Aggregates.CommonModels.Dialog;

using Buttons;
using Text;

public class GenericDialogRichBodyViewModel
{
    public string ImageUrl
    {
        get;
        set;
    }

    public string Title
    {
        get;
        set;
    }

    public LinkedText Subtitle
    {
        get;
        set;
    }

    public ButtonViewModel[] Buttons
    {
        get;
        set;
    }

    public bool IsCloseButtonHidden
    {
        get;
        set;
    } = false;
}
