﻿namespace BNine.Application.Aggregates.CommonModels.Dialog;

public class EitherDialog<T1, T2>
    where T1 : GenericSuccessDialog
    where T2 : GenericFailDialog
{
    public EitherDialog(T1 success)
    {
        Success = success;
        Fail = null;
    }

    public EitherDialog(T2 fail)
    {
        Fail = fail;
        Success = null;
    }

    public bool IsSuccess => Success != null;

    public T1 Success
    {
        get;
    }

    public T2 Fail
    {
        get;
    }
}
