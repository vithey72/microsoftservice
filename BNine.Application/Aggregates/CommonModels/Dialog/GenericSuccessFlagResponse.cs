﻿namespace BNine.Application.Aggregates.CommonModels.Dialog;

public record GenericSuccessFlagResponse(bool IsSuccess);
