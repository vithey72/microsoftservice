﻿namespace BNine.Application.Aggregates.CommonModels.Dialog;

public class GenericDialogTwoButtonBodyViewModel : GenericDialogBodyViewModel
{
    public string AlternativeButtonText
    {
        get;
        set;
    }
}
