﻿namespace BNine.Application.Aggregates.CommonModels.Dialog;

public class GenericDialogBodyViewModel
{
    public string Title
    {
        get;
        set;
    }

    public string Subtitle
    {
        get;
        set;
    }

    public string ButtonText
    {
        get;
        set;
    }
}
