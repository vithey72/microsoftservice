﻿namespace BNine.Application.Aggregates.CommonModels.Dialog;

public class GenericDialogHeaderViewModel
{
    public string Title
    {
        get;
        set;
    }

    public string Subtitle
    {
        get;
        set;
    }
}
