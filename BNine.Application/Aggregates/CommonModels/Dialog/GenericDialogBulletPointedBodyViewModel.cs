﻿namespace BNine.Application.Aggregates.CommonModels.Dialog;

using BNine.Application.Aggregates.AgreementDocuments.AgreementScreen.Models;
using Buttons;
using DialogElements;
using Text;

public class GenericDialogBulletPointedBodyViewModel
{
    public string ImageUrl
    {
        get;
        set;
    }

    public string Title
    {
        get;
        set;
    }

    public LinkedText Subtitle
    {
        get;
        set;
    }

    public DeeplinkedBulletPointViewModel[] BulletPoints

    {
        get;
        set;
    }

    public CheckBox ExplicitPayrollDisconnectionConfirmationCheckBox
    {
        get;
        set;
    }

    public ButtonViewModel[] Buttons
    {
        get;
        set;
    }

    public bool IsCloseButtonHidden
    {
        get;
        set;
    } = false;
}
