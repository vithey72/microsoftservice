﻿namespace BNine.Application.Aggregates.CommonModels.Dialog;

using Buttons;

public class GenericDialogButtonWithIconViewModel : GenericDialogRichBodyViewModel
{
    public ButtonWithIconViewModel[] ButtonsWithIcon
    {
        get;
        set;
    }
}
