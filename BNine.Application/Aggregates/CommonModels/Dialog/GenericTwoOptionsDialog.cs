﻿namespace BNine.Application.Aggregates.CommonModels.Dialog;

public class GenericTwoOptionsDialog : GenericDialog
{
    public new GenericDialogTwoButtonBodyViewModel Body
    {
        get;
        set;
    }
}
