﻿namespace BNine.Application.Aggregates.CommonModels.Dialog;

public class GenericDialogWithBulletPoints
{
    public GenericDialogHeaderViewModel Header
    {
        get;
        set;
    }

    public GenericDialogBulletPointedBodyViewModel Body
    {
        get;
        set;
    }
}
