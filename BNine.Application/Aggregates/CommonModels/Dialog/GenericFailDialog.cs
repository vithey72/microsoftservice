﻿namespace BNine.Application.Aggregates.CommonModels.Dialog;

public class GenericFailDialog : GenericDialog
{
    public string ErrorCode
    {
        get;
        set;
    }
}
