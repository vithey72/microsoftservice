﻿namespace BNine.Application.Aggregates.CommonModels.Dialog;

public class GenericDialogWithPicture : GenericDialog
{
    public string PictureUrl
    {
        get;
        set;
    }
}
