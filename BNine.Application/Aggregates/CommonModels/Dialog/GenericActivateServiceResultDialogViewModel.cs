﻿namespace BNine.Application.Aggregates.CommonModels.Dialog;

public class GenericActivateServiceResultDialogViewModel : GenericDialogBasicViewModel
{
    public bool IsEnabled
    {
        get;
        set;
    }
}
