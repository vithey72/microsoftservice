﻿namespace BNine.Application.Aggregates.CommonModels.Dialog;

public class GenericDialog
{
    public GenericDialogHeaderViewModel Header
    {
        get;
        set;
    }

    public GenericDialogBodyViewModel Body
    {
        get;
        set;
    }
}
