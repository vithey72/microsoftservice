﻿namespace BNine.Application.Aggregates.CommonModels.Dialog;

/// <summary>
/// With infinite buttons.
/// </summary>
public class GenericDialogV2
{
    public GenericDialogHeaderViewModel Header
    {
        get;
        set;
    }

    public GenericDialogRichBodyViewModel Body
    {
        get;
        set;
    }
}
