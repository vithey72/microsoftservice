﻿namespace BNine.Application.Aggregates.CommonModels.Dialog;

public class GenericFeatureViewModel
{
    public string Text
    {
        get;
        set;
    }

    public string IconType
    {
        get;
        set;
    }
}
