﻿namespace BNine.Application.Aggregates.CommonModels.Dialog;

public class GenericConfirmationDialogViewModel
{
    public string Header
    {
        get;
        set;
    }

    public GenericFeatureViewModel[] Features
    {
        get;
        set;
    }

    public string ConfirmationButtonText
    {
        get;
        set;
    }

    public string CancelButtonText
    {
        get;
        set;
    }
}
