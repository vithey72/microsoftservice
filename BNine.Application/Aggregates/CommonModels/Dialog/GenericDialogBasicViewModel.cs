﻿namespace BNine.Application.Aggregates.CommonModels.Dialog;

public class GenericDialogBasicViewModel
{
    public string Header
    {
        get;
        set;
    }

    public string Subtitle
    {
        get;
        set;
    }

    public string ButtonText
    {
        get;
        set;
    }
}
