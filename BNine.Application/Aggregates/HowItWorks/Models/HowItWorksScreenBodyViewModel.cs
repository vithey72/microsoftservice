﻿namespace BNine.Application.Aggregates.HowItWorks;

using CommonModels.Buttons;
using CommonModels.DialogElements;

public class HowItWorksScreenBodyViewModel
{
    public string Title
    {
        get;
        set;
    }
    public string ImageUrl
    {
        get;
        set;
    }
    public BulletPointViewModel[] BulletPoints
    {
        get;
        set;
    }
    public ButtonViewModel[] Buttons
    {
        get;
        set;
    }


}
