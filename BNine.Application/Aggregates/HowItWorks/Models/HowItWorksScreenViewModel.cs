﻿namespace BNine.Application.Aggregates.HowItWorks;

using CommonModels.Dialog;

public class HowItWorksScreenViewModel
{
    public GenericDialogHeaderViewModel Header
    {
        get;
        set;
    }

    public HowItWorksScreenBodyViewModel Body
    {
        get;
        set;
    }
}
