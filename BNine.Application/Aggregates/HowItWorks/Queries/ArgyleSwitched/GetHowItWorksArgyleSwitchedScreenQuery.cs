﻿namespace BNine.Application.Aggregates.HowItWorks.Queries.ArgyleSwitched;

using MediatR;

public record GetHowItWorksArgyleSwitchedScreenQuery: IRequest<HowItWorksScreenViewModel>;

