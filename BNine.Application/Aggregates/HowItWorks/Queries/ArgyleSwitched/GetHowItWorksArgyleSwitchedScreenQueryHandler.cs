﻿namespace BNine.Application.Aggregates.HowItWorks.Queries.ArgyleSwitched;

using Abstractions;
using AutoMapper;
using CommonModels.Buttons;
using CommonModels.Dialog;
using CommonModels.DialogElements;
using Constants;
using Interfaces;
using MediatR;

public class GetHowItWorksArgyleSwitchedScreenQueryHandler: AbstractRequestHandler
    , IRequestHandler<GetHowItWorksArgyleSwitchedScreenQuery, HowItWorksScreenViewModel>
{
    public GetHowItWorksArgyleSwitchedScreenQueryHandler(IMediator mediator,
        IBNineDbContext dbContext, IMapper mapper, ICurrentUserService currentUser):
        base(mediator, dbContext, mapper, currentUser)
    {
    }

    public Task<HowItWorksScreenViewModel> Handle(GetHowItWorksArgyleSwitchedScreenQuery request, CancellationToken cancellationToken)
    {
        var blobStorageBase = "https://b9prodaccount.blob.core.windows.net/static-documents-container/Misc/";
        return Task.FromResult(new HowItWorksScreenViewModel()
        {
            Header = new GenericDialogHeaderViewModel()
            {
                Title = "Congrats!"
            },
            Body = new HowItWorksScreenBodyViewModel()
            {
                Title = "Your payroll's now going directly into your B9 account",
                ImageUrl = String.Concat(blobStorageBase, "how_it_works_argyle_main_245_160.png"),
                BulletPoints = new[]
                {
                    new BulletPointViewModel("If we get all the info about your income, your first advance gets unlocked immediately")
                        .WithCustom(String.Concat(blobStorageBase, "how_it_works_connect_icon_40_40.png")),
                    new BulletPointViewModel("If we don't get it, no worries. Just wait for that first paycheck" +
                                             " to be deposited into your B9 account.")
                        .WithCustom(String.Concat(blobStorageBase, "how_it_works_clock_icon_40_40.png")),
                    new BulletPointViewModel("The more you deposit, the higher your Advance Limit will be. Get it up to $500!")
                        .WithCustom(String.Concat(blobStorageBase, "how_it_works_raise_icon_40_40.png")),
                },
                Buttons = new[]
                {
                    new ButtonViewModel("NEXT", ButtonStyleEnum.Solid)
                        .WithDeeplink(DeepLinkRoutes.HowItWorksVirtualCard)
                }

            },

        });
    }
}
