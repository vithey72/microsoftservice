﻿namespace BNine.Application.Aggregates.HowItWorks.Queries.CommonVirtualCard;

using Abstractions;
using AutoMapper;
using CommonModels.Buttons;
using CommonModels.Dialog;
using CommonModels.DialogElements;
using Constants;
using Enums;
using Helpers;
using Interfaces;
using MediatR;

public class GetHowItWorksCommonVirtualCardScreenQueryHandler
    : AbstractRequestHandler
    , IRequestHandler<GetHowItWorksCommonVirtualCardScreenQuery, HowItWorksScreenViewModel>
{
    private readonly IPartnerProviderService _partnerProvider;

    public GetHowItWorksCommonVirtualCardScreenQueryHandler(
        IMediator mediator,
        IBNineDbContext dbContext,
        IMapper mapper,
        ICurrentUserService currentUser,
        IPartnerProviderService partnerProvider
        )
        : base(mediator, dbContext, mapper, currentUser)
    {
        _partnerProvider = partnerProvider;
    }

    public Task<HowItWorksScreenViewModel> Handle(GetHowItWorksCommonVirtualCardScreenQuery request, CancellationToken cancellationToken)
    {
        var partner = _partnerProvider.GetPartner();
        var title = $"Thanks for joining {StringProvider.GetPartnerName(_partnerProvider)}!";
        var blobStorageBase = "https://b9prodaccount.blob.core.windows.net/static-documents-container/Misc/";
        var mainImageUrl = partner switch
        {
            PartnerApp.Eazy => "https://b9prodaccount.blob.core.windows.net/static-documents-container/Misc/HowItWorks/how_it_works_mainimg_eazy.png",
            PartnerApp.Poetryy => "https://b9prodaccount.blob.core.windows.net/static-documents-container/Misc/HowItWorks/how_it_works_mainimg_poetryy.png",
            PartnerApp.USNational => "https://b9prodaccount.blob.core.windows.net/static-documents-container/Misc/HowItWorks/how_it_works_mainimg_natio.png",
            PartnerApp.Qorbis => "https://b9prodaccount.blob.core.windows.net/static-documents-container/Misc/HowItWorks/how_it_works_mainmg_qorbis.png",
            PartnerApp.ManaPacific => "https://b9prodaccount.blob.core.windows.net/static-documents-container/Misc/HowItWorks/how_it_works_mainmg_mana.png",
            PartnerApp.P2P => "https://b9prodaccount.blob.core.windows.net/static-documents-container/Misc/HowItWorks/how_it_works_mainmg_p2p.png",
            PartnerApp.Booq => "https://b9prodaccount.blob.core.windows.net/static-documents-container/Misc/HowItWorks/how_it_works_mainmg_booq.png",
            PartnerApp.MBanqApp => "https://b9devaccount.blob.core.windows.net/static-documents-container/Misc/how_it_works_mbanq_app.png",
            PartnerApp.PayHammer => "https://b9devaccount.blob.core.windows.net/static-documents-container/Misc/how_it_works_mainmg_payhammer.png",

            _ => "https://b9prodaccount.blob.core.windows.net/static-documents-container/Misc/HowItWorks/how_it_works_mainimg_bnine_v2.png",
        };
        var bulletPoints = partner switch
        {
            PartnerApp.Eazy => new[]
            {
                new BulletPointViewModel("Add funds to your Eazy Account and start using it today!")
                    .WithCustom(
                        "https://b9prodaccount.blob.core.windows.net/static-documents-container/Misc/HowItWorks/how_it_works_eazy_bulletpoint1.png"),
                new BulletPointViewModel("Enjoy your Classic plan.")
                    .WithCustom(
                        "https://b9prodaccount.blob.core.windows.net/static-documents-container/Misc/HowItWorks/how_it_works_eazy_bulletpoint2.png"),
            },
            PartnerApp.Poetryy => new[]
            {
                new BulletPointViewModel("Add funds to your PoetrYY Account and start using it today!")
                    .WithCustom(
                        "https://b9prodaccount.blob.core.windows.net/static-documents-container/Misc/HowItWorks/how_it_works_poetryy_bulletpoint1.png"),
                new BulletPointViewModel("Enjoy your Basic plan for $4.99 a month")
                    .WithCustom(
                        "https://b9prodaccount.blob.core.windows.net/static-documents-container/Misc/HowItWorks/how_it_works_poetryy_bulletpoint2.png"),

            },
            PartnerApp.USNational => new[]
            {
                new BulletPointViewModel("Add funds to your USA National Account and start using it today!")
                    .WithCustom(
                        "https://b9prodaccount.blob.core.windows.net/static-documents-container/Misc/HowItWorks/how_it_works_bulletpoint1_natio.png"),
                new BulletPointViewModel("Enjoy your free Basic plan")
                    .WithCustom(
                        "https://b9prodaccount.blob.core.windows.net/static-documents-container/Misc/HowItWorks/how_it_works_bulletpoint2_natio.png"),

            },
            PartnerApp.Qorbis => new[]
            {
                new BulletPointViewModel("Enjoy all QORBIS's benefits from a single app.")
                    .WithCustom(
                        "https://b9prodaccount.blob.core.windows.net/static-documents-container/Misc/HowItWorks/how_it_works_qorbis_bulletpoint1.png"),
                new BulletPointViewModel("Every QORBIS member gets a free digital debit card.")
                    .WithCustom(
                        "https://b9prodaccount.blob.core.windows.net/static-documents-container/Misc/HowItWorks/how_it_works_qorbis_bulletpoint2.png")
            },
            PartnerApp.ManaPacific => new[]
{
                new BulletPointViewModel("Add funds to your Mana Pacific \nAccount and start using it today!")
                    .WithCustom(
                        "https://b9prodaccount.blob.core.windows.net/static-documents-container/Misc/HowItWorks/how_it_works_mana_bulletpoint1.png"),
                new BulletPointViewModel("Enjoy your Basic plan.")
                    .WithCustom(
                        "https://b9prodaccount.blob.core.windows.net/static-documents-container/Misc/HowItWorks/how_it_works_mana_bulletpoint2.png")
            },
            PartnerApp.P2P => new[]
{
                new BulletPointViewModel("Add funds to your P2P Account \nand start using it today!")
                    .WithCustom(
                        "https://b9prodaccount.blob.core.windows.net/static-documents-container/Misc/HowItWorks/how_it_works_p2p_bulletpoint1.png"),
                new BulletPointViewModel("Enjoy your P2P Basic plan \nfor $9.90 a month")
                    .WithCustom(
                        "https://b9prodaccount.blob.core.windows.net/static-documents-container/Misc/HowItWorks/how_it_works_p2p_bulletpoint2.png")
            },
            PartnerApp.Booq => new[]
{
                new BulletPointViewModel("Add funds to your Booq Account and start using it today!")
                    .WithCustom(
                        "https://b9prodaccount.blob.core.windows.net/static-documents-container/Misc/HowItWorks/how_it_works_booq_bulletpoint1.png"),
                new BulletPointViewModel("Enjoy your Booq Basic plan \nfor $5 a month")
                    .WithCustom(
                        "https://b9prodaccount.blob.core.windows.net/static-documents-container/Misc/HowItWorks/how_it_works_booq_bulletpoint2.png")
            },
            PartnerApp.PayHammer => new[]
{
                new BulletPointViewModel("Add funds to your Payhammer\n Account and start using it today!")
                    .WithCustom(
                        "https://b9prodaccount.blob.core.windows.net/static-documents-container/Misc/HowItWorks/how_it_works_payhammer_bulletpoint1.png"),
                new BulletPointViewModel("Enjoy your Basic plan")
                    .WithCustom(
                        "https://b9prodaccount.blob.core.windows.net/static-documents-container/Misc/HowItWorks/how_it_works_payhammer_bulletpoint2.png")
            },

            _ => new[]
            {
                new BulletPointViewModel(
                        "There are no subscription fees for your first 30 days of B9 Basic or even after that, " +
                        "if your direct-deposit more than $5K/month. Deposits from multiple employers count. " +
                        "Same goes for government benefits like SSI.")
                    .WithCustom(string.Concat(blobStorageBase, "HowItWorks/how_it_works_b9_bullet_point_1.png")),
                new BulletPointViewModel(
                        $"Every {StringProvider.GetPartnerName(_partnerProvider)} member gets a virtual debit card with up to 5% cash back. " +
                        $"So now you can cover your monthly membership fee just buying the stuff you need!")
                    .WithCustom(string.Concat(blobStorageBase, "how_it_works_virtual_card_offer_icon_50_49.png")),
            },
        };

        return Task.FromResult(new HowItWorksScreenViewModel
        {
            Header = new GenericDialogHeaderViewModel
            {
                Title = title,
            },
            Body = new HowItWorksScreenBodyViewModel
            {
                ImageUrl = mainImageUrl,
                BulletPoints = bulletPoints,
                Buttons = new[]
                {
                    new ButtonViewModel("FINISH", ButtonStyleEnum.Solid)
                        .WithDeeplink(DeepLinkRoutes.Wallet)
                }
            },
        });
    }
}
