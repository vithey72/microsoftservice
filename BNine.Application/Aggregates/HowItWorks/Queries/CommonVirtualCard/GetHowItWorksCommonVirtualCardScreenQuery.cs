﻿namespace BNine.Application.Aggregates.HowItWorks.Queries.CommonVirtualCard;

using MediatR;

public record GetHowItWorksCommonVirtualCardScreenQuery: IRequest<HowItWorksScreenViewModel>;

