﻿namespace BNine.Application.Aggregates.HowItWorks.Queries.ArgyleNoSwitch;

using Abstractions;
using AutoMapper;
using CommonModels.Buttons;
using CommonModels.Dialog;
using CommonModels.DialogElements;
using Constants;
using Interfaces;
using MediatR;

public class GetHowItWorksArgyleNoSwitchScreenQueryHandler: AbstractRequestHandler
    , IRequestHandler<GetHowItWorksArgyleNoSwitchScreenQuery, HowItWorksScreenViewModel>
{
    public GetHowItWorksArgyleNoSwitchScreenQueryHandler(IMediator mediator, IBNineDbContext dbContext, IMapper mapper, ICurrentUserService currentUser)
        : base(mediator, dbContext, mapper, currentUser)
    {
    }

    public Task<HowItWorksScreenViewModel> Handle(GetHowItWorksArgyleNoSwitchScreenQuery request, CancellationToken cancellationToken)
    {
        var blobStorageBase = "https://b9prodaccount.blob.core.windows.net/static-documents-container/Misc/";
        return Task.FromResult(new HowItWorksScreenViewModel()
        {
            Header = new GenericDialogHeaderViewModel()
            {
                Title = "How Cash Advance Works"
            },
            Body = new HowItWorksScreenBodyViewModel()
            {
                ImageUrl = String.Concat(blobStorageBase, "how_it_works_cash_advance_main_245_160.png"),
                BulletPoints = new[]
                {
                    new BulletPointViewModel("Switch your paycheck, SSI or benefit deposit to your B9 account")
                        .WithCustom(String.Concat(blobStorageBase, "how_it_works_connect_icon_40_40.png")),
                    new BulletPointViewModel("If we get all the info about your income, " +
                                             "your first advance gets unlocked immediately. " +
                                             "If we don't get it, just wait for your first paycheck")
                        .WithCustom(String.Concat(blobStorageBase, "how_it_works_clock_icon_40_40.png")),
                    new BulletPointViewModel("The more you deposit, the higher your Advance Limit will be - " +
                                             "especially with B9 Premium, which allows limits up to $500.")
                        .WithCustom(String.Concat(blobStorageBase, "how_it_works_raise_icon_40_40.png")),
                },
                Buttons = new[]
                {
                    new ButtonViewModel("NEXT", ButtonStyleEnum.Solid)
                        .WithDeeplink(DeepLinkRoutes.HowItWorksVirtualCard)
                }

            },

        });
    }
}
