﻿namespace BNine.Application.Aggregates.HowItWorks.Queries.ArgyleNoSwitch;

using MediatR;

public record GetHowItWorksArgyleNoSwitchScreenQuery : IRequest<HowItWorksScreenViewModel>;

