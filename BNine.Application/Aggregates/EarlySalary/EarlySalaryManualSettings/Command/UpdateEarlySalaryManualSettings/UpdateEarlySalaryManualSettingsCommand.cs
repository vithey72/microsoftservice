﻿namespace BNine.Application.Aggregates.EarlySalary.EarlySalaryManualSettings.Command.UpdateEarlySalaryManualSettings
{
    using AutoMapper;
    using BNine.Application.Mappings;
    using BNine.Domain.Entities.Settings;
    using MediatR;
    using Newtonsoft.Json;

    public class UpdateEarlySalaryManualSettingsCommand : IRequest<Unit>, IMapTo<EarlySalaryManualSettings>
    {
        [JsonProperty("defaultAmountAllocation")]
        public decimal DefaultAmountAllocation
        {
            get; set;
        }

        [JsonProperty("minAmountAllocation")]
        public decimal MinAmountAllocation
        {
            get; set;
        }

        [JsonProperty("maxAmountAllocation")]
        public decimal MaxAmountAllocation
        {
            get; set;
        }

        [JsonProperty("defaultPercentAllocation")]
        public decimal DefaultPercentAllocation
        {
            get; set;
        }

        [JsonProperty("minPercentAllocation")]
        public decimal MinPercentAllocation
        {
            get; set;
        }

        [JsonProperty("maxPercentAllocation")]
        public decimal MaxPercentAllocation
        {
            get; set;
        }

        public void Mapping(Profile profile) => profile.CreateMap<UpdateEarlySalaryManualSettingsCommand, EarlySalaryManualSettings>()
            .ForMember(d => d.DefaultAmountAllocation, opt => opt.MapFrom(s => s.DefaultAmountAllocation))
            .ForMember(d => d.MinAmountAllocation, opt => opt.MapFrom(s => s.MinAmountAllocation))
            .ForMember(d => d.MaxAmountAllocation, opt => opt.MapFrom(s => s.MaxAmountAllocation))
            .ForMember(d => d.DefaultPercentAllocation, opt => opt.MapFrom(s => s.DefaultPercentAllocation))
            .ForMember(d => d.MaxPercentAllocation, opt => opt.MapFrom(s => s.MaxPercentAllocation))
            .ForMember(d => d.MinPercentAllocation, opt => opt.MapFrom(s => s.MinPercentAllocation));
    }
}
