﻿namespace BNine.Application.Aggregates.EarlySalary.EarlySalaryManualSettings.Command.UpdateEarlySalaryManualSettings
{
    using System.Threading;
    using System.Threading.Tasks;
    using AutoMapper;
    using BNine.Application.Abstractions;
    using BNine.Application.Exceptions;
    using BNine.Application.Interfaces;
    using MediatR;
    using Microsoft.EntityFrameworkCore;

    public class UpdateEarlySalaryManualSettingsCommandHandler : AbstractRequestHandler, IRequestHandler<UpdateEarlySalaryManualSettingsCommand>
    {
        public UpdateEarlySalaryManualSettingsCommandHandler(
            IMediator mediator,
            IBNineDbContext dbContext,
            IMapper mapper,
            ICurrentUserService currentUser) : base(mediator, dbContext, mapper, currentUser)
        {
        }

        public async Task<Unit> Handle(UpdateEarlySalaryManualSettingsCommand request, CancellationToken cancellationToken)
        {
            var argyleSettings = await DbContext.EarlySalaryManualSettings.SingleOrDefaultAsync(cancellationToken);
            if (argyleSettings == null)
            {
                throw new NotFoundException("EarlySalaryManualSettings not found");
            }

            Mapper.Map(request, argyleSettings);

            await DbContext.SaveChangesAsync(cancellationToken);

            return Unit.Value;
        }
    }
}
