﻿namespace BNine.Application.Aggregates.EarlySalary.EarlySalaryManualSettings.Queries.GetArgyleSettings
{
    using System.Threading;
    using System.Threading.Tasks;
    using AutoMapper;
    using AutoMapper.QueryableExtensions;
    using BNine.Application.Abstractions;
    using BNine.Application.Exceptions;
    using BNine.Application.Interfaces;
    using MediatR;
    using Microsoft.EntityFrameworkCore;

    public class GetEarlySalaryManualSettingsQueryHandler : AbstractRequestHandler, IRequestHandler<GetEarlySalaryManualSettingsQuery, EarlySalaryManualInfo>
    {
        public GetEarlySalaryManualSettingsQueryHandler(
            IMediator mediator,
            IBNineDbContext dbContext,
            IMapper mapper,
            ICurrentUserService currentUser)
            : base(mediator, dbContext, mapper, currentUser)
        {
        }

        public async Task<EarlySalaryManualInfo> Handle(GetEarlySalaryManualSettingsQuery request, CancellationToken cancellationToken)
        {
            var result = await DbContext.EarlySalaryManualSettings
                .ProjectTo<EarlySalaryManualInfo>(Mapper.ConfigurationProvider)
                .SingleOrDefaultAsync(cancellationToken);

            if (result == null)
            {
                throw new NotFoundException("Argyle settings not found");
            }

            return result;
        }
    }
}
