﻿namespace BNine.Application.Aggregates.EarlySalary.EarlySalaryManualSettings.Queries.GetArgyleSettings
{
    using MediatR;

    public class GetEarlySalaryManualSettingsQuery : IRequest<EarlySalaryManualInfo>
    {
    }
}
