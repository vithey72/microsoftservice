﻿namespace BNine.Application.Aggregates.EarlySalary.EarlySalary.Commands.SendEarlySalaryManualForm
{
    using System.Linq;
    using BNine.Application.Interfaces;
    using FluentValidation;

    public class SendEarlySalaryManualFormCommandValidator
        : AbstractValidator<SendEarlySalaryManualFormCommand>
    {
        public SendEarlySalaryManualFormCommandValidator(IBNineDbContext dbContext)
        {
            var settings = dbContext.EarlySalaryManualSettings.Single();

            RuleFor(x => x.AmountAllocation)
                .NotEmpty()
                .When(x => !x.PercentAllocation.HasValue)
                .Empty()
                .When(x => x.PercentAllocation.HasValue)
                .GreaterThanOrEqualTo(settings.MinAmountAllocation)
                .When(x => x.AmountAllocation.HasValue)
                .LessThanOrEqualTo(settings.MaxAmountAllocation)
                .When(x => x.AmountAllocation.HasValue);

            RuleFor(x => x.PercentAllocation)
                .NotEmpty()
                .When(x => !x.AmountAllocation.HasValue)
                .Empty()
                .When(x => x.AmountAllocation.HasValue)
                .GreaterThanOrEqualTo(settings.MinPercentAllocation)
                .When(x => x.PercentAllocation.HasValue)
                .LessThanOrEqualTo(settings.MaxPercentAllocation)
                .When(x => x.PercentAllocation.HasValue);

            RuleFor(x => x.Email)
                .NotEmpty()
                .EmailAddress();
        }
    }
}
