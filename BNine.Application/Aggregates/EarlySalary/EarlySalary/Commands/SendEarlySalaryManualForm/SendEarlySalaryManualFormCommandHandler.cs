﻿namespace BNine.Application.Aggregates.EarlySalary.EarlySalary.Commands.SendEarlySalaryManualForm
{
    using System.Threading;
    using System.Threading.Tasks;
    using AutoMapper;
    using BNine.Application.Abstractions;
    using BNine.Application.Interfaces;
    using BNine.Domain.Entities.User.EarlySalary;
    using BNine.Domain.Events.EarlySalary;
    using MediatR;

    public class SendEarlySalaryManualFormCommandHandler
        : AbstractRequestHandler, IRequestHandler<SendEarlySalaryManualFormCommand, Unit>
    {
        public SendEarlySalaryManualFormCommandHandler(IMediator mediator, IBNineDbContext dbContext, IMapper mapper, ICurrentUserService currentUser)
            : base(mediator, dbContext, mapper, currentUser)
        {
        }

        public async Task<Unit> Handle(SendEarlySalaryManualFormCommand request, CancellationToken cancellationToken)
        {
            var dalRequest = Mapper.Map<EarlySalaryManualRequest>(request);
            dalRequest.UserId = CurrentUser.UserId.Value;

            dalRequest.DomainEvents.Add(new EarlySalaryManualRequestCreatedEvent(dalRequest));

            DbContext.EarlySalaryManualRequests.Add(dalRequest);

            await DbContext.SaveChangesAsync(cancellationToken);

            return Unit.Value;
        }
    }
}
