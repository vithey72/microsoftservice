﻿namespace BNine.Application.Aggregates.EarlySalary.EarlySalary.Commands.SendEarlySalaryManualForm
{
    using AutoMapper;
    using BNine.Application.Mappings;
    using BNine.Domain.Entities.User.EarlySalary;
    using MediatR;
    using Newtonsoft.Json;

    public class SendEarlySalaryManualFormCommand : IRequest<Unit>, IMapTo<EarlySalaryManualRequest>
    {
        [JsonProperty("amountAllocation")]
        public decimal? AmountAllocation
        {
            get; set;
        }

        [JsonProperty("percentAllocation")]
        public decimal? PercentAllocation
        {
            get; set;
        }

        [JsonProperty("email")]
        public string Email
        {
            get; set;
        }

        [JsonProperty("employerName")]
        public string EmployerName
        {
            get; set;
        }

        [JsonProperty("employerContactPerson")]
        public string EmployerContactPerson
        {
            get; set;
        }

        [JsonProperty("employerEmail")]
        public string EmployerEmail
        {
            get; set;
        }

        public void Mapping(Profile profile) => profile.CreateMap<SendEarlySalaryManualFormCommand, EarlySalaryManualRequest>()
            .ForMember(d => d.AmountAllocation, opt => opt.MapFrom(s => s.AmountAllocation))
            .ForMember(d => d.PercentAllocation, opt => opt.MapFrom(s => s.PercentAllocation))
            .ForMember(d => d.Email, opt => opt.MapFrom(s => s.Email))
            .ForMember(d => d.EmployerEmail, opt => opt.MapFrom(s => s.EmployerEmail))
            .ForMember(d => d.EmployerContactPerson, opt => opt.MapFrom(s => s.EmployerContactPerson))
            .ForMember(d => d.EmployerName, opt => opt.MapFrom(s => s.EmployerName));
    }
}
