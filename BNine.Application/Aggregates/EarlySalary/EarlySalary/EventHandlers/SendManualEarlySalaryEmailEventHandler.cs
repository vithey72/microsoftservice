﻿namespace BNine.Application.Aggregates.EarlySalary.EarlySalary.EventHandlers
{
    using System.Linq;
    using System.Threading;
    using System.Threading.Tasks;
    using BNine.Application.Aggregates.CheckingAccounts.Queries.GetInfo;
    using BNine.Application.Aggregates.Emails.Commands.Documents.DepositEnrolment;
    using BNine.Application.Exceptions;
    using BNine.Application.Interfaces;
    using BNine.Application.Models;
    using BNine.Application.Models.Emails.EmailsWithTemplate;
    using BNine.Domain.Entities.User;
    using BNine.Domain.Events.EarlySalary;
    using Helpers;
    using MediatR;
    using Microsoft.EntityFrameworkCore;

    public class SendManualEarlySalaryEmailEventHandler
        : INotificationHandler<DomainEventNotification<EarlySalaryManualRequestCreatedEvent>>
    {
        private readonly IPartnerProviderService _partnerProviderService;
        private readonly IBNineDbContext _dbContext;
        private readonly ICurrentUserService _currentUser;
        private readonly IMediator _mediator;

        public SendManualEarlySalaryEmailEventHandler(
            IMediator mediator,
            ICurrentUserService currentUser,
            IBNineDbContext dbContext,
            IPartnerProviderService partnerProviderService
            )
        {
            _partnerProviderService = partnerProviderService;
            _dbContext = dbContext;
            _currentUser = currentUser;
            _mediator = mediator;
        }

        public async Task Handle(DomainEventNotification<EarlySalaryManualRequestCreatedEvent> notification, CancellationToken cancellationToken)
        {
            var user = await _dbContext.Users
                .Where(x => x.Id == _currentUser.UserId)
                .Select(x => new { x.Email, x.FirstName })
                .FirstOrDefaultAsync(cancellationToken);

            if (user == null)
            {
                throw new NotFoundException(nameof(User), _currentUser.UserId);
            }

            var checkingAccountInfo = await _mediator.Send(new GetInfoQuery());

            var pdfFile = ManualSwitchPdfGenerator.GenerateManualEarlySalaryPdf(
                $"{checkingAccountInfo.OwnerFirstName} {checkingAccountInfo.OwnerLastName}",
                checkingAccountInfo.AccountNumber,
                checkingAccountInfo.RoutingNumber,
                notification.DomainEvent.EarlySalaryManualRequest.AmountAllocation,
                notification.DomainEvent.EarlySalaryManualRequest.PercentAllocation,
                notification.DomainEvent.EarlySalaryManualRequest.EmployerName,
                _partnerProviderService
                );

            var attachment = new EmailAttachmentData("Generated application (Early salary).pdf", pdfFile);
            var response = await _mediator.Send(new DepositEnrolmentEmailCommand(notification.DomainEvent.EarlySalaryManualRequest.Email, user.FirstName, attachment, true));

            await _dbContext.SaveChangesAsync(cancellationToken);
        }
    }
}
