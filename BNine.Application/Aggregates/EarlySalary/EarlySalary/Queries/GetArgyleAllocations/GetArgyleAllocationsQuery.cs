﻿namespace BNine.Application.Aggregates.EarlySalary.EarlySalary.Queries.GetArgyleAllocations
{
    using System;
    using System.Collections.Generic;
    using Abstractions;
    using MediatR;
    using Users.Models;

    public class GetArgyleAllocationsQuery : MBanqSelfServiceUserRequest, IRequest<IEnumerable<PayAllocationsListItem>>
    {
        public Guid UserId
        {
            get;
        }

        public GetArgyleAllocationsQuery(Guid userId)
        {
            UserId = userId;
        }
    }
}
