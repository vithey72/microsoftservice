﻿namespace BNine.Application.Aggregates.EarlySalary.EarlySalary.Queries.GetArgyleAllocations
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading;
    using System.Threading.Tasks;
    using Abstractions;
    using AutoMapper;
    using Common;
    using Domain.Entities.BankAccount;
    using Exceptions;
    using Interfaces;
    using Interfaces.Argyle;
    using MediatR;
    using Microsoft.Extensions.DependencyInjection;
    using Models.Argyle;
    using Users.Models;

    public class GetArgyleAllocationsQueryHandler
        : AbstractRequestHandler, IRequestHandler<GetArgyleAllocationsQuery, IEnumerable<PayAllocationsListItem>>
    {
        private IPayAllocationsService PayAllocationsService
        {
            get;
        }

        private IServiceProvider ServiceProvider
        {
            get;
        }

        public GetArgyleAllocationsQueryHandler(
            IServiceProvider serviceProvider,
            IPayAllocationsService payAllocationsService, IMediator mediator, IBNineDbContext dbContext, IMapper mapper,
            ICurrentUserService currentUser) : base(mediator, dbContext, mapper, currentUser)
        {
            ServiceProvider = serviceProvider;
            PayAllocationsService = payAllocationsService;
        }

        public async Task<IEnumerable<PayAllocationsListItem>> Handle(GetArgyleAllocationsQuery request,
            CancellationToken cancellationToken)
        {
            var argyleUser = DbContext.EarlySalaryWidgetConfigurations
                .Where(x => x.UserId == request.UserId)
                .Where(x => !x.User.EarlySalaryWidgetConfiguration.DeletedAt.HasValue)
                .Select(x => new { x.WidgetUserId })
                .FirstOrDefault();

            if (argyleUser == null)
            {
                return new List<PayAllocationsListItem>();
            }

            IEnumerable<PayAllocationInfo> payAllocationInfo = null;

            if (payAllocationInfo == null || !payAllocationInfo.Any())
            {
                return new List<PayAllocationsListItem>();
            }

            int? accountExternalId = DbContext.Users
                .Where(x => x.Id == request.UserId)
                .Select(x => x.CurrentAccount.ExternalId)
                .FirstOrDefault();

            if (!accountExternalId.HasValue || accountExternalId.Value == 0)
            {
                throw new NotFoundException(nameof(CurrentAccount), request.UserId);
            }

            string accountNumber;

            if (string.IsNullOrEmpty(request.Ip))
            {
                var service = ServiceProvider
                    .GetRequiredService<Interfaces.Bank.Administration.IBankSavingAccountsService>();
                var accountInfo = await service.GetSavingAccountInfo(accountExternalId.Value);
                accountNumber = accountInfo.AccountNumber;
            }
            else
            {
                var service = ServiceProvider
                    .GetRequiredService<Interfaces.Bank.Client.IBankSavingAccountsService>();
                var accountInfo =
                    await service.GetSavingAccountInfo(accountExternalId.Value, request.MbanqAccessToken, request.Ip);
                accountNumber = accountInfo.AccountNumber;
            }

            var accountNumberLast2Digits = ArgyleRoutingNumberHelper.TakeLastNumbers(accountNumber);

            var filteredPayAllocations = payAllocationInfo
                .Where(x =>
                    (
                        string.IsNullOrEmpty(x.RoutingNumber) ||
                        x.RoutingNumber.EndsWith("68")
                    )
                 && (
                        (x.AccountNumber != null && x.AccountNumber.EndsWith(accountNumberLast2Digits)) ||
                        (x.RoutingNumber != null && x.RoutingNumber.EndsWith("68") && x.AccountNumber == null))
                    )
                    .ToList();

            return filteredPayAllocations
                .Select(x => new PayAllocationsListItem
                {
                    AllocationType = x.AllocationType,
                    AllocationValue = x.AllocationValue,
                    ArgyleUserId = x.ArgyleUserId,
                    Status = x.Status,
                });
        }
    }
}
