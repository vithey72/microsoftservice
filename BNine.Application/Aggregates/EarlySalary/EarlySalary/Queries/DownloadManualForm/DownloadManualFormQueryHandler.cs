﻿namespace BNine.Application.Aggregates.EarlySalary.EarlySalary.Queries.DownloadManualForm;

using System.Text;
using System.Threading.Tasks;
using Abstractions;
using AutoMapper;
using Constants;
using Domain.Entities.User;
using Helpers;
using Interfaces;
using Interfaces.Bank.Administration;
using MediatR;
using Microsoft.EntityFrameworkCore;

public class DownloadManualFormQueryHandler
    : AbstractRequestHandler
        , IRequestHandler<DownloadManualFormQuery, byte[]>
{
    private readonly IBankSavingAccountsService _bankAccountService;
    private readonly IPartnerProviderService _partnerProviderService;

    public DownloadManualFormQueryHandler(
        IMediator mediator,
        IBNineDbContext dbContext,
        IMapper mapper,
        ICurrentUserService currentUser,
        IBankSavingAccountsService accountsService,
        IPartnerProviderService partnerProviderService
    )
        : base(mediator, dbContext, mapper, currentUser)
    {
        _bankAccountService = accountsService;
        _partnerProviderService = partnerProviderService;
    }

    public async Task<byte[]> Handle(DownloadManualFormQuery request, CancellationToken cancellationToken)
    {
        var user = await DbContext.Users
            .Include(u => u.CurrentAccount)
            .FirstOrDefaultAsync(u => u.Id == request.UserId, cancellationToken);

        if (user == null)
        {
            throw new KeyNotFoundException("User is not present in DB.");
        }

        VerifyHashCode(request, user);

        var checkingAccountInfo = await _bankAccountService.GetSavingAccountInfo(user.CurrentAccount.ExternalId);
        var pdfFile = ManualSwitchPdfGenerator.GenerateManualEarlySalaryPdf(
            $"{user.FirstName} {user.LastName}",
            checkingAccountInfo.AccountNumber,
            BankInformation.RoutingNumber,
            null,
            null,
            null,
            _partnerProviderService
            );

        return pdfFile;
    }

    private static void VerifyHashCode(DownloadManualFormQuery request, User user)
    {
        var hashBase = $"{user.Id}{user.ExternalId}{user.ExternalClientId}".ToUpper();
        var hashAlgorithm = new Org.BouncyCastle.Crypto.Digests.Sha3Digest(512);

        var input = Encoding.ASCII.GetBytes(hashBase);

        hashAlgorithm.BlockUpdate(input, 0, input.Length);

        var result = new byte[64]; // 512 / 8 = 64
        hashAlgorithm.DoFinal(result, 0);

        var hashString = BitConverter.ToString(result);
        hashString = hashString.Replace("-", "").ToLowerInvariant();

        if (hashString != request.HashCode)
        {
            throw new UnauthorizedAccessException("Hash provided was invalid.");
        }
    }
}
