﻿namespace BNine.Application.Aggregates.EarlySalary.EarlySalary.Queries.DownloadManualForm;

using MediatR;

public class DownloadManualFormQuery : IRequest<byte[]>
{
    public Guid UserId
    {
        get;
        set;
    }

    public string HashCode
    {
        get;
        set;
    }
}
