﻿namespace BNine.Application.Aggregates.EarlySalary.EarlySalary.Queries.GetPdf
{
    using MediatR;

    public class GetPdfQuery : IRequest<bool>
    {
        public string Beneficiary
        {
            get; set;
        }

        public string AccountNumber
        {
            get; set;
        }

        public string RoutingNumber
        {
            get; set;
        }
    }
}
