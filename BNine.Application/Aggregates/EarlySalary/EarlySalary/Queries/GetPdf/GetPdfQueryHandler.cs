﻿namespace BNine.Application.Aggregates.EarlySalary.EarlySalary.Queries.GetPdf
{
    using System.Linq;
    using System.Threading;
    using System.Threading.Tasks;
    using Abstractions;
    using AutoMapper;
    using BNine.Application.Models.Emails.EmailsWithTemplate;
    using Domain.Entities.User;
    using Emails.Commands.Documents.DepositEnrolment;
    using Exceptions;
    using Helpers;
    using Interfaces;
    using MediatR;
    using Microsoft.EntityFrameworkCore;

    public class GetPdfQueryHandler
        : AbstractRequestHandler, IRequestHandler<GetPdfQuery, bool>
    {
        private readonly IPartnerProviderService _partnerProviderService;

        public GetPdfQueryHandler(
            IMediator mediator,
            IBNineDbContext dbContext,
            IMapper mapper,
            ICurrentUserService currentUser,
            IPartnerProviderService partnerProviderService
            )
            : base(mediator, dbContext, mapper, currentUser)
        {
            _partnerProviderService = partnerProviderService;
        }

        public async Task<bool> Handle(GetPdfQuery request, CancellationToken cancellationToken)
        {
            var user = await DbContext.Users
                .Where(x => x.Id == CurrentUser.UserId)
                .Select(x => new { x.Email, x.FirstName })
                .FirstOrDefaultAsync();

            if (user == null)
            {
                throw new NotFoundException(nameof(User), CurrentUser.UserId);
            }

            var pdfFile = ManualSwitchPdfGenerator.GeneratePdf(
                request.Beneficiary,
                request.AccountNumber,
                request.RoutingNumber,
                _partnerProviderService
                );

            var attachment = new EmailAttachmentData("Generated application (Early salary).pdf", pdfFile);
            var response = await Mediator.Send(new DepositEnrolmentEmailCommand(user.Email, user.FirstName, attachment));

            return response;
        }
    }
}
