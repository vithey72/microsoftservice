﻿namespace BNine.Application.Aggregates.EarlySalary.ArgyleSettings.Queries.GetArgyleSettings
{
    using System.Threading;
    using System.Threading.Tasks;
    using AutoMapper;
    using AutoMapper.QueryableExtensions;
    using BNine.Application.Abstractions;
    using BNine.Application.Exceptions;
    using BNine.Application.Interfaces;
    using MediatR;
    using Microsoft.EntityFrameworkCore;

    public class GetArgyleSettingsQueryHandler : AbstractRequestHandler, IRequestHandler<GetArgyleSettingsQuery, ArgyleSettingsInfo>
    {
        public GetArgyleSettingsQueryHandler(
            IMediator mediator,
            IBNineDbContext dbContext,
            IMapper mapper,
            ICurrentUserService currentUser)
            : base(mediator, dbContext, mapper, currentUser)
        {
        }

        public async Task<ArgyleSettingsInfo> Handle(GetArgyleSettingsQuery request, CancellationToken cancellationToken)
        {
            var result = await DbContext.ArgyleSettings
                .ProjectTo<ArgyleSettingsInfo>(Mapper.ConfigurationProvider)
                .SingleOrDefaultAsync(cancellationToken);

            if (result == null)
            {
                throw new NotFoundException("Argyle settings not found");
            }

            return result;
        }
    }
}
