﻿namespace BNine.Application.Aggregates.EarlySalary.ArgyleSettings.Queries.GetArgyleSettings
{
    using MediatR;

    public class GetArgyleSettingsQuery : IRequest<ArgyleSettingsInfo>
    {
    }
}
