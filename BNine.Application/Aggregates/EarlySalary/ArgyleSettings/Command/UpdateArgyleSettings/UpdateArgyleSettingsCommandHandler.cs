﻿namespace BNine.Application.Aggregates.EarlySalary.ArgyleSettings.Command.UpdateArgyleSettings
{
    using System.Threading;
    using System.Threading.Tasks;
    using AutoMapper;
    using BNine.Application.Abstractions;
    using BNine.Application.Exceptions;
    using BNine.Application.Interfaces;
    using MediatR;
    using Microsoft.EntityFrameworkCore;

    public class UpdateArgyleSettingsCommandHandler : AbstractRequestHandler, IRequestHandler<UpdateArgyleSettingsCommand>
    {
        public UpdateArgyleSettingsCommandHandler(
            IMediator mediator,
            IBNineDbContext dbContext,
            IMapper mapper,
            ICurrentUserService currentUser) : base(mediator, dbContext, mapper, currentUser)
        {
        }

        public async Task<Unit> Handle(UpdateArgyleSettingsCommand request, CancellationToken cancellationToken)
        {
            var argyleSettings = await DbContext.ArgyleSettings.SingleOrDefaultAsync(cancellationToken);
            if (argyleSettings == null)
            {
                throw new NotFoundException("Argyle settings not found");
            }

            Mapper.Map(request, argyleSettings);

            await DbContext.SaveChangesAsync(cancellationToken);

            return Unit.Value;
        }
    }
}
