﻿namespace BNine.Application.Aggregates.EarlySalary.WidgetConfiguration.Models;

using Newtonsoft.Json;

public class ArgyleUserConfiguration : PaycheckSwitchConfigurationBase
{
    [JsonProperty("token")]
    public string Token
    {
        get;
        set;
    }

    [JsonProperty("distributionConfig")]
    public string DistributionConfig
    {
        get;
        set;
    }
}
