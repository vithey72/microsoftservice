﻿namespace BNine.Application.Aggregates.EarlySalary.WidgetConfiguration.Models;

using Newtonsoft.Json;

public class TruvUserConfiguration : PaycheckSwitchConfigurationBase
{
    [JsonProperty("token")]
    public string Token
    {
        get;
        set;
    }
}
