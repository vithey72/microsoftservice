﻿namespace BNine.Application.Aggregates.EarlySalary.WidgetConfiguration.Queries.Get;

using BNine.Application.Aggregates.EarlySalary.WidgetConfiguration.Models;
using MediatR;

public class GetTruvUserConfigurationQuery : IRequest<TruvUserConfiguration>
{
}
