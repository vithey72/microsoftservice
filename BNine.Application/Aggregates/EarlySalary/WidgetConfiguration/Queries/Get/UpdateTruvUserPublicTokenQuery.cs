﻿namespace BNine.Application.Aggregates.EarlySalary.WidgetConfiguration.Queries.Get;

using MediatR;
using Newtonsoft.Json;

public class UpdateTruvUserPublicTokenQuery : IRequest
{
    [JsonProperty("publicToken")]
    public string PublicToken
    {
        get;
        set;
    }
}
