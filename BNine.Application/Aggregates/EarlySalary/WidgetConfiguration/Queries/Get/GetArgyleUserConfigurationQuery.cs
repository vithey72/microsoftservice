﻿namespace BNine.Application.Aggregates.EarlySalary.WidgetConfiguration.Queries.Get;

using MediatR;
using Models;

public class GetArgyleUserConfigurationQuery : IRequest<ArgyleUserConfiguration>
{
}
