﻿namespace BNine.Application.Aggregates.EarlySalary.WidgetConfiguration.Queries.Get
{
    using System.Threading;
    using System.Threading.Tasks;
    using Abstractions;
    using AutoMapper;
    using BNine.Application.Aggregates.PaycheckSwitch;
    using BNine.Application.Extensions;
    using BNine.Domain.Entities.User.UserDetails.Settings;
    using Interfaces;
    using MediatR;
    using Microsoft.EntityFrameworkCore;
    using Models;

    public class GetArgyleUserConfigurationQueryHandler
        : AbstractRequestHandler
        , IRequestHandler<GetArgyleUserConfigurationQuery, ArgyleUserConfiguration>
    {
        private readonly IArgyleFactory _argyleFactory;

        public GetArgyleUserConfigurationQueryHandler(
            IArgyleFactory argyleFactory,
            IMediator mediator,
            IBNineDbContext dbContext,
            IMapper mapper,
            ICurrentUserService currentUser) : base(mediator, dbContext, mapper, currentUser)
        {
            _argyleFactory = argyleFactory;
        }

        public async Task<ArgyleUserConfiguration> Handle(GetArgyleUserConfigurationQuery request, CancellationToken cancellationToken)
        {
            var userId = CurrentUser.UserId();
            await EnsureArgyle(userId, cancellationToken);

            var argyle = _argyleFactory.Create(userId);

            var configuration = await argyle.GetArgyleConfiguration(cancellationToken);

            return configuration;
        }

        private async Task EnsureArgyle(Guid userId, CancellationToken cancellationToken)
        {
            var user = await DbContext.Users.FirstOrDefaultAsync(x => x.Id == userId, cancellationToken);

            if (user.Settings.PaycheckSwitchProvider != PaycheckSwitchProvider.Argyle)
            {
                user.Settings.PaycheckSwitchProvider = PaycheckSwitchProvider.Argyle;

                var widgetConfiguration = await DbContext.EarlySalaryWidgetConfigurations
                    .Where(x => !x.User.EarlySalaryWidgetConfiguration.DeletedAt.HasValue)
                    .Where(x => x.UserId == userId)
                    .FirstOrDefaultAsync(cancellationToken);

                if (widgetConfiguration is not null)
                {
                    DbContext.EarlySalaryWidgetConfigurations.Remove(widgetConfiguration);
                }

                await DbContext.SaveChangesAsync(cancellationToken);
            }
        }
    }
}
