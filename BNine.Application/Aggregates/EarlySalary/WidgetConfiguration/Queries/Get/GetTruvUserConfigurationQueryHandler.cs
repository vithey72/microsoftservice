﻿namespace BNine.Application.Aggregates.EarlySalary.WidgetConfiguration.Queries.Get;

using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using BNine.Application.Abstractions;
using BNine.Application.Aggregates.EarlySalary.WidgetConfiguration.Models;
using BNine.Application.Aggregates.PaycheckSwitch;
using BNine.Application.Extensions;
using BNine.Application.Interfaces;
using MediatR;
public class GetTruvUserConfigurationQueryHandler
    : AbstractRequestHandler
    , IRequestHandler<GetTruvUserConfigurationQuery, TruvUserConfiguration>
{
    private readonly ITruvFactory _truvFactory;

    public GetTruvUserConfigurationQueryHandler(
        IMediator mediator,
        IBNineDbContext dbContext,
        IMapper mapper,
        ICurrentUserService currentUser,
        ITruvFactory truvFactory) : base(mediator, dbContext, mapper, currentUser)
    {
        _truvFactory = truvFactory;
    }

    public async Task<TruvUserConfiguration> Handle(GetTruvUserConfigurationQuery request, CancellationToken cancellationToken)
    {
        var truv = _truvFactory.Create(CurrentUser.UserId());

        var configuration = await truv.GetTruvConfiguration(cancellationToken);

        return configuration;
    }
}
