﻿namespace BNine.Application.Aggregates.EarlySalary.WidgetConfiguration.Queries.Get;

using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using BNine.Application.Abstractions;
using BNine.Application.Aggregates.PaycheckSwitch;
using BNine.Application.Extensions;
using BNine.Application.Interfaces;
using MediatR;

internal class UpdateTruvUserPublicTokenQueryHandler
    : AbstractRequestHandler
    , IRequestHandler<UpdateTruvUserPublicTokenQuery>
{
    private readonly ITruvFactory _truvFactory;

    public UpdateTruvUserPublicTokenQueryHandler(
        IMediator mediator,
        IBNineDbContext dbContext,
        IMapper mapper,
        ICurrentUserService currentUser,
        ITruvFactory truvFactory) : base(mediator, dbContext, mapper, currentUser)
    {
        _truvFactory = truvFactory;
    }

    public async Task<Unit> Handle(UpdateTruvUserPublicTokenQuery request, CancellationToken cancellationToken)
    {
        var truv = _truvFactory.Create(CurrentUser.UserId());

        await truv.UpdatePublicToken(request.PublicToken, cancellationToken);

        return Unit.Value;
    }
}
