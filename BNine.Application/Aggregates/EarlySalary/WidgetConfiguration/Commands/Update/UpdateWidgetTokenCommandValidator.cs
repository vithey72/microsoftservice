﻿namespace BNine.Application.Aggregates.EarlySalary.WidgetConfiguration.Commands.Update
{
    using FluentValidation;

    public class UpdateWidgetTokenCommandValidator : AbstractValidator<UpdateWidgetTokenCommand>
    {
        public UpdateWidgetTokenCommandValidator()
        {
            RuleFor(x => x.Token).NotEmpty();
        }
    }
}
