﻿namespace BNine.Application.Aggregates.EarlySalary.WidgetConfiguration.Commands.Update
{
    using System.Linq;
    using System.Threading;
    using System.Threading.Tasks;
    using Abstractions;
    using AutoMapper;
    using BNine.Application.Aggregates.ServiceBusEvents.Commands.PublishB9Event;
    using BNine.Constants;
    using Domain.Entities.User.EarlySalary;
    using Exceptions;
    using Interfaces;
    using MediatR;
    using Microsoft.EntityFrameworkCore;

    public class UpdateWidgetTokenCommandHandler : AbstractRequestHandler, IRequestHandler<UpdateWidgetTokenCommand>
    {
        public UpdateWidgetTokenCommandHandler(
            IMediator mediator,
            IBNineDbContext dbContext,
            IMapper mapper,
            ICurrentUserService currentUser) : base(mediator, dbContext, mapper, currentUser)
        {
        }

        public async Task<Unit> Handle(UpdateWidgetTokenCommand request, CancellationToken cancellationToken)
        {
            var widgetConfiguration = await DbContext.EarlySalaryWidgetConfigurations
                .Where(x => x.UserId == CurrentUser.UserId)
                .FirstOrDefaultAsync();

            if (widgetConfiguration == null)
            {
                throw new NotFoundException(nameof(EarlySalaryWidgetConfiguration), CurrentUser.UserId);
            }

            widgetConfiguration.Token = request.Token;
            widgetConfiguration.UpdatedAt = System.DateTime.UtcNow;
            await DbContext.SaveChangesAsync(cancellationToken);

            await Mediator.Send(new PublishB9EventCommand(widgetConfiguration, ServiceBusTopics.B9EarlySalaryWidgetConfigurations, ServiceBusEvents.B9Updated));

            return Unit.Value;
        }
    }
}
