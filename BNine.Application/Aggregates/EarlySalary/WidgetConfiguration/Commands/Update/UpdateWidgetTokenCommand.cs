﻿namespace BNine.Application.Aggregates.EarlySalary.WidgetConfiguration.Commands.Update
{
    using MediatR;
    using Newtonsoft.Json;

    public class UpdateWidgetTokenCommand : IRequest
    {
        [JsonProperty("token")]
        public string Token
        {
            get;
            set;
        }
    }
}
