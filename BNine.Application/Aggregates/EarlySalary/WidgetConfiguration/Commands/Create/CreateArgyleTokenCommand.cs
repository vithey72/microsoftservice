﻿namespace BNine.Application.Aggregates.EarlySalary.WidgetConfiguration.Commands.Create
{
    using System;
    using MediatR;
    using Newtonsoft.Json;

    public class CreateArgyleTokenCommand : IRequest
    {
        [JsonProperty("userId")]
        public Guid WidgetUserId
        {
            get;
            set;
        }

        [JsonProperty("token")]
        public string Token
        {
            get;
            set;
        }
    }
}
