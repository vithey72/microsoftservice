﻿namespace BNine.Application.Aggregates.EarlySalary.WidgetConfiguration.Commands.Create
{
    using System;
    using System.Linq;
    using System.Threading;
    using System.Threading.Tasks;
    using Abstractions;
    using AutoMapper;
    using BNine.Application.Aggregates.ServiceBusEvents.Commands.PublishB9Event;
    using BNine.Constants;
    using BNine.Domain.Entities.User.UserDetails.Settings;
    using Domain.Entities.User;
    using Domain.Entities.User.EarlySalary;
    using Exceptions;
    using Interfaces;
    using MediatR;
    using Microsoft.EntityFrameworkCore;

    public class CreateArgyleTokenCommandHandler
        : AbstractRequestHandler
        , IRequestHandler<CreateArgyleTokenCommand>
    {
        public CreateArgyleTokenCommandHandler(
            IMediator mediator,
            IBNineDbContext dbContext,
            IMapper mapper,
            ICurrentUserService currentUser
            ) : base(mediator, dbContext, mapper, currentUser)
        {
        }

        public async Task<Unit> Handle(CreateArgyleTokenCommand request, CancellationToken cancellationToken)
        {
            var user = await DbContext.Users
                .Where(x => x.Id == CurrentUser.UserId)
                .FirstOrDefaultAsync(cancellationToken);

            if (user == null)
            {
                throw new NotFoundException(nameof(User), CurrentUser.UserId);
            }

            var widgetConfiguration = new EarlySalaryWidgetConfiguration
            {
                UserId = CurrentUser.UserId!.Value,
                WidgetUserId = request.WidgetUserId,
                Token = request.Token
            };

            user.EarlySalarySettings = new EarlySalaryUserSettings
            {
                EarlySalarySubscriptionDate = DateTime.UtcNow,
                IsEarlySalaryEnabled = true
            };

            var existingWidgetConfiguration = await DbContext
                .EarlySalaryWidgetConfigurations
                .FirstOrDefaultAsync(wc => wc.UserId == CurrentUser.UserId, cancellationToken);

            if (existingWidgetConfiguration == null)
            {
                await DbContext.EarlySalaryWidgetConfigurations.AddAsync(widgetConfiguration, cancellationToken);
            }
            else
            {
                UpdateEntityProperties(existingWidgetConfiguration, widgetConfiguration);
            }
            await DbContext.SaveChangesAsync(cancellationToken);

            await Mediator.Send(new PublishB9EventCommand(
                widgetConfiguration,
                ServiceBusTopics.B9EarlySalaryWidgetConfigurations,
                ServiceBusEvents.B9Created),
                CancellationToken.None);

            await Mediator.Send(new PublishB9EventCommand(
                      user,
                      ServiceBusTopics.B9User,
                      ServiceBusEvents.B9EarlySalarySettingsCreated),
                CancellationToken.None);

            return Unit.Value;
        }

        private static void UpdateEntityProperties(EarlySalaryWidgetConfiguration existingWidgetConfiguration, EarlySalaryWidgetConfiguration widgetConfiguration)
        {
            var now = DateTime.UtcNow;
            existingWidgetConfiguration.WidgetUserId = widgetConfiguration.WidgetUserId;
            existingWidgetConfiguration.Token = widgetConfiguration.Token;
            existingWidgetConfiguration.CreatedAt = now;
            existingWidgetConfiguration.UpdatedAt = now;
            existingWidgetConfiguration.DeletedAt = null;
        }
    }
}
