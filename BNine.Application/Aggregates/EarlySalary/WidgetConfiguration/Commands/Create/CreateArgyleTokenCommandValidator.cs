﻿namespace BNine.Application.Aggregates.EarlySalary.WidgetConfiguration.Commands.Create
{
    using FluentValidation;

    public class CreateArgyleTokenCommandValidator : AbstractValidator<CreateArgyleTokenCommand>
    {
        public CreateArgyleTokenCommandValidator()
        {
            RuleFor(x => x.WidgetUserId).NotEmpty();
            RuleFor(x => x.Token).NotEmpty();
        }
    }
}
