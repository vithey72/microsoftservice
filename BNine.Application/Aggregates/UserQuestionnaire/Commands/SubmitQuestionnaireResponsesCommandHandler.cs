﻿namespace BNine.Application.Aggregates.UserQuestionnaire.Commands
{
    using System;
    using System.Linq;
    using System.Threading;
    using System.Threading.Tasks;
    using Abstractions;
    using AutoMapper;
    using Domain.Entities.UserQuestionnaire;
    using Exceptions;
    using Interfaces;
    using MediatR;
    using Microsoft.EntityFrameworkCore;

    public class SubmitQuestionnaireResponsesCommandHandler
        : AbstractRequestHandler,
            IRequestHandler<SubmitQuestionnaireResponsesCommand, Unit>
    {
        public SubmitQuestionnaireResponsesCommandHandler(
            IMediator mediator,
            IBNineDbContext dbContext,
            IMapper mapper,
            ICurrentUserService currentUser)
            : base(mediator, dbContext, mapper, currentUser)
        {
        }

        public async Task<Unit> Handle(SubmitQuestionnaireResponsesCommand request, CancellationToken cancellationToken)
        {
            var questionSet =
                await DbContext
                    .QuestionSets
                    .FirstOrDefaultAsync(qs => qs.Name == request.QuestionSet, cancellationToken);
            if (questionSet == null)
            {
                throw new ValidationException("questionSet", $"Unknown question set name: {request.QuestionSet}");
            }

            var submittedQuestionnaire = new SubmittedUserQuestionnaire
            {
                UserId = CurrentUser.UserId!.Value,
                CompletionDate = DateTime.Now,
                QuestionSet = questionSet,
                Responses = request.SubmittedQuestionnaireResponses
                    .Select(sqr => new SubmittedQuestionResponse
                    {
                        QuestionId = sqr.QuestionId,
                        Responses = sqr.ResponseIds
                            .Distinct()
                            .Select(id => new SubmittedQuestionResponseQuestionResponse
                            {
                                QuestionResponseId = id,
                            })
                            .ToList(),
                        OpenQuestionResponse = sqr.OpenResponseText,
                    }).ToList()
            };

            DbContext.CompletedUserQuestionnaires.Add(submittedQuestionnaire);
            await DbContext.SaveChangesAsync(cancellationToken);

            return Unit.Value;
        }
    }
}
