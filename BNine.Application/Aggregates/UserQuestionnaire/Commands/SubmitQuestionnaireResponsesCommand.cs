﻿namespace BNine.Application.Aggregates.UserQuestionnaire.Commands
{
    using System;
    using MediatR;
    using Models;
    using Newtonsoft.Json;

    public class SubmitQuestionnaireResponsesCommand : IRequest<Unit>
    {
        [JsonProperty("questionSet")]
        public string QuestionSet
        {
            get;
            set;
        }

        [JsonProperty("submittedQuestionnaireResponses")]
        public SubmittedQuestionResponseModel[] SubmittedQuestionnaireResponses
        {
            get;
            set;
        } = Array.Empty<SubmittedQuestionResponseModel>();
    }
}
