﻿namespace BNine.Application.Aggregates.UserQuestionnaire.Queries
{
    using MediatR;
    using Models;

    public class GetQuestionsQuery : IRequest<GetQuestionsResponseModel>
    {
        public string QuestionSetName
        {
            get; set;
        }
    }
}
