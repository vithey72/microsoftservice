﻿namespace BNine.Application.Aggregates.UserQuestionnaire.Queries
{
    using System.Linq;
    using System.Threading;
    using System.Threading.Tasks;
    using Abstractions;
    using AutoMapper;
    using CommonModels.Buttons;
    using CommonModels.Dialog;
    using Domain.Entities.UserQuestionnaire;
    using Interfaces;
    using MediatR;
    using Microsoft.EntityFrameworkCore;
    using Models;

    public class GetQuestionsQueryHandler
    : AbstractRequestHandler,
        IRequestHandler<GetQuestionsQuery, GetQuestionsResponseModel>
    {
        public IFeatureConfigurationService FeatureConfigurationService
        {
            get;
        }

        public GetQuestionsQueryHandler(
            IMediator mediator,
            IBNineDbContext dbContext,
            IMapper mapper,
            IFeatureConfigurationService featureConfigurationService,
            ICurrentUserService currentUser)
            : base(mediator, dbContext, mapper, currentUser)
        {
            FeatureConfigurationService = featureConfigurationService;
        }

        public async Task<GetQuestionsResponseModel> Handle(GetQuestionsQuery request, CancellationToken cancellationToken)
        {
            if (string.IsNullOrEmpty(request.QuestionSetName))
            {
                request.QuestionSetName = "Default";
            }

            var questionSet = await DbContext
                .QuestionSets
                .Include(qs => qs.Questions)
                .ThenInclude(qqs => qqs.Question)
                .ThenInclude(q => q.ClosedResponses)
                .FirstOrDefaultAsync(x => x.Name == request.QuestionSetName, cancellationToken);

            if (questionSet == null)
            {
                return new GetQuestionsResponseModel { QuestionSet = request.QuestionSetName };
            }

            var questions = questionSet
                .Questions
                .OrderBy(qqs => qqs.Question.Order)
                .Select(ConvertToQuestionModel)
                .ToArray();

            return new GetQuestionsResponseModel
            {
                Questions = questions,
                Header = new GenericDialogHeaderViewModel
                {
                    Title = questionSet.Header,
                    Subtitle = questionSet.HeaderSubtitle,
                },
                Buttons = BuildQuestionnaireButtons(questionSet),
                QuestionSet = request.QuestionSetName,
            };
        }

        private UserQuestionModel ConvertToQuestionModel(QuestionQuestionSet qqs)
        {
            return new UserQuestionModel
            {
                QuestionId = qqs.Question.Id,
                Type = qqs.Question.QuestionType,
                Title = qqs.Question.Title,
                Tag = qqs.Question.Tag,
                ClosedResponses = qqs.Question
                    .ClosedResponses
                    .OrderBy(cr => cr.Order)
                    .Select(cr => new ClosedQuestionResponse { ResponseId = cr.Id, Text = cr.Text })
                    .ToArray(),
                OpenResponseCharacterLimit = qqs.Question.OpenResponseCharacterLimit,
            };
        }

        private ButtonViewModel[] BuildQuestionnaireButtons(QuestionSet questionSet)
        {
            var buttons = new List<ButtonViewModel>();
            if (!string.IsNullOrEmpty(questionSet.MainButtonText))
            {
                buttons.Add(new ButtonViewModel(questionSet.MainButtonText, ButtonStyleEnum.Solid));
            }
            if (!string.IsNullOrEmpty(questionSet.AltButtonText))
            {
                buttons.Add(new ButtonViewModel(questionSet.MainButtonText, ButtonStyleEnum.Bordered));
            }

            return buttons.ToArray();
        }
    }
}
