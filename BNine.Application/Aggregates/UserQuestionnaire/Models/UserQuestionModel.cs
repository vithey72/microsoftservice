﻿namespace BNine.Application.Aggregates.UserQuestionnaire.Models
{
    using System;
    using Enums;
    using Newtonsoft.Json;

    public class UserQuestionModel
    {
        [JsonProperty("questionId")]
        public Guid QuestionId
        {
            get;
            set;
        }

        [JsonProperty("type")]
        public UserQuestionType Type
        {
            get;
            set;
        }

        [JsonProperty("title")]
        public string Title
        {
            get;
            set;
        }

        [JsonProperty("tag")]
        public string Tag
        {
            get;
            set;
        }

        [JsonProperty("closedResponses")]
        public ClosedQuestionResponse[] ClosedResponses
        {
            get;
            set;
        }

        [JsonProperty("openResponseCharacterLimit")]
        public int OpenResponseCharacterLimit
        {
            get;
            set;
        }
    }
}
