﻿namespace BNine.Application.Aggregates.UserQuestionnaire.Models
{
    using System;
    using CommonModels.Buttons;
    using CommonModels.Dialog;
    using Newtonsoft.Json;

    public class GetQuestionsResponseModel
    {
        [JsonProperty("questionSet")]
        public string QuestionSet
        {
            get;
            set;
        }

        [JsonProperty("header")]
        public GenericDialogHeaderViewModel Header
        {
            get;
            set;
        }

        [JsonProperty("questions")]
        public UserQuestionModel[] Questions
        {
            get;
            set;
        } = Array.Empty<UserQuestionModel>();

        [JsonProperty("buttons")]
        public ButtonViewModel[] Buttons
        {
            get;
            set;
        }
    }
}
