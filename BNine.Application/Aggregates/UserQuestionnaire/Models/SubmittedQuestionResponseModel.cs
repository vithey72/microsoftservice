﻿namespace BNine.Application.Aggregates.UserQuestionnaire.Models
{
    using System;
    using Enums;
    using Newtonsoft.Json;

    public class SubmittedQuestionResponseModel
    {
        [JsonProperty("questionId")]
        public Guid QuestionId
        {
            get;
            set;
        }

        [JsonProperty("questionType")]
        public UserQuestionType QuestionType
        {
            get;
            set;
        }

        [JsonProperty("responseIds")]
        public Guid[] ResponseIds
        {
            get;
            set;
        } = Array.Empty<Guid>();

        [JsonProperty("openResponseText")]
        public string OpenResponseText
        {
            get;
            set;
        }
    }
}
