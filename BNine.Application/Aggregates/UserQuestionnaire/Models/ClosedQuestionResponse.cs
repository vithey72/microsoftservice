﻿namespace BNine.Application.Aggregates.UserQuestionnaire.Models
{
    using System;
    using Newtonsoft.Json;

    public class ClosedQuestionResponse
    {
        [JsonProperty("responseId")]
        public Guid ResponseId
        {
            get;
            set;
        }

        [JsonProperty("text")]
        public string Text
        {
            get;
            set;
        }
    }
}
