﻿namespace BNine.Application.Aggregates.MarketingThirdParty.Queries.GetAprilMarketingInfoScreen;

using Abstractions;
using AutoMapper;
using BNine.Settings;
using CommonModels.Buttons;
using CommonModels.Dialog;
using CommonModels.DialogElements;
using CommonModels.Text;
using Enums;
using Interfaces;
using MediatR;
using Microsoft.Extensions.Options;
using Models;

public class GetAprilMarketingInfoScreenQueryHandler : AbstractRequestHandler
    , IRequestHandler<GetAprilMarketingInfoScreenQuery, MarketingThirdPartyInfoScreenViewModel>
{
    private readonly IBNineDbContext _dbContext;
    private readonly IAprilTaxService _aprilTaxService;
    private readonly AprilTaxServiceSettings _settings;

    public GetAprilMarketingInfoScreenQueryHandler(
        IMediator mediator,
        IBNineDbContext dbContext,
        IMapper mapper,
        ICurrentUserService currentUser,
        IAprilTaxService aprilTaxService,
        IOptions<AprilTaxServiceSettings> options
    ) : base(mediator, dbContext, mapper, currentUser)
    {
        _dbContext = dbContext;
        _aprilTaxService = aprilTaxService;
        _settings = options.Value;
    }

    public async Task<MarketingThirdPartyInfoScreenViewModel> Handle(GetAprilMarketingInfoScreenQuery request, CancellationToken cancellationToken)
    {
        // TODO move all this hardcoded stuff to DB
        // TODO https://bninecom.atlassian.net/browse/B9-4717
        return new MarketingThirdPartyInfoScreenViewModel
        {
            Header = new GenericDialogHeaderViewModel
            {
                Title = "Online TAX refund",
            },
            Body = new MarketingThirdPartyInfoScreenBodyViewModel()
            {
                Title = "B9 Smart Tax Service",
                ImageUrl = "https://b9prodaccount.blob.core.windows.net/static-documents-container/Misc/april_tax.png",
                BulletPoints = new[]
                {
                    new BulletPointViewModel("Quick and easy tax filing online").WithKnownType(BulletPointTypeEnum.GreenCheckmarkImage),
                    new BulletPointViewModel("TAX refund to your B9 Account").WithKnownType(BulletPointTypeEnum.GreenCheckmarkImage),
                    new BulletPointViewModel("Qualified customer support team").WithKnownType(BulletPointTypeEnum.GreenCheckmarkImage),
                },
                Subtitle = "With B9, you can master the tax code without actually learning anything about the tax code",
                Buttons = new[]
                {
                    new ButtonViewModel("NEXT", ButtonStyleEnum.Solid)
                        .WithUrl(await GetRegistrationUrl(cancellationToken), LinkFollowMode.BrowserPreview)
                        .EmitsUserEvent("button", null, null),
                },
                Footnote = new LinkedText(
                    "The service is provided by our partner, April. When you click the NEXT button, you will be redirected to a secured page for completion of the necessary steps.\n" +
                    "The cost of the service is $20, payment is made to April after filling out your tax return."),
            },
        };
    }
    private async Task<string> GetRegistrationUrl(CancellationToken cancellationToken)
    {
        var user = _dbContext.Users.Single(x => x.Id == CurrentUser.UserId);

        var authToken = await _aprilTaxService.GetAccessToken(user.Id, user.Email, cancellationToken);

        var registrationHash = await _aprilTaxService.GetLoginUrl(authToken, cancellationToken);

        return _settings.ApplicationUrl + "?userRegistrationCode=" + registrationHash;
    }
}
