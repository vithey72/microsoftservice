﻿namespace BNine.Application.Aggregates.MarketingThirdParty.Queries.GetQuickBooksInfoScreen;

using MediatR;
using Models;

public record GetQuickBooksInfoScreenQuery : IRequest<MarketingThirdPartyInfoScreenViewModel>;
