﻿namespace BNine.Application.Aggregates.MarketingThirdParty.Queries.GetQuickBooksInfoScreen;

using Abstractions;
using AutoMapper;
using CommonModels.Buttons;
using CommonModels.Dialog;
using CommonModels.DialogElements;
using Interfaces;
using MediatR;
using Models;

public class GetQuickBooksInfoScreenQueryHandler: AbstractRequestHandler
    , IRequestHandler<GetQuickBooksInfoScreenQuery, MarketingThirdPartyInfoScreenViewModel>
{
    public GetQuickBooksInfoScreenQueryHandler(IMediator mediator, IBNineDbContext dbContext, IMapper mapper, ICurrentUserService currentUser) :
        base(mediator, dbContext, mapper, currentUser)
    {
    }

    public Task<MarketingThirdPartyInfoScreenViewModel> Handle(GetQuickBooksInfoScreenQuery request, CancellationToken cancellationToken)
    {
        // TODO move all this hardcoded stuff to DB
        // TODO https://bninecom.atlassian.net/browse/B9-4717
        var blobStorageBase = "https://b9prodaccount.blob.core.windows.net/static-documents-container/Misc/";
        return Task.FromResult(new MarketingThirdPartyInfoScreenViewModel()
        {
            Header = new GenericDialogHeaderViewModel()
            {
                Title = "Quickbooks"
            },
            Body = new MarketingThirdPartyInfoScreenBodyViewModel()
            {
                Title = "How-to guide",
                ImageUrl = String.Concat(blobStorageBase, "quickbooks_info_screen.png"),
                BulletPoints = new[]
                {
                    new BulletPointViewModel("Download the QuickBooks Payroll Direct Deposit Form")
                        .WithCustom(String.Concat(blobStorageBase, "quickbooks_download_icon.png")),
                    new BulletPointViewModel("Fill out the main account information\n(You'll need a B9 account)")
                        .WithCustom(String.Concat(blobStorageBase, "quickbooks_fillin_icon.png")),
                    new BulletPointViewModel("Next, fill out the company name in the top field, employee ID, full name, and the date you filled out the QuickBooks Direct Deposit form")
                        .WithCustom(String.Concat(blobStorageBase, "quickbooks_fillin_icon.png")),
                    new BulletPointViewModel("Print the document")
                        .WithCustom(String.Concat(blobStorageBase, "quickbooks_print_icon.png")),
                    new BulletPointViewModel("Sign and give it to your employer or payroll supervisor for processing or to the company Human Resources (HR) department")
                        .WithCustom(String.Concat(blobStorageBase, "quickbooks_sign_icon.png")),
                },
                Buttons = new[]
                {
                    new ButtonViewModel("GET DIRECT DEPOSIT FORM", ButtonStyleEnum.Solid)
                        .WithUrl("https://b9prodaccount.blob.core.windows.net/static-documents-container/QuickBooks/IntuitQuickBooksPayroll.pdf")
                        .EmitsUserEvent("get-form", null, null),
                }

            },

        });
    }
}
