﻿namespace BNine.Application.Aggregates.MarketingThirdParty.Models;

using BNine.Application.Aggregates.CommonModels.Dialog;

public class MarketingThirdPartyInfoScreenViewModel
{
    public GenericDialogHeaderViewModel Header
    {
        get;
        set;
    }

    public MarketingThirdPartyInfoScreenBodyViewModel Body
    {
        get;
        set;
    }
}
