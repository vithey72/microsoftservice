﻿namespace BNine.Application.Aggregates.MarketingThirdParty.Models;

using CommonModels.Buttons;
using CommonModels.DialogElements;
using CommonModels.Text;

public class MarketingThirdPartyInfoScreenBodyViewModel
{
    public string Title
    {
        get;
        set;
    }

    public string ImageUrl
    {
        get;
        set;
    }

    public BulletPointViewModel[] BulletPoints
    {
        get;
        set;
    }

    public string Subtitle
    {
        get;
        set;
    }

    public LinkedText Footnote
    {
        get;
        set;
    }

    public ButtonViewModel[] Buttons
    {
        get;
        set;
    }
}
