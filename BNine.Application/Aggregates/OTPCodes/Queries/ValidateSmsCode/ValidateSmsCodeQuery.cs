﻿namespace BNine.Application.Aggregates.OTPCodes.Queries.ValidateSmsCode
{
    using BNine.Application.Aggregates.OTPCodes.Models;
    using BNine.Enums;
    using MediatR;
    using Newtonsoft.Json;

    public class ValidateSmsCodeQuery : IRequest<Code>
    {
        public ValidateSmsCodeQuery(string phone, string code, OTPAction action)
        {
            Phone = phone;
            Code = code;
            Action = action;
        }

        [JsonProperty("phone")]
        public string Phone
        {
            get; set;
        }

        [JsonProperty("code")]
        public string Code
        {
            get; set;
        }

        [JsonIgnore]
        public OTPAction Action
        {
            get; set;
        }
    }
}
