﻿namespace BNine.Application.Aggregates.OTPCodes.Queries.ValidateSmsCode
{
    using System;
    using System.Linq;
    using System.Threading;
    using System.Threading.Tasks;
    using AutoMapper;
    using BNine.Application.Abstractions;
    using BNine.Application.Aggregates.Configuration.Queries.GetConfiguration;
    using BNine.Application.Aggregates.OTPCodes.Models;
    using BNine.Application.Interfaces;
    using BNine.Constants;
    using MediatR;
    using Microsoft.EntityFrameworkCore;

    public class ValidateSmsCodeQueryHandler
        : AbstractRequestHandler, IRequestHandler<ValidateSmsCodeQuery, Code>
    {
        public ValidateSmsCodeQueryHandler(
            IMediator mediator,
            IBNineDbContext dbContext,
            IMapper mapper,
            ICurrentUserService currentUser)
            : base(mediator, dbContext, mapper, currentUser)
        {
        }

        public async Task<Code> Handle(ValidateSmsCodeQuery request, CancellationToken cancellationToken)
        {
            var isValid = DbContext.OTPs
                .Where(x => x.PhoneNumber == request.Phone)
                .Where(x => x.IsValid)
                .Where(x => x.Action == request.Action)
                .Where(x => x.Date.AddHours(1) > DateTime.UtcNow)
                .Any(x => x.Value == request.Code);

            if (!isValid && request.Code == "555555")
            {
                var userId = (await DbContext.Users
                    .Where(x => x.Phone == request.Phone)
                    .FirstOrDefaultAsync(cancellationToken))?.Id;

                var config = await Mediator.Send(
                    userId.HasValue ? new GetConfigurationQuery(userId.Value) : new GetConfigurationQuery(),
                    cancellationToken);
                if (config.HasFeatureEnabled(FeaturesNames.Features.SmsVerificationDevelopmentMode))
                {
                    isValid = true;
                }
            }

            return new Code
            {
                CodeValue = request.Code,
                IsValid = isValid
            };
        }
    }
}
