﻿namespace BNine.Application.Aggregates.OTPCodes.Commands.DeactivateCode
{
    using System;
    using System.Linq;
    using System.Threading;
    using System.Threading.Tasks;
    using AutoMapper;
    using BNine.Application.Abstractions;
    using BNine.Application.Aggregates.Configuration.Queries.GetConfiguration;
    using BNine.Application.Exceptions;
    using BNine.Application.Interfaces;
    using BNine.Constants;
    using MediatR;

    public class DeactivateCodeCommandHandler
        : AbstractRequestHandler, IRequestHandler<DeactivateCodeCommand, Unit>
    {
        public DeactivateCodeCommandHandler(
            IMediator mediator,
            IBNineDbContext dbContext,
            IMapper mapper,
            ICurrentUserService currentUser)
            : base(mediator, dbContext, mapper, currentUser)
        {
        }

        public async Task<Unit> Handle(DeactivateCodeCommand request, CancellationToken cancellationToken)
        {
            if (request.Code == null)
            {
                throw new Exception("Unexpected error, code is missing");
            }

            var dal = DbContext.OTPs
                .Where(x => x.PhoneNumber == request.PhoneNumber)
                .FirstOrDefault(x => x.Value == request.Code);

            if (dal != null)
            {
                dal.IsValid = false;
                await DbContext.SaveChangesAsync(cancellationToken);
            }
            else if (request.Code != "555555")
            {
                var userId = DbContext.Users
                    .FirstOrDefault(x => x.Phone == request.PhoneNumber)?.Id;

                if (!userId.HasValue)
                {
                    throw new BadRequestException("There no user for the phone number");
                }

                var config = await Mediator.Send(new GetConfigurationQuery(userId.Value));
                if (config.HasFeatureEnabled(FeaturesNames.Features.SmsVerificationDevelopmentMode))
                {
                    return Unit.Value;
                }
            }

            return Unit.Value;
        }
    }
}
