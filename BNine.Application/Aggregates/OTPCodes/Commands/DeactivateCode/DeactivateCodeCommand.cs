﻿namespace BNine.Application.Aggregates.OTPCodes.Commands.DeactivateCode
{
    using MediatR;

    public class DeactivateCodeCommand : IRequest<Unit>
    {
        public string Code
        {
            get; set;
        }

        public string PhoneNumber
        {
            get;
            set;
        }
    }
}
