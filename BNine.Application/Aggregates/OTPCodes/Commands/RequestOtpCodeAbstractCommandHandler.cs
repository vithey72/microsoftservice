﻿namespace BNine.Application.Aggregates.OTPCodes.Commands
{
    using System;
    using System.Net;
    using System.Threading.Tasks;
    using AutoMapper;
    using BNine.Application.Abstractions;
    using RequestSmsCode;
    using BNine.Application.Exceptions;
    using BNine.Application.Interfaces;
    using BNine.Common;
    using BNine.Domain.Entities;
    using BNine.Domain.Entities.User;
    using BNine.ResourceLibrary;
    using Helpers;
    using MediatR;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.Extensions.Localization;
    using Microsoft.Extensions.Logging;

    public abstract class RequestOtpCodeAbstractCommandHandler : AbstractRequestHandler
    {
        private readonly ILogger<RequestOtpCodeAbstractCommandHandler> _logger;
        private readonly IPartnerProviderService _partnerProvider;

        protected ISmsService SmsService
        {
            get;
        }

        protected ICallService CallService
        {
            get;
        }

        protected IStringLocalizer<SharedResource> SharedLocalizer
        {
            get;
        }

        public ICurrentUserIpAddressService UserIpAddressService
        {
            get;
        }

        public RequestOtpCodeAbstractCommandHandler(
            IMediator mediator,
            IBNineDbContext dbContext,
            IMapper mapper,
            ICurrentUserService currentUser,
            ISmsService smsService,
            ICallService callService,
            IStringLocalizer<SharedResource> sharedLocalizer,
            ILogger<RequestOtpCodeAbstractCommandHandler> logger,
            IPartnerProviderService partnerProvider,
            ICurrentUserIpAddressService userIpAddressService
        ) : base(mediator, dbContext, mapper, currentUser)
        {
            _logger = logger;
            _partnerProvider = partnerProvider;
            SmsService = smsService;
            CallService = callService;
            SharedLocalizer = sharedLocalizer;
            UserIpAddressService = userIpAddressService;
        }

        protected async Task CreateCode<T>(RequestOtpCodeAbstractCommand<T> request)
        {

            var isVoiceRequest = request.IsVoiceRequest is true;
            var ipAddress = UserIpAddressService.CurrentIp;
            if (isVoiceRequest)
            {
                var userPhoneNumber = request.Phone;
                CheckIpAddressIsValid(ipAddress);
                await CheckShortVoiceLimitReached(userPhoneNumber, ipAddress, CallService.MaxCallsShortLimit);
                await CheckLongVoiceLimitReached(userPhoneNumber, ipAddress, CallService.MaxCallsLongLimit);
            }
            var code = CodeGenerator.GenerateCode();

            DbContext.OTPs.Add(new OTP
            {
                Action = request.Action,
                Value = code,
                IsValid = true,
                PhoneNumber = request.Phone,
                IsFromVoiceRequest = request.IsVoiceRequest,
                IpAddress = ipAddress,
                Date = DateTime.UtcNow
            });

            await DbContext.SaveChangesAsync(CancellationToken.None);

            try
            {
                if(isVoiceRequest)
                {

                    var callBody =
                        $"Please listen carefully for your security code. " +
                        $"{StringProvider.GetOTPCodeMessage(code, _partnerProvider)}";
                    await CallService.CallAsync(request.Phone, callBody);
                }
                else
                {
                    var smsBody = $"<#>{StringProvider.GetOTPCodeMessage(code, _partnerProvider)}\n\nlp+o6v3RRZY";
                    await SmsService.SendMessageAsync(request.Phone, smsBody);
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Could not send OTP code for action type {request.Action} to number {request.Phone}", request.Action, request.Phone);
            }
        }

        protected async Task UserMustBeExisted(string phone)
        {
            var isUserExist = await DbContext.Users.AnyAsync(x => x.Phone == phone);

            if (!isUserExist)
            {
                throw new ValidationException(nameof(User), SharedLocalizer["noUserFoundValidationErrorMessage"].Value);
            }
        }

        private void CheckIpAddressIsValid(string ipAddress)
        {
            var success = IPAddress.TryParse(ipAddress, out var _);
            if (!success)
            {
                throw new ValidationException(nameof(UserIpAddressService.CurrentIp),
                    "IP address present in the request is not valid. Please try again");
            }
        }

        private async Task CheckShortVoiceLimitReached(string phone, string ipAddress, int? maxCount, int upperThresholdMinutes = 1)
        {

            maxCount ??= 1;

            if (await DbContext.OTPs.Where(otp =>
                    (otp.PhoneNumber == phone ||
                    otp.IpAddress == ipAddress) &&
                    otp.IsFromVoiceRequest.HasValue && otp.IsFromVoiceRequest.Value &&
                    otp.Date > DateTime.UtcNow.AddMinutes(-upperThresholdMinutes)).CountAsync() >= maxCount)
            {
                throw new ValidationException(nameof(OTP),
                    $"Max attempts reached. Retry later or try SMS instead.");
            }
        }

        private async Task CheckLongVoiceLimitReached(string phone, string ipAddress, int? maxCount, int upperThresholdDays = 1)
        {
            maxCount ??= 3;

            if (await DbContext.OTPs.Where(otp =>
                    (otp.PhoneNumber == phone ||
                    otp.IpAddress == ipAddress) &&
                    otp.IsFromVoiceRequest.HasValue && otp.IsFromVoiceRequest.Value &&
                    otp.Date > DateTime.UtcNow.AddDays(-upperThresholdDays)).CountAsync() >= maxCount)
            {
                throw new ValidationException(nameof(OTP),
                    $"Max attempts reached. Retry later or try SMS instead.");
            };
        }


    }
}
