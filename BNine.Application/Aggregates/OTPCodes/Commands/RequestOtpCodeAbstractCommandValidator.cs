﻿namespace BNine.Application.Aggregates.OTPCodes.Commands
{
    using BNine.Application.Aggregates.OTPCodes.Commands.RequestSmsCode;
    using BNine.Common;
    using FluentValidation;

    public abstract class RequestOtpCodeAbstractCommandValidator<T, V>
        : AbstractValidator<T> where T : RequestOtpCodeAbstractCommand<V>
    {
        public RequestOtpCodeAbstractCommandValidator()
        {
            RuleFor(x => x.Phone)
                .NotEmpty()
                .Must(PhoneHelper.IsValidPhone)
                .WithMessage("Phone number entered incorrectly. Please try again");
        }
    }
}
