﻿namespace BNine.Application.Aggregates.OTPCodes.Commands.RequestChangePinCode
{
    using BNine.Application.Aggregates.OTPCodes.Commands.RequestSmsCode;
    using MediatR;

    public class RequestChangePinCodeCommand : RequestOtpCodeAbstractCommand<Unit>
    {
        public RequestChangePinCodeCommand()
        {
            Action = Enums.OTPAction.ResetPinCode;
        }
    }
}
