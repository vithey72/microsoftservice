﻿namespace BNine.Application.Aggregates.OTPCodes.Commands.RequestChangePinCode
{
    using System.Threading;
    using System.Threading.Tasks;
    using AutoMapper;
    using BNine.Application.Interfaces;
    using BNine.ResourceLibrary;
    using MediatR;
    using Microsoft.Extensions.Localization;
    using Microsoft.Extensions.Logging;

    public class RequestChangePinCodeCommandHandler
        : RequestOtpCodeAbstractCommandHandler, IRequestHandler<RequestChangePinCodeCommand, Unit>
    {
        public RequestChangePinCodeCommandHandler(IMediator mediator,
            IBNineDbContext dbContext,
            IMapper mapper,
            ICurrentUserService currentUser,
            ISmsService smsService,
            ICallService callService,
            IStringLocalizer<SharedResource> sharedLocalizer,
            ILogger<RequestOtpCodeAbstractCommandHandler> logger,
            IPartnerProviderService partnerProvider,
            ICurrentUserIpAddressService userIpAddressService
            )
            : base(mediator, dbContext, mapper, currentUser, smsService, callService, sharedLocalizer, logger, partnerProvider, userIpAddressService)
        {
        }

        public async Task<Unit> Handle(RequestChangePinCodeCommand request, CancellationToken cancellationToken)
        {
            await UserMustBeExisted(request.Phone);

            await CreateCode(request);

            return Unit.Value;
        }
    }
}
