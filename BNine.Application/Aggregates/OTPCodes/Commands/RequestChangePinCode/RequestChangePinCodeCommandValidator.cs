﻿namespace BNine.Application.Aggregates.OTPCodes.Commands.RequestChangePinCode
{
    using MediatR;

    public class RequestChangePinCodeCommandValidator
        : RequestOtpCodeAbstractCommandValidator<RequestChangePinCodeCommand, Unit>
    {
        public RequestChangePinCodeCommandValidator() : base()
        {
        }
    }
}
