﻿namespace BNine.Application.Aggregates.OTPCodes.Commands.RequestSmsCode
{
    using BNine.Enums;
    using MediatR;
    using Newtonsoft.Json;

    public abstract class RequestOtpCodeAbstractCommand<T> : IRequest<T>
    {
        [JsonProperty("phone")]
        public string Phone
        {
            get; set;
        }

        [JsonIgnore]
        public OTPAction Action
        {
            get; set;
        }

        [JsonProperty("isVoiceRequest")]
        public bool? IsVoiceRequest
        {
            get;
            set;
        }
    }
}
