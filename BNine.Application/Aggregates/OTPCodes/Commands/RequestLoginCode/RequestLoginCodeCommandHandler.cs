﻿namespace BNine.Application.Aggregates.OTPCodes.Commands.RequestLoginCode
{
    using System.Linq;
    using System.Threading;
    using System.Threading.Tasks;
    using AutoMapper;
    using BNine.Application.Exceptions;
    using BNine.Application.Interfaces;
    using BNine.Common.TableConnect.Common;
    using BNine.ResourceLibrary;
    using MediatR;
    using Microsoft.Extensions.Localization;
    using Microsoft.Extensions.Logging;

    public class RequestLoginCodeCommandHandler
        : RequestOtpCodeAbstractCommandHandler, IRequestHandler<RequestLoginCodeCommand, Unit>
    {
        public RequestLoginCodeCommandHandler(
            IMediator mediator,
            IBNineDbContext dbContext,
            IMapper mapper,
            ICurrentUserService currentUser,
            ISmsService smsService,
            ICallService callService,
            IStringLocalizer<SharedResource> sharedLocalizer,
            ILogger<RequestLoginCodeCommandHandler> logger,
            IPartnerProviderService partnerProvider,
            ICurrentUserIpAddressService userIpAddressService)
            : base(mediator, dbContext, mapper, currentUser, smsService, callService, sharedLocalizer, logger, partnerProvider, userIpAddressService)
        {
        }

        public async Task<Unit> Handle(RequestLoginCodeCommand request, CancellationToken cancellationToken)
        {
            CredentialsMustBeValid(request);

            await CreateCode(request);

            return Unit.Value;
        }

        private void CredentialsMustBeValid(RequestLoginCodeCommand request)
        {
            var user = DbContext.Users
                .Where(x => x.Phone.Equals(request.Phone))
                .Select(x => new
                {
                    x.Password
                })
                .FirstOrDefault();

            if (user == null || !CryptoHelper.VerifyHashedPassword(user.Password, request.Password))
            {
                throw new ValidationException(nameof(request.Password), SharedLocalizer["incorrectCredentialsValidationErrorMessage"].Value);
            }
        }
    }
}
