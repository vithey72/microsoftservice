﻿namespace BNine.Application.Aggregates.OTPCodes.Commands.RequestLoginCode
{
    using FluentValidation;
    using MediatR;

    public class RequestLoginCodeCommandValidator
        : RequestOtpCodeAbstractCommandValidator<RequestLoginCodeCommand, Unit>
    {
        public RequestLoginCodeCommandValidator()
        {
            RuleFor(x => x.Password)
                .NotEmpty();
        }
    }
}
