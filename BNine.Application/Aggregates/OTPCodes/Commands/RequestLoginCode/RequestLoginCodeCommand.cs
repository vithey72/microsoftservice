﻿namespace BNine.Application.Aggregates.OTPCodes.Commands.RequestLoginCode
{
    using BNine.Application.Aggregates.OTPCodes.Commands.RequestSmsCode;
    using MediatR;
    using Newtonsoft.Json;

    public class RequestLoginCodeCommand : RequestOtpCodeAbstractCommand<Unit>
    {
        public RequestLoginCodeCommand()
        {
            Action = Enums.OTPAction.Login;
        }

        [JsonProperty("password")]
        public string Password
        {
            get; set;
        }
    }
}
