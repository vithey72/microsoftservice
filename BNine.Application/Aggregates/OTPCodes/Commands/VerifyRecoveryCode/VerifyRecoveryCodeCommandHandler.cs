﻿namespace BNine.Application.Aggregates.OTPCodes.Commands.VerifyRecoveryCode
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading;
    using System.Threading.Tasks;
    using Abstractions;
    using AutoMapper;
    using BNine.Application.Aggregates.Configuration.Queries.GetConfiguration;
    using BNine.Application.Aggregates.ServiceBusEvents.Commands.PublishB9Event;
    using BNine.Constants;
    using Enums;
    using Exceptions;
    using Interfaces;
    using MediatR;
    using Microsoft.Extensions.Localization;
    using Models;
    using Newtonsoft.Json;
    using ResourceLibrary;

    public class VerifyRecoveryCodeCommandHandler :
        AbstractRequestHandler,
        IRequestHandler<VerifyRecoveryCodeCommand, Code>
    {
        private IStringLocalizer<SharedResource> SharedLocalizer
        {
            get;
        }

        public VerifyRecoveryCodeCommandHandler(
            IMediator mediator,
            IBNineDbContext dbContext,
            IMapper mapper,
            ICurrentUserService currentUser,
            IStringLocalizer<SharedResource> sharedLocalizer)
            : base(mediator, dbContext, mapper, currentUser)
        {
            SharedLocalizer = sharedLocalizer;
        }

        public async Task<Code> Handle(VerifyRecoveryCodeCommand request, CancellationToken cancellationToken)
        {
            var isValid = DbContext.OTPs
                .Where(x => x.PhoneNumber == request.PhoneNumber)
                .Where(x => x.IsValid)
                .Where(x => x.Action == OTPAction.Recovery)
                .Where(x => x.Date.AddHours(1) > DateTime.UtcNow)
                .Any(x => x.Value == request.RecoveryCode);

            if (!isValid && request.RecoveryCode == "555555")
            {
                var userId = DbContext.Users
                    .Where(x => x.Phone == request.PhoneNumber)
                    .FirstOrDefault()?.Id;

                if (!userId.HasValue)
                {
                    throw new BadRequestException("There no user for the phone number");
                }

                var config = await Mediator.Send(new GetConfigurationQuery(userId.Value));
                if (config.HasFeatureEnabled(FeaturesNames.Features.SmsVerificationDevelopmentMode))
                {
                    isValid = true;
                }
            }

            if (!isValid)
            {
                throw new ValidationException(nameof(request.RecoveryCode),
                    SharedLocalizer["invalidCodeValidationErrorMessage"].Value);
            }

            var user = DbContext.Users.FirstOrDefault(x => x.Phone.Equals(request.PhoneNumber));

            if (user != null)
            {
                user.Settings.IsResetPasswordAllowed = true;

                await DbContext.SaveChangesAsync(cancellationToken);

                await Mediator.Send(new PublishB9EventCommand(
                      user,
                      ServiceBusTopics.B9User,
                      ServiceBusEvents.B9UserSettingsUpdated,
                      new Dictionary<string, object>
                      {
                          { ServiceBusDefaults.UpdatedFieldsAdditionalProperty , JsonConvert.SerializeObject(new List<string> { nameof(user.Settings.IsResetPasswordAllowed) }) }
                      }));
            }
            else
            {
                throw new ValidationException(nameof(request.PhoneNumber),
                    SharedLocalizer["noUserFoundValidationErrorMessage"].Value);
            }

            return new Code { CodeValue = request.RecoveryCode, IsValid = true };
        }
    }
}
