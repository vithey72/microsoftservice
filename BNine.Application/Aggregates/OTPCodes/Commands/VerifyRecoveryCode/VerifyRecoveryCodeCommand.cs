﻿namespace BNine.Application.Aggregates.OTPCodes.Commands.VerifyRecoveryCode
{
    using BNine.Application.Aggregates.OTPCodes.Models;
    using MediatR;
    using Newtonsoft.Json;

    public class VerifyRecoveryCodeCommand : IRequest<Code>
    {
        [JsonProperty("phoneNumber")]
        public string PhoneNumber
        {
            get; set;
        }

        [JsonProperty("recoveryCode")]
        public string RecoveryCode
        {
            get; set;
        }
    }
}
