﻿namespace BNine.Application.Aggregates.OTPCodes.Commands.VerifyRecoveryCode
{
    using System.Linq;
    using FluentValidation;
    using Interfaces;
    using Microsoft.Extensions.Localization;
    using ResourceLibrary;

    public class VerifyRecoveryCodeCommandValidator : AbstractValidator<VerifyRecoveryCodeCommand>
    {
        private IBNineDbContext DbContext
        {
            get;
        }

        public VerifyRecoveryCodeCommandValidator(
            IBNineDbContext dbContext,
            IStringLocalizer<SharedResource> sharedLocalizer
            )
        {
            DbContext = dbContext;

            RuleFor(x => x.PhoneNumber)
                .Must(IsUserExists)
                .WithMessage(sharedLocalizer["noUserFoundValidationErrorMessage"].Value);

            RuleFor(x => x.RecoveryCode)
                .NotEmpty();
        }

        private bool IsUserExists(string phoneNumber)
        {
            return DbContext.Users.Any(x => x.Phone.Equals(phoneNumber));
        }
    }
}
