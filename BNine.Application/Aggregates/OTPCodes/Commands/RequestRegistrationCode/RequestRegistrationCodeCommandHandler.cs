﻿namespace BNine.Application.Aggregates.OTPCodes.Commands.RequestRegistrationCode
{
    using System;
    using System.Linq;
    using System.Threading;
    using System.Threading.Tasks;
    using AutoMapper;
    using BNine.Application.Aggregates.OTPCodes.Models;
    using BNine.Application.Aggregates.ServiceBusEvents.Commands.PublishB9Event;
    using BNine.Application.Exceptions;
    using BNine.Application.Interfaces;
    using BNine.Constants;
    using BNine.Domain.Entities;
    using BNine.Enums;
    using BNine.ResourceLibrary;
    using MediatR;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.Extensions.Localization;
    using Microsoft.Extensions.Logging;

    public class RequestRegistrationCodeCommandHandler
        : RequestOtpCodeAbstractCommandHandler,
        IRequestHandler<RequestRegistrationCodeCommand, RegistrationResponse>
    {
        public RequestRegistrationCodeCommandHandler(
            IMediator mediator,
            IBNineDbContext dbContext,
            IMapper mapper,
            ICurrentUserService currentUser,
            ISmsService smsService,
            ICallService callService,
            IStringLocalizer<SharedResource> sharedLocalizer,
            ILogger<RequestRegistrationCodeCommandHandler> logger,
            IPartnerProviderService partnerProvider,
            ICurrentUserIpAddressService userIpAddressService
            ) : base(mediator, dbContext, mapper, currentUser, smsService, callService, sharedLocalizer, logger, partnerProvider, userIpAddressService)
        {
        }

        public async Task<RegistrationResponse> Handle(RequestRegistrationCodeCommand request, CancellationToken cancellationToken)
        {
            await CheckIfNotExisted(request.Phone);

            var appsflyerUser = await CreateAnonymousAppsflyerUser(request.Phone, request.AppsflyerId, cancellationToken);

            await CreateCode(request);


            return new RegistrationResponse
            {
                AppsflyerId = appsflyerUser.Id
            };
        }

        private async Task<AnonymousAppsflyerId> CreateAnonymousAppsflyerUser(string phone, string appsflyerId, CancellationToken cancellationToken)
        {
            var appsflyerUserId = DbContext.AnonymousAppsflyerIds.FirstOrDefault(x => x.Phone.Equals(phone));

            if (appsflyerUserId == null)
            {
                appsflyerUserId = new AnonymousAppsflyerId
                {
                    Id = Guid.NewGuid(),
                    Phone = phone,
                    AppsflyerId = appsflyerId
                };

                await DbContext.AnonymousAppsflyerIds.AddAsync(appsflyerUserId);
                await DbContext.SaveChangesAsync(cancellationToken);

                await Mediator.Send(new PublishB9EventCommand(appsflyerUserId, ServiceBusTopics.B9AppsflyerId, ServiceBusEvents.B9Created));
            }

            return appsflyerUserId;
        }

        private async Task CheckIfNotExisted(string phone)
        {
            var userWithSamePhone = await DbContext.Users
                .Where(x => x.Phone.Equals(phone))
                .Select(x => new { x.Status })
                .FirstOrDefaultAsync();

            if (userWithSamePhone != null)
            {
                throw new ValidationException(
                    nameof(phone),
                    userWithSamePhone.Status == UserStatus.Active
                        ? ErrorCodes.User.ActiveUserWithProvidedPhoneAlreadyExists
                        : ErrorCodes.User.InactiveUserWithProvidedPhoneAlreadyExists,
                    SharedLocalizer["accountExistsValidationErrorMessage"].Value);
            }
        }
    }
}
