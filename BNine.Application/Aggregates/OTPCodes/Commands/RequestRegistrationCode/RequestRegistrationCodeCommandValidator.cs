﻿namespace BNine.Application.Aggregates.OTPCodes.Commands.RequestRegistrationCode
{
    using Models;

    public class RequestRegistrationCodeCommandValidator
         : RequestOtpCodeAbstractCommandValidator<RequestRegistrationCodeCommand, RegistrationResponse>
    {
        public RequestRegistrationCodeCommandValidator() : base()
        {
        }
    }

}

