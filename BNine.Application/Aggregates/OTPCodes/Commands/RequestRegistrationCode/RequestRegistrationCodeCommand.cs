﻿namespace BNine.Application.Aggregates.OTPCodes.Commands.RequestRegistrationCode
{
    using BNine.Application.Aggregates.OTPCodes.Commands.RequestSmsCode;
    using Models;
    using Newtonsoft.Json;

    public class RequestRegistrationCodeCommand : RequestOtpCodeAbstractCommand<RegistrationResponse>
    {
        [JsonProperty("appsflyerId")]
        public string AppsflyerId
        {
            get; set;
        }

        public RequestRegistrationCodeCommand()
        {
            Action = Enums.OTPAction.Signup;
        }
    }
}
