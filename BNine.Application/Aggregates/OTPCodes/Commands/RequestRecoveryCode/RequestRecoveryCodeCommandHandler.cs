﻿namespace BNine.Application.Aggregates.OTPCodes.Commands.RequestRecoveryCode
{
    using System.Linq;
    using System.Threading;
    using System.Threading.Tasks;
    using AutoMapper;
    using BNine.Application.Exceptions;
    using BNine.Application.Interfaces;
    using BNine.Domain.Entities.User;
    using BNine.ResourceLibrary;
    using MediatR;
    using Microsoft.Extensions.Localization;
    using Microsoft.Extensions.Logging;

    public class RequestRecoveryCodeCommandHandler :
        RequestOtpCodeAbstractCommandHandler,
        IRequestHandler<RequestRecoveryCodeCommand, Unit>
    {
        public RequestRecoveryCodeCommandHandler(
            IMediator mediator,
            IBNineDbContext dbContext,
            IMapper mapper,
            ICurrentUserService currentUser,
            ISmsService smsService,
            ICallService callService,
            IStringLocalizer<SharedResource> sharedLocalizer,
            ILogger<RequestRecoveryCodeCommandHandler> logger,
            IPartnerProviderService partnerProvider,
            ICurrentUserIpAddressService userIpAddressService
            )
            : base(mediator, dbContext, mapper, currentUser, smsService, callService, sharedLocalizer, logger, partnerProvider, userIpAddressService)
        {
        }

        public async Task<Unit> Handle(RequestRecoveryCodeCommand request, CancellationToken cancellationToken)
        {
            CheckIfUserExists(request.Phone);
            await CreateCode(request);
            return Unit.Value;
        }

        private void CheckIfUserExists(string phone)
        {
            var isUserExist = DbContext.Users.Any(x => x.Phone == phone);

            if (!isUserExist)
            {
                throw new ValidationException(nameof(User), SharedLocalizer["noUserFoundValidationErrorMessage"].Value);
            }
        }
    }
}
