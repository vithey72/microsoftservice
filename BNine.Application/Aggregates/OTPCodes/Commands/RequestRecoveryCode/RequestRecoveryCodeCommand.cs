﻿namespace BNine.Application.Aggregates.OTPCodes.Commands.RequestRecoveryCode
{
    using BNine.Application.Aggregates.OTPCodes.Commands.RequestSmsCode;
    using MediatR;

    public class RequestRecoveryCodeCommand : RequestOtpCodeAbstractCommand<Unit>
    {
        public RequestRecoveryCodeCommand()
        {
            Action = Enums.OTPAction.Recovery;
        }
    }
}
