﻿namespace BNine.Application.Aggregates.OTPCodes.Models
{
    using System;
    using Newtonsoft.Json;

    public class RegistrationResponse
    {
        [JsonProperty("appsflyerId")]
        public Guid AppsflyerId
        {
            get;
            set;
        }
    }
}
