﻿namespace BNine.Application.Aggregates.AdminUser.Commands;

using System.Text.RegularExpressions;
using FluentValidation;

public class UpdateAdminApiUserCommandValidator  : AbstractValidator<UpdateAdminApiUserCommand>
{
    public UpdateAdminApiUserCommandValidator()
    {
        RuleFor(x => x.Password)
            .Must(IsPasswordStrong)
            .WithMessage(@"Password requirements validation failed:
                Password should be At least 7 chars, at least 1 uppercase char (A-Z), at least 1 number (0-9) and at least one special char");

        RuleFor(x => x.Email)
            .NotEmpty();
        RuleFor(x => x.Role)
            .NotEmpty();
        RuleFor(x => x.FirstName)
            .NotEmpty();
        RuleFor(x => x.LastName)
            .NotEmpty();
    }

    public static bool IsPasswordStrong(string password)
    {
        return Regex.IsMatch(password, @"^.*(?=.{7,})(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[^a-zA-Z0-9]).*$");
    }
}
