﻿namespace BNine.Application.Aggregates.AdminUser.Commands;

using Abstractions;
using AutoMapper;
using Common.TableConnect.Common;
using Enums;
using Exceptions;
using Extensions;
using Interfaces;
using MediatR;
using Models.Dto;

public class UpdateAdminApiUserCommandHandler : AbstractRequestHandler,
    IRequestHandler<UpdateAdminApiUserCommand, BNine.Application.Models.Dto.AdminApiUser>
{
    public UpdateAdminApiUserCommandHandler(IMediator mediator, IBNineDbContext dbContext, IMapper mapper,
        ICurrentUserService currentUser) : base(mediator, dbContext, mapper, currentUser)
    {
    }

    public async Task<AdminApiUser> Handle(UpdateAdminApiUserCommand request, CancellationToken cancellationToken)
    {
        var domainUser = DbContext.AdminUsers
            .FirstOrDefault(x => x.Id == request.UserId);

        if (domainUser == null)
        {
            throw new NotFoundException($"User with id {request.UserId} not found");
        }

        if (!string.IsNullOrEmpty(request.Email) && request.Email != domainUser.Email)
        {
            if (DbContext.AdminUsers.FirstOrDefault(x => x.Email == request.Email) is not null)
            {
                throw new ValidationException("email", "User with the same email already exists");
            }

            domainUser.Email = request.Email;
        }

        if (!string.IsNullOrEmpty(request.FirstName) && request.FirstName != domainUser.FirstName)
        {
            domainUser.FirstName = request.FirstName;
        }

        if (!string.IsNullOrEmpty(request.LastName) && request.LastName != domainUser.LastName)
        {
            domainUser.LastName = request.LastName;
        }

        if (request.Role != default && request.Role != domainUser.RoleId)
        {
            domainUser.RoleId = request.Role;
        }

        //check if the password is different, then change it
        if (!string.IsNullOrEmpty(request.Password) && !CryptoHelper.VerifyHashedPassword(domainUser.Password, request.Password))
        {
            domainUser.Password = CryptoHelper.HashPassword(request.Password);
        }

        await DbContext.SaveChangesAsync(cancellationToken);

        return new AdminApiUser()
        {
            Id = domainUser.Id,
            Email = domainUser.Email,
            CreatedAt = domainUser.CreatedAt,
            FirstName = domainUser.FirstName,
            LastName = domainUser.LastName,
            Role = EnumExtensions.GetEnumMemberValue<AdminRole>(domainUser.RoleId)
        };
    }
}
