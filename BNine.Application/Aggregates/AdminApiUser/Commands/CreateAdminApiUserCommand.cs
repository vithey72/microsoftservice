﻿namespace BNine.Application.Aggregates.AdminUser.Commands;

using Enums;
using MediatR;

public class CreateAdminApiUserCommand : IRequest<BNine.Application.Models.Dto.AdminApiUser>
{
    public string Email
    {
        get; set;
    }

    public string Password
    {
        get; set;
    }

    public string FirstName
    {
        get; set;
    }

    public string LastName
    {
        get; set;
    }

    public AdminRole Role
    {
        get;
        set;
    }
}
