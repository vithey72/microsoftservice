﻿namespace BNine.Application.Aggregates.AdminUser.Commands;

using Abstractions;
using AutoMapper;
using Common.TableConnect.Common;
using Enums;
using Exceptions;
using Extensions;
using Interfaces;
using MediatR;
using Models.Dto;

public class CreateAdminApiUserCommandHandler : AbstractRequestHandler,
    IRequestHandler<CreateAdminApiUserCommand, BNine.Application.Models.Dto.AdminApiUser>
{
    public CreateAdminApiUserCommandHandler(IMediator mediator, IBNineDbContext dbContext, IMapper mapper,
        ICurrentUserService currentUser) : base(mediator, dbContext, mapper, currentUser)
    {
    }

    public async Task<AdminApiUser> Handle(CreateAdminApiUserCommand request, CancellationToken cancellationToken)
    {
        if (DbContext.AdminUsers.FirstOrDefault(x => x.Email == request.Email) is not null)
        {
            throw new ValidationException(nameof(request.Email), "User with the same email already exists");
        }

        var domainUser = new BNine.Domain.Entities.AdminApiUser()
        {
            Email = request.Email,
            RoleId = request.Role,
            Password = CryptoHelper.HashPassword(request.Password),
            CreatedAt = DateTime.UtcNow,
            FirstName = request.FirstName,
            LastName = request.LastName
        };

        var result = await DbContext.AdminUsers.AddAsync(domainUser);

        await DbContext.SaveChangesAsync(cancellationToken);

        return new AdminApiUser()
        {
            Id = domainUser.Id,
            Email = domainUser.Email,
            CreatedAt = domainUser.CreatedAt,
            FirstName = domainUser.FirstName,
            LastName = domainUser.LastName,
            Role = EnumExtensions.GetEnumMemberValue<AdminRole>(domainUser.RoleId)
        };
    }
}
