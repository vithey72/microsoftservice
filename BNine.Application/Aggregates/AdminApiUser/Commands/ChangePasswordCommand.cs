﻿namespace BNine.Application.Aggregates.AdminUser.Commands;

using MediatR;

public class ChangePasswordCommand: IRequest<Unit>
{
    public Guid UserId
    {
        get;
        set;
    }

    public string OldPassword
    {
        get; set;
    }

    public string NewPassword
    {
        get; set;
    }

    public string NewPasswordSecondInput
    {
        get; set;
    }
}
