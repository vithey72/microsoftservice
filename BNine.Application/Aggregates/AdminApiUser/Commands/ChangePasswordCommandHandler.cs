﻿namespace BNine.Application.Aggregates.AdminUser.Commands;

using Abstractions;
using AutoMapper;
using Common.TableConnect.Common;
using Exceptions;
using Interfaces;
using MediatR;

public class ChangePasswordCommandHandler : AbstractRequestHandler,
    IRequestHandler<ChangePasswordCommand, Unit>
{
    public ChangePasswordCommandHandler(IMediator mediator, IBNineDbContext dbContext, IMapper mapper,
        ICurrentUserService currentUser) : base(mediator, dbContext, mapper, currentUser)
    {
    }

    public async Task<Unit> Handle(ChangePasswordCommand request, CancellationToken cancellationToken)
    {
        var domainUser = DbContext.AdminUsers
            .FirstOrDefault(x => x.Id == request.UserId);

        if (domainUser == null)
        {
            throw new NotFoundException($"User with id {request.UserId} not found");
        }

        if (!CryptoHelper.VerifyHashedPassword(domainUser.Password, request.OldPassword))
        {
            throw new ValidationException("password", "Password is incorrect");
        }

        if (request.NewPassword != request.NewPasswordSecondInput)
        {
            throw new ValidationException("newPassword", "Passwords do not match");
        }

        //check if the password is different, then change it
        if (!CryptoHelper.VerifyHashedPassword(domainUser.Password, request.NewPassword))
        {
            domainUser.Password = CryptoHelper.HashPassword(request.NewPassword);
        }

        await DbContext.SaveChangesAsync(cancellationToken);

        return Unit.Value;
    }
}
