﻿namespace BNine.Application.Aggregates.AdminUser.Queries
{
    using System.Linq;
    using System.Threading;
    using System.Threading.Tasks;
    using AutoMapper;
    using BNine.Application.Abstractions;
    using BNine.Application.Interfaces;
    using BNine.Application.Models.Dto;
    using Enums;
    using Exceptions;
    using Extensions;
    using MediatR;

    public class GetAdminApiUserByEmailHandler
        : AbstractRequestHandler, IRequestHandler<GetAdminApiUserByEmailQuery, AdminApiUser>
    {
        public GetAdminApiUserByEmailHandler(
            IMediator mediator,
            IBNineDbContext dbContext,
            IMapper mapper,
            ICurrentUserService currentUser)
            : base(mediator, dbContext, mapper, currentUser)
        {
        }

        public async Task<AdminApiUser> Handle(GetAdminApiUserByEmailQuery request, CancellationToken cancellationToken)
        {
            var user = DbContext.AdminUsers
                .Where(x => x.Email == request.Email)
                .Select(x => new AdminApiUser()
                {
                    Id = x.Id,
                    PasswordHash = x.Password,
                    Email = x.Email,
                    CreatedAt = x.CreatedAt,
                    FirstName = x.FirstName,
                    LastName = x.LastName,
                    Role = EnumExtensions.GetEnumMemberValue<AdminRole>(x.RoleId)
                })
                .FirstOrDefault();

            if (user == null)
            {
                throw new NotFoundException($"User with email {request.Email} not found");
            }

            return user;
        }
    }
}
