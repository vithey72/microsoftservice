﻿namespace BNine.Application.Aggregates.AdminUser.Queries
{
    using System;
    using MediatR;

    public class GetAdminApiUserByIdQuery : IRequest<BNine.Application.Models.Dto.AdminApiUser>
    {
        public Guid Id
        {
            get; set;
        }
    }
}
