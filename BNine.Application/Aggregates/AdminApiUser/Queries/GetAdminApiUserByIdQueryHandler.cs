﻿namespace BNine.Application.Aggregates.AdminUser.Queries
{
    using System.Linq;
    using System.Threading;
    using System.Threading.Tasks;
    using AutoMapper;
    using BNine.Application.Abstractions;
    using BNine.Application.Interfaces;
    using Enums;
    using Exceptions;
    using Extensions;
    using MediatR;

    public class GetAdminApiUserByIdQueryHandler
        : AbstractRequestHandler, IRequestHandler<GetAdminApiUserByIdQuery, BNine.Application.Models.Dto.AdminApiUser>
    {
        public GetAdminApiUserByIdQueryHandler(
            IMediator mediator,
            IBNineDbContext dbContext,
            IMapper mapper,
            ICurrentUserService currentUser)
            : base(mediator, dbContext, mapper, currentUser)
        {
        }

        public async Task<BNine.Application.Models.Dto.AdminApiUser> Handle(GetAdminApiUserByIdQuery request, CancellationToken cancellationToken)
        {
            var user = DbContext.AdminUsers
                .FirstOrDefault(x => x.Id == request.Id);

            if (user == null)
            {
                throw new NotFoundException($"User with id {request.Id} not found");
            }

            var result = new BNine.Application.Models.Dto.AdminApiUser()
            {
                Id = user.Id,
                PasswordHash = user.Password,
                Email = user.Email,
                CreatedAt = user.CreatedAt,
                FirstName = user.FirstName,
                LastName = user.LastName,
                Role = EnumExtensions.GetEnumMemberValue<AdminRole>(user.RoleId)
            };

            return result;
        }
    }
}
