﻿namespace BNine.Application.Aggregates.AdminUser.Queries;

using Abstractions;
using AutoMapper;
using Enums;
using Extensions;
using Interfaces;
using MediatR;
using Models.Dto;

public class GetAdminApiUsersCommandHandler : AbstractRequestHandler,
    IRequestHandler<GetAdminApiUsersCommand, List<BNine.Application.Models.Dto.AdminApiUser>>
{
    public GetAdminApiUsersCommandHandler(IMediator mediator, IBNineDbContext dbContext, IMapper mapper,
        ICurrentUserService currentUser) : base(mediator, dbContext, mapper, currentUser)
    {
    }

    public async Task<List<AdminApiUser>> Handle(GetAdminApiUsersCommand request, CancellationToken cancellationToken)
    {
        var users = DbContext.AdminUsers.Select(x => new AdminApiUser()
        {
            Id = x.Id,
            Email = x.Email,
            Role = EnumExtensions.GetEnumMemberValue<AdminRole>(x.RoleId),
            CreatedAt = x.CreatedAt,
            FirstName = x.FirstName,
            LastName = x.LastName
        }).ToList();

        return users;
    }
}
