﻿namespace BNine.Application.Aggregates.AdminUser.Queries
{
    using BNine.Application.Models.Dto;
    using MediatR;

    public class GetAdminApiUserByEmailQuery : IRequest<AdminApiUser>
    {
        public string Email
        {
            get; set;
        }
    }
}
