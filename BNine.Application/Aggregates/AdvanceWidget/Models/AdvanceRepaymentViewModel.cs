﻿namespace BNine.Application.Aggregates.AdvanceWidget.Models;

using Newtonsoft.Json;

public class AdvanceRepaymentViewModel
{
    [JsonProperty("amountToRepay")]
    public decimal AmountToRepay
    {
        get;
        set;
    }

    [JsonProperty("fee")]
    public decimal? Fee
    {
        get;
        set;
    }
}
