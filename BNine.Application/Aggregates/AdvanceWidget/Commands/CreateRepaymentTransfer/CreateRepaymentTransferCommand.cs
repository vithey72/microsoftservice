﻿namespace BNine.Application.Aggregates.AdvanceWidget.Commands.CreateRepaymentTransfer;

using Abstractions;
using MediatR;

public class CreateRepaymentTransferCommand
    : MBanqSelfServiceUserRequest
    , IRequest<int>
{
}
