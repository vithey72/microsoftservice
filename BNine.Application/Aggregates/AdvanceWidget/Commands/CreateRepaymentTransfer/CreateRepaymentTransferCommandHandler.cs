﻿namespace BNine.Application.Aggregates.AdvanceWidget.Commands.CreateRepaymentTransfer
{
    using Abstractions;
    using AutoMapper;
    using Domain.Entities;
    using Enums;
    using Exceptions;
    using Interfaces;
    using Interfaces.Bank.Client;
    using MediatR;
    using Microsoft.EntityFrameworkCore;

    public class CreateRepaymentTransferCommandHandler
        : AbstractRequestHandler
        , IRequestHandler<CreateRepaymentTransferCommand, int>
    {
        private readonly IBankLoanAccountsService _bankLoanAccountsService;

        public CreateRepaymentTransferCommandHandler(
            IMediator mediator,
            IBNineDbContext dbContext,
            IMapper mapper,
            ICurrentUserService currentUser,
            IBankLoanAccountsService bankLoanAccountsService)
            : base(mediator, dbContext, mapper, currentUser)
        {
            _bankLoanAccountsService = bankLoanAccountsService;
        }

        public async Task<int> Handle(CreateRepaymentTransferCommand request, CancellationToken cancellationToken)
        {
            var loanAccount = await DbContext.LoanAccounts
                .Where(x => x.Status == LoanAccountStatus.Active && x.UserId == CurrentUser.UserId)
                .FirstOrDefaultAsync(cancellationToken);

            if (loanAccount == null)
            {
                throw new NotFoundException(nameof(LoanAccount), CurrentUser.UserId);
            }

            var loan = await _bankLoanAccountsService.GetLoan(request.MbanqAccessToken, request.Ip, loanAccount.ExternalId);

            await _bankLoanAccountsService.MakeRepaymentLoanAccount(request.MbanqAccessToken, request.Ip, loanAccount.ExternalId, loan.OutstandingAmount);

            var closedLoan = await _bankLoanAccountsService.GetLoan(request.MbanqAccessToken, request.Ip, loanAccount.ExternalId);

            if (closedLoan != null && closedLoan.Closed)
            {
                loanAccount.Status = LoanAccountStatus.Closed;

                await DbContext.SaveChangesAsync(cancellationToken);
            }

            return Convert.ToInt32(loan.DisbursedAmount);
        }
    }
}
