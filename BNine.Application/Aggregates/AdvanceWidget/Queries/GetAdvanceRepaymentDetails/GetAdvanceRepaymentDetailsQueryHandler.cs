﻿namespace BNine.Application.Aggregates.AdvanceWidget.Queries.GetAdvanceRepaymentDetails;

using Abstractions;
using AutoMapper;
using Enums;
using Interfaces;
using Interfaces.Bank.Client;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Models;

using IBankLoanAccountsAdministrationService = Interfaces.Bank.Administration.IBankLoanAccountsService;

public class GetAdvanceRepaymentDetailsQueryHandler
    : AbstractRequestHandler
    , IRequestHandler<GetAdvanceRepaymentDetailsQuery, AdvanceRepaymentViewModel>
{
    private const string NoAdvanceToRepayMessage = "No advance to repay.";
    private readonly IBankLoanAccountsService _bankLoanAccountsService;
    private readonly IBankLoanAccountsAdministrationService _bankLoanAccountsAdministrationService;

    public GetAdvanceRepaymentDetailsQueryHandler(
        IMediator mediator,
        IBNineDbContext dbContext,
        IMapper mapper,
        ICurrentUserService currentUser,
        IBankLoanAccountsService bankLoanAccountsService,
        IBankLoanAccountsAdministrationService bankLoanAccountsAdministrationService
        )
        : base(mediator, dbContext, mapper, currentUser)
    {
        _bankLoanAccountsService = bankLoanAccountsService;
        _bankLoanAccountsAdministrationService = bankLoanAccountsAdministrationService;
    }

    public async Task<AdvanceRepaymentViewModel> Handle(GetAdvanceRepaymentDetailsQuery request, CancellationToken cancellationToken)
    {
        var loanDbEntry = await DbContext
            .LoanAccounts
            .FirstOrDefaultAsync(x => x.UserId == CurrentUser.UserId
                                      && x.Status == LoanAccountStatus.Active,
                cancellationToken);

        if (loanDbEntry == null)
        {
            throw new InvalidOperationException(NoAdvanceToRepayMessage);
        }

        var loanBankEntry = await _bankLoanAccountsService.GetLoan(
            request.MbanqAccessToken,
            request.Ip,
            loanDbEntry.ExternalId);

        if (loanBankEntry.Closed)
        {
            loanDbEntry.Status = LoanAccountStatus.Closed;
            await DbContext.SaveChangesAsync(cancellationToken);
            throw new InvalidOperationException(NoAdvanceToRepayMessage);
        }

        var outstandingAmount = loanBankEntry.OutstandingAmount;

        var loanAccount = await _bankLoanAccountsAdministrationService.GetLoan(loanDbEntry.ExternalId);
        var hasBoost = loanAccount is { FeeAmount: > 0m };
        if (hasBoost)
        {
            var advanceToRepay = Math.Max(0m, outstandingAmount - loanAccount.FeeAmount);
            var feeToRepay = outstandingAmount - advanceToRepay;
            return new AdvanceRepaymentViewModel
            {
                AmountToRepay = advanceToRepay,
                Fee = feeToRepay,
            };
        }
        else
        {
            return new AdvanceRepaymentViewModel
            {
                AmountToRepay = outstandingAmount,
            };
        }
    }
}
