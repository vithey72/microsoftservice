﻿namespace BNine.Application.Aggregates.AdvanceWidget.Queries.GetAdvanceRepaymentDetails;

using Abstractions;
using MediatR;
using Models;

public class GetAdvanceRepaymentDetailsQuery
    : MBanqSelfServiceUserRequest
    , IRequest<AdvanceRepaymentViewModel>
{
}
