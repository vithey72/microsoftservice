﻿namespace BNine.Application.Aggregates.IncomeAndTaxesScreen.Models;

using CommonModels.Dialog;
using Sections;

public class IncomeAndTaxesScreenViewModel
{
    public decimal IncomeAmount
    {
        get;
        set;
    }

    public Employer[] Employers
    {
        get;
        set;
    }

    public AprilSectionViewModel AprilSection
    {
        get;
        set;
    }

    public QuickBooksSectionViewModel QuickBooksSection
    {
        get;
        set;
    }

    public bool ShowSwitchPayrollDisclaimer => Employers == null || Employers.Length == 0;

    public GenericDialogRichBodyViewModel IncomeTaxesTooltipDialog
    {
        get;
        set;
    }

    public GenericDialogRichBodyViewModel SourceIncomeTooltipDialog
    {
        get;
        set;
    }

    public GenericDialogRichBodyViewModel AddIncomeTooltipDialog
    {
        get;
        set;
    }

    public class Employer
    {
        public string Title
        {
            get;
            set;
        }

        public string IconUrl
        {
            get;
            set;
        }
    }

}
