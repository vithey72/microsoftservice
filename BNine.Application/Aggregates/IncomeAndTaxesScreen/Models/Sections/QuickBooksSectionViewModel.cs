﻿namespace BNine.Application.Aggregates.IncomeAndTaxesScreen.Models.Sections;

using CommonModels.Buttons;
using CommonModels.DialogElements;

public class QuickBooksSectionViewModel: SectionViewModelWithImageUrl
{
    public ButtonWithRichDialogViewModel Button
    {
        get;
        set;
    }
}
