﻿namespace BNine.Application.Aggregates.IncomeAndTaxesScreen.Models.Sections;

using CommonModels.Buttons;
using CommonModels.DialogElements;

public class AprilSectionViewModel: SectionViewModelWithImageUrl
{
    public ButtonViewModel Button
    {
        get;
        set;
    }
}
