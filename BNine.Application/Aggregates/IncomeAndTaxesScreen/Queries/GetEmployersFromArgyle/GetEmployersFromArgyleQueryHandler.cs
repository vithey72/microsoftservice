﻿namespace BNine.Application.Aggregates.IncomeAndTaxesScreen.Queries.GetEmployersFromArgyle;

using System.Threading.Tasks;
using BNine.Application.Aggregates.PaycheckSwitch;
using MediatR;
using Models;

public class GetEmployersFromArgyleQueryHandler : IRequestHandler<GetEmployersFromArgyleQuery, IncomeAndTaxesScreenViewModel.Employer[]>
{
    private readonly IPaycheckSwitchDiscovery _paycheckSwitchDiscovery;

    public GetEmployersFromArgyleQueryHandler(
        IPaycheckSwitchDiscovery paycheckSwitchDiscovery
    )
    {
        _paycheckSwitchDiscovery = paycheckSwitchDiscovery;
    }

    public async Task<IncomeAndTaxesScreenViewModel.Employer[]> Handle(GetEmployersFromArgyleQuery request, CancellationToken cancellationToken)
    {
        var paycheckSwitch = await _paycheckSwitchDiscovery.DiscoverUserPaycheckSwitch(request.UserId, cancellationToken);

        var employers = await paycheckSwitch.GetEmployers(cancellationToken);

        return employers;
    }
}
