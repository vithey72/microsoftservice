﻿namespace BNine.Application.Aggregates.IncomeAndTaxesScreen.Queries.GetEmployersFromArgyle;

using BNine.Application.Aggregates.IncomeAndTaxesScreen.Models;
using MediatR;

public record GetEmployersFromArgyleQuery(Guid UserId) : IRequest<IncomeAndTaxesScreenViewModel.Employer[]>;
