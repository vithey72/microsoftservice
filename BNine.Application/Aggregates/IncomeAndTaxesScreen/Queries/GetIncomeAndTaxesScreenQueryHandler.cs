﻿namespace BNine.Application.Aggregates.IncomeAndTaxesScreen.Queries;

using System;
using System.Threading.Tasks;
using Abstractions;
using AutoMapper;
using CommonModels.Buttons;
using CommonModels.Dialog;
using CommonModels.Text;
using Configuration.Queries.GetConfiguration;
using Constants;
using Extensions;
using GetEmployersFromArgyle;
using Interfaces;
using Interfaces.Argyle;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Models;
using Models.Sections;

public class GetIncomeAndTaxesScreenQueryHandler
    : AbstractRequestHandler
    , IRequestHandler<GetIncomeAndTaxesScreenQuery, IncomeAndTaxesScreenViewModel>
{
    private readonly IArgyleEmploymentsService _argyleEmploymentsService;
    private readonly IArgyleLinkItemsService _argyleLinkItemsService;
    private readonly ITransferIconsAndNamesProviderService _iconsService;

    public GetIncomeAndTaxesScreenQueryHandler(
        IMediator mediator,
        IBNineDbContext dbContext,
        IMapper mapper,
        ICurrentUserService currentUser,
        IArgyleEmploymentsService argyleEmploymentsService,
        IArgyleLinkItemsService argyleLinkItemsService,
        ITransferIconsAndNamesProviderService iconsService
        )
        : base(mediator, dbContext, mapper, currentUser)
    {
        _argyleEmploymentsService = argyleEmploymentsService;
        _argyleLinkItemsService = argyleLinkItemsService;
        _iconsService = iconsService;
    }

    public async Task<IncomeAndTaxesScreenViewModel> Handle(GetIncomeAndTaxesScreenQuery request, CancellationToken cancellationToken)
    {
        var response = new IncomeAndTaxesScreenViewModel
        {
            IncomeAmount = await Get30DaysIncome(cancellationToken),
            Employers = await Mediator.Send(new GetEmployersFromArgyleQuery(CurrentUser.UserId!.Value), cancellationToken),
            AprilSection = await GetAprilSection(cancellationToken),
            QuickBooksSection = await GetQuickBooksSection(cancellationToken)
        };
        AddTooltips(response);

        return response;
    }

    private async Task<decimal> Get30DaysIncome(CancellationToken cancellationToken)
    {
        var achKeywords = (await DbContext.LoanSettings.SingleAsync(cancellationToken))
            .ACHPrincipalKeywords
            .ToArray();
        return await DbContext
            .ACHCreditTransfers
            .GetPayrollSum(CurrentUser.UserId!.Value, achKeywords, TimeSpan.FromDays(30));
    }

    private async Task<AprilSectionViewModel> GetAprilSection(CancellationToken cancellationToken)
    {
        var features = await Mediator.Send(new GetConfigurationQuery(), cancellationToken);
        if (!features.HasFeatureEnabled(FeaturesNames.Features.AprilTaxPlatform))
        {
            return null;
        }

        return new AprilSectionViewModel
        {
            Title = "Get your tax refund in a few clicks",
            Subtitle = "File your 2022 taxes with B9",
            Button = new ButtonViewModel("LEARN MORE", ButtonStyleEnum.Solid)
                .WithDeeplink(DeepLinkRoutes.April),
            ImageUrl = "https://b9prodaccount.blob.core.windows.net/static-documents-container/Misc/april_mini.png",
        };
    }

    private async Task<QuickBooksSectionViewModel> GetQuickBooksSection(CancellationToken cancellationToken)
    {
        var features = await Mediator.Send(new GetConfigurationQuery(), cancellationToken);
        if (!features.HasFeatureEnabled(FeaturesNames.Features.QuickBooksPlatform))
        {
            return null;
        }

        var quickbooksTooltipDialog = new GenericDialogRichBodyViewModel()
        {
            ImageUrl = "https://b9prodaccount.blob.core.windows.net/static-documents-container/Misc/quickbooks_dialog_tooltip.png",
            Buttons = new[]
            {
                new ButtonViewModel ("YES", ButtonStyleEnum.Solid)
                    .WithDeeplink(DeepLinkRoutes.QuickBooksInfo)
                    .EmitsUserEvent("yes-button", null, null),
                new ButtonViewModel("NO", ButtonStyleEnum.Bordered)
                    .WithDeeplink(DeepLinkRoutes.QuickBooksInfo)
                    .EmitsUserEvent("no-button", null, null)
            },
            Subtitle = new LinkedText("Does your company use QuickBooks to manage their payroll?")
        };

        return new QuickBooksSectionViewModel()
        {
            Title = "We work with QuickBooks",
            Subtitle = "Tap below to see how",
            Button = new ButtonWithRichDialogViewModel("LEARN MORE", ButtonStyleEnum.Solid, quickbooksTooltipDialog)
                .WithDeeplink(DeepLinkRoutes.QuickBooksDialog),
            ImageUrl = "https://b9prodaccount.blob.core.windows.net/static-documents-container/Misc/quickbooks_mini.png"

        };

    }

    private void AddTooltips(IncomeAndTaxesScreenViewModel viewModel)
    {
        viewModel.IncomeTaxesTooltipDialog = new GenericDialogRichBodyViewModel
        {
            Title = "Income & Taxes",
            Subtitle = new LinkedText(
                "This section shows information about all of your employers and work income transferred to your B9 Account and on which your Advance limit depends.\n" +
                "A service to fill your taxes is also available here."),
            Buttons = new[] { new ButtonViewModel("GOT IT", ButtonStyleEnum.Solid), },
            ImageUrl = "https://b9prodaccount.blob.core.windows.net/static-documents-container/Misc/income_and_taxes_tooltip.png",
        };

        viewModel.SourceIncomeTooltipDialog = new GenericDialogRichBodyViewModel
        {
            Title = "Source of income",
            Subtitle = new LinkedText(
                "Is a source of work-related income and will be used for B9 Advance limit calculation.\n" +
                "Do you have an extra income source besides a paycheck? Gov. benefits, freelance, rental properties, etc. You can connect these online or highlight in your transactions list."),
            Buttons = new[]
            {
                new ButtonViewModel("CONNECT INCOME SOURCE ONLINE", ButtonStyleEnum.Solid)
                    .WithDeeplink(DeepLinkRoutes.Argyle)
                    .EmitsUserEvent(
                        "connect-online",
                        "EmploymentMode-SourceIncomeHint-Argyle",
                        null),
                new ButtonViewModel("MARK TRANSACTIONS AS INCOME", ButtonStyleEnum.Bordered)
                    .WithDeeplink(DeepLinkRoutes.MarkPotentialIncome)
                    .EmitsUserEvent("mark-transactions", null, null),
            },
        };

        viewModel.AddIncomeTooltipDialog = new GenericDialogRichBodyViewModel
        {
            Title = "Got another source of Income?",
            Subtitle =
                new LinkedText(
                    "Let us know so we can increase your Advance limit."),
            Buttons = new[]
            {
                new ButtonViewModel("SWITCH IT TO B9 ONLINE", ButtonStyleEnum.Solid)
                    .WithDeeplink(DeepLinkRoutes.Argyle)
                    .EmitsUserEvent(
                        "online-deposit",
                        "EmploymentMode-AddIncomeButton-Argyle",
                        null),
                new ButtonViewModel("SWITCH IT TO B9 MANUALLY", ButtonStyleEnum.Solid)
                    .WithDeeplink(DeepLinkRoutes.EarlySalaryManualForm)
                    .EmitsUserEvent("manual-deposit", null, null),
                new ButtonViewModel("TELL US ABOUT YOUR NEW GIG", ButtonStyleEnum.Solid)
                    .WithDeeplink(DeepLinkRoutes.UserQuestionnaire,
                        new { questionSet = FeaturesNames.Settings.Questionnaire.Quizzes.SourceOfIncome })
                    .EmitsUserEvent("user-questionnaire", null, null),

            },
        };
    }
}
