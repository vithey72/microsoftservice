﻿namespace BNine.Application.Aggregates.IncomeAndTaxesScreen.Queries;

using MediatR;
using Models;

public record GetIncomeAndTaxesScreenQuery : IRequest<IncomeAndTaxesScreenViewModel>;
