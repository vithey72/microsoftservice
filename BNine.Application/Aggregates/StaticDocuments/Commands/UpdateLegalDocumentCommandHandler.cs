﻿namespace BNine.Application.Aggregates.StaticDocuments.Commands;

using Abstractions;
using AutoMapper;
using Domain.Entities.Settings;
using Exceptions;
using Interfaces;
using Interfaces.LegalDocuments;
using MediatR;
using Microsoft.EntityFrameworkCore;

public class UpdateLegalDocumentCommandHandler:
    AbstractRequestHandler, IRequestHandler<UpdateLegalDocumentCommand, Unit>
{
    private readonly ILegalDocumentsStorageService _legalDocumentsStorageService;

    public UpdateLegalDocumentCommandHandler(
        ILegalDocumentsStorageService legalDocumentsStorageService,
        IMediator mediator,
        IBNineDbContext dbContext, IMapper mapper, ICurrentUserService currentUser)
        : base(mediator, dbContext, mapper, currentUser)
    {
        _legalDocumentsStorageService = legalDocumentsStorageService;
    }

    public async Task<Unit> Handle(UpdateLegalDocumentCommand request, CancellationToken cancellationToken)
    {
        var document = await DbContext.StaticDocuments
            .FirstOrDefaultAsync(x => x.Key == request.DocumentKey,
                cancellationToken: cancellationToken);
        if (document == null)
        {
            throw new NotFoundException(nameof(StaticDocument), request.DocumentKey);
        }
        var updatedDynamicUrl = await
            _legalDocumentsStorageService.UploadLegalDocument(document, request, cancellationToken);

        document.StoragePath = updatedDynamicUrl;
        await DbContext.SaveChangesAsync(cancellationToken);
        return Unit.Value;
    }


}
