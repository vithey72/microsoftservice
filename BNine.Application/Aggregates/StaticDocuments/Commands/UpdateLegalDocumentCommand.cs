﻿namespace BNine.Application.Aggregates.StaticDocuments.Commands;

using MediatR;

public class UpdateLegalDocumentCommand: IRequest<Unit>
{
    public string DocumentKey
    {
        get;
    }

    public byte[] FileData
    {
        get;
        set;
    }

    public string ContentType
    {
        get;
    }

    public string FileExtension
    {
        get;
    }

    public UpdateLegalDocumentCommand(string documentKey,
        string contentType,
        string fileExtension)
    {
        DocumentKey = documentKey;
        ContentType = contentType;
        FileExtension = fileExtension;
    }
}
