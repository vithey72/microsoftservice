﻿namespace BNine.Application.Aggregates.StaticDocuments.Commands;

using FluentValidation;

public class UpdateLegalDocumentValidator : AbstractValidator<UpdateLegalDocumentCommand>
{
    public UpdateLegalDocumentValidator()
    {
        RuleFor(x => x.FileExtension)
            .Must(ext => ext == ".pdf")
            .WithMessage("The document should be a pdf file");
    }
}
