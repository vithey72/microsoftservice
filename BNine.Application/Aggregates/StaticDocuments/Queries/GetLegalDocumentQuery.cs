﻿namespace BNine.Application.Aggregates.StaticDocuments.Queries;

using Configuration.Models;
using MediatR;

public class GetLegalDocumentQuery : IRequest<DocumentResponseModel>
{
    public string DocumentKey
    {
        get;
        set;
    }
}
