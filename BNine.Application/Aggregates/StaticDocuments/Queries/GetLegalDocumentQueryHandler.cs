﻿namespace BNine.Application.Aggregates.StaticDocuments.Queries;

using Abstractions;
using AutoMapper;
using Configuration.Models;
using Interfaces;
using MediatR;
using Microsoft.EntityFrameworkCore;

public class GetLegalDocumentQueryHandler: AbstractRequestHandler, IRequestHandler<GetLegalDocumentQuery, DocumentResponseModel>
{
    public GetLegalDocumentQueryHandler(
        IMediator mediator, IBNineDbContext dbContext, IMapper mapper,
        ICurrentUserService currentUser) : base(mediator, dbContext, mapper, currentUser)
    {
    }

    public async Task<DocumentResponseModel> Handle(
        GetLegalDocumentQuery request, CancellationToken cancellationToken)
    {
        var document = await DbContext.StaticDocuments.FirstOrDefaultAsync(x => x.Key == request.DocumentKey);

        var documentViewModel = Mapper.Map<DocumentResponseModel>(document);

        return documentViewModel;
    }
}
