﻿namespace BNine.Application.Aggregates.StaticDocuments.Queries;

using Abstractions;
using AutoMapper;
using Configuration.Models;
using Interfaces;
using MediatR;
using Microsoft.EntityFrameworkCore;

public class GetLegalDocumentsQueryHandler: AbstractRequestHandler, IRequestHandler<GetLegalDocumentsQuery, List<DocumentResponseModel>>
{
    public GetLegalDocumentsQueryHandler(
        IMediator mediator, IBNineDbContext dbContext, IMapper mapper,
        ICurrentUserService currentUser) : base(mediator, dbContext, mapper, currentUser)
    {
    }

    public async Task<List<DocumentResponseModel>> Handle(
        GetLegalDocumentsQuery request, CancellationToken cancellationToken)
    {
        var documents = await DbContext.StaticDocuments.ToListAsync();

        var documentsViewModel = Mapper.Map<List<DocumentResponseModel>>(documents);

        return documentsViewModel;
    }
}
