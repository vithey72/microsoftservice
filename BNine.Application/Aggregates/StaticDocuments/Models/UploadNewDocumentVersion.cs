﻿namespace BNine.Application.Aggregates.StaticDocuments.Models;

using Microsoft.AspNetCore.Http;

public class UploadNewDocumentVersion
{
    public IFormFile File
    {
        get;
        set;
    }

    public string DocumentKey
    {
        get;
        set;
    }



}
