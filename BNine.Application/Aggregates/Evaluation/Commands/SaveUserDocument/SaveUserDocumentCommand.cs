﻿namespace BNine.Application.Aggregates.Evaluation.Commands.SaveUserDocument
{
    using AutoMapper;
    using BNine.Application.Extensions;
    using BNine.Application.Mappings;
    using BNine.Domain.Entities.User;
    using Enums;
    using Helpers;
    using MediatR;
    using Newtonsoft.Json;

    public class SaveUserDocumentCommand : IRequest<Unit>, IMapTo<Document>
    {
        [JsonIgnore]
        public Guid UserId
        {
            get;
            set;
        }

        [JsonProperty("documentScansExternalId")]
        public Guid DocumentScansExternalId
        {
            get;
            set;
        }

        /// <summary>
        /// Camelcase value from here <see cref="DocumentType"/>.
        /// </summary>
        [JsonProperty("proofOfIdentityKey")]
        public string ProofOfIdentityKey
        {
            get; set;
        }

        [JsonProperty("documentNumber")]
        public string DocumentNumber
        {
            get; set;
        }

        [JsonProperty("issuingState")]
        public string IssuingState
        {
            get; set;
        }

        [JsonProperty("issuingCountry")]
        public string IssuingCountry
        {
            get; set;
        }

        public void Mapping(Profile profile) => profile.CreateMap<SaveUserDocumentCommand, Document>()
            .ForMember(d => d.OcrProvider, o => o.MapFrom(s => EvaluationProviderEnum.Socure))
            .ForMember(d => d.DocumentScansExternalId, o => o.MapFrom(s => s.DocumentScansExternalId))
            .ForMember(d => d.Number, o => o.MapFrom(s => s.DocumentNumber))
            .ForMember(d => d.IssuingCountry, o => o.MapFrom(s => s.IssuingCountry))
            .ForMember(d => d.IssuingState, o => o.MapFrom(s => s.IssuingState))
            .ForMember(d => d.TypeKey, o => o.MapFrom(s => ClientDocumentsHelper.GetKey(EnumExtensions.ParseByEnumMemberAttribute<DocumentType>(s.ProofOfIdentityKey))));
    }
}
