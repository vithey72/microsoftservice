﻿namespace BNine.Application.Aggregates.Evaluation.Commands.SaveUserDocument;

using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using BNine.Application.Aggregates.ServiceBusEvents.Commands.PublishB9Event;
using Exceptions;
using Constants;
using Domain.Entities.User;
using BNine.Domain.Events.Users;
using Configuration.Queries.GetConfiguration;
using Enums;
using Interfaces;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;

public class SaveUserDocumentCommandHandler : IRequestHandler<SaveUserDocumentCommand>
{
    private readonly IMediator _mediator;
    private readonly IBNineDbContext _dbContext;
    private readonly IMapper _mapper;
    private readonly IPartnerProviderService _partnerProviderService;

    public SaveUserDocumentCommandHandler(
        IMediator mediator,
        IBNineDbContext dbContext,
        IMapper mapper,
        IPartnerProviderService partnerProviderService
        )
    {
        _mediator = mediator;
        _dbContext = dbContext;
        _mapper = mapper;
        _partnerProviderService = partnerProviderService;
    }

    public async Task<Unit> Handle(SaveUserDocumentCommand request, CancellationToken cancellationToken)
    {
        if (request.DocumentScansExternalId == Guid.Empty)
        {
            throw new ValidationException("documentScansExternalId", "Please provide document scan IDs");
        }

        var user = await _dbContext.Users
            .Include(x => x.Document)
            .FirstOrDefaultAsync(x => x.Id == request.UserId,
                cancellationToken);

        if (user == null)
        {
            throw new NotFoundException(nameof(User), request.UserId);
        }

        user.Document = _mapper.Map(request, user.Document);

        if (user.Status == UserStatus.PendingDocumentVerification)
        {
            var features = await _mediator.Send(new GetConfigurationQuery(request.UserId),
                cancellationToken);

            var partner = _partnerProviderService.GetPartner();

            if (features.HasFeatureEnabled(FeaturesNames.Features.SelfieOnboarding) && partner is PartnerApp.Default or PartnerApp.MBanqApp)
            {
                user.StatusDetailed = UserStatusDetailed.PendingSelfie;
            }

            user.Status = UserStatus.PendingSummary;
            await SendStatusChangeEvent(cancellationToken, user);
        }

        user.DomainEvents.Add(new OnboardingStepSuccessEvent(user, UserStatus.PendingDocumentVerification, UserStatusDetailed.PendingDocumentVerification));
        await _dbContext.SaveChangesAsync(cancellationToken);

        await _mediator.Send(new PublishB9EventCommand(
                user,
                ServiceBusTopics.B9User,
                ServiceBusEvents.B9UserDocumentsCreated),
            cancellationToken);

        return Unit.Value;
    }

    private async Task SendStatusChangeEvent(CancellationToken cancellationToken, User user)
    {
        await _mediator.Send(new PublishB9EventCommand(
            user,
            ServiceBusTopics.B9User,
            ServiceBusEvents.B9Updated,
            new Dictionary<string, object>
            {
                {
                    ServiceBusDefaults.UpdatedFieldsAdditionalProperty,
                    JsonConvert.SerializeObject(new List<string> { nameof(user.Status) })
                }
            }), cancellationToken);
    }
}
