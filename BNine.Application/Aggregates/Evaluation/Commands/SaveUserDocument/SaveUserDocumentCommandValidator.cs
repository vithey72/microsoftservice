﻿namespace BNine.Application.Aggregates.Evaluation.Commands.SaveUserDocument;

using Extensions;
using Interfaces;
using BNine.Application.Models.Validators;
using Enums;
using FluentValidation;

public class SaveUserDocumentCommandValidator : AbstractOnboardingStepValidator<SaveUserDocumentCommand>
{
    public SaveUserDocumentCommandValidator(IBNineDbContext dbContext, ICurrentUserService currentUserService) : base(dbContext, currentUserService)
    {
        Step = UserStatus.PendingDocumentVerification;

        RuleFor(x => x.ProofOfIdentityKey)
            .Must(x => EnumExtensions.TryParseByEnumMemberAttribute(x, out DocumentType _))
            .WithMessage("Unexpected document type");
    }
}
