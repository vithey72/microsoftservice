﻿namespace BNine.Application.Aggregates.Evaluation.Commands;

using Newtonsoft.Json;

public class SocureTransactionRequest
{
    [JsonProperty("modules")]
    public string[] Modules
    {
        get; set;
    } = {
            "kyc",
            "emailrisk",
            "addressrisk",
            "documentverification",
            "alertlist",
            "decision",
            "phonerisk",
            "social",
            "fraud",
            "watchliststandard",
            "devicerisk",
        };
    [JsonProperty("firstName")]
    public string FirstName
    {
        get;
        set;
    }

    [JsonProperty("deviceSessionId")]
    public string DeviceSessionId
    {
        get;
        set;
    }

    [JsonProperty("surName")]
    public string LastName
    {
        get;
        set;
    }

    [JsonProperty("dob")]
    public string DateOfBirth
    {
        get;
        set;
    }

    /// <summary>
    /// SSN or ITIN.
    /// </summary>
    [JsonProperty("nationalId")]
    public string NationalId
    {
        get;
        set;
    }

    [JsonProperty("email")]
    public string Email
    {
        get;
        set;
    }

    [JsonProperty("mobileNumber")]
    public string MobileNumber
    {
        get; set;
    }

    [JsonProperty("physicalAddress")]
    public string PhysicalAddress
    {
        get;
        set;
    }

    [JsonProperty("physicalAddress2")]
    public string PhysicalAddress2
    {
        get;
        set;
    }

    [JsonProperty("city")]
    public string City
    {
        get;
        set;
    }

    /// <summary>
    /// Two letter state abbreviation. Uppercase.
    /// </summary>
    [JsonProperty("state")]
    public string State
    {
        get;
        set;
    }

    [JsonProperty("zip")]
    public string Zip
    {
        get;
        set;
    }

    [JsonProperty("country")]
    public string Country
    {
        get;
        set;
    } = "us";

    /// <summary>
    /// TODO: add
    /// </summary>
    [JsonProperty("ip_address_v4")]
    public string IpAddress
    {
        get;
        set;
    }

    [JsonProperty("userConsent")]
    public bool UserConsent
    {
        get;
        set;
    } = true;

    [JsonProperty("consentTimestamp")]
    public string ConsentTimestamp
    {
        get;
        set;
    } = DateTime.Now.ToUniversalTime().ToString("o");

    [JsonProperty("documentUuid")]
    public Guid DocumentId
    {
        get;
        set;
    }

    /// <summary>
    /// Driver license number.
    /// </summary>
    [JsonProperty("driverLicense")]
    public string DriverLicense
    {
        get;
        set;
    }

    /// <summary>
    /// Driver license two letter state abbreviation. Uppercase.
    /// </summary>
    [JsonProperty("driverLicenseState")]
    public string DriverLicenseState
    {
        get;
        set;
    }
}
