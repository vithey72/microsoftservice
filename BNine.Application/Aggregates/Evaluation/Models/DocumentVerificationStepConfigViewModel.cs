﻿namespace BNine.Application.Aggregates.Evaluation.Models;

public class DocumentVerificationStepConfigViewModel
{
    public string HeaderTitle
    {
        get;
        set;
    }

    public string HeaderSubtitle
    {
        get;
        set;
    }

    public string BulletListTitle
    {
        get;
        set;
    }

    public List<string> BulletPoints
    {
        get;
        set;
    }

    public string SelfieTitle
    {
        get;
        set;
    }

    public string SelfieSubtitle
    {
        get;
        set;
    }

    public bool IsSelfieBlockVisible
    {
        get;
        set;
    }
}
