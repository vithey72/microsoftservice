﻿namespace BNine.Application.Aggregates.Evaluation.Models;

using Enums;

public class SocureClientDocumentModel
{
    public string FileName
    {
        get;
        set;
    }

    public DocumentSide DocumentSide
    {
        get;
        set;
    }
}
