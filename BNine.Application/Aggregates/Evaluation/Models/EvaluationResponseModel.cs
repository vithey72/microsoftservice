﻿namespace BNine.Application.Aggregates.Evaluation.Models
{
    public class EvaluationResponseModel
    {
        public string StatusCode
        {
            get; set;
        }

        public object EvaluationResponse
        {
            get; set;
        }

    }
}
