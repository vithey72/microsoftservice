﻿namespace BNine.Application.Aggregates.Evaluation.Models
{
    using System.Collections.Generic;
    using Newtonsoft.Json;
    using Newtonsoft.Json.Linq;

    public class EvaluationSuccessResponse
    {
        [JsonProperty("status_code")]
        public string StatusCode
        {
            get; set;
        }

        [JsonProperty("evaluation_token")]
        public string EvaluationToken
        {
            get; set;
        }

        [JsonProperty("entity_token")]
        public string EntityToken
        {
            get; set;
        }

        [JsonProperty("summary")]
        public Summary Summary
        {
            get; set;
        }

        [JsonProperty("raw_responses")]
        public Dictionary<string, JObject[]> RawResponses
        {
            get; set;
        }

        [JsonIgnore]
        public string RawResult
        {
            get; set;
        }

        [JsonProperty("formatted")]
        public AddressFromAlloy FormattedAddress
        {
            get; set;
        }
    }

    public class Summary
    {
        [JsonProperty("result")]
        public string Result
        {
            get; set;
        }

        [JsonProperty("outcome")]
        public string OutCome
        {
            get; set;
        }

        [JsonProperty("services")]
        public Dictionary<string, string> Services
        {
            get; set;
        }
    }

    public class AddressFromAlloy
    {
        [JsonProperty("address_line_1")]
        public string AddressLine1
        {
            get; set;
        }
        [JsonProperty("address_line_2")]
        public string AddressLine2
        {
            get; set;
        }
        [JsonProperty("address_city")]
        public string City
        {
            get; set;
        }
        [JsonProperty("address_state")]
        public string StateCode
        {
            get; set;
        }
        [JsonProperty("address_postal_code")]
        public string PostalCode
        {
            get; set;
        }
        [JsonProperty("address_country_code")]
        public string CountryCode
        {
            get; set;
        }

    }
}
