﻿namespace BNine.Application.Aggregates.Evaluation.Models;

using Enums;
using Newtonsoft.Json;

/// <summary>
/// TODO: add more fields for logging/debug reasons.
/// </summary>
public class SocureTransactionResponse
{
    public const string ApprovedStatus = "accept";
    public const string DeniedStatus = "reject";
    public const string ResubmitStatus = "resubmit";

    /// <summary>
    /// Trace id in their system.
    /// </summary>
    [JsonProperty("referenceId")]
    public Guid ReferenceId
    {
        get;
        set;
    }

    [JsonProperty("decision")]
    public SocureDecision Decision
    {
        get;
        set;
    }

    [JsonProperty("documentVerification")]
    public DocumentVerification DocVerification
    {
        get;
        set;
    }

    [JsonIgnore]
    public KycStatusEnum DecisionEnum => ParseDecision(Decision.Value);

    [JsonProperty("deviceData")]
    public SocureDeviceData DeviceData
    {
        get;
        set;
    } = new();

    /// <summary>
    /// Json of response.
    /// </summary>
    [JsonIgnore]
    public string RawResult
    {
        get; set;
    }

    public class SocureDecision
    {
        [JsonProperty("value")]
        public string Value
        {
            get; set;
        }
    }

    private static KycStatusEnum ParseDecision(string rawValue) =>
        rawValue switch
        {
            ApprovedStatus => KycStatusEnum.Approved,
            DeniedStatus => KycStatusEnum.Denied,
            ResubmitStatus => KycStatusEnum.Resubmit,
            _ => KycStatusEnum.Unknown,
        };

    public class DocumentVerification
    {
        public SocureDecision Decision
        {
            get;
            set;
        }

        [JsonIgnore]
        public KycStatusEnum DecisionEnum => ParseDecision(Decision.Value);

        [JsonProperty("reasonCodes")]
        public string[] ReasonCodes
        {
            get;
            set;
        }

        [JsonProperty("documentType")]
        public DocumentType DocType
        {
            get;
            set;
        }

        [JsonProperty("documentData")]
        public DocumentData DocData
        {
            get;
            set;
        }

        public class DocumentData
        {
            [JsonProperty("firstName")]
            public string FirstName
            {
                get; set;
            }

            [JsonProperty("surName")]
            public string LastName
            {
                get; set;
            }

            [JsonProperty("fullName")]
            public string FullName
            {
                get; set;
            }

            [JsonProperty("address")]
            public string Address
            {
                get; set;
            }

            [JsonProperty("documentNumber")]
            public string DocumentNumber
            {
                get; set;
            }

            [JsonProperty("dob")]
            public string DateOfBirth
            {
                get; set;
            }

            [JsonProperty("expirationDate")]
            public string ExpirationDate
            {
                get; set;
            }

            [JsonProperty("issueDate")]
            public string IssueDate
            {
                get; set;
            }
        }

        public class DocumentType
        {
            public string Type
            {
                get;
                set;
            }

            [JsonIgnore]
            public Enums.DocumentType TypeParsed
            {
                get
                {
                    if (Type.Contains("Drivers License") || Type.Contains("Learners Permit"))
                    {
                        return IsUsaIssued()
                            ? Enums.DocumentType.DriverLicense
                            : Enums.DocumentType.ForeignDriverLicense;
                    }

                    if (Type.Contains("Identification Card"))
                    {
                        return Enums.DocumentType.StateIdentityCard;
                    }

                    if (Type.Contains("Passport"))
                    {
                        return IsUsaIssued()
                            ? Enums.DocumentType.Passport
                            : Enums.DocumentType.ForeignPassport;
                    }

                    if (Type.Contains("Visa"))
                    {
                        return Enums.DocumentType.Visa;
                    }

                    return Enums.DocumentType.Other;
                }

            }

            private bool IsUsaIssued()
            {
                return Country is "US" or "USA";
            }

            /// <summary>
            /// In ISO 3166-1 alpha-2 format.
            /// </summary>
            public string Country
            {
                get;
                set;
            }

            public string State
            {
                get;
                set;
            }
        }
    }

    public class SocureDeviceData
    {
        [JsonProperty("information")]
        public DeviceInformation DeviceInformation
        {
            get;
            set;
        }

        public bool IsAndroid() => DeviceInformation?.OperatingSystem is "Android" or null;

        public bool IsIos() => DeviceInformation?.OperatingSystem is "iOS";
    }

    public class DeviceInformation
    {
        [JsonProperty("deviceModelNumber")]
        public string DeviceModelNumber
        {
            get;
            set;
        }

        [JsonProperty("deviceManufacturer")]
        public string DeviceManufacturer
        {
            get;
            set;
        }

        [JsonProperty("operatingSystem")]
        public string OperatingSystem
        {
            get;
            set;
        }

        [JsonProperty("operatingSystemVersion")]
        public string OperatingSystemVersion
        {
            get;
            set;
        }
    }
}
