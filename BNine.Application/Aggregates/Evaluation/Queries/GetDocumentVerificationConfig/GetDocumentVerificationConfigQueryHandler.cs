﻿namespace BNine.Application.Aggregates.Evaluation.Queries.GetDocumentVerificationConfig;

using Abstractions;
using AutoMapper;
using Configuration.Queries.GetConfiguration;
using Constants;
using Interfaces;
using MediatR;
using Models;

public class GetDocumentVerificationConfigQueryHandler : AbstractRequestHandler,
    IRequestHandler<GetDocumentVerificationStepConfigQuery, DocumentVerificationStepConfigViewModel>
{
    public GetDocumentVerificationConfigQueryHandler(IMediator mediator, IBNineDbContext dbContext, IMapper mapper,
        ICurrentUserService currentUser) : base(mediator, dbContext, mapper, currentUser)
    {
    }

    public async Task<DocumentVerificationStepConfigViewModel> Handle(GetDocumentVerificationStepConfigQuery request,
        CancellationToken cancellationToken)
    {
        var configuration = await Mediator.Send(new GetConfigurationQuery(), cancellationToken);

        var configViewModel = new DocumentVerificationStepConfigViewModel()
        {
            HeaderTitle = "Proof of identity",
            HeaderSubtitle = "Time to scan your government issued ID, which helps to secure your account and payments.",
            BulletListTitle = "Please prepare the ID, you can use any of:",
            BulletPoints =
                new List<string>() { "Valid US Driver's License", "Valid US Identification Card", "US Passport" },
            SelfieTitle = "Please prepare to get a selfie:",
            SelfieSubtitle = "The photo should be clear",
            IsSelfieBlockVisible = false
        };

        if (configuration.HasFeatureEnabled(FeaturesNames.Features.SelfieOnboarding))
        {
            configViewModel.IsSelfieBlockVisible = true;
            configViewModel.HeaderSubtitle =
                $"Time to scan your government issued ID and get a selfie, which helps to secure your account and payments.";
        }

        return configViewModel;
    }
}
