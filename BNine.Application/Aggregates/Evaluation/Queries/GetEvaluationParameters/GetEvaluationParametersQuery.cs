﻿namespace BNine.Application.Aggregates.Evaluation.Queries.GetEvaluationParameters
{
    using MediatR;

    public class GetEvaluationParametersQuery : IRequest<string>
    {
    }
}
