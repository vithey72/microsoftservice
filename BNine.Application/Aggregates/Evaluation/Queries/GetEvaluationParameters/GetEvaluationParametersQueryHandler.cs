﻿namespace BNine.Application.Aggregates.Evaluation.Queries.GetEvaluationParameters
{
    using System.Threading;
    using System.Threading.Tasks;
    using AutoMapper;
    using BNine.Application.Abstractions;
    using BNine.Application.Interfaces;
    using Interfaces.Alloy;
    using MediatR;

    public class GetEvaluationParametersQueryHandler
        : AbstractRequestHandler,
            IRequestHandler<GetEvaluationParametersQuery, string>
    {
        private readonly IDocsAlloyOnboardingService _docsAlloyOnboarding;
        public GetEvaluationParametersQueryHandler(
            IMediator mediator,
            IBNineDbContext dbContext,
            IMapper mapper,
            ICurrentUserService currentUser,
            IDocsAlloyOnboardingService docsAlloyOnboarding)
            : base(mediator, dbContext, mapper, currentUser)
        {
            _docsAlloyOnboarding = docsAlloyOnboarding;
        }

        public async Task<string> Handle(GetEvaluationParametersQuery request, CancellationToken cancellationToken)
        {
            var response = await _docsAlloyOnboarding.GetEvaluationParameters();

            return response;
        }
    }
}
