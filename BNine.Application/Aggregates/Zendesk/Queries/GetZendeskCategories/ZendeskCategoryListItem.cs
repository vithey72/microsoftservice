﻿namespace BNine.Application.Aggregates.Zendesk.Queries.GetZendeskCategories
{
    using System.Linq;
    using AutoMapper;
    using BNine.Application.Mappings;
    using BNine.Domain.ListItems;
    using BNine.Enums;
    using Newtonsoft.Json;

    public class ZendeskCategoryListItem : IMapFrom<ZendeskCategory>
    {
        [JsonProperty("value")]
        public string Value
        {
            get; set;
        }

        [JsonProperty("tag")]
        public string Tag
        {
            get; set;
        }

        [JsonProperty("default")]
        public bool Default
        {
            get; set;
        } = false;

        public void Mapping(Profile profile)
        {
            var language = Language.English;

            profile.CreateMap<ZendeskCategory, ZendeskCategoryListItem>()
                .ForMember(d => d.Tag, opt => opt.MapFrom(s => s.Key))
                .ForMember(d => d.Value, opt => opt.MapFrom(s => s.Value.Where(x => x.Language == language).First().Text));
        }
    }
}
