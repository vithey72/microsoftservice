﻿namespace BNine.Application.Aggregates.Zendesk.Queries.GetZendeskCategories
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading;
    using System.Threading.Tasks;
    using AutoMapper;
    using AutoMapper.QueryableExtensions;
    using BNine.Application.Abstractions;
    using BNine.Application.Interfaces;
    using Helpers;
    using MediatR;
    using Microsoft.EntityFrameworkCore;

    public class GetZendeskCategoriesQueryHandler
        : AbstractRequestHandler, IRequestHandler<GetZendeskCategoriesQuery, IEnumerable<ZendeskCategoryListItem>>
    {
        public GetZendeskCategoriesQueryHandler(IMediator mediator, IBNineDbContext dbContext, IMapper mapper, ICurrentUserService currentUser) : base(mediator, dbContext, mapper, currentUser)
        {
        }

        public async Task<IEnumerable<ZendeskCategoryListItem>> Handle(GetZendeskCategoriesQuery request, CancellationToken cancellationToken)
        {
            var language = LocalizationHelper.GetLanguage(request.CultureInfo);

            var categories = await DbContext.ZendeskCategories
                .OrderBy(x => x.Order)
                .ProjectTo<ZendeskCategoryListItem>(Mapper.ConfigurationProvider, new
                {
                    language
                })
                .ToListAsync(cancellationToken);

            return categories;
        }
    }
}
