﻿namespace BNine.Application.Aggregates.Zendesk.Queries.GetZendeskCategories
{
    using System.Collections.Generic;
    using System.Globalization;
    using MediatR;
    using Newtonsoft.Json;

    public class GetZendeskCategoriesQuery : IRequest<IEnumerable<ZendeskCategoryListItem>>
    {
        public GetZendeskCategoriesQuery(CultureInfo cultureInfo)
        {
            CultureInfo = cultureInfo;
        }

        [JsonIgnore]
        public CultureInfo CultureInfo
        {
            get; set;
        }
    }
}
