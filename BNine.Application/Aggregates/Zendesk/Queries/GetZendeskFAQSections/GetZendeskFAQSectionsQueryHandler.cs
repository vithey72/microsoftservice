﻿namespace BNine.Application.Aggregates.Zendesk.Queries.GetZendeskFAQSections
{
    using System.Threading;
    using System.Threading.Tasks;
    using AutoMapper;
    using BNine.Application.Abstractions;
    using BNine.Application.Interfaces;
    using BNine.Application.Interfaces.Zendesk;
    using BNine.Application.Models.Zendesk;
    using MediatR;

    public class GetZendeskFAQSectionsQueryHandler : AbstractRequestHandler, IRequestHandler<GetZendeskFAQSectionsQuery, FAQSections>
    {
        private IZendeskFaqService ZendeskFaqService
        {
            get; set;
        }

        public GetZendeskFAQSectionsQueryHandler(IZendeskFaqService zendeskFaqService, IMediator mediator, IBNineDbContext dbContext, IMapper mapper, ICurrentUserService currentUser)
            : base(mediator, dbContext, mapper, currentUser)
        {
            ZendeskFaqService = zendeskFaqService;
        }

        public async Task<FAQSections> Handle(GetZendeskFAQSectionsQuery request, CancellationToken cancellationToken)
        {
            return await ZendeskFaqService.GetFaqSections();
        }
    }
}
