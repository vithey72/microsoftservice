﻿namespace BNine.Application.Aggregates.Zendesk.Queries.GetZendeskFAQSections
{
    using BNine.Application.Models.Zendesk;
    using MediatR;

    public class GetZendeskFAQSectionsQuery : IRequest<FAQSections>
    {
    }
}
