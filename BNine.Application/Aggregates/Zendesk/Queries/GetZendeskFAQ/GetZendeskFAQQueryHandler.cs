﻿namespace BNine.Application.Aggregates.Zendesk.Queries.GetZendeskFAQ
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading;
    using System.Threading.Tasks;
    using AutoMapper;
    using BNine.Application.Abstractions;
    using BNine.Application.Interfaces;
    using BNine.Application.Interfaces.Zendesk;
    using Helpers;
    using MediatR;

    [Obsolete]
    public class GetZendeskFAQQueryHandler
        : AbstractRequestHandler, IRequestHandler<GetZendeskFAQQuery, IEnumerable<ZendeskFAQListItem>>
    {
        private IZendeskFaqService ZendeskFaqService
        {
            get; set;
        }

        public GetZendeskFAQQueryHandler(IZendeskFaqService zendeskFaqService, IMediator mediator, IBNineDbContext dbContext, IMapper mapper, ICurrentUserService currentUser) : base(mediator, dbContext, mapper, currentUser)
        {
            ZendeskFaqService = zendeskFaqService;
        }

        public async Task<IEnumerable<ZendeskFAQListItem>> Handle(GetZendeskFAQQuery request, CancellationToken cancellationToken)
        {
            var language = LocalizationHelper.GetLanguage(request.CultureInfo);

            var faq = await ZendeskFaqService.GetFaqArticles(language);

            return faq.Select(x => new ZendeskFAQListItem
            {
                Title = x.Title,
                Body = x.Body
            });
        }
    }
}
