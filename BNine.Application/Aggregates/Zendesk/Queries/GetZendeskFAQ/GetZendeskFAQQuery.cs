﻿namespace BNine.Application.Aggregates.Zendesk.Queries.GetZendeskFAQ
{
    using System.Collections.Generic;
    using System.Globalization;
    using System.Text.Json.Serialization;
    using MediatR;

    public class GetZendeskFAQQuery : IRequest<IEnumerable<ZendeskFAQListItem>>
    {
        public GetZendeskFAQQuery(CultureInfo cultureInfo)
        {
            CultureInfo = cultureInfo;
        }

        [JsonIgnore]
        public CultureInfo CultureInfo
        {
            get; set;
        }
    }
}
