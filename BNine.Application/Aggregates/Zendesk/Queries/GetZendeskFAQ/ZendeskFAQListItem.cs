﻿namespace BNine.Application.Aggregates.Zendesk.Queries.GetZendeskFAQ
{
    using Newtonsoft.Json;

    public class ZendeskFAQListItem
    {
        [JsonProperty("title")]
        public string Title
        {
            get; set;
        }

        [JsonProperty("body")]
        public string Body
        {
            get; set;
        }
    }
}
