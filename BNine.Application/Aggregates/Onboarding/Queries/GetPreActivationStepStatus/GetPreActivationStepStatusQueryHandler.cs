﻿namespace BNine.Application.Aggregates.Onboarding.Queries.GetPreActivationStepStatus;

using CommonModels.Dialog;
using Constants;
using Enums;
using Exceptions;
using Interfaces;
using Interfaces.Bank.Administration;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Models;

public class GetPreActivationStepStatusQueryHandler
    : IRequestHandler<GetPreActivationStepStatusQuery, PreActivationStepStatusViewModel>
{
    private readonly IBNineDbContext _dbContext;
    private readonly IPartnerProviderService _partnerProvider;
    private readonly ICurrentUserService _currentUserService;
    private readonly IBankChargesService _chargesService;

    public GetPreActivationStepStatusQueryHandler(
        IBNineDbContext dbContext,
        IPartnerProviderService partnerProvider,
        ICurrentUserService currentUserService,
        IBankChargesService chargesService
        )
    {
        _dbContext = dbContext;
        _partnerProvider = partnerProvider;
        _currentUserService = currentUserService;
        _chargesService = chargesService;
    }

    public async Task<PreActivationStepStatusViewModel> Handle(GetPreActivationStepStatusQuery request, CancellationToken token)
    {
        if (_partnerProvider.GetPartner() != PartnerApp.Qorbis)
        {
            return null;
        }

        var user = await _dbContext.Users
            .Include(u => u.CurrentAccount)
            .FirstOrDefaultAsync(x => x.Id == _currentUserService.UserId, token);
        if (user!.Status != UserStatus.PreActivation)
        {
            throw new ValidationException("status",
                $"Invalid user state: {user.Status}. Expected: {UserStatus.PreActivation}");
        }

        var tariffCharge = await _chargesService.GetActiveTariffCharge(user.CurrentAccount.ExternalId);
        if (tariffCharge.AmountPaid != 0) // subscription is already paid
        {
            return null;
        }

        var annualTariff = _dbContext.TariffPlans.FirstOrDefault(x => x.Id == PaidUserServices.Basic1Month);
        if (annualTariff == null)
        {
            throw new KeyNotFoundException($"Tariff with Id = {PaidUserServices.Basic1Month} is missing in the DB.");
        }

        return new PreActivationStepStatusViewModel
        {
            Header = new GenericDialogHeaderViewModel
            {
                Title = "Payment",
                Subtitle = "Enter cards details manually",
            },
            SubscriptionOptions = new[]
            {
                new PreActivationStepStatusViewModel.SubscriptionOption
                {
                    TariffId = annualTariff.Id,
                    Cost = annualTariff.TotalPrice,
                    Fee = null,
                    IsSelectedByDefault = true,
                }
            },
        };
    }
}
