﻿namespace BNine.Application.Aggregates.Onboarding.Queries.GetPreActivationStepStatus;

using MediatR;
using Models;

public record GetPreActivationStepStatusQuery : IRequest<PreActivationStepStatusViewModel>;
