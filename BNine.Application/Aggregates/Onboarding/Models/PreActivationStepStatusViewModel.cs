﻿namespace BNine.Application.Aggregates.Onboarding.Models;

using BNine.Application.Aggregates.CommonModels.Dialog;
using Helpers;

public class PreActivationStepStatusViewModel
{
    public GenericDialogHeaderViewModel Header
    {
        get;
        set;
    }

    public SubscriptionOption[] SubscriptionOptions
    {
        get;
        set;
    }

    public class SubscriptionOption
    {
        public Guid TariffId
        {
            get;
            set;
        }

        public string Header
        {
            get;
            set;
        }

        public decimal Cost
        {
            get;
            set;
        }

        public decimal? Fee
        {
            get;
            set;
        }

        public string ButtonText => $"PAY {CurrencyFormattingHelper.AsRegular(Cost)}";

        public bool IsSelectedByDefault
        {
            get;
            set;
        }
    }
}
