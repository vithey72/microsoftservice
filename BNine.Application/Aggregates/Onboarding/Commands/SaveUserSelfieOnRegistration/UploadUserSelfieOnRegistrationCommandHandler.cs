﻿namespace BNine.Application.Aggregates.Onboarding.Commands.SaveUserSelfieOnRegistration;

using System;
using System.Threading.Tasks;
using BNine.Application.Aggregates.Selfie.Commands;
using BNine.Domain.Events.Users;
using BNine.Enums;
using Interfaces;
using MediatR;
using Microsoft.EntityFrameworkCore;

public class UploadUserSelfieOnRegistrationCommandHandler : IRequestHandler<UploadUserSelfieOnRegistrationCommand, Unit>
{
    private readonly IMediator _mediator;
    private readonly IBNineDbContext _dbContext;

    public UploadUserSelfieOnRegistrationCommandHandler(
        IMediator mediator,
        IBNineDbContext dbContext
        )
    {
        _mediator = mediator;
        _dbContext = dbContext;
    }

    public async Task<Unit> Handle(UploadUserSelfieOnRegistrationCommand request, CancellationToken cancellationToken)
    {
        var user = await _dbContext
            .Users
            .Include(u => u.Settings)
            .SingleAsync(x => x.Id == request.UserId, cancellationToken);

        if (user.StatusDetailed != UserStatusDetailed.PendingSelfie)
        {
            return Unit.Value;
        }

        var command = new UploadSelfieWithTypeCommand
        {
            ContentType = request.SelfieFile.Image.ContentType,
            FileExtension = Path.GetExtension(request.SelfieFile.Image.FileName),
            SelfieSourceFlow = SelfieInitiatingFlow.Onboarding,
        };

        using (var memoryStream = new MemoryStream())
        {
            await request.SelfieFile.Image.CopyToAsync(memoryStream, cancellationToken);
            command.FileData = memoryStream.ToArray();
        }

        try
        {
            await _mediator.Send(command, cancellationToken);

            user.StatusDetailed = UserStatusDetailed.PendingSummary;
            user.DomainEvents.Add(new OnboardingStepSuccessEvent(
                user, UserStatus.PendingSummary, UserStatusDetailed.PendingSelfie));

            await _dbContext.SaveChangesAsync(cancellationToken);
        }
        catch (Exception ex)
        {
            user.DomainEvents.Add(new OnboardingStepFailedEvent(
                user, UserStatus.PendingSummary, UserStatusDetailed.PendingSelfie, new[] { ex.Message }));
            await _dbContext.SaveChangesAsync(cancellationToken);
            throw;
        }

        return Unit.Value;
    }
}
