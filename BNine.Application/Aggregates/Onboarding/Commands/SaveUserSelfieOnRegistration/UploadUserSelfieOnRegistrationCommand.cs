﻿namespace BNine.Application.Aggregates.Onboarding.Commands.SaveUserSelfieOnRegistration;

using BNine.Application.Aggregates.Selfie.Models;
using MediatR;
using Newtonsoft.Json;

public struct UploadUserSelfieOnRegistrationCommand : IRequest<Unit>
{
    public SelfieUpload SelfieFile
    {
        get;
        set;
    }

    [JsonIgnore]
    public Guid UserId
    {
        get;
        set;
    }
}
