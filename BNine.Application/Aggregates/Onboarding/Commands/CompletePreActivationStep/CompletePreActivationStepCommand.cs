﻿namespace BNine.Application.Aggregates.Onboarding.Commands.CompletePreActivationStep;

using MediatR;

public record CompletePreActivationStepCommand : IRequest<Unit>;
