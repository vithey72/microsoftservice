﻿namespace BNine.Application.Aggregates.Onboarding.Commands.CompletePreActivationStep;

using System;
using System.Threading.Tasks;
using Enums;
using Interfaces;
using Interfaces.Bank.Administration;
using MediatR;
using Microsoft.EntityFrameworkCore;

public class CompletePreActivationStepCommandHandler : IRequestHandler<CompletePreActivationStepCommand, Unit>
{
    private readonly IBNineDbContext _dbContext;
    private readonly ICurrentUserService _currentUserService;
    private readonly IPartnerProviderService _partnerProvider;
    private readonly IBankChargesService _chargesService;

    public CompletePreActivationStepCommandHandler(
        IBNineDbContext dbContext,
        ICurrentUserService currentUserService,
        IPartnerProviderService partnerProvider,
        IBankChargesService chargesService
        )
    {
        _dbContext = dbContext;
        _currentUserService = currentUserService;
        _partnerProvider = partnerProvider;
        _chargesService = chargesService;
    }

    public async Task<Unit> Handle(CompletePreActivationStepCommand request, CancellationToken cancellationToken)
    {
        var user = await _dbContext
            .Users
            .Include(u => u.CurrentAccount)
            .FirstOrDefaultAsync(x => x.Id == _currentUserService.UserId, cancellationToken);
        if (user.Status != UserStatus.PreActivation)
        {
            throw new InvalidOperationException($"Expected user in status: {UserStatus.PreActivation}");
        }

        var partner = _partnerProvider.GetPartner();
        if (partner == PartnerApp.Qorbis)
        {
            var charge = await _chargesService.GetActiveTariffCharge(user.CurrentAccount.ExternalId);
            if (charge.AmountPaid == 0)
            {
                throw new InvalidOperationException("User cannot complete this step without purchasing a tariff.");
            }
        }

        user.Status = UserStatus.Active;
        user.ActivatedAt = DateTime.UtcNow;

        await _dbContext.SaveChangesAsync(cancellationToken);

        return Unit.Value;
    }
}
