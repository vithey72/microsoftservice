﻿namespace BNine.Application.Aggregates.Features.Models;

using Domain.Entities.Features;

public class GetAllGroupsResponse
{
    public GetAllGroupsResponse(List<Group> groups)
    {
        Groups = groups;
    }

    public List<Group> Groups
    {
        get;
    }
}
