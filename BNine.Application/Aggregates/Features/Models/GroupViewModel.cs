﻿namespace BNine.Application.Aggregates.Features.Models;

using AutoMapper;
using Domain.Entities.Features;
using Mappings;

public class GroupViewModel : IMapFrom<Group>
{
    public Guid Id
    {
        get;
        set;
    }

    public string Name
    {
        get;
        set;
    }

    public GroupKind Kind
    {
        get;
        set;
    }

    public GroupMember[] Members
    {
        get;
        set;
    }

    public DateTime? CreatedAt
    {
        get;
        set;
    }

    public DateTime? UpdatedAt
    {
        get;
        set;
    }

    public record GroupMember(Guid Id, string FullName, DateTime? AddedAt);

    public void Mapping(Profile profile) => profile.CreateMap<Group, GroupViewModel>()
        .ForMember(d => d.Id, o => o.MapFrom(s => s.Id))
        .ForMember(d => d.Name, o => o.MapFrom(s => s.Name))
        .ForMember(d => d.Kind, o => o.MapFrom(s => s.Kind))
        .ForMember(d => d.CreatedAt, o => o.MapFrom(s => s.CreatedAt))
        .ForMember(d => d.UpdatedAt, o => o.MapFrom(s => s.UpdatedAt))
        .ForMember(d => d.Members, o => o.MapFrom(s => Convert(s)))
    ;

    private static IEnumerable<GroupMember> Convert(Group s)
    {
        var result = s.UserGroups?
            .OrderByDescending(ug => ug.CreatedAt)
            .Select(ug => new GroupMember(ug.User.Id, ug.User.FirstName + " " + ug.User.LastName, ug.CreatedAt))
            .ToList();

        return result;
    }
}
