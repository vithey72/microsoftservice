﻿namespace BNine.Application.Aggregates.Features.Models;

public record AddOrRemoveUsersFromGroupResponse(GroupViewModel Group, int Total, int Delta);
