﻿namespace BNine.Application.Aggregates.Features.Commands.DeleteGroup;

using MediatR;

public record DeleteGroupCommand(Guid GroupId) : IRequest<Unit>;
