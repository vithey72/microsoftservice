﻿namespace BNine.Application.Aggregates.Features.Commands.DeleteGroup;

using System.Threading.Tasks;
using Abstractions;
using AutoMapper;
using Exceptions;
using Interfaces;
using MediatR;
using Microsoft.EntityFrameworkCore;

public class DeleteGroupCommandHandler
    : AbstractRequestHandler
    , IRequestHandler<DeleteGroupCommand, Unit>
{
    public DeleteGroupCommandHandler(
        IMediator mediator,
        IBNineDbContext dbContext,
        IMapper mapper,
        ICurrentUserService currentUser
        )
        : base(mediator, dbContext, mapper, currentUser)
    {
    }

    public async Task<Unit> Handle(DeleteGroupCommand request, CancellationToken cancellationToken)
    {
        var group = await DbContext.Groups.FirstOrDefaultAsync(g => g.Id == request.GroupId, cancellationToken);
        if (group == null)
        {
            throw new NotFoundException($"Group with Id = {request.GroupId} does not exist.");
        }

        DbContext.Groups.Remove(group);

        await DbContext.SaveChangesAsync(cancellationToken);

        return Unit.Value;
    }
}
