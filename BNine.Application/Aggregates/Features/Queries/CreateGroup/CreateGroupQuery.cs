﻿namespace BNine.Application.Aggregates.Features.Queries.CreateGroup;

using Domain.Entities.Features;
using MediatR;
using Models;

public record CreateGroupQuery(string Name, GroupKind GroupType) : IRequest<GroupViewModel>;
