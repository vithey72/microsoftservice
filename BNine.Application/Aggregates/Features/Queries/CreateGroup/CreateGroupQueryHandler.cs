﻿namespace BNine.Application.Aggregates.Features.Queries.CreateGroup;

using Abstractions;
using AutoMapper;
using Domain.Entities.Features;
using Domain.Entities.Features.Groups;
using Interfaces;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Models;

public class CreateGroupQueryHandler
    : AbstractRequestHandler
    , IRequestHandler<CreateGroupQuery, GroupViewModel>
{
    public CreateGroupQueryHandler(
        IMediator mediator,
        IBNineDbContext dbContext,
        IMapper mapper,
        ICurrentUserService currentUser)
        : base(mediator, dbContext, mapper, currentUser)
    {
    }

    public async Task<GroupViewModel> Handle(CreateGroupQuery request, CancellationToken cancellationToken)
    {
        Group group = request.GroupType switch
        {
            GroupKind.UserList => new UserListGroup(),
            GroupKind.Personal => new PersonalGroup(),
            GroupKind.Sample => throw new NotImplementedException($"Group type '{GroupKind.Sample}' is not yet supported. {SupportedTypes()}"),
            _ => throw new InvalidOperationException($"Unsupported group type: '{request.GroupType}'. {SupportedTypes()}"),
        };

        var now = DateTime.UtcNow;
        group.CreatedAt = now;
        group.UpdatedAt = now;

        group.Name = request.Name;

        var existingGroup = await DbContext
            .Groups
            .FirstOrDefaultAsync(g => g.Name == request.Name, cancellationToken);
        if (existingGroup != null)
        {
            throw new InvalidOperationException($"Group with name {existingGroup.Name} already exists.");
        }

        await DbContext.Groups.AddAsync(group, cancellationToken);

        await DbContext.SaveChangesAsync(cancellationToken);

        return Mapper.Map<GroupViewModel>(group);
    }

    private string SupportedTypes() => $"Supported group types are '{GroupKind.UserList}' and '{GroupKind.Personal}'";
}
