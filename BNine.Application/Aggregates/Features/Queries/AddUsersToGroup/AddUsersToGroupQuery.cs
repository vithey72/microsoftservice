﻿namespace BNine.Application.Aggregates.Features.Queries.AddUsersToGroup;

using System;
using MediatR;
using Models;

public record AddUsersToGroupQuery(Guid GroupId, Guid[] UserIds) : IRequest<AddOrRemoveUsersFromGroupResponse>;
