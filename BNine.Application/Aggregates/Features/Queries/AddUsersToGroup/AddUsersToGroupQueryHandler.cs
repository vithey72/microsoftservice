﻿namespace BNine.Application.Aggregates.Features.Queries.AddUsersToGroup;

using System.Threading.Tasks;
using Abstractions;
using AutoMapper;
using Interfaces;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Models;

public class AddUsersToGroupQueryHandler
    : AbstractRequestHandler
        , IRequestHandler<AddUsersToGroupQuery, AddOrRemoveUsersFromGroupResponse>
{
    public AddUsersToGroupQueryHandler(
        IMediator mediator,
        IBNineDbContext dbContext,
        IMapper mapper,
        ICurrentUserService currentUser
    )
        : base(mediator, dbContext, mapper, currentUser)
    {
    }

    public async Task<AddOrRemoveUsersFromGroupResponse> Handle(AddUsersToGroupQuery request,
        CancellationToken cancellationToken)
    {
        var groupId = request.GroupId;
        var userIds = string.Join(",", request.UserIds);
        var group = await DbContext.Groups
            .Include(g => g.UserGroups)
            .FirstOrDefaultAsync(g => g.Id == groupId, cancellationToken);

        // Raw SQL MERGE statement
        // It's hard to find better way to pass in the list of guids to raw sql query without using TVP
        FormattableString sql = @$"MERGE INTO [features].[UserGroups] AS target
                USING
                (
                    SELECT
                {groupId} AS GroupId,
                    Id AS UserId
                FROM
                    [user].[Users] where [Id] in (select CONVERT(UNIQUEIDENTIFIER, LTRIM(RTRIM(value))) from STRING_SPLIT({userIds}, ','))
                    ) AS source
                ON
                    (target.GroupId = source.GroupId AND target.UserId = source.UserId)
                WHEN NOT MATCHED THEN
                INSERT (GroupId, UserId, CreatedAt, UpdatedAt)
                VALUES ({groupId}, source.UserId, GETUTCDATE(), GETUTCDATE());";


        var entriesCount = await DbContext.Database.ExecuteSqlInterpolatedAsync(sql, cancellationToken);
        var addedCount = entriesCount;
        var totalCount = group.UserGroups.Count + addedCount;

        var viewModel = new GroupViewModel
        {
            Id = group.Id,
            CreatedAt = group.CreatedAt,
            Kind = group.Kind,
            Name = group.Name,
            UpdatedAt = group.UpdatedAt,
        };

        return new AddOrRemoveUsersFromGroupResponse(viewModel, totalCount, addedCount);
    }
}
