﻿namespace BNine.Application.Aggregates.Features.Queries.UpdateGroup;

using System;
using MediatR;
using Models;

public record UpdateGroupCommand(Guid GroupId, string NewName, Guid[] UserIds) : IRequest<GroupViewModel>;
