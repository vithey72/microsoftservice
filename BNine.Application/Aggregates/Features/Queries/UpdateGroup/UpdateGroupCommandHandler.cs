﻿namespace BNine.Application.Aggregates.Features.Queries.UpdateGroup;

using System;
using System.Threading.Tasks;
using Abstractions;
using AutoMapper;
using Domain.Entities.Features;
using Interfaces;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Models;

public class UpdateGroupCommandHandler
    : AbstractRequestHandler
    , IRequestHandler<UpdateGroupCommand, GroupViewModel>
{
    public UpdateGroupCommandHandler(
        IMediator mediator,
        IBNineDbContext dbContext,
        IMapper mapper,
        ICurrentUserService currentUser
        ) : base(mediator, dbContext, mapper, currentUser)
    {
    }

    public async Task<GroupViewModel> Handle(UpdateGroupCommand request, CancellationToken cancellationToken)
    {
        var existingGroup = await DbContext
            .Groups
            .Include(g => g.UserGroups)
            .ThenInclude(ug => ug.User)
            .FirstOrDefaultAsync(x => x.Id == request.GroupId, cancellationToken);

        if (existingGroup == null)
        {
            throw new KeyNotFoundException($"Group with Id = {request.GroupId} is not present in DB.");
        }

        if (!string.IsNullOrEmpty(request.NewName))
        {
            existingGroup.Name = request.NewName;
        }

        if (request.UserIds != null)
        {
            existingGroup.UserGroups.RemoveAll(_ => true);
            await DbContext.SaveChangesAsync(cancellationToken);
            foreach (var requestUserId in request.UserIds)
            {
                var userGroup = new UserGroup
                {
                    UserId = requestUserId,
                    GroupId = existingGroup.Id,
                    CreatedAt = DateTime.UtcNow,
                };
                existingGroup.UserGroups.Add(userGroup);
            }
        }

        await DbContext.SaveChangesAsync(cancellationToken);
        return Mapper.Map<GroupViewModel>(existingGroup);
    }
}
