﻿namespace BNine.Application.Aggregates.Features.Queries.GetAllGroups;

using System.Linq;
using System.Threading.Tasks;
using Abstractions;
using AutoMapper;
using Interfaces;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Models;

public class GetAllGroupsQueryHandler
    : AbstractRequestHandler
    , IRequestHandler<GetAllGroupsQuery, GetAllGroupsResponse>
{
    public GetAllGroupsQueryHandler(
        IMediator mediator,
        IBNineDbContext dbContext,
        IMapper mapper,
        ICurrentUserService currentUser
        )
        : base(mediator, dbContext, mapper, currentUser)
    {
    }

    public async Task<GetAllGroupsResponse> Handle(GetAllGroupsQuery request, CancellationToken cancellationToken)
    {
        var groups = await DbContext.Groups
            .Where(g => request.GroupType == null
                        || g.Kind == request.GroupType)
            .ToListAsync(cancellationToken);

        return new GetAllGroupsResponse(groups);
    }
}
