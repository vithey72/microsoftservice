﻿namespace BNine.Application.Aggregates.Features.Queries.GetAllGroups;

using Domain.Entities.Features;
using MediatR;
using Models;

public record GetAllGroupsQuery(GroupKind? GroupType) : IRequest<GetAllGroupsResponse>;
