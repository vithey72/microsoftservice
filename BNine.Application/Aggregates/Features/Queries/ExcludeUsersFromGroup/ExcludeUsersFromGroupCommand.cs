﻿namespace BNine.Application.Aggregates.Features.Queries.ExcludeUsersFromGroup;

using System;
using BNine.Application.Aggregates.Features.Models;
using MediatR;

public record ExcludeUsersFromGroupCommand(Guid GroupId, Guid[] UserIds) : IRequest<AddOrRemoveUsersFromGroupResponse>;
