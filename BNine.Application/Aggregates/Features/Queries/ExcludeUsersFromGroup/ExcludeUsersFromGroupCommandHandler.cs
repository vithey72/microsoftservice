﻿namespace BNine.Application.Aggregates.Features.Queries.ExcludeUsersFromGroup;

using System;
using System.Threading.Tasks;
using AutoMapper;
using BNine.Application.Abstractions;
using BNine.Application.Aggregates.Features.Models;
using BNine.Application.Interfaces;
using BNine.Domain.Entities.Features;
using MediatR;
using Microsoft.EntityFrameworkCore;

public class ExcludeUsersFromGroupCommandHandler
    : AbstractRequestHandler
    , IRequestHandler<ExcludeUsersFromGroupCommand, AddOrRemoveUsersFromGroupResponse>
{
    public ExcludeUsersFromGroupCommandHandler(
        IMediator mediator,
        IBNineDbContext dbContext,
        IMapper mapper,
        ICurrentUserService currentUser
        ) : base(mediator, dbContext, mapper, currentUser)
    {
    }

    public async Task<AddOrRemoveUsersFromGroupResponse> Handle(ExcludeUsersFromGroupCommand request, CancellationToken cancellationToken)
    {
        var existingGroup = await DbContext
            .Groups
            .Include(g => g.UserGroups)
            .ThenInclude(ug => ug.User)
            .FirstOrDefaultAsync(x => x.Id == request.GroupId, cancellationToken);

        if (existingGroup == null)
        {
            throw new KeyNotFoundException($"Group with Id = {request.GroupId} is not present in DB.");
        }

        if (existingGroup.Kind != GroupKind.UserList)
        {
            throw new InvalidOperationException($"Operation only possible with groups of type {GroupKind.UserList}");
        }

        var removedCount = 0;
        if (request.UserIds != null)
        {
            removedCount = existingGroup
                .UserGroups
                .RemoveAll(
                x => request.UserIds.Contains(x.UserId));
            await DbContext.SaveChangesAsync(cancellationToken);
        }

        await DbContext.SaveChangesAsync(cancellationToken);
        var viewModel = Mapper.Map<GroupViewModel>(existingGroup);

        return new AddOrRemoveUsersFromGroupResponse(viewModel, existingGroup.UserGroups.Count, -removedCount);
    }
}
