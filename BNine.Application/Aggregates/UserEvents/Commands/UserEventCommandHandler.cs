﻿namespace BNine.Application.Aggregates.UserEvents.Commands;

using System.Threading;
using System.Threading.Tasks;
using Abstractions;
using AutoMapper;
using Domain.Entities.User;
using EventHandlers;
using Exceptions;
using Interfaces;
using MediatR;
using Microsoft.EntityFrameworkCore;

public class UserEventCommandHandler : AbstractRequestHandler, IRequestHandler<UserEventCommand, Unit>
{
    public UserEventCommandHandler(
        IMediator mediator,
        IBNineDbContext dbContext,
        IMapper mapper,
        ICurrentUserService currentUser)
        : base(mediator, dbContext, mapper, currentUser)
    {
    }

    public async Task<Unit> Handle(UserEventCommand request, CancellationToken cancellationToken)
    {
        var user = await DbContext.Users
            .FirstOrDefaultAsync(x => x.Id == CurrentUser.UserId, cancellationToken);

        if (user is null)
        {
            throw new NotFoundException(nameof(User), CurrentUser.UserId);
        }

        if (user.IsBlocked)
        {
            return Unit.Value;
        }

        user.DomainEvents.Add(new UserEvent { EventName = request.EventName, BannerId = request.BannerId });

        await DbContext.SaveChangesAsync(cancellationToken);

        return Unit.Value;
    }
}
