﻿namespace BNine.Application.Aggregates.UserEvents.Commands
{
    using MediatR;

    public class UserEventCommand : IRequest<Unit>
    {
        public string EventName
        {
            get; set;
        }

        public string BannerId
        {
            get; set;
        }
    }
}
