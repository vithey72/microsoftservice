﻿namespace BNine.Application.Aggregates.UserEvents.EventHandlers;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Banners.Commands;
using BNine.Application.Interfaces.ServiceBus;
using BNine.Application.Models;
using BNine.Application.Models.ServiceBus;
using Configuration.Queries.GetConfiguration;
using Constants;
using Domain.Entities.Features;
using Interfaces;
using MediatR;
using Microsoft.EntityFrameworkCore;
using JsonSerializer = System.Text.Json.JsonSerializer;

public class UserEventHandler : INotificationHandler<DomainEventNotification<UserEvent>>
{
    private readonly IBNineDbContext _dbContext;
    private readonly IMediator _mediator;
    private readonly ICurrentUserService _currentUser;
    private readonly IFeatureConfigurationService _featureConfigurationService;
    private readonly IServiceBus _serviceBus;
    private readonly IMarketingBannersService _marketingBannersService;

    public UserEventHandler(IBNineDbContext dbContext,
                            IMediator mediator,
                            ICurrentUserService currentUser,
                            IFeatureConfigurationService featureConfigurationService,
                            IServiceBus serviceBus,
                            IMarketingBannersService marketingBannersService
                            )
    {
        _dbContext = dbContext;
        _mediator = mediator;
        _currentUser = currentUser;
        _featureConfigurationService = featureConfigurationService;
        _serviceBus = serviceBus;
        _marketingBannersService = marketingBannersService;
    }

    public async Task Handle(DomainEventNotification<UserEvent> notification, CancellationToken cancellationToken)
    {
        var domainEvent = notification.DomainEvent;
        await PublishUserEventAsync(_currentUser.UserId, domainEvent, cancellationToken);

        if (domainEvent.EventName == UserEventNames.ArgyleBannerSeen)
        {
            await MarkArgyleBannerSeen(cancellationToken);
        }
        if (domainEvent.EventName == UserEventNames.CashbackOnboardingSlideshowSeen)
        {
            await MarkCashbackSlideshowSeen(cancellationToken);
        }

        if (domainEvent.EventName is UserEventNames.ReferralBannerClosed or UserEventNames.ReferralBannerButtonClicked)
        {
            await SwitchMarketingBanner(domainEvent.BannerId, cancellationToken);
        }

        if (domainEvent.EventName.StartsWith(UserEventNames.UserQuizPassedPrefix))
        {
            var quizName = domainEvent.EventName.Substring(UserEventNames.UserQuizPassedPrefix.Length);
            await MarkQuizCompleted(quizName, cancellationToken);
        }
        if (domainEvent.EventName.StartsWith(UserEventNames.ManualForm.Prefix))
        {
            if (domainEvent.EventName != UserEventNames.ManualForm.Displaying)
            {
                await HideManualFormSurvey(cancellationToken);
            }
        }
    }

    private Task PublishUserEventAsync(Guid? userId, UserEvent userEvent, CancellationToken cancellationToken = default)
    {
        return _serviceBus.Publish(
            new IntegrationEvent
            {
                Body = JsonSerializer.Serialize(new { UserId = _currentUser.UserId, Event = userEvent }),
                EventName = userEvent.EventName,
                TopicName = ServiceBusTopics.ItUserEvents,
            },
            cancellationToken);
    }

    private async Task SwitchMarketingBanner(string bannerId, CancellationToken cancellationToken)
    {
        await _mediator.Send(new DisableMarketingBannerCommand { BannerId = bannerId }, cancellationToken);
    }

    private async Task MarkArgyleBannerSeen(CancellationToken cancellationToken)
    {
        var userConfiguration = await _mediator.Send(new GetConfigurationQuery(), cancellationToken);
        if (!userConfiguration.HasFeatureEnabled(FeaturesNames.Features.ArgyleSuccessScreen))
        {
            return;
        }
        await AddUserToGroup(FeaturesNames.Groups.SeenArgyleSuccessScreen, cancellationToken);
    }

    private async Task MarkCashbackSlideshowSeen(CancellationToken cancellationToken)
    {
        var userConfiguration = await _mediator.Send(new GetConfigurationQuery(), cancellationToken);
        if (!userConfiguration.HasFeatureEnabled(FeaturesNames.Features.CashbackProgramOnboarding))
        {
            return;
        }
        await AddUserToGroup(FeaturesNames.Groups.SeenCashbackOnboardingSlideshow, cancellationToken);
    }

    private async Task AddUserToGroup(string groupName, CancellationToken cancellationToken)
    {
        var group = await _dbContext.Groups
            .FirstOrDefaultAsync(x => x.Name == groupName, cancellationToken);

        var existingUserGroup = await _dbContext.UserGroups
            .FirstOrDefaultAsync(ug => ug.GroupId == group.Id && ug.UserId == _currentUser.UserId, cancellationToken);

        if (existingUserGroup is null)
        {
            var now = DateTime.Now;
            var userGroup = new UserGroup
            {
                CreatedAt = now,
                UpdatedAt = now,
                UserId = _currentUser.UserId!.Value,
                GroupId = group.Id,
            };
            await _dbContext.UserGroups.AddAsync(userGroup, cancellationToken);
        }
        await _dbContext.SaveChangesAsync(cancellationToken);
    }

    private async Task MarkQuizCompleted(string quizName, CancellationToken cancellationToken)
    {
        var userConfiguration = await _mediator.Send(new GetConfigurationQuery());
        if (userConfiguration.HasFeatureEnabled(FeaturesNames.Features.UserQuestionnaire) &&
            userConfiguration.PropertyExists(FeaturesNames.Features.UserQuestionnaire,
                FeaturesNames.Settings.Questionnaire.Passed))
        {
            var passed = (userConfiguration.Features[FeaturesNames.Features.UserQuestionnaire] as dynamic).Passed;
            var props = (ICollection<KeyValuePair<string, object>>)passed;
            var kvPair = props.FirstOrDefault(x => string.Compare(x.Key, quizName, StringComparison.OrdinalIgnoreCase) == 0);
            if ((bool)kvPair.Value == false && !string.IsNullOrEmpty(kvPair.Key))
            {
                var groupName = FeaturesNames.Settings.Questionnaire.QuizGroupName(kvPair.Key);
                var group = await _dbContext.Groups
                    .FirstOrDefaultAsync(x => x.Name == groupName, cancellationToken);

                if (group == null)
                {
                    return;
                }

                var existingUserGroup = await _dbContext.UserGroups
                    .FirstOrDefaultAsync(ug => ug.GroupId == group.Id && ug.UserId == _currentUser.UserId,
                        cancellationToken);
                if (existingUserGroup == null)
                {
                    var now = DateTime.Now;
                    var userGroup = new UserGroup
                    {
                        CreatedAt = now,
                        UpdatedAt = now,
                        UserId = _currentUser.UserId!.Value,
                        GroupId = group.Id,
                    };
                    await _dbContext.UserGroups.AddAsync(userGroup, cancellationToken);
                }

                await _dbContext.SaveChangesAsync(cancellationToken);
            }
        }
    }

    private async Task HideManualFormSurvey(CancellationToken cancellationToken)
    {
        var userConfiguration = await _mediator.Send(new GetConfigurationQuery());
        if (userConfiguration.HasFeatureEnabled(FeaturesNames.Features.ManualFormSurvey))
        {
            await _featureConfigurationService.RemoveUserFromGroup(
                _currentUser.UserId!.Value,
                FeaturesNames.Groups.ShowManualFormSurvey,
                cancellationToken);
        }
    }
}
