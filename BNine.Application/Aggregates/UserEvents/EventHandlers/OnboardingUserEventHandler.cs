﻿namespace BNine.Application.Aggregates.UserEvents.EventHandlers;

using System;
using System.Threading;
using System.Threading.Tasks;
using BNine.Application.Aggregates.UserEvents.Events;
using Domain.Events.Users;
using Enums;
using Interfaces;
using MediatR;
using Microsoft.EntityFrameworkCore;

public class OnboardingUserEventHandler : INotificationHandler<OnboardingUserEvent>
{
    private readonly ICurrentUserService _currentUser;
    private readonly IBNineDbContext _dbContext;

    public OnboardingUserEventHandler(
        ICurrentUserService currentUser,
        IBNineDbContext dbContext
        )
    {
        _currentUser = currentUser;
        _dbContext = dbContext;
    }

    public async Task Handle(OnboardingUserEvent onboardingEvent, CancellationToken cancellationToken)
    {
        onboardingEvent.CreatedAt ??= DateTime.UtcNow;

        var user = await _dbContext.Users
            .SingleAsync(x => x.Id == _currentUser.UserId!.Value, CancellationToken.None);

        var stepDetailed = onboardingEvent.StepDetailed == UserStatusDetailed.Unknown
            ? (UserStatusDetailed)((int)onboardingEvent.Step * 10)
            : onboardingEvent.StepDetailed;
        if (onboardingEvent.Succeeded)
        {
            var successEvent = new OnboardingStepSuccessEvent(user, onboardingEvent.Step, stepDetailed, onboardingEvent.SourcePlatform);
            user.DomainEvents.Add(successEvent);
        }
        else
        {
            var failEvent = new OnboardingStepFailedEvent(user, onboardingEvent.Step, stepDetailed,
                new[]
                {
                    onboardingEvent.Message
                }, onboardingEvent.SourcePlatform);
            user.DomainEvents.Add(failEvent);
        }

        await _dbContext.SaveChangesAsync(CancellationToken.None);
    }
}
