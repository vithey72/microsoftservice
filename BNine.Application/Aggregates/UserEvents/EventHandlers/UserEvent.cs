﻿namespace BNine.Application.Aggregates.UserEvents.EventHandlers
{
    using Domain.Infrastructure;

    public class UserEvent : DomainEvent
    {
        public string EventName
        {
            get; set;
        }

        public string BannerId
        {
            get; set;
        }
    }
}
