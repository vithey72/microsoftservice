﻿namespace BNine.Application.Aggregates.UserEvents.EventHandlers;

using System;
using System.Threading;
using System.Threading.Tasks;
using BNine.Application.Aggregates.UserEvents.Events;
using BNine.Application.Interfaces.ServiceBus;
using BNine.Application.Models.ServiceBus;
using Constants;
using Interfaces;
using MediatR;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

public class UiUserEventNotificationHandler : INotificationHandler<UiUserEvent>
{
    private const string UiEventName = "ui";
    private readonly ICurrentUserService _currentUser;
    private readonly IServiceBus _serviceBus;

    /// <summary>
    /// Redirects all UI events to a Service Bus.
    /// </summary>
    public UiUserEventNotificationHandler(
                            ICurrentUserService currentUser,
                            IServiceBus serviceBus
    )
    {
        ArgumentNullException.ThrowIfNull(currentUser);
        ArgumentNullException.ThrowIfNull(serviceBus);

        _currentUser = currentUser;
        _serviceBus = serviceBus;
    }

    public async Task Handle(UiUserEvent @domainEvent, CancellationToken cancellationToken)
    {
        domainEvent.CreatedAt = DateTime.Now;
        domainEvent.UserId = _currentUser.UserId.Value;
        await _serviceBus.Publish(
            new IntegrationEvent
            {
                Body = JsonConvert.SerializeObject(domainEvent, new JsonSerializerSettings
                {
                    ContractResolver = new CamelCasePropertyNamesContractResolver
                    {
                        NamingStrategy = new CamelCaseNamingStrategy
                        {
                            ProcessDictionaryKeys = true,
                            OverrideSpecifiedNames = true
                        }
                    },

                }),
                EventName = UiEventName,
                TopicName = ServiceBusTopics.ItUserEvents,
            },
            cancellationToken);
    }
}
