﻿namespace BNine.Application.Aggregates.UserEvents.EventHandlers;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Banners.Commands;
using BNine.Application.Aggregates.Configuration.Queries.GetAppMenu;
using BNine.Application.Aggregates.UserEvents.Events;
using BNine.Application.Models.Feature.InfoPopup;
using BNine.Common.Extensions;
using CheckingAccounts.Models;
using CheckingAccounts.Queries.GetFilteredTransfers;
using Configuration.Queries.GetConfiguration;
using Constants;
using Domain.Constants;
using Domain.Entities.Features;
using Interfaces;
using Marketing.Queries.GetUserEligibilityForPostAdvanceSwitchToPremium;
using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using PushNotifications.Commands.InitUnemploymentProtectionDialog;
using PushNotifications.Commands.SendInitPostAdvanceSwitchToPremiumDialogSilentPush;
using PushNotifications.Commands.SendRateOurAppSilentPush;
using PushNotifications.Commands.SendUnemploymentProtectionAlertSilentPush;
using JsonSerializer = System.Text.Json.JsonSerializer;

/// <summary>
/// Changes a state of some entity relating to user, based on UI event received.
/// </summary>
public class UiEventStateHandler : INotificationHandler<UiUserEvent>
{
    private readonly IBNineDbContext _dbContext;
    private readonly IMediator _mediator;
    private readonly ICurrentUserService _currentUser;
    private readonly IFeatureConfigurationService _featureConfigurationService;
    private readonly IUserActivitiesCooldownService _cooldownService;
    private readonly IHttpContextAccessor _httpContext;

    public UiEventStateHandler(
        IBNineDbContext dbContext,
        IMediator mediator,
        ICurrentUserService currentUser,
        IFeatureConfigurationService featureConfigurationService,
        IUserActivitiesCooldownService cooldownService,
        IHttpContextAccessor httpContext
        )
    {
        _dbContext = dbContext;
        _mediator = mediator;
        _currentUser = currentUser;
        _featureConfigurationService = featureConfigurationService;
        _cooldownService = cooldownService;
        _httpContext = httpContext;
    }

    public async Task Handle(UiUserEvent domainEvent, CancellationToken cancellationToken)
    {
        if (domainEvent.Is(UiEventSource.Screens.Argyle, "pay-distribution-banner", action: UiEventSource.Actions.Show))
        {
            await MarkArgyleBannerSeen(cancellationToken);
        }

        if (domainEvent.Is(UiEventSource.Screens.Wallet, "referral-banners", "cross-button", action: UiEventSource.Actions.Click) ||
            domainEvent.Is(UiEventSource.Screens.Wallet, "referral-banners", "button", action: UiEventSource.Actions.Click))
        {
            await HandleTopBannerAction(domainEvent.Data["bannerId"], cancellationToken);
        }

        if (domainEvent.Is(UiEventSource.Screens.Questionnaire, action: UiEventSource.Actions.Complete))
        {
            await MarkQuizCompleted(domainEvent.Data["quizName"], cancellationToken);
        }

        if (domainEvent.Source.Screen == UiEventSource.Screens.ManualForm &&
            domainEvent.Source.Widget == "survey" &&
            domainEvent.Action == UiEventSource.Actions.Click)
        {
            await HideManualFormSurvey(cancellationToken);
        }

        if (domainEvent.IsOnAnyScreen("info-popup", "button", action: UiEventSource.Actions.Click)
          ||domainEvent.IsOnAnyScreen("info-popup", "close-button", action: UiEventSource.Actions.Click))
        {
            await HandleInfoPopupAction(domainEvent.Data["popupId"], cancellationToken);
        }

        if (domainEvent.Is(UiEventSource.Screens.GetAdvance,
                "success",
                "done-button",
                UiEventSource.Actions.Click))
        {
            await OfferSwitchToPremiumIfApplicable();
        }

        if (domainEvent.Is(UiEventSource.Screens.GetAdvance,
                "unemployment-protection",
                "interested-button",
                UiEventSource.Actions.Click))
        {
            await SendUnemploymentProtectionAlertSilentPush();
        }

        if (domainEvent.Is(UiEventSource.Screens.Wallet,
                "tile-menu",
                "argyle",
                UiEventSource.Actions.Click))
        {
            await PutArgyleTileRedDotOnCooldown();
        }
    }

    private async Task MarkArgyleBannerSeen(CancellationToken cancellationToken)
    {
        var userConfiguration = await _mediator.Send(new GetConfigurationQuery());
        if (!userConfiguration.HasFeatureEnabled(FeaturesNames.Features.ArgyleSuccessScreen))
        {
            return;
        }
        var group = await _dbContext.Groups
                .FirstOrDefaultAsync(x => x.Name == FeaturesNames.Groups.SeenArgyleSuccessScreen, cancellationToken);

        var existingUserGroup = await _dbContext.UserGroups
            .FirstOrDefaultAsync(ug => ug.GroupId == group.Id && ug.UserId == _currentUser.UserId, cancellationToken);

        if (existingUserGroup is null)
        {
            var now = DateTime.Now;
            var userGroup = new UserGroup
            {
                CreatedAt = now,
                UpdatedAt = now,
                UserId = _currentUser.UserId!.Value,
                GroupId = group.Id,
            };
            await _dbContext.UserGroups.AddAsync(userGroup, cancellationToken);
        }

        await _dbContext.SaveChangesAsync(cancellationToken);
    }

    private async Task MarkQuizCompleted(string quizName, CancellationToken cancellationToken)
    {
        var userConfiguration = await _mediator.Send(new GetConfigurationQuery());
        if (userConfiguration.HasFeatureEnabled(FeaturesNames.Features.UserQuestionnaire) &&
            userConfiguration.PropertyExists(FeaturesNames.Features.UserQuestionnaire,
                FeaturesNames.Settings.Questionnaire.Passed))
        {
            var passed = (userConfiguration.Features[FeaturesNames.Features.UserQuestionnaire] as dynamic).Passed;
            var props = (ICollection<KeyValuePair<string, object>>)passed;
            var kvPair = props.FirstOrDefault(x => string.Compare(x.Key, quizName, StringComparison.OrdinalIgnoreCase) == 0);
            if ((bool)kvPair.Value == false && !string.IsNullOrEmpty(kvPair.Key))
            {
                var groupName = FeaturesNames.Settings.Questionnaire.QuizGroupName(kvPair.Key);
                var group = await _dbContext.Groups
                    .FirstOrDefaultAsync(x => x.Name == groupName, cancellationToken);

                if (group == null)
                {
                    return;
                }

                var existingUserGroup = await _dbContext.UserGroups
                    .FirstOrDefaultAsync(ug => ug.GroupId == group.Id && ug.UserId == _currentUser.UserId,
                        cancellationToken);
                if (existingUserGroup == null)
                {
                    var now = DateTime.Now;
                    var userGroup = new UserGroup
                    {
                        CreatedAt = now,
                        UpdatedAt = now,
                        UserId = _currentUser.UserId!.Value,
                        GroupId = group.Id,
                    };
                    await _dbContext.UserGroups.AddAsync(userGroup, cancellationToken);
                }

                await _dbContext.SaveChangesAsync(cancellationToken);
            }
        }
    }

    private async Task HideManualFormSurvey(CancellationToken cancellationToken)
    {
        var userConfiguration = await _mediator.Send(new GetConfigurationQuery());
        if (userConfiguration.HasFeatureEnabled(FeaturesNames.Features.ManualFormSurvey))
        {
            await _featureConfigurationService.RemoveUserFromGroup(
                _currentUser.UserId!.Value,
                FeaturesNames.Groups.ShowManualFormSurvey,
                cancellationToken);
        }
    }

    private async Task HandleTopBannerAction(string bannerId, CancellationToken cancellationToken)
    {
        await _mediator.Send(new DisableMarketingBannerCommand { BannerId = bannerId }, cancellationToken);
    }

    private async Task HandleInfoPopupAction(string popupId, CancellationToken cancellationToken)
    {
        await _mediator.Send(new DisableMarketingBannerCommand { BannerId = popupId }, cancellationToken);

        // legacy
        var featureName = FeaturesNames.Features.InfoPopup;
        popupId = popupId.FirstCharToUpper();

        var configuration = await _mediator.Send(new GetConfigurationQuery());
        if (!configuration.HasFeatureEnabled(featureName))
        {
            return;
        }

        var infoPopupSettings = JsonConvert.DeserializeObject<InfoPopupSettings>(JsonConvert.SerializeObject(configuration.Features[featureName]));
        if (infoPopupSettings?.Popup is null || !infoPopupSettings.Popup.ContainsKey(popupId))
        {
            return;
        }

        var popup = infoPopupSettings.Popup[popupId];

        int secondsToAdd;
        if (popup.ShowAfterProgressive != null && popup.ShowAfterProgressive.Length > 0)
        {
            secondsToAdd = popup.ShowAfterProgressive[Math.Min(popup.CyclesPassed, popup.ShowAfterProgressive.Length - 1)];
        }
        else
        {
            secondsToAdd = popup.ShowAfter;
        }

        popup.ValidFrom = DateTime.UtcNow.AddSeconds(secondsToAdd).ToString("yyyy-MM-ddTHH:mm:ss");
        popup.CyclesPassed += 1;

        var featurePersonalGroup = await _featureConfigurationService.GetPersonalFeatureConfigAsync(
            _currentUser.UserId.Value,
            featureName,
            null,
            cancellationToken);
        featurePersonalGroup.Settings = JsonSerializer.Serialize(infoPopupSettings);
        await _dbContext.SaveChangesAsync(cancellationToken);
    }

    private async Task SendRateOurAppSilentPush()
    {
        var command = new SendRateOurAppSilentPushCommand(_currentUser.UserId!.Value);
        await _mediator.Send(command);
    }

    private async Task<bool> OfferSwitchToPremiumIfApplicable()
    {
        var isEligible = await _mediator.Send(new GetUserEligibilityForPostAdvanceSwitchToPremiumQuery
        {
            MbanqAccessToken = _currentUser.UserMbanqToken,
            Ip = _httpContext.HttpContext.Connection.RemoteIpAddress.ToString(),
        });
        if (isEligible)
        {
            await _mediator.Send(new SendInitPostAdvanceSwitchToPremiumDialogSilentPushCommand());
        }

        return isEligible;
    }

    private async Task InitiateUnemploymentProtectionDialog()
    {
        var command = new InitUnemploymentProtectionDialogCommand();
        await _mediator.Send(command);
    }

    private async Task SendUnemploymentProtectionAlertSilentPush()
    {
        var command = new SendUnemploymentProtectionAlertSilentPushCommand();
        await _mediator.Send(command);
    }

    private async Task PutArgyleTileRedDotOnCooldown()
    {
        var menu = await _mediator.Send(new GetAppMenuQuery());
        var tile = menu.Tiles.FirstOrDefault(x => x.Key == "argyle");
        if (tile == null)
        {
            return;
        }

        if (tile.HasNotifications == false)
        {
            // no red dot => don't create a cooldown for it
            return;
        }

        var recentTransactions = await _mediator.Send(new GetFilteredTransfersQuery
        {
            MbanqAccessToken = _currentUser.UserMbanqToken,
            Ip = _httpContext.HttpContext.Connection.RemoteIpAddress.ToString(),
            Filter = new GetFilteredTransfersExpandedFilter
            {
                TimePeriod = new GetFilteredTransfersFilter.TimePeriodFilter
                {
                    MinDateUtc = DateTime.UtcNow.AddMonths(-1),
                    MaxDateUtc = DateTime.UtcNow,
                }
            },
            Paging = new GetFilteredTransfersPagingParameters { PageSize = 1 },
        });
        var isDormant = !recentTransactions.Items.Any();
        if (isDormant)
        {
            await _cooldownService.AddCooldown(
                _currentUser.UserId!.Value,
                CooldownActivitiesNames.Menu.RedDotForArgyleTile,
                TimeSpan.FromHours(4),
                CancellationToken.None);
        }
        else
        {
            await _cooldownService.AddCooldown(
                _currentUser.UserId!.Value,
                CooldownActivitiesNames.Menu.RedDotForArgyleTile,
                TimeSpan.FromDays(14),
                CancellationToken.None);
        }
    }
}
