﻿namespace BNine.Application.Aggregates.UserEvents.Events;

using Enums;
using MediatR;

/// <summary>
/// Sent from the client when something important happens in Onboarding flow.
/// </summary>
public class OnboardingUserEvent : INotification
{
    public DateTime? CreatedAt
    {
        get;
        set;
    }

    public UserStatus Step
    {
        get;
        set;
    }

    public UserStatusDetailed StepDetailed
    {
        get;
        set;
    }

    public string Message
    {
        get;
        set;
    }

    public bool Succeeded
    {
        get;
        set;
    }

    public EventSourcePlatform SourcePlatform
    {
        get;
        set;
    }
}

