﻿namespace BNine.Application.Aggregates.UserEvents.Events;

using MediatR;

public class UiUserEvent : INotification
{
    public Guid UserId
    {
        get; set;
    }

    public DateTime CreatedAt
    {
        get; set;
    }

    public UiUserEventSource Source
    {
        get; set;
    }

    public string Action
    {
        get; set;
    }

    public Dictionary<string, string> Data
    {
        get; set;
    }

    public bool Is(string screen, string widget = null, string element = null, string action = null) =>
        Source.Screen == screen &&
        IsOnAnyScreen(widget, element, action);

    public bool IsOnAnyScreen(string widget = null, string element = null, string action = null) =>
        Source.Widget == widget &&
        Source.Element == element &&
        Action == action;
}

