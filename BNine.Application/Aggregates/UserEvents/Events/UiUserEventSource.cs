namespace BNine.Application.Aggregates.UserEvents.Events;

public record UiUserEventSource(string Screen, string Widget = null, string Element = null);