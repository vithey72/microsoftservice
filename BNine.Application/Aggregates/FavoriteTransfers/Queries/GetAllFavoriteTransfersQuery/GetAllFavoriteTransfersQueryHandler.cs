﻿namespace BNine.Application.Aggregates.FavoriteTransfers.Queries.GetAllFavoriteTransfersQuery;

using System.Threading;
using System.Threading.Tasks;
using Abstractions;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using Model;
using Exceptions;
using Interfaces;
using Interfaces.Bank.Client;
using Domain.Entities.BankAccount;
using Extensions;
using Interfaces.TransactionHistory;
using MediatR;
using Microsoft.EntityFrameworkCore;

public class GetAllFavoriteTransfersQueryHandler
    : AbstractRequestHandler
        , IRequestHandler<GetAllFavoriteTransfersQuery, FavoriteTransfersListViewModel>
{
    private readonly IBankSavingsTransactionsService _bankTransfersService;
    private readonly ITransfersMappingService _transfersMappingService;

    public GetAllFavoriteTransfersQueryHandler(IBankSavingsTransactionsService bankTransfersService,
        IMediator mediator,
        IBNineDbContext dbContext,
        IMapper mapper,
        ICurrentUserService currentUser,
        ITransfersMappingService transfersMappingService)
        : base(mediator, dbContext, mapper, currentUser)
    {
        _bankTransfersService = bankTransfersService;
        _transfersMappingService = transfersMappingService;
    }

    public async Task<FavoriteTransfersListViewModel> Handle(GetAllFavoriteTransfersQuery request, CancellationToken cancellationToken)
    {
        var user = await DbContext.Users
            .Include(x => x.CurrentAccount)
            .Where(x => x.Id == CurrentUser.UserId)
            .FirstOrDefaultAsync(cancellationToken);

        if (user?.CurrentAccount is null)
        {
            throw new NotFoundException(nameof(CurrentAccount), CurrentUser.UserId);
        }

        var favoriteTransfersVms = await DbContext.FavoriteUserTransfers
            .Where(st => st.UserId == CurrentUser.UserId)
            .OrderByDescending(x => x.UpdatedAt)
            .Take(request.Take)
            .ProjectTo<FavoriteTransferViewModel>(Mapper.ConfigurationProvider)
            .AsNoTracking()
            .ToListAsync(cancellationToken);

        if (favoriteTransfersVms.Any())
        {
            await AddAllIcons(request, favoriteTransfersVms);
        }

        return new FavoriteTransfersListViewModel
        {
            FavoriteTransfers = favoriteTransfersVms
        };
    }

    private async Task AddAllIcons(GetAllFavoriteTransfersQuery request, List<FavoriteTransferViewModel> favoriteTransfersVmItem)
    {
        var savingsTransaction = await _bankTransfersService.GetSavingsAccountTransfersArray(
            favoriteTransfersVmItem.Select(x => x.TransferId).ToArray(),
            request.MbanqAccessToken,
            request.Ip);

        foreach (var favoritesEntry in favoriteTransfersVmItem)
        {
            var transferInBank = savingsTransaction.FirstOrDefault(x => x.Id == favoritesEntry.TransferId);
            if (transferInBank == null)
            {
                continue;
            }

            var transactionDisplayType = transferInBank.TransferDisplayType;
            favoritesEntry.IconUrl = await _transfersMappingService.InferIconUrl(
                transactionDisplayType.InferIconOption(),
                null,
                transferInBank.Counterparty);
        }
    }
}
