﻿namespace BNine.Application.Aggregates.FavoriteTransfers.Queries.GetAllFavoriteTransfersQuery
{
    using Abstractions;
    using Model;
    using MediatR;

    public class GetAllFavoriteTransfersQuery : MBanqSelfServiceUserRequest,
      IRequest<FavoriteTransfersListViewModel>
    {
        /// <summary>
        /// Currently unused.
        /// </summary>
        public bool IncludeSuggestions
        {
            get; set;
        }

        public int Take
        {
            get; set;
        } = 999; // if value not passed, return all favorites
    }
}
