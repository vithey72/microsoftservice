﻿namespace BNine.Application.Aggregates.FavoriteTransfers.Model
{
    using AutoMapper;
    using Mappings;
    using BNine.Domain.Entities.Transfers;

    public class FavoriteTransferViewModel : IMapFrom<FavoriteUserTransfer>
    {
        public Guid? Id
        {
            get; set;
        }

        public int TransferId
        {
            get; set;
        }

        public string Title
        {
            get; set;
        }

        public string Subtitle
        {
            get; set;
        }

        public string IconUrl
        {
            get; set;
        }

        public SavedTransferReturnType Type
        {
            get;
            set;
        } = SavedTransferReturnType.Favorite;

        public void Mapping(Profile profile)
        {
            profile.CreateMap<FavoriteUserTransfer, FavoriteTransferViewModel>()
                  .ForMember(dst => dst.Id, opt => opt.MapFrom(s => s.Id))
                  .ForMember(dst => dst.Title, opt => opt.MapFrom(s => s.Name))
                  .ForMember(dst => dst.Subtitle, opt => opt.MapFrom(s => s.Description))
                  .ForMember(dst => dst.TransferId, opt => opt.MapFrom(s => s.TransferId));
        }

        public enum SavedTransferReturnType
        {
            Favorite,
            Suggested
        }
    }
}
