﻿namespace BNine.Application.Aggregates.FavoriteTransfers.Model
{
    public class FavoriteTransfersListViewModel
    {
        public List<FavoriteTransferViewModel> FavoriteTransfers
        {
            get; set;
        }
    }
}
