﻿namespace BNine.Application.Aggregates.FavoriteTransfers.Command.CreateFavoriteTransferCommand;

using System.Threading;
using System.Threading.Tasks;
using Abstractions;
using AutoMapper;
using Exceptions;
using Interfaces;
using Interfaces.Bank.Client;
using BNine.Domain.Entities.Transfers;
using Common;
using Domain.Entities.User;
using Enums.Transfers;
using Interfaces.TransactionHistory;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Models.MBanq;

public class CreateFavoriteTransferCommandHandler : AbstractRequestHandler
    , IRequestHandler<CreateFavoriteTransferCommand, Guid>
{
    private readonly IBankSavingsTransactionsService _bankTransfersService;
    private readonly IBankClientExternalCardsService _externalCardsService;
    private readonly ITransfersMappingService _transfersMappingService;

    public CreateFavoriteTransferCommandHandler(
        IBankSavingsTransactionsService bankTransfersService,
        IMediator mediator,
        IBNineDbContext dbContext,
        IMapper mapper,
        ICurrentUserService currentUser,
        IBankSavingAccountsService accountsService,
        IBankClientExternalCardsService externalCardsService,
        ITransfersMappingService transfersMappingService
    )
        : base(mediator, dbContext, mapper, currentUser)
    {
        _bankTransfersService = bankTransfersService;
        _externalCardsService = externalCardsService;
        _transfersMappingService = transfersMappingService;
    }

    public async Task<Guid> Handle(CreateFavoriteTransferCommand request, CancellationToken cancellationToken)
    {
        var user = await DbContext.Users
            .Include(u => u.CurrentAccount)
            .FirstOrDefaultAsync(u => u.Id == CurrentUser.UserId, cancellationToken);

        if (user == null)
        {
            throw new NotFoundException(nameof(User), CurrentUser.UserId);
        }

        var savingsTransaction = await _bankTransfersService.GetSavingsAccountTransfer(
            request.TransferExternalId,
            request.MbanqAccessToken,
            request.Ip);
        if (savingsTransaction == null)
        {
            throw new NotFoundException(nameof(savingsTransaction), request.TransferExternalId);
        }

        var type = savingsTransaction.TransferDisplayType;

        var defaultName = type switch
        {
            TransferDisplayType.AchSent => savingsTransaction.Creditor?.Name ?? "ACH Transfer",
            TransferDisplayType.SentToB9Client => savingsTransaction.Creditor?.Name ?? "B9 Client",
            TransferDisplayType.PushToExternalCard => "To Card",
            _ => string.Empty,
        };

        var savedTransfer = new FavoriteUserTransfer
        {
            TransferId = request.TransferExternalId,
            UserId = CurrentUser.UserId!.Value,
            Name = defaultName,
            Description = await FetchDescription(type, savingsTransaction, user, request),
            CreditorIdentifier = savingsTransaction.Creditor?.Identifier,
        };

        DbContext.FavoriteUserTransfers.Add(savedTransfer);
        await DbContext.SaveChangesAsync(cancellationToken);

        return savedTransfer.Id;
    }

    private async Task<string> FetchDescription(TransferDisplayType transferType, SavingsTransactionItem savingsTransaction,
        User user, CreateFavoriteTransferCommand request)
    {
        if (transferType == TransferDisplayType.AchSent)
        {
            var (account, _) = _transfersMappingService.ExtractAchRecipientNumbers(savingsTransaction);
            return SecretNumberHelper.GetLast4DigitsWithPrefix(account, "Account *");
        }
        if (transferType == TransferDisplayType.SentToB9Client)
        {
            var (_, account) = await _transfersMappingService.GetAccountAndCardNumberTexts(
                user.CurrentAccount.ExternalId, null, request.MbanqAccessToken, request.Ip);

            return account;
        }

        if (transferType == TransferDisplayType.PushToExternalCard)
        {
            return await GetExternalCardNumber(
                request,
                savingsTransaction.Creditor.Identifier.Split(':').LastOrDefault());
        }

        return "Transfer";
    }

    private async Task<string> GetExternalCardNumber(CreateFavoriteTransferCommand request, string cardId)
    {
        var intId = Convert.ToInt32(cardId);
        var cards = await _externalCardsService.GetCards(request.MbanqAccessToken, request.Ip, new[] { intId });
        var card = cards.FirstOrDefault();
        return card != null ? "Card *" + card.Last4Digits : "To External Card";
    }
}
