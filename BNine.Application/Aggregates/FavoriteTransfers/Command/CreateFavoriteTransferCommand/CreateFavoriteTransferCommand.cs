﻿namespace BNine.Application.Aggregates.FavoriteTransfers.Command.CreateFavoriteTransferCommand
{
    using Abstractions;
    using MediatR;

    public class CreateFavoriteTransferCommand : MBanqSelfServiceUserRequest, IRequest<Guid>
    {
        /// <summary>
        /// Id of transfer in bank.
        /// </summary>
        public int TransferExternalId
        {
            get; set;
        }
    }
}
