﻿namespace BNine.Application.Aggregates.FavoriteTransfers.Command.DeleteFavoriteTransfersAssociatedWithExternalCardCommand
{
    using System.Threading;
    using System.Threading.Tasks;
    using Abstractions;
    using AutoMapper;
    using DeleteFavoriteTransferCommand;
    using Enums.Transfers;
    using Interfaces;
    using Interfaces.Bank.Client;
    using MediatR;
    using Microsoft.EntityFrameworkCore;

    public class DeleteFavoriteTransfersAssociatedWithExternalCardHandler : AbstractRequestHandler
        , IRequestHandler<DeleteFavoriteTransfersAssociatedWithExternalCardCommand>
    {
        private readonly IBankSavingsTransactionsService _bankTransfersService;

        public DeleteFavoriteTransfersAssociatedWithExternalCardHandler(
            IMediator mediator,
            IBNineDbContext dbContext,
            IMapper mapper,
            ICurrentUserService currentUser,
            ITransferIconsAndNamesProviderService transferIconsAndNamesProviderService,
            IBankSavingAccountsService accountsService,
            IBankInternalCardsService internalCardsService,
            IBankSavingsTransactionsService bankTransfersService
            )
            : base(mediator, dbContext, mapper, currentUser)
        {
            _bankTransfersService = bankTransfersService;
        }

        /// <summary>
        /// Iterates over all of user's saved transfers and deletes ones that belong to a card being deleted.
        /// </summary>
        public async Task<Unit> Handle(DeleteFavoriteTransfersAssociatedWithExternalCardCommand request, CancellationToken cancellationToken)
        {
            var userTransfers = await DbContext.FavoriteUserTransfers
                .Where(st => st.UserId == CurrentUser.UserId)
                .ToListAsync(cancellationToken);

            foreach (var savedTransfer in userTransfers)
            {
                var savingsTransaction = await _bankTransfersService.GetSavingsAccountTransfer(
                    savedTransfer.TransferId,
                    request.MbanqAccessToken,
                    request.Ip);
                var transactionDisplayType = savingsTransaction.TransferDisplayType;
                if (transactionDisplayType == TransferDisplayType.PushToExternalCard &&
                    savingsTransaction.GetExternalCardId() == request.ExternalCardId)
                {
                    await Mediator.Send(new DeleteFavoriteTransferCommand { Id = savedTransfer.Id }, cancellationToken);
                }
            }


            return Unit.Value;
        }
    }
}
