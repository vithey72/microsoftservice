﻿namespace BNine.Application.Aggregates.FavoriteTransfers.Command.DeleteFavoriteTransfersAssociatedWithExternalCardCommand
{
    using Abstractions;
    using MediatR;

    public class DeleteFavoriteTransfersAssociatedWithExternalCardCommand
        : MBanqSelfServiceUserRequest
        , IRequest
    {
        public long ExternalCardId
        {
            get; set;
        }
    }
}
