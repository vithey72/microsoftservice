﻿namespace BNine.Application.Aggregates.FavoriteTransfers.Command.DeleteFavoriteTransferCommand
{
    using System.Threading;
    using System.Threading.Tasks;
    using AutoMapper;
    using Abstractions;
    using Interfaces;
    using MediatR;
    using Microsoft.EntityFrameworkCore;

    public class DeleteFavoriteTransferCommandHandler : AbstractRequestHandler
        , IRequestHandler<DeleteFavoriteTransferCommand>
    {
        public DeleteFavoriteTransferCommandHandler(IMediator mediator, IBNineDbContext dbContext, IMapper mapper, ICurrentUserService currentUser)
            : base(mediator, dbContext, mapper, currentUser)
        {
        }

        public async Task<Unit> Handle(DeleteFavoriteTransferCommand request, CancellationToken cancellationToken)
        {
            var transfer = await DbContext.FavoriteUserTransfers.FirstOrDefaultAsync(t => t.Id == request.Id);
            if (transfer == null)
            {
                return Unit.Value;
            }

            DbContext.FavoriteUserTransfers.Remove(transfer);
            await DbContext.SaveChangesAsync(cancellationToken);

            return Unit.Value;
        }
    }
}
