﻿namespace BNine.Application.Aggregates.FavoriteTransfers.Command.DeleteFavoriteTransferCommand
{
    using MediatR;

    public class DeleteFavoriteTransferCommand : IRequest
    {
        public Guid Id
        {
            get; set;
        }
    }
}
