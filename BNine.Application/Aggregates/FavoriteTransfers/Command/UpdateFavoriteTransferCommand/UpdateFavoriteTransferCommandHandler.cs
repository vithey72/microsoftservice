﻿namespace BNine.Application.Aggregates.FavoriteTransfers.Command.UpdateFavoriteTransferCommand
{
    using System.Threading;
    using System.Threading.Tasks;
    using AutoMapper;
    using BNine.Application.Abstractions;
    using BNine.Application.Exceptions;
    using BNine.Application.Interfaces;
    using BNine.Application.Interfaces.Bank.Client;
    using MediatR;
    using Microsoft.EntityFrameworkCore;

    public class UpdateFavoriteTransferCommandHandler : AbstractRequestHandler
        , IRequestHandler<UpdateFavoriteTransferCommand>
    {
        private readonly IBankSavingsTransactionsService _bankTransfersService;

        public UpdateFavoriteTransferCommandHandler(IBankSavingsTransactionsService bankTransfersService,
            IMediator mediator,
            IBNineDbContext dbContext,
            IMapper mapper,
            ICurrentUserService currentUser) : base(mediator, dbContext, mapper, currentUser)
        {
            _bankTransfersService = bankTransfersService;
        }

        public async Task<Unit> Handle(UpdateFavoriteTransferCommand request, CancellationToken cancellationToken)
        {
            var transferExternalId = DbContext.FavoriteUserTransfers
                .FirstOrDefault(st => st.Id == request.Id)?
                .TransferId;

            if (transferExternalId == null)
            {
                throw new NotFoundException(nameof(DbContext.FavoriteUserTransfers), request.Id);
            }

            var savingsTransaction = await _bankTransfersService.GetSavingsAccountTransfer(
                transferExternalId.Value,
                request.MbanqAccessToken,
                request.Ip);
            if (savingsTransaction == null)
            {
                throw new NotFoundException(nameof(savingsTransaction), transferExternalId);
            }

            var transfer = await DbContext.FavoriteUserTransfers.FirstOrDefaultAsync(t => t.Id == request.Id, cancellationToken);
            if (transfer == null)
            {
                throw new NotFoundException(nameof(FavoriteTransfers), request.Id);
            }

            transfer.TransferId = transferExternalId.Value;
            transfer.Name = request.Name;
            transfer.UpdatedAt = DateTime.UtcNow;

            await DbContext.SaveChangesAsync(cancellationToken);
            return Unit.Value;
        }
    }
}
