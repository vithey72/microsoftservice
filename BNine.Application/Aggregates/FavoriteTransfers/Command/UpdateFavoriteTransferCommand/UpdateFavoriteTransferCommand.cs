﻿namespace BNine.Application.Aggregates.FavoriteTransfers.Command.UpdateFavoriteTransferCommand;

using Abstractions;
using MediatR;
using Newtonsoft.Json;

public class UpdateFavoriteTransferCommand : MBanqSelfServiceUserRequest, IRequest
{
    [JsonIgnore]
    public Guid Id
    {
        get; set;
    }

    public string Name
    {
        get; set;
    }
}
