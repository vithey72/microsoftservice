﻿namespace BNine.Application.Aggregates.Selfie.Commands;

using BNine.Application.Exceptions;
using BNine.Application.Interfaces;
using BNine.Domain.Entities.User;
using MediatR;
using Microsoft.EntityFrameworkCore;

public class UploadSelfieWithTypeCommandHandler : IRequestHandler<UploadSelfieWithTypeCommand, Unit>
{
    private readonly IBNineDbContext _dbContext;
    private readonly ICurrentUserService _currentUser;
    private readonly IClientSelfieDocumentsService _clientSelfieDocumentsService;

    public UploadSelfieWithTypeCommandHandler(
        IBNineDbContext dbContext,
        ICurrentUserService currentUser,
        IClientSelfieDocumentsService clientSelfieDocumentsService
        )
    {
        _dbContext = dbContext;
        _currentUser = currentUser;
        _clientSelfieDocumentsService = clientSelfieDocumentsService;
    }

    public async Task<Unit> Handle(UploadSelfieWithTypeCommand request, CancellationToken cancellationToken)
    {
        var user = _dbContext
            .Users
            .Include(u => u.Settings)
            .FirstOrDefault(x => x.Id == _currentUser.UserId);

        if (user is null)
        {
            throw new NotFoundException(nameof(User), _currentUser.UserId);
        }
        await _clientSelfieDocumentsService.UploadDocument(_currentUser.UserId.Value, request.FileData, request.ContentType,
            request.FileExtension, request.SelfieSourceFlow, cancellationToken);

        user.Settings.IsSelfieScanned = true;

        await _dbContext.SaveChangesAsync(cancellationToken);

        return Unit.Value;
    }
}
