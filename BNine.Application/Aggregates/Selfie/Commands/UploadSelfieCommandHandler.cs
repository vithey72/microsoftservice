﻿namespace BNine.Application.Aggregates.Selfie.Commands;

using AutoMapper;
using BNine.Application.Abstractions;
using BNine.Application.Exceptions;
using BNine.Application.Interfaces;
using BNine.Domain.Entities.User;
using MediatR;

public class UploadSelfieCommandHandler : AbstractRequestHandler, IRequestHandler<UploadSelfieCommand, Unit>
{
    private readonly IClientSelfieDocumentsService _clientSelfieDocumentsService;

    public UploadSelfieCommandHandler(
        IMediator mediator,
        IBNineDbContext dbContext,
        IMapper mapper,
        ICurrentUserService currentUser,
        IClientSelfieDocumentsService clientSelfieDocumentsService
        ) : base(mediator, dbContext, mapper, currentUser)
    {
        _clientSelfieDocumentsService = clientSelfieDocumentsService;
    }

    public async Task<Unit> Handle(UploadSelfieCommand request, CancellationToken cancellationToken)
    {
        var user = DbContext.Users
            .FirstOrDefault(x => x.Id == CurrentUser.UserId);

        if (user is null)
        {
            throw new NotFoundException(nameof(User), CurrentUser.UserId);
        }

        await _clientSelfieDocumentsService.UploadDocument(CurrentUser.UserId.Value, request.FileData, request.ContentType,
            request.FileExtension, cancellationToken);

        user.Settings.IsSelfieScanned = true;

        await DbContext.SaveChangesAsync(cancellationToken);

        return Unit.Value;
    }
}
