﻿namespace BNine.Application.Aggregates.Selfie.Commands;

using MediatR;
public class UploadSelfieCommand : IRequest<Unit>
{
    public byte[] FileData
    {
        get;
        set;
    }

    public string ContentType
    {
        get;
        set;
    }

    public string FileExtension
    {
        get;
        set;
    }
}
