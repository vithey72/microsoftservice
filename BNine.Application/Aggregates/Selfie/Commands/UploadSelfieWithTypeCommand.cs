﻿namespace BNine.Application.Aggregates.Selfie.Commands;

using BNine.Enums;
using MediatR;

public class UploadSelfieWithTypeCommand : IRequest<Unit>
{
    public byte[] FileData
    {
        get;
        set;
    }

    public string ContentType
    {
        get;
        set;
    }

    public string FileExtension
    {
        get;
        set;
    }

    public SelfieInitiatingFlow SelfieSourceFlow
    {
        get;
        set;
    }
}
