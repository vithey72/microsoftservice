﻿namespace BNine.Application.Aggregates.Selfie.Models;

using Microsoft.AspNetCore.Http;

public class SelfieUpload
{
    public IFormFile Image
    {
        get;
        set;
    }
}
