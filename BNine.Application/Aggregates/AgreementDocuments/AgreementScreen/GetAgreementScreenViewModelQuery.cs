﻿namespace BNine.Application.Aggregates.AgreementDocuments.AgreementScreen;

using MediatR;
using Models;

public record GetAgreementScreenViewModelQuery : IRequest<AgreementScreenViewModel>;
