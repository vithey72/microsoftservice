﻿namespace BNine.Application.Aggregates.AgreementDocuments.AgreementScreen.Models;

using CommonModels.Text;

public class AgreementScreenViewModelBase
{
    public List<CheckBox> CheckBoxes
    {
        get;
        set;
    }
}


public class CheckBox
{
    public int Id
    {
        get;
        set;
    }

    public LinkedText LinkedText
    {
        get;
        set;
    }

    public bool IsRequired
    {
        get;
        set;
    }

    public bool IsSelected
    {
        get;
        set;
    }
}
