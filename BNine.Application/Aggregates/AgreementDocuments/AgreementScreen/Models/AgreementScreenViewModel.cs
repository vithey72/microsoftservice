﻿namespace BNine.Application.Aggregates.AgreementDocuments.AgreementScreen.Models;

using CommonModels.Buttons;

public class AgreementScreenViewModel : AgreementScreenViewModelBase
{
    public string Title
    {
        get;
        set;
    }

    public string Subtitle
    {
        get;
        set;
    }


    public string AgreeToAllButtonText
    {
        get;
        set;
    }

    public ButtonViewModel ContinueButton
    {
        get;
        set;
    }

    public string FooterText
    {
        get;
        set;
    }


}

