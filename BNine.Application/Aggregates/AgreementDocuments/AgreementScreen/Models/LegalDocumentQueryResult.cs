﻿namespace BNine.Application.Aggregates.AgreementDocuments.AgreementScreen.Models;

public class LegalDocumentQueryResult
{
    public string Key
    {
        get;
        set;
    }

    public string Url
    {
        get;
        set;
    }
}
