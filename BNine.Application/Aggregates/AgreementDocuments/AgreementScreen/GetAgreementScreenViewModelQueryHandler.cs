﻿namespace BNine.Application.Aggregates.AgreementDocuments.AgreementScreen;

using Abstractions;
using Application.Helpers;
using AutoMapper;
using CommonModels.Buttons;
using CommonModels.DialogElements;
using CommonModels.Text;
using Domain.Entities.Settings;
using Enums;
using Helpers;
using Interfaces;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Models;

public class GetAgreementScreenViewModelQueryHandler : AbstractRequestHandler,
    IRequestHandler<GetAgreementScreenViewModelQuery, AgreementScreenViewModel>
{
    private readonly IPartnerProviderService _partnerProviderService;
    private readonly string _customerAccPlaceholder = DocumentHelper.WrapKey(StaticDocumentKeys.CustomerAccountAndCardholderAgreement);
    private readonly string _electronicCommunicationPlaceholder = DocumentHelper.WrapKey(StaticDocumentKeys.ElectronicCommunicationConsent);
    private readonly string _privacyPolicyPlaceholder = DocumentHelper.WrapKey(StaticDocumentKeys.PrivacyPolicy);
    private readonly string _smsCampaignPlaceholder = DocumentHelper.WrapKey(StaticDocumentKeys.SmsCampaignTermsOfServicesMarketing);
    private readonly string _termsOfServicePlaceholder = DocumentHelper.WrapKey(StaticDocumentKeys.TermsOfServiceKey);

    public GetAgreementScreenViewModelQueryHandler(IMediator mediator,
        IBNineDbContext dbContext,
        IMapper mapper,
        ICurrentUserService currentUser,
        IPartnerProviderService partnerProviderService) :
        base(mediator, dbContext, mapper, currentUser)
    {
        _partnerProviderService = partnerProviderService;
    }

    public async Task<AgreementScreenViewModel> Handle(GetAgreementScreenViewModelQuery request,
        CancellationToken cancellationToken)
    {
        var legalDocuments =
            (await DbContext.StaticDocuments.Select(d =>
                    new
                    {
                        d.Key,
                        d.StoragePath
                    })
                .ToListAsync(cancellationToken))
            .Select(x => new LegalDocumentQueryResult() { Key = x.Key, Url = x.StoragePath })
            .ToList();


        var partnerName = StringProvider.GetPartnerName(_partnerProviderService);
        var partnerArticle = StringProvider.GetPartnerArticle(_partnerProviderService);
        var partner = _partnerProviderService.GetPartner();
        var checkBoxes = new List<CheckBox>
        {
            new()
            {
                Id = 0,
                LinkedText = new LinkedText(
                    $"I have read and agree to the {_electronicCommunicationPlaceholder}",
                    new[]
                    {
                        new EmbeddedUrlViewModel(
                            _electronicCommunicationPlaceholder,
                            "Electronic Communication Consent",
                            DocumentHelper.FetchDocumentUrl(legalDocuments, StaticDocumentKeys.ElectronicCommunicationConsent),
                            LinkFollowMode.BrowserPreview)

                    }),
                IsRequired = true,
                IsSelected = false
            },
            new()
            {
                Id = 1,
                LinkedText = new LinkedText(
                    $"I have read and agree to the {_customerAccPlaceholder}, {_termsOfServicePlaceholder}",
                    new[]
                    {
                        new EmbeddedUrlViewModel(
                            _customerAccPlaceholder, "Customer Account and Cardholder Agreement",
                            DocumentHelper.FetchDocumentUrl(legalDocuments, StaticDocumentKeys.CustomerAccountAndCardholderAgreement),
                            LinkFollowMode.BrowserPreview),
                        new EmbeddedUrlViewModel(
                            _termsOfServicePlaceholder, "Terms of Service",
                            DocumentHelper.FetchDocumentUrl(legalDocuments, StaticDocumentKeys.TermsOfServiceKey),
                            LinkFollowMode.BrowserPreview)
                    }),
                IsRequired = true,
                IsSelected = false
            },
            new()
            {
                Id = 2,
                LinkedText = new LinkedText(
                    $"By applying for {partnerArticle} {partnerName} Account and providing my contact details," +
                    $" I am agreeing to the {_privacyPolicyPlaceholder} which outlines how {partnerName} can " +
                    "use my information, including for marketing purposes",
                    new[]
                    {
                        new EmbeddedUrlViewModel(_privacyPolicyPlaceholder,
                            "Privacy Policy",
                            DocumentHelper.FetchDocumentUrl(legalDocuments, StaticDocumentKeys.PrivacyPolicy),
                            LinkFollowMode.BrowserPreview)
                    }),
                IsRequired = true,
                IsSelected = false
            }
        };

        if (partner is PartnerApp.Default or PartnerApp.MBanqApp)
        {
            checkBoxes.Add(new()
            {
                Id = 3,
                LinkedText =
                    new LinkedText($"Optional: I would like to receive marketing offers" +
                                   $" from {partnerName} at the phone number provided.\n" +
                                   "I understand that message and data rates may apply and" +
                                   $" that I can reply HELP for help or STOP to cancel. {_smsCampaignPlaceholder}.",
                        new[]
                        {
                            new EmbeddedUrlViewModel(_smsCampaignPlaceholder,
                                $"{partnerName} SMS Campaign Terms of Services (Marketing)",
                                DocumentHelper.FetchDocumentUrl(legalDocuments,
                                    StaticDocumentKeys.SmsCampaignTermsOfServicesMarketing),
                                LinkFollowMode.BrowserPreview)
                        }
                    ),
                IsRequired = false,
                IsSelected = false
            });
        }

        var response = new AgreementScreenViewModel
        {
            Title = "Almost done",
            Subtitle = "By checking the circles below, I consent that:",
            CheckBoxes = checkBoxes,
            AgreeToAllButtonText = "I agree to all",
            ContinueButton = new ButtonViewModel("AGREE AND APPLY NOW", ButtonStyleEnum.Solid),
            FooterText = $"{partnerName} is not a bank. Banking Services are provided by Mbanq banking partner, Evolve Bank & Trust, Member FDIC. The {partnerName} Account and {partnerName} Visa® Card are provided by Evolve Bank & Trust, Member FDIC."

        };

        return response;
    }
}

