﻿namespace BNine.Application.Aggregates.AgreementDocuments.AgreementScreen.Helpers;

using Models;

public static class DocumentHelper
{
    public static string WrapKey(string key) => $"{{{key}}}";
    public static string FetchDocumentUrl(List<LegalDocumentQueryResult> documents, string key) =>
        documents.FirstOrDefault(d => d.Key == key)?.Url;
}
