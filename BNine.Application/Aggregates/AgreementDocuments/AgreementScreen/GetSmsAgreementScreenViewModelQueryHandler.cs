﻿namespace BNine.Application.Aggregates.AgreementDocuments.AgreementScreen;

using Abstractions;
using Application.Helpers;
using AutoMapper;
using CommonModels.DialogElements;
using CommonModels.Text;
using Domain.Entities.Settings;
using Enums;
using Helpers;
using Interfaces;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Models;

public class GetSmsAgreementScreenViewModelQueryHandler: AbstractRequestHandler,
    IRequestHandler<GetSmsAgreementScreenViewModelQuery, AgreementScreenViewModelBase>
{
    private readonly IPartnerProviderService _partnerProviderService;
    private readonly string _smsCampaignPlaceholder = DocumentHelper.WrapKey(StaticDocumentKeys.SmsCampaignTermsOfServicesOtp);
    private readonly string _privacyPolicyPlaceholder = DocumentHelper.WrapKey(StaticDocumentKeys.PrivacyPolicy);

    public GetSmsAgreementScreenViewModelQueryHandler(
        IMediator mediator,
        IBNineDbContext dbContext,
        IMapper mapper,
        ICurrentUserService currentUser,
        IPartnerProviderService partnerProviderService
        )
        : base(mediator, dbContext, mapper, currentUser)
    {
        _partnerProviderService = partnerProviderService;
    }

    public async Task<AgreementScreenViewModelBase> Handle(GetSmsAgreementScreenViewModelQuery request, CancellationToken cancellationToken)
    {
        var legalDocuments =
            (await DbContext.StaticDocuments.Select(d =>
                    new { d.Key, d.StoragePath })
                .ToListAsync(cancellationToken))
            .Select(x => new LegalDocumentQueryResult() { Key = x.Key, Url = x.StoragePath} )
            .ToList();


        var checkBoxes = new List<CheckBox>();

        var partner = _partnerProviderService.GetPartner();

        if (partner is PartnerApp.Default or PartnerApp.MBanqApp)
        {
            checkBoxes.Add(new CheckBox
            {
                Id = 0,
                LinkedText =
                    new LinkedText($"I would like to receive OTP Code " +
                                   $"from {StringProvider.GetPartnerName(_partnerProviderService)} at the phone number provided.\n" +
                                   "Message and data rates may apply. " +
                                   $"Reply HELP for help or STOP to cancel. One message per request. {_smsCampaignPlaceholder}, {_privacyPolicyPlaceholder}.",
                        new[]
                        {
                            new EmbeddedUrlViewModel(_smsCampaignPlaceholder,
                                "SMS Campaign Terms of Services",
                                DocumentHelper.FetchDocumentUrl(legalDocuments, StaticDocumentKeys.SmsCampaignTermsOfServicesOtp),
                                LinkFollowMode.BrowserPreview),
                            new EmbeddedUrlViewModel(_privacyPolicyPlaceholder,
                                "Privacy Policy",
                                DocumentHelper.FetchDocumentUrl(legalDocuments, StaticDocumentKeys.PrivacyPolicy),
                                LinkFollowMode.BrowserPreview)
                        }
                    ),
                IsRequired = true,
                IsSelected = true
            });
        }

        return new AgreementScreenViewModelBase() { CheckBoxes = checkBoxes };

    }
}
