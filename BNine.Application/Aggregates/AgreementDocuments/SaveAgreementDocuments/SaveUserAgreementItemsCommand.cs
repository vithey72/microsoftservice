﻿namespace BNine.Application.Aggregates.AgreementDocuments.SaveAgreementDocuments;

using Enums;
using MediatR;

public record SaveUserAgreementItemsCommand(List<string> UserAgreedDocumentsKeys, AgreementScreenType ScreenType): IRequest<Unit>;
