﻿namespace BNine.Application.Aggregates.AgreementDocuments.SaveAgreementDocuments;

using AutoMapper;
using BNine.Application.Abstractions;
using BNine.Application.Interfaces;
using MediatR;
using Microsoft.EntityFrameworkCore;

public class SaveUserAgreementItemsCommandHandler : AbstractRequestHandler, IRequestHandler<SaveUserAgreementItemsCommand>
{

    public SaveUserAgreementItemsCommandHandler(IMediator mediator,
        IBNineDbContext dbContext,
        IMapper mapper,
        ICurrentUserService currentUser)
        :
        base(mediator, dbContext, mapper, currentUser)
    {
    }


    public async Task<Unit> Handle(SaveUserAgreementItemsCommand request, CancellationToken cancellationToken)
    {


        var userId = CurrentUser.UserId!.Value;


        var user = DbContext.Users
            .Include(u => u.Settings)
            .FirstOrDefault(u => u.Id == userId);


        var updatedAgreements = user!.Settings.ConfirmedAgreementItems != null ?
            user!.Settings.ConfirmedAgreementItems.Union(request.UserAgreedDocumentsKeys).ToList() :
            request.UserAgreedDocumentsKeys;


        user!.Settings.ConfirmedAgreementItems = updatedAgreements;
        await DbContext.SaveChangesAsync(cancellationToken);

        return Unit.Value;
    }
}
