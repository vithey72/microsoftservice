﻿namespace BNine.Application.Aggregates.AgreementDocuments.SaveAgreementDocuments;

using AutoMapper;
using BNine.Application.Abstractions;
using BNine.Application.Exceptions;
using BNine.Application.Interfaces;
using Domain.Entities.Settings;
using Enums;
using MediatR;
using Microsoft.EntityFrameworkCore;

public class ValidateSaveUserAgreementItemsCommandHandler : AbstractRequestHandler,
    IRequestHandler<ValidateSaveUserAgreementItemsCommand, Unit>
{
    private readonly IPartnerProviderService _partnerProviderService;

    public ValidateSaveUserAgreementItemsCommandHandler(
        IMediator mediator,
        IBNineDbContext dbContext,
        IMapper mapper,
        ICurrentUserService currentUser,
        IPartnerProviderService partnerProviderService)
        :
        base(mediator, dbContext, mapper, currentUser)
    {
        _partnerProviderService = partnerProviderService;
    }

    public async Task<Unit> Handle(ValidateSaveUserAgreementItemsCommand request, CancellationToken cancellationToken)
    {
        if(request.UserAgreedItemsKeys is null || request.UserAgreedItemsKeys.Count == 0)
        {
            return Unit.Value;
        }

        var userId = CurrentUser.UserId!.Value;

        var user = await DbContext.Users
            .Include(u => u.Settings)
            .FirstOrDefaultAsync(u => u.Id == userId,
                cancellationToken: cancellationToken);

        if (user is null)
        {
            throw new NotFoundException("user", userId);
        }

        if (user.Settings.ConfirmedAgreementItems != null && user.Settings.ConfirmedAgreementItems.SequenceEqual(request.UserAgreedItemsKeys))
        {
            return Unit.Value;
        }

        // if user has made it to this endpoint, it means that he has agreed to the SMS OTP consent
        // but it's required only for B9/MBanq, not for the rest of the partners
        var partner = _partnerProviderService.GetPartner();
        if ((partner is PartnerApp.Default or PartnerApp.MBanqApp) && !request.UserAgreedItemsKeys.Contains(StaticDocumentKeys.SmsCampaignTermsOfServicesOtp))
        {
            request.UserAgreedItemsKeys.Add(StaticDocumentKeys.SmsCampaignTermsOfServicesOtp);
        }


        var allDocuments = await DbContext.StaticDocuments
            .Select(d =>
            new {
                 d.Key,
                 d.IsAgreementRequired
            })
            .ToListAsync(cancellationToken);


        var requiredToAgreeItemsKeys =
            allDocuments.Where(x => x.IsAgreementRequired)
            .Select(x => x.Key)
            .Concat(ConsentItemsSettings.ConsentData.Where(d => d.Value.IsRequired)
                .Select(d => d.Key))
            .ToList();

        var allAgreementItemsKeys = allDocuments.Select(x => x.Key)
            .Concat(ConsentItemsSettings.ConsentData.Keys).ToList();

        if (request.ScreenType == AgreementScreenType.Default && !requiredToAgreeItemsKeys.All(k => request.UserAgreedItemsKeys.Contains(k)))
        {
            throw new ValidationException("userAgreementDocuments",
                "User must agree to all required documents");
        }

        if (request.UserAgreedItemsKeys.Except(allAgreementItemsKeys).Any())
        {
            throw new ValidationException("userAgreementDocuments",
                "User agreed to non-existing document(s)");
        }

        return Unit.Value;
    }


}
