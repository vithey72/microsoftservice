﻿namespace BNine.Application.Aggregates.AgreementDocuments.SaveAgreementDocuments;

using Enums;
using MediatR;

public record ValidateSaveUserAgreementItemsCommand(List<string> UserAgreedItemsKeys, AgreementScreenType ScreenType) : IRequest<Unit>;
