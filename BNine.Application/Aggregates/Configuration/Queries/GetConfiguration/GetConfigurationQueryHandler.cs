﻿namespace BNine.Application.Aggregates.Configuration.Queries.GetConfiguration
{
    using System;
    using System.Collections.Generic;
    using System.Dynamic;
    using System.Linq;
    using System.Threading;
    using System.Threading.Tasks;
    using Abstractions;
    using AutoMapper;
    using Users.Queries.GetUserGroups;
    using BNine.Domain.Entities.Features;
    using Interfaces;
    using MediatR;
    using Microsoft.EntityFrameworkCore;
    using Models;
    using Newtonsoft.Json;
    using Newtonsoft.Json.Linq;
    using BNine.Constants;
    using BNine.Application.Models.Feature.InfoPopup;
    using Common.TableConnect.Common;
    using Domain.Entities.User;
    using Enums;


    public class GetConfigurationQueryHandler
        : AbstractRequestHandler
        , IRequestHandler<GetConfigurationQuery, ConfigurationResponseModel>
    {
        private readonly IPartnerProviderService _partnerProvider;

        public GetConfigurationQueryHandler(
            IMediator mediator,
            IBNineDbContext dbContext,
            IMapper mapper,
            ICurrentUserService currentUser,
            IPartnerProviderService partnerProvider
            ) :
            base(mediator, dbContext, mapper, currentUser)
        {
            _partnerProvider = partnerProvider;
        }

        public async Task<ConfigurationResponseModel> Handle(GetConfigurationQuery request, CancellationToken cancellationToken)
        {
            var features = await DbContext.Features.ToListAsync();
            dynamic featureSettings = new ExpandoObject();

            var userId = request.UserId.HasValue ? request.UserId : CurrentUser.UserId;
            var isUnknownPlatformVersion = request is { AppVersion: null, MobilePlatform: null };
            User currentUser = null;
            if (userId.HasValue)
            {
                var user = await DbContext.Users.FindAsync(userId.Value)!;
                if (user == null)
                {
                    throw new KeyNotFoundException("This user does not exist: " + userId);
                }

                currentUser = user;

                var groups = await Mediator.Send(new GetUserGroupsQuery(userId.Value), cancellationToken);

                var registrationDate = user.ActivatedAt ?? user.CreatedAt;
                var accountAge = DateTime.Now - registrationDate;
                var groupedFeatureGroups = groups
                    .SelectMany(g => g.FeatureGroups)
                    .Where(g =>
                        (g.MaxAccountAgeHours == null || g.MaxAccountAgeHours.Value > accountAge.TotalHours) &&
                        (g.MinAccountAgeHours == null || g.MinAccountAgeHours.Value <= accountAge.TotalHours) &&
                        (g.MinRegistrationDate == null || g.MinRegistrationDate <= registrationDate) &&
                        (IsPlatformAndVersionConditionSatisfied(request, g, isUnknownPlatformVersion))
                        )
                    .GroupBy(fg => fg.Feature.Name);

                foreach (var feature in groupedFeatureGroups)
                {
                    var mergedFeatureJson = MergeFeatureGroupSettings(feature.ToList());

                    var mergedFeature = mergedFeatureJson == null ? "{}" : mergedFeatureJson.ToString();
                    var isEnabled = feature.LastOrDefault(fg => fg.IsEnabled != null)?.IsEnabled ?? false;

                    (featureSettings as IDictionary<string, object>)[feature.Key] = AddIsEnabledToFeature(mergedFeature, isEnabled);
                }
            }
            else
            {
                var accessToken = CurrentUser.AccessToken;
                if (accessToken != null && accessToken.ValidTo < DateTime.UtcNow)
                {
                    throw new UnauthorizedAccessException("Token has expired");
                }

                var group = await DbContext.Groups.Include(f => f.FeatureGroups).FirstAsync(g => g.Kind == GroupKind.Default);
                var featureGroupsWithAdditionalInfo =
                    group.FeatureGroups.Select(fg => (
                    FeatureGroup: fg,
                    FeatureName: features.First(f => f.Id == fg.FeatureId).Name))
                        .Where(fgf =>
                            IsPlatformAndVersionConditionSatisfied(request, fgf.FeatureGroup, isUnknownPlatformVersion));

                foreach (var (featureGroup, featureName) in featureGroupsWithAdditionalInfo)
                {
                    (featureSettings as IDictionary<string, object>)[featureName] = AddIsEnabledToFeature(featureGroup.Settings, featureGroup.IsEnabled.Value);
                }
            }

            await RearrangeFeatureSettings(featureSettings as IDictionary<string, object>, currentUser);

            var result = Mapper.Map<ConfigurationResponseModel>(new Configuration
            {
                Features = featureSettings,
            });

            return result;
        }

        private JObject MergeFeatureGroupSettings(List<FeatureGroup> featureGroupsByFeatureName)
        {
            JObject settingsJson = null;
            foreach (var fg in featureGroupsByFeatureName)
            {
                if (settingsJson != null && fg.Settings != null && fg.Group.Kind == GroupKind.Personal)
                {
                    settingsJson.Merge(JObject.Parse(fg.Settings), new JsonMergeSettings
                    {
                        MergeArrayHandling = MergeArrayHandling.Merge
                    });
                }
                else
                {
                    settingsJson = fg.Settings == null ? null : JObject.Parse(fg.Settings);
                }
            }
            return settingsJson;
        }

        private dynamic AddIsEnabledToFeature(string settingsJson, bool isEnabled)
        {
            dynamic settings = JsonConvert.DeserializeObject<ExpandoObject>(settingsJson == null ? "{}" : settingsJson);
            settings.IsEnabled = isEnabled;
            return settings;
        }

        private async Task RearrangeFeatureSettings(IDictionary<string, object> featureSettings, User user)
        {
            foreach (var fg in featureSettings.ToList())
            {
                switch (fg.Key)
                {
                    case FeaturesNames.Features.InfoPopup:
                        var infoPopupSettings = JsonConvert.DeserializeObject<InfoPopupSettings>(JsonConvert.SerializeObject(fg.Value));
                        if (infoPopupSettings != null)
                        {
                            var maxValue = infoPopupSettings.Popup.Values.Max(x => x.Priority);
                            var popups = infoPopupSettings.Popup.Where(p => p.Value.Priority != maxValue || DateTime.Parse(p.Value.ValidFrom) >= DateTime.UtcNow);
                            foreach (var p in popups)
                            {
                                infoPopupSettings.Popup.Remove(p.Key);
                            }

                            // TODO: remove when we're done with this
                            await HandleStatementPopupSpecialLink(infoPopupSettings);

                            dynamic settings = JsonConvert.DeserializeObject<ExpandoObject>(JsonConvert.SerializeObject(infoPopupSettings));
                            featureSettings[fg.Key] = settings;

                        }
                        break;
                    case FeaturesNames.Features.RateYourExperience:
                        var questionSet = await DbContext.CompletedUserQuestionnaires.FirstOrDefaultAsync(x =>
                            x.UserId == CurrentUser.UserId && x.QuestionSet.Name == FeaturesNames.Settings.Questionnaire.Quizzes.RateYourExperience);

                        if (questionSet is not null)
                        {
                            featureSettings[fg.Key] = new { IsEnabled = false };
                        }
                        DisableForMbanq(featureSettings, fg);

                        break;

                    case FeaturesNames.Features.ExternalCardSelfie:
                        if (user?.Settings is not null && user.Settings.IsSelfieScanned)
                        {
                            featureSettings[fg.Key] = new { IsEnabled = false };
                        }
                        DisableForMbanq(featureSettings, fg);

                        break;
                    // https://bninecom.atlassian.net/browse/B9-4741
                    case FeaturesNames.Features.IncomeAndTaxesScreen:
                        DisableForMbanq(featureSettings, fg);
                        break;
                    default:
                        break;
                }
            }
        }

        private void DisableForMbanq(IDictionary<string, object> featureSettings, KeyValuePair<string, object> fg)
        {
            if (_partnerProvider.GetPartner() == PartnerApp.MBanqApp)
            {
                featureSettings[fg.Key] = new { IsEnabled = false };
            }
        }

        private async Task HandleStatementPopupSpecialLink(InfoPopupSettings infoPopupSettings)
        {
            var statementPopup = infoPopupSettings
                .Popup
                .FirstOrDefault(x => x.Key == FeaturesNames.Settings.InfoPopup.VipiskaPopupName);

            if (CurrentUser.UserId != null)
            {
                if (statementPopup.Value != null)
                {
                    var (month, year) = InferStatementMonthAndYear();
                    if (!await UserHasTransfersLastMonth(month, year))
                    {
                        infoPopupSettings.Popup.Remove(FeaturesNames.Settings.InfoPopup.VipiskaPopupName);
                    }
                    else
                    {
                        statementPopup.Value.Title = statementPopup.Value.Title.Replace(
                            "{DATE_PLACEHOLDER}",
                            new DateTime(year, month, 1).ToString("MMM yy"));
                        statementPopup.Value.ExternalLink = CalculateStatementPopupLink(month, year);
                    }
                }
            }
            else
            {
                infoPopupSettings.Popup.Remove(FeaturesNames.Settings.InfoPopup.VipiskaPopupName);
            }
        }

        private async Task<bool> UserHasTransfersLastMonth(int month, int year)
        {
            // logic is redundant, we will be using a Group of users who should see it
            return true;
        }

        private bool IsPlatformAndVersionConditionSatisfied(GetConfigurationQuery request,
            FeatureGroup featureGroup, bool isUnknownPlatformVersion)
        {
            if (isUnknownPlatformVersion)
            {
                return featureGroup.MinAppVersion == null &&
                       featureGroup.MobilePlatform == null;
            }
            return (request.AppVersion >= featureGroup.MinAppVersion &&
                    featureGroup.MobilePlatform == request.MobilePlatform) ||
                   (featureGroup.MinAppVersion == null &&
                    featureGroup.MobilePlatform == null);
        }

        protected (int month, int year) InferStatementMonthAndYear()
        {
            var now = DateTime.Now;
            if (now.Month != 1)
            {
                return (now.Month - 1, now.Year);
            }

            return (12, now.Year - 1);
        }

        private string CalculateStatementPopupLink(int month, int year)
        {
            var user = DbContext
                .Users
                .First(u => u.Id == CurrentUser.UserId!);

            var address = $"https://bnine.com/activity-report/?userid={user.ExternalClientId}" +
                              $"&period={month:0#}{year.ToString().Substring(2)}" +
                              $"&hash={CryptoHelper.ComputeMd5Hash($"{user.ExternalClientId}666").ToLower()}";

            return address;
        }
    }
}
