﻿namespace BNine.Application.Aggregates.Configuration.Queries.GetConfiguration
{
    using System;
    using Enums;
    using MediatR;
    using Models;

    public class GetConfigurationQuery : IRequest<ConfigurationResponseModel>
    {
        public GetConfigurationQuery()
        {
        }

        public GetConfigurationQuery(MobilePlatform? mobilePlatform, Version appVersion)
        {
            MobilePlatform = mobilePlatform;
            AppVersion = appVersion;
        }

        public GetConfigurationQuery(Guid userId)
        {
            UserId = userId;
        }
        public MobilePlatform? MobilePlatform
        {
            get;
        }

        public Version AppVersion
        {
            get;
        }

        public Guid? UserId
        {
            get;
            internal set;
        }
    }
}
