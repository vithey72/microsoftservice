﻿namespace BNine.Application.Aggregates.Configuration.Queries.GetWalletBlocks;

using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using BNine.Application.Abstractions;
using BNine.Application.Interfaces;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;

internal class GetWalletBlockCollectionQueryHandler
        : AbstractRequestHandler
        , IRequestHandler<GetWalletBlockCollectionQuery, WalletBlockCollectionViewModel>
{
    public GetWalletBlockCollectionQueryHandler(
        IMediator mediator,
        IBNineDbContext dbContext,
        IMapper mapper,
        ICurrentUserService currentUser
        ) :
        base(mediator, dbContext, mapper, currentUser)
    {

    }

    public async Task<WalletBlockCollectionViewModel> Handle(GetWalletBlockCollectionQuery request, CancellationToken cancellationToken)
    {
        var walletBlockCollectionSettings = await DbContext.WalletBlockCollectionSettings.FirstOrDefaultAsync(cancellationToken);

        var viewModel = JsonConvert.DeserializeObject<WalletBlockCollectionViewModel>(walletBlockCollectionSettings.TextDescription);

        return viewModel;
    }
}
