﻿namespace BNine.Application.Aggregates.Configuration.Queries.GetWalletBlocks;

using MediatR;

public class GetWalletBlockCollectionQuery : IRequest<WalletBlockCollectionViewModel>
{
}
