﻿namespace BNine.Application.Aggregates.Configuration.Queries.GetWalletBlocks;

using BNine.Application.Aggregates.CommonModels;
using BNine.Application.Aggregates.CommonModels.DialogElements;
using BNine.Application.Aggregates.CommonModels.Text;
using Newtonsoft.Json;

public class WalletBlockCollectionViewModel
{
    [JsonProperty("widthPercentage")]
    public int WidthPercentage
    {
        get; set;
    }

    [JsonProperty("blocks")]
    public WalletBlockViewModel[] Blocks
    {
        get; set;
    }
}

public class WalletBlockViewModel
{
    [JsonProperty("orderNumber")]
    public int OrderNumber
    {
        get; set;
    }

    [JsonProperty("deeplinkObject")]
    public DeeplinkObject Deeplink
    {
        get;
        set;
    }

    [JsonProperty("eventTrackingObject")]
    public EventTrackingObject EventTracking
    {
        get; set;
    }

    [JsonProperty("imageUrl")]
    public string ImageUrl
    {
        get;
        set;
    }

    [JsonProperty("title")]
    public FormattedText Title
    {
        get; set;
    }

    [JsonProperty("subtitle")]
    public FormattedText Subtitle
    {
        get; set;
    }

    [JsonProperty("backgroundColor")]
    public ColorViewModel BackgroundColor
    {
        get; set;
    }
}

