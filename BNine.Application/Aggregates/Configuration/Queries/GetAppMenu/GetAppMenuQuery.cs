﻿namespace BNine.Application.Aggregates.Configuration.Queries.GetAppMenu;

using MediatR;
using Models;

public record GetAppMenuQuery : IRequest<GetAppMenuViewModel>;
