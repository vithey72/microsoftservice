﻿namespace BNine.Application.Aggregates.Configuration.Queries.GetAppMenu;

using System.Threading.Tasks;
using Abstractions;
using AutoMapper;
using CommonModels.DialogElements;
using Constants;
using Domain.Constants;
using Enums;
using Interfaces;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Models;
using Users.Queries.Cashback.GetCashbackAvailability;

public class GetAppMenuQueryHandler
    : AbstractRequestHandler
    , IRequestHandler<GetAppMenuQuery, GetAppMenuViewModel>
{
    private readonly IPartnerProviderService _partnerProvider;
    private readonly IUserActivitiesCooldownService _cooldownService;

    public GetAppMenuQueryHandler(
        IMediator mediator,
        IBNineDbContext dbContext,
        IMapper mapper,
        ICurrentUserService currentUser,
        IPartnerProviderService partnerProvider,
        IUserActivitiesCooldownService cooldownService
    )
        : base(mediator, dbContext, mapper, currentUser)
    {
        _partnerProvider = partnerProvider;
        _cooldownService = cooldownService;
    }

    public async Task<GetAppMenuViewModel> Handle(GetAppMenuQuery request, CancellationToken cancellationToken)
    {
        var walletTile = new GetAppMenuViewModel.MenuTile
        {
            Key = "wallet",
            Caption = "Wallet",
            DeeplinkObject = new DeeplinkObject(DeepLinkRoutes.Wallet, null),
            IconUrl = "https://b9prodaccount.blob.core.windows.net/static-documents-container/MenuIcons/01 wallet.png",
            EventTracking = BuildEventTrackingObject(EventNames.Menu.WalletTile),
        };

        var cardsTile = new GetAppMenuViewModel.MenuTile
        {
            Key = "cards",
            Caption = "Cards",
            DeeplinkObject = new DeeplinkObject(DeepLinkRoutes.Cards, null),
            IconUrl = "https://b9prodaccount.blob.core.windows.net/static-documents-container/MenuIcons/02 cards.png",
            EventTracking = BuildEventTrackingObject(EventNames.Menu.CardsTile),
        };

        var advanceTile = new GetAppMenuViewModel.MenuTile
        {
            Key = "advance",
            Caption = "Advance",
            DeeplinkObject = new DeeplinkObject(DeepLinkRoutes.B9Score, null),
            IconUrl = "https://b9prodaccount.blob.core.windows.net/static-documents-container/MenuIcons/03 advance.png",
            EventTracking = BuildEventTrackingObject(EventNames.Menu.AdvanceTile),
        };

        var cashbackTile = new GetAppMenuViewModel.MenuTile
        {
            Key = "cashback",
            Caption = "Cashback",
            DeeplinkObject = new DeeplinkObject(DeepLinkRoutes.CashbackProgram, null),
            IconUrl = "https://b9prodaccount.blob.core.windows.net/static-documents-container/MenuIcons/04 cashback.png",
            HasNotifications = !(await Mediator.Send(new GetCashbackAvailabilityQuery(), cancellationToken))
                .CashbackSwitch
                .IsEnabled,
            EventTracking = BuildEventTrackingObject(EventNames.Menu.CashbackTile),
        };

        var historyTile = new GetAppMenuViewModel.MenuTile
        {
            Key = "history",
            Caption = "History",
            DeeplinkObject = new DeeplinkObject(DeepLinkRoutes.TransfersHistory, null),
            IconUrl = "https://b9prodaccount.blob.core.windows.net/static-documents-container/MenuIcons/05 history.png",
            EventTracking = BuildEventTrackingObject(EventNames.Menu.HistoryTile),
        };

        var incomeTile = new GetAppMenuViewModel.MenuTile
        {
            Key = "income_and_taxes",
            Caption = "Income",
            DeeplinkObject = new DeeplinkObject(DeepLinkRoutes.IncomeAndTaxes, null),
            IconUrl = "https://b9prodaccount.blob.core.windows.net/static-documents-container/MenuIcons/06 income.png",
            EventTracking = BuildEventTrackingObject(EventNames.Menu.IncomeTile),
        };

        var supportTile = new GetAppMenuViewModel.MenuTile
        {
            Key = "support",
            Caption = "Support",
            DeeplinkObject = new DeeplinkObject(DeepLinkRoutes.Support, null),
            IconUrl = "https://b9prodaccount.blob.core.windows.net/static-documents-container/MenuIcons/07 support.png",
            EventTracking = BuildEventTrackingObject(EventNames.Menu.SupportTile),
        };

        var profileTile = new GetAppMenuViewModel.MenuTile
        {
            Key = "profile",
            Caption = "Profile",
            DeeplinkObject = new DeeplinkObject(DeepLinkRoutes.Profile, null),
            IconUrl = "https://b9prodaccount.blob.core.windows.net/static-documents-container/MenuIcons/08 profile.png",
            EventTracking = BuildEventTrackingObject(EventNames.Menu.ProfileTile),
        };

        var notificationsTile = new GetAppMenuViewModel.MenuTile
        {
            Key = "notifications",
            Caption = "Notifications",
            DeeplinkObject = new DeeplinkObject(DeepLinkRoutes.NotificationCenter, null),
            IconUrl = "https://b9prodaccount.blob.core.windows.net/static-documents-container/MenuIcons/09 notif.png",
            EventTracking = BuildEventTrackingObject(EventNames.Menu.NotificationsTile),
        };

        var tariffsTile = new GetAppMenuViewModel.MenuTile
        {
            Key = "tariffs",
            Caption = "Tariffs",
            DeeplinkObject = new DeeplinkObject(DeepLinkRoutes.AdvanceTariffPlan, null),
            IconUrl = "https://b9prodaccount.blob.core.windows.net/static-documents-container/MenuIcons/10 tariffs.png",
            EventTracking = BuildEventTrackingObject(EventNames.Menu.TariffsTile),
        };

        var statementTile = new GetAppMenuViewModel.MenuTile
        {
            Key = "statement",
            Caption = "Account Statement",
            DeeplinkObject = new DeeplinkObject(DeepLinkRoutes.AccountStatement, null),
            IconUrl = "https://b9prodaccount.blob.core.windows.net/static-documents-container/MenuIcons/11 satement.png",
            EventTracking = BuildEventTrackingObject(EventNames.Menu.AccountStatementTile)
        };

        var argyleTile = new GetAppMenuViewModel.MenuTile
        {
            Key = "argyle",
            Caption = "Switch Deposit",
            DeeplinkObject = new DeeplinkObject(DeepLinkRoutes.SwitchDeposit, null),
            IconUrl = "https://b9prodaccount.blob.core.windows.net/static-documents-container/MenuIcons/12 swtichdeposit.png",
            HasNotifications = await InferArgyleRedDotVisibility(cancellationToken),
            EventTracking = BuildEventTrackingObject(EventNames.Menu.SwitchDepositTile),
        };

        var partner = _partnerProvider.GetPartner();
        var allTiles = new List<GetAppMenuViewModel.MenuTile>
        {
            walletTile,
            cardsTile,
            advanceTile,
            cashbackTile,
            historyTile,
            incomeTile,
            supportTile,
            profileTile,
            notificationsTile,
            tariffsTile,
            statementTile,
            argyleTile,
        };

        if (partner is PartnerApp.Poetryy
            or PartnerApp.Eazy
            or PartnerApp.USNational
            or PartnerApp.Qorbis
            or PartnerApp.ManaPacific
            or PartnerApp.P2P
            or PartnerApp.Booq
            or PartnerApp.PayHammer)
        {
            allTiles.Remove(advanceTile);
            allTiles.Remove(cashbackTile);
            allTiles.Remove(incomeTile);
            allTiles.Remove(argyleTile);
        }

        return new GetAppMenuViewModel(allTiles);
    }

    private EventTrackingObject BuildEventTrackingObject(string tileEvent)
    {
        return new EventTrackingObject(
            new UserEventInstructionsObject(tileEvent, null), null);
    }

    private async Task<bool> InferArgyleRedDotVisibility(CancellationToken cancellationToken)
    {
        // skip this calculation if it's pointless
        if (_partnerProvider.GetPartner() is not (PartnerApp.Default or PartnerApp.MBanqApp))
        {
            return false;
        }

        var advanceStats = (await DbContext
            .Users
            .Include(u => u.UserSyncedStats)
            .SingleAsync(u => u.Id == CurrentUser.UserId, cancellationToken))
            .UserSyncedStats;

        if (advanceStats == null)
        {
            // user probably just registered and hasn't yet connected payroll
            return true;
        }

        var userDepositsPayroll = advanceStats.HasPayAllocation && advanceStats.PayrollSumFor30Days > 0;
        if (userDepositsPayroll)
        {
            return false;
        }

        var isOnCooldown = await _cooldownService.IsOnCooldown(
            CurrentUser.UserId!.Value,
            CooldownActivitiesNames.Menu.RedDotForArgyleTile,
            cancellationToken);

        return !isOnCooldown;
    }
}
