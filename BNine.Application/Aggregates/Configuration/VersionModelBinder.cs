﻿namespace BNine.Application.Aggregates.Configuration;

using Microsoft.AspNetCore.Mvc.ModelBinding;

public class VersionModelBinder : IModelBinder
{
    public Task BindModelAsync(ModelBindingContext bindingContext)
    {
        if (bindingContext == null)
        {
            throw new ArgumentNullException(nameof(bindingContext));
        }
        var value = bindingContext.HttpContext.Request.Headers["B9App-Version"].ToString().Split("-")[0];
        if(string.IsNullOrEmpty(value))
        {
            ModelBindingResult.Success(null);
            return Task.CompletedTask;
        }
        var modelName = bindingContext.ModelName;
        if (!Version.TryParse(value, out var version))
        {
            bindingContext.ModelState.TryAddModelError(
                modelName, $"Version {value} provided could not be parsed by the server");
            return Task.CompletedTask;
        }
        bindingContext.Result = ModelBindingResult.Success(version);
        return Task.CompletedTask;
    }
}
