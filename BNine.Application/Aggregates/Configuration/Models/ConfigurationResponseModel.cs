﻿namespace BNine.Application.Aggregates.Configuration.Models
{
    using System.Collections.Generic;
    using AutoMapper;
    using Mappings;
    using Newtonsoft.Json;
    using Newtonsoft.Json.Linq;

    public class ConfigurationResponseModel : IMapFrom<Configuration>
    {
        [JsonProperty("features")]
        public IDictionary<string, object> Features
        {
            get;
            set;
        } = new Dictionary<string, object>();

        public bool HasFeatureEnabled(string featureName)
        {
            if (!Features.ContainsKey(featureName))
            {
                return false;
            }

            return (bool)(Features[featureName] as dynamic).IsEnabled;
        }

        public bool PropertyExists(string featureName, string name)
        {
            var obj = Features[featureName] as IDictionary<string, object>;
            return obj.ContainsKey(name);
        }

        public void Mapping(Profile profile) => profile.CreateMap<Configuration, ConfigurationResponseModel>()
            .ForMember(dest => dest.Features, opt => opt.MapFrom(s => s.Features));
    }

    public class FeatureConfigurationModel
    {
        [JsonProperty("isEnabled")]
        public bool IsEnabled
        {
            get;
            set;
        }

        [JsonProperty("settings", NullValueHandling = NullValueHandling.Ignore)]
        public JObject Settings
        {
            get;
            set;
        }
    }
}
