﻿namespace BNine.Application.Aggregates.Configuration.Models;

using System.Linq;
using CommonModels.DialogElements;

public class GetAppMenuViewModel
{
    public GetAppMenuViewModel(IEnumerable<MenuTile> menuTiles)
    {
        Tiles = menuTiles.ToArray();
    }

    public MenuTile[] Tiles
    {
        get;
    }

    public bool HasNotifications => Tiles.Any(x => x.HasNotifications);

    public class MenuTile
    {
        public string Key
        {
            get;
            set;
        }

        public string Caption
        {
            get;
            set;
        }

        public string IconUrl
        {
            get;
            set;
        }

        public DeeplinkObject DeeplinkObject
        {
            get;
            set;
        }

        public EventTrackingObject EventTracking
        {
            get;
            set;
        }

        public bool HasNotifications
        {
            get;
            set;
        }
    }
}
