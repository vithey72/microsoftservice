﻿namespace BNine.Application.Aggregates.Configuration.Models
{
    using System;
    using AutoMapper;
    using Domain.Entities.Settings;
    using Mappings;
    using Newtonsoft.Json;

    public class UpdatedDocumentResponseModel : IMapFrom<StaticDocument>
    {
        [JsonProperty("version")]
        public int Version
        {
            get;
            set;
        }

        [JsonProperty("updatedAt")]
        public DateTime UpdatedAt
        {
            get;
            set;
        }

        [JsonProperty("storagePath")]
        public string StoragePath
        {
            get;
            set;
        }

        public void Mapping(Profile profile) => profile.CreateMap<StaticDocument, UpdatedDocumentResponseModel>()
            .ForMember(dest => dest.Version, opt => opt.MapFrom(s => s.Version))
            .ForMember(dest => dest.StoragePath, opt => opt.MapFrom(s => s.StoragePath))
            .ForMember(dest => dest.UpdatedAt, opt => opt.MapFrom(s => s.UpdatedAt));
    }
}
