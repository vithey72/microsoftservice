﻿namespace BNine.Application.Aggregates.Configuration.Models
{
    using System;
    using AutoMapper;
    using Domain.Entities.Settings;
    using Mappings;
    using Newtonsoft.Json;

    public class Document : IMapFrom<StaticDocument>
    {
        [JsonProperty("id")]
        public Guid? Id
        {
            get;
            set;
        }

        [JsonProperty("key")]
        public string Key
        {
            get;
            set;
        }

        [JsonProperty("title")]
        public string Title
        {
            get;
            set;
        }

        public void Mapping(Profile profile) => profile.CreateMap<StaticDocument, Document>()
            .ForMember(d => d.Id, opt => opt.MapFrom(s => s.Id))
            .ForMember(d => d.Key, opt => opt.MapFrom(s => s.Key))
            .ForMember(d => d.Title, opt => opt.MapFrom(s => s.Title))
            .ReverseMap();
    }
}
