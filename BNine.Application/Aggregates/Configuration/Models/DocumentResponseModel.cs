﻿namespace BNine.Application.Aggregates.Configuration.Models
{
    using System;
    using AutoMapper;
    using Domain.Entities.Settings;
    using Mappings;
    using Newtonsoft.Json;

    public class DocumentResponseModel : IMapFrom<StaticDocument>
    {
        [JsonProperty("id")]
        public Guid Id
        {
            get;
            set;
        }

        [JsonProperty("key")]
        public string Key
        {
            get;
            set;
        }

        [JsonProperty("title")]
        public string Title
        {
            get;
            set;
        }

        [JsonProperty("url")]
        public string StoragePath
        {
            get;
            set;
        }

        [JsonProperty("staticUrl")]
        public string StaticStoragePath
        {
            get;
            set;
        }

        public void Mapping(Profile profile) => profile.CreateMap<StaticDocument, DocumentResponseModel>()
            .ForMember(d => d.Id, opt => opt.MapFrom(s => s.Id))
            .ForMember(d => d.Key, opt => opt.MapFrom(s => s.Key))
            .ForMember(d => d.Title, opt => opt.MapFrom(s => s.Title))
            .ForMember(d => d.StoragePath, opt => opt.MapFrom(s => s.StoragePath))
            .ForMember(d => d.StaticStoragePath, opt => opt.MapFrom(s => s.StaticStoragePath));
    }
}
