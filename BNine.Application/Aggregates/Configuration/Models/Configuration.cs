﻿namespace BNine.Application.Aggregates.Configuration.Models
{
    using System.Collections.Generic;
    using Newtonsoft.Json;

    public class Configuration
    {
        [JsonProperty("documents")]
        public List<DocumentResponseModel> Documents
        {
            get;
            set;
        }

        [JsonProperty("features")]
        public IDictionary<string, object> Features
        {
            get;
            set;
        } = new Dictionary<string, object>();
    }
}
