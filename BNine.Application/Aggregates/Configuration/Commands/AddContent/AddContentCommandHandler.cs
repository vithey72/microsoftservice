﻿namespace BNine.Application.Aggregates.Configuration.Commands.AddContent
{
    using System;
    using System.Threading;
    using System.Threading.Tasks;
    using Abstractions;
    using AutoMapper;
    using BNine.Settings.Blob;
    using Constants;
    using Domain.Entities.Settings;
    using Interfaces;
    using MediatR;
    using Microsoft.Extensions.Options;
    using Models;

    public class AddContentCommandHandler : AbstractRequestHandler,
        IRequestHandler<AddContentCommand, UpdatedDocumentResponseModel>
    {
        private BlobStorageSettings BlobSettings
        {
            get;
        }

        private IBlobStorageService BlobStorageService
        {
            get;
        }

        public AddContentCommandHandler(IMediator mediator, IBNineDbContext dbContext, IMapper mapper,
            ICurrentUserService currentUser, IOptions<BlobStorageSettings> options,
            IBlobStorageService blobStorageService) : base(mediator, dbContext, mapper, currentUser)
        {
            BlobStorageService = blobStorageService;
            BlobSettings = options.Value;
        }

        public async Task<UpdatedDocumentResponseModel> Handle(AddContentCommand request,
            CancellationToken cancellationToken)
        {
            var document = await DbContext.StaticDocuments.FindAsync(request.Id);

            var doc = document is null
                ? DbContext.StaticDocuments.Add(await AddFile(request)).Entity
                : await UpdateFile(request, document);

            await DbContext.SaveChangesAsync(cancellationToken);

            var updatedDoc = Mapper.Map<UpdatedDocumentResponseModel>(doc);

            return updatedDoc;
        }

        private async Task<StaticDocument> AddFile(AddContentCommand request)
        {
            try
            {
                var document = Mapper.Map<StaticDocument>(request);
                var fileUrl =
                    await BlobStorageService.UploadFile(ContainerNames.SharedContainer, request.File, request.File.FileName);

                document.StoragePath = fileUrl;
                return document;
            }
            catch (Exception e)
            {
                throw new ArgumentException(e.Message, e);
            }
        }

        private async Task<StaticDocument> UpdateFile(AddContentCommand request, StaticDocument document)
        {
            try
            {
                var doc = MapDocument(request, document);
                await BlobStorageService.UploadFile(ContainerNames.SharedContainer, request.File,request.File.FileName);
                return doc;
            }
            catch (Exception e)
            {
                throw new ArgumentException(e.Message, e);
            }
        }

        private StaticDocument MapDocument(AddContentCommand request, StaticDocument document)
        {
            document.Key = request.Key ?? document.Key;
            document.Title = request.Title ?? document.Title;
            document.Version = document.Version + 1;
            document.CreatedAt = document.CreatedAt;
            document.UpdatedAt = DateTime.UtcNow;
            return document;
        }
    }
}
