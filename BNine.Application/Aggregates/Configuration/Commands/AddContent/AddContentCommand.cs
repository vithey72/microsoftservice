﻿namespace BNine.Application.Aggregates.Configuration.Commands.AddContent
{
    using System;
    using AutoMapper;
    using Domain.Entities.Settings;
    using Mappings;
    using MediatR;
    using Microsoft.AspNetCore.Http;
    using Models;
    using Newtonsoft.Json;

    public class AddContentCommand : IRequest<UpdatedDocumentResponseModel>, IMapTo<StaticDocument>
    {
        [JsonProperty("id")]
        public Guid? Id
        {
            get;
            set;
        }

        [JsonProperty("key")]
        public string Key
        {
            get;
            set;
        }

        [JsonProperty("title")]
        public string Title
        {
            get;
            set;
        }

        [JsonProperty("file")]
        public IFormFile File
        {
            get;
            set;
        }

        public void Mapping(Profile profile) => profile.CreateMap<AddContentCommand, StaticDocument>()
            .ForMember(d => d.Id, opt => opt.MapFrom(s => s.Id ?? Guid.NewGuid()))
            .ForMember(d => d.Key, opt => opt.MapFrom(s => s.Key))
            .ForMember(d => d.Title, opt => opt.MapFrom(s => s.Title))
            .ForMember(d => d.Version, opt => opt.MapFrom(s => 1));
    }
}
