﻿namespace BNine.Application.Aggregates.Configuration.Commands.UpdateConfiguration
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading;
    using System.Threading.Tasks;
    using Abstractions;
    using AutoMapper;
    using BNine.Settings.Blob;
    using Constants;
    using Interfaces;
    using MediatR;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.Extensions.Options;
    using Models;


    public class UpdateConfigurationCommandHandler : AbstractRequestHandler,
        IRequestHandler<UpdateConfigurationCommand, Unit>
    {

        private BlobStorageSettings BlobSettings
        {
            get;
        }

        private IBlobStorageService BlobStorageService
        {
            get;
        }

        public UpdateConfigurationCommandHandler(IMediator mediator, IBNineDbContext dbContext, IMapper mapper,
            ICurrentUserService currentUser, IOptions<BlobStorageSettings> options,
            IBlobStorageService blobStorageService) : base(mediator, dbContext, mapper, currentUser)
        {
            BlobStorageService = blobStorageService;
            BlobSettings = options.Value;
        }

        public async Task<Unit> Handle(UpdateConfigurationCommand request, CancellationToken cancellationToken)
        {
            try
            {
                await RemoveFiles(request.Documents);
                return Unit.Value;
            }
            catch (Exception e)
            {
                throw new ArgumentException(e.Message, e);
            }
        }

        private async Task RemoveFiles(IEnumerable<Document> newConfiguration)
        {
            var configuration = await DbContext.StaticDocuments.ToListAsync();
            var newConfigurationKeys = newConfiguration.Select(x => x.Id.ToString());
            var oldFiles = configuration.Where(x => !newConfigurationKeys.Any(x.Id.ToString().Contains)).ToList();

            DbContext.StaticDocuments.RemoveRange(oldFiles);
            await DbContext.SaveChangesAsync(default);
            await BlobStorageService.RemoveFiles(ContainerNames.SharedContainer, oldFiles.Select(x => x.StoragePath));
        }
    }
}
