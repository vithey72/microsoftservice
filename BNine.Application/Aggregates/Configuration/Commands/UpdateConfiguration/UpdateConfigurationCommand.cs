﻿namespace BNine.Application.Aggregates.Configuration.Commands.UpdateConfiguration
{
    using System.Collections.Generic;
    using MediatR;
    using Models;
    using Newtonsoft.Json;

    public class UpdateConfigurationCommand : IRequest<Unit>
    {
        [JsonProperty("documents")]
        public List<Document> Documents
        {
            get;
            set;
        }
    }
}
