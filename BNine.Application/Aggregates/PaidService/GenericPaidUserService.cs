﻿namespace BNine.Application.Aggregates.PaidService;

using BNine.Application.Interfaces.Bank.Administration;
using BNine.Domain.Entities.PaidUserServices;

#nullable enable

public class GenericPaidUserService : IPaidUserServicePattern
{
    private readonly Guid _serviceId;
    private readonly IPaidUserServicesService _paidUserServicesService;

    protected GenericPaidUserService(
        Guid serviceId,
        IPaidUserServicesService paidUserServicesService
        )
    {
        _serviceId = serviceId;
        _paidUserServicesService = paidUserServicesService;
    }

    public async Task<UsedPaidUserService> Enable(Guid userId, string? idempotencyKey, CancellationToken cancellationToken)
    {
        return await _paidUserServicesService.EnableService(_serviceId, idempotencyKey, userId);
    }

    public async Task<bool> IsAlreadyPurchased(Guid userId, CancellationToken cancellationToken)
    {
        return await _paidUserServicesService.ServiceIsAlreadyPurchased(_serviceId, userId);
    }

    public async Task<UsedPaidUserService> GetEnabledForUser(Guid userId, CancellationToken cancellationToken)
    {
        return await _paidUserServicesService.GetEnabledService(_serviceId, userId);
    }

    public async Task<decimal> GetServiceCost(CancellationToken cancellationToken)
    {
        return await _paidUserServicesService.GetServiceCost(_serviceId);
    }

    public async Task MarkAsUsed(Guid userId, CancellationToken cancellationToken)
    {
        await _paidUserServicesService.MarkServiceAsUsed(_serviceId, userId);
    }
}
