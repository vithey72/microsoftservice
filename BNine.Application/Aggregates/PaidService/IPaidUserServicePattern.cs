﻿namespace BNine.Application.Aggregates.PaidService;

using BNine.Domain.Entities.PaidUserServices;

#nullable enable

public interface IPaidUserServicePattern
{
    Task<UsedPaidUserService> Enable(Guid userId, string? idempotencyKey, CancellationToken cancellationToken);

    Task<bool> IsAlreadyPurchased(Guid userId, CancellationToken cancellationToken);

    Task<UsedPaidUserService> GetEnabledForUser(Guid userId, CancellationToken cancellationToken);

    Task<decimal> GetServiceCost(CancellationToken cancellationToken);

    Task MarkAsUsed(Guid userId, CancellationToken cancellationToken);
}
