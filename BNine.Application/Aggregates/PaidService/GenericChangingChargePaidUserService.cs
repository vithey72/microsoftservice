﻿namespace BNine.Application.Aggregates.PaidService;

using System;
using System.Threading;
using System.Threading.Tasks;
using BNine.Domain.Entities.PaidUserServices;

#nullable enable

public class GenericChangingChargePaidUserService : IChangingChargePaidUserServicePattern
{
    private readonly IPaidUserServicePattern _obsoleteServiceVersion;
    private readonly IPaidUserServicePattern _newServiceVersion;

    protected GenericChangingChargePaidUserService(
        IPaidUserServicePattern obsoleteServiceVersion,
        IPaidUserServicePattern newServiceVersion
        )
    {
        _obsoleteServiceVersion = obsoleteServiceVersion;
        _newServiceVersion = newServiceVersion;
    }

    public Task<UsedPaidUserService> Enable(Guid userId, string? idempotencyKey, CancellationToken cancellationToken)
    {
        return _newServiceVersion.Enable(userId, idempotencyKey, cancellationToken);
    }

    public async Task<UsedPaidUserService> GetEnabledForUser(Guid userId, CancellationToken cancellationToken)
    {
        if (await _newServiceVersion.IsAlreadyPurchased(userId, cancellationToken))
        {
            return await _newServiceVersion.GetEnabledForUser(userId, cancellationToken);
        }

        return await _obsoleteServiceVersion.GetEnabledForUser(userId, cancellationToken);
    }

    public Task<decimal> GetServiceCost(CancellationToken cancellationToken)
    {
        return _newServiceVersion.GetServiceCost(cancellationToken);
    }

    public async Task<bool> IsAlreadyPurchased(Guid userId, CancellationToken cancellationToken)
    {
        if (await _newServiceVersion.IsAlreadyPurchased(userId, cancellationToken))
        {
            return true;
        }

        return await _obsoleteServiceVersion.IsAlreadyPurchased(userId, cancellationToken);
    }

    public async Task MarkAsUsed(Guid userId, CancellationToken cancellationToken)
    {
        await _obsoleteServiceVersion.MarkAsUsed(userId, cancellationToken);
        await _newServiceVersion.MarkAsUsed(userId, cancellationToken);
    }
}
