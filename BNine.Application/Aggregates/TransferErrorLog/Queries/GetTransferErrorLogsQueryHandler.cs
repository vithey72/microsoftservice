﻿namespace BNine.Application.Aggregates.TransferErrorLog.Queries;

using Abstractions;
using Application.Models;
using AutoMapper;
using Domain.Entities.Transfers;
using Exceptions;
using Interfaces;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Models;

public class GetTransferErrorLogsQueryHandler : AbstractRequestHandler,
    IRequestHandler<GetTransferErrorLogsQuery, PaginatedList<TransferErrorLogViewModel>>
{
    public GetTransferErrorLogsQueryHandler(IMediator mediator, IBNineDbContext dbContext, IMapper mapper,
        ICurrentUserService currentUser) : base(mediator, dbContext, mapper, currentUser)
    {
    }

    public async Task<PaginatedList<TransferErrorLogViewModel>> Handle(GetTransferErrorLogsQuery request,
        CancellationToken cancellationToken)
    {
        var user = await DbContext.Users
            .Include(x => x.TransferErrorLogs)
            .FirstOrDefaultAsync(x => x.Id == request.ClientId);

        if (user == null)
        {
            throw new NotFoundException($"User not found with id: {request.ClientId}");
        }

        var resultList = user.TransferErrorLogs.OrderByDescending(x => x.Timestamp)
            .Select(x => MapTransferErrorToViewModel(x)).ToList();

        return new PaginatedList<TransferErrorLogViewModel>(resultList, resultList.Count, 1, resultList.Count);
    }

    private TransferErrorLogViewModel MapTransferErrorToViewModel(TransferErrorLog domainErrorLog)
    {
        var viewModel = new TransferErrorLogViewModel()
        {
            TransferDirectionString = domainErrorLog.TransferDirection.ToString(),
            TransferNetworkTypeString = domainErrorLog.TransferNetworkType.ToString()
        };

        Mapper.Map(domainErrorLog, viewModel);

        return viewModel;
    }
}
