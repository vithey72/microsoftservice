﻿namespace BNine.Application.Aggregates.TransferErrorLog.Queries;

using Application.Models;
using MediatR;
using Models;

public class GetTransferErrorLogsQuery : IRequest<PaginatedList<TransferErrorLogViewModel>>
{
    public Guid ClientId
    {
        get;
        set;
    }
}
