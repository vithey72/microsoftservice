﻿namespace BNine.Application.Aggregates.TransferErrorLog.Models;

using Domain.Entities.Transfers;
using Enums.Transfers;
using Mappings;

public class TransferErrorLogViewModel : IMapFrom<TransferErrorLog>
{
    public Guid Id
    {
        get;
        set;
    }

    public Guid UserId
    {
        get;
        set;
    }

    public TransferDirection TransferDirection
    {
        get; set;
    }

    public string TransferDirectionString
    {
        get; set;
    }

    public TransferType TransferNetworkType
    {
        get;
        set;
    }

    public string TransferNetworkTypeString
    {
        get;
        set;
    }

    public string ExceptionHeader
    {
        get;
        set;
    }

    public string ExceptionBody
    {
        get;
        set;
    }

    public string MbanqRawErrorPayload
    {
        get;
        set;
    }

    public DateTime Timestamp
    {
        get;
        set;
    }
}
