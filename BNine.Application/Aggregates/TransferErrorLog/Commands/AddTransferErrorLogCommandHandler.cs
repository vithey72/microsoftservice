﻿namespace BNine.Application.Aggregates.TransferErrorLog.Commands;

using Abstractions;
using AutoMapper;
using Domain.Entities.Transfers;
using Interfaces;
using MediatR;
using Microsoft.Extensions.Logging;

public class AddTransferErrorLogCommandHandler
    : AbstractRequestHandler
        , IRequestHandler<AddTransferErrorLogCommand, Unit>
{
    private readonly ILogger<AddTransferErrorLogCommandHandler> _logger;

    public AddTransferErrorLogCommandHandler(
        IMediator mediator,
        IBNineDbContext dbContext,
        IMapper mapper,
        ICurrentUserService currentUser,
        ILogger<AddTransferErrorLogCommandHandler> logger) : base(mediator, dbContext, mapper, currentUser)
    {
        _logger = logger;
    }

    public async Task<Unit> Handle(AddTransferErrorLogCommand request, CancellationToken cancellationToken)
    {
        try
        {
            await HandleRequest(request, cancellationToken);
        }
        catch (Exception ex)
        {
            _logger.LogWarning("Error occured during adding transfer failure log {userId} error: {error}",
                CurrentUser.UserId, ex);
        }

        return Unit.Value;
    }

    private async Task HandleRequest(AddTransferErrorLogCommand request, CancellationToken cancellationToken)
    {
        if (!CurrentUser.UserId.HasValue)
        {
            throw new UnauthorizedAccessException();
        }

        await DbContext.TransferErrorLogs.AddAsync(
            new TransferErrorLog()
            {
                TransferNetworkType = request.TransferNetworkType,
                ExceptionHeader = request.ExceptionHeader,
                ExceptionBody = request.ExceptionBody,
                MbanqRawErrorPayload = request.MbanqRawErrorPayload,
                UserId = CurrentUser.UserId.Value,
                TransferDirection = request.TransferDirection
            }, cancellationToken);

        await DbContext.SaveChangesAsync(cancellationToken);
    }
}
