﻿namespace BNine.Application.Aggregates.TransferErrorLog.Commands;

using Enums.Transfers;
using MediatR;

public class AddTransferErrorLogCommand : IRequest
{
    public TransferDirection TransferDirection
    {
        get; set;
    }

    public TransferType TransferNetworkType
    {
        get;
        set;
    }

    public string ExceptionHeader
    {
        get;
        set;
    }

    public string ExceptionBody
    {
        get;
        set;
    }

    public string MbanqRawErrorPayload
    {
        get;
        set;
    }
}
