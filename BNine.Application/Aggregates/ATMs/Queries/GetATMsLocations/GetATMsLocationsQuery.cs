﻿namespace BNine.Application.Aggregates.ATMs.Queries.GetATMsLocations
{
    using BNine.Application.Aggregates.ATMs.Models;
    using BNine.Application.Models;
    using MediatR;
    using Newtonsoft.Json;

    public class GetATMsLocationsQuery : IRequest<PaginatedList<ATMInfoModel>>
    {
        public GetATMsLocationsQuery(double longitude, double latitude, long radius, long skip, long take)
        {
            Longitude = longitude;
            Latitude = latitude;
            Radius = radius;
            Skip = skip;
            Take = take;
        }

        [JsonProperty("longitude")]
        public double Longitude
        {
            get; set;
        }

        [JsonProperty("latitude")]
        public double Latitude
        {
            get; set;
        }

        [JsonProperty("radius")]
        public long Radius
        {
            get; set;
        }

        [JsonProperty("skip")]
        public long Skip
        {
            get; set;
        }

        [JsonProperty("take")]
        public long Take
        {
            get; set;
        }

    }
}
