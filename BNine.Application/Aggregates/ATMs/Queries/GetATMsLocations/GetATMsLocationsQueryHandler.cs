﻿namespace BNine.Application.Aggregates.ATMs.Queries.GetATMsLocations
{
    using System.Threading;
    using System.Threading.Tasks;
    using AutoMapper;
    using BNine.Application.Abstractions;
    using BNine.Application.Aggregates.ATMs.Models;
    using BNine.Application.Interfaces;
    using BNine.Application.Models;
    using Interfaces.Bank.Client;
    using MediatR;

    public class GetATMsLocationsQueryHandler
        : AbstractRequestHandler
        , IRequestHandler<GetATMsLocationsQuery, PaginatedList<ATMInfoModel>>
    {
        private IBankATMsService BankATMsService
        {
            get;
        }

        public GetATMsLocationsQueryHandler(
            IMediator mediator,
            IBNineDbContext dbContext,
            IMapper mapper,
            ICurrentUserService currentUser,
            IBankATMsService bankATMsService)
            : base(mediator, dbContext, mapper, currentUser)
        {
            BankATMsService = bankATMsService;
        }

        async Task<PaginatedList<ATMInfoModel>> IRequestHandler<GetATMsLocationsQuery, PaginatedList<ATMInfoModel>>.Handle(GetATMsLocationsQuery request, CancellationToken cancellationToken)
        {
            return await BankATMsService.GetATMs(request.Longitude, request.Latitude, request.Radius);
        }
    }
}
