﻿namespace BNine.Application.Aggregates.ATMs.Models
{
    using Newtonsoft.Json;

    // TODO: move classes to separate files

    public class ATMInfoModel
    {
        [JsonProperty("name")]
        public string Name
        {
            get; set;
        }

        [JsonProperty("isDepositAvailable")]
        public bool IsDepositAvailable
        {
            get; set;
        }

        [JsonProperty("isAvailable24Hours")]
        public bool IsAvailable24Hours
        {
            get; set;
        }

        [JsonProperty("isHandicappedAccessible")]
        public bool IsHandicappedAccessible
        {
            get; set;
        }

        [JsonProperty("locationDescription")]
        public string LocationDescription
        {
            get; set;
        }

        [JsonProperty("logoName")]
        public string LogoName
        {
            get; set;
        }

        [JsonProperty("network")]
        public string Network
        {
            get; set;
        }

        [JsonProperty("location")]
        public ATMLocation Location
        {
            get; set;
        }
    }

    public class ATMLocation
    {
        [JsonProperty("address")]
        public ATMAddress Address
        {
            get; set;
        }

        [JsonProperty("coordinates")]
        public ATMCoordinates Coordinates
        {
            get; set;
        }
    }

    public class ATMAddress
    {
        [JsonProperty("city")]
        public string City
        {
            get; set;
        }

        [JsonProperty("line1")]
        public string AddressLine1
        {
            get; set;
        }

        [JsonProperty("line2")]
        public string AddressLine2
        {
            get; set;
        }

        [JsonProperty("state")]
        public string State
        {
            get; set;
        }

        [JsonProperty("postalCode")]
        public int PostalCode
        {
            get; set;
        }

        [JsonProperty("country")]
        public string Country
        {
            get; set;
        }
    }

    public class ATMCoordinates
    {
        [JsonProperty("longitude")]
        public double Longitude
        {
            get; set;
        }

        [JsonProperty("latitude")]
        public double Latitude
        {
            get; set;
        }
    }
}
