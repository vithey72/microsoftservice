﻿namespace BNine.Application.Aggregates.TariffPlans.Events
{
    using MediatR;

    public class TariffPlanChangedEvent : INotification
    {
        public long ClientId
        {
            get; set;
        }
    }
}
