﻿namespace BNine.Application.Aggregates.TariffPlans.Models;

using System;
using static UserClassificationsList;

public record struct UserClassificationsList(Classification Current, Classification? Upcoming = null)
{
    public record struct Classification(int ExternalId, string Name, DateTime? PaymentDate, DateTime? ActiveSince = null);
}

