﻿namespace BNine.Application.Aggregates.TariffPlans.Models;

using AutoMapper;
using BNine.Application.Mappings;
using Domain.Entities.TariffPlan;
using Newtonsoft.Json;

public class TariffPlanOptionModel : TariffPlanModelBase, IMapFrom<TariffPlan>
{
    public int MonthsDuration
    {
        get; set;
    }

    public decimal DiscountPercentage
    {
        get; set;
    }

    public decimal PreDiscountPrice
    {
        get; set;
    }

    public bool IsBaseTariff
    {
        get; set;
    }

    public string Information
    {
        get; set;
    }

    [Obsolete("Left for backwards compatibility. Use the field TariffFamilyImageUrl from parent object.")]
    public string AdvertisementPictureUrl
    {
        get;
        set;
    }

    [JsonIgnore]
    public bool IsHidden
    {
        get;
        set;
    }

    public void Mapping(Profile profile)
    {
        profile.CreateMap<TariffPlan, TariffPlanOptionModel>()
            .ForMember(dst => dst.Id, opt => opt.MapFrom(s => s.Id))
            .ForMember(dst => dst.Name, opt => opt.MapFrom(src => src.Name))
            .ForMember(dst => dst.MonthlyFee, opt => opt.MapFrom(src => src.MonthlyFee))
            .ForMember(dst => dst.MonthsDuration, opt => opt.MapFrom(src => src.MonthsDuration))
            .ForMember(dst => dst.DiscountPercentage, opt => opt.MapFrom(src => src.DiscountPercentage))
            .ForMember(dst => dst.TotalPrice, opt => opt.MapFrom(src => src.TotalPrice))
            .ForMember(dst => dst.PreDiscountPrice, opt => opt.MapFrom(src => src.PreDiscountPrice))
            .ForMember(dst => dst.IsBaseTariff, opt => opt.MapFrom(src => src.IsParent))
            .ForMember(dst => dst.AdvertisementPictureUrl, opt => opt.MapFrom(src => src.AdvertisementPictureUrl))
            .ForMember(dst => dst.IsHidden, opt => opt.MapFrom(src => src.IsDisabled))

            ;
    }
}
