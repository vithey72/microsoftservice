﻿namespace BNine.Application.Aggregates.TariffPlans.Models
{
    using AutoMapper;
    using BNine.Application.Mappings;
    using BNine.Domain.Entities.TariffPlan;
    using BNine.Enums.TariffPlan;
    using CommonModels.DialogElements;
    using CommonModels.Text;
    using Newtonsoft.Json;

    public class TariffPlanModel : TariffPlanModelBase, IMapFrom<TariffPlan>
    {
        public string Subtitle
        {
            get; set;
        }

        public string Information
        {
            get; set;
        }

        public TariffPlanFamily Type
        {
            get; set;
        }

        [Obsolete("Left for backwards compatibility. Use field below.")]
        public string PictureUrl
        {
            get; set;
        }

        public string TariffFamilyImageUrl
        {
            get;
            set;
        }

        public TariffViewmodelImageSubtitle TariffImageSubtitle
        {
            get;
            set;
        } = null;

        public string WelcomeScreenPictureUrl
        {
            get; set;
        }

        public List<TariffFeatureModel> Features
        {
            get; set;
        }

        public TariffPlanOptionModel[] TariffOptions
        {
            get; set;
        }

        public string ButtonText
        {
            get;
            set;
        }

        public bool ButtonIsMuted
        {
            get;
            set;
        }

        public bool ButtonLeadsToDetails
        {
            get;
            set;
        }

        public class TariffViewmodelImageSubtitle
        {
            public FormattedText Text
            {
                get;
                set;
            }

            public string HintText
            {
                get;
                set;
            }

            public HintWithDialogViewModel Hint
            {
                get;
                set;
            }

            public string MainColor
            {
                get;
                set;
            }

            public string GradientColor
            {
                get;
                set;
            }

            public bool HasGradient => GradientColor != null;
        }

        public void Mapping(Profile profile)
        {
            profile.CreateMap<TariffPlan, TariffPlanModel>()
                .ForMember(dst => dst.Id, opt => opt.MapFrom(s => s.Id))
                .ForMember(dst => dst.Name, opt => opt.MapFrom(src => src.ShortName))
                .ForMember(dst => dst.Type, opt => opt.MapFrom(src => src.Type))
                .ForMember(dst => dst.PictureUrl, opt => opt.MapFrom(src => src.PictureUrl))
                .ForMember(dst => dst.WelcomeScreenPictureUrl, opt => opt.MapFrom(src => src.WelcomeScreenPictureUrl))
                .ForMember(dst => dst.Features, opt => opt.MapFrom(src => JsonConvert.DeserializeObject<List<TariffFeatureModel>>(src.Features ?? String.Empty)))
                .ForMember(dst => dst.MonthlyFee, opt => opt.MapFrom(src => src.MonthlyFee))
                .ForMember(dst => dst.TariffFamilyImageUrl, opt => opt.MapFrom(src => src.TariffFamilyImageUrl))
                ;
        }
    }
}
