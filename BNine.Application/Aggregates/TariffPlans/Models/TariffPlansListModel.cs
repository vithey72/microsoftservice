﻿namespace BNine.Application.Aggregates.TariffPlans.Models
{
    public class TariffPlansListModel
    {
        public List<TariffPlanModel> TariffPlans
        {

            get; set;
        }
    }
}
