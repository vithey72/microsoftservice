﻿namespace BNine.Application.Aggregates.TariffPlans.Models;

using System;
using Common;

public class TariffPlansTimelineViewModel
{
    public string Header
    {
        get;
        set;
    }

    public string Subtitle
    {
        get;
        set;
    }

    /// <summary>
    /// In order from the most recent tariff.
    /// </summary>
    public List<BillingTimelineEntry> BillingTimeline
    {
        get;
        set;
    }

    public class BillingTimelineEntry
    {
        public string Header
        {
            get;
            set;
        }

        public DateTime Timestamp
        {
            get;
            set;
        }

        public string DateFormatted => DateHelper.ToAmericanFormat(Timestamp);

        public bool IsChecked
        {
            get;
            set;
        }
    }
}
