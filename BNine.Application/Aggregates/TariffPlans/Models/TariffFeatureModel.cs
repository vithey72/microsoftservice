﻿namespace BNine.Application.Aggregates.TariffPlans.Models
{
    using CommonModels.DialogElements;

    public class TariffFeatureModel
    {
        public string Text
        {
            get; set;
        }

        public HintWithDialogViewModel Hint
        {
            get;
            set;
        }

        public string AdditionalInfo
        {
            get; set;
        }
    }
}
