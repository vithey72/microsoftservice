﻿namespace BNine.Application.Aggregates.TariffPlans.Models;

using BNine.Application.Helpers;
using BNine.Application.Interfaces;
using BNine.Domain.Entities.TariffPlan;
using BNine.Enums;
using BNine.Enums.TariffPlan;

public class UserTariffModel : IUserTariffModel
{
    private const string BaseTariffPlansContainerUrl =
        "https://b9prodaccount.blob.core.windows.net/static-documents-container/TariffPlans/";
    private const string B9BasicTariffIconUrl = BaseTariffPlansContainerUrl + "B9Basic_icon.png";
    private const string B9PremiumTariffIconUrl = BaseTariffPlansContainerUrl + "B9Premium_icon.png";
    private const string MbanqTariffIconUrl = BaseTariffPlansContainerUrl + "Mbanq_icon.png";
    private const string EazyTariffIconUrl = BaseTariffPlansContainerUrl + "Eazy_icon.png";
    private const string PoetryyTariffIconUrl = BaseTariffPlansContainerUrl + "PoetrYY_icon.png";
    private const string UsNationalTariffIconUrl = BaseTariffPlansContainerUrl + "USANational_icon.png";
    private const string QorbisTariffIconUrl = BaseTariffPlansContainerUrl + "QORBIS_icon.png";
    private const string ManaPacificTariffIconUrl = BaseTariffPlansContainerUrl + "ManaPacific_icon.png";
    private const string BooqTariffIconUrl = BaseTariffPlansContainerUrl + "Booq_icon.png";
    private const string P2PTariffIconUrl = BaseTariffPlansContainerUrl + "P2p_icon.png";
    private const string PayHammerTariffIconUrl = BaseTariffPlansContainerUrl + "payhammer_icon.png";

    private readonly TariffPlan _entity;
    private readonly IPartnerProviderService _partnerProvider;

    public UserTariffModel(TariffPlan entity, IPartnerProviderService partnerProvider)
    {
        _entity = entity;
        _partnerProvider = partnerProvider;
    }

    public TariffPlan Entity => _entity;

    public string GetTariffTitle()
    {
        return _entity.Name;
    }

    public string GetTariffPriceText()
    {
        if (_entity.MonthsDuration == 12)
        {
            return $"{CurrencyFormattingHelper.AsAdaptive(_entity.MonthlyFee)} per year";
        }

        return $"{CurrencyFormattingHelper.AsRegular(_entity.MonthlyFee)}/month";
    }

    public string GetTariffImageUrl()
    {
        return _partnerProvider.GetPartner() switch
        {
            PartnerApp.Default => _entity.Type switch
            {
                TariffPlanFamily.Advance => B9BasicTariffIconUrl,
                TariffPlanFamily.Premium => B9PremiumTariffIconUrl,
                _ => string.Empty,
            },
            PartnerApp.MBanqApp => MbanqTariffIconUrl,
            PartnerApp.Eazy => EazyTariffIconUrl,
            PartnerApp.Poetryy => PoetryyTariffIconUrl,
            PartnerApp.USNational => UsNationalTariffIconUrl,
            PartnerApp.Qorbis => QorbisTariffIconUrl,
            PartnerApp.ManaPacific => ManaPacificTariffIconUrl,
            PartnerApp.Booq => BooqTariffIconUrl,
            PartnerApp.P2P => P2PTariffIconUrl,
            PartnerApp.PayHammer => PayHammerTariffIconUrl,
            _ => string.Empty,
        };
    }
}
