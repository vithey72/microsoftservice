﻿namespace BNine.Application.Aggregates.TariffPlans.Models;

using BNine.Enums.TariffPlan;

public class TariffPlanModelBase
{
    public Guid Id
    {
        get; set;
    }

    public string Name
    {
        get; set;
    }

    public decimal MonthlyFee
    {
        get; set;
    }

    public decimal TotalPrice
    {
        get; set;
    }

    public TariffPlanViewModelStatus Status
    {
        get; set;
    }
}
