﻿namespace BNine.Application.Aggregates.TariffPlans.Models;

public class ConfirmationDetailsModel
{
    public string Title
    {
        get; set;
    }

    public string Subtitle
    {
        get; set;
    }

    public string IconUrl
    {
        get; set;
    }

    public bool IsDowngrade => DowngradePoints != null && DowngradePoints.Length > 0;

    public string DowngradeHeader
    {
        get;
        set;
    }

    public string[] DowngradePoints
    {
        get;
        set;
    }
}
