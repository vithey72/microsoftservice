﻿namespace BNine.Application.Aggregates.TariffPlans.Models;

public class OnboardingTariffPurchaseStatus
{
    public bool IsTariffPurchased
    {
        get;
        set;
    }
}
