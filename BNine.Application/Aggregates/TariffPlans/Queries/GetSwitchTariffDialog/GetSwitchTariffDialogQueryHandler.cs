﻿namespace BNine.Application.Aggregates.TariffPlans.Queries.GetSwitchTariffDialog;

using Abstractions;
using AutoMapper;
using Enums.TariffPlan;
using Interfaces;
using MediatR;
using Microsoft.EntityFrameworkCore;

public class GetSwitchTariffDialogQueryHandler
    : AbstractRequestHandler
    , IRequestHandler<GetSwitchTariffDialogQuery, (string header, string[] bulletPoints)>
{
    private readonly ITariffPlanService _tariffPlanService;

    public GetSwitchTariffDialogQueryHandler(
        IMediator mediator,
        IBNineDbContext dbContext,
        IMapper mapper,
        ICurrentUserService currentUser,
        ITariffPlanService tariffPlanService
    )
        : base(mediator, dbContext, mapper, currentUser)
    {
        _tariffPlanService = tariffPlanService;
    }

    /// <summary>
    /// Return dialog model if user is downgrading from Premium to Basic, otherwise return null.
    /// </summary>
    public async Task<(string header, string[] bulletPoints)> Handle(GetSwitchTariffDialogQuery request, CancellationToken cancellationToken)
    {
        var currentTariff = await _tariffPlanService.GetCurrentTariff(CurrentUser.UserId!.Value, cancellationToken);
        if (currentTariff == null || currentTariff.Type != TariffPlanFamily.Premium)
        {
            return default;
        }

        var targetTariff = await DbContext.TariffPlans
            .FirstOrDefaultAsync(tp => tp.Id == request.TariffId, cancellationToken);
        if (targetTariff == null)
        {
            throw new KeyNotFoundException($"Tariff with Id = {request.TariffId} is not present in DB.");
        }

        if (targetTariff.Type == TariffPlanFamily.Advance)
        {
            return
                ("Are you sure you want to go back to your basic plan?",
                    new[]
                    {
                        "Your paycheck cash-out max may turn lower.",
                        "Premium plan allows you to grow your Advance up to $500.",
                        "If you switch back you will also loose access to your credit report via our app."
                    }
                );
        }

        return default;
    }
}
