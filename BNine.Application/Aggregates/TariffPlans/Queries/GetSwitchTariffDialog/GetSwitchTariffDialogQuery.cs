﻿namespace BNine.Application.Aggregates.TariffPlans.Queries.GetSwitchTariffDialog;

using System;
using MediatR;

public record GetSwitchTariffDialogQuery(Guid TariffId) : IRequest<(string header, string[] bulletPoints)>;
