﻿namespace BNine.Application.Aggregates.TariffPlans.Queries.GetTariffsTimeline;

using Abstractions;
using AutoMapper;
using Common;
using Domain.Entities.TariffPlan;
using Enums.TariffPlan;
using Helpers;
using Interfaces;
using Interfaces.Bank.Administration;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Models;

public class GetTariffsTimelineQueryHandler
    : AbstractRequestHandler
    , IRequestHandler<GetTariffsTimelineQuery, TariffPlansTimelineViewModel>
{
    private readonly IPartnerProviderService _partnerProvider;
    private readonly IBankClassificationsService _classificationsService;
    private readonly ITariffPlanService _tariffPlanService;

    public GetTariffsTimelineQueryHandler(
        IMediator mediator,
        IBNineDbContext dbContext,
        IMapper mapper,
        ICurrentUserService currentUser,
        IPartnerProviderService partnerProvider,
        IBankClassificationsService classificationsService,
        ITariffPlanService tariffPlanService
    ) :
        base(mediator, dbContext, mapper, currentUser)
    {
        _partnerProvider = partnerProvider;
        _classificationsService = classificationsService;
        _tariffPlanService = tariffPlanService;
    }

    public async Task<TariffPlansTimelineViewModel> Handle(GetTariffsTimelineQuery request, CancellationToken cancellationToken)
    {
        var user = await DbContext.Users.SingleAsync(x => x.Id == CurrentUser.UserId);
        var classificationsTimeline = await _classificationsService
            .GetUserClassifications(user.ExternalClientId!.Value, cancellationToken);

        var activeUserTariff = await _tariffPlanService.GetTariffByClassificationId(classificationsTimeline.Current.ExternalId, cancellationToken);

        if (classificationsTimeline.Upcoming == null)
        {
            if (_partnerProvider.GetPartner() is Enums.PartnerApp.Qorbis)
            {
                var currentUserTariffPlan = await DbContext.UserTariffPlans
                        .AsNoTracking()
                        .Where(x => x.UserId == CurrentUser.UserId)
                        .FirstOrDefaultAsync(cancellationToken);
                return BuildFlatModel(activeUserTariff, currentUserTariffPlan?.NextPaymentDate ?? DateTime.MaxValue, request.CalledAfterPurchase);
            }
            return BuildFlatModel(activeUserTariff, classificationsTimeline.Current.PaymentDate ?? DateTime.MaxValue, request.CalledAfterPurchase);
        }

        var upcomingUserTariff = await _tariffPlanService.GetTariffByClassificationId(classificationsTimeline.Upcoming.Value!.ExternalId, cancellationToken);

        return BuildModelWithTimeline(
            activeUserTariff, classificationsTimeline.Current.ActiveSince ?? DateTime.MinValue,
            upcomingUserTariff, classificationsTimeline.Upcoming.Value.PaymentDate ?? DateTime.MaxValue,
            request.CalledAfterPurchase);
    }

    private TariffPlansTimelineViewModel BuildModelWithTimeline(TariffPlan activePlan, DateTime activePlanStart,
        TariffPlan upcomingPlan, DateTime upcomingPlanStart, bool calledAfterPurchase)
    {
        var response = new TariffPlansTimelineViewModel
        {
            Header = calledAfterPurchase ? "Your Plan has been successfully changed!" : "Your plan details",
            BillingTimeline = new List<TariffPlansTimelineViewModel.BillingTimelineEntry>(),
        };

        // free month merge-crutch
        if (_partnerProvider.GetPartner().RequiresSpecialFreeTariffHandling()
            && activePlan.Type == upcomingPlan.Type
            && activePlan.Type == TariffPlanFamily.Advance)
        {
            return BuildFlatModel(upcomingPlan, upcomingPlanStart, calledAfterPurchase);
        }

        response.BillingTimeline.Add(new TariffPlansTimelineViewModel.BillingTimelineEntry
        {
            Header = GetPlanHeader(activePlan, true),
            Timestamp = activePlanStart.AddHours(12), // crutch
            IsChecked = true,
        });
        response.BillingTimeline.Add(new TariffPlansTimelineViewModel.BillingTimelineEntry
        {
            Header = GetPlanHeader(upcomingPlan, false),
            Timestamp = upcomingPlanStart.AddHours(12), // crutch
            IsChecked = false,
        });

        return response;

        string GetPlanHeader(TariffPlan plan, bool active)
        {
            var verb = active ? "Started" : "Will start";
            return StringProvider.ReplaceCustomizedStrings($"{verb} {plan.Name} for {CurrencyFormattingHelper.AsRegular(plan.TotalPrice)}", _partnerProvider);
        }
    }

    private TariffPlansTimelineViewModel BuildFlatModel(TariffPlan tariff, DateTime nextPaymentDate, bool calledAfterPurchase)
    {
        if (tariff == null)
        {
            return new TariffPlansTimelineViewModel
            {
                Header = "No information to display",
            };
        }
        return new TariffPlansTimelineViewModel
        {
            Header = calledAfterPurchase
                ? $"Your {StringProvider.ReplaceCustomizedStrings(tariff.ShortName, _partnerProvider)} has been activated!"
                : "Your plan details",
            Subtitle = $"Next payment date: {DateHelper.ToAmericanFormat(nextPaymentDate)} —" +
                       $" {CurrencyFormattingHelper.AsRegular(tariff.TotalPrice)}",
        };
    }
}
