﻿namespace BNine.Application.Aggregates.TariffPlans.Queries.GetTariffsTimeline
{
    using MediatR;
    using Models;
    using Newtonsoft.Json;

    public class GetTariffsTimelineQuery : IRequest<TariffPlansTimelineViewModel>
    {
        [JsonIgnore]
        public bool CalledAfterPurchase
        {
            get; set;
        }
    }
}
