﻿namespace BNine.Application.Aggregates.TariffPlans.Queries.GetTariffPlansInfo
{
    using System.Threading;
    using System.Threading.Tasks;
    using AutoMapper;
    using BNine.Application.Aggregates.TariffPlans.Models;
    using BNine.Application.Interfaces;
    using BNine.Domain.Entities.TariffPlan;
    using BNine.Enums.TariffPlan;
    using Common;
    using Enums;
    using Helpers;
    using MediatR;
    using Microsoft.EntityFrameworkCore;
    using Newtonsoft.Json;

    /// <summary>
    /// https://www.figma.com/file/LzJlM29xwzhGphCEB1DRUe/iOS?node-id=18829%3A266719
    /// </summary>
    public class GetTariffPlansInfoQueryHandler : IRequestHandler<GetTariffPlansInfoQuery, TariffPlansListModel>
    {
        private readonly IBNineDbContext _dbContext;
        private readonly IMapper _mapper;
        private readonly ICurrentUserService _currentUser;
        private readonly IPartnerProviderService _partnerProvider;
        private readonly ITariffPlanService _tariffPlanService;

        public GetTariffPlansInfoQueryHandler(
            IBNineDbContext dbContext,
            IMapper mapper,
            ICurrentUserService currentUser,
            IPartnerProviderService partnerProvider,
            ITariffPlanService tariffPlanService
            )
        {
            _dbContext = dbContext;
            _mapper = mapper;
            _currentUser = currentUser;
            _partnerProvider = partnerProvider;
            _tariffPlanService = tariffPlanService;
        }

        public async Task<TariffPlansListModel> Handle(GetTariffPlansInfoQuery request, CancellationToken cancellationToken)
        {
            var allTariffPlans = await _dbContext.TariffPlans
                   .AsNoTracking()
                   .Where(t => !t.IsDisabled)
                   .ToListAsync(cancellationToken);

            var (currentTariff, cyclePaymentDate, upcomingTariff, switchDate)
                = await _tariffPlanService.GetCurrentAndUpcomingTariffs(_currentUser.UserId!.Value, cancellationToken);

            // https://bninecom.atlassian.net/browse/B9-4741
            if (_partnerProvider.GetPartner() == PartnerApp.MBanqApp)
            {
                ApplyCrutches(new[] { currentTariff, upcomingTariff });
                ApplyCrutches(allTariffPlans);
            }

            var tariffFamiliesGroup = allTariffPlans.GroupBy(tp => tp.Type).ToArray();

            var resultTariffPlanModels = new List<TariffPlanModel>();

            var queueIsFull = upcomingTariff != null;

            foreach (var tariffGroup in tariffFamiliesGroup)
            {
                var parentTariff = tariffGroup.FirstOrDefault(tp => tp.IsParent);
                if (parentTariff == null) { continue; }

                var activeEntry = tariffGroup.FirstOrDefault(x => x.Id == currentTariff.Id);
                var expectedNextInTheFamily = tariffGroup.FirstOrDefault(x => x.Id == upcomingTariff?.Id);
                var biggestDiscount = tariffGroup.MaxBy(x => x.DiscountPercentage);

                // either the other option or the only option
                var potentialSwapTarget = parentTariff != activeEntry ? parentTariff : biggestDiscount;

                var familyHeadliner = activeEntry ?? expectedNextInTheFamily ?? parentTariff;

                var parentTariffVm = _mapper.Map<TariffPlanModel>(familyHeadliner);

                var familyIsActive = activeEntry != null;
                var familyIsUpcoming = expectedNextInTheFamily != null;

                var familyHasOptions = tariffGroup.Count() != 1;
                var isDowngrade = currentTariff.TotalPrice > potentialSwapTarget.TotalPrice;

                parentTariffVm.Subtitle = InferSubtitle(familyIsActive, familyIsUpcoming);

                parentTariffVm.Information = InferInfoLevelOne(potentialSwapTarget, currentTariff, upcomingTariff, switchDate,
                    queueIsFull, familyHasOptions, familyIsActive, isDowngrade, familyIsUpcoming);
                parentTariffVm.Information += InferInfoLevelTwo(currentTariff, upcomingTariff, familyIsActive,
                    cyclePaymentDate ?? switchDate, queueIsFull);

                SelectTariffPicture(parentTariffVm, currentTariff);
                parentTariffVm.TariffImageSubtitle = InferTariffImageSubtitle(familyIsActive, familyHeadliner);

                (parentTariffVm.ButtonText, parentTariffVm.ButtonIsMuted, parentTariffVm.ButtonLeadsToDetails)
                    = InferButton(familyIsActive, familyIsUpcoming, familyHasOptions, queueIsFull);

                // a parent is an option of itself as well
                var childTariffsVms = tariffGroup
                    .Select(tariff => _mapper.Map<TariffPlanOptionModel>(tariff))
                    .OrderBy(tariff => tariff.MonthlyFee)
                    .ToArray();

                AddOptionsInformation(childTariffsVms, currentTariff);

                parentTariffVm.Status = familyIsActive
                    ? TariffPlanViewModelStatus.Active
                    : familyIsUpcoming
                        ? TariffPlanViewModelStatus.Upcoming
                        : TariffPlanViewModelStatus.Inactive;

                parentTariffVm.TariffOptions = childTariffsVms.Where(cto => !cto.IsHidden).ToArray();

                foreach (var planOption in parentTariffVm.TariffOptions)
                {
                    planOption.Status = InferOptionStatus(planOption, currentTariff, upcomingTariff);
                }

                resultTariffPlanModels.Add(parentTariffVm);
            }

            return new TariffPlansListModel
            {
                TariffPlans = resultTariffPlanModels,
            };
        }

        private string InferInfoLevelOne(TariffPlan potentialSwap, TariffPlan current,
            TariffPlan upcoming, DateTime? switchDate, bool queueIsFull, bool familyHasOptions, bool familyIsActive,
            bool isDowngrade, bool familyIsUpcoming)
        {
            if (queueIsFull)
            {
                if (!familyIsUpcoming)
                {
                    return $"You will be able to activate the {potentialSwap.ShortName} for {potentialSwap.MonthsDuration} month(s) when the current plan ends";
                }

                return $"{current.ShortName} for {current.MonthsDuration} months(s) available until {DateHelper.ToAmericanFormat(switchDate ?? DateTime.MaxValue)}\n" +
                       $"{upcoming.ShortName} for {upcoming.MonthsDuration} month(s) will be activated on {DateHelper.ToAmericanFormat(switchDate ?? DateTime.MaxValue)}";
            }

            if (familyIsActive)
            {
                if (!familyHasOptions)
                {
                    return string.Empty;
                }

                if (isDowngrade)
                {
                    return
                        $"If you switch to the {potentialSwap.ShortName} for " +
                        $"{GetMonthsPlurality(potentialSwap.MonthsDuration)}, it will " +
                        "begin once your current cycle ends";
                }

                return ThePaymentWillBeChargedImmediately(potentialSwap, true);
            }

            return isDowngrade
                ? $"If you activate the {potentialSwap.Name}, it will begin once your current cycle ends"
                : ThePaymentWillBeChargedImmediately(potentialSwap, false);
        }

        private string InferInfoLevelTwo(TariffPlan current, TariffPlan upcoming, bool familyIsActive, DateTime? paymentDate, bool queueIsFull)
        {
            if (upcoming == null && current.TotalPrice == 0)
            {
                return string.Empty;
            }

            if (familyIsActive || queueIsFull)
            {
                return IndentInfoText($"Next payment date: {DateHelper.ToAmericanFormat(paymentDate ?? DateTime.MaxValue)} — " +
                                      $"{CurrencyFormattingHelper.AsRegular((upcoming ?? current).TotalPrice)}");
            }

            return string.Empty;
        }

        private string IndentInfoText(string information)
        {
            var lines = information.Split('\n');
            return string.Join("\n", lines.Select(x => $"<p>{x}</p>"));
        }

        private (string ButtonText, bool ButtonIsMuted, bool ButtonLeadsToDetails) InferButton(
            bool familyIsActive, bool familyIsUpcoming, bool familyHasOptions, bool queueIsFull)
        {
            if (queueIsFull && familyIsUpcoming)
            {
                return ("DETAILS", false, true);
            }

            if (queueIsFull)
            {
                return ("ACTIVATE PLAN", true, false);
            }

            if (!familyIsActive)
            {
                return ("ACTIVATE PLAN", false, false);
            }

            if (!familyHasOptions)
            {
                return ("YOUR CURRENT PLAN", true, false);
            }

            return ("CHANGE PLAN", false, false);

        }

        private string InferSubtitle(bool familyIsActive, bool familyIsUpcoming)
        {
            if (familyIsActive)
            {
                return "Your current plan";
            }

            if (familyIsUpcoming)
            {
                return "Your next plan";
            }

            return "Choose the Plan that's right for you";
        }

        private static TariffPlanModel.TariffViewmodelImageSubtitle InferTariffImageSubtitle(bool familyIsActive, TariffPlan familyHeadliner)
        {
            return familyIsActive
                ? JsonConvert.DeserializeObject<TariffPlanModel.TariffViewmodelImageSubtitle>(familyHeadliner.ActiveImageSubtitleObject ?? "{}")
                : JsonConvert.DeserializeObject<TariffPlanModel.TariffViewmodelImageSubtitle>(familyHeadliner.AdvertisementImageSubtitleObject ?? "{}");
        }

        private static string ThePaymentWillBeChargedImmediately(TariffPlan potentialSwap, bool isSameFamily)
        {
            var verb = isSameFamily ? "activate" : "switch to";
            if (potentialSwap.MonthsDuration == 1)
            {
                return $"If you {verb} the {potentialSwap.ShortName}, the payment will be charged immediately";
            }

            return $"If you {verb} the {potentialSwap.ShortName} for {potentialSwap.MonthsDuration} months, the payment will be charged immediately";
        }

        private static string GetMonthsPlurality(int monthsCountActive)
        {
            var ending = monthsCountActive == 1 ? "month" : "months";
            return monthsCountActive + " " + ending;
        }

        private void AddOptionsInformation(TariffPlanOptionModel[] childTariffsVms, TariffPlan activePlan)
        {
            foreach (var entry in childTariffsVms)
            {
                if (!activePlan.IsDisabled && entry.TotalPrice > activePlan.TotalPrice)
                {
                    entry.Information = "You will be charged immediately";
                }
            }
        }

        [Obsolete("Now it's a same picture per family, but with different text rendered over it")]
        private void SelectTariffPicture(TariffPlanModel parentTariff, TariffPlan activePlan)
        {
            if (activePlan.Id != parentTariff.Id && activePlan.Type == parentTariff.Type)
            {
                parentTariff.PictureUrl = activePlan.PictureUrl;
            }
        }


        private TariffPlanViewModelStatus InferOptionStatus(TariffPlanOptionModel planOption, TariffPlan currentTariff,
            TariffPlan upcomingTariff)
        {
            if (planOption.Id == currentTariff.Id)
            {
                return TariffPlanViewModelStatus.Active;
            }

            if (upcomingTariff != null && planOption.Id == upcomingTariff.Id)
            {
                return TariffPlanViewModelStatus.Upcoming;
            }

            return TariffPlanViewModelStatus.Inactive;
        }

        private static void ApplyCrutches(IEnumerable<TariffPlan> tariffPlans)
        {
            foreach (var tariffPlan in tariffPlans)
            {
                ApplyRenamingForMbanqAppCrutch(tariffPlan);
            }
        }

        public static void ApplyRenamingForMbanqAppCrutch(TariffPlan tariffPlan)
        {
            if (tariffPlan == null)
            {
                return;
            }

            if (tariffPlan.Type == TariffPlanFamily.Advance)
            {
                tariffPlan.Name = "MBANQ Basic Plan";
                tariffPlan.ShortName = "Basic Plan";
                tariffPlan.AdvertisementPictureUrl = tariffPlan.WelcomeScreenPictureUrl = tariffPlan.PictureUrl = tariffPlan.TariffFamilyImageUrl =
                    "https://b9devaccount.blob.core.windows.net/static-documents-container/TariffPlans/mbanq_basic_plan.png";
                tariffPlan.ActiveImageSubtitleObject = "{}";
                tariffPlan.AdvertisementImageSubtitleObject = "{}";
                tariffPlan.Features =
                    @"[
{
  ""additionalInfo"" : null,
  ""text"" : ""MBANQ Visa® Debit Card with \nup to 5% cashback""
},
{
  ""additionalInfo"" : null,
  ""text"" : ""Instant transfers to MBANQ Members""
}]";
            }

            if (tariffPlan.Type == TariffPlanFamily.Premium)
            {
                tariffPlan.Name = "MBANQ Premium Plan";
                tariffPlan.ShortName = "Premium Plan";
                tariffPlan.AdvertisementPictureUrl = tariffPlan.WelcomeScreenPictureUrl = tariffPlan.PictureUrl = tariffPlan.TariffFamilyImageUrl =
                    "https://b9devaccount.blob.core.windows.net/static-documents-container/TariffPlans/mbanq_premium_plan.png";
                tariffPlan.ActiveImageSubtitleObject = "{}";
                tariffPlan.AdvertisementImageSubtitleObject = "{}";
                tariffPlan.Features =
                    @"[
{
  ""additionalInfo"" : null,
  ""text"" : ""Everything in the \nMBANQ Basic Plan — plus:""
},
{
  ""additionalInfo"" : ""Credit Report is not a product of Evolve Bank & Trust, and offered by third party provider and affiliate of MBANQ."",
  ""text"" : ""Credit Report""
},
{
  ""additionalInfo"" : ""Credit Score is not a product of Evolve Bank & Trust, and offered by third party provider and affiliate of MBANQ."",
  ""text"" : ""Credit Score""
},
{
  ""additionalInfo"" : ""• Dedicated priority answer line \n• We will answer you in an hour \n\nOur Customer Support Team is active the following hours: \n• Mon-Fri: 6 AM to 9 PM Pacific Standard Time (9 AM to 12 AM Eastern Standard Time) \n• Sat-Sun: 6 AM to 6 PM Pacific Standard Time (9 AM to 9 PM Eastern Standard Time) \n\nTime of response may depend on external factors."",
  ""text"" : ""Premium Support""
}
]";
            }
        }
    }
}
