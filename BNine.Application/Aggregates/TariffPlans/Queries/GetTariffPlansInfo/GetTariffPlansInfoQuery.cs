﻿namespace BNine.Application.Aggregates.TariffPlans.Queries;

using BNine.Application.Aggregates.TariffPlans.Models;
using MediatR;

public class GetTariffPlansInfoQuery : IRequest<TariffPlansListModel>
{
}
