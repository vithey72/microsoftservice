﻿namespace BNine.Application.Aggregates.TariffPlans.Queries.PurchasePlanOnOnboarding;

using Abstractions;
using Behaviours;
using CommonModels.Dialog;
using MediatR;

[Sequential]
public class PurchasePlanOnOnboardingCommand
    : MBanqSelfServiceUserRequest
    , IRequest<EitherDialog<GenericSuccessDialog, GenericFailDialog>>
{
    public Guid TariffPlanId
    {
        get;
        set;
    }

    public int ExternalCardId
    {
        get;
        set;
    }
}
