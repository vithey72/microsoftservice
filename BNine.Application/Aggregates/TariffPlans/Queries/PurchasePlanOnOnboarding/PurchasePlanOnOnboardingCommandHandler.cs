﻿namespace BNine.Application.Aggregates.TariffPlans.Queries.PurchasePlanOnOnboarding;

using Abstractions;
using AutoMapper;
using CommonModels.Dialog;
using Constants;
using Helpers;
using Interfaces;
using Interfaces.Bank.Administration;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Transfers.Commands.ReplenishFromExternalCard;
using IBankSavingAccountsService = Interfaces.Bank.Client.IBankSavingAccountsService;

public class PurchasePlanOnOnboardingCommandHandler
    : AbstractRequestHandler
    , IRequestHandler<PurchasePlanOnOnboardingCommand, EitherDialog<GenericSuccessDialog, GenericFailDialog>>
{
    private readonly IBankSavingAccountsService _savingAccountsService;
    private readonly ITariffPlanService _tariffPlanService;
    private readonly IPartnerProviderService _partnerProvider;
    private readonly ILogger<PurchasePlanOnOnboardingCommandHandler> _logger;
    private readonly IBankExternalCardsService _externalCardsService;

    public PurchasePlanOnOnboardingCommandHandler(
        IMediator mediator,
        IBNineDbContext dbContext,
        IMapper mapper,
        ICurrentUserService currentUser,
        IBankSavingAccountsService savingAccountsService,
        ITariffPlanService tariffPlanService,
        IPartnerProviderService partnerProvider,
        ILogger<PurchasePlanOnOnboardingCommandHandler> logger,
        IBankExternalCardsService externalCardsService
        ) : base(mediator, dbContext, mapper, currentUser)
    {
        _savingAccountsService = savingAccountsService;
        _tariffPlanService = tariffPlanService;
        _partnerProvider = partnerProvider;
        _logger = logger;
        _externalCardsService = externalCardsService;
    }

    /// <summary>
    /// Pulls required amount from card, and activates the tariff.
    /// If pull fails, removes the card from MBanq.
    /// </summary>
    public async Task<EitherDialog<GenericSuccessDialog, GenericFailDialog>> Handle(
        PurchasePlanOnOnboardingCommand request, CancellationToken cancellationToken)
    {
        var user = await DbContext
            .Users
            .Include(u => u.CurrentAccount)
            .SingleAsync(x => x.Id == CurrentUser.UserId, cancellationToken);

        var tariff = await DbContext
            .TariffPlans
            .SingleOrDefaultAsync(
                x => x.Id == request.TariffPlanId,
                cancellationToken);

        if (tariff == null)
        {
            throw new InvalidOperationException($"Tariff with Id = {request.TariffPlanId} does not exist.");
        }

        var tariffCost = tariff.TotalPrice;
        var currentBalance = await _savingAccountsService
            .GetSavingAccountInfo(user.CurrentAccount.ExternalId, request.MbanqAccessToken, request.Ip);
        if (currentBalance.AvailableBalance < tariffCost)
        {
            try
            {
                await Mediator.Send(new ReplenishFromExternalCardCommand
                {
                    Amount = tariffCost,
                    CardId = request.ExternalCardId,
                    Ip = request.Ip,
                    MbanqAccessToken = request.MbanqAccessToken,
                }, CancellationToken.None);
            }
            catch // already logged to DB
            {
                await _externalCardsService.DeleteCard(user.ExternalClientId!.Value, request.ExternalCardId);
                return GetCommonFailModel();
            }
        }

        await _tariffPlanService.SwitchTariffPlan(user.Id, tariff.Id);

        return new EitherDialog<GenericSuccessDialog, GenericFailDialog>(
            new GenericSuccessDialog
            {
                Header = new GenericDialogHeaderViewModel
                {
                    Title = "Payment",
                },
                Body = new GenericDialogBodyViewModel
                {
                    Subtitle = $"Complete\n\nYour payment has been accepted and you are now ready to " +
                               $"use {StringProvider.GetPartnerName(_partnerProvider)} Mobile Banking",
                    ButtonText = "DONE",
                }
            });
    }

    private EitherDialog<GenericSuccessDialog, GenericFailDialog> GetCommonFailModel()
    {
        return new EitherDialog<GenericSuccessDialog, GenericFailDialog>(
            new GenericFailDialog
            {
                ErrorCode = ApiResponseErrorCodes.InsufficientFunds,
                Header = new GenericDialogHeaderViewModel
                {
                    Title = "Payment",
                },
                Body = new GenericDialogBodyViewModel
                {
                    Title = "Transfer Failed",
                    Subtitle = "Insufficient funds",
                    ButtonText = "TRY ANOTHER CARD",
                }
            });
    }
}
