﻿namespace BNine.Application.Aggregates.TariffPlans.Queries.GetConfirmationDetails;

using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using BNine.Application.Abstractions;
using BNine.Application.Aggregates.TariffPlans.Models;
using BNine.Application.Exceptions;
using BNine.Application.Interfaces;
using BNine.Domain.Entities.TariffPlan;
using BNine.Enums.TariffPlan;
using Enums;
using GetSwitchTariffDialog;
using GetTariffPlansInfo;
using Helpers;
using MediatR;
using Microsoft.EntityFrameworkCore;

public class GetConfirmationDetailsQueryHandler
    : AbstractRequestHandler
    , IRequestHandler<GetConfirmationDetailsQuery, ConfirmationDetailsModel>
{
    private readonly IPartnerProviderService _partnerProvider;
    private readonly ITariffPlanService _tariffPlanService;

    public GetConfirmationDetailsQueryHandler(IMediator mediator,
        IBNineDbContext dbContext,
        IMapper mapper,
        ICurrentUserService currentUser,
        IPartnerProviderService partnerProvider,
        ITariffPlanService tariffPlanService
    )
        : base(mediator, dbContext, mapper, currentUser)
    {
        _partnerProvider = partnerProvider;
        _tariffPlanService = tariffPlanService;
    }

    public async Task<ConfirmationDetailsModel> Handle(GetConfirmationDetailsQuery request, CancellationToken cancellationToken)
    {
        var tariffPlan = await DbContext
            .TariffPlans
            .AsNoTracking()
            .FirstOrDefaultAsync(t => t.Id == request.TariffPlanId, cancellationToken);
        if (tariffPlan == null)
        {
            throw new NotFoundException(nameof(TariffPlan), request.TariffPlanId);
        }

        var currentPlan = await _tariffPlanService.GetCurrentTariff(CurrentUser.UserId!.Value, cancellationToken);
        var confirmationDetails = new ConfirmationDetailsModel
        {
            IconUrl = tariffPlan.IconUrl
        };

        if (_partnerProvider.GetPartner() == PartnerApp.MBanqApp)
        {
            GetTariffPlansInfoQueryHandler.ApplyRenamingForMbanqAppCrutch(tariffPlan);
        }

        var totalFee = $"{CurrencyFormattingHelper.AsRegular(tariffPlan.TotalPrice)}";
        switch (tariffPlan.Type)
        {
            case TariffPlanFamily.Advance:
                {
                    var downgradeDialog = await Mediator.Send(new GetSwitchTariffDialogQuery(request.TariffPlanId));
                    confirmationDetails.Title = $"Switch to {tariffPlan.Name}?";
                    confirmationDetails.Subtitle = "The plan will begin as soon as the current cycle ends";
                    confirmationDetails.DowngradePoints = downgradeDialog.bulletPoints;
                    confirmationDetails.DowngradeHeader = downgradeDialog.header;
                    //TODO: Make configurable from database
                    confirmationDetails.IconUrl = "https://b9prodaccount.blob.core.windows.net/static-documents-container/TariffPlansActionIcons/downgradeTariffPlanIcon.png";
                    break;
                }
            case TariffPlanFamily.Premium:
                {
                    if (tariffPlan.TotalPrice > currentPlan.TotalPrice)
                    {
                        confirmationDetails.Title = $"Activate the {tariffPlan.Name}?";
                        confirmationDetails.Subtitle = $"{totalFee} will be charged right after you confirm the " +
                                                       "update to your current plan. In the event you have less than " +
                                                       $"{totalFee} in your account, the change will not be completed.";
                    }
                    else
                    {
                        confirmationDetails.Title = $"Switch to {tariffPlan.Name}?";
                        confirmationDetails.Subtitle = "The plan will begin as soon as the current cycle ends";
                    }
                    break;
                }
        }

        return confirmationDetails;
    }
}
