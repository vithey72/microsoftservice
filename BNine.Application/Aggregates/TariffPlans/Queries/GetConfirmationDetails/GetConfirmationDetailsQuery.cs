﻿namespace BNine.Application.Aggregates.TariffPlans.Queries.GetConfirmationDetails
{
    using BNine.Application.Aggregates.TariffPlans.Models;
    using MediatR;

    public class GetConfirmationDetailsQuery : IRequest<ConfirmationDetailsModel>
    {
        public Guid TariffPlanId
        {
            get; set;
        }
    }
}
