﻿namespace BNine.Application.Aggregates.TariffPlans.Queries.SwitchTariff
{
    using MediatR;
    using Models;
    using Newtonsoft.Json;

    public class SwitchTariffQuery : IRequest<TariffPlansTimelineViewModel>
    {
        [JsonProperty("id")]
        public Guid Id
        {
            get; set;
        }
    }
}
