﻿namespace BNine.Application.Aggregates.TariffPlans.Queries.SwitchTariff;

using Abstractions;
using AutoMapper;
using GetTariffsTimeline;
using Interfaces;
using MediatR;
using Models;

public class SwitchTariffQueryHandler : AbstractRequestHandler,
    IRequestHandler<SwitchTariffQuery, TariffPlansTimelineViewModel>
{
    private ITariffPlanService TariffPlanService
    {
        get;
    }

    public SwitchTariffQueryHandler(ITariffPlanService tariffPlanService,
        IMediator mediator,
        IBNineDbContext dbContext,
        IMapper mapper,
        ICurrentUserService currentUser) :
        base(mediator, dbContext, mapper, currentUser)
    {
        TariffPlanService = tariffPlanService;
    }

    public async Task<TariffPlansTimelineViewModel> Handle(SwitchTariffQuery request, CancellationToken cancellationToken)
    {
        await TariffPlanService.SwitchTariffPlan(CurrentUser.UserId.Value, request.Id);
        return await Mediator.Send(new GetTariffsTimelineQuery { CalledAfterPurchase = true });
    }
}
