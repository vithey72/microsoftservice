﻿namespace BNine.Application.Aggregates.TariffPlans.Commands.SwitchTariff;

using Behaviours;
using MediatR;
using Newtonsoft.Json;

[Sequential]
public class SwitchTariffCommand : IRequest<Unit>
{
    [JsonProperty("id")]
    public Guid Id
    {
        get; set;
    }
}
