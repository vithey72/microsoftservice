﻿namespace BNine.Application.Aggregates.TariffPlans.Commands.SwitchTariff
{
    using System.Threading;
    using System.Threading.Tasks;
    using AutoMapper;
    using BNine.Application.Abstractions;
    using BNine.Application.Interfaces;
    using MediatR;

    public class SwitchTariffCommandHandler : AbstractRequestHandler,
            IRequestHandler<SwitchTariffCommand, Unit>
    {
        private ITariffPlanService TariffPlanService
        {
            get;
        }

        public SwitchTariffCommandHandler(ITariffPlanService tariffPlanService,
            IMediator mediator,
            IBNineDbContext dbContext,
            IMapper mapper,
            ICurrentUserService currentUser) :
            base(mediator, dbContext, mapper, currentUser)
        {
            TariffPlanService = tariffPlanService;
        }

        public async Task<Unit> Handle(SwitchTariffCommand request, CancellationToken cancellationToken)
        {
            await TariffPlanService.SwitchTariffPlan(CurrentUser.UserId.Value, request.Id);
            return Unit.Value;
        }
    }
}
