﻿namespace BNine.Application.Aggregates.TariffPlans.EventHandlers
{
    using System.Threading;
    using System.Threading.Tasks;
    using BNine.Application.Aggregates.TariffPlans.Events;
    using BNine.Application.Interfaces;
    using MediatR;

    public class TariffPlanChangedEventHandler : INotificationHandler<TariffPlanChangedEvent>
    {
        private ITariffPlanService TariffPlanService
        {
            get;
        }

        public TariffPlanChangedEventHandler(ITariffPlanService tariffPlanService)
        {
            TariffPlanService = tariffPlanService;
        }

        public async Task Handle(TariffPlanChangedEvent notification, CancellationToken cancellationToken)
        {
            await TariffPlanService.UpdateUserTariffPlan(notification.ClientId);
        }
    }
}
