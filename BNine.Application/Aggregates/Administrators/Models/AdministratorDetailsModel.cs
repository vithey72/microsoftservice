﻿namespace BNine.Application.Aggregates.Administrators.Models
{
    using System;
    using AutoMapper;
    using BNine.Application.Mappings;
    using BNine.Domain.Entities.Administrator;

    public class AdministratorDetailsModel : IMapFrom<Administrator>
    {
        public Guid Id
        {
            get; set;
        }

        public string Name
        {
            get; set;
        }

        public void Mapping(Profile profile) => profile.CreateMap<Administrator, AdministratorDetailsModel>()
            .ForMember(d => d.Id, o => o.MapFrom(s => s.Id))
            .ForMember(d => d.Name, o => o.MapFrom(s => s.Name));
    }
}
