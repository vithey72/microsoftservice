﻿namespace BNine.Application.Aggregates.Administrators.Queries.GetAdministratorByExternalProvider
{
    using System.Linq;
    using System.Threading;
    using System.Threading.Tasks;
    using AutoMapper;
    using AutoMapper.QueryableExtensions;
    using BNine.Application.Abstractions;
    using BNine.Application.Aggregates.Administrators.Models;
    using BNine.Application.Interfaces;
    using MediatR;
    using Microsoft.EntityFrameworkCore;

    public class GetAdministratorByExternalProviderQueryHandler
        : AbstractRequestHandler, IRequestHandler<GetAdministratorByExternalProviderQuery, AdministratorDetailsModel>
    {
        public GetAdministratorByExternalProviderQueryHandler(IMediator mediator, IBNineDbContext dbContext, IMapper mapper, ICurrentUserService currentUser) : base(mediator, dbContext, mapper, currentUser)
        {
        }

        public async Task<AdministratorDetailsModel> Handle(GetAdministratorByExternalProviderQuery request, CancellationToken cancellationToken)
        {
            var administrator = await DbContext.Administrators
                .Where(x => x.ExternalIdentities.Any(identity => identity.Id.Equals(request.ProviderId) && identity.Provider.Equals(request.Provider)))
                .ProjectTo<AdministratorDetailsModel>(Mapper.ConfigurationProvider)
                .FirstOrDefaultAsync(cancellationToken);

            return administrator;
        }
    }
}
