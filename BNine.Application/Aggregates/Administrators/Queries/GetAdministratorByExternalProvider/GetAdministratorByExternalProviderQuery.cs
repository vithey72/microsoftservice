﻿namespace BNine.Application.Aggregates.Administrators.Queries.GetAdministratorByExternalProvider
{
    using BNine.Application.Aggregates.Administrators.Models;
    using MediatR;

    public class GetAdministratorByExternalProviderQuery : IRequest<AdministratorDetailsModel>
    {
        public GetAdministratorByExternalProviderQuery(string provider, string providerId)
        {
            Provider = provider;
            ProviderId = providerId;
        }

        public string Provider
        {
            get; set;
        }

        public string ProviderId
        {
            get; set;
        }
    }
}
