﻿namespace BNine.Application.Aggregates.Administrators.Commands.CreateAdministratorWithExternalIdentity
{
    using System.Collections.Generic;
    using AutoMapper;
    using BNine.Application.Aggregates.Administrators.Models;
    using BNine.Application.Mappings;
    using BNine.Domain.Entities.Administrator;
    using MediatR;

    public class CreateAdministratorWithExternalIdentityCommand : IRequest<AdministratorDetailsModel>, IMapTo<Administrator>
    {
        public CreateAdministratorWithExternalIdentityCommand()
        {

        }

        public CreateAdministratorWithExternalIdentityCommand(string provider, string providerId, string name)
        {
            Provider = provider;
            ProviderId = providerId;
            Name = name;
        }

        public string Provider
        {
            get; set;
        }

        public string ProviderId
        {
            get; set;
        }

        public string Name
        {
            get; set;
        }

        public void Mapping(Profile profile) => profile.CreateMap<CreateAdministratorWithExternalIdentityCommand, Administrator>()
            .ForMember(d => d.Name, o => o.MapFrom(s => s.Name))
            .ForMember(d => d.ExternalIdentities, o => o.MapFrom(s => new List<ExternalIdentity>() {
                new ExternalIdentity { Id = s.ProviderId, Provider = s.Provider }
            }));
    }
}
