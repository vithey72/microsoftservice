﻿namespace BNine.Application.Aggregates.Administrators.Commands.CreateAdministratorWithExternalIdentity
{
    using System.Threading;
    using System.Threading.Tasks;
    using AutoMapper;
    using BNine.Application.Abstractions;
    using BNine.Application.Aggregates.Administrators.Models;
    using BNine.Application.Interfaces;
    using BNine.Domain.Entities.Administrator;
    using MediatR;

    public class CreateAdministratorWithExternalIdentityCommandHandler
        : AbstractRequestHandler, IRequestHandler<CreateAdministratorWithExternalIdentityCommand, AdministratorDetailsModel>
    {
        public CreateAdministratorWithExternalIdentityCommandHandler(IMediator mediator, IBNineDbContext dbContext, IMapper mapper, ICurrentUserService currentUser) : base(mediator, dbContext, mapper, currentUser)
        {
        }

        public async Task<AdministratorDetailsModel> Handle(CreateAdministratorWithExternalIdentityCommand request, CancellationToken cancellationToken)
        {
            var admin = Mapper.Map<Administrator>(request);

            await DbContext.Administrators.AddAsync(admin, cancellationToken);

            await DbContext.SaveChangesAsync(cancellationToken);

            return Mapper.Map<AdministratorDetailsModel>(admin);
        }
    }
}
