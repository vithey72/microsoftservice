﻿namespace BNine.Application.Aggregates.Administrators.Commands.CreateAdministratorWithExternalIdentity
{
    using FluentValidation;

    public class CreateAdministratorWithExternalIdentityCommandValidator
        : AbstractValidator<CreateAdministratorWithExternalIdentityCommand>
    {
        public CreateAdministratorWithExternalIdentityCommandValidator()
        {
            RuleFor(x => x.Name)
                .NotEmpty();

            RuleFor(x => x.Provider)
                .NotEmpty();

            RuleFor(x => x.ProviderId)
                .NotEmpty();
        }
    }
}
