﻿namespace BNine.Application.Aggregates.PaycheckSwitch;

public interface IPaycheckSwitchDiscovery
{
    Task<IUserPaycheckSwitch> DiscoverUserPaycheckSwitch(Guid userId, CancellationToken cancellationToken);
}
