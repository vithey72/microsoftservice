﻿namespace BNine.Application.Aggregates.PaycheckSwitch;

#nullable enable

public interface ITruvFactory
{
    ITruv Create(Guid userId);

    Task<ITruv?> CreateForTruvUserId(Guid truvUserId, CancellationToken cancellationToken);
}
