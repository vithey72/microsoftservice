﻿namespace BNine.Application.Aggregates.PaycheckSwitch;

using BNine.Application.Interfaces.Truv;

internal interface ITruvPendingRefreshTaskRepository
{
    Task SetPendingRefreshTask(Guid userId, TruvTaskId taskId, DateTime utcStartedAt, CancellationToken cancellationToken);

    Task<bool> HasPendingTask(Guid userId, CancellationToken cancellationToken);

    Task<bool> ScheduleRefreshTaskIfHasPending(Guid userId, CancellationToken cancellationToken);

    Task UpdatePendingTask(
        Guid userId,
        TruvTaskId taskId,
        DateTime utcUpdatedAt,
        CancellationToken cancellationToken);

    Task FinishPendingTaskAndRemoveSchedule(
        Guid userId,
        DateTime utcFinishedAt,
        CancellationToken cancellationToken);
}
