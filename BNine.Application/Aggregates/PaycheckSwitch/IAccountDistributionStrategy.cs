﻿namespace BNine.Application.Aggregates.PaycheckSwitch;

using BNine.Domain.Entities.User.UserDetails.Settings;

public interface IAccountDistributionStrategy
{
    Task<PaycheckSwitchProvider> GetPaycheckSwitchProviderForNewUser(CancellationToken cancellationToken);
}
