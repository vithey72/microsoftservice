﻿namespace BNine.Application.Aggregates.PaycheckSwitch;

using System.Threading.Tasks;
using BNine.Application.Interfaces;
using BNine.Domain.Entities.User.UserDetails.Settings;

internal class AccountDistributionStrategy : IAccountDistributionStrategy
{
    private readonly IClientAccountSettingsProvider _clientAccountSettingsProvider;

    public AccountDistributionStrategy(
        IClientAccountSettingsProvider clientAccountSettingsProvider
        )
    {
        _clientAccountSettingsProvider = clientAccountSettingsProvider;
    }

    public async Task<PaycheckSwitchProvider> GetPaycheckSwitchProviderForNewUser(CancellationToken cancellationToken)
    {
        var settings = await _clientAccountSettingsProvider.GetClientAccountSettings(cancellationToken);

        var random = new Random(DateTime.Now.Millisecond);
        var randomValue = random.Next(0, 100);
        if (randomValue < settings.TruvDistributionShareInPercents)
        {
            return PaycheckSwitchProvider.Truv;
        }

        return PaycheckSwitchProvider.Argyle;
    }
}
