﻿namespace BNine.Application.Aggregates.PaycheckSwitch;

using Microsoft.Extensions.DependencyInjection;

public static class ConfigurationExtensions
{
    public static IServiceCollection AddPaycheckSwitch(this IServiceCollection services)
    {
        services.AddTransient<IAccountDistributionStrategy, AccountDistributionStrategy>();
        services.AddTransient<IPaycheckSwitchDiscovery, PaycheckSwitchDiscovery>();
        services.AddTransient<IArgyleFactory, ArgyleFactory>();
        services.AddTransient<ITruvFactory, TruvFactory>();
        services.AddTransient<ITruvPendingRefreshTaskRepository, TruvPendingRefreshTaskRepository>();

        return services;
    }
}
