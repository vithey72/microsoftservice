﻿namespace BNine.Application.Aggregates.PaycheckSwitch;

public interface IArgyleFactory
{
    IArgyle Create(Guid userId);
}
