﻿namespace BNine.Application.Aggregates.PaycheckSwitch;

using System.Threading;
using System.Threading.Tasks;
using BNine.Application.Aggregates.EarlySalary.WidgetConfiguration.Models;
using BNine.Application.Aggregates.IncomeAndTaxesScreen.Models;
using BNine.Application.Exceptions;
using BNine.Application.Interfaces;
using BNine.Application.Interfaces.Bank.Administration;
using BNine.Application.Interfaces.Truv;
using BNine.Application.Models.ServiceBus;
using BNine.Constants;
using BNine.Domain.Entities.User;
using BNine.Domain.Entities.User.EarlySalary;
using BNine.Domain.Entities.User.UserDetails.Settings;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;

#nullable enable

internal class Truv : ITruv
{
    private readonly Guid _userId;

    private readonly IDateTimeUtcProviderService _dateProvider;
    private readonly IBNineDbContext _dbContext;
    private readonly ITruvBridgeTokenProvider _truvBridgeTokenProvider;
    private readonly ITruvClientAccountService _truvClientAccountService;
    private readonly ITruvLinkProvider _truvLinkProvider;
    private readonly ITruvDataRefreshTaskService _truvDataRefreshTaskService;
    private readonly ITruvEmploymentProvider _truvEmploymentProvider;
    private readonly ITruvPendingRefreshTaskRepository _truvPendingRefreshTaskRepository;
    private readonly IBankSavingAccountsService _bankAccountService;

    private readonly ILogger<Truv> _logger;

    public Truv(
        Guid userId,
        IDateTimeUtcProviderService dateProvider,
        IBNineDbContext dbContext,
        ITruvBridgeTokenProvider truvBridgeTokenProvider,
        ITruvClientAccountService truvClientAccountService,
        ITruvLinkProvider truvLinkProvider,
        ITruvDataRefreshTaskService truvDataRefreshTaskService,
        IBankSavingAccountsService bankAccountService,
        ITruvEmploymentProvider truvEmploymentProvider,
        ITruvPendingRefreshTaskRepository truvPendingRefreshTaskRepository,
        ILogger<Truv> logger
        )
    {
        _userId = userId;
        _dateProvider = dateProvider;
        _dbContext = dbContext;
        _truvBridgeTokenProvider = truvBridgeTokenProvider;
        _truvClientAccountService = truvClientAccountService;
        _truvLinkProvider = truvLinkProvider;
        _truvDataRefreshTaskService = truvDataRefreshTaskService;
        _truvEmploymentProvider = truvEmploymentProvider;
        _truvPendingRefreshTaskRepository = truvPendingRefreshTaskRepository;
        _bankAccountService = bankAccountService;
        _logger = logger;

        AssertUserIsConfiguredForTruv();
    }

    public async Task<PaycheckSwitchConfigurationBase> GetConfiguration(CancellationToken cancellationToken)
    {
        var configuration = await GetTruvConfiguration(cancellationToken);

        return configuration;
    }

    public async Task<TruvUserConfiguration> GetTruvConfiguration(CancellationToken cancellationToken)
    {
        var token = await GetToken(cancellationToken);

        return new TruvUserConfiguration
        {
            Token = token,
        };
    }

    public async Task<IntegrationEvent> CreateIntegrationEvent(string eventName, IDictionary<string, object> eventItems, CancellationToken cancellationToken)
    {
        var user = await LoadUserForTruvInternals(cancellationToken);

        if (!string.IsNullOrEmpty(user.TruvAccount.LinkId))
        {
            eventItems.Add("linkId", user.TruvAccount.LinkId);
        }

        var userIdString = user.TruvAccount.TruvUserId.GetTruvUserIdString();
        eventItems.Add("truvUserId", userIdString);

        return new IntegrationEvent
        {
            Body = JsonConvert.SerializeObject(eventItems),
            EventName = $"{ServiceBusEvents.TruvUIEvent}.{eventName}",
            TopicName = ServiceBusTopics.TruvUI
        };
    }

    public async Task DeleteAccount(CancellationToken cancellationToken)
    {
        var user = await LoadUser(cancellationToken);

        if (user.TruvAccount is null)
        {
            return;
        }
        if (!user.TruvAccount.DeletedAt.HasValue)
        {
            return;
        }

        await _truvClientAccountService.DeleteAccount(user.TruvAccount.TruvUserId, cancellationToken);

        user.TruvAccount.UpdatedAt = _dateProvider.UtcNow();
        user.TruvAccount.DeletedAt = _dateProvider.UtcNow();

        await _dbContext.SaveChangesAsync(cancellationToken);
    }

    public async Task UpdatePublicToken(string publicToken, CancellationToken cancellationToken)
    {
        var linkToken = await _truvLinkProvider.ProvideLinkToken(publicToken, cancellationToken);

        var user = await LoadUserForTruvInternals(cancellationToken);

        user.TruvAccount.AccessToken = linkToken.AccessToken;
        user.TruvAccount.LinkId = linkToken.LinkId;
        user.TruvAccount.UpdatedAt = _dateProvider.UtcNow();

        await _dbContext.SaveChangesAsync(cancellationToken);

        await TryBeginDataRefresh(user.TruvAccount, cancellationToken);
    }

    public async Task<IncomeAndTaxesScreenViewModel.Employer[]?> GetEmployers(CancellationToken cancellationToken)
    {
        var user = await TryLoadUserForTruvInternals(cancellationToken);

        if (user is null)
        {
            return null;
        }

        if (string.IsNullOrEmpty(user.TruvAccount?.LinkId))
        {
            return null;
        }

        var isDataRefreshPending = await _truvPendingRefreshTaskRepository.HasPendingTask(user.Id, cancellationToken);
        if (isDataRefreshPending)
        {
            return null;
        }

        var employments = await _truvEmploymentProvider.ProvideEmployments(user.TruvAccount.LinkId, cancellationToken);
        var result = employments.Select(ConvertEmploymentToModel).ToArray();

        return result.ToArray();
    }

    public async Task UpdatePendingTaskStatusIfApplicable(TruvTaskStatusUpdate taskStatus, CancellationToken cancellationToken)
    {
        var user = await LoadUserForTruvInternals(cancellationToken);

        if (user.TruvAccount.PendingTaskStatusUpdatedAt > taskStatus.UpdatedAt)
        {
            return;
        }

        switch (taskStatus.Status)
        {
            case TruvTaskStatus.Pending:
                await UpdateTaskIsPending(user.TruvAccount, taskStatus.TaskId, taskStatus.UpdatedAt, cancellationToken);
                break;
            case TruvTaskStatus.Failed:
                await UpdateTaskFailedAndRemoveSchedule(user.TruvAccount, taskStatus.UpdatedAt, cancellationToken);
                break;
            case TruvTaskStatus.Succeeded when user.TruvAccount.IsRefreshTaskScheduled:
                await UpdateTaskSucceededAndStartScheduled(user.TruvAccount, taskStatus.UpdatedAt, cancellationToken);
                break;
            case TruvTaskStatus.Succeeded:
                await UpdateTaskSucceeded(user.TruvAccount, taskStatus.UpdatedAt, cancellationToken);
                break;
            default:
                throw new InvalidOperationException();
        }
    }

    private Task UpdateTaskIsPending(TruvAccount truvAccount, TruvTaskId taskId, DateTime utcUpdatedAt,
        CancellationToken cancellationToken)
    {
        return _truvPendingRefreshTaskRepository.UpdatePendingTask(
            truvAccount.UserId,
            taskId,
            utcUpdatedAt,
            cancellationToken);
    }

    private Task UpdateTaskFailedAndRemoveSchedule(TruvAccount truvAccount, DateTime utcUpdatedAt,
        CancellationToken cancellationToken)
    {
        return _truvPendingRefreshTaskRepository.FinishPendingTaskAndRemoveSchedule(
            truvAccount.UserId,
            utcUpdatedAt,
            cancellationToken);
    }

    private async Task UpdateTaskSucceededAndStartScheduled(TruvAccount truvAccount, DateTime utcUpdatedAt,
        CancellationToken cancellationToken)
    {
        await _truvPendingRefreshTaskRepository.FinishPendingTaskAndRemoveSchedule(
            truvAccount.UserId,
            utcUpdatedAt,
            cancellationToken);

        await TryBeginDataRefresh(truvAccount, cancellationToken);
    }

    private Task UpdateTaskSucceeded(TruvAccount truvAccount, DateTime utcUpdatedAt,
        CancellationToken cancellationToken)
    {
        return _truvPendingRefreshTaskRepository.FinishPendingTaskAndRemoveSchedule(
            truvAccount.UserId,
            utcUpdatedAt,
            cancellationToken);
    }


    private IncomeAndTaxesScreenViewModel.Employer ConvertEmploymentToModel(TruvEmployment employment)
    {
        return new IncomeAndTaxesScreenViewModel.Employer
        {
            Title = employment.EmployerTitle,
            IconUrl = employment.EmployerIconUrl,
        };
    }

    private async Task TryBeginDataRefresh(TruvAccount account, CancellationToken cancellationToken)
    {
        try
        {
            var taskScheduled = await _truvPendingRefreshTaskRepository.ScheduleRefreshTaskIfHasPending(account.UserId, cancellationToken);
            if (taskScheduled)
            {
                return;
            }

            var taskStartedAt = _dateProvider.UtcNow();

            var taskId = await _truvDataRefreshTaskService.BeginDataRefresh(account.AccessToken, cancellationToken);

            await _truvPendingRefreshTaskRepository.SetPendingRefreshTask(account.UserId, taskId, taskStartedAt, cancellationToken);
        }
        catch (Exception ex)
        {
            _logger.LogWarning(ex, $"Failed to start or schedule truv data refresh task, user id {account.UserId}, link id {account.LinkId}");
        }
    }

    private async Task<string> GetToken(CancellationToken cancellationToken)
    {
        var utcNow = _dateProvider.UtcNow();

        var user = await LoadUser(cancellationToken);

        if (user.TruvAccount == null)
        {
            user.TruvAccount = new TruvAccount
            {
                TruvUserId = await CreateAccount(user.Id, cancellationToken),
                CreatedAt = utcNow,
                UpdatedAt = utcNow,
                BridgeToken = "WIP",
            };
            await _dbContext.SaveChangesAsync(cancellationToken);
        }

        if (user.TruvAccount.BridgeTokenExpiresAt <= utcNow)
        {
            await UpdateToken(user, cancellationToken);
        }

        await _dbContext.SaveChangesAsync(cancellationToken);

        return user.TruvAccount.BridgeToken;
    }

    private async Task<Guid> CreateAccount(Guid userId, CancellationToken cancellationToken)
    {
        var account = await _truvClientAccountService.CreateAccount(userId, cancellationToken);
        return account.Id;
    }

    private async Task UpdateToken(User user, CancellationToken cancellationToken)
    {
        var bankAccountInfo = await _bankAccountService.GetSavingAccountInfo(user.CurrentAccount.ExternalId);
        var token = await _truvBridgeTokenProvider.GetToken(user.TruvAccount.TruvUserId, bankAccountInfo.AccountNumber, cancellationToken);
        user.TruvAccount.BridgeToken = token.Value;
        user.TruvAccount.BridgeTokenExpiresAt = token.ValidTo;
    }

    private async Task<User> LoadUserForTruvInternals(CancellationToken cancellationToken)
    {
        var user = await LoadUser(cancellationToken);

        if (user.TruvAccount is null)
        {
            throw new NotFoundException(nameof(user.TruvAccount), _userId);
        }

        return user;
    }

    private async Task<User?> TryLoadUserForTruvInternals(CancellationToken cancellationToken)
    {
        var user = await LoadUser(cancellationToken);

        if (user.TruvAccount is null)
        {
            return null;
        }

        return user;
    }

    private async Task<User> LoadUser(CancellationToken cancellationToken)
    {
        var user = await _dbContext.Users
            .Include(x => x.CurrentAccount)
            .Include(x => x.TruvAccount)
            .Where(x => x.Id == _userId)
            .SingleAsync(cancellationToken);

        return user;
    }

    private void AssertUserIsConfiguredForTruv()
    {
        var user = _dbContext.Users.Single(x => x.Id == _userId);
        if (user.Settings.PaycheckSwitchProvider != PaycheckSwitchProvider.Truv)
        {
            throw new ArgumentOutOfRangeException("Expected paycheck switch provider " + PaycheckSwitchProvider.Truv);
        }
    }
}
