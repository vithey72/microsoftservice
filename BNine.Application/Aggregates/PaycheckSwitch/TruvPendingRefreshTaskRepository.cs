﻿namespace BNine.Application.Aggregates.PaycheckSwitch;

using System;
using System.Threading;
using System.Threading.Tasks;
using BNine.Application.Interfaces;
using BNine.Application.Interfaces.Truv;
using Microsoft.EntityFrameworkCore;

internal class TruvPendingRefreshTaskRepository : ITruvPendingRefreshTaskRepository
{
    private readonly IBNineDbContext _dbContext;
    private readonly ITruvDataRefreshTaskService _truvDataRefreshTaskService;

    public TruvPendingRefreshTaskRepository(
        IBNineDbContext dbContext,
        ITruvDataRefreshTaskService truvDataRefreshTaskService)
    {
        _dbContext = dbContext;
        _truvDataRefreshTaskService = truvDataRefreshTaskService;
    }

    public async Task<bool> HasPendingTask(Guid userId, CancellationToken cancellationToken)
    {
        var user = await _dbContext.Users.SingleAsync(x => x.Id == userId, cancellationToken);
        if (user.TruvAccount.PendingTaskId is null)
        {
            return false;
        }

        var taskId = new TruvTaskId(user.TruvAccount.PendingTaskId);
        var status = await _truvDataRefreshTaskService.GetTaskStatus(taskId, cancellationToken);

        if (status == TruvTaskStatus.Succeeded)
        {
            user.TruvAccount.PendingTaskId = null;
            await _dbContext.SaveChangesAsync(cancellationToken);
            return false;
        }

        return true;
    }

    public async Task<bool> ScheduleRefreshTaskIfHasPending(Guid userId, CancellationToken cancellationToken)
    {
        var user = await _dbContext.Users.SingleAsync(x => x.Id == userId, cancellationToken);
        if (user.TruvAccount.PendingTaskId is null)
        {
            return false;
        }

        user.TruvAccount.IsRefreshTaskScheduled = true;
        await _dbContext.SaveChangesAsync(cancellationToken);

        return true;
    }

    public async Task SetPendingRefreshTask(
        Guid userId,
        TruvTaskId taskId,
        DateTime utcStartedAt,
        CancellationToken cancellationToken)
    {
        var user = await _dbContext.Users.SingleAsync(x => x.Id == userId, cancellationToken);
        user.TruvAccount.PendingTaskId = taskId.Value;
        user.TruvAccount.PendingTaskStatusUpdatedAt = utcStartedAt;
        user.TruvAccount.IsRefreshTaskScheduled = false;
        await _dbContext.SaveChangesAsync(cancellationToken);
    }

    public async Task UpdatePendingTask(
        Guid userId,
        TruvTaskId taskId,
        DateTime utcUpdatedAt,
        CancellationToken cancellationToken)
    {
        var user = await _dbContext.Users.SingleAsync(x => x.Id == userId, cancellationToken);
        user.TruvAccount.PendingTaskId = taskId.Value;
        user.TruvAccount.PendingTaskStatusUpdatedAt = utcUpdatedAt;
        await _dbContext.SaveChangesAsync(cancellationToken);
    }

    public async Task FinishPendingTaskAndRemoveSchedule(
        Guid userId,
        DateTime utcFinishedAt,
        CancellationToken cancellationToken)
    {
        var user = await _dbContext.Users.SingleAsync(x => x.Id == userId, cancellationToken);
        user.TruvAccount.IsRefreshTaskScheduled = false;
        user.TruvAccount.PendingTaskId = null;
        user.TruvAccount.PendingTaskStatusUpdatedAt = utcFinishedAt;
        await _dbContext.SaveChangesAsync(cancellationToken);
    }
}

