﻿namespace BNine.Application.Aggregates.PaycheckSwitch;

using BNine.Application.Aggregates.EarlySalary.WidgetConfiguration.Models;
using BNine.Application.Aggregates.IncomeAndTaxesScreen.Models;
using BNine.Application.Models.ServiceBus;

public interface IUserPaycheckSwitch
{
    Task<PaycheckSwitchConfigurationBase> GetConfiguration(CancellationToken cancellationToken);

    Task DeleteAccount(CancellationToken cancellationToken);

    Task<IntegrationEvent> CreateIntegrationEvent(string eventName, IDictionary<string, object> eventItems, CancellationToken cancellationToken);

    Task<IncomeAndTaxesScreenViewModel.Employer[]> GetEmployers(CancellationToken cancellationToken);
}
