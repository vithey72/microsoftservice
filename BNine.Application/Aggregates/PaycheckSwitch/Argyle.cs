﻿namespace BNine.Application.Aggregates.PaycheckSwitch;

using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Threading;
using System.Threading.Tasks;
using BNine.Application.Aggregates.EarlySalary.WidgetConfiguration.Models;
using BNine.Application.Aggregates.IncomeAndTaxesScreen.Models;
using BNine.Application.Exceptions;
using BNine.Application.Interfaces;
using BNine.Application.Interfaces.Argyle;
using BNine.Application.Interfaces.Bank.Administration;
using BNine.Application.Models.ServiceBus;
using BNine.Constants;
using BNine.Domain.Entities.User.EarlySalary;
using BNine.Domain.Entities.User.UserDetails.Settings;
using BNine.Enums.Transfers.Controller;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;

internal class Argyle : IArgyle
{
    private readonly Guid _userId;

    private readonly IBNineDbContext _dbContext;
    private readonly IDateTimeUtcProviderService _dateTimeProviderService;
    private readonly IBankSavingAccountsService _bankAccountService;
    private readonly IArgyleService _argyleService;
    private readonly IArgyleUsersService _argyleUsersService;
    private readonly IArgyleEmploymentsService _argyleEmploymentsService;
    private readonly IArgyleLinkItemsService _argyleLinkItemsService;
    private readonly ITransferIconsAndNamesProviderService _iconsService;

    public Argyle(
        Guid userId,
        IBNineDbContext dbContext,
        IDateTimeUtcProviderService dateTimeProviderService,
        IBankSavingAccountsService bankAccountService,
        IArgyleService argyleService,
        IArgyleUsersService argyleUsersService,
        IArgyleEmploymentsService argyleEmploymentsService,
        IArgyleLinkItemsService argyleLinkItemsService,
        ITransferIconsAndNamesProviderService iconsService
        )
    {
        _userId = userId;
        _dbContext = dbContext;
        _dateTimeProviderService = dateTimeProviderService;
        _bankAccountService = bankAccountService;
        _argyleService = argyleService;
        _argyleUsersService = argyleUsersService;
        _argyleEmploymentsService = argyleEmploymentsService;
        _argyleLinkItemsService = argyleLinkItemsService;
        _iconsService = iconsService;

        AssertUserIsConfiguredForArgyle();
    }

    private void AssertUserIsConfiguredForArgyle()
    {
        var user = _dbContext.Users.Single(x => x.Id == _userId);
        if (user.Settings.PaycheckSwitchProvider != PaycheckSwitchProvider.Argyle)
        {
            throw new ArgumentOutOfRangeException("Expected paycheck switch provider " + PaycheckSwitchProvider.Argyle);
        }
    }

    public async Task<PaycheckSwitchConfigurationBase> GetConfiguration(CancellationToken cancellationToken)
    {
        var configuration = await GetArgyleConfiguration(cancellationToken);

        return configuration;
    }

    public async Task<ArgyleUserConfiguration> GetArgyleConfiguration(CancellationToken cancellationToken)
    {
        var argyleDistributionConfig = await GetArgyleDistributionConfig(cancellationToken);
        var token = await GetToken(cancellationToken);

        return new ArgyleUserConfiguration
        {
            Token = token,
            DistributionConfig = argyleDistributionConfig,
        };
    }

    public async Task DeleteAccount(CancellationToken cancellationToken)
    {
        var user = await _dbContext.Users.FirstOrDefaultAsync(x => x.Id == _userId, cancellationToken);
        if (user?.EarlySalaryWidgetConfiguration is null)
        {
            return;
        }
        if (!user.EarlySalaryWidgetConfiguration.DeletedAt.HasValue)
        {
            return;
        }

        var argyleUser = await _argyleUsersService.GetUser(user.EarlySalaryWidgetConfiguration.WidgetUserId);
        if (argyleUser is null)
        {
            return;
        }

        await _argyleUsersService.DeleteUser(user.EarlySalaryWidgetConfiguration.WidgetUserId);

        user.EarlySalaryWidgetConfiguration.DeletedAt = _dateTimeProviderService.UtcNow();

        await _dbContext.SaveChangesAsync(cancellationToken);
    }

    public Task<IntegrationEvent> CreateIntegrationEvent(string eventName, IDictionary<string, object> eventItems, CancellationToken cancellationToken)
    {
        var @event = new IntegrationEvent
        {
            Body = JsonConvert.SerializeObject(eventItems),
            EventName = $"{ServiceBusEvents.ArgyleUIEvent}.{eventName}",
            TopicName = ServiceBusTopics.ArgyleUI
        };

        return Task.FromResult(@event);
    }

    public async Task<IncomeAndTaxesScreenViewModel.Employer[]> GetEmployers(CancellationToken cancellationToken)
    {
        var argyleWidgetEntity = await _dbContext
            .EarlySalaryWidgetConfigurations
            .FirstOrDefaultAsync(x =>
                    x.UserId == _userId &&
                    x.DeletedAt == null,
                cancellationToken);

        if (argyleWidgetEntity == null)
        {
            return null;
        }

        var employmentsLinkIds = (await _argyleEmploymentsService
                .GetEmploymentsLinkIds(argyleWidgetEntity.WidgetUserId, cancellationToken))
            .ToArray();
        var linkItems = await _argyleLinkItemsService.GetLinkItems(employmentsLinkIds, cancellationToken);

        var result = new List<IncomeAndTaxesScreenViewModel.Employer>();
        foreach (var employmentId in employmentsLinkIds)
        {
            var linkItem = linkItems.Items.FirstOrDefault(x => x.Id == employmentId);
            result.Add(new IncomeAndTaxesScreenViewModel.Employer
            {
                Title = linkItem?.Name ?? employmentId,
                IconUrl = linkItem?.LogoUrl ?? _iconsService.FindIconUrl(BankTransferIconOption.MoneyBag),
            });
        }

        return result.ToArray();
    }

    private async Task<string> GetArgyleDistributionConfig(CancellationToken cancellationToken)
    {
        var user = await _dbContext.Users
            .Include(x => x.CurrentAccount)
            .AsNoTracking()
            .FirstOrDefaultAsync(x => x.Id == _userId, cancellationToken);

        if (user?.CurrentAccount == null)
        {
            throw new NotFoundException(nameof(user.CurrentAccount), _userId);
        }

        var argyleSettings = _dbContext.ArgyleSettings
            .SingleOrDefault();

        if (argyleSettings == null)
        {
            throw new NotFoundException("Argyle settings not found");
        }

        var info = await _bankAccountService.GetSavingAccountInfo(user.CurrentAccount.ExternalId);
        var config = await _argyleService.GetEncryptedConfig(
            info.AccountNumber,
            argyleSettings.DefaultAmountAllocation,
            argyleSettings.MinAmountAllocation,
            argyleSettings.MaxAmountAllocation,
            argyleSettings.DefaultPercentAllocation,
            argyleSettings.MinPercentAllocation,
            argyleSettings.MaxPercentAllocation);

        return config;
    }

    private async Task<string> GetToken(CancellationToken cancellationToken)
    {
        var widgetConfiguration = await _dbContext.EarlySalaryWidgetConfigurations
            .Where(x => !x.User.EarlySalaryWidgetConfiguration.DeletedAt.HasValue)
            .Where(x => x.UserId == _userId)
            .FirstOrDefaultAsync(cancellationToken);

        if (widgetConfiguration == null)
        {
            var (token, argyleUserId) = await _argyleService.CreateAccount(_userId);
            var configurationEntity = new EarlySalaryWidgetConfiguration
            {
                Token = token,
                WidgetUserId = argyleUserId,
                UserId = _userId,
                CreatedAt = DateTime.UtcNow,
                UpdatedAt = DateTime.UtcNow,
            };
            _dbContext.EarlySalaryWidgetConfigurations.Add(configurationEntity);
            await _dbContext.SaveChangesAsync(cancellationToken);
            widgetConfiguration = configurationEntity;
        }

        var handler = new JwtSecurityTokenHandler();
        var jwtToken = handler.ReadJwtToken(widgetConfiguration.Token);
        if (jwtToken.ValidTo < DateTime.UtcNow)
        {
            var updatedToken = await _argyleService.UpdateToken(widgetConfiguration.WidgetUserId);
            widgetConfiguration.Token = updatedToken;
            await _dbContext.SaveChangesAsync(cancellationToken);
        }

        return widgetConfiguration.Token;
    }
}
