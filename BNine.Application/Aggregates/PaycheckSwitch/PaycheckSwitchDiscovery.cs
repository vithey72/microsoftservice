﻿namespace BNine.Application.Aggregates.PaycheckSwitch;

using System;
using System.Threading;
using System.Threading.Tasks;
using BNine.Application.Interfaces;
using BNine.Domain.Entities.User.UserDetails.Settings;
using Microsoft.EntityFrameworkCore;

internal class PaycheckSwitchDiscovery : IPaycheckSwitchDiscovery
{
    private readonly IBNineDbContext _dbContext;
    private readonly IArgyleFactory _argyleFactory;
    private readonly ITruvFactory _truvFactory;

    public PaycheckSwitchDiscovery(
        IBNineDbContext dbContext,
        IArgyleFactory argyleFactory,
        ITruvFactory truvFactory)
    {
        _dbContext = dbContext;
        _argyleFactory = argyleFactory;
        _truvFactory = truvFactory;
    }

    public async Task<IUserPaycheckSwitch> DiscoverUserPaycheckSwitch(Guid userId, CancellationToken cancellationToken)
    {
        var user = await _dbContext.Users.FirstOrDefaultAsync(x => x.Id == userId, cancellationToken);

        var paycheckSwitch = user.Settings.PaycheckSwitchProvider switch
        {
            PaycheckSwitchProvider.Argyle => CreateArgyle(userId),
            PaycheckSwitchProvider.Truv => CreateTruv(userId),
            _ => throw new ArgumentOutOfRangeException(nameof(user.Settings.PaycheckSwitchProvider)),
        };

        return paycheckSwitch;
    }

    private IUserPaycheckSwitch CreateArgyle(Guid userId)
    {
        var argyle = _argyleFactory.Create(userId);

        return argyle;
    }

    private IUserPaycheckSwitch CreateTruv(Guid userId)
    {
        var truv = _truvFactory.Create(userId);

        return truv;
    }
}
