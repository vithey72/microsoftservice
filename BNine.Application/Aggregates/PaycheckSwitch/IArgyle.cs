﻿namespace BNine.Application.Aggregates.PaycheckSwitch;

using BNine.Application.Aggregates.EarlySalary.WidgetConfiguration.Models;

public interface IArgyle : IUserPaycheckSwitch
{
    Task<ArgyleUserConfiguration> GetArgyleConfiguration(CancellationToken cancellationToken);
}
