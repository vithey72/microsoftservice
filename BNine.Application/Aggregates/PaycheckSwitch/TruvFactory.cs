﻿namespace BNine.Application.Aggregates.PaycheckSwitch;

using BNine.Application.Interfaces;
using BNine.Application.Interfaces.Bank.Administration;
using BNine.Application.Interfaces.Truv;
using BNine.Domain.Entities.User;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;

#nullable enable

internal class TruvFactory : ITruvFactory
{
    private readonly IServiceProvider _serviceProvider;
    private readonly IDateTimeUtcProviderService _dateProvider;
    private readonly IBNineDbContext _dbContext;
    private readonly ITruvBridgeTokenProvider _truvBridgeTokenProvider;
    private readonly ITruvClientAccountService _truvClientAccountService;
    private readonly ITruvLinkProvider _truvLinkProvider;
    private readonly ITruvDataRefreshTaskService _truvDataRefreshTaskService;
    private readonly ITruvEmploymentProvider _truvEmploymentProvider;
    private readonly ITruvPendingRefreshTaskRepository _truvPendingRefreshTaskRepository;
    private readonly IBankSavingAccountsService _bankAccountService;

    public TruvFactory(
        IServiceProvider serviceProvider,
        IDateTimeUtcProviderService dateProvider,
        IBNineDbContext dbContext,
        ITruvBridgeTokenProvider truvBridgeTokenProvider,
        ITruvClientAccountService truvClientAccountService,
        ITruvLinkProvider truvLinkProvider,
        ITruvDataRefreshTaskService truvDataRefreshTaskService,
        ITruvEmploymentProvider truvEmploymentProvider,
        ITruvPendingRefreshTaskRepository truvPendingRefreshTaskRepository,
        IBankSavingAccountsService bankAccountService
        )
    {
        _serviceProvider = serviceProvider;
        _dateProvider = dateProvider;
        _dbContext = dbContext;
        _truvBridgeTokenProvider = truvBridgeTokenProvider;
        _truvClientAccountService = truvClientAccountService;
        _truvLinkProvider = truvLinkProvider;
        _truvDataRefreshTaskService = truvDataRefreshTaskService;
        _truvEmploymentProvider = truvEmploymentProvider;
        _truvPendingRefreshTaskRepository = truvPendingRefreshTaskRepository;
        _bankAccountService = bankAccountService;
    }

    public ITruv Create(Guid userId)
    {
        var truvLogger = _serviceProvider.GetRequiredService<ILogger<Truv>>();

        return new Truv(
            userId,
            _dateProvider,
            _dbContext,
            _truvBridgeTokenProvider,
            _truvClientAccountService,
            _truvLinkProvider,
            _truvDataRefreshTaskService,
            _bankAccountService,
            _truvEmploymentProvider,
            _truvPendingRefreshTaskRepository,
            truvLogger
            );
    }

    public async Task<ITruv?> CreateForTruvUserId(Guid truvUserId, CancellationToken cancellationToken)
    {
        var user = await LoadUser(truvUserId, cancellationToken);

        if (user is null)
        {
            return null;
        }

        var truvLogger = _serviceProvider.GetRequiredService<ILogger<Truv>>();

        return new Truv(
            user.Id,
            _dateProvider,
            _dbContext,
            _truvBridgeTokenProvider,
            _truvClientAccountService,
            _truvLinkProvider,
            _truvDataRefreshTaskService,
            _bankAccountService,
            _truvEmploymentProvider,
            _truvPendingRefreshTaskRepository,
            truvLogger
            );
    }

    private async Task<User?> LoadUser(Guid truvUserId, CancellationToken cancellationToken)
    {
        var user = await _dbContext.Users
            .Where(x => x.TruvAccount.TruvUserId == truvUserId)
            .SingleOrDefaultAsync(cancellationToken);

        return user;
    }
}
