﻿namespace BNine.Application.Aggregates.PaycheckSwitch;

using BNine.Application.Aggregates.EarlySalary.WidgetConfiguration.Models;
using BNine.Application.Interfaces.Truv;

public interface ITruv : IUserPaycheckSwitch
{
    Task<TruvUserConfiguration> GetTruvConfiguration(CancellationToken cancellationToken);

    Task UpdatePublicToken(string publicToken, CancellationToken cancellationToken);

    Task UpdatePendingTaskStatusIfApplicable(TruvTaskStatusUpdate taskStatus, CancellationToken cancellationToken);
}
