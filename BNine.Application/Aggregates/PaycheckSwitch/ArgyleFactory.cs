﻿namespace BNine.Application.Aggregates.PaycheckSwitch;

using System;
using BNine.Application.Interfaces;
using BNine.Application.Interfaces.Argyle;
using BNine.Application.Interfaces.Bank.Administration;

internal class ArgyleFactory : IArgyleFactory
{
    private readonly IDateTimeUtcProviderService _dateProvider;
    private readonly IBNineDbContext _dbContext;
    private readonly IBankSavingAccountsService _bankAccountService;
    private readonly IArgyleService _argyleService;
    private readonly IArgyleUsersService _argyleUsersService;
    private readonly IArgyleEmploymentsService _argyleEmploymentsService;
    private readonly IArgyleLinkItemsService _argyleLinkItemsService;
    private readonly ITransferIconsAndNamesProviderService _iconsService;

    public ArgyleFactory(
        IDateTimeUtcProviderService dateProvider,
        IBNineDbContext dbContext,
        IBankSavingAccountsService bankAccountService,
        IArgyleService argyleService,
        IArgyleUsersService argyleUsersService,
        IArgyleEmploymentsService argyleEmploymentsService,
        IArgyleLinkItemsService argyleLinkItemsService,
        ITransferIconsAndNamesProviderService iconsService
        )
    {
        _dateProvider = dateProvider;
        _dbContext = dbContext;
        _bankAccountService = bankAccountService;
        _argyleService = argyleService;
        _argyleUsersService = argyleUsersService;
        _argyleEmploymentsService = argyleEmploymentsService;
        _argyleLinkItemsService = argyleLinkItemsService;
        _iconsService = iconsService;
    }

    public IArgyle Create(Guid userId)
    {
        return new Argyle(
            userId,
            _dbContext,
            _dateProvider,
            _bankAccountService,
            _argyleService,
            _argyleUsersService,
            _argyleEmploymentsService,
            _argyleLinkItemsService,
            _iconsService);
    }
}
