﻿namespace BNine.Application.Aggregates.Emails.Commands.Onboarding.NotCompleted
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading;
    using System.Threading.Tasks;
    using AutoMapper;
    using BNine.Settings;
    using Common;
    using Enums;
    using Interfaces;
    using Interfaces.Firebase;
    using MediatR;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.Extensions.Options;
    using Models.Emails.EmailsWithTemplate;
    using Models.Emails.EmailsWithTemplate.Onboarding;

    public class OnboardingNotCompletedEmailCommandHandler :
        TemplateEmailAbstractCommandHandler,
        IRequestHandler<OnboardingNotCompletedEmailCommand>
    {
        private DynamicShortLinksSettings ShortLinkSettings
        {
            get;
        }

        public OnboardingNotCompletedEmailCommandHandler(
            IDynamicLinkService dynamicLinkService,
            IOptions<DynamicShortLinksSettings> shortLinkOptions,
            IOptions<SendGridSettings> sendGridSettings,
            IEmailService emailService,
            IOptions<EmailTemplatesSettings> templatesOptions,
            IMediator mediator,
            IBNineDbContext dbContext,
            IMapper mapper,
            ICurrentUserService currentUser) : base(dynamicLinkService, emailService, sendGridSettings, templatesOptions, mediator, dbContext, mapper, currentUser)
        {
            ShortLinkSettings = shortLinkOptions.Value;
        }

        public async Task<Unit> Handle(OnboardingNotCompletedEmailCommand request, CancellationToken cancellationToken)
        {
            var currentTime = DateTime.UtcNow.TrimToHour();

            var firstDate = currentTime.AddMinutes(-30);
            var secondDate = currentTime.AddHours(-24);
            var thirdDate = currentTime.AddHours(-51);

            var firstDateTemplates = await GenerateTemplates(firstDate, TemplatesIds.OnboardingNotCompletedFirstTemplate);
            var secondDateTemplates = await GenerateTemplates(secondDate, TemplatesIds.OnboardingNotCompletedSecondTemplate);
            var thirdDateTemplates = await GenerateTemplates(thirdDate, TemplatesIds.OnboardingNotCompletedThirdTemplate);

            await EmailService.SendEmails(
                firstDateTemplates
                    .Concat(secondDateTemplates)
                    .Concat(thirdDateTemplates)
                    .ToList());

            return Unit.Value;
        }

        private async Task<List<EmailMessageTemplate>> GenerateTemplates(DateTime rangeStart, string templateId)
        {
            var rangeEnd = rangeStart.AddMinutes(30);
            var usersData = await DbContext.Users
                .Where(x => x.Status != UserStatus.Active && x.Status != UserStatus.Declined)
                .Where(x => x.CreatedAt >= rangeStart && x.CreatedAt < rangeEnd)
                .Select(x => new { x.Email, x.FirstName })
                .ToListAsync();

            return usersData.Select(ud => GetTemplate(ud.FirstName, ud.Email, templateId)).ToList();
        }

        private EmailMessageTemplate GetTemplate(string firstName, string email, string templateId)
        {
            var templateData = new OnboardingNotCompletedTemplateData
            {
                Name = firstName,
                Link = ShortLinkSettings.ContinueOnboardingLink
            };

            return GetTemplate(templateId, email, templateData, SendGridSettings.MarketingGroupId);
        }
    }
}
