﻿namespace BNine.Application.Aggregates.Emails.Commands
{
    using System.Collections.Generic;
    using Abstractions;
    using Application.Models.Emails.EmailsWithTemplate;
    using AutoMapper;
    using BNine.Settings;
    using Interfaces;
    using Interfaces.Firebase;
    using MediatR;
    using Microsoft.Extensions.Options;

    public abstract class TemplateEmailAbstractCommandHandler : AbstractRequestHandler
    {
        protected IDynamicLinkService DynamicLinkService
        {
            get;
        }

        protected IEmailService EmailService
        {
            get;
        }

        protected EmailTemplatesSettings TemplatesIds
        {
            get;
        }

        protected SendGridSettings SendGridSettings
        {
            get;
        }

        protected TemplateEmailAbstractCommandHandler(
            IDynamicLinkService dynamicLinkService,
            IEmailService emailService,
            IOptions<SendGridSettings> sendGridSettings,
            IOptions<EmailTemplatesSettings> templatesOptions,
            IMediator mediator,
            IBNineDbContext dbContext,
            IMapper mapper,
            ICurrentUserService currentUser) : base(mediator, dbContext, mapper, currentUser)
        {
            DynamicLinkService = dynamicLinkService;
            EmailService = emailService;
            TemplatesIds = templatesOptions.Value;
            SendGridSettings = sendGridSettings.Value;
        }

        protected EmailMessageTemplate GetTemplate(string templateId, string recipientEmail, BaseTemplateData data, int asmGroupId, IEnumerable<EmailAttachmentData> attachments = null)
        {
            return new EmailMessageTemplate
            {
                RecipientEmail = recipientEmail,
                TemplateId = templateId,
                TemplateData = data,
                Attachments = attachments ?? new List<EmailAttachmentData>(),
                AsmGroupId = asmGroupId
            };
        }
    }
}
