﻿namespace BNine.Application.Aggregates.Emails.Commands
{
    using BNine.Application.Aggregates.CheckingAccounts.Queries.GetStatements;
    using FluentValidation;
    using Microsoft.Extensions.Configuration;

    public class SendNotificationsCommandValidator : AbstractValidator<GetStatementsQuery>
    {
        public SendNotificationsCommandValidator(IConfiguration configuration)
        {
            RuleFor(x => x.PrivateKey)
                .Equal(configuration["EmailCustomerStatementsApiKey"]) //TODO: something is not right with this variable name
                .WithMessage("Invalid function API private key");
        }
    }
}
