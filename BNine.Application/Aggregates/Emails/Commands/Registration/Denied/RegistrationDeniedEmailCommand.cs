﻿namespace BNine.Application.Aggregates.Emails.Commands.Registration.Denied
{
    using System;
    using MediatR;
    using Newtonsoft.Json;

    public class RegistrationDeniedEmailCommand : IRequest
    {
        [JsonProperty("userId")]
        public Guid UserId
        {
            get; set;
        }
    }
}
