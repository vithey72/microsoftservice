﻿namespace BNine.Application.Aggregates.Emails.Commands.Registration.Denied
{
    using System.Linq;
    using System.Threading;
    using System.Threading.Tasks;
    using AutoMapper;
    using BNine.Settings;
    using Exceptions;
    using Interfaces;
    using Interfaces.Firebase;
    using MediatR;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.Extensions.Options;
    using Models.Dto;
    using Models.Emails.EmailsWithTemplate.Registration;

    public class RegistrationDeniedEmailCommandHandler :
        TemplateEmailAbstractCommandHandler,
        IRequestHandler<RegistrationDeniedEmailCommand>
    {
        public RegistrationDeniedEmailCommandHandler(
            IDynamicLinkService dynamicLinkService,
            IEmailService emailService,
            IOptions<SendGridSettings> sendGridSettings,
            IOptions<EmailTemplatesSettings> templatesOptions,
            IMediator mediator,
            IBNineDbContext dbContext,
            IMapper mapper,
            ICurrentUserService currentUser) : base(dynamicLinkService, emailService, sendGridSettings, templatesOptions, mediator, dbContext, mapper, currentUser)
        {
        }

        public async Task<Unit> Handle(RegistrationDeniedEmailCommand request, CancellationToken cancellationToken)
        {
            var user = await DbContext.Users
                .Where(x => x.Id == request.UserId)
                .Select(x => new { x.Email, x.FirstName })
                .FirstOrDefaultAsync(cancellationToken);

            if (user == null)
            {
                throw new NotFoundException(nameof(User), request.UserId);
            }

            var templateData = new RegistrationDeniedTemplateData { Name = user.FirstName };
            var template = GetTemplate(TemplatesIds.RegistrationDeniedTemplate, user.Email, templateData, SendGridSettings.OperationalGroupId);

            await EmailService.SendEmail(template);

            return Unit.Value;
        }
    }
}
