﻿namespace BNine.Application.Aggregates.Emails.Commands.Registration.Finished
{
    using System.Linq;
    using System.Threading;
    using System.Threading.Tasks;
    using AutoMapper;
    using BNine.Settings;
    using Exceptions;
    using Interfaces;
    using Interfaces.Firebase;
    using MediatR;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.Extensions.Options;
    using Models.Dto;
    using Models.Emails.EmailsWithTemplate.Registration;

    public class RegistrationFinishedEmailCommandHandler :
        TemplateEmailAbstractCommandHandler,
        IRequestHandler<RegistrationFinishedEmailCommand>
    {
        private DynamicShortLinksSettings ShortLinkSettings
        {
            get;
        }

        public RegistrationFinishedEmailCommandHandler(
            IDynamicLinkService dynamicLinkService,
            IOptions<DynamicShortLinksSettings> shortLinkOptions,
            IOptions<SendGridSettings> sendGridSettings,
            IEmailService emailService,
            IOptions<EmailTemplatesSettings> templatesOptions,
            IMediator mediator,
            IBNineDbContext dbContext,
            IMapper mapper,
            ICurrentUserService currentUser) : base(dynamicLinkService, emailService, sendGridSettings, templatesOptions, mediator, dbContext, mapper, currentUser)
        {
            ShortLinkSettings = shortLinkOptions.Value;
        }

        public async Task<Unit> Handle(RegistrationFinishedEmailCommand request, CancellationToken cancellationToken)
        {
            var user = await DbContext.Users
                .Where(x => x.Id == request.UserId)
                .Select(x => new { x.Email, x.FirstName })
                .FirstOrDefaultAsync(cancellationToken);

            if (user == null)
            {
                throw new NotFoundException(nameof(User), request.UserId);
            }

            var templateData = new RegistrationFinishedTemplateData
            {
                Name = user.FirstName,
                Link = ShortLinkSettings.SwitchDepositLink,
            };
            var template = GetTemplate(TemplatesIds.RegistrationFinishedTemplate, user.Email,
                templateData, SendGridSettings.OperationalGroupId);

            await EmailService.SendEmail(template);

            return Unit.Value;
        }
    }
}
