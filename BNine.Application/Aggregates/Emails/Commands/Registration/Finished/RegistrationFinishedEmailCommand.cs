﻿namespace BNine.Application.Aggregates.Emails.Commands.Registration.Finished
{
    using System;
    using MediatR;
    using Newtonsoft.Json;

    public class RegistrationFinishedEmailCommand : IRequest
    {
        [JsonProperty("userId")]
        public Guid UserId
        {
            get; set;
        }
    }
}
