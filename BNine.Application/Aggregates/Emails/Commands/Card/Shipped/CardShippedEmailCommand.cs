﻿namespace BNine.Application.Aggregates.Emails.Commands.Card.Shipped
{
    using MediatR;

    public record CardShippedEmailCommand(string Email, string Name) : IRequest
    {
    }
}
