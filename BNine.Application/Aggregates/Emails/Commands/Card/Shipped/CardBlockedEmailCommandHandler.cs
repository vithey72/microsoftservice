﻿namespace BNine.Application.Aggregates.Emails.Commands.Card.Shipped
{
    using System.Threading;
    using System.Threading.Tasks;
    using AutoMapper;
    using BNine.Settings;
    using Interfaces;
    using Interfaces.Firebase;
    using MediatR;
    using Microsoft.Extensions.Options;
    using Models.Emails.EmailsWithTemplate.Card;

    public class CardShippedEmailCommandHandler :
        TemplateEmailAbstractCommandHandler,
        IRequestHandler<CardShippedEmailCommand>
    {
        public CardShippedEmailCommandHandler(
            IDynamicLinkService dynamicLinkService,
            IEmailService emailService,
            IOptions<SendGridSettings> sendGridSettings,
            IOptions<EmailTemplatesSettings> templatesOptions,
            IMediator mediator,
            IBNineDbContext dbContext,
            IMapper mapper,
            ICurrentUserService currentUser)
            : base(dynamicLinkService, emailService, sendGridSettings, templatesOptions, mediator, dbContext, mapper, currentUser)
        {
        }

        public async Task<Unit> Handle(CardShippedEmailCommand request, CancellationToken cancellationToken)
        {
            var templateData = new CardShippedTemplateData
            {
                FirstName = request.Name,
            };

            var template = GetTemplate(TemplatesIds.CardShippedTemplate, request.Email, templateData, SendGridSettings.OperationalGroupId);

            await EmailService.SendEmail(template);

            return Unit.Value;
        }
    }
}
