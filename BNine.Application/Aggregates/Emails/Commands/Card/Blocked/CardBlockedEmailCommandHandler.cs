﻿namespace BNine.Application.Aggregates.Emails.Commands.Card.Blocked
{
    using System.Threading;
    using System.Threading.Tasks;
    using AutoMapper;
    using BNine.Settings;
    using Common;
    using Interfaces;
    using Interfaces.Firebase;
    using MediatR;
    using Microsoft.Extensions.Options;
    using Models.Emails.EmailsWithTemplate.Card;

    public class CardBlockedEmailCommandHandler :
        TemplateEmailAbstractCommandHandler,
        IRequestHandler<CardBlockedEmailCommand>
    {
        public CardBlockedEmailCommandHandler(
            IDynamicLinkService dynamicLinkService,
            IEmailService emailService,
            IOptions<SendGridSettings> sendGridSettings,
            IOptions<EmailTemplatesSettings> templatesOptions,
            IMediator mediator,
            IBNineDbContext dbContext,
            IMapper mapper,
            ICurrentUserService currentUser) : base(dynamicLinkService, emailService, sendGridSettings, templatesOptions, mediator, dbContext, mapper, currentUser)
        {
        }

        public async Task<Unit> Handle(CardBlockedEmailCommand request, CancellationToken cancellationToken)
        {
            var cardNumber = SecretNumberHelper.GetLastFourDigits(request.CardNumber);

            var templateData = new CardBlockedTemplateData
            {
                Name = request.Name,
                CardNumber = cardNumber
            };

            var template = GetTemplate(TemplatesIds.CardBlockedTemplate, request.EmailAddress, templateData, SendGridSettings.OperationalGroupId);

            await EmailService.SendEmail(template);

            return Unit.Value;
        }
    }
}
