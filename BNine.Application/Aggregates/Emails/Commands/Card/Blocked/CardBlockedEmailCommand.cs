﻿namespace BNine.Application.Aggregates.Emails.Commands.Card.Blocked
{
    using MediatR;
    using Newtonsoft.Json;

    public class CardBlockedEmailCommand : IRequest
    {
        public CardBlockedEmailCommand(string email, string name, string cardNumber)
        {
            EmailAddress = email;
            Name = name;
            CardNumber = cardNumber;
        }

        [JsonProperty("email")]
        public string EmailAddress
        {
            get;
            set;
        }

        [JsonProperty("name")]
        public string Name
        {
            get;
            set;
        }

        [JsonProperty("cardNumber")]
        public string CardNumber
        {
            get;
            set;
        }
    }
}
