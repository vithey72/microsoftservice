﻿namespace BNine.Application.Aggregates.Emails.Commands.Argyle.Connect
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading;
    using System.Threading.Tasks;
    using AutoMapper;
    using BNine.Settings;
    using Enums;
    using Interfaces;
    using Interfaces.Firebase;
    using MediatR;
    using Microsoft.Extensions.Options;
    using Models.Emails.EmailsWithTemplate;
    using Models.Emails.EmailsWithTemplate.Argyle;

    public class ConnectArgyleEmailCommandHandler :
        TemplateEmailAbstractCommandHandler,
        IRequestHandler<ConnectArgyleEmailCommand>
    {
        private DynamicShortLinksSettings ShortLinkSettings
        {
            get;
        }

        public ConnectArgyleEmailCommandHandler(
            IDynamicLinkService dynamicLinkService,
            IOptions<DynamicShortLinksSettings> shortLinkOptions,
            IEmailService emailService,
            IOptions<SendGridSettings> sendGridSettings,
            IOptions<EmailTemplatesSettings> templatesOptions,
            IMediator mediator,
            IBNineDbContext dbContext,
            IMapper mapper,
            ICurrentUserService currentUser) : base(dynamicLinkService, emailService, sendGridSettings, templatesOptions, mediator, dbContext, mapper, currentUser)
        {
            ShortLinkSettings = shortLinkOptions.Value;
        }

        public async Task<Unit> Handle(ConnectArgyleEmailCommand request, CancellationToken cancellationToken)
        {
            var currentDate = DateTime.UtcNow.Date;

            var firstDate = currentDate.AddDays(-1);

            var usersDataByDate = DbContext.Users
                .Where(x => x.Status == UserStatus.Active)
                .Where(x => x.EarlySalarySettings.EarlySalarySubscriptionDate >= firstDate &&
                            x.EarlySalarySettings.EarlySalarySubscriptionDate < firstDate.AddDays(1))
                .Select(x => new { x.EarlySalarySettings.EarlySalarySubscriptionDate, x.Email, x.FirstName })
                .ToList();

            var templates = new List<EmailMessageTemplate>();

            templates.AddRange(usersDataByDate.Select(x => GetTemplate(x.FirstName, x.Email, TemplatesIds.ConnectArgyleProposalFirstTemplate)));

            await EmailService.SendEmails(templates);
            return Unit.Value;
        }

        private EmailMessageTemplate GetTemplate(string firstName, string email, string templateId)
        {
            var templateData = new ConnectArgyleTemplateData
            {
                Name = firstName,
                Link = ShortLinkSettings.SwitchDepositLink,

            };

            return GetTemplate(templateId, email, templateData, SendGridSettings.MarketingGroupId);
        }
    }
}
