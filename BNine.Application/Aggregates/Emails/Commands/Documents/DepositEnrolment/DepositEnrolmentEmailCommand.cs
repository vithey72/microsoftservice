﻿namespace BNine.Application.Aggregates.Emails.Commands.Documents.DepositEnrolment
{
    using MediatR;
    using Models.Emails.EmailsWithTemplate;
    using Newtonsoft.Json;

    public class DepositEnrolmentEmailCommand : IRequest<bool>
    {
        public DepositEnrolmentEmailCommand(string email, string name, EmailAttachmentData attachment, bool manual = false)
        {
            Email = email;
            Name = name;
            Attachment = attachment;
            Manual = manual;
        }

        [JsonProperty("email")]
        public string Email
        {
            get;
        }

        [JsonProperty("name")]
        public string Name
        {
            get;
        }

        [JsonProperty("manual")]
        public bool Manual
        {
            get;
        }

        [JsonProperty("attachment")]
        public EmailAttachmentData Attachment
        {
            get;
        }
    }
}
