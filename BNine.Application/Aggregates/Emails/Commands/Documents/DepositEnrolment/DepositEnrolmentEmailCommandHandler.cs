﻿namespace BNine.Application.Aggregates.Emails.Commands.Documents.DepositEnrolment
{
    using System.Threading;
    using System.Threading.Tasks;
    using AutoMapper;
    using BNine.Settings;
    using Interfaces;
    using Interfaces.Firebase;
    using MediatR;
    using Microsoft.Extensions.Options;
    using Models.Emails.EmailsWithTemplate.Documents;

    public class DepositEnrolmentEmailCommandHandler :
        TemplateEmailAbstractCommandHandler,
        IRequestHandler<DepositEnrolmentEmailCommand, bool>
    {
        public DepositEnrolmentEmailCommandHandler(
            IDynamicLinkService dynamicLinkService,
            IEmailService emailService,
            IOptions<SendGridSettings> sendGridSettings,
            IOptions<EmailTemplatesSettings> templatesOptions,
            IMediator mediator,
            IBNineDbContext dbContext,
            IMapper mapper,
            ICurrentUserService currentUser) : base(dynamicLinkService, emailService, sendGridSettings, templatesOptions, mediator, dbContext, mapper, currentUser)
        {
        }

        public async Task<bool> Handle(DepositEnrolmentEmailCommand request, CancellationToken cancellationToken)
        {
            var templateData = new DepositEnrolmentTemplateData { Name = request.Name };

            var templateId = request.Manual ? TemplatesIds.EarlySalaryManualFormTemplate : TemplatesIds.DepositEnrolmentTemplate;

            var template = GetTemplate(
                templateId,
                request.Email,
                templateData,
                SendGridSettings.OperationalGroupId,
                new[] { request.Attachment });

            var response = await EmailService.SendEmail(template);

            return response.IsSucceeded;
        }
    }
}
