﻿namespace BNine.Application.Aggregates.Emails.Commands.Documents.SpendingAccountStatement
{
    using System.Threading;
    using System.Threading.Tasks;
    using AutoMapper;
    using BNine.Settings;
    using Enums;
    using Interfaces;
    using Interfaces.Firebase;
    using MediatR;
    using Microsoft.Extensions.Options;
    using Models.Emails.EmailsWithTemplate.Documents;

    public class SpendingAccountStatementEmailCommandHandler :
        TemplateEmailAbstractCommandHandler,
        IRequestHandler<SpendingAccountStatementEmailCommand>
    {
        private readonly IPartnerProviderService _partnerProvider;

        public SpendingAccountStatementEmailCommandHandler(
            IDynamicLinkService dynamicLinkService,
            IEmailService emailService,
            IOptions<SendGridSettings> sendGridSettings,
            IOptions<EmailTemplatesSettings> templatesOptions,
            IMediator mediator,
            IBNineDbContext dbContext,
            IMapper mapper,
            ICurrentUserService currentUser,
            IPartnerProviderService partnerProvider
            )
            : base(dynamicLinkService, emailService, sendGridSettings, templatesOptions, mediator, dbContext, mapper, currentUser)
        {
            _partnerProvider = partnerProvider;
        }

        public async Task<Unit> Handle(SpendingAccountStatementEmailCommand request, CancellationToken cancellationToken)
        {
            var templateData = new SpendingAccountStatementData { Name = request.Name, };

            var templateId = _partnerProvider.GetPartner() == PartnerApp.MBanqApp
                ? "d-40269c0ab9ab467283fdc38cf69a3d37"
                : TemplatesIds.SpendingAccountStatement;
            var template = GetTemplate(
                templateId,
                request.Email,
                templateData,
                SendGridSettings.OperationalGroupId,
                new[] { request.Attachment });

            await EmailService.SendEmail(template);

            return Unit.Value;
        }
    }
}
