﻿namespace BNine.Application.Aggregates.Emails.Commands.Documents.SpendingAccountStatement
{
    using MediatR;
    using Models.Emails.EmailsWithTemplate;
    using Newtonsoft.Json;

    public class SpendingAccountStatementEmailCommand : IRequest
    {
        public SpendingAccountStatementEmailCommand(string email, string name, EmailAttachmentData attachment)
        {
            Email = email;
            Name = name;
            Attachment = attachment;
        }

        [JsonProperty("email")]
        public string Email
        {
            get;
        }

        [JsonProperty("name")]
        public string Name
        {
            get;
        }

        [JsonProperty("attachment")]
        public EmailAttachmentData Attachment
        {
            get;
        }
    }
}
