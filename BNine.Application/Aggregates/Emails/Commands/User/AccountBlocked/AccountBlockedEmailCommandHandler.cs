﻿namespace BNine.Application.Aggregates.Emails.Commands.User.AccountBlocked
{
    using System.Threading;
    using System.Threading.Tasks;
    using AutoMapper;
    using BNine.Settings;
    using Common;
    using Interfaces;
    using Interfaces.Firebase;
    using MediatR;
    using Microsoft.Extensions.Options;
    using Models.Emails.EmailsWithTemplate.User;

    public class AccountBlockedEmailCommandHandler :
        TemplateEmailAbstractCommandHandler,
        IRequestHandler<AccountBlockedEmailCommand>
    {
        public AccountBlockedEmailCommandHandler(
            IDynamicLinkService dynamicLinkService,
            IEmailService emailService,
            IOptions<SendGridSettings> sendGridSettings,
            IOptions<EmailTemplatesSettings> templatesOptions,
            IMediator mediator,
            IBNineDbContext dbContext,
            IMapper mapper,
            ICurrentUserService currentUser) : base(dynamicLinkService, emailService, sendGridSettings, templatesOptions, mediator, dbContext, mapper, currentUser)
        {
        }

        public async Task<Unit> Handle(AccountBlockedEmailCommand request, CancellationToken cancellationToken)
        {
            var accountNumber = SecretNumberHelper.GetLastFourDigits(request.AccountNumber);

            var templateData = new AccountBlockedTemplateData
            {
                Name = request.Name,
                AccountNumber = accountNumber
            };

            var template = GetTemplate(TemplatesIds.AccountBlockedTemplate, request.EmailAddress, templateData, SendGridSettings.OperationalGroupId);

            await EmailService.SendEmail(template);

            return Unit.Value;
        }
    }
}
