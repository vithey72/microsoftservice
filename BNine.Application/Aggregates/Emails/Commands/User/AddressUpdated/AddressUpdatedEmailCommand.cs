﻿namespace BNine.Application.Aggregates.Emails.Commands.User.AddressUpdated
{
    using MediatR;
    using Newtonsoft.Json;

    public class AddressUpdatedEmailCommand : IRequest
    {
        public AddressUpdatedEmailCommand(string email, string name)
        {
            EmailAddress = email;
            Name = name;
        }

        [JsonProperty("email")]
        public string EmailAddress
        {
            get;
            set;
        }

        [JsonProperty("name")]
        public string Name
        {
            get;
            set;
        }
    }
}
