﻿namespace BNine.Application.Aggregates.Emails.Commands.User.ConfirmEmailUpdate
{
    using System.Threading;
    using System.Threading.Tasks;
    using AutoMapper;
    using BNine.Application.Exceptions;
    using BNine.Settings;
    using Interfaces;
    using Interfaces.Firebase;
    using MediatR;
    using Microsoft.Extensions.Options;
    using Models.Emails.EmailsWithTemplate.User;

    public class ConfirmEmailUpdateEmailCommandHandler :
        TemplateEmailAbstractCommandHandler,
        IRequestHandler<ConfirmEmailUpdateEmailCommand>
    {
        public ConfirmEmailUpdateEmailCommandHandler(
            IDynamicLinkService dynamicLinkService,
            IEmailService emailService,
            IOptions<SendGridSettings> sendGridSettings,
            IOptions<EmailTemplatesSettings> templatesOptions,
            IMediator mediator,
            IBNineDbContext dbContext,
            IMapper mapper,
            ICurrentUserService currentUser) : base(dynamicLinkService, emailService, sendGridSettings, templatesOptions, mediator, dbContext, mapper, currentUser)
        {
        }

        public async Task<Unit> Handle(ConfirmEmailUpdateEmailCommand request, CancellationToken cancellationToken)
        {
            var user = DbContext
                .Users
                .FirstOrDefault(u => u.Id == CurrentUser.UserId);

            if (user == null)
            {
                throw new NotFoundException($"User with id {CurrentUser.UserId} not found");
            }

            var linkSuffix = "/email-update-confirmation";
            var linkQueryObject = new
            {
                EmailChangeRequestId = request.ChangeRequestId,
                UserId = request.UserId
            };

            var dynamicLink = DynamicLinkService.GetDynamicShortLink(linkQueryObject, linkSuffix);

            var templateData = new ConfirmEmailUpdateTemplateData
            {
                Link = dynamicLink,
                Name = user.FirstName
            };
            var template = GetTemplate(TemplatesIds.ConfirmEmailUpdateTemplate, request.EmailAddress, templateData, SendGridSettings.OperationalGroupId);

            await EmailService.SendEmail(template);

            return Unit.Value;
        }
    }
}
