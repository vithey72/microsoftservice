﻿namespace BNine.Application.Aggregates.Emails.Commands.User.ConfirmEmailUpdate
{
    using System;
    using MediatR;
    using Newtonsoft.Json;

    public class ConfirmEmailUpdateEmailCommand : IRequest
    {
        public ConfirmEmailUpdateEmailCommand(Guid userId, string email, Guid changeRequestId)
        {
            UserId = userId;
            EmailAddress = email;
            ChangeRequestId = changeRequestId.ToString();
        }

        [JsonProperty("email")]
        public string EmailAddress
        {
            get;
            set;
        }

        [JsonProperty("changeRequestId")]
        public string ChangeRequestId
        {
            get;
            set;
        }

        [JsonProperty("userId")]
        public Guid UserId
        {
            get;
            set;
        }
    }
}
