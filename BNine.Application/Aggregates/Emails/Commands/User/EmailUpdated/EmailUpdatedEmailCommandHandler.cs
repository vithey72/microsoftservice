﻿namespace BNine.Application.Aggregates.Emails.Commands.User.EmailUpdated
{
    using System.Threading;
    using System.Threading.Tasks;
    using AutoMapper;
    using BNine.Settings;
    using Interfaces;
    using Interfaces.Firebase;
    using MediatR;
    using Microsoft.Extensions.Options;
    using Models.Emails.EmailsWithTemplate.User;

    public class EmailUpdatedEmailCommandHandler :
        TemplateEmailAbstractCommandHandler,
        IRequestHandler<EmailUpdatedEmailCommand>
    {
        public EmailUpdatedEmailCommandHandler(
            IDynamicLinkService dynamicLinkService,
            IEmailService emailService,
            IOptions<SendGridSettings> sendGridSettings,
            IOptions<EmailTemplatesSettings> templatesOptions,
            IMediator mediator,
            IBNineDbContext dbContext,
            IMapper mapper,
            ICurrentUserService currentUser) : base(dynamicLinkService, emailService, sendGridSettings, templatesOptions, mediator, dbContext, mapper, currentUser)
        {
        }

        public async Task<Unit> Handle(EmailUpdatedEmailCommand request, CancellationToken cancellationToken)
        {
            var templateData = new EmailUpdatedTemplateData
            {
                Name = request.Name
            };

            var template = GetTemplate(TemplatesIds.EmailUpdatedTemplate, request.EmailAddress, templateData, SendGridSettings.OperationalGroupId);

            await EmailService.SendEmail(template);

            return Unit.Value;
        }
    }
}
