﻿namespace BNine.Application.Aggregates.Emails.Commands.User.EmailUpdated
{
    using MediatR;
    using Newtonsoft.Json;

    public class EmailUpdatedEmailCommand : IRequest
    {
        public EmailUpdatedEmailCommand(string email, string name)
        {
            EmailAddress = email;
            Name = name;
        }

        [JsonProperty("email")]
        public string EmailAddress
        {
            get;
            set;
        }

        [JsonProperty("name")]
        public string Name
        {
            get;
            set;
        }
    }
}
