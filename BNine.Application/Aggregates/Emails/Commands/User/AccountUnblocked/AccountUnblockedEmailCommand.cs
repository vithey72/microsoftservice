﻿namespace BNine.Application.Aggregates.Emails.Commands.User.AccountUnblocked
{
    using MediatR;
    using Newtonsoft.Json;

    public class AccountUnblockedEmailCommand : IRequest
    {
        public AccountUnblockedEmailCommand(string email, string name, string accountNumber)
        {
            EmailAddress = email;
            Name = name;
            AccountNumber = accountNumber;
        }

        [JsonProperty("email")]
        public string EmailAddress
        {
            get;
            set;
        }

        [JsonProperty("name")]
        public string Name
        {
            get;
            set;
        }

        [JsonProperty("accountNumber")]
        public string AccountNumber
        {
            get;
            set;
        }
    }
}
