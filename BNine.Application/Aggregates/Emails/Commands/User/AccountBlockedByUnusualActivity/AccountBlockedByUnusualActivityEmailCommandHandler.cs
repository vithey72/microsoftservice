﻿namespace BNine.Application.Aggregates.Emails.Commands.User.AccountBlockedByUnusualActivity
{
    using System.Threading;
    using System.Threading.Tasks;
    using AutoMapper;
    using BNine.Settings;
    using Common;
    using Interfaces;
    using Interfaces.Firebase;
    using MediatR;
    using Microsoft.Extensions.Options;
    using Models.Emails.EmailsWithTemplate.User;

    public class AccountBlockedByUnusualActivityEmailCommandHandler :
        TemplateEmailAbstractCommandHandler,
        IRequestHandler<AccountBlockedByUnusualActivityEmailCommand>
    {
        public AccountBlockedByUnusualActivityEmailCommandHandler(
            IDynamicLinkService dynamicLinkService,
            IEmailService emailService,
            IOptions<SendGridSettings> sendGridSettings,
            IOptions<EmailTemplatesSettings> templatesOptions,
            IMediator mediator,
            IBNineDbContext dbContext,
            IMapper mapper,
            ICurrentUserService currentUser) : base(dynamicLinkService, emailService, sendGridSettings, templatesOptions, mediator, dbContext, mapper, currentUser)
        {
        }

        public async Task<Unit> Handle(AccountBlockedByUnusualActivityEmailCommand request, CancellationToken cancellationToken)
        {
            var accountNumber = SecretNumberHelper.GetLastFourDigits(request.AccountNumber);

            var templateData = new AccountBlockedByUnusualActivityTemplateData
            {
                Name = request.Name,
                AccountNumber = accountNumber
            };

            var template = GetTemplate(TemplatesIds.AccountBlockedByUnusualActivity, request.EmailAddress, templateData, SendGridSettings.OperationalGroupId);

            await EmailService.SendEmail(template);

            return Unit.Value;
        }
    }
}
