﻿namespace BNine.Application.Aggregates.Emails.Commands.User.PhoneUpdated
{
    using MediatR;
    using Newtonsoft.Json;

    public class PhoneUpdatedEmailCommand : IRequest
    {
        public PhoneUpdatedEmailCommand(string email, string name)
        {
            EmailAddress = email;
            Name = name;
        }

        [JsonProperty("email")]
        public string EmailAddress
        {
            get;
            set;
        }

        [JsonProperty("name")]
        public string Name
        {
            get;
            set;
        }
    }
}
