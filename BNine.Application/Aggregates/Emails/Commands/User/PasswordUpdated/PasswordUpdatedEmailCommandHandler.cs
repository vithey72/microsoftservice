﻿namespace BNine.Application.Aggregates.Emails.Commands.User.PasswordUpdated
{
    using System.Threading;
    using System.Threading.Tasks;
    using AutoMapper;
    using BNine.Settings;
    using Interfaces;
    using Interfaces.Firebase;
    using MediatR;
    using Microsoft.Extensions.Options;
    using Models.Emails.EmailsWithTemplate.User;

    public class PasswordUpdatedEmailCommandHandler :
        TemplateEmailAbstractCommandHandler,
        IRequestHandler<PasswordUpdatedEmailCommand>
    {
        public PasswordUpdatedEmailCommandHandler(
            IDynamicLinkService dynamicLinkService,
            IEmailService emailService,
            IOptions<SendGridSettings> sendGridSettings,
            IOptions<EmailTemplatesSettings> templatesOptions,
            IMediator mediator,
            IBNineDbContext dbContext,
            IMapper mapper,
            ICurrentUserService currentUser) : base(dynamicLinkService, emailService, sendGridSettings, templatesOptions, mediator, dbContext, mapper, currentUser)
        {
        }

        public async Task<Unit> Handle(PasswordUpdatedEmailCommand request, CancellationToken cancellationToken)
        {
            var template = GetTemplate(TemplatesIds.PasswordChangedTemplate, request.Email, new UpdatePasswordTemplateData(), SendGridSettings.OperationalGroupId);

            await EmailService.SendEmail(template);

            return Unit.Value;
        }
    }
}
