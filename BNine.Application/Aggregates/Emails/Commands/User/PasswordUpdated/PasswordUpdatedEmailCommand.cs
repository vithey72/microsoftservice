﻿namespace BNine.Application.Aggregates.Emails.Commands.User.PasswordUpdated
{
    using MediatR;

    public class PasswordUpdatedEmailCommand : IRequest
    {
        public PasswordUpdatedEmailCommand(string email)
        {
            Email = email;
        }

        public string Email
        {
            get;
            set;
        }
    }
}
