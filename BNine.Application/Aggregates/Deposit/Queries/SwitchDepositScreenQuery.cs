﻿namespace BNine.Application.Aggregates.Deposit.Queries;

using MediatR;
using Models;

public record SwitchDepositScreenQuery : IRequest<SwitchDepositScreenViewModel>;

