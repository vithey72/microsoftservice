﻿namespace BNine.Application.Aggregates.Deposit.Queries;

using Abstractions;
using AutoMapper;
using CommonModels.Buttons;
using CommonModels.Text;
using Constants;
using Helpers;
using Interfaces;
using MediatR;
using Models;
using Models.Sections;

public class SwitchDepositScreenQueryHandler
    : AbstractRequestHandler
    , IRequestHandler<SwitchDepositScreenQuery, SwitchDepositScreenViewModel>
{
    private readonly IPartnerProviderService _partnerProviderService;

    public SwitchDepositScreenQueryHandler(
        IMediator mediator,
        IBNineDbContext dbContext,
        IMapper mapper,
        ICurrentUserService currentUser,
        IPartnerProviderService partnerProviderService
        )
        :
        base(mediator, dbContext, mapper, currentUser)
    {
        _partnerProviderService = partnerProviderService;
    }

    public Task<SwitchDepositScreenViewModel> Handle(SwitchDepositScreenQuery request,
        CancellationToken cancellationToken)
    {
        var placeHolder = "{UPTO}";
        var sectionViewModel = new SwitchDepositSectionViewModel()
        {
            Title = new FormattedText($"GET A CASH ADVANCE\n{placeHolder}",
                new FormattedTextFragmentViewModel[]
                {
                    new(placeHolder, "OF UP TO $500", TypographicalEmphasis.Underlined, ThematicColor.BrandPrimary)
                }),
            ImageUrl = "https://b9prodaccount.blob.core.windows.net/static-documents-container/Misc/deposit_screen_section_image.png",
            TopText =
                "You can get an instant cash advance on your paycheck " +
                "as soon as your direct deposit of at least $300 is deposited into a B9 bank account. " +
                "Any type of income is eligible " +
                "whether full-time, part-time, contract, and even " +
                "govt. benefits like SSI.",
            BottomText =
                $"Fully secured direct deposit switch can be changed by client at any time. Funds deposited into {StringProvider.GetPartnerName(_partnerProviderService)} " +
                $"Accounts provided by Evolve Bank & Trust are covered by the FDIC standard deposit insurance amount. " +
                $"The standard deposit insurance amount is $250,000 per depositor, per insured bank, for each account ownership category."
        };

        return Task.FromResult(
            new SwitchDepositScreenViewModel
            {
                Title = "Deposit",
                Subtitle = new LinkedText("Paycheck"),
                ImageUrl = "https://b9prodaccount.blob.core.windows.net/static-documents-container/Misc/deposit_screen_main_image.png",
                SectionViewModel = sectionViewModel,
                Text = "OR GET THE PRE-FILLED-OUT FORM:",
                Buttons = new[]
                {
                    new ButtonViewModel
                    {
                        Text = "ONLINE DEPOSIT SWITCH",
                        Style = ButtonStyleEnum.Solid
                    }.WithDeeplink(DeepLinkRoutes.Argyle),
                    new ButtonViewModel
                    {
                        Text = "MANUAL DEPOSIT SWITCH",
                        Style = ButtonStyleEnum.Bordered
                    }.WithDeeplink(DeepLinkRoutes.EarlySalaryManualForm)
                }

            });
    }
}
