﻿namespace BNine.Application.Aggregates.Deposit.Models.Sections;

using BNine.Application.Aggregates.CommonModels.DialogElements;
using BNine.Application.Aggregates.CommonModels.Text;

public class SwitchDepositSectionViewModel : SectionViewModelBaseGeneric<FormattedText, string>
{
    public string TopText
    {
        get;
        set;
    }

    public string BottomText
    {
        get;
        set;
    }
}
