﻿namespace BNine.Application.Aggregates.Deposit.Models;

using CommonModels.Dialog;
using Sections;

public class SwitchDepositScreenViewModel : GenericDialogRichBodyViewModel
{
    public SwitchDepositSectionViewModel SectionViewModel
    {
        get;
        set;
    }

    public string Text
    {
        get;
        set;
    }
}
