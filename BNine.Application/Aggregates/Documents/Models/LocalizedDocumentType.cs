﻿namespace BNine.Application.Aggregates.Documents.Models
{
    public class LocalizedDocumentType
    {
        public string Key
        {
            get; set;
        }

        public string LocalizedValue
        {
            get; set;
        }

        public int Order
        {
            get; set;
        }

        public string ExternalName
        {
            get; set;
        }
    }
}
