﻿namespace BNine.Application.Aggregates.Documents.Queries.GetDocumentTypes
{
    using System.Collections.Generic;
    using System.Globalization;
    using System.Text.Json.Serialization;
    using BNine.Application.Aggregates.Documents.Models;
    using MediatR;

    public class GetDocumentTypesQuery : IRequest<IEnumerable<LocalizedDocumentType>>
    {
        public GetDocumentTypesQuery(CultureInfo cultureInfo)
        {
            CultureInfo = cultureInfo;
        }

        [JsonIgnore]
        public CultureInfo CultureInfo
        {
            get; set;
        }
    }
}
