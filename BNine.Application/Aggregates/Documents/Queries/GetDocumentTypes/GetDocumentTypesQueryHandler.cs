﻿namespace BNine.Application.Aggregates.Documents.Queries.GetDocumentTypes
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading;
    using System.Threading.Tasks;
    using AutoMapper;
    using BNine.Application.Abstractions;
    using BNine.Application.Aggregates.Documents.Models;
    using BNine.Application.Interfaces;
    using Helpers;
    using MediatR;
    using Microsoft.EntityFrameworkCore;

    public class GetDocumentTypesQueryHandler
        : AbstractRequestHandler, IRequestHandler<GetDocumentTypesQuery, IEnumerable<LocalizedDocumentType>>
    {
        public GetDocumentTypesQueryHandler(IMediator mediator, IBNineDbContext dbContext, IMapper mapper, ICurrentUserService currentUser) : base(mediator, dbContext, mapper, currentUser)
        {
        }

        public async Task<IEnumerable<LocalizedDocumentType>> Handle(GetDocumentTypesQuery request, CancellationToken cancellationToken)
        {
            var documentTypes = await DbContext.DocumentTypes.ToListAsync();

            var localizedDocumentTypes = documentTypes.Select(
                x => new LocalizedDocumentType
                {
                    Key = x.Key,
                    LocalizedValue = x.Value
                        .First(n => n.Language == LocalizationHelper.GetLanguage(request.CultureInfo))
                        .Text,
                    Order = x.Order,
                    ExternalName = x.ExternalName
                }
                )
                .Where(x => x.Order != -1)
                .OrderBy(x => x.Order)
                .AsEnumerable();

            return localizedDocumentTypes;
        }
    }
}
