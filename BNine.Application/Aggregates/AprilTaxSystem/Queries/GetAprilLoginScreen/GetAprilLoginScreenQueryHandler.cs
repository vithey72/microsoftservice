﻿namespace BNine.Application.Aggregates.AprilTaxSystem.Queries.GetAprilLoginScreen;

using Abstractions;
using AutoMapper;
using BNine.Settings;
using CommonModels.Buttons;
using CommonModels.Dialog;
using CommonModels.DialogElements;
using CommonModels.Text;
using Enums;
using Interfaces;
using MediatR;
using Microsoft.Extensions.Options;
using Models;

public class GetAprilLoginScreenQueryHandler
    : AbstractRequestHandler
    , IRequestHandler<GetAprilLoginScreenQuery, GetAprilLoginScreenViewModel>
{
    private readonly IBNineDbContext _dbContext;
    private readonly IAprilTaxService _aprilTaxService;
    private readonly AprilTaxServiceSettings _settings;

    public GetAprilLoginScreenQueryHandler(
        IMediator mediator,
        IBNineDbContext dbContext,
        IMapper mapper,
        ICurrentUserService currentUser,
        IAprilTaxService aprilTaxService,
        IOptions<AprilTaxServiceSettings> options
        ) : base(mediator, dbContext, mapper, currentUser)
    {
        _dbContext = dbContext;
        _aprilTaxService = aprilTaxService;
        _settings = options.Value;
    }

    public async Task<GetAprilLoginScreenViewModel> Handle(GetAprilLoginScreenQuery request, CancellationToken cancellationToken)
    {
        return new GetAprilLoginScreenViewModel
        {
            Header = new GenericDialogHeaderViewModel
            {
                Title = "Online TAX refund",
            },
            Body = new GetAprilLoginScreenViewModel.AprilLoginScreenBody
            {
                Title = "B9 Smart Tax Service",
                ImageUrl = "https://b9prodaccount.blob.core.windows.net/static-documents-container/Misc/april_tax.png",
                BulletPoints = new[]
                {
                    new BulletPointViewModel("Quick and easy tax filing online").WithKnownType(BulletPointTypeEnum.GreenCheckmarkImage),
                    new BulletPointViewModel("TAX refund to your B9 Account").WithKnownType(BulletPointTypeEnum.GreenCheckmarkImage),
                    new BulletPointViewModel("Qualified customer support team").WithKnownType(BulletPointTypeEnum.GreenCheckmarkImage),
                },
                Subtitle = "With B9, you can master the tax code without actually learning anything about the tax code",
                Buttons = new[]
                {
                    new ButtonViewModel("NEXT", ButtonStyleEnum.Solid)
                        .WithUrl(await GetRegistrationUrl(cancellationToken), LinkFollowMode.BrowserPreview),
                },
                Footnote = new LinkedText(
                    "The service is provided by our partner, April. When you click the NEXT button, you will be redirected to a secured page for completion of the necessary steps.\n" +
                    "The cost of the service is $20, payment is made to April after filling out your tax return."),
            },
        };
    }

    private async Task<string> GetRegistrationUrl(CancellationToken cancellationToken)
    {
        var user = _dbContext.Users.Single(x => x.Id == CurrentUser.UserId);

        var authToken = await _aprilTaxService.GetAccessToken(user.Id, user.Email, cancellationToken);

        var registrationHash = await _aprilTaxService.GetLoginUrl(authToken, cancellationToken);

        return _settings.ApplicationUrl + "?userRegistrationCode=" + registrationHash;
    }
}
