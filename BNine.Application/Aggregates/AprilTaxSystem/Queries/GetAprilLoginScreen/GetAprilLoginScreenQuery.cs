﻿namespace BNine.Application.Aggregates.AprilTaxSystem.Queries.GetAprilLoginScreen;

using MediatR;
using Models;

public record GetAprilLoginScreenQuery : IRequest<GetAprilLoginScreenViewModel>;
