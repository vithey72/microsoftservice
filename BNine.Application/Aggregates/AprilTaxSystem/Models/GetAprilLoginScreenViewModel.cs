﻿namespace BNine.Application.Aggregates.AprilTaxSystem.Models;

using CommonModels.Buttons;
using CommonModels.Dialog;
using CommonModels.DialogElements;
using CommonModels.Text;

public class GetAprilLoginScreenViewModel
{
    public GenericDialogHeaderViewModel Header
    {
        get;
        set;
    }

    public AprilLoginScreenBody Body
    {
        get;
        set;
    }

    public class AprilLoginScreenBody
    {
        public string Title
        {
            get;
            set;
        }

        public string ImageUrl
        {
            get;
            set;
        }

        public BulletPointViewModel[] BulletPoints
        {
            get;
            set;
        }

        public string Subtitle
        {
            get;
            set;
        }

        public LinkedText Footnote
        {
            get;
            set;
        }

        public ButtonViewModel[] Buttons
        {
            get;
            set;
        }
    }
}
