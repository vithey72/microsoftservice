﻿namespace BNine.Application.Aggregates.Array.Queries;

using MediatR;

public record CreateArrayUserQuery : IRequest<bool>;
