﻿namespace BNine.Application.Aggregates.Array.Queries
{
    using System;
    using System.Threading;
    using System.Threading.Tasks;
    using Abstractions;
    using AutoMapper;
    using BNine.Application.Aggregates.CreditScore.Models;
    using BNine.Application.Exceptions;
    using BNine.Application.Exceptions.ApiResponse;
    using BNine.Application.Interfaces.Bank.Administration;
    using BNine.Array.Aggregates.Models;
    using BNine.Array.Interfaces;
    using BNine.Constants;
    using BNine.Domain.Entities.BankAccount;
    using BNine.Domain.Entities.TariffPlan;
    using BNine.Domain.Entities.User;
    using BNine.Enums.TariffPlan;
    using Enums;
    using Helpers;
    using Interfaces;
    using MediatR;
    using Microsoft.EntityFrameworkCore;

    public class GetArrayCustomerPropertiesQueryHandler
    : AbstractRequestHandler,
        IRequestHandler<GetArrayCustomerPropertiesQuery, GetArrayCustomerPropertiesResponse>
    {
        private readonly IArrayUserRegistrationService _arrayService;
        private readonly ITariffPlanService _tariffPlanService;
        private readonly IBankSavingAccountsService _bankAccountService;
        private readonly IBankChargesService _bankChargesService;
        private readonly ICheckCreditScorePaidService _creditScorePaidService;

        public GetArrayCustomerPropertiesQueryHandler(
            IBankSavingAccountsService bankAccountService,
            ITariffPlanService tariffPlanService,
            IMediator mediator,
            IBNineDbContext dbContext,
            IMapper mapper,
            IArrayUserRegistrationService arrayService,
            ICurrentUserService currentUser,
            IBankChargesService bankChargesService,
            ICheckCreditScorePaidService creditScorePaidService
            )
            : base(mediator, dbContext, mapper, currentUser)
        {
            _bankChargesService = bankChargesService;
            _arrayService = arrayService;
            _tariffPlanService = tariffPlanService;
            _bankAccountService = bankAccountService;
            _creditScorePaidService = creditScorePaidService;
        }

        public async Task<GetArrayCustomerPropertiesResponse> Handle(GetArrayCustomerPropertiesQuery request, CancellationToken cancellationToken)
        {
            var user = await DbContext
                .Users
                .Include(u => u.ArrayCustomerSettings)
                .Include(u => u.CurrentAccount)
                .Include(u => u.UserTariffPlans)
                .ThenInclude(ut => ut.TariffPlan)
                .Include(u => u.Identity)
                .FirstOrDefaultAsync(u => u.Id == CurrentUser.UserId, cancellationToken);

            if (user == null)
            {
                throw new NotFoundException(nameof(User), user);
            }

            await ThrowIfInvalidTariff(user, cancellationToken);

            if (user?.ArrayCustomerSettings == null)
            {
                return null;
            }

            var userToken = await _arrayService.CreateUserToken(
                user.ArrayCustomerSettings.ArrayUserId,
                cancellationToken);

            if (userToken.HasValue && user.ArrayCustomerSettings.ActivatedAt == null)
            {
                user.ArrayCustomerSettings.ActivatedAt = DateTime.Now;
                await DbContext.SaveChangesAsync(cancellationToken);
            }

            return new GetArrayCustomerPropertiesResponse
            {
                UserId = user.ArrayCustomerSettings.ArrayUserId,
                ClientKey = user.ArrayCustomerSettings.ArrayUserId,
                Provider = user.Identity.Type == IdentityType.SSN ? "exp" : "efx",
                UserToken = userToken,
            };
        }

        private async Task ThrowIfInvalidTariff(User user, CancellationToken cancellationToken)
        {
            var tariff = await _tariffPlanService.GetCurrentTariff(user.Id, cancellationToken);

            await ThrowIfDontHaveAccessToArray(user.CurrentAccount, tariff, cancellationToken);
        }

        private async Task ThrowIfDontHaveAccessToArray(CurrentAccount bankAccount, TariffPlan tariff, CancellationToken cancellationToken)
        {
            switch (tariff)
            {
                case not null when tariff.Type == TariffPlanFamily.Premium:
                    await ThrowIfPremiumMonthlyChargeDeferred(bankAccount, tariff);
                    break;

                case not null when tariff.Type == TariffPlanFamily.Advance:
                    await ThrowIfCheckCreditScorePaidServiceDisabled(cancellationToken);
                    break;

                default:
                    throw new ApiResponseException(ApiResponseErrorCodes.WrongTariffPlan, $"User doesn't have access to this feature");
            }
        }

        private async Task ThrowIfPremiumMonthlyChargeDeferred(CurrentAccount bankAccount, TariffPlan userTariff)
        {
            var monthlyCharge = await _bankChargesService.GetActiveTariffCharge(bankAccount.ExternalId);

            if (monthlyCharge == null)
            {
                throw new NotFoundException(nameof(monthlyCharge), userTariff.ExternalChargeId);
            }

            if (monthlyCharge.TotalDeferredChargeAmount != decimal.Zero)
            {
                throw new ApiResponseException(
                    ApiResponseErrorCodes.OverdueTariffPayment,
                    "You have an outstanding balance of\n" +
                    $"{CurrencyFormattingHelper.AsRegular(monthlyCharge.TotalDeferredChargeAmount)} on your {userTariff.Name}\n" +
                    "Please pay your balance to\n" +
                    "switch the Plan");
            }
        }

        private async Task ThrowIfCheckCreditScorePaidServiceDisabled(CancellationToken cancellationToken)
        {
            if (await ServiceIsPurchased(cancellationToken))
            {
                return;
            }

            throw new ApiResponseException(ApiResponseErrorCodes.WrongTariffPlan, $"User doesn't have access to this feature");
        }

        private Task<bool> ServiceIsPurchased(CancellationToken cancellationToken)
        {
            return _creditScorePaidService.IsAlreadyPurchased(CurrentUser.UserId.Value, cancellationToken);
        }
    }
}
