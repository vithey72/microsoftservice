﻿namespace BNine.Application.Aggregates.Array.Queries
{
    using Abstractions;
    using AutoMapper;
    using BNine.Array.Aggregates.Models;
    using BNine.Array.Interfaces;
    using BNine.Settings;
    using Domain.Entities.User;
    using Interfaces;
    using Interfaces.Bank.Administration;
    using MediatR;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.Extensions.Options;

    public class CreateArrayUserQueryHandler
        : AbstractRequestHandler,
          IRequestHandler<CreateArrayUserQuery, bool>
    {
        private readonly IArrayUserRegistrationService _arrayUserRegistrationService;
        private readonly ArraySettings _arraySettings;
        private readonly IBankIdentityService _mBanqIdentityService;

        public CreateArrayUserQueryHandler(
            IMediator mediator,
            IBNineDbContext dbContext,
            IMapper mapper,
            ICurrentUserService currentUser,
            IArrayUserRegistrationService arrayUserRegistrationService,
            IBankIdentityService mBanqIdentityService,
            IOptions<ArraySettings> arraySettings)
            : base(mediator, dbContext, mapper, currentUser)
        {
            _mBanqIdentityService = mBanqIdentityService;
            _arrayUserRegistrationService = arrayUserRegistrationService;
            _arraySettings = arraySettings.Value;
        }

        public async Task<bool> Handle(CreateArrayUserQuery query, CancellationToken cancellationToken)
        {
            var user = await DbContext
                .Users
                .Include(u => u.Address)
                .Include(u => u.Identity)
                .Include(u => u.ArrayCustomerSettings)
                .FirstOrDefaultAsync(u => u.Id == CurrentUser.UserId, cancellationToken);

            if (user?.Address == null) { throw new ArgumentNullException(nameof(user.Address)); }
            if (user.DateOfBirth == null) { throw new ArgumentNullException(nameof(user.DateOfBirth)); }
            if (user.Identity?.Value == null) { throw new ArgumentNullException(nameof(user.Identity.Value)); }
            if (user.ExternalClientId == null) { throw new ArgumentNullException(nameof(user.ExternalClientId)); }

            if (user.ArrayCustomerSettings != null)
            {
                return false;
            }

            var request = new CreateArrayUserRequest
            {
                Address = new CreateArrayUserRequest.UserAddress
                {
                    City = user.Address.City,
                    State = user.Address.State,
                    Street = user.Address.AddressLine,
                    Zip = user.Address.ZipCode,
                },
                AppKey = _arraySettings.AppKey,
                DateOfBirth = user.DateOfBirth.Value,
                FirstName = user.FirstName,
                LastName = user.LastName,
                Ssn = user.Identity.Value,
                Email = user.Email,
                PhoneNumber = user.Phone.TrimStart('+'),
            };

            var response = await _arrayUserRegistrationService.CreateUser(request, cancellationToken);

            var now = DateTime.Now;
            user.ArrayCustomerSettings = new ArrayCustomerSettings
            {
                ArrayUserId = response.UserId,
                CreatedAt = now,
                UpdatedAt = now,
            };
            await DbContext.SaveChangesAsync(cancellationToken);

            return true;
        }
    }
}
