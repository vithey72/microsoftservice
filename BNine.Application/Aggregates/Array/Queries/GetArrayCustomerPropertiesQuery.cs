﻿namespace BNine.Application.Aggregates.Array.Queries;

using BNine.Array.Aggregates.Models;
using MediatR;

public record GetArrayCustomerPropertiesQuery : IRequest<GetArrayCustomerPropertiesResponse>;
