﻿namespace BNine.Application.Aggregates.ReferalProgramm.Models
{
    using System;
    using Newtonsoft.Json;

    public class BonusCodeModel
    {
        [JsonProperty("code")]
        public string Code
        {
            get; set;
        }

        [JsonProperty("senderId")]
        public Guid SenderId
        {
            get; set;
        }
    }
}
