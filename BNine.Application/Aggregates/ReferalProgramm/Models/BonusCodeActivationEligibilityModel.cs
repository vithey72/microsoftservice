﻿namespace BNine.Application.Aggregates.ReferalProgramm.Models
{
    using System;
    using BNine.Enums;
    using Newtonsoft.Json;

    public class BonusCodeActivationEligibilityModel
    {
        [JsonProperty("status")]
        public BonusCodeActivationEligibilityStatus Status
        {
            get; set;
        }

        [JsonProperty("isActivationEligibilityBlockEnabled")]
        public bool IsActivationEligibilityBlockEnabled
        {
            get; set;
        }

        /// <summary>
        /// Date associated with this status.
        /// </summary>
        [JsonProperty("statusDate")]
        public DateTime? StatusDate
        {
            get; set;
        }
    }
}
