﻿namespace BNine.Application.Aggregates.ReferalProgramm.Commands.DeleteReceivedBonusBlock
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading;
    using System.Threading.Tasks;
    using AutoMapper;
    using BNine.Application.Abstractions;
    using BNine.Application.Aggregates.ServiceBusEvents.Commands.PublishB9Event;
    using BNine.Application.Interfaces;
    using BNine.Constants;
    using Domain.Entities.User;
    using Exceptions;
    using MediatR;
    using Microsoft.EntityFrameworkCore;
    using Newtonsoft.Json;

    public class DeleteReceivedBonusBlockCommandHandler
        : AbstractRequestHandler
        , IRequestHandler<DeleteReceivedBonusBlockCommand, Unit>
    {
        public DeleteReceivedBonusBlockCommandHandler(
            IMediator mediator,
            IBNineDbContext dbContext,
            IMapper mapper,
            ICurrentUserService currentUser)
            : base(mediator, dbContext, mapper, currentUser)
        {
        }

        public async Task<Unit> Handle(DeleteReceivedBonusBlockCommand request, CancellationToken cancellationToken)
        {
            var user = await DbContext.Users
                .Where(x => x.Id == CurrentUser.UserId)
                .FirstOrDefaultAsync(cancellationToken);

            if (user == null)
            {
                throw new NotFoundException(nameof(User), CurrentUser.UserId);
            }

            user.Settings.IsReceivedBonusBlockEnabled = false;

            await DbContext.SaveChangesAsync(cancellationToken);

            await Mediator.Send(new PublishB9EventCommand(
                user,
                ServiceBusTopics.B9User,
                ServiceBusEvents.B9UserSettingsUpdated,
                new Dictionary<string, object>
                {
                    {
                        ServiceBusDefaults.UpdatedFieldsAdditionalProperty,
                        JsonConvert.SerializeObject(new List<string>
                        {
                            nameof(user.Settings.IsReceivedBonusBlockEnabled)
                        })
                    }
                }));

            return Unit.Value;
        }
    }
}
