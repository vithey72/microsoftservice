﻿namespace BNine.Application.Aggregates.ReferalProgramm.Commands.ActivateBonusCode
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading;
    using System.Threading.Tasks;
    using Abstractions;
    using Application.Models.MBanq;
    using Application.Models.MBanq.Transfers;
    using AutoMapper;
    using BNine.Application.Aggregates.ServiceBusEvents.Commands.PublishB9Event;
    using BNine.Application.Models.BonusCodes;
    using BNine.Common;
    using BNine.Constants;
    using BNine.Domain.Entities.BonusCodes;
    using BNine.Settings;
    using CheckingAccounts.Queries.GetTransactions;
    using Domain.Entities;
    using Domain.Entities.User;
    using Enums;
    using Enums.Transfers;
    using Exceptions;
    using Interfaces;
    using Interfaces.Bank.Client;
    using MediatR;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.Extensions.Options;
    using Newtonsoft.Json;

    public class ActivateBonusCodeCommandHandler
        : AbstractRequestHandler
        , IRequestHandler<ActivateBonusCodeCommand, Unit>
    {
        private const decimal BonusAmount = 5;
        private const decimal MinimumReplenishments = 10;
        private const decimal MaxSenderRewards = 200;
        private const decimal MaxSenderRewardsPoetryy = 500;
        private const decimal MaxSenderRewardsManaPacific = 1000;

        private readonly IBankTransfersService _bankTransfersService;
        private readonly BNineBonusAccountSettings _configuration;
        private readonly IPartnerProviderService _partnerProvider;

        public ActivateBonusCodeCommandHandler(
            IMediator mediator,
            IBNineDbContext dbContext,
            IMapper mapper,
            ICurrentUserService currentUser,
            IBankTransfersService bankTransfersService,
            IOptions<BNineBonusAccountSettings> configuration,
            IPartnerProviderService partnerProvider
            )
            : base(mediator, dbContext, mapper, currentUser)
        {
            _bankTransfersService = bankTransfersService;
            _configuration = configuration.Value;
            _partnerProvider = partnerProvider;
        }

        public async Task<Unit> Handle(ActivateBonusCodeCommand request, CancellationToken cancellationToken)
        {
            var actor = DbContext.Users
               .Where(x => x.Id == CurrentUser.UserId)
               .Select(x => new { x.CreatedAt })
               .FirstOrDefault();

            if (actor.CreatedAt < TimeZoneHelper.EasternStandartTimeToUtc(new DateTime(2021, 11, 26)))
            {
                throw new ValidationException(nameof(request), $"Your account is created earlier than November 26th, 2021");
            }

            var existingUserActivationRecord = await DbContext.BonusCodeActivations
                .Include(bca => bca.BonusCode)
                .FirstOrDefaultAsync(
                    bca => bca.ReceiverId == CurrentUser.UserId,
                    cancellationToken);

            if (existingUserActivationRecord != null)
            {
                throw new ValidationException("BonusCodeActivation", "The code is already activated");
            }

            await TryActivateBonusCode(request, cancellationToken);

            return Unit.Value;
        }

        private async Task TryActivateBonusCode(ActivateBonusCodeCommand request, CancellationToken cancellationToken)
        {
            var userReceiver = GetUserWithAccount(CurrentUser.UserId);

            var applicableBonusCode = await DbContext.BonusCodes
                .Include(bc => bc.Activations)
                .Where(
                    bc =>
                        bc.Code == request.BonusCode &&
                        bc.Activations.All(act => act.ReceiverId != userReceiver.Id) &&
                        bc.SenderId != userReceiver.Id)
                .FirstOrDefaultAsync(cancellationToken);

            if (applicableBonusCode == null)
            {
                throw new NotFoundException(nameof(BonusCode), request.BonusCode);
            }

            var receiverTransactions = await Mediator.Send(new GetTransactionsQuery()
            {
                Skip = 0,
                Take = 100,
                IpAddress = request.Ip,
                MbanqAccessToken = CurrentUser.UserMbanqToken,
            }, cancellationToken);

            var relevantTransactionTypes = new[] {
                TransactionTypes.CARD_AUTHORIZE_PAYMENT,
                TransactionTypes.EXTERNAL_CARD,
                TransactionTypes.ACH,
                TransactionTypes.CASH,
                TransactionTypes.TOP_UP_WALLET,
            };
            var relevantTransactions = receiverTransactions.Items?
                .Where(t => t.Direction == TransferDirection.Credit &&
                            relevantTransactionTypes.Contains(t.Type) &&
                            t.Succeeded &&
                            t.Date > applicableBonusCode.CreationDate
                            )
                .ToArray();

            if (relevantTransactions.Sum(x => x.Amount) < MinimumReplenishments)
            {
                throw new BadRequestException("Not enough replenishments");
            }

            var codeSender = GetUserWithAccount(applicableBonusCode.SenderId);

            applicableBonusCode.Activations.Add(
                new BonusCodeActivation
                {
                    BonusCodeId = applicableBonusCode.Id,
                    ReceiverId = userReceiver.Id,
                    CreatedAt = DateTime.UtcNow,
                });
            await DbContext.SaveChangesAsync(cancellationToken);

            var currentActivation = applicableBonusCode.Activations
                .Where(bca => bca.ReceiverId == userReceiver.Id).Single();

            await PerformReceiverTransfer(request, userReceiver, currentActivation, cancellationToken);

            var maxRewards = _partnerProvider.GetPartner() switch
            {
                PartnerApp.Poetryy => MaxSenderRewardsPoetryy,
                PartnerApp.ManaPacific => MaxSenderRewardsManaPacific,
                _ => MaxSenderRewards,
            };
            var isSenderEligibleForBonus = DbContext.BonusCodes
               .Where(bc => bc.SenderId == applicableBonusCode.SenderId)
               .SelectMany(bc => bc.Activations)
               .Count() * BonusAmount <= maxRewards;

            if (isSenderEligibleForBonus)
            {
                await PerformSenderTransfer(request, codeSender, currentActivation, cancellationToken);
            }

            var currentActivationModel = Mapper.Map<BonusCodeActivationModel>(currentActivation);

            await Mediator.Send(new PublishB9EventCommand(
              currentActivationModel,
              ServiceBusTopics.B9BonusCodeActivations,
              ServiceBusEvents.B9Created
              ));
        }

        private User GetUserWithAccount(Guid? userId)
        {
            var user = DbContext.Users
                .Include(x => x.CurrentAccount)
                .FirstOrDefault(x => x.Id == userId);

            if (user == null)
            {
                throw new NotFoundException(nameof(User), userId);
            }

            return user;
        }

        private async Task PerformSenderTransfer(ActivateBonusCodeCommand request, User codeSender, BonusCodeActivation currentActivation, CancellationToken cancellationToken)
        {
            var senderTransfer = CreateBonusTransfer(codeSender, codeSender.CurrentAccount.ExternalId);
            await SubmitTransfer(request.Ip, codeSender, senderTransfer);
            currentActivation.SenderTransferSuccess = true;
            currentActivation.UpdatedAt = DateTime.UtcNow;
            codeSender.Settings.IsReceivedBonusBlockEnabled = true;

            await DbContext.SaveChangesAsync(cancellationToken);

            await Mediator.Send(new PublishB9EventCommand(
              codeSender,
              ServiceBusTopics.B9User,
              ServiceBusEvents.B9UserSettingsUpdated,
              new Dictionary<string, object>
              {
                  { ServiceBusDefaults.UpdatedFieldsAdditionalProperty , JsonConvert.SerializeObject(new List<string> { nameof(codeSender.Settings.IsReceivedBonusBlockEnabled) }) }
              }));
        }

        private async Task PerformReceiverTransfer(ActivateBonusCodeCommand request, User userReceiver, BonusCodeActivation currentActivation, CancellationToken cancellationToken)
        {
            var recieverTransfer = CreateBonusTransfer(userReceiver, userReceiver.CurrentAccount.ExternalId);
            await SubmitTransfer(request.Ip, userReceiver, recieverTransfer);
            currentActivation.ReceiverTransferSuccess = true;
            currentActivation.UpdatedAt = DateTime.UtcNow;
            userReceiver.Settings.IsReceivedBonusBlockEnabled = true;

            await DbContext.SaveChangesAsync(cancellationToken);
        }

        private Transfer CreateBonusTransfer(User user, int savingAccountId)
        {
            return new Transfer
            {
                Type = TransferDirection.Credit,
                PaymentType = TransferType.Internal,
                Amount = BonusAmount,
                Reference = "Refferal Incentive",
                Debtor = new Counterparty
                {
                    Identifier = $"ID:{_configuration.SavingsAccountId}"
                },
                Creditor = new Counterparty
                {
                    Identifier = $"ID:{savingAccountId}",
                    Name = $"{user.FirstName} {user.LastName}"
                }
            };
        }

        private async Task SubmitTransfer(string ip, User user, Transfer mBanqTransfer)
        {
            await _bankTransfersService.CreateAndSubmitTransfer("referalBonus", ip, mBanqTransfer);
            user.Settings.IsReceivedBonusBlockEnabled = true;
        }
    }
}
