﻿namespace BNine.Application.Aggregates.ReferalProgramm.Commands.ActivateBonusCode
{
    using System;
    using BNine.Application.Abstractions;
    using MediatR;
    using Newtonsoft.Json;

    public class ActivateBonusCodeCommand : MBanqSelfServiceUserRequest, IRequest<Unit>
    {
        [JsonIgnore]
        public Guid UserId
        {
            get; set;
        }

        [JsonProperty("bonusCode")]
        public string BonusCode
        {
            get; set;
        }
    }
}
