﻿namespace BNine.Application.Aggregates.ReferalProgramm.Commands.ActivateBonusCode
{
    using System.Linq;
    using System.Text.RegularExpressions;
    using FluentValidation;
    using Interfaces;
    using Microsoft.Extensions.Localization;
    using ResourceLibrary;

    public class ActivateBonusCodeCommandValidator : AbstractValidator<ActivateBonusCodeCommand>
    {
        private IBNineDbContext DbContext
        {
            get;
        }

        private IStringLocalizer<SharedResource> SharedLocalizer
        {
            get;
        }

        public ActivateBonusCodeCommandValidator(
            IBNineDbContext dbContext,
            IStringLocalizer<SharedResource> sharedLocalizer
            )
        {
            DbContext = dbContext;

            SharedLocalizer = sharedLocalizer;

            RuleFor(x => x.BonusCode)
                .NotEmpty()
                .Must(IsValidBonusCode)
                .WithMessage(SharedLocalizer["IncorrectEnteredDataValidationErrorMessage"].Value)
                ;

            RuleFor(x => x)
                .Must(IsCodeExists)
                .WithMessage(SharedLocalizer["invalidCodeValidationErrorMessage"].Value)
                ;
        }

        private bool IsValidBonusCode(string bonusCode)
        {
            return bonusCode != null && Regex.IsMatch(bonusCode, @"^[0-9]{6}$");
        }

        private bool IsCodeExists(ActivateBonusCodeCommand command)
        {
            return DbContext.BonusCodes
                .Where(x => x.SenderId != command.UserId)
                .Any(x => x.Code == command.BonusCode);
        }
    }
}
