﻿namespace BNine.Application.Aggregates.ReferalProgramm.Queries.GetBonusCodeActivationEligibility;

using Abstractions;
using BNine.Application.Aggregates.ReferalProgramm.Models;
using MediatR;

public class GetBonusCodeActivationEligibilityQuery : MBanqSelfServiceUserRequest, IRequest<BonusCodeActivationEligibilityModel>
{
}
