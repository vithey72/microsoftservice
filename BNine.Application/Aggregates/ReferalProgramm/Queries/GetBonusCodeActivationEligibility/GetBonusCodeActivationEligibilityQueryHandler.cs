﻿namespace BNine.Application.Aggregates.ReferalProgramm.Queries.GetBonusCode
{
    using System;
    using System.Linq;
    using System.Threading;
    using System.Threading.Tasks;
    using Abstractions;
    using AutoMapper;
    using BNine.Application.Aggregates.ReferalProgramm.Queries.GetBonusCodeActivationEligibility;
    using BNine.Common;
    using BNine.Enums;
    using BNine.Enums.Transfers;
    using CheckingAccounts.Queries.GetTransactions;
    using Constants;
    using Domain.Entities.User;
    using Exceptions;
    using Interfaces;
    using MediatR;
    using Microsoft.EntityFrameworkCore;
    using Models;

    public class GetBonusCodeActivationEligibilityQueryHandler
        : AbstractRequestHandler
        , IRequestHandler<GetBonusCodeActivationEligibilityQuery, BonusCodeActivationEligibilityModel>
    {
        private const decimal MinimumReplenishments = 10;

        public GetBonusCodeActivationEligibilityQueryHandler(
            IMediator mediator,
            IBNineDbContext dbContext,
            IMapper mapper,
            ICurrentUserService currentUser)
            : base(mediator, dbContext, mapper, currentUser)
        {
        }

        public async Task<BonusCodeActivationEligibilityModel> Handle(GetBonusCodeActivationEligibilityQuery request, CancellationToken token)
        {
            var user = DbContext.Users.FirstOrDefault(x => x.Id == CurrentUser.UserId);
            if (user == null)
            {
                throw new NotFoundException(nameof(User), CurrentUser.UserId);
            }

            if (user.CreatedAt < TimeZoneHelper.EasternStandartTimeToUtc(new DateTime(2021, 11, 26)))
            {
                return new BonusCodeActivationEligibilityModel { Status = BonusCodeActivationEligibilityStatus.OldRegistration };
            }

            // a user can activate a bonus code only once
            var existingReceivedCode = await DbContext
                .BonusCodeActivations
                .FirstOrDefaultAsync(bca => bca.ReceiverId == user.Id, token);

            if (existingReceivedCode != null)
            {
                return new BonusCodeActivationEligibilityModel
                {
                    Status = BonusCodeActivationEligibilityStatus.AlreadyActivated,
                    StatusDate = existingReceivedCode.UpdatedAt,
                };
            }

            var receiverTransactions = await Mediator.Send(new GetTransactionsQuery()
            {
                Skip = 0,
                Take = 100,
                IpAddress = request.Ip,
                MbanqAccessToken = CurrentUser.UserMbanqToken,
            }, token);

            var relevantTransactionTypes = new[] {
                TransactionTypes.CARD_AUTHORIZE_PAYMENT,
                TransactionTypes.EXTERNAL_CARD,
                TransactionTypes.ACH,
                TransactionTypes.CASH,
                TransactionTypes.TOP_UP_WALLET,
            };
            var relevantTransactions = receiverTransactions.Items?
                .Where(t => t.Direction == TransferDirection.Credit &&
                            relevantTransactionTypes.Contains(t.Type) &&
                            t.Succeeded
                            )
                .ToArray();

            if (relevantTransactions.Sum(x => x.Amount) < MinimumReplenishments)
            {
                return new BonusCodeActivationEligibilityModel
                {
                    Status = BonusCodeActivationEligibilityStatus.InsufficientDeposit,
                };
            }

            var isActivationEligibilityBlockVisible =
                relevantTransactions?
                    .Where(x => x.Date > DateTime.Now.AddDays(-14))
                    .Sum(x => x.Amount) >= MinimumReplenishments;

            return new BonusCodeActivationEligibilityModel
            {
                Status = BonusCodeActivationEligibilityStatus.Ok,
                IsActivationEligibilityBlockEnabled = user.Settings.IsActivationEligibilityBlockEnabled && isActivationEligibilityBlockVisible
            };
        }
    }
}
