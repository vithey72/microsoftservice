﻿namespace BNine.Application.Aggregates.ReferalProgramm.Queries.GetBonusCode
{
    using System.Linq;
    using System.Threading;
    using System.Threading.Tasks;
    using Abstractions;
    using AutoMapper;
    using BNine.Application.Aggregates.ServiceBusEvents.Commands.PublishB9Event;
    using BNine.Constants;
    using Common;
    using Domain.Entities;
    using Domain.Entities.User;
    using Exceptions;
    using Interfaces;
    using MediatR;
    using Microsoft.EntityFrameworkCore;
    using Models;

    public class GetBonusCodeQueryHandler
        : AbstractRequestHandler
        , IRequestHandler<GetBonusCodeQuery, BonusCodeModel>
    {
        public GetBonusCodeQueryHandler(
            IMediator mediator,
            IBNineDbContext dbContext,
            IMapper mapper,
            ICurrentUserService currentUser)
            : base(mediator, dbContext, mapper, currentUser)
        {
        }

        public async Task<BonusCodeModel> Handle(GetBonusCodeQuery request, CancellationToken cancellationToken)
        {
            var user = DbContext.Users.FirstOrDefault(x => x.Id == CurrentUser.UserId);
            if (user == null)
            {
                throw new NotFoundException(nameof(User), CurrentUser.UserId);
            }

            // generate 10 codes, use first unused
            var randCodes = Enumerable.Range(0, 10).Select(i => CodeGenerator.GenerateCode()).ToArray();
            var existingCodes = await DbContext.BonusCodes.Where(x => randCodes.Contains(x.Code)).Select(x => x.Code).ToListAsync();

            var bonusCode = new BonusCode
            {
                Code = randCodes.Except(existingCodes).FirstOrDefault() ?? existingCodes.First(),
                SenderId = user.Id,
            };

            await DbContext.BonusCodes.AddAsync(bonusCode);
            await DbContext.SaveChangesAsync(cancellationToken);

            await Mediator.Send(new PublishB9EventCommand(
               bonusCode,
               ServiceBusTopics.B9BonusCodes,
               ServiceBusEvents.B9Created));

            return new BonusCodeModel
            {
                Code = bonusCode.Code,
                SenderId = bonusCode.SenderId
            };
        }
    }
}
