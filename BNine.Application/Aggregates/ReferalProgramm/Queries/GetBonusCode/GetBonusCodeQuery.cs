﻿namespace BNine.Application.Aggregates.ReferalProgramm.Queries.GetBonusCode
{
    using BNine.Application.Aggregates.ReferalProgramm.Models;
    using MediatR;

    public class GetBonusCodeQuery : IRequest<BonusCodeModel>
    {

    }
}
