﻿namespace BNine.Application.Aggregates.ReferalProgramm.Queries.GetReceivedBonusBlockStatus
{
    using MediatR;

    public class GetReceivedBonusBlockStatusQuery : IRequest<bool>
    {
    }
}
