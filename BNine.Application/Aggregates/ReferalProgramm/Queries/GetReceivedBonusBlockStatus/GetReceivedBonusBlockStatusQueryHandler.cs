﻿namespace BNine.Application.Aggregates.ReferalProgramm.Queries.GetReceivedBonusBlockStatus
{
    using System.Linq;
    using System.Threading;
    using System.Threading.Tasks;
    using AutoMapper;
    using BNine.Application.Abstractions;
    using BNine.Application.Interfaces;
    using Domain.Entities.User;
    using Exceptions;
    using MediatR;

    public class GetReceivedBonusBlockStatusQueryHandler
        : AbstractRequestHandler
        , IRequestHandler<GetReceivedBonusBlockStatusQuery, bool>
    {
        public GetReceivedBonusBlockStatusQueryHandler(
            IMediator mediator,
            IBNineDbContext dbContext,
            IMapper mapper,
            ICurrentUserService currentUser)
            : base(mediator, dbContext, mapper, currentUser)
        {
        }

        public Task<bool> Handle(GetReceivedBonusBlockStatusQuery request, CancellationToken cancellationToken)
        {
            var user = DbContext.Users.FirstOrDefault(x => x.Id == CurrentUser.UserId);
            if (user == null)
            {
                throw new NotFoundException(nameof(User), CurrentUser.UserId);
            }

            return Task.FromResult(user.Settings.IsReceivedBonusBlockEnabled);
        }
    }
}
