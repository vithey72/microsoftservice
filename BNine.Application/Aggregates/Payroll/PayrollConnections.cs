﻿namespace BNine.Application.Aggregates.Payroll;

public record PayrollConnections(int TotalCount, int ExternalCount, Guid[] InternalDestinationIds) : IPayrollConnections;
