﻿namespace BNine.Application.Aggregates.Payroll;

using BNine.Application.Interfaces;
using BNine.Domain.Entities.User;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;

public class PayrollConnectionsProvider : IPayrollConnectionsProvider
{
    private readonly IBNineDbContext _dbContext;

    public PayrollConnectionsProvider(
        IBNineDbContext dbContext
        )
    {
        _dbContext = dbContext;
    }

    public async Task<IPayrollConnections> GetPayrollConnections(Guid userId, CancellationToken cancellationToken)
    {
        var userStats = await GetUserStats(userId, cancellationToken);

        if (userStats is null)
        {
            return new PayrollConnections(0, 0, new Guid[0]);
        }

        var internalDestinationIds = System.Array.Empty<Guid>();
        if (userStats.ActiveInternalPayAllocationIds is not null)
        {
            var deserialized = JsonConvert.DeserializeObject<Guid[]>(userStats.ActiveInternalPayAllocationIds);
            if (deserialized is not null)
            {
                internalDestinationIds = deserialized;
            }
        }

        return new PayrollConnections(
            userStats.ActivePayAllocationCount,
            userStats.ActiveExternalPayAllocationCount,
            internalDestinationIds
            );
    }

    private Task<UserSyncedStats?> GetUserStats(Guid userId, CancellationToken cancellationToken)
    {
        return _dbContext.Users
            .Where(x => x.Id == userId)
            .Select(x => x.UserSyncedStats).AsNoTracking()
            .FirstOrDefaultAsync(cancellationToken);
    }
}
