﻿namespace BNine.Application.Aggregates.Payroll;

public interface IPayrollConnectionsProvider
{
    Task<IPayrollConnections> GetPayrollConnections(Guid userId, CancellationToken cancellationToken);
}
