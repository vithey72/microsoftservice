﻿namespace BNine.Application.Aggregates.Payroll;

public interface IPayrollConnections
{
    int TotalCount
    {
        get;
    }

    int ExternalCount
    {
        get;
    }

    Guid[] InternalDestinationIds
    {
        get;
    }
}
