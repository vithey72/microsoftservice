﻿namespace BNine.Application.Aggregates.ServiceBusEvents.Commands.PublishB9Event
{
    using System;
    using System.Linq;
    using System.Reflection;
    using System.Threading;
    using System.Threading.Tasks;
    using AutoMapper;
    using BNine.Application.Abstractions;
    using BNine.Application.Interfaces;
    using BNine.Application.Interfaces.ServiceBus;
    using BNine.Domain.Interfaces;
    using MediatR;
    using Newtonsoft.Json;
    using Newtonsoft.Json.Serialization;

    public class PublishB9EntityUpdateCommandHandler
        : AbstractRequestHandler, IRequestHandler<PublishB9EntityUpdateCommand, Unit>
    {
        private IServiceBus ServiceBus
        {
            get;
        }

        public PublishB9EntityUpdateCommandHandler(
            IServiceBus serviceBus,
            IMediator mediator,
            IBNineDbContext dbContext,
            IMapper mapper,
            ICurrentUserService currentUser
            ) : base(mediator, dbContext, mapper, currentUser)
        {
            ServiceBus = serviceBus;
        }

        public async Task<Unit> Handle(PublishB9EntityUpdateCommand request, CancellationToken cancellationToken)
        {
            var x = JsonConvert.SerializeObject(request.Data, new JsonSerializerSettings
            {
                MaxDepth = 1,
                ReferenceLoopHandling = ReferenceLoopHandling.Ignore,
                ContractResolver = ShouldSerializeContractResolver.Instance
            });
            await ServiceBus.Publish(new Application.Models.ServiceBus.IntegrationEvent
            {
                Body = JsonConvert.SerializeObject(request.Data, new JsonSerializerSettings
                {
                    MaxDepth = 1,
                    ReferenceLoopHandling = ReferenceLoopHandling.Ignore,
                    ContractResolver = ShouldSerializeContractResolver.Instance
                }),
                EventName = request.Event,
                TopicName = request.Topic,
                ApplicationProperties = request.ApplicationProperties,
            });

            return Unit.Value;
        }

        internal class ShouldSerializeContractResolver : DefaultContractResolver
        {
            public static readonly ShouldSerializeContractResolver Instance = new ShouldSerializeContractResolver();

            private static readonly Type[] BaseInterfaces = new Type[] { typeof(IBaseEntity), typeof(IHistoryEntry) };

            protected override JsonProperty CreateProperty(MemberInfo member, MemberSerialization memberSerialization)
            {
                JsonProperty property = base.CreateProperty(member, memberSerialization);

                if (property.PropertyType.GetInterfaces().Intersect(BaseInterfaces).Any())
                {
                    property.ShouldSerialize = No;
                }

                return property;
            }

            private static bool No(object obj)
            {
                return false;
            }
        }
    }
}
