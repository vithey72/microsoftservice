﻿namespace BNine.Application.Aggregates.ServiceBusEvents.Commands.PublishB9Event
{
    using System.Collections.Generic;
    using MediatR;

    public class PublishB9EventCommand : IRequest<Unit>
    {
        public PublishB9EventCommand(object data, string topic, string @event, IDictionary<string, object> applicationProperties = null)
        {
            Data = data;
            Topic = topic;
            Event = @event;

            if (applicationProperties != null)
            {
                ApplicationProperties = applicationProperties;
            }
        }

        public object Data
        {
            get;
        }

        public string Topic
        {
            get;
        }

        public string Event
        {
            get;
        }

        public IDictionary<string, object> ApplicationProperties
        {
            get;
        } = new Dictionary<string, object>();
    }
}
