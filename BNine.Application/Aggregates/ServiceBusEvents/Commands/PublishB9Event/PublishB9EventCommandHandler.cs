﻿namespace BNine.Application.Aggregates.ServiceBusEvents.Commands.PublishB9Event
{
    using System.Threading;
    using System.Threading.Tasks;
    using AutoMapper;
    using BNine.Application.Abstractions;
    using BNine.Application.Interfaces;
    using BNine.Application.Interfaces.ServiceBus;
    using MediatR;
    using Newtonsoft.Json;

    public class PublishB9EventCommandHandler
        : AbstractRequestHandler, IRequestHandler<PublishB9EventCommand, Unit>
    {
        private IServiceBus ServiceBus
        {
            get;
        }

        public PublishB9EventCommandHandler(
            IServiceBus serviceBus,
            IMediator mediator,
            IBNineDbContext dbContext,
            IMapper mapper,
            ICurrentUserService currentUser
            ) : base(mediator, dbContext, mapper, currentUser)
        {
            ServiceBus = serviceBus;
        }

        public async Task<Unit> Handle(PublishB9EventCommand request, CancellationToken cancellationToken)
        {
            await ServiceBus.Publish(new Application.Models.ServiceBus.IntegrationEvent
            {
                Body = JsonConvert.SerializeObject(request.Data, new JsonSerializerSettings
                {
                    MaxDepth = 1,
                    ReferenceLoopHandling = ReferenceLoopHandling.Ignore
                }),
                EventName = request.Event,
                TopicName = request.Topic,
                ApplicationProperties = request.ApplicationProperties
            });

            return Unit.Value;
        }
    }
}
