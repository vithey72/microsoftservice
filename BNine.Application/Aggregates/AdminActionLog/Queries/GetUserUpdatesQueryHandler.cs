﻿namespace BNine.Application.Aggregates.LogAdminAction.Queries;

using Abstractions;
using AutoMapper;
using Domain.Entities.User;
using Enums;
using Extensions;
using Interfaces;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Models.AdminActionLog;
using Newtonsoft.Json;

public class GetUserUpdatesQueryHandler : AbstractRequestHandler, IRequestHandler<GetUserUpdatesQuery, List<UserUpdate>>
{
    public GetUserUpdatesQueryHandler(IMediator mediator, IBNineDbContext dbContext, IMapper mapper,
        ICurrentUserService currentUser) : base(mediator, dbContext, mapper, currentUser)
    {
    }

    public async Task<List<UserUpdate>> Handle(GetUserUpdatesQuery request, CancellationToken cancellationToken)
    {
        //TODO: Add index for EntityName and EntityId
        var adminActionLogEntries = await DbContext.AdminActionLogEntry
            .Where(x =>
                x.EntityName == "User" &&
                x.EntityId == request.ClientId.ToString())
            .OrderByDescending(x => x.UpdateAt)
            .ToListAsync(cancellationToken);

        var userBlockReasons = await DbContext.UserBlockReasons.ToListAsync(cancellationToken);

        return adminActionLogEntries.Select(logEntry =>
        {
            var userUpdate = new UserUpdate()
            {
                Id = logEntry.Id,
                Type = logEntry.Type.GetEnumDescriptionValue(),
                UpdateAt = logEntry.UpdateAt,
                UpdateBy = logEntry.UpdateByUserName
            };

            var beforeChangesUserEntity = JsonConvert.DeserializeObject<User>(logEntry.BeforeChanges);
            var afterChangesUserEntity = JsonConvert.DeserializeObject<User>(logEntry.AfterChanges);

            switch (logEntry.Type)
            {
                case AdminActionType.ChangeName:
                    userUpdate.UpdateFrom = $"{beforeChangesUserEntity.FirstName} {beforeChangesUserEntity.LastName}";
                    userUpdate.UpdateTo = $"{afterChangesUserEntity.FirstName} {afterChangesUserEntity.LastName}";
                    break;

                case AdminActionType.ChangeEmail:
                    userUpdate.UpdateFrom = beforeChangesUserEntity.Email;
                    userUpdate.UpdateTo = afterChangesUserEntity.Email;
                    break;

                case AdminActionType.ChangePhoneNumber:
                    userUpdate.UpdateFrom = beforeChangesUserEntity.Phone;
                    userUpdate.UpdateTo = afterChangesUserEntity.Phone;
                    break;

                case AdminActionType.ChangeAddress:
                    userUpdate.UpdateFrom = $@"
                            {beforeChangesUserEntity.Address.AddressLine},
                            {beforeChangesUserEntity.Address.Unit},
                            {beforeChangesUserEntity.Address.City},
                            {beforeChangesUserEntity.Address.State},
                            {beforeChangesUserEntity.Address.ZipCode}";
                    userUpdate.UpdateTo = $@"
                            {afterChangesUserEntity.Address.AddressLine},
                            {afterChangesUserEntity.Address.Unit},
                            {afterChangesUserEntity.Address.City},
                            {afterChangesUserEntity.Address.State},
                            {afterChangesUserEntity.Address.ZipCode}";
                    break;

                case AdminActionType.BlockClient:
                    var userBlockReason =
                        userBlockReasons.FirstOrDefault(x => x.Id == afterChangesUserEntity.UserBlockReasonId);
                    userUpdate.Notes = userBlockReason.Value;
                    break;

                case AdminActionType.CloseAccount:
                    var closeReason =
                        userBlockReasons.FirstOrDefault(x => x.Id == afterChangesUserEntity.UserCloseReasonId);
                    userUpdate.Notes = closeReason != null ? closeReason.Value : "Unknown";
                    break;

                case AdminActionType.UnblockClient:
                case AdminActionType.DeleteArgyleData:
                case AdminActionType.BlockAdvance:
                    break;

                default:
                    throw new Exception($"Admin Action type: {logEntry.Type} is not supported");
            }

            return userUpdate;
        }).ToList();
    }
}
