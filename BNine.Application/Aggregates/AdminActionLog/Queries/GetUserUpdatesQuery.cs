﻿namespace BNine.Application.Aggregates.LogAdminAction.Queries;

using MediatR;
using Models.AdminActionLog;

public class GetUserUpdatesQuery: IRequest<List<UserUpdate>>
{
    public Guid ClientId
    {
        get;
        set;
    }
}
