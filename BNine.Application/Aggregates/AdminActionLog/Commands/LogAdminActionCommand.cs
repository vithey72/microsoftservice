﻿using MediatR;

namespace BNine.Application.Aggregates.LogAdminAction.Commands;

public class LogAdminActionCommand : IRequest<Unit>
{
    /// <summary>
    /// Change type according to the Dwh Integration
    /// </summary>
    public Enums.AdminActionType Type
    {
        get; set;
    }

    /// <summary>
    /// Sql table name, corresponds to the domain entity name
    /// </summary>
    public string EntityName
    {
        get; set;
    }

    /// <summary>
    /// Sql row id
    /// </summary>
    public string EntityId
    {
        get; set;
    }

    /// <summary>
    /// Notes with reason to change
    /// </summary>
    public string Notes
    {
        get; set;
    }

    /// <summary>
    /// Serializaed entity json
    /// </summary>
    public string BeforeChanges
    {
        get;
        set;
    }

    /// <summary>
    /// Serializaed entity json
    /// </summary>
    public string AfterChanges
    {
        get;
        set;
    }
}
