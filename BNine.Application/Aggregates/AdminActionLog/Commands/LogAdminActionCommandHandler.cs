﻿using AutoMapper;
using BNine.Application.Abstractions;
using BNine.Application.Interfaces;
using BNine.Application.Interfaces.DwhIntegration;
using BNine.Application.Services.AdminAPI;
using MediatR;

namespace BNine.Application.Aggregates.LogAdminAction.Commands;

using Newtonsoft.Json;

public class LogAdminActionCommandHandler
     : AbstractRequestHandler, IRequestHandler<LogAdminActionCommand, Unit>
{
    private static readonly Guid ApiAdminGuid = new("11111111-1111-1111-1111-111111111111");
    private static readonly string ApiAdminName = "ApiKeyAuth";

    private IDwhIntegrationService _dwhIntegrationService;
    private IAdminApiCurrentUserService _currentUserService;

    public LogAdminActionCommandHandler(
        IMediator mediator,
        IBNineDbContext dbContext,
        IMapper mapper,
        IAdminApiCurrentUserService currentUser,
        IDwhIntegrationService dwhIntegrationService)
        : base(mediator, dbContext, mapper, currentUser)
    {
        _dwhIntegrationService = dwhIntegrationService;
        _currentUserService = currentUser;
    }

    public async Task<Unit> Handle(
        LogAdminActionCommand request,
        CancellationToken cancellationToken)
    {
        var (userIdString, userName) = ExtractCurrentUserIdentity();

        var domainLogEntry = new BNine.Domain.Entities.AdminActionLogEntry.AdminActionLogEntry()
        {
            Type = request.Type,
            Notes = request.Notes,
            UpdateAt = DateTime.UtcNow,
            UpdateByUserId = userIdString,
            UpdateByUserName = userName,
            EntityId = request.EntityId,
            EntityName = request.EntityName,
            BeforeChanges = request.BeforeChanges,
            AfterChanges = request.AfterChanges
        };

        await DbContext.AdminActionLogEntry.AddAsync(domainLogEntry);

        await DbContext.SaveChangesAsync(cancellationToken);

        var logEntry = new BNine.Application.Models.DwhIntegration.LogAdminAction.AdminActionLogEntry()
        {
            Type = request.Type,
            Notes = request.Notes,
            UpdateAt = DateTime.UtcNow,
            UpdateByUserId = userIdString,
            UpdateByUserName = userName,
            BeforeChanges = JsonConvert.DeserializeObject(request.BeforeChanges),
            AfterChanges = JsonConvert.DeserializeObject(request.AfterChanges),
            EntityId = request.EntityId,
            EntityName = request.EntityName,
            Id = domainLogEntry.Id
        };

        await _dwhIntegrationService.LogAdminAction(logEntry);

        return Unit.Value;
    }

    private (string userId, string userName) ExtractCurrentUserIdentity()
    {
        if (!_currentUserService.UsesApiKeyAuth)
        {
            return (_currentUserService.UserId.ToString(), _currentUserService.UserName);
        }

        return (ApiAdminGuid.ToString(), ApiAdminName);
    }
}
