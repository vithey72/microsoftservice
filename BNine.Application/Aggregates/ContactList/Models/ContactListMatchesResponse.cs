﻿#nullable enable
namespace BNine.Application.Aggregates.ContactList.Models;

public class ContactListMatchesResponse
{
    public List<string> Matches
    {
        get;
        set;
    } = new List<string>();

    public string? Comment
    {
        get;
        set;
    }
}
