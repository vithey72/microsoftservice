﻿namespace BNine.Application.Aggregates.ContactList;

using System.Text.RegularExpressions;
using Enums;
using Exceptions;
using Interfaces;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Models;

public class MatchUserContactListQueryHandler : IRequestHandler<MatchUserContactListQuery, ContactListMatchesResponse>
{
    private readonly IBNineDbContext _dbContext;
    private readonly Dictionary<PhoneCountryFormatEnum, Regex> _regexPatterns = new()
    {
        {PhoneCountryFormatEnum.US, new(@"^\+1\d{10}$")}
    };

    public MatchUserContactListQueryHandler(IBNineDbContext dbContext)
    {
        _dbContext = dbContext;
    }


    public async Task<ContactListMatchesResponse> Handle(MatchUserContactListQuery request, CancellationToken cancellationToken)
    {


        var currentUser = await _dbContext.Users.Include(u => u.Settings)
            .FirstOrDefaultAsync(u => u.Id == request.UserId, cancellationToken);

        if (currentUser == null)
        {
            throw new NotFoundException($"User with id {request.UserId} not found");
        }

        var regexPatternsToUse = request.Countries
            .Select(GetCountryRegex)
            .Distinct()
            .ToList();

        var numbersToMatchFiltered = request.PhoneNumbersToMatch.Select(CleanPhoneNumber)
            .Where(p => MatchWithRegexPatterns(p, regexPatternsToUse) && p != currentUser.Phone).Distinct().ToList();


        if (numbersToMatchFiltered.Count == 0)
        {
            return new ContactListMatchesResponse()
            {
                Comment = "No valid phone numbers to match where sent in the request"
            };
        }

        var matches = await _dbContext.Users
            .Where(u => numbersToMatchFiltered.Contains(u.Phone) && u.Status == UserStatus.Active)
            .Select(u => u.Phone)
            .ToListAsync(cancellationToken);

        currentUser.Settings.LastCheckMatchesFromContactListCount = matches.Count;
        await _dbContext.SaveChangesAsync(cancellationToken);

        return new ContactListMatchesResponse
        {
            Matches = matches
        };
    }

    private Regex GetCountryRegex(PhoneCountryFormatEnum countryFormat) =>
        _regexPatterns.TryGetValue(countryFormat, out var regex) ? regex : _regexPatterns[PhoneCountryFormatEnum.US];

    private static bool MatchWithRegexPatterns(string phoneNumber, List<Regex> regexPatterns)
    {
        return regexPatterns.Any(regex => regex.IsMatch(phoneNumber));
    }

    private static string CleanPhoneNumber(string phoneNumber)
    {
        return phoneNumber
            .Replace("-", "")
            .Replace("(", "")
            .Replace(")", "")
            .Replace(" ", "")
            .Replace(".", "");
    }
}
