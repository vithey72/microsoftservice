﻿namespace BNine.Application.Aggregates.ContactList;

using MediatR;
using Models;
using Newtonsoft.Json;

public class MatchUserContactListQuery : IRequest<ContactListMatchesResponse>
{

    [JsonIgnore]
    public Guid UserId
    {
        get;
        set;
    }
    public List<string> PhoneNumbersToMatch
    {
        get;
        set;
    } = new List<string>();

    public List<PhoneCountryFormatEnum> Countries
    {
        get;
        set;
    } = new (){ PhoneCountryFormatEnum.US };
};
