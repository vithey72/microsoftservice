﻿namespace BNine.Application.Aggregates.Plaid.Queries.GetProfileBankAccounts
{
    using System.Collections.Generic;
    using BNine.Application.Aggregates.Plaid.Models;
    using MediatR;

    public class GetProfileBankAccountsQuery : IRequest<List<ProfileBankAccount>>
    {
    }
}
