﻿namespace BNine.Application.Aggregates.Plaid.Queries.GetProfileBankAccounts
{
    using System.Collections.Generic;
    using System.Threading;
    using System.Threading.Tasks;
    using AutoMapper;
    using BNine.Application.Abstractions;
    using BNine.Application.Aggregates.Plaid.Models;
    using BNine.Application.Interfaces;
    using MediatR;

    public class GetProfileBankAccountsQueryHandler
        : AbstractRequestHandler
        , IRequestHandler<GetProfileBankAccountsQuery, List<ProfileBankAccount>>
    {
        public GetProfileBankAccountsQueryHandler(
            IMediator mediator,
            IBNineDbContext dbContext,
            IMapper mapper,
            ICurrentUserService currentUser)
            : base(mediator, dbContext, mapper, currentUser)
        {
        }

        public async Task<List<ProfileBankAccount>> Handle(GetProfileBankAccountsQuery request, CancellationToken cancellationToken)
        {
            throw new Exception();
        }
    }
}
