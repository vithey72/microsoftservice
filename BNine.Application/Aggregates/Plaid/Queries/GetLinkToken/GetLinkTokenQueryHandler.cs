﻿namespace BNine.Application.Aggregates.Plaid.Queries.GetLinkToken
{
    using System.Threading;
    using System.Threading.Tasks;
    using AutoMapper;
    using BNine.Application.Abstractions;
    using BNine.Application.Interfaces;
    using BNine.Settings;
    using Interfaces.Plaid;
    using MediatR;
    using Microsoft.Extensions.Options;

    public class GetLinkTokenQueryHandler
        : AbstractRequestHandler
            , IRequestHandler<GetLinkTokenQuery, string>
    {
        private PlaidSettings PlaidSetting
        {
            get;
        }

        private IPlaidAuthService PlaidAuthService
        {
            get;
        }

        public GetLinkTokenQueryHandler(
            IOptions<PlaidSettings> options,
            IMediator mediator,
            IBNineDbContext dbContext,
            IMapper mapper,
            ICurrentUserService currentUser,
            IPlaidAuthService plaidAuthService)
            : base(mediator, dbContext, mapper, currentUser)
        {
            PlaidSetting = options.Value;
            PlaidAuthService = plaidAuthService;
        }

        public async Task<string> Handle(GetLinkTokenQuery request, CancellationToken cancellationToken)
        {
            throw new System.Exception();
        }
    }
}
