﻿namespace BNine.Application.Aggregates.Plaid.Queries.GetLinkToken
{
    using Enums;
    using MediatR;

    public class GetLinkTokenQuery : IRequest<string>
    {
        public GetLinkTokenQuery(MobilePlatform platform)
        {
            Platform = platform;
        }

        public MobilePlatform Platform
        {
            get;
        }
    }
}
