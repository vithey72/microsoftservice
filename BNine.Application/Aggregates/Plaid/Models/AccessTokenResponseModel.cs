﻿namespace BNine.Application.Aggregates.Plaid.Models
{
    using Newtonsoft.Json;

    public class AccessTokenResponseModel
    {
        [JsonProperty("access_token")]
        public string AccessToken
        {
            get; set;
        }

        [JsonProperty("item_id")]
        public string ItemId
        {
            get; set;
        }
    }
}
