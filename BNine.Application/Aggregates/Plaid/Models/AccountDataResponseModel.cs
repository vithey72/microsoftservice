﻿namespace BNine.Application.Aggregates.Plaid.Models
{
    using System.Collections.Generic;
    using Newtonsoft.Json;

    public class Balances
    {
        [JsonProperty("available")]
        public decimal? Available
        {
            get; set;
        }

        [JsonProperty("current")]
        public decimal? Current
        {
            get; set;
        }

        [JsonProperty("limit")]
        public object Limit
        {
            get; set;
        }

        [JsonProperty("iso_currency_code")]
        public string IsoCurrencyCode
        {
            get; set;
        }

        [JsonProperty("unofficial_currency_code")]
        public object UnofficialCurrencyCode
        {
            get; set;
        }
    }

    public class Account
    {
        [JsonProperty("accountNumber")]
        public string AccountNumber
        {
            get; set;
        }

        [JsonProperty("account_id")]
        public string AccountId
        {
            get; set;
        }

        [JsonProperty("balances")]
        public Balances Balances
        {
            get; set;
        }

        [JsonProperty("mask")]
        public string Mask
        {
            get; set;
        }

        [JsonProperty("name")]
        public string Name
        {
            get; set;
        }

        [JsonProperty("official_name")]
        public string OfficialName
        {
            get; set;
        }

        [JsonProperty("subtype")]
        public string Subtype
        {
            get; set;
        }

        [JsonProperty("type")]
        public string Type
        {
            get; set;
        }
    }

    public class Ach
    {
        [JsonProperty("account")]
        public string Account
        {
            get; set;
        }

        [JsonProperty("account_id")]
        public string AccountId
        {
            get; set;
        }

        [JsonProperty("routing")]
        public string Routing
        {
            get; set;
        }

        [JsonProperty("wire_routing")]
        public string WireRouting
        {
            get; set;
        }
    }

    public class Eft
    {
        [JsonProperty("account")]
        public string Account
        {
            get; set;
        }

        [JsonProperty("account_id")]
        public string AccountId
        {
            get; set;
        }

        [JsonProperty("institution")]
        public string Institution
        {
            get; set;
        }

        [JsonProperty("branch")]
        public string Branch
        {
            get; set;
        }
    }

    public class International
    {
        [JsonProperty("account_id")]
        public string AccountId
        {
            get; set;
        }

        [JsonProperty("bic")]
        public string Bic
        {
            get; set;
        }

        [JsonProperty("iban")]
        public string Iban
        {
            get; set;
        }
    }

    public class Bac
    {
        [JsonProperty("account")]
        public string Account
        {
            get; set;
        }

        [JsonProperty("account_id")]
        public string AccountId
        {
            get; set;
        }

        [JsonProperty("sort_code")]
        public string SortCode
        {
            get; set;
        }
    }

    public class Numbers
    {
        [JsonProperty("ach")]
        public List<Ach> Ach
        {
            get; set;
        }

        [JsonProperty("eft")]
        public List<Eft> Eft
        {
            get; set;
        }

        [JsonProperty("international")]
        public List<International> International
        {
            get; set;
        }

        [JsonProperty("bacs")]
        public List<Bac> Bacs
        {
            get; set;
        }
    }

    public class Item
    {
        [JsonProperty("available_products")]
        public List<string> AvailableProducts
        {
            get; set;
        }

        [JsonProperty("billed_products")]
        public List<string> BilledProducts
        {
            get; set;
        }

        [JsonProperty("consent_expiration_time")]
        public object ConsentExpirationTime
        {
            get; set;
        }

        [JsonProperty("error")]
        public object Error
        {
            get; set;
        }

        [JsonProperty("institution_id")]
        public string InstitutionId
        {
            get; set;
        }

        [JsonProperty("item_id")]
        public string ItemId
        {
            get; set;
        }

        [JsonProperty("update_type")]
        public string UpdateType
        {
            get; set;
        }

        [JsonProperty("webhook")]
        public string Webhook
        {
            get; set;
        }
    }

    public class AccountDataResponseModel
    {
        [JsonProperty("accounts")]
        public List<Account> Accounts
        {
            get; set;
        }

        [JsonProperty("numbers")]
        public Numbers Numbers
        {
            get; set;
        }

        [JsonProperty("item")]
        public Item Item
        {
            get; set;
        }

        [JsonProperty("request_id")]
        public string RequestId
        {
            get; set;
        }
    }
}
