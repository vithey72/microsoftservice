﻿namespace BNine.Application.Aggregates.Plaid.Models.Transactions
{
    using System.Collections.Generic;
    using Newtonsoft.Json;

    public class ItemInfo
    {
        [JsonProperty("itemId")]
        public string ItemId
        {
            get;
            set;
        }

        [JsonProperty("institutionId")]
        public string InstitutionId
        {
            get;
            set;
        }

        [JsonProperty("webhook")]
        public string Webhook
        {
            get;
            set;
        }

        [JsonProperty("error")]
        public ErrorInfo Error
        {
            get;
            set;
        }

        [JsonProperty("availableProducts")]
        public IEnumerable<string> AvailableProducts
        {
            get;
            set;
        }

        [JsonProperty("billedProducts")]
        public IEnumerable<string> BilledProducts
        {
            get;
            set;
        }

        [JsonProperty("consentExpirationTime")]
        public string ConsentExpirationTime
        {
            get;
            set;
        }

        [JsonProperty("updateType")]
        public string UpdateType
        {
            get;
            set;
        }
    }
}
