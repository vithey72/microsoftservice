﻿namespace BNine.Application.Aggregates.Plaid.Models.Transactions
{
    using Newtonsoft.Json;

    public class LocationInfo
    {
        [JsonProperty("address")]
        public string Address
        {
            get;
            set;
        }

        [JsonProperty("city")]
        public string City
        {
            get;
            set;
        }

        [JsonProperty("region")]
        public string Region
        {
            get;
            set;
        }

        [JsonProperty("postalCode")]
        public string PostalCode
        {
            get;
            set;
        }

        [JsonProperty("country")]
        public string Country
        {
            get;
            set;
        }

        [JsonProperty("storeNumber")]
        public string StoreNumber
        {
            get;
            set;
        }

        [JsonProperty("lat")]
        public float? Latitude
        {
            get;
            set;
        }

        [JsonProperty("lon")]
        public float? Longitude
        {
            get;
            set;
        }
    }
}
