﻿namespace BNine.Application.Aggregates.Plaid.Models.Transactions
{
    using Newtonsoft.Json;

    public class ErrorInfo
    {
        [JsonProperty("errorType")]
        public string ErrorType
        {
            get;
            set;
        }

        [JsonProperty("errorCode")]
        public string ErrorCode
        {
            get;
            set;
        }

        [JsonProperty("errorMessage")]
        public string ErrorMessage
        {
            get;
            set;
        }

        [JsonProperty("displayMessage")]
        public string DisplayMessage
        {
            get;
            set;
        }

        [JsonProperty("requestId")]
        public string RequestId
        {
            get;
            set;
        }

        [JsonProperty("status")]
        public int? Status
        {
            get;
            set;
        }

        [JsonProperty("documentationUrl")]
        public string DocumentationUrl
        {
            get;
            set;
        }

        [JsonProperty("suggestedAction")]
        public string SuggestedAction
        {
            get;
            set;
        }
    }
}
