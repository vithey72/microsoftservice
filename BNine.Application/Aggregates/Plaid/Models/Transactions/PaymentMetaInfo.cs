﻿namespace BNine.Application.Aggregates.Plaid.Models.Transactions
{
    using Newtonsoft.Json;

    public class PaymentMetaInfo
    {
        [JsonProperty("referenceNumber")]
        public string ReferenceNumber
        {
            get;
            set;
        }

        [JsonProperty("ppdId")]
        public string PpdId
        {
            get;
            set;
        }

        [JsonProperty("payee")]
        public string Payee
        {
            get;
            set;
        }

        [JsonProperty("byOrderOf")]
        public string ByOrderOf
        {
            get;
            set;
        }

        [JsonProperty("payer")]
        public string Payer
        {
            get;
            set;
        }

        [JsonProperty("paymentMethod")]
        public string PaymentMethod
        {
            get;
            set;
        }

        [JsonProperty("paymentProcessor")]
        public string PaymentProcessor
        {
            get;
            set;
        }

        [JsonProperty("reason")]
        public string Reason
        {
            get;
            set;
        }
    }
}
