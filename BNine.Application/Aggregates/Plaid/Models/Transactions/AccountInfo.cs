﻿namespace BNine.Application.Aggregates.Plaid.Models.Transactions
{
    using Newtonsoft.Json;

    public class AccountInfo
    {
        [JsonProperty("accountId")]
        public string AccountId
        {
            get;
            set;
        }

        [JsonProperty("mask")]
        public string Mask
        {
            get;
            set;
        }

        [JsonProperty("name")]
        public string Name
        {
            get;
            set;
        }

        [JsonProperty("officialName")]
        public string OfficialName
        {
            get;
            set;
        }

        [JsonProperty("subtype")]
        public string Subtype
        {
            get;
            set;
        }

        [JsonProperty("type")]
        public string Type
        {
            get;
            set;
        }

        [JsonProperty("verificationStatus")]
        public string VerificationStatus
        {
            get;
            set;
        }

        [JsonProperty("balances")]
        public BalanceInfo Balances
        {
            get;
            set;
        }
    }
}
