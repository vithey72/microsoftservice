﻿namespace BNine.Application.Aggregates.Plaid.Models.Transactions
{
    using Newtonsoft.Json;

    public class BalanceInfo
    {
        [JsonProperty("available")]
        public decimal? Available
        {
            get;
            set;
        }

        [JsonProperty("current")]
        public decimal Current
        {
            get;
            set;
        }

        [JsonProperty("isoCurrencyCode")]
        public string IsoCurrencyCode
        {
            get;
            set;
        }

        [JsonProperty("limit")]
        public decimal? Limit
        {
            get;
            set;
        }

        [JsonProperty("unofficialCurrencyCode")]
        public string UnofficialCurrencyCode
        {
            get;
            set;
        }

        [JsonProperty("lastUpdatedDatetime")]
        public string LastUpdatedDateTime
        {
            get;
            set;
        }
    }
}
