﻿namespace BNine.Application.Aggregates.Plaid.Models.Transactions
{
    using System.Collections.Generic;
    using Newtonsoft.Json;

    public class TransactionsDataInfo
    {
        [JsonProperty("transactions")]
        public IEnumerable<TransactionInfo> Transactions
        {
            get;
            set;
        }

        [JsonProperty("accounts")]
        public IEnumerable<AccountInfo> Accounts
        {
            get;
            set;
        }

        [JsonProperty("totalTransactions")]
        public int TotalTransactions
        {
            get;
            set;
        }

        [JsonProperty("item")]
        public ItemInfo Item
        {
            get;
            set;
        }

        [JsonProperty("requestId")]
        public string RequestId
        {
            get;
            set;
        }
    }
}
