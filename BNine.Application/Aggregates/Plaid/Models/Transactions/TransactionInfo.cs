﻿namespace BNine.Application.Aggregates.Plaid.Models.Transactions
{
    using System.Collections.Generic;
    using Newtonsoft.Json;

    public class TransactionInfo
    {
        [JsonProperty("accountId")]
        public string AccountId
        {
            get;
            set;
        }

        [JsonProperty("amount")]
        public decimal Amount
        {
            get;
            set;
        }

        [JsonProperty("isoCurrencyCode")]
        public string IsoCurrencyCode
        {
            get;
            set;
        }

        [JsonProperty("unofficialCurrencyCode")]
        public string UnofficialCurrencyCode
        {
            get;
            set;
        }

        [JsonProperty("category")]
        public IEnumerable<string> Categories
        {
            get;
            set;
        }

        [JsonProperty("categoryId")]
        public string CategoryId
        {
            get;
            set;
        }

        [JsonProperty("date")]
        public string Date
        {
            get;
            set;
        }

        [JsonProperty("datetime")]
        public string DateTime
        {
            get;
            set;
        }

        [JsonProperty("authorizedDate")]
        public string AuthorizedDate
        {
            get;
            set;
        }

        [JsonProperty("authorizedDatetime")]
        public string AuthorizedDateTime
        {
            get;
            set;
        }

        [JsonProperty("location")]
        public LocationInfo Location
        {
            get;
            set;
        }

        [JsonProperty("name")]
        public string Name
        {
            get;
            set;
        }

        [JsonProperty("merchantName")]
        public string MerchantName
        {
            get;
            set;
        }

        [JsonProperty("paymentMeta")]
        public PaymentMetaInfo PaymentMeta
        {
            get;
            set;
        }

        [JsonProperty("paymentChannel")]
        public string PaymentChannel
        {
            get;
            set;
        }

        [JsonProperty("pending")]
        public bool Pending
        {
            get;
            set;
        }

        [JsonProperty("pendingTransactionId")]
        public string PendingTransactionId
        {
            get;
            set;
        }

        [JsonProperty("accountOwner")]
        public string AccountOwner
        {
            get;
            set;
        }

        [JsonProperty("transactionId")]
        public string TransactionId
        {
            get;
            set;
        }

        [JsonProperty("transactionCode")]
        public string TransactionCode
        {
            get;
            set;
        }
    }
}
