﻿namespace BNine.Application.Aggregates.Plaid.Models
{
    using System;
    using AutoMapper;
    using BNine.Application.Mappings;
    using BNine.Domain.Entities;

    public class ProfileBankAccount : IMapTo<ExternalBankAccount>
    {
        public Guid Id
        {
            get; set;
        }

        public DateTime BindingDate
        {
            get; set;
        }

        public string AccountNumber
        {
            get; set;
        }

        public string RoutingNumber
        {
            get; set;
        }

        public void Mapping(Profile profile)
        {
            profile.CreateMap<ExternalBankAccount, ProfileBankAccount>();
        }
    }
}
