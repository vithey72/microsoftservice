﻿namespace BNine.Application.Aggregates.Plaid.Commands.CreateAccount
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading;
    using System.Threading.Tasks;
    using Abstractions;
    using Application.Models.Dto;
    using AutoMapper;
    using BNine.Application.Extensions.IQueryableExtensions;
    using Domain.Entities;
    using Exceptions;
    using Interfaces;
    using Interfaces.Plaid;
    using MediatR;
    using Models;

    public class CreateAccountCommandHandler
        : AbstractRequestHandler,
        IRequestHandler<CreateAccountCommand, bool>
    {
        private IPlaidTransactionService TransactionService
        {
            get;
        }

        private IPlaidAuthService PlaidAuthService
        {
            get;
        }

        public CreateAccountCommandHandler(
            IPlaidTransactionService transactionService,
            IMediator mediator,
            IBNineDbContext dbContext,
            IMapper mapper,
            ICurrentUserService currentUser,
            IPlaidAuthService plaidAuthService)
            : base(mediator, dbContext, mapper, currentUser)
        {
            TransactionService = transactionService;
            PlaidAuthService = plaidAuthService;
        }

        public async Task<bool> Handle(CreateAccountCommand request, CancellationToken cancellationToken)
        {
            throw new Exception();
            var accessInfo = await PlaidAuthService.GetAccessToken(request.PublicToken);

            var accountData = await PlaidAuthService.GetAccountData(accessInfo.AccessToken);

            var user = DbContext.Users
                .FirstOrDefault(x => x.Id == CurrentUser.UserId);
            if (user == null)
            {
                throw new NotFoundException(nameof(User), CurrentUser.UserId);
            }

            var bankAccounts = AddBankAccounts(CurrentUser.UserId.Value, accountData, accessInfo);

            await AddPlaidData(bankAccounts, accessInfo);

            await DbContext.SaveChangesAsync(cancellationToken);
            // await PlaidAuthService.DisposeAccessToken(accessInfo.AccessToken);

            return true;
        }

        private Dictionary<Guid, string> AddBankAccounts(Guid userId, AccountDataResponseModel accountData, AccessTokenResponseModel accessInfo)
        {
            var bankAccounts = new Dictionary<Guid, string>();
            foreach (var account in accountData.Numbers.Ach)
            {
                var availableAmount = accountData.Accounts.FirstOrDefault(x => x.AccountId == account.AccountId)?.Balances?.Available;

                var bankAccount = new ExternalBankAccount
                {
                    Id = Guid.NewGuid(),
                    AccountNumber = account.Account,
                    RoutingNumber = account.Routing,
                    BindingDate = DateTime.UtcNow,
                    UserId = userId,
                    ExternalAccountId = account.AccountId,
                    Currency = accountData.Accounts.FirstOrDefault(x => x.AccountId == account.AccountId)?.Balances?.IsoCurrencyCode,
                    InitialAvailableAmount = availableAmount,
                    AccessToken = accessInfo.AccessToken
                };

                var addedBeforeAccount = DbContext.BankAccounts
                    .NotRemoved()
                    .Where(x => x.ExternalAccountId == account.AccountId && x.UserId == CurrentUser.UserId)
                    .FirstOrDefault();

                if (addedBeforeAccount == null)
                {
                    DbContext.BankAccounts.Add(bankAccount);
                    bankAccounts.Add(bankAccount.Id, account.AccountId);
                }
                else
                {
                    addedBeforeAccount.InitialAvailableAmount = availableAmount;
                    addedBeforeAccount.UpdatedAt = DateTime.UtcNow;
                }
            }

            return bankAccounts;
        }

        private async Task AddPlaidData(Dictionary<Guid, string> bankAccounts, AccessTokenResponseModel accessInfo)
        {
            if (!bankAccounts.Any())
            {
                return;
            }

            foreach (var bankAccount in bankAccounts)
            {
                var transactionsData = await TransactionService.GetRawTransactionsData(
                    accessInfo.AccessToken,
                    new[] { bankAccount.Value });

                var plaidData = new Plaid
                {
                    BankAccountId = bankAccount.Key,
                    Transactions = new List<string>
                    {
                        transactionsData
                    },
                    CreatedAt = DateTime.UtcNow,
                    UpdatedAt = DateTime.UtcNow,
                };

                await DbContext.PlaidData.AddAsync(plaidData);
            }
        }
    }
}
