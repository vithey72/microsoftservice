﻿namespace BNine.Application.Aggregates.Plaid.Commands.CreateAccount
{
    using MediatR;
    using Newtonsoft.Json;

    public class CreateAccountCommand : IRequest<bool>
    {
        [JsonProperty("public_token")]
        public string PublicToken
        {
            get; set;
        }
    }
}
