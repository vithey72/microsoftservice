﻿namespace BNine.Application.Aggregates.ExternalCards.Commands.DeleteExternalCard;

using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using Abstractions;
using Exceptions;
using Interfaces;
using Interfaces.Bank.Client;
using Domain.Entities.User;
using MediatR;
using Microsoft.EntityFrameworkCore;
using FavoriteTransfers.Command.DeleteFavoriteTransfersAssociatedWithExternalCardCommand;

public class DeleteExternalCardCommandHandler
    : AbstractRequestHandler, IRequestHandler<DeleteExternalCardCommand, Unit>
{
    private IBankClientExternalCardsService BankClientExternalCardsService
    {
        get;
    }

    public DeleteExternalCardCommandHandler(
        IBankClientExternalCardsService bankClientExternalCardsService,
        IMediator mediator,
        IBNineDbContext dbContext,
        IMapper mapper,
        ICurrentUserService currentUser
    ) : base(mediator, dbContext, mapper, currentUser)
    {
        BankClientExternalCardsService = bankClientExternalCardsService;
    }

    public async Task<Unit> Handle(DeleteExternalCardCommand request, CancellationToken cancellationToken)
    {
        var user = DbContext.Users
            .Where(x => x.Id == CurrentUser.UserId)
            .Select(x => new
            {
                x.ExternalClientId
            })
            .FirstOrDefault();

        if (user == null || !user.ExternalClientId.HasValue)
        {
            throw new NotFoundException(nameof(User), CurrentUser.UserId);
        }

        await BankClientExternalCardsService.DeleteCard(request.ExternalId, user.ExternalClientId.Value, request.Ip, request.MbanqAccessToken);

        var externalCard = await DbContext.ExternalCards
            .Where(x => x.UserId == CurrentUser.UserId)
            .Where(x => x.ExternalId == request.ExternalId)
            .FirstOrDefaultAsync(cancellationToken);

        if (externalCard != null)
        {
            externalCard.DeletedAt = DateTime.UtcNow;
            await DbContext.SaveChangesAsync(cancellationToken);
        }

        await Mediator.Send(new DeleteFavoriteTransfersAssociatedWithExternalCardCommand
        {
            ExternalCardId = request.ExternalId,
            MbanqAccessToken = request.MbanqAccessToken,
            Ip = request.Ip,
        }, cancellationToken);

        return Unit.Value;
    }
}
