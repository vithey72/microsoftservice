﻿namespace BNine.Application.Aggregates.ExternalCards.Commands.DeleteExternalCard
{
    using BNine.Application.Abstractions;
    using MediatR;

    public class DeleteExternalCardCommand
        : MBanqSelfServiceUserRequest, IRequest<Unit>
    {
        public DeleteExternalCardCommand(long externalId, string mbanqAccessToken, string ip)
        {
            ExternalId = externalId;
            MbanqAccessToken = mbanqAccessToken;
            Ip = ip;
        }

        public long ExternalId
        {
            get;
        }
    }
}
