﻿namespace BNine.Application.Aggregates.ExternalCards.Commands.SaveExternalCardDetails
{
    using System;
    using System.Linq;
    using System.Threading;
    using System.Threading.Tasks;
    using AutoMapper;
    using BNine.Application.Abstractions;
    using BNine.Application.Interfaces;
    using MediatR;

    public class SaveExternalCardDetailsCommandHandler
        : AbstractRequestHandler, IRequestHandler<SaveExternalCardDetailsCommand, Unit>
    {
        public SaveExternalCardDetailsCommandHandler(IMediator mediator, IBNineDbContext dbContext, IMapper mapper, ICurrentUserService currentUser) : base(mediator, dbContext, mapper, currentUser)
        {
        }

        public async Task<Unit> Handle(SaveExternalCardDetailsCommand request, CancellationToken cancellationToken)
        {
            var card = DbContext.ExternalCards
                .Where(x => !x.DeletedAt.HasValue)
                .Where(x => x.ExternalId == request.Id)
                .Where(x => x.UserId == CurrentUser.UserId)
                .FirstOrDefault();

            if (card == null)
            {
                DbContext.ExternalCards.Add(new Domain.Entities.ExternalCards.ExternalCard
                {
                    ExternalId = request.Id,
                    Name = request.Name,
                    UserId = CurrentUser.UserId.Value
                });
            }
            else
            {
                card.Name = request.Name;
                card.UpdatedAt = DateTime.UtcNow;
            }

            await DbContext.SaveChangesAsync(cancellationToken);

            return Unit.Value;
        }
    }
}
