﻿namespace BNine.Application.Aggregates.ExternalCards.Commands.SaveExternalCardDetails
{
    using MediatR;
    using Newtonsoft.Json;

    public class SaveExternalCardDetailsCommand
        : IRequest<Unit>
    {
        [JsonProperty("name")]
        public string Name
        {
            get; set;
        }

        [JsonIgnore]
        public long Id
        {
            get; set;
        }
    }
}
