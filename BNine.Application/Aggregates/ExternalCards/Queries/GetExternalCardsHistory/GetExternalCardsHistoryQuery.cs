﻿namespace BNine.Application.Aggregates.ExternalCards.Queries.GetExternalCardsHistory;

using MediatR;
using Models.MBanq;

public class GetExternalCardsHistoryQuery: IRequest<List<ExternalCardOperationHistory>>
{
    public Guid ClientId
    {
        get;
        set;
    }
}
