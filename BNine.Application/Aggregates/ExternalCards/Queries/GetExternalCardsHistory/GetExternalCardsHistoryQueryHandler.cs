﻿namespace BNine.Application.Aggregates.ExternalCards.Queries.GetExternalCardsHistory;

using Abstractions;
using AutoMapper;
using Exceptions;
using Interfaces;
using Interfaces.Bank.Administration;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Models.MBanq;

public class GetExternalCardsHistoryQueryHandler
    : AbstractRequestHandler, IRequestHandler<GetExternalCardsHistoryQuery, List<ExternalCardOperationHistory>>
{
    private readonly IBankExternalCardsService _externalCardsService;

    public GetExternalCardsHistoryQueryHandler(
        IMediator mediator,
        IBNineDbContext dbContext,
        IMapper mapper,
        ICurrentUserService currentUser,
        IBankExternalCardsService externalCardsService) : base(mediator, dbContext, mapper, currentUser)
    {
        _externalCardsService = externalCardsService;
    }

    public async Task<List<ExternalCardOperationHistory>> Handle(GetExternalCardsHistoryQuery request, CancellationToken cancellationToken)
    {
        var user = await DbContext.Users.FirstOrDefaultAsync(x => x.Id == request.ClientId);

        if (user == null)
        {
            throw new NotFoundException($"User not found with id: {request.ClientId}");
        }

        if (!user.ExternalClientId.HasValue)
        {
            return null;
        }

        var operationHistory = await _externalCardsService.GetExternalCardsHistory(user.ExternalClientId.Value);

        return operationHistory;
    }
}
