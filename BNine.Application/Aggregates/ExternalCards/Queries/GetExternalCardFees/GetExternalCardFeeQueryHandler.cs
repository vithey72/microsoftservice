﻿namespace BNine.Application.Aggregates.ExternalCards.Queries.GetExternalCardFees
{
    using System.Linq;
    using System.Threading;
    using System.Threading.Tasks;
    using AutoMapper;
    using BNine.Application.Abstractions;
    using BNine.Application.Interfaces;
    using BNine.Application.Interfaces.Bank.Administration;
    using BNine.Constants;
    using MediatR;
    using Microsoft.Extensions.Caching.Memory;

    public class GetExternalCardFeeQueryHandler
        : AbstractRequestHandler, IRequestHandler<GetExternalCardFeeQuery, ExternalCardFee>
    {
        private IBankChargesService BankChargesService
        {
            get;
        }

        private IMemoryCache Cache
        {
            get;
        }

        public GetExternalCardFeeQueryHandler(
            IBankChargesService bankChargesService,
            IMediator mediator,
            IBNineDbContext dbContext,
            IMapper mapper,
            ICurrentUserService currentUser,
            IMemoryCache cache
            ) : base(mediator, dbContext, mapper, currentUser)
        {
            BankChargesService = bankChargesService;
            Cache = cache;
        }

        public async Task<ExternalCardFee> Handle(GetExternalCardFeeQuery request, CancellationToken cancellationToken)
        {
            if (Cache.TryGetValue(CacheDefaults.ExternalCardsFee, out ExternalCardFee fee))
            {
                return fee;
            }

            var chargeProducts = DbContext.MBanqChargeProducts
                .Where(x => x.ChargeType == Domain.Entities.MBanq.ChargeType.PullFromCardFee || x.ChargeType == Domain.Entities.MBanq.ChargeType.PushToCardFee)
                .ToList();

            if (chargeProducts.Count != 2)
            {
                throw new System.Exception("Unknown charges");
            }

            var pullFromCardProductId = chargeProducts.First(x => x.ChargeType == Domain.Entities.MBanq.ChargeType.PullFromCardFee).ExternalId;

            var pullFromCardCharge = await BankChargesService.GetChargeDetails(pullFromCardProductId);

            var pushToCardProductId = chargeProducts.First(x => x.ChargeType == Domain.Entities.MBanq.ChargeType.PushToCardFee).ExternalId;

            var pushToCardCharge = await BankChargesService.GetChargeDetails(pushToCardProductId);

            var result = new ExternalCardFee
            {
                PullFromExternalCardMinFeeAmount = pullFromCardCharge.MinCap is not null ? pullFromCardCharge.MinCap.Value : 0,
                PullFromExternalCardMinFeePercent = pullFromCardCharge.Amount,
                PushToExternalCardMinFeeAmount = pushToCardCharge.MinCap is not null ? pullFromCardCharge.MinCap.Value : 0,
                PushToExternalCardMinFeePercent = pushToCardCharge.Amount
            };

            Cache.Set(CacheDefaults.ExternalCardsFee, result, new MemoryCacheEntryOptions().SetAbsoluteExpiration(System.TimeSpan.FromMinutes(5)));

            return result;
        }
    }
}
