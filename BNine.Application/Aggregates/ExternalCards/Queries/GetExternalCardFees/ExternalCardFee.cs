﻿namespace BNine.Application.Aggregates.ExternalCards.Queries.GetExternalCardFees
{
    using Newtonsoft.Json;

    public class ExternalCardFee
    {
        [JsonProperty("pullFromExternalCardMinFeeAmount")]
        public decimal PullFromExternalCardMinFeeAmount
        {
            get; set;
        }

        [JsonProperty("pullFromExternalCardMinFeePercent")]
        public decimal PullFromExternalCardMinFeePercent
        {
            get; set;
        }

        [JsonProperty("pushToExternalCardMinFeeAmount")]
        public decimal PushToExternalCardMinFeeAmount
        {
            get; set;
        }

        [JsonProperty("pushToExternalCardMinFeePercent")]
        public decimal PushToExternalCardMinFeePercent
        {
            get; set;
        }
    }
}
