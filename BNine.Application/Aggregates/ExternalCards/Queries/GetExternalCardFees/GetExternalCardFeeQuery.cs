﻿namespace BNine.Application.Aggregates.ExternalCards.Queries.GetExternalCardFees
{
    using MediatR;

    public class GetExternalCardFeeQuery : IRequest<ExternalCardFee>
    {
    }
}
