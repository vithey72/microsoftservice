﻿namespace BNine.Application.Aggregates.ExternalCards.Queries.GetPublicKey
{
    using MediatR;

    public class GetPublicKeyQuery : IRequest<PublicKeyInfo>
    {
    }
}
