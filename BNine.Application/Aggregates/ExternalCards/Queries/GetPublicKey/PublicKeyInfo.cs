﻿namespace BNine.Application.Aggregates.ExternalCards.Queries.GetPublicKey
{
    using Newtonsoft.Json;

    public class PublicKeyInfo
    {
        [JsonProperty("key")]
        public string Key
        {
            get; set;
        }

        [JsonProperty("clientId")]
        public int ClientId
        {
            get; set;
        }
    }
}
