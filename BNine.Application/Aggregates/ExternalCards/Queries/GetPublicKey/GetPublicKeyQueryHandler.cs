﻿namespace BNine.Application.Aggregates.ExternalCards.Queries.GetPublicKey
{
    using System.Linq;
    using System.Threading;
    using System.Threading.Tasks;
    using AutoMapper;
    using BNine.Application.Abstractions;
    using BNine.Application.Exceptions;
    using BNine.Application.Interfaces;
    using BNine.Application.Interfaces.Bank.Administration;
    using MediatR;

    public class GetPublicKeyQueryHandler
        : AbstractRequestHandler, IRequestHandler<GetPublicKeyQuery, PublicKeyInfo>
    {
        private IBankExternalCardsService BankExternalCardsService
        {
            get;
        }

        public GetPublicKeyQueryHandler(IBankExternalCardsService bankExternalCardsService, IMediator mediator, IBNineDbContext dbContext, IMapper mapper, ICurrentUserService currentUser) : base(mediator, dbContext, mapper, currentUser)
        {
            BankExternalCardsService = bankExternalCardsService;
        }

        public async Task<PublicKeyInfo> Handle(GetPublicKeyQuery request, CancellationToken cancellationToken)
        {
            var key = await BankExternalCardsService.GetPublicKey();

            var externalClientId = DbContext.Users
                .Where(x => x.Id == CurrentUser.UserId)
                .Select(x => x.ExternalClientId)
                .FirstOrDefault();

            if (!externalClientId.HasValue)
            {
                throw new NotFoundException("ExternalClient", CurrentUser.UserId);
            }

            return new PublicKeyInfo
            {
                Key = key,
                ClientId = externalClientId.Value
            };
        }
    }
}
