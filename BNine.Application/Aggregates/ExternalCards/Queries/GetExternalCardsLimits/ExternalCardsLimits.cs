﻿namespace BNine.Application.Aggregates.ExternalCards.Queries.GetExternalCardsLimits
{
    using AutoMapper;
    using BNine.Application.Mappings;
    using Domain.Entities.Settings;
    using Newtonsoft.Json;

    public class ExternalCardsLimits : IMapFrom<SavingAccountSettings>
    {
        [JsonProperty("pullFromExternalCardMinAmount")]
        public decimal PullFromExternalCardMinAmount
        {
            get; set;
        }

        [JsonProperty("pullFromExternalCardMaxAmount")]
        public decimal PullFromExternalCardMaxAmount
        {
            get; set;
        }

        [JsonProperty("pushToExternalCardMinAmount")]
        public decimal PushToExternalCardMinAmount
        {
            get; set;
        }

        [JsonProperty("pushToExternalCardMaxAmount")]
        public decimal PushToExternalCardMaxAmount
        {
            get; set;
        }

        public void Mapping(Profile profile) => profile.CreateMap<SavingAccountSettings, ExternalCardsLimits>()
            .ForMember(d => d.PullFromExternalCardMaxAmount, opt => opt.MapFrom(s => s.PullFromExternalCardMaxAmount))
            .ForMember(d => d.PullFromExternalCardMinAmount, opt => opt.MapFrom(s => s.PullFromExternalCardMinAmount))
            .ForMember(d => d.PushToExternalCardMinAmount, opt => opt.MapFrom(s => s.PushToExternalCardMinAmount))
            .ForMember(d => d.PushToExternalCardMaxAmount, opt => opt.MapFrom(s => s.PushToExternalCardMaxAmount));
    }
}
