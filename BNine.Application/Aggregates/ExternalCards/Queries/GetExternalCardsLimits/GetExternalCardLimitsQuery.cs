﻿namespace BNine.Application.Aggregates.ExternalCards.Queries.GetExternalCardsLimits
{
    using MediatR;

    public class GetExternalCardLimitsQuery : IRequest<ExternalCardsLimits>
    {
    }
}
