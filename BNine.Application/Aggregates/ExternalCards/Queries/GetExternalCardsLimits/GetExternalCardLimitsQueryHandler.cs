﻿namespace BNine.Application.Aggregates.ExternalCards.Queries.GetExternalCardsLimits
{
    using System.Threading;
    using System.Threading.Tasks;
    using AutoMapper;
    using AutoMapper.QueryableExtensions;
    using BNine.Application.Abstractions;
    using BNine.Application.Interfaces;
    using MediatR;
    using Microsoft.EntityFrameworkCore;

    public class GetExternalCardLimitsQueryHandler
        : AbstractRequestHandler, IRequestHandler<GetExternalCardLimitsQuery, ExternalCardsLimits>
    {
        public GetExternalCardLimitsQueryHandler(IMediator mediator, IBNineDbContext dbContext, IMapper mapper, ICurrentUserService currentUser) : base(mediator, dbContext, mapper, currentUser)
        {
        }

        public async Task<ExternalCardsLimits> Handle(GetExternalCardLimitsQuery request, CancellationToken cancellationToken)
        {
            var fees = await DbContext.SavingAccountSettings
                .ProjectTo<ExternalCardsLimits>(Mapper.ConfigurationProvider)
                .SingleOrDefaultAsync(cancellationToken);

            return fees;
        }
    }
}
