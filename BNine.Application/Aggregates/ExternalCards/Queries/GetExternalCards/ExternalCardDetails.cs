﻿namespace BNine.Application.Aggregates.ExternalCards.Queries.GetExternalCards
{
    using AutoMapper;
    using BNine.Application.Mappings;
    using BNine.Application.Models.MBanq;
    using Newtonsoft.Json;

    public class ExternalCardDetails : IMapFrom<ExternalCardListItem>
    {
        [JsonProperty("id")]
        public long Id
        {
            get; set;
        }

        [JsonProperty("last4Digits")]
        public string Last4Digits
        {
            get; set;
        }

        [JsonProperty("networkName")]
        public string NetworkName
        {
            get; set;
        }

        [JsonProperty("name")]
        public string Name
        {
            get; set;
        }

        public void Mapping(Profile profile) => profile.CreateMap<ExternalCardListItem, ExternalCardDetails>()
            .ForMember(d => d.Id, opt => opt.MapFrom(s => s.ExternalId))
            .ForMember(d => d.Last4Digits, opt => opt.MapFrom(s => s.Last4Digits))
            .ForMember(d => d.NetworkName, opt => opt.MapFrom(s => s.NetworkName))
            .ForMember(d => d.Name, opt => opt.Ignore());
    }
}
