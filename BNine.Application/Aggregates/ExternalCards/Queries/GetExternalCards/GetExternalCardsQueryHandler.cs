﻿namespace BNine.Application.Aggregates.ExternalCards.Queries.GetExternalCards
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading;
    using System.Threading.Tasks;
    using AutoMapper;
    using BNine.Application.Abstractions;
    using BNine.Application.Interfaces;
    using BNine.Application.Interfaces.Bank.Client;
    using MediatR;

    public class GetExternalCardsQueryHandler
        : AbstractRequestHandler, IRequestHandler<GetExternalCardsQuery, IEnumerable<ExternalCardDetails>>
    {
        private IBankClientExternalCardsService BankClientExternalCardsService
        {
            get;
        }

        public GetExternalCardsQueryHandler(
            IBankClientExternalCardsService bankClientExternalCardsService,
            IMediator mediator,
            IBNineDbContext dbContext,
            IMapper mapper,
            ICurrentUserService currentUser
            ) : base(mediator, dbContext, mapper, currentUser)
        {
            BankClientExternalCardsService = bankClientExternalCardsService;
        }

        public async Task<IEnumerable<ExternalCardDetails>> Handle(GetExternalCardsQuery request, CancellationToken cancellationToken)
        {
            var mbanqCards = await BankClientExternalCardsService.GetAllUserCards(request.MbanqAccessToken, request.Ip);

            var cards = Mapper.Map<IEnumerable<ExternalCardDetails>>(mbanqCards);

            var databaseCards = DbContext.ExternalCards
                .Where(x => !x.DeletedAt.HasValue)
                .Where(x => x.UserId == CurrentUser.UserId)
                .Where(x => mbanqCards.Select(c => c.ExternalId).Contains(x.ExternalId))
                .ToList();

            foreach (var databaseCard in databaseCards)
            {
                var card = cards.First(x => x.Id == databaseCard.ExternalId);
                card.Name = databaseCard.Name;
            }

            foreach (var card in cards.Where(x => string.IsNullOrEmpty(x.Name)))
            {
                if (card.NetworkName?.Contains("visa", StringComparison.OrdinalIgnoreCase) == true)
                {
                    card.Name = "Visa";
                    continue;
                }

                if (card.NetworkName?.Contains("mastercard", StringComparison.OrdinalIgnoreCase) == true)
                {
                    card.Name = "MasterCard";
                    continue;
                }

                if (card.NetworkName?.Contains("amex", StringComparison.OrdinalIgnoreCase) == true)
                {
                    card.Name = "American Express";
                    continue;
                }

                card.Name = "Noname";
            }

            return cards;
        }
    }
}
