﻿namespace BNine.Application.Aggregates.ExternalCards.Queries.GetExternalCards
{
    using System.Collections.Generic;
    using BNine.Application.Abstractions;
    using MediatR;

    public class GetExternalCardsQuery
        : MBanqSelfServiceUserRequest, IRequest<IEnumerable<ExternalCardDetails>>
    {
        public GetExternalCardsQuery(string mbanqAccessToken, string ip)
        {
            MbanqAccessToken = mbanqAccessToken;
            Ip = ip;
        }
    }
}
