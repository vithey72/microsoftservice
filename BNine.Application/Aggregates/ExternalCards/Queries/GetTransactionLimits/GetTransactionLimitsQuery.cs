﻿namespace BNine.Application.Aggregates.ExternalCards.Queries.GetTransactionLimits
{
    using MediatR;

    public class GetTransactionLimitsQuery
        : IRequest<TransactionLimitsInfo>
    {
    }
}
