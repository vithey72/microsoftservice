﻿namespace BNine.Application.Aggregates.ExternalCards.Queries.GetTransactionLimits
{
    using BNine.Constants;
    using Newtonsoft.Json;

    public class TransactionLimitsInfo
    {
        [JsonProperty("transactionLimit")]
        public decimal TransactionLimit
        {
            get; set;
        } = ExternalCardsLimits.TransactionLimit;

        [JsonProperty("dailyLimit")]
        public decimal DailyLimit
        {
            get; set;
        } = ExternalCardsLimits.DailyLimit;

        [JsonProperty("monthlyLimit")]
        public decimal MonthlyLimit
        {
            get; set;
        } = ExternalCardsLimits.MonthlyLimit;
    }
}
