﻿namespace BNine.Application.Aggregates.ExternalCards.Queries.GetTransactionLimits
{
    using System.Threading;
    using System.Threading.Tasks;
    using AutoMapper;
    using BNine.Application.Abstractions;
    using BNine.Application.Interfaces;
    using MediatR;

    public class GetTransactionLimitsQueryHandler
        : AbstractRequestHandler, IRequestHandler<GetTransactionLimitsQuery, TransactionLimitsInfo>
    {
        public GetTransactionLimitsQueryHandler(IMediator mediator, IBNineDbContext dbContext, IMapper mapper, ICurrentUserService currentUser) : base(mediator, dbContext, mapper, currentUser)
        {
        }

        public Task<TransactionLimitsInfo> Handle(GetTransactionLimitsQuery request, CancellationToken cancellationToken)
        {
            return Task.FromResult(new TransactionLimitsInfo());
        }
    }
}
