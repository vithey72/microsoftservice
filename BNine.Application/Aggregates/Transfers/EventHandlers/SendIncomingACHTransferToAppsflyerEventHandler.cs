﻿namespace BNine.Application.Aggregates.Transfers.EventHandlers
{
    using System;
    using System.Linq;
    using System.Threading;
    using System.Threading.Tasks;
    using BNine.Application.Exceptions;
    using BNine.Application.Interfaces;
    using BNine.Application.Interfaces.Appsflyer;
    using BNine.Application.Models;
    using BNine.Constants;
    using BNine.Domain.Entities.User;
    using BNine.Domain.Events.ACHCreditTransfers;
    using Enums;
    using MediatR;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.Extensions.Logging;

    public class SendIncomingACHTransferToAppsflyerEventHandler : INotificationHandler<DomainEventNotification<ACHCreditTransferAddedEvent>>
    {
        private IAppsflyerServerEventsService AppsflyerServerEventsService
        {
            get;
        }

        private IBNineDbContext DbContext
        {
            get; set;
        }

        private ILogger Logger
        {
            get;
        }

        public SendIncomingACHTransferToAppsflyerEventHandler(
            IBNineDbContext dbContext,
            ILogger<SendIncomingACHTransferToAppsflyerEventHandler> logger,
            IAppsflyerServerEventsService appsflyerServerEventsService,
            IMediator mediator
            )
        {
            Mediator = mediator;
            AppsflyerServerEventsService = appsflyerServerEventsService;
            DbContext = dbContext;
            Logger = logger;
        }

        private IMediator Mediator
        {
            get;
        }

        public async Task Handle(DomainEventNotification<ACHCreditTransferAddedEvent> notification, CancellationToken cancellationToken)
        {
            var loanSettings = await DbContext.LoanSettings
              .SingleOrDefaultAsync(cancellationToken);

            if (!loanSettings.ACHPrincipalKeywords.Any(kw => notification.DomainEvent.Transfer.Reference.Contains(kw, StringComparison.OrdinalIgnoreCase)))
            {
                return;
            }

            var user = await DbContext.Users
                .Include(x => x.Devices)
                .Where(x => x.Id == notification.DomainEvent.Transfer.UserId)
                .Select(x => new
                {
                    x.AppsflyerId,
                    x.Devices
                }).FirstOrDefaultAsync(cancellationToken);

            if (user == null)
            {
                throw new NotFoundException(nameof(User), notification.DomainEvent.Transfer.UserId);
            }

            var appsflyerUser = await DbContext.AnonymousAppsflyerIds
                .Where(x => x.Id == user.AppsflyerId)
                .FirstOrDefaultAsync(cancellationToken);

            if (string.IsNullOrEmpty(appsflyerUser?.AppsflyerId))
            {
                Logger.LogError($"Publishing Appsflyer Server event failed: appsflyerUser not found", appsflyerUser);
            }
            else
            {
                try
                {
                    var platform = MobilePlatform.iOs;

                    if (user.Devices.Any() && user.Devices.All(x => x.Type == MobilePlatform.Android))
                    {
                        platform = MobilePlatform.Android;
                    }

                    await AppsflyerServerEventsService.SendEvent(appsflyerUser.AppsflyerId, appsflyerUser.Id, AppsflyerServerEvents.InconimgDeposit, platform);
                }
                catch (Exception ex)
                {
                    Logger.LogError($"Publishing Appsflyer Server event failed: {ex.Message}", appsflyerUser);
                }
            }
        }
    }
}
