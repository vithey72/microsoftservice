﻿namespace BNine.Application.Aggregates.Transfers.Commands.CreateInternalTransfer
{
    using System;
    using System.Linq;
    using System.Threading;
    using System.Threading.Tasks;
    using Application.Models.MBanq.Transfers;
    using AutoMapper;
    using BNine.Application.Abstractions;
    using BNine.Application.Exceptions;
    using BNine.Application.Interfaces;
    using BNine.Application.Interfaces.Bank.Client;
    using BNine.Application.Models.MBanq;
    using BNine.Domain.Entities.BankAccount;
    using BNine.Enums;
    using BNine.Enums.Transfers;
    using MediatR;
    using Microsoft.EntityFrameworkCore;

    public class CreateInternalTransferCommandHandler
        : AbstractRequestHandler
        , IRequestHandler<CreateInternalTransferCommand, int>
    {
        private const string InsufficientAccountBalanceKey = "INSUFFICIENT_ACCOUNT_BALANCE";

        private IBankTransfersService BankTransfersService
        {
            get;
        }

        public CreateInternalTransferCommandHandler(IBankTransfersService bankTransfersService, IMediator mediator, IBNineDbContext dbContext, IMapper mapper, ICurrentUserService currentUser) : base(mediator, dbContext, mapper, currentUser)
        {
            BankTransfersService = bankTransfersService;
        }

        public async Task<int> Handle(CreateInternalTransferCommand request, CancellationToken cancellationToken)
        {
            var recipient = await DbContext.Users
                .Where(x => !x.IsBlocked)
                .Where(x => x.Phone.Trim().Equals(request.Phone.Trim()) && x.Status == UserStatus.Active)
                .Select(x => new { x.Id, x.FirstName, x.LastName })
                .FirstOrDefaultAsync();

            if (recipient == null)
            {
                throw new ValidationException("phone", $"Recipient with phone '{request.Phone}' is not registered.");
            }

            var externalSavingAccounts = await DbContext.Users
                .Include(x => x.CurrentAccount)
                .Where(x => x.Id == CurrentUser.UserId || x.Id == recipient.Id)
                .Select(x => new { x.Id, x.CurrentAccount.ExternalId })
                .ToDictionaryAsync(x => x.Id, x => x.ExternalId);

            if (externalSavingAccounts.Count != 2)
            {
                throw new ValidationException(nameof(CurrentAccount), "Sender or recipient doesn't have saving account");
            }

            var result = await BankTransfersService.CreateAndSubmitTransfer(request.MbanqAccessToken, request.Ip, new Transfer
            {
                Type = TransferDirection.Credit,
                PaymentType = TransferType.Internal,
                Amount = request.Amount,
                Reference = request.Note,
                Debtor = new Counterparty
                {
                    Identifier = $"ID:{externalSavingAccounts[CurrentUser.UserId!.Value]}"
                },
                Creditor = new Counterparty
                {
                    Identifier = $"ID:{externalSavingAccounts[recipient.Id]}",
                    Name = $"{recipient.FirstName} {recipient.LastName}"
                }
            });

            HandleError(result);

            return result.Data.Transfer.Id;
        }

        private void HandleError(GraphQLResponse<CreateAndSubmitTransferResponse> result)
        {
            if (result.Data.Transfer == null)
            {
                if (result.Errors.Any(x =>
                    x.Extensions.Code.Trim().Equals(InsufficientAccountBalanceKey, StringComparison.OrdinalIgnoreCase)))
                {
                    throw new ValidationException("amount", "Insufficient account balance.");
                }

                throw new ValidationException("command", result.Errors.FirstOrDefault()?.Message);
            }
        }
    }
}
