﻿namespace BNine.Application.Aggregates.Transfers.Commands.CreateInternalTransfer
{
    using BNine.Application.Abstractions;
    using MediatR;
    using Newtonsoft.Json;

    public class CreateInternalTransferCommand : MBanqSelfServiceUserRequest, IRequest<int>
    {
        [JsonProperty("phone")]
        public string Phone
        {
            get; set;
        }

        [JsonProperty("amount")]
        public decimal Amount
        {
            get; set;
        }

        [JsonProperty("note")]
        public string Note
        {
            get;
            set;
        }
    }
}
