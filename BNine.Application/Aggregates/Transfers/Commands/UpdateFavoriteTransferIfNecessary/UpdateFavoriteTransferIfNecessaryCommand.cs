﻿namespace BNine.Application.Aggregates.Transfers.Commands.UpdateFavoriteTransferIfNecessary;

using MediatR;

public class UpdateFavoriteTransferIfNecessaryCommand : IRequest<Unit>
{
    public int ClientId
    {
        get;
        set;
    }

    public string CreditorId
    {
        get;
        set;
    }



    public Guid CorrelationId
    {
        get;
        set;
    }
}
