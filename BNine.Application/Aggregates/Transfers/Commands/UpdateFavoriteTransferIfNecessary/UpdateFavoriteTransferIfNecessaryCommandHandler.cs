﻿namespace BNine.Application.Aggregates.Transfers.Commands.UpdateFavoriteTransferIfNecessary
{
    using System;
    using System.Threading.Tasks;
    using AutoMapper;
    using Interfaces;
    using Interfaces.Bank.Client;
    using MediatR;
    using Microsoft.EntityFrameworkCore;

    public class UpdateFavoriteTransferIfNecessaryCommandHandler : IRequestHandler<UpdateFavoriteTransferIfNecessaryCommand, Unit>
    {
        private readonly IBNineDbContext _dbContext;
        private readonly IBankSavingsTransactionsService _savingsTransactionsService;

        public UpdateFavoriteTransferIfNecessaryCommandHandler(
            IMediator mediator,
            IBNineDbContext dbContext,
            IBankSavingsTransactionsService savingsTransactionsService,
            IMapper mapper)
        {
            _dbContext = dbContext;
            _savingsTransactionsService = savingsTransactionsService;
        }

        public async Task<Unit> Handle(UpdateFavoriteTransferIfNecessaryCommand request, CancellationToken cancellationToken)
        {
            var referencedUser = await _dbContext
                .Users
                .Include(u => u.CurrentAccount)
                .FirstOrDefaultAsync(x => x.ExternalClientId == request.ClientId, cancellationToken);

            if (referencedUser == null)
            {
                return Unit.Value;
            }

            var existingFavorite = await _dbContext
                .FavoriteUserTransfers
                .FirstOrDefaultAsync(x =>
                    x.UserId == referencedUser.Id &&
                    x.CreditorIdentifier == request.CreditorId, cancellationToken);

            if (existingFavorite == null) { return Unit.Value; }
            var transfer = await _savingsTransactionsService
                .GetTransferByCorrelationId(referencedUser.CurrentAccount.ExternalId, request.CorrelationId);
            existingFavorite.TransferId = transfer.Id;
            existingFavorite.UpdatedAt = DateTime.Now;

            await _dbContext.SaveChangesAsync(cancellationToken);

            return Unit.Value;
        }
    }
}
