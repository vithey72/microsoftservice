﻿namespace BNine.Application.Aggregates.Transfers.Commands.CreateACHTransfer
{
    using System.Text.RegularExpressions;
    using BNine.ResourceLibrary;
    using FluentValidation;
    using Microsoft.Extensions.Localization;

    public class CreateACHTransferCommandValidator
    : AbstractValidator<CreateACHTransferCommand>
    {
        public CreateACHTransferCommandValidator(
            IStringLocalizer<SharedResource> sharedLocalizer
            )
        {
            RuleFor(x => x.OtpCode)
                .NotEmpty();

            RuleFor(x => x.DeviceDetails)
                .NotEmpty();

            RuleFor(x => x.Recipient.AccountNumber)
                .NotEmpty();

            RuleFor(x => x.Recipient.AccountNumber)
                .Must(IsValidAccountNumber)
                .WithMessage(sharedLocalizer["invalidAccountNumberValidationErrorMessage"].Value)
                ;
        }

        private bool IsValidAccountNumber(string accountNumber)
        {
            return Regex.IsMatch(accountNumber, @"^[0-9]{4,17}$");
        }
    }
}
