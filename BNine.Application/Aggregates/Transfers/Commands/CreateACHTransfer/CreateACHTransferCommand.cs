﻿namespace BNine.Application.Aggregates.Transfers.Commands.CreateACHTransfer;

using BNine.Application.Abstractions;
using CommonModels.Dialog;
using Enums;
using MediatR;
using Models;
using Newtonsoft.Json;

public class CreateACHTransferCommand
    : MBanqSelfServiceUserRequest
    , IRequest<EitherDialog<GenericSuccessDialog, GenericFailDialog>>
{
    [JsonProperty("amount")]
    public decimal Amount
    {
        get;
        set;
    }

    [JsonProperty("recipient")]
    public Recipient Recipient
    {
        get;
        set;
    }

    [JsonProperty("note")]
    public string Note
    {
        get;
        set;
    }

    [JsonProperty("otp")]
    public string OtpCode
    {
        get;
        set;
    }

    [JsonProperty("deviceDetails")]
    public DeviceDetails DeviceDetails
    {
        get;
        set;
    }

    [JsonProperty("accountType")]
    public AchAccountType AccountType
    {
        get;
        set;
    }
}
