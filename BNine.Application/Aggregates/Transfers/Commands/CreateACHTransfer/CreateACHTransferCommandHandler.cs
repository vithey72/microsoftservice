﻿namespace BNine.Application.Aggregates.Transfers.Commands.CreateACHTransfer
{
    using System;
    using System.Dynamic;
    using System.Linq;
    using System.Threading;
    using System.Threading.Tasks;
    using Abstractions;
    using Application.Models.MBanq;
    using Application.Models.MBanq.Transfers;
    using AutoMapper;
    using CommonModels.Dialog;
    using Constants;
    using Domain.Entities;
    using Domain.Entities.TransfersSecurityOTPs;
    using Domain.Entities.User;
    using Enums.Transfers;
    using Exceptions;
    using Interfaces;
    using Interfaces.Bank.Client;
    using MediatR;
    using Microsoft.EntityFrameworkCore;
    using Newtonsoft.Json;
    using ServiceBusEvents.Commands.PublishB9Event;
    using Models;
    using TransferErrorLog.Commands;

    public class CreateACHTransferCommandHandler
        : AbstractRequestHandler
        , IRequestHandler<CreateACHTransferCommand, EitherDialog<GenericSuccessDialog, GenericFailDialog>>
    {
        private const string InsufficientAccountBalanceKey = "INSUFFICIENT_ACCOUNT_BALANCE";

        private readonly IBankTransfersService _bankTransfersService;
        private readonly INotificationUsersService _notificationUsersService;
        private readonly IBankSavingAccountsService _bankSavingAccountsService;

        // not a good practice
        private EitherDialog<GenericSuccessDialog, GenericFailDialog> FailResultDialog
        {
            get;
            set;
        }

        public CreateACHTransferCommandHandler(
            IBankSavingAccountsService bankSavingAccountsService,
            INotificationUsersService notificationUsersService,
            IMediator mediator,
            IBNineDbContext dbContext,
            IMapper mapper,
            ICurrentUserService currentUser,
            IBankTransfersService bankTransfersService
            )
            : base(mediator, dbContext, mapper, currentUser)
        {
            _bankTransfersService = bankTransfersService;
            _notificationUsersService = notificationUsersService;
            _bankSavingAccountsService = bankSavingAccountsService;
        }

        public async Task<EitherDialog<GenericSuccessDialog, GenericFailDialog>> Handle(
            CreateACHTransferCommand request, CancellationToken cancellationToken)
        {
            // Validate OTP code

            var user = await DbContext.Users
                .Include(u => u.Devices)
                .FirstOrDefaultAsync(u => u.Id == CurrentUser.UserId.Value, cancellationToken);

            if (user == null)
            {
                throw new NotFoundException(nameof(User), CurrentUser.UserId);
            }

            var activeDevices = user.Devices.Where(x => x.DeletedAt == null).ToList();

            var validOtpCode = await ValidateTransferSecurityOtpCode(request, user, activeDevices);

            if (FailResultDialog?.IsSuccess == false)
            {
                return FailResultDialog;
            }

            var usedDevice = activeDevices.FirstOrDefault(x => x.ExternalDeviceId.ToString() == validOtpCode?.DeviceId);

            // Create and submit transfer

            var externalSavingAccounts = await DbContext.Users
                .Where(x => x.Id == CurrentUser.UserId)
                .Select(x => new { x.CurrentAccount.ExternalId })
                .FirstOrDefaultAsync(cancellationToken);

            if (externalSavingAccounts == null)
            {
                throw new NotFoundException(nameof(User), CurrentUser.UserId);
            }

            var result = await _bankTransfersService.CreateAndSubmitTransfer(request.MbanqAccessToken, request.Ip,
                new Transfer
                {
                    Type = TransferDirection.Credit,
                    ReceiverAccountType = request.AccountType,
                    PaymentType = TransferType.ACH,
                    Amount = request.Amount,
                    Reference = request.Note,
                    Debtor = new Counterparty { Identifier = $"ID:{externalSavingAccounts.ExternalId}" },
                    Creditor = BuildAchCounterparty(request.Recipient, request.Recipient.Address)
                });

            await HandleError(result);

            if (FailResultDialog?.IsSuccess == false)
            {
                return FailResultDialog;
            }

            var externalSavingAccount =
                await _bankSavingAccountsService.GetSavingAccountInfo(externalSavingAccounts.ExternalId,
                    request.MbanqAccessToken, request.Ip);

            await _notificationUsersService.SendACHDebitNotification(CurrentUser.UserId.Value,
                externalSavingAccount.AvailableBalance, request.Amount, request.Recipient.Name);

            // Log transfer details

            await DbContext.SaveChangesAsync(cancellationToken);

            await SendAchSentEventToDwh(request, usedDevice, user.Phone, result.Data.Transfer.Id);

            var successDialog = new EitherDialog<GenericSuccessDialog, GenericFailDialog>(
                new GenericSuccessDialog()
                {
                    Header = new GenericDialogHeaderViewModel()
                    {
                        Title = "Transfer money", Subtitle = "Delivery within 3 business days."
                    },
                    Body = new GenericDialogBodyViewModel()
                    {
                        Title = "Money successfully transferred", Subtitle = null, ButtonText = "DONE"
                    }
                });

            return FailResultDialog ?? successDialog;
        }

        private Counterparty BuildAchCounterparty(Recipient recipient, RecipientAddress address)
        {
            var counterparty = new Counterparty
            {
                Identifier = $"ACH://{recipient.RoutingNumber}/{recipient.AccountNumber}", Name = recipient.Name,
            };

            if (address == null)
            {
                return counterparty;
            }

            counterparty.Address = string.IsNullOrEmpty(address.Unit)
                ? address.Address
                : $"{address.Address}, Unit {address.Unit}";
            counterparty.City = address.City;
            counterparty.Country = address.State;
            counterparty.PostalCode = address.ZipCode;

            return counterparty;
        }

        private async Task<TransfersSecurityOTP> ValidateTransferSecurityOtpCode(CreateACHTransferCommand request,
            User user, List<Device> activeDevices)
        {
            var activeDeviceIds = activeDevices.Select(d => d.ExternalDeviceId);

            var validOtpCode = await DbContext.TransfersSecurityOTPs
                .FirstOrDefaultAsync(x =>
                    x.IsValid == true &&
                    x.UserId == CurrentUser.UserId &&
                    x.OperationType == TransferType.ACH &&
                    x.CreatedDate.AddMinutes(3) > DateTime.UtcNow &&
                    (activeDeviceIds.Contains(x.DeviceId) || x.PhoneNumber == user.Phone) &&
                    x.Value == request.OtpCode);

            if (validOtpCode is null)
            {
                FailResultDialog = new EitherDialog<GenericSuccessDialog, GenericFailDialog>(
                    new GenericFailDialog
                    {
                        ErrorCode = ApiResponseErrorCodes.OtpFailed,
                        Body = new GenericDialogBodyViewModel
                        {
                            Title = "Incorrect confirmation code", ButtonText = "GOT IT",
                        },
                    });
            }

            var payload = JsonConvert.SerializeObject(request, Formatting.None,
                new JsonSerializerSettings() { ReferenceLoopHandling = ReferenceLoopHandling.Ignore });

            validOtpCode?.Terminate(payload);

            return validOtpCode;
        }

        private async Task SendAchSentEventToDwh(CreateACHTransferCommand request, Device device,
            string userPhone,
            int transferId)
        {
            var deviceDetailsWithIp = AddIpToDeviceDetails(request.DeviceDetails);

            await Mediator.Send(new PublishB9EventCommand(
                new
                {
                    Details = deviceDetailsWithIp,
                    UserId = CurrentUser.UserId.Value,
                    DeviceId = device?.Id,
                    ExternalDeviceId = device?.ExternalDeviceId,
                    UserPhone = userPhone,
                    DeviceDetailsType = TransferType.ACH,
                    TransferId = transferId,
                }, ServiceBusTopics.B9DeviceDetails, ServiceBusEvents.B9Created));
        }

        private async Task HandleError(GraphQLResponse<CreateAndSubmitTransferResponse> result)
        {
            if (result.Data?.Transfer == null)
            {
                if (result.Errors != null && result.Errors.Any(x =>
                        x.Extensions.Code.Trim()
                            .Equals(InsufficientAccountBalanceKey, StringComparison.OrdinalIgnoreCase)))
                {
                    FailResultDialog = new EitherDialog<GenericSuccessDialog, GenericFailDialog>(
                        new GenericFailDialog
                        {
                            ErrorCode = ApiResponseErrorCodes.AccountBalance,
                            Body = new GenericDialogBodyViewModel
                            {
                                Title = "Insufficient account balance",
                                ButtonText = "DONE",
                            },
                        });
                }

                FailResultDialog = new EitherDialog<GenericSuccessDialog, GenericFailDialog>(
                    new GenericFailDialog
                    {
                        ErrorCode = ApiResponseErrorCodes.MBanqValidation,
                        Body = new GenericDialogBodyViewModel
                        {
                            Title = result.Errors?.FirstOrDefault()?.Message ?? "Unknown error",
                            ButtonText = "DONE",
                        },
                    });

                await SaveTransferErrorLogIntoDatabase(result);
            }
        }

        private async Task SaveTransferErrorLogIntoDatabase(GraphQLResponse<CreateAndSubmitTransferResponse> result)
        {
            await Mediator.Send(new AddTransferErrorLogCommand()
            {
                TransferNetworkType = TransferType.ACH,
                TransferDirection = TransferDirection.Debit,
                ExceptionHeader = FailResultDialog?.Fail?.Body?.Title,
                MbanqRawErrorPayload = result?.Errors != null ? JsonConvert.SerializeObject(result.Errors) : null
            });
        }

        private object AddIpToDeviceDetails(DeviceDetails deviceDetails)
        {
            dynamic details = deviceDetails?.Details == null
                ? new ExpandoObject()
                : JsonConvert.DeserializeObject<ExpandoObject>(JsonConvert.SerializeObject(deviceDetails?.Details));
            details.externalIpAddress = deviceDetails?.ExternalIpAddress;
            return details;
        }
    }
}
