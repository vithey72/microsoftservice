﻿namespace BNine.Application.Aggregates.Transfers.Commands.SaveACHTransfer
{
    using System.Linq;
    using System.Threading;
    using System.Threading.Tasks;
    using AutoMapper;
    using BNine.Application.Abstractions;
    using BNine.Application.Interfaces;
    using BNine.Domain.Entities.BankAccount;
    using BNine.Domain.Events.ACHCreditTransfers;
    using MediatR;

    public class SaveACHTransferCommandHandler
        : AbstractRequestHandler, IRequestHandler<SaveACHTransferCommand, Unit>
    {
        public SaveACHTransferCommandHandler(IMediator mediator, IBNineDbContext dbContext, IMapper mapper, ICurrentUserService currentUser) : base(mediator, dbContext, mapper, currentUser)
        {
        }

        public async Task<Unit> Handle(SaveACHTransferCommand request, CancellationToken cancellationToken)
        {
            var user = DbContext.Users
                .Where(x => x.CurrentAccount.ExternalId == request.CreditorAccountId)
                .FirstOrDefault();

            if (user == null)
            {
                return Unit.Value;
            }

            if (DbContext.ACHCreditTransfers.Any(x => x.ExternalId == request.ExternalTransferId))
            {
                return Unit.Value;
            }

            var transfer = new ACHCreditTransfer
            {
                Amount = request.Amount,
                UserId = user.Id,
                CreatedAt = request.CreatedAt,
                UpdatedAt = DateTime.Now,
                ExternalId = request.ExternalTransferId,
                Reference = request.Reference.Any() ? string.Join("; ", request.Reference) : null,
                SavingsAccountTransferExternalId = request.SavingsAccountTransferExternalId
            };
            transfer.DomainEvents.Add(new ACHCreditTransferAddedEvent(transfer));

            DbContext.ACHCreditTransfers.Add(transfer);

            await DbContext.SaveChangesAsync(cancellationToken);

            return Unit.Value;
        }
    }
}
