﻿namespace BNine.Application.Aggregates.Transfers.Commands.SaveACHTransfer
{
    using System;
    using System.Collections.Generic;
    using MediatR;

    public class SaveACHTransferCommand
        : IRequest<Unit>
    {
        public int CreditorAccountId
        {
            get;
        }

        public int ExternalTransferId
        {
            get;
        }

        public DateTime CreatedAt
        {
            get;
        }

        public decimal Amount
        {
            get;
        }

        public IEnumerable<string> Reference
        {
            get;
        }

        public int SavingsAccountTransferExternalId
        {
            get;
            set;
        }

        public SaveACHTransferCommand(int creditorAccountId, int externalTransferId, DateTime createdAt, decimal amount,
            IEnumerable<string> reference, int savingsAccountTransferExternalId)
        {
            CreditorAccountId = creditorAccountId;
            CreatedAt = createdAt;
            Amount = amount;
            ExternalTransferId = externalTransferId;
            Reference = reference;
            SavingsAccountTransferExternalId = savingsAccountTransferExternalId;
        }
    }
}
