﻿namespace BNine.Application.Aggregates.Transfers.Commands.CreatePushToExternalCardTransfer
{
    using System;
    using Newtonsoft.Json;

    public class PushToExternalCardResponse
    {
        [JsonProperty("id")]
        public int Id
        {
            get; set;
        }

        [JsonProperty("status")]
        public string Status
        {
            get; set;
        }

        [JsonProperty("createdAt")]
        public DateTime CreatedAt
        {
            get; set;
        }
    }
}
