﻿namespace BNine.Application.Aggregates.Transfers.Commands.CreatePushToExternalCardTransfer
{
    using System;
    using System.Linq;
    using System.Threading;
    using System.Threading.Tasks;
    using Application.Models.MBanq.Transfers;
    using AutoMapper;
    using BNine.Application.Abstractions;
    using BNine.Application.Aggregates.ExternalCards.Queries.GetExternalCardsLimits;
    using BNine.Application.Exceptions;
    using BNine.Application.Exceptions.ApiResponse;
    using BNine.Application.Interfaces;
    using BNine.Application.Interfaces.Bank.Client;
    using BNine.Application.Models.MBanq;
    using BNine.Constants;
    using BNine.Domain.Entities.BankAccount;
    using BNine.Domain.Entities.User;
    using BNine.Enums.Transfers;
    using BNine.ResourceLibrary;
    using Interfaces.Bank.Administration;
    using MediatR;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.Extensions.Localization;
    using Microsoft.Extensions.Logging;
    using Newtonsoft.Json;
    using TransferErrorLog.Commands;
    using IBankInternalCardsService = Interfaces.Bank.Client.IBankInternalCardsService;

    public class CreatePushToExternalCardTransferCommandHandler
        : AbstractRequestHandler, IRequestHandler<CreatePushToExternalCardTransferCommand, PushToExternalCardResponse>
    {
        private readonly IBankExternalCardsService _bankExternalCardsService;
        private IBankTransfersService BankTransfersService
        {
            get;
        }

        public INotificationUsersService NotificationUsersUsersService
        {
            get;
        }

        private Interfaces.Bank.Administration.IBankSavingAccountsService BankSavingAccountsService
        {
            get;
        }

        private IBankSavingsTransactionsService BankSavingsTransactionsService
        {
            get;
        }

        private IBankInternalCardsService BankInternalCardsService
        {
            get;
        }

        private IStringLocalizer<SharedResource> SharedLocalizer
        {
            get;
        }

        private ILogger<CreatePushToExternalCardTransferCommandHandler> Logger
        {
            get;
        }

        private IPushPullFromExternalCardErrorHandlingService TransferResponseHandleErrorService
        {
            get;
        }

        public CreatePushToExternalCardTransferCommandHandler(
            IBankSavingsTransactionsService bankSavingsTransactionsService,
            Interfaces.Bank.Administration.IBankSavingAccountsService bankSavingAccountsService,
            IBankTransfersService bankTransfersService,
            IMediator mediator,
            IBNineDbContext dbContext,
            IMapper mapper,
            ICurrentUserService currentUser,
            INotificationUsersService notificationUsersService,
            IBankInternalCardsService bankInternalCardsService,
            IBankExternalCardsService bankExternalCardsService,
            ILogger<CreatePushToExternalCardTransferCommandHandler> logger,
            IStringLocalizer<SharedResource> sharedLocalizer,
            IPushPullFromExternalCardErrorHandlingService transferResponseHandleErrorService
            ) : base(mediator, dbContext, mapper, currentUser)
        {
            _bankExternalCardsService = bankExternalCardsService;
            BankSavingAccountsService = bankSavingAccountsService;
            BankSavingsTransactionsService = bankSavingsTransactionsService;
            BankTransfersService = bankTransfersService;
            NotificationUsersUsersService = notificationUsersService;
            BankInternalCardsService = bankInternalCardsService;
            Logger = logger;
            SharedLocalizer = sharedLocalizer;
            TransferResponseHandleErrorService = transferResponseHandleErrorService;
        }

        public async Task<PushToExternalCardResponse> Handle(CreatePushToExternalCardTransferCommand request, CancellationToken cancellationToken)
        {
            var user = await DbContext.Users
                .Include(x => x.CurrentAccount)
                .Include(x => x.Devices)
                .Include(x => x.DebitCards)
                .Where(x => x.Id == CurrentUser.UserId)
                .FirstOrDefaultAsync(cancellationToken);

            if (user == null)
            {
                throw new NotFoundException(nameof(User), CurrentUser.UserId);
            }

            if (user.CurrentAccount.ExternalId == 0)
            {
                throw new NotFoundException(nameof(CurrentAccount), CurrentUser.UserId);
            }

            await MustNotExceedLimits(request, cancellationToken);

            var accountInfo = await BankSavingAccountsService.GetSavingAccountInfo(user.CurrentAccount.ExternalId);

            var result = await BankTransfersService.CreateAndSubmitTransfer(
                request.MbanqAccessToken,
                request.Ip,
                new Transfer
                {
                    Type = TransferDirection.Credit,
                    PaymentType = TransferType.CARD,
                    Amount = request.Amount,
                    Debtor = new Counterparty
                    {
                        Identifier = $"ACCOUNT:{accountInfo.AccountNumber}",
                        Name = $"{user.FirstName} {user.LastName}"
                    },
                    Creditor = new Counterparty
                    {
                        Identifier = $"EXTERNALCARD:{request.CardId}",
                        Name = $"{user.FirstName} {user.LastName}"
                    }
                }
            );

            if (result?.Errors?.Any() == true)
            {
                Logger.LogWarning("Push to external card by user {userId} error: {error}",
                    CurrentUser.UserId, JsonConvert.SerializeObject(result.Errors));
            }

            if (result.Data?.Transfer == null)
            {
                try
                {
                    TransferResponseHandleErrorService.HandlePushPullFromExternalCardErrors(result.Errors);

                    await SaveTransferErrorLogIntoDatabase(result, result.Errors.FirstOrDefault()?.Message);
                }
                catch (ApiResponseException ex)
                {
                    await SaveTransferErrorLogIntoDatabase(result, ex?.Message, ex);

                    throw;
                }
            }

            var transfer = await BankTransfersService.GetTransfer(result.Data.Transfer.Id, request.MbanqAccessToken, request.Ip);
            var accountInfoAfter = await BankSavingAccountsService.GetSavingAccountInfo(user.CurrentAccount.ExternalId);

            var externalCardDetails = await _bankExternalCardsService.GetExternalCard(
                user.ExternalClientId!.Value,
                request.CardId);

            await NotificationUsersUsersService.SendPushToExternalNotification(
                user.Id, externalCardDetails.LastDigits, request.Amount, accountInfoAfter.AccountBalance);

            return new PushToExternalCardResponse
            {
                Id = result.Data.Transfer.Id,
                Status = transfer.Status,
                CreatedAt = transfer.DateTime
            };
        }

        private async Task SaveTransferErrorLogIntoDatabase(GraphQLResponse<CreateAndSubmitTransferResponse> result,
            string header, Exception exception = null)
        {
            await Mediator.Send(new AddTransferErrorLogCommand()
            {
                TransferNetworkType = TransferType.CARD,
                TransferDirection = TransferDirection.Debit,
                ExceptionBody = exception != null ? JsonConvert.SerializeObject(exception) : null,
                ExceptionHeader = header,
                MbanqRawErrorPayload = result?.Errors != null ? JsonConvert.SerializeObject(result?.Errors) : null
            });
        }

        private async Task MustNotExceedLimits(CreatePushToExternalCardTransferCommand request, CancellationToken cancellationToken)
        {
            var limits = await Mediator.Send(new GetExternalCardLimitsQuery());

            if (request.Amount < limits.PushToExternalCardMinAmount)
            {
                throw new ApiResponseException(ApiResponseErrorCodes.OnetimeMinAmountExceeded, String.Format(SharedLocalizer["onetimeMinAmountExceededErrorMessage"], limits.PushToExternalCardMinAmount));
            }

            if (request.Amount > limits.PushToExternalCardMaxAmount)
            {
                throw new ApiResponseException(ApiResponseErrorCodes.OnetimeMaxAmountExceeded, String.Format(SharedLocalizer["onetimeMaxAmountExceededErrorMessage"], limits.PushToExternalCardMaxAmount));
            }
        }
    }
}
