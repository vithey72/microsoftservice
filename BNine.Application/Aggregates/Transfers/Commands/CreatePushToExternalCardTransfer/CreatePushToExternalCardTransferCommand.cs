﻿namespace BNine.Application.Aggregates.Transfers.Commands.CreatePushToExternalCardTransfer
{
    using Abstractions;
    using MediatR;
    using Newtonsoft.Json;

    public class CreatePushToExternalCardTransferCommand
        : MBanqSelfServiceUserRequest, IRequest<PushToExternalCardResponse>
    {
        [JsonProperty("cardId")]
        public int CardId
        {
            get; set;
        }

        [JsonProperty("amount")]
        public decimal Amount
        {
            get; set;
        }
    }
}
