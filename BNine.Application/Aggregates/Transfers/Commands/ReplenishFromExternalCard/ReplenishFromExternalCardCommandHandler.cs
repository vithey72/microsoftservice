﻿namespace BNine.Application.Aggregates.Transfers.Commands.ReplenishFromExternalCard
{
    using System;
    using System.Linq;
    using System.Threading;
    using System.Threading.Tasks;
    using Application.Models.MBanq.Transfers;
    using AutoMapper;
    using BNine.Application.Abstractions;
    using BNine.Application.Aggregates.ExternalCards.Queries.GetExternalCardsLimits;
    using BNine.Application.Aggregates.Transfers.Models;
    using BNine.Application.Exceptions;
    using BNine.Application.Exceptions.ApiResponse;
    using BNine.Application.Interfaces;
    using BNine.Application.Interfaces.Bank.Client;
    using BNine.Application.Models.MBanq;
    using BNine.Constants;
    using BNine.Domain.Entities.BankAccount;
    using BNine.Domain.Entities.User;
    using BNine.Enums.Transfers;
    using BNine.ResourceLibrary;
    using MediatR;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.Extensions.Localization;
    using Microsoft.Extensions.Logging;
    using Newtonsoft.Json;
    using TransferErrorLog.Commands;

    public class ReplenishFromExternalCardCommandHandler
        : AbstractRequestHandler, IRequestHandler<ReplenishFromExternalCardCommand, ExternalCardTransferInfo>
    {
        private const string INSUFFICIENT_CARD_BALANCE = "INSUFFICIENT_CARD_BALANCE";
        private const string SAVING_ACCOUNT_BLOCKED = "error.msg.saving.account.blocked.transaction.not.allowed";
        private const string TRANSFER_REACH_LIMIT_CODE = "error.msg.transfer.reach.limit";

        private IBankTransfersService BankTransfersService
        {
            get;
        }
        public ILogger<ReplenishFromExternalCardCommandHandler> Logger
        {
            get;
        }
        private Interfaces.Bank.Administration.IBankSavingAccountsService BankSavingAccountsService
        {
            get;
        }

        private IBankSavingsTransactionsService BankSavingsTransactionsService
        {
            get;
        }

        private IStringLocalizer<SharedResource> SharedLocalizer
        {
            get;
        }

        private IPushPullFromExternalCardErrorHandlingService TransferResponseHandleErrorService
        {
            get;
        }

        public ReplenishFromExternalCardCommandHandler(
            IStringLocalizer<SharedResource> sharedLocalizer,
            IBankSavingsTransactionsService bankSavingsTransactionsService,
            Interfaces.Bank.Administration.IBankSavingAccountsService bankSavingAccountsService,
            IBankTransfersService bankTransfersService,
            IMediator mediator, IBNineDbContext dbContext,
            IMapper mapper,
            ICurrentUserService currentUser,
            ILogger<ReplenishFromExternalCardCommandHandler> logger,
            IPushPullFromExternalCardErrorHandlingService transferResponseHandleErrorService
            )
            : base(mediator, dbContext, mapper, currentUser)
        {
            BankTransfersService = bankTransfersService;
            Logger = logger;
            BankSavingAccountsService = bankSavingAccountsService;
            BankSavingsTransactionsService = bankSavingsTransactionsService;
            SharedLocalizer = sharedLocalizer;
            TransferResponseHandleErrorService = transferResponseHandleErrorService;
        }

        public async Task<ExternalCardTransferInfo> Handle(ReplenishFromExternalCardCommand request, CancellationToken cancellationToken)
        {
            var user = await DbContext.Users
                .Where(x => x.Id == CurrentUser.UserId)
                .Select(x => new
                {
                    x.ExternalClientId,
                    x.FirstName,
                    x.LastName,
                    AccountId = x.CurrentAccount.ExternalId
                }).FirstOrDefaultAsync(cancellationToken);

            if (user == null)
            {
                throw new NotFoundException(nameof(User), CurrentUser.UserId);
            }

            if (user.AccountId == 0)
            {
                throw new NotFoundException(nameof(CurrentAccount), CurrentUser.UserId);
            }

            await MustNotExceedLimits(request);

            var accountInfo = await BankSavingAccountsService.GetSavingAccountInfo(user.AccountId);

            var result = await BankTransfersService.CreateAndSubmitTransfer(
                request.MbanqAccessToken,
                request.Ip,
                new Transfer
                {
                    Type = TransferDirection.Debit,
                    PaymentType = TransferType.CARD,
                    Amount = request.Amount,
                    Debtor = new Counterparty
                    {
                        Identifier = $"EXTERNALCARD:{request.CardId}",
                        Name = $"{user.FirstName} {user.LastName}"
                    },
                    Creditor = new Counterparty
                    {
                        Identifier = $"ACCOUNT:{accountInfo.AccountNumber}",
                        Name = $"{user.FirstName} {user.LastName}"
                    }
                }
            );


            if (result?.Errors?.Any() == true)
            {
                Logger.LogWarning("Pull from external card error: {error}", JsonConvert.SerializeObject(result.Errors));
            }

            if (result?.Data?.Transfer == null)
            {
                try
                {
                    TransferResponseHandleErrorService.HandlePushPullFromExternalCardErrors(result.Errors);
                }
                catch (ApiResponseException ex)
                {
                    await SaveTransferErrorLogIntoDatabase(ex, result);

                    throw;
                }
            }

            var transfer = await BankTransfersService.GetTransfer(result.Data.Transfer.Id, request.MbanqAccessToken, request.Ip);

            return new ExternalCardTransferInfo
            {
                Id = result.Data.Transfer.Id,
                Status = transfer.Status,
                CreatedAt = transfer.DateTime
            };
        }

        private async Task SaveTransferErrorLogIntoDatabase(ApiResponseException ex,
            GraphQLResponse<CreateAndSubmitTransferResponse> result)
        {
            await Mediator.Send(new AddTransferErrorLogCommand()
            {
                TransferNetworkType = TransferType.CARD,
                TransferDirection = TransferDirection.Credit,
                ExceptionBody = ex != null ? JsonConvert.SerializeObject(ex) : null,
                ExceptionHeader = ex?.Message,
                MbanqRawErrorPayload = result?.Errors != null ? JsonConvert.SerializeObject(result?.Errors) : null
            });
        }

        private async Task MustNotExceedLimits(ReplenishFromExternalCardCommand request)
        {
            var limits = await Mediator.Send(new GetExternalCardLimitsQuery());

            if (request.Amount > limits.PullFromExternalCardMaxAmount)
            {
                throw new ApiResponseException(ApiResponseErrorCodes.OnetimeMaxAmountExceeded, String.Format(SharedLocalizer["onetimeMaxAmountExceededErrorMessage"], limits.PushToExternalCardMaxAmount));
            }

            if (request.Amount < limits.PullFromExternalCardMinAmount)
            {
                throw new ApiResponseException(ApiResponseErrorCodes.OnetimeMinAmountExceeded, String.Format(SharedLocalizer["onetimeMinAmountExceededErrorMessage"], limits.PushToExternalCardMinAmount));
            }
        }
    }
}
