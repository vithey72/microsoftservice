﻿namespace BNine.Application.Aggregates.Transfers.Commands.ReplenishFromExternalCard
{
    using BNine.Application.Abstractions;
    using BNine.Application.Aggregates.Transfers.Models;
    using MediatR;
    using Newtonsoft.Json;

    public class ReplenishFromExternalCardCommand
        : MBanqSelfServiceUserRequest, IRequest<ExternalCardTransferInfo>
    {
        [JsonProperty("cardId")]
        public int CardId
        {
            get; set;
        }

        [JsonProperty("amount")]
        public decimal Amount
        {
            get; set;
        }
    }
}
