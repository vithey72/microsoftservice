﻿namespace BNine.Application.Aggregates.Transfers.Commands.SendTransferSecurityOTP;

using Enums.Transfers;
using MediatR;
using Newtonsoft.Json;

public class SendTransferSecurityOtpSmsCommand : IRequest<Unit>
{
    [JsonProperty("opType")]
    public TransferType OperationType
    {
        get;
        set;
    }

    [JsonIgnore]
    public string ExternalIpAddress
    {
        get;
        set;
    }
}
