﻿namespace BNine.Application.Aggregates.Transfers.Commands.SendTransferSecurityOTP;

using FluentValidation;

public class SendTransferSecurityOtpSmsCommandValidator : AbstractValidator<SendTransferSecurityOtpSmsCommand>
{
    public SendTransferSecurityOtpSmsCommandValidator()
    {
        RuleFor(x => x.OperationType).NotEmpty();
    }
}
