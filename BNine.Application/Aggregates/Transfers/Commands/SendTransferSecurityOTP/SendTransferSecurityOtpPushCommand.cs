﻿namespace BNine.Application.Aggregates.Transfers.Commands.SendTransferSecurityOTP;

using Enums.Transfers;
using MediatR;
using Newtonsoft.Json;

public class SendTransferSecurityOtpPushCommand : IRequest<Unit>
{
    [JsonProperty("deviceIdDigits")]
    public string DeviceIdDigits
    {
        get;
        set;
    }

    [JsonProperty("opType")]
    public TransferType OperationType
    {
        get;
        set;
    }

    [JsonIgnore]
    public string ExternalIpAddress
    {
        get;
        set;
    }
}
