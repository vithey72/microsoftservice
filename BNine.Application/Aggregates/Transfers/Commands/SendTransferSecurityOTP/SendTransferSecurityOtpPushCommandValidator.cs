﻿namespace BNine.Application.Aggregates.Transfers.Commands.SendTransferSecurityOTP;

using FluentValidation;

public class SendTransferSecurityOtpPushCommandValidator : AbstractValidator<SendTransferSecurityOtpPushCommand>
{
    public SendTransferSecurityOtpPushCommandValidator()
    {
        RuleFor(x => x.OperationType).NotEmpty();
        RuleFor(x => x.DeviceIdDigits).NotEmpty();
    }
}
