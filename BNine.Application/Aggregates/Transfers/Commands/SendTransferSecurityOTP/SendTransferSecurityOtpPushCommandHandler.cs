﻿namespace BNine.Application.Aggregates.Transfers.Commands.SendTransferSecurityOTP;

using Abstractions;
using AutoMapper;
using Common;
using Domain.Entities;
using Domain.Entities.TransfersSecurityOTPs;
using Domain.Entities.User;
using Exceptions;
using Extensions;
using Helpers;
using Interfaces;
using MediatR;
using Microsoft.EntityFrameworkCore;

public class SendTransferSecurityOtpPushCommandHandler
    : AbstractRequestHandler
    , IRequestHandler<SendTransferSecurityOtpPushCommand, Unit>
{
    private readonly INotificationUsersService _notificationUsersService;
    private readonly IPartnerProviderService _partnerProvider;

    public SendTransferSecurityOtpPushCommandHandler(
        IMediator mediator,
        IBNineDbContext dbContext,
        IMapper mapper,
        ICurrentUserService currentUser,
        INotificationUsersService notificationUsersService,
        IPartnerProviderService partnerProvider
        ) : base(mediator, dbContext, mapper, currentUser)
    {
        _partnerProvider = partnerProvider;
        _notificationUsersService = notificationUsersService;
    }


    public async Task<Unit> Handle(SendTransferSecurityOtpPushCommand request, CancellationToken cancellationToken)
    {
        var user = await DbContext.Users
            .Include(u => u.Devices)
            .FirstOrDefaultAsync(u => u.Id == CurrentUser.UserId.Value, cancellationToken);

        if (user == null)
        {
            throw new NotFoundException(nameof(User), CurrentUser.UserId);
        }

        var currentDevice = user.Devices.FirstOrDefault(x =>
            x.ExternalDeviceId.Contains(request.DeviceIdDigits, StringComparison.OrdinalIgnoreCase));

        if (currentDevice is null)
        {
            throw new NotFoundException(nameof(Device));
        }

        var otpCode = CodeGenerator.GenerateCode();

        var notificationText =
            $"Your code for {StringProvider.GetPartnerName(_partnerProvider)} " +
            $"operation {EnumExtensions.GetEnumMemberValue(request.OperationType)} " +
            $"is {otpCode}";

        await SendPushNotification(user, currentDevice, notificationText, otpCode);

        await DbContext.TransfersSecurityOTPs.AddAsync(new TransfersSecurityOTP()
        {
            UserId = user.Id,
            Value = otpCode,
            CreatedDate = DateTime.UtcNow,
            DeviceId = currentDevice.ExternalDeviceId,
            IsValid = true,
            OperationType = request.OperationType,
        });

        await DbContext.SaveChangesAsync(cancellationToken);

        return Unit.Value;
    }

    private async Task SendPushNotification(User user, Device device, string notificationText, string otpCode)
    {
        await _notificationUsersService.SendStealthyPushNotification(
            user, device, notificationText, "otp-code", otpCode);
    }
}
