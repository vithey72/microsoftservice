﻿namespace BNine.Application.Aggregates.Transfers.Commands.SendTransferSecurityOTP;

using Abstractions;
using AutoMapper;
using BNine.Application.Helpers;
using Common;
using Domain.Entities.TransfersSecurityOTPs;
using Domain.Entities.User;
using Exceptions;
using Extensions;
using Interfaces;
using MediatR;
using Microsoft.EntityFrameworkCore;

public class SendTransferSecurityOtpSmsCommandHandler
    : AbstractRequestHandler
    , IRequestHandler<SendTransferSecurityOtpSmsCommand, Unit>
{
    private ISmsService _smsService;
    private readonly IPartnerProviderService _partnerProvider;

    public SendTransferSecurityOtpSmsCommandHandler(
        IMediator mediator,
        IBNineDbContext dbContext,
        IMapper mapper,
        ICurrentUserService currentUser,
        ISmsService smsService,
        IPartnerProviderService partnerProvider
        )
        : base(mediator, dbContext, mapper, currentUser)
    {
        _smsService = smsService;
        _partnerProvider = partnerProvider;
    }


    public async Task<Unit> Handle(SendTransferSecurityOtpSmsCommand request, CancellationToken cancellationToken)
    {
        var user = await DbContext.Users
            .Include(u => u.Devices)
            .FirstOrDefaultAsync(u => u.Id == CurrentUser.UserId.Value, cancellationToken);

        if (user == null)
        {
            throw new NotFoundException(nameof(User), CurrentUser.UserId);
        }

        var otpCode = CodeGenerator.GenerateCode();

        var notificationText =
            $"Your code for {StringProvider.GetPartnerName(_partnerProvider)} " +
            $"operation {EnumExtensions.GetEnumMemberValue(request.OperationType)} " +
            $"is {otpCode}";

        await SendSmsNotification(user, notificationText);

        await DbContext.TransfersSecurityOTPs.AddAsync(new TransfersSecurityOTP()
        {
            UserId = user.Id,
            Value = otpCode,
            CreatedDate = DateTime.UtcNow,
            DeviceId = null,
            IsValid = true,
            OperationType = request.OperationType,
            PhoneNumber = user.Phone
        });

        await DbContext.SaveChangesAsync(cancellationToken);

        return Unit.Value;
    }

    private async Task SendSmsNotification(User user, string notificationText)
    {
        var smsBody = $"<#>{notificationText}\n\nlp+o6v3RRZY";
        await _smsService.SendMessageAsync(user.Phone, smsBody);
    }
}
