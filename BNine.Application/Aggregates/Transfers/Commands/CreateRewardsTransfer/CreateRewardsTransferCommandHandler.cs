﻿#nullable enable
namespace BNine.Application.Aggregates.Transfers.Commands.CreateRewardsTransfer;

using Application.Models.MBanq;
using Application.Models.MBanq.Transfers;
using CommonModels.Dialog;
using Enums;
using Enums.Transfers;
using Helpers;
using Interfaces;
using Interfaces.Bank.Client;
using MediatR;
using Microsoft.EntityFrameworkCore;

public class CreateRewardsTransferCommandHandler : IRequestHandler<CreateRewardsTransferCommand, EitherDialog<GenericSuccessDialog, GenericFailDialog>>
{
    private readonly IBankTransfersService _bankTransfersService;
    private readonly IBNineDbContext _dbContext;
    private readonly IMediator _mediator;

    public CreateRewardsTransferCommandHandler(
        IBankTransfersService bankTransfersService,
        IBNineDbContext dbContext,
        IMediator mediator)
    {
        _bankTransfersService = bankTransfersService;
        _dbContext = dbContext;
        _mediator = mediator;
    }

    public async Task<EitherDialog<GenericSuccessDialog, GenericFailDialog>> Handle(CreateRewardsTransferCommand request, CancellationToken cancellationToken)
    {
        var userAccounts = await _dbContext.Users
            .Include(u => u.CurrentAccount)
            .Include(u => u.CurrentAdditionalAccounts)
            .Where(u => u.Id == request.UserId)
            .Select(u => new { u.CurrentAccount, u.CurrentAdditionalAccounts })
            .FirstOrDefaultAsync(cancellationToken);

        if (userAccounts == null)
        {
            throw new InvalidOperationException($"User with Id {request.UserId} does not exist");
        }

        var mainAcc = userAccounts.CurrentAccount;
        var rewardsAcc =
            userAccounts.CurrentAdditionalAccounts.FirstOrDefault(acc =>
                acc.AccountType == CurrentAdditionalAccountEnum.Reward);

        if (rewardsAcc == null)
        {
            throw new InvalidOperationException("User does not have a rewards account");
        }

        var transfer = new Transfer()
        {
            Type = TransferDirection.Credit,
            PaymentType = TransferType.Internal,
            Amount = request.Amount,
            Currency = "USD",
            Debtor = new Counterparty { Identifier = $"ID:{rewardsAcc.ExternalId}", },
            Creditor = new Counterparty { Identifier = $"ID:{mainAcc.ExternalId}", }
        };

        var response =
            await _bankTransfersService.CreateAndSubmitTransfer(request.MBanqToken, request.IpAddress, transfer);

        var errorModel = response.GetErrorModelIfAnyErrorsHappened(propagateToTitle: false);

        if (errorModel != null)
        {
            var errorLogCommand = response.CreateErrorLogCommand(transfer);
            await _mediator.Send(errorLogCommand, CancellationToken.None);
            return errorModel;
        }

        return new EitherDialog<GenericSuccessDialog, GenericFailDialog>(new GenericSuccessDialog()
        {
            Header = new GenericDialogHeaderViewModel()
            {
                Title = "Redeem for cash"
            },
            Body = new GenericDialogBodyViewModel()
            {
                Subtitle = "The money has been successfully \nwithdrawn from your Rewards Account",
                ButtonText = "DONE"
            },
        });
    }
}
