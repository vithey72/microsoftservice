﻿namespace BNine.Application.Aggregates.Transfers.Commands.CreateRewardsTransfer;

using CommonModels.Dialog;
using MediatR;
using Newtonsoft.Json;

public class CreateRewardsTransferCommand : IRequest<EitherDialog<GenericSuccessDialog, GenericFailDialog>>
{
    [JsonIgnore]
    public Guid UserId
    {
        get;
        set;
    }

    [JsonIgnore]
    public string MBanqToken
    {
        get;
        set;
    }

    [JsonIgnore]
    public string IpAddress
    {
        get;
        set;
    }

    public decimal Amount
    {
        get;
        set;
    }
}

