﻿namespace BNine.Application.Aggregates.Transfers.Commands.InitiateLocalTransferCommand
{
    using System.Threading;
    using System.Threading.Tasks;
    using AutoMapper;
    using BNine.Application.Abstractions;
    using BNine.Application.Interfaces;
    using Interfaces.Bank.Client;
    using MediatR;

    public class InitiateLocalTransferCommandHandler
        : AbstractRequestHandler
        , IRequestHandler<InitiateLocalTransferCommand, int>
    {

        private IBankTransfersService BankTransfersService
        {
            get;
        }

        public InitiateLocalTransferCommandHandler(
            IMediator mediator,
            IBNineDbContext dbContext,
            IMapper mapper,
            ICurrentUserService currentUser,
            IBankTransfersService transfersService
            )
            : base(mediator, dbContext, mapper, currentUser)
        {
            BankTransfersService = transfersService;
        }

        public Task<int> Handle(InitiateLocalTransferCommand request, CancellationToken cancellationToken)
        {

            //var transferId = await BankTransfersService.CreateLocalTransfer(request);

            //var response = await BankTransfersService.ExecuteTransfer(transferId);

            //return transferId;
            throw new System.NotImplementedException();
        }
    }
}
