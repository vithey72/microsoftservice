﻿namespace BNine.Application.Aggregates.Transfers.Commands.InitiateLocalTransferCommand
{
    using System.Collections.Generic;
    using MediatR;
    using Newtonsoft.Json;

    public class InitiateLocalTransferCommand : IRequest<int>
    {
        [JsonProperty("type")]
        public string Type
        {
            get; set;
        }

        [JsonProperty("dateFormat")]
        public string DateFormat
        {
            get; set;
        }

        [JsonProperty("externalId")]
        public string ExternalId
        {
            get; set;
        }

        [JsonProperty("amount")]
        public int Amount
        {
            get; set;
        }

        [JsonProperty("reference")]
        public List<string> Reference
        {
            get; set;
        }

        [JsonProperty("debtor")]
        public Debtor TransferDebtor
        {
            get; set;
        }

        [JsonProperty("creditor")]
        public Creditor TransferCreditor
        {
            get; set;
        }
    }

    public class Debtor
    {
        [JsonProperty("identifier")]
        public string Identifier
        {
            get; set;
        }
    }

    public class Creditor
    {
        [JsonProperty("identifier")]
        public string Identifier
        {
            get; set;
        }

        [JsonProperty("name")]
        public string Name
        {
            get; set;
        }
    }
}
