﻿namespace BNine.Application.Aggregates.Transfers.Commands.ReplenishFromPlaidLinkedAccountCommand
{
    using System;
    using BNine.Application.Abstractions;
    using MediatR;
    using Newtonsoft.Json;

    public class ReplenishFromPlaidLinkedAccountCommand : MBanqSelfServiceUserRequest, IRequest<int>
    {
        [JsonProperty("accountId")]
        public Guid AccountId
        {
            get; set;
        }

        [JsonProperty("amount")]
        public decimal Amount
        {
            get; set;
        }
    }
}
