﻿namespace BNine.Application.Aggregates.Transfers.Commands.ReplenishFromPlaidLinkedAccountCommand
{
    using FluentValidation;

    public class ReplenishFromPlaidLinkedAccountCommandValidator : AbstractValidator<ReplenishFromPlaidLinkedAccountCommand>
    {
        private const int MaxAmount = 1000;

        public ReplenishFromPlaidLinkedAccountCommandValidator()
        {
            RuleFor(x => x.Amount).LessThanOrEqualTo(MaxAmount)
                .WithMessage($"Please enter amount below {MaxAmount}");
        }
    }
}
