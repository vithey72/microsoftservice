﻿namespace BNine.Application.Aggregates.Transfers.Commands.ReplenishFromPlaidLinkedAccountCommand
{
    using System;
    using System.Linq;
    using System.Threading;
    using System.Threading.Tasks;
    using Application.Models.MBanq.Transfers;
    using AutoMapper;
    using BNine.Application.Abstractions;
    using BNine.Application.Exceptions;
    using BNine.Application.Extensions;
    using BNine.Application.Extensions.IQueryableExtensions;
    using BNine.Application.Interfaces;
    using BNine.Application.Interfaces.Bank.Client;
    using BNine.Application.Interfaces.Plaid;
    using BNine.Application.Models.MBanq;
    using BNine.Domain.Entities;
    using BNine.Domain.Entities.BankAccount;
    using BNine.Enums.Transfers;
    using MediatR;
    using Microsoft.EntityFrameworkCore;

    public class ReplenishFromPlaidLinkedAccountCommandHandler
        : AbstractRequestHandler, IRequestHandler<ReplenishFromPlaidLinkedAccountCommand, int>
    {
        private const string InsufficientAccountBalanceKey = "INSUFFICIENT_ACCOUNT_BALANCE";

        private IBankTransfersService BankTransfersService
        {
            get;
        }

        private IPlaidAccountsService PlaidAccountsService
        {
            get;
        }

        public ReplenishFromPlaidLinkedAccountCommandHandler(IPlaidAccountsService plaidAccountsService, IBankTransfersService bankTransfersService, IMediator mediator, IBNineDbContext dbContext, IMapper mapper, ICurrentUserService currentUser) : base(mediator, dbContext, mapper, currentUser)
        {
            BankTransfersService = bankTransfersService;
            PlaidAccountsService = plaidAccountsService;
        }

        public async Task<int> Handle(ReplenishFromPlaidLinkedAccountCommand request, CancellationToken cancellationToken)
        {
            throw new Exception("Sorry, this method is not available now");
            var user = await DbContext.Users
                .Where(x => x.Id == CurrentUser.UserId)
                .Select(x => new
                {
                    ExternalSavingAccountId = (int?)x.CurrentAccount.ExternalId,
                    x.FirstName,
                    x.LastName
                })
                .FirstOrDefaultAsync();

            if (user?.ExternalSavingAccountId == null)
            {
                throw new NotFoundException(nameof(CurrentAccount), CurrentUser.UserId);
            }

            var linkedAccount = await DbContext.BankAccounts
                .NotRemoved()
                .Where(x => x.Id == request.AccountId && x.UserId == CurrentUser.UserId)
                .FirstOrDefaultAsync(cancellationToken);

            if (linkedAccount == null)
            {
                throw new NotFoundException(nameof(ExternalBankAccount), request.AccountId);
            }

            await MustNotExceedFrequencyOfPayments(linkedAccount, cancellationToken);

            await MustNotExceedAvailableBalance(request, linkedAccount, cancellationToken);

            var result = await BankTransfersService.CreateAndSubmitTransfer(request.MbanqAccessToken, request.Ip, new Transfer
            {
                Type = TransferDirection.Debit,
                PaymentType = TransferType.ACH,
                Amount = request.Amount,
                Debtor = new Counterparty
                {
                    Identifier = $"ACH://{linkedAccount.RoutingNumber}/{linkedAccount.AccountNumber}",
                    Name = $"{user.FirstName} {user.LastName}"
                },
                Creditor = new Counterparty
                {
                    Identifier = $"ID:{user.ExternalSavingAccountId}",
                    Name = $"{user.FirstName} {user.LastName}"
                }
            });

            HandleError(result);

            DbContext.ExternalBankAccountTransfers.Add(new ExternalBankAccountTransfer
            {
                CreatedAt = DateTime.UtcNow,
                ExternalBankAccountId = linkedAccount.Id,
                ExternalTransferId = result.Data.Transfer.Id
            });

            await DbContext.SaveChangesAsync(cancellationToken);

            return result.Data.Transfer.Id;
        }

        private async Task MustNotExceedFrequencyOfPayments(ExternalBankAccount linkedAccount, CancellationToken cancellationToken)
        {
            var date = DateTime.Now.AddWorkDays(-3);
            var isExceededLimit = await DbContext.ExternalBankAccountTransfers
                .Where(x => x.ExternalBankAccountId == linkedAccount.Id)
                .Where(x => x.CreatedAt > date)
                .AnyAsync(cancellationToken);

            if (isExceededLimit)
            {
                throw new ValidationException("account", "1 deposit allowed every 3 business days. Please try again later.");
            }
        }

        private async Task MustNotExceedAvailableBalance(ReplenishFromPlaidLinkedAccountCommand request, ExternalBankAccount linkedAccount, CancellationToken cancellationToken)
        {
            if (string.IsNullOrEmpty(linkedAccount.ExternalAccountId))
            {
                return;
            }

            if (string.IsNullOrEmpty(linkedAccount.AccessToken))
            {
                return;
            }

            var balance = await PlaidAccountsService.GetBalance(linkedAccount.AccessToken, linkedAccount.ExternalAccountId);

            if (balance == null)
            {
                throw new NotFoundException(nameof(ExternalBankAccountBalance), linkedAccount.ExternalAccountId);
            }

            DbContext.ExternalBankAccountBalances.Add(new ExternalBankAccountBalance
            {
                ExternalBankAccountId = linkedAccount.Id,
                Currency = balance.Currency,
                CurrentAmount = balance.Current,
                AvailableAmount = balance.Available
            });

            await DbContext.SaveChangesAsync(cancellationToken);

            if (balance.Currency != "USD")
            {
                return;
            }

            if (!balance.Available.HasValue && !balance.Current.HasValue)
            {
                return;
            }

            var limit = balance.Available ?? balance.Current.Value;

            if (request.Amount > limit)
            {
                throw new ValidationException("amount", "Insufficient account balance.");
            }
        }

        private void HandleError(GraphQLResponse<CreateAndSubmitTransferResponse> result)
        {
            if (result.Data.Transfer == null)
            {
                if (result.Errors.Any(x =>
                    x.Extensions.Code.Trim().Equals(InsufficientAccountBalanceKey, StringComparison.OrdinalIgnoreCase)))
                {
                    throw new ValidationException("amount", "Insufficient account balance.");
                }

                throw new ValidationException("command", result.Errors.FirstOrDefault()?.Message);
            }
        }
    }
}
