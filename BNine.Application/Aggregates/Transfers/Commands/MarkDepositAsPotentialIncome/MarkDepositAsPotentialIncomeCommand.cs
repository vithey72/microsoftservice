﻿namespace BNine.Application.Aggregates.Transfers.Commands.MarkDepositAsPotentialIncome;

using Abstractions;
using CommonModels.Dialog;
using MediatR;

public class MarkDepositAsPotentialIncomeCommand
    : MBanqSelfServiceUserRequest
    , IRequest<GenericSuccessFlagResponse>
{
    public int TransferId
    {
        get;
        set;
    }
}
