﻿namespace BNine.Application.Aggregates.Transfers.Commands.MarkDepositAsPotentialIncome;

using System;
using System.Threading.Tasks;
using Abstractions;
using AutoMapper;
using CommonModels.Dialog;
using Domain.Entities;
using Enums.Transfers;
using Interfaces;
using Interfaces.Bank.Client;
using MediatR;
using Microsoft.EntityFrameworkCore;

public class MarkDepositAsPotentialIncomeCommandHandler
    : AbstractRequestHandler
    , IRequestHandler<MarkDepositAsPotentialIncomeCommand, GenericSuccessFlagResponse>
{
    private readonly IBankSavingsTransactionsService _savingsTransactionsService;

    public MarkDepositAsPotentialIncomeCommandHandler(
        IMediator mediator,
        IBNineDbContext dbContext,
        IMapper mapper,
        ICurrentUserService currentUser,
        IBankSavingsTransactionsService savingsTransactionsService
        )
        : base(mediator, dbContext, mapper, currentUser)
    {
        _savingsTransactionsService = savingsTransactionsService;
    }

    public async Task<GenericSuccessFlagResponse> Handle(MarkDepositAsPotentialIncomeCommand request, CancellationToken cancellationToken)
    {
        var transfer = await _savingsTransactionsService
                .GetSavingsAccountTransfer(request.TransferId, request.MbanqAccessToken, request.Ip);

        if (transfer.TransferDisplayType is not
            TransferDisplayType.AchReceived and not
            TransferDisplayType.ReceivedTransferToCard)
        {
            throw new InvalidOperationException("The transfer passed in not an incoming ACH or card transfer.");
        }

        var entryExists = await DbContext
            .PotentialAchIncomeTransfers
            .AnyAsync(x => x.TransferExternalId == request.TransferId, cancellationToken);

        if (entryExists)
        {
            return new GenericSuccessFlagResponse(false);
        }

        var newEntry = new PotentialAchIncomeTransfer
        {
            TransferExternalId = request.TransferId,
            CreatedAt = DateTime.UtcNow,
            Reference = transfer.Note ?? string.Empty,
            UserId = CurrentUser.UserId!.Value,
        };

        await DbContext.PotentialAchIncomeTransfers.AddAsync(newEntry, cancellationToken);
        await DbContext.SaveChangesAsync(cancellationToken);

        return new GenericSuccessFlagResponse(true);
    }
}
