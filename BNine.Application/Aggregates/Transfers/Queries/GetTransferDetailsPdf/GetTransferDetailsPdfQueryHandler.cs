﻿namespace BNine.Application.Aggregates.Transfers.Queries.GetTransferDetailsPdf;

using System.Collections;
using System.Reflection;
using System.Threading.Tasks;
using Abstractions;
using AutoMapper;
using CheckingAccounts.Models;
using CheckingAccounts.Queries.GetTransferDetails;
using Interfaces;
using Interfaces.Bank.Client;
using iTextSharp.text.pdf;
using MediatR;

public class GetTransferDetailsPdfQueryHandler
    : AbstractRequestHandler
    , IRequestHandler<GetTransferDetailsPdfQuery, byte[]>
{

    public GetTransferDetailsPdfQueryHandler(
        IMediator mediator,
        IBNineDbContext dbContext,
        IMapper mapper,
        ICurrentUserService currentUser,
        ITransferIconsAndNamesProviderService iconsAndNamesProviderService,
        IBankSavingAccountsService accountsService,
        IBankInternalCardsService cardsService,
        IBankSavingsTransactionsService transactionsService
        )
        : base(mediator, dbContext, mapper, currentUser)
    {
    }

    public async Task<byte[]> Handle(GetTransferDetailsPdfQuery request, CancellationToken cancellationToken)
    {
        var obj = new Helper();

        var viewModel = await Mediator.Send(new GetTransferDetailsQuery
        {
            Ip = request.Ip,
            Type = request.TransferType,
            TransferId = request.TransferId,
            MbanqAccessToken = request.MbanqAccessToken,
        }, cancellationToken);

        var form = await File.ReadAllBytesAsync(GetTemplatePath(viewModel.Header), cancellationToken);
        var props = PropertyFiller(viewModel);
        await using Stream ms = new MemoryStream(form);
        return obj.GetFilledPdfFile(props, ms);
    }

    private string GetTemplatePath(string type) => Path.Combine(
        Path.GetDirectoryName(Assembly.GetEntryAssembly().Location),
        "Documents",
        type == "Rejected" ? "TransfersFailPage.pdf" : "TransferSuccessPage.pdf");

    private PropertyFillerDictionary PropertyFiller(BankTransferDetailsViewModel item)
    {
        return new PropertyFillerDictionary(
            new PropertyFillerRecord[]
            {
                new("DebtorOrCreditorName", item.DebtorOrCreditorName),
                new("DateTime", item.TimeStampUtc.ToString("dd MMM yyyy, HH:mm") + " EST"),
                new("Amount", item.Amount),
                new("Status", item.Header),
                new("DetailsKeys", item.Details.First().Title),
                new("DetailsValues", item.Details.First().Value), //TODO: multilined field
                new("RefLink", "Hello world"),
            });
    }

    public class PropertyFillerDictionary
    {
        private readonly PropertyFillerRecord[] _properties;

        public PropertyFillerRecord this[string key] => _properties.Single(x => x.Key == key);

        public PropertyFillerDictionary(PropertyFillerRecord[] properties)
        {
            _properties = properties;
        }

        public bool ContainsKey(string key) => _properties.Any(x => x.Key == key);
    }

    public record PropertyFillerRecord(string Key, string Value);

    public class Helper
    {
        public byte[] GetFilledPdfFile(PropertyFillerDictionary props, Stream template)
        {
            var stream = new MemoryStream();
            var reader = new PdfReader(template);
            var stamper = new PdfStamper(reader, stream);

            var fields = stamper.AcroFields;
            var acroForm = new PdfAcroForm(stamper.Writer);
            acroForm.NeedAppearances = true;
            stamper.AcroFields.GenerateAppearances = true;

            foreach (var field in fields.Fields)
            {
                if (field is DictionaryEntry entry)
                {
                    var key = entry.Key.ToString();
                    if (!props.ContainsKey(key))
                    {
                        continue;
                    }
                    var prop = props[key];
                    stamper.AcroFields.SetField(key, prop.Value);
                }
            }

            stamper.FormFlattening = true;
            stamper.Close();

            return stream.ToArray();
        }
    }
}
