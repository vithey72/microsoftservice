﻿namespace BNine.Application.Aggregates.Transfers.Queries.GetTransferDetailsPdf;

using Abstractions;
using Enums.Transfers.Controller;
using MediatR;

public class GetTransferDetailsPdfQuery : MBanqSelfServiceUserRequest, IRequest<byte[]>
{
    public int TransferId
    {
        get;
        set;
    }

    public BankTransferCompletionState TransferType
    {
        get;
        set;
    }
}
