﻿namespace BNine.Application.Aggregates.Transfers.Queries.CheckTransferIsPotentialIncome;

using Abstractions;
using MediatR;
using Models;

public class CheckTransferIsPotentialIncomeQuery
    : MBanqSelfServiceUserRequest
    , IRequest<CheckTransferIsPotentialIncomeResponse>
{
    public int TransferId
    {
        get;
        set;
    }
}
