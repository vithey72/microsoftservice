﻿namespace BNine.Application.Aggregates.Transfers.Queries.CheckTransferIsPotentialIncome;


using Abstractions;
using AutoMapper;
using Interfaces;
using Interfaces.Bank.Client;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Models;

public class CheckTransferIsPotentialIncomeQueryHandler
    : AbstractRequestHandler
    , IRequestHandler<CheckTransferIsPotentialIncomeQuery, CheckTransferIsPotentialIncomeResponse>
{
    private readonly IBankSavingsTransactionsService _savingsTransactionsService;

    public CheckTransferIsPotentialIncomeQueryHandler(
        IMediator mediator,
        IBNineDbContext dbContext,
        IMapper mapper,
        ICurrentUserService currentUser,
        IBankSavingsTransactionsService savingsTransactionsService
        )
        : base(mediator, dbContext, mapper, currentUser)
    {
        _savingsTransactionsService = savingsTransactionsService;
    }

    public async Task<CheckTransferIsPotentialIncomeResponse> Handle(CheckTransferIsPotentialIncomeQuery request, CancellationToken cancellationToken)
    {
        var transfer = await _savingsTransactionsService
            .GetSavingsAccountTransfer(request.TransferId, request.MbanqAccessToken, request.Ip);

        var isIncomingAch = transfer.Debtor?.Identifier?.StartsWith("ACH:") ?? false;
        if (!isIncomingAch)
        {
            return new CheckTransferIsPotentialIncomeResponse(null);
        }

        var dbEntry = await DbContext
            .PotentialAchIncomeTransfers
            .FirstOrDefaultAsync(x => x.UserId == CurrentUser.UserId &&
                                      x.TransferExternalId == request.TransferId,
                cancellationToken);

        return new CheckTransferIsPotentialIncomeResponse(dbEntry != null);
    }
}
