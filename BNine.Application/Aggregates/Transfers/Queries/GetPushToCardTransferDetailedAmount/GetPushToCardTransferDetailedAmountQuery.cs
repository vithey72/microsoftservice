﻿namespace BNine.Application.Aggregates.Transfers.Queries.GetPushToCardTransferDetailedAmount
{
    using System;
    using MediatR;

    public class GetPushToCardTransferDetailedAmountQuery : IRequest<DetailedAmount>
    {
        public GetPushToCardTransferDetailedAmountQuery(decimal amount)
        {
            Amount = Math.Round(amount, 2, MidpointRounding.ToZero);
        }

        public decimal Amount
        {
            get;
        }
    }
}
