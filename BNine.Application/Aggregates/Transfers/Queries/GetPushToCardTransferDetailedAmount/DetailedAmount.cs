﻿namespace BNine.Application.Aggregates.Transfers.Queries.GetPushToCardTransferDetailedAmount
{
    using Newtonsoft.Json;

    public class DetailedAmount
    {
        [JsonProperty("amount")]
        public decimal Amount
        {
            get; set;
        }

        [JsonProperty("fee")]
        public decimal Fee
        {
            get; set;
        }

        [JsonProperty("totalAmount")]
        public decimal TotalAmount
        {
            get; set;
        }
    }
}
