﻿namespace BNine.Application.Aggregates.Transfers.Queries.GetPullFromCardTransferDetailedAmount
{
    using System;
    using System.Threading;
    using System.Threading.Tasks;
    using AutoMapper;
    using BNine.Application.Abstractions;
    using BNine.Application.Aggregates.ExternalCards.Queries.GetExternalCardFees;
    using BNine.Application.Interfaces;
    using MediatR;

    public class GetPullTransferDetailedAmountQueryHandler
        : AbstractRequestHandler, IRequestHandler<GetPullTransferDetailedAmountQuery, DetailedAmount>
    {
        public GetPullTransferDetailedAmountQueryHandler(IMediator mediator, IBNineDbContext dbContext, IMapper mapper, ICurrentUserService currentUser) : base(mediator, dbContext, mapper, currentUser)
        {
        }

        public async Task<DetailedAmount> Handle(GetPullTransferDetailedAmountQuery request, CancellationToken cancellationToken)
        {
            var fees = await Mediator.Send(new GetExternalCardFeeQuery());

            var fee = decimal.Divide(decimal.Multiply(request.Amount, fees.PullFromExternalCardMinFeePercent), 100);

            var accurateFee = Math.Round(fee, 2) < fees.PullFromExternalCardMinFeeAmount ? fees.PullFromExternalCardMinFeeAmount : Math.Round(fee, 2);

            return new DetailedAmount
            {
                Amount = request.Amount,
                Fee = accurateFee,
                TotalAmount = decimal.Add(request.Amount, accurateFee)
            };
        }
    }
}
