﻿namespace BNine.Application.Aggregates.Transfers.Queries.GetPullFromCardTransferDetailedAmount
{
    using System;
    using MediatR;

    public class GetPullTransferDetailedAmountQuery : IRequest<DetailedAmount>
    {
        public GetPullTransferDetailedAmountQuery(decimal amount)
        {
            Amount = Math.Round(amount, 2, MidpointRounding.ToZero);
        }

        public decimal Amount
        {
            get;
        }
    }
}
