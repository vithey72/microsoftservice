﻿namespace BNine.Application.Aggregates.Transfers.Models
{
    using Newtonsoft.Json;

    public class Recipient
    {
        [JsonProperty("accountNumber")]
        public string AccountNumber
        {
            get; set;
        }

        [JsonProperty("routingNumber")]
        public string RoutingNumber
        {
            get; set;
        }

        [JsonProperty("name")]
        public string Name
        {
            get; set;
        }

        [JsonProperty("address")]
        public RecipientAddress Address
        {
            get; set;
        }
    }
}
