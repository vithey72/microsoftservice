﻿namespace BNine.Application.Aggregates.Transfers.Models
{
    using System;
    using Newtonsoft.Json;

    public class ExternalCardTransferInfo
    {
        [JsonProperty("id")]
        public int Id
        {
            get; set;
        }

        [JsonProperty("status")]
        public string Status
        {
            get; set;
        }

        [JsonProperty("createdAt")]
        public DateTime CreatedAt
        {
            get; set;
        }
    }
}
