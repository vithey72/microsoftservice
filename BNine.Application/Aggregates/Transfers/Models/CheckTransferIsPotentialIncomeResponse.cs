﻿namespace BNine.Application.Aggregates.Transfers.Models;

public record CheckTransferIsPotentialIncomeResponse(bool? IsMarked);
