﻿namespace BNine.Application.Aggregates.Transfers.Models;

using Newtonsoft.Json;

public class DeviceDetails
{
    [JsonProperty("details")]
    public object Details
    {
        get;
        set;
    }

    [JsonProperty("externalIpAddress")]
    public string ExternalIpAddress
    {
        get;
        set;
    }
}
