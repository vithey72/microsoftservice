﻿namespace BNine.Application.Aggregates.Transfers.Models
{
    using Newtonsoft.Json;

    public class RecipientAddress
    {
        [JsonProperty("zipCode")]
        public string ZipCode
        {
            get; set;
        }

        [JsonProperty("city")]
        public string City
        {
            get; set;
        }

        [JsonProperty("state")]
        public string State
        {
            get; set;
        }

        [JsonProperty("address")]
        public string Address
        {
            get; set;
        }

        [JsonProperty("unit")]
        public string Unit
        {
            get; set;
        }
    }
}
