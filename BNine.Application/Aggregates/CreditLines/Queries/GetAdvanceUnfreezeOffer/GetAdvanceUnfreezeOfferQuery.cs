﻿namespace BNine.Application.Aggregates.CreditLines.Queries.GetAdvanceUnfreezeOffer;

using MediatR;
using Models;

public class GetAdvanceUnfreezeOfferQuery : IRequest<AdvanceUnfreezeOfferViewModel>
{
}
