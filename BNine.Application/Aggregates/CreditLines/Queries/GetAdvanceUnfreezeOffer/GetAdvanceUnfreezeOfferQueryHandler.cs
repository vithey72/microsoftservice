﻿namespace BNine.Application.Aggregates.CreditLines.Queries.GetAdvanceUnfreezeOffer;

using AutoMapper;
using Constants;
using Interfaces;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Models;

public class GetAdvanceUnfreezeOfferQueryHandler
    : BaseAdvanceUnfreezeQueryHandler,
    IRequestHandler<GetAdvanceUnfreezeOfferQuery, AdvanceUnfreezeOfferViewModel>
{
    public GetAdvanceUnfreezeOfferQueryHandler(
        IMediator mediator,
        IBNineDbContext dbContext,
        IMapper mapper,
        ICurrentUserService currentUser
        )
        : base(mediator, dbContext, mapper, currentUser)
    {
    }

    public async Task<AdvanceUnfreezeOfferViewModel> Handle(GetAdvanceUnfreezeOfferQuery request, CancellationToken token)
    {

        var user = await DbContext.Users
            .Include(u => u.CurrentAccount)
            .Include(u => u.AdvanceStats)
            .FirstOrDefaultAsync(u => u.Id == CurrentUser.UserId!.Value, token);

        if (user == null)
        {
            throw new KeyNotFoundException("User is not present in DB");
        }

        if (user.AdvanceStats == null || !user.AdvanceStats.IsEligibleForExtension())
        {
            throw new InvalidOperationException("There is no Advance Extension offer for this user right now.");
        }

        var (paidServiceEntity, unfreezeCost) = await base.SelectPaidServiceEntity(token, user);

        if (paidServiceEntity == null)
        {
            throw new InvalidOperationException("Service is missing from DB");
        }

        var existingPurchase = await base.GetIfAlreadyPurchased();
        var alreadyPaid = existingPurchase != null;

        return new AdvanceUnfreezeOfferViewModel
        {
            AlreadyPaid = alreadyPaid,
            NormalAdvance =
                new AdvanceUnfreezeOfferViewModel.AdvanceBox
                {
                    ValueHeader = "$0",
                    Subtitle = "Your Available Advance is 0",
                },
            UnfreezeAdvance = new AdvanceUnfreezeOfferViewModel.AdvanceBox()
            {
                ValueHeader = "$" + Convert.ToInt32(user.AdvanceStats.ExtensionLimit),
                Subtitle = "You are able to unfreeze",
            },
            Text =
                @$"Your available B9 Advance is 0 because there was no paycheck deposited in the last 14 days.

This special offer is available until {user.AdvanceStats.ExtensionExpiresAt!.Value.Date.ToString(DateFormat.MonthFirst)} incl.

If you accept this amount will be repaid from your next paycheck or you have to repay it from your account in the next 14 days to avoid an overdue balance.",
            ButtonNextText = !alreadyPaid ? $"PAY ${unfreezeCost} TO UNLOCK ADVANCE" : "UNLOCK ADVANCE",
            ButtonBackText = "BACK"
        };
    }
}
