﻿namespace BNine.Application.Aggregates.CreditLines.Queries.GetAfterAdvanceTipDialog;

using Abstractions;
using AutoMapper;
using CommonModels.Dialog;
using CommonModels.Static;
using Constants;
using Helpers;
using Interfaces;
using Interfaces.Bank.Client;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Models;

public class GetAfterAdvanceTipDialogQueryHandler
    : AbstractRequestHandler
    , IRequestHandler<GetAfterAdvanceTipDialogQuery, GetAfterAdvanceTipDialogViewModel>
{
    private readonly IBankLoanAccountsService _loanAccountsService;

    public GetAfterAdvanceTipDialogQueryHandler(
        IMediator mediator,
        IBNineDbContext dbContext,
        IMapper mapper,
        ICurrentUserService currentUser,
        IBankLoanAccountsService loanAccountsService
    )
        : base(mediator, dbContext, mapper, currentUser)
    {
        _loanAccountsService = loanAccountsService;
    }

    public async Task<GetAfterAdvanceTipDialogViewModel> Handle(GetAfterAdvanceTipDialogQuery request, CancellationToken cancellationToken)
    {
        var advances = await DbContext.LoanAccounts
            .Where(l => l.UserId == CurrentUser.UserId)
            .OrderByDescending(l => l.CreatedAt)
            .Take(2)
            .ToListAsync(cancellationToken);

        var isFirstAdvance = advances.Count == 1;
        var latestAdvance = advances.FirstOrDefault();

        if (latestAdvance == null)
        {
            throw new InvalidOperationException("Cannot tip without ever taking an advance.");
        }
        var bankLoan = await _loanAccountsService.GetLoan(request.MbanqAccessToken, request.Ip, latestAdvance.ExternalId);

        return new GetAfterAdvanceTipDialogViewModel
        {
            IsFirstAdvanceFlow = isFirstAdvance,
            Screen1 = new GetAfterAdvanceTipDialogViewModel.TipDialogScreen1
            {
                Header = new GenericDialogHeaderViewModel
                {
                    Title = DialogTexts.Headers.EarlyPaydayTitle,
                    Subtitle = DialogTexts.Headers.EarlyPaydaySub,
                },
                Body = new GenericDialogBodyViewModel
                {
                    Title = "Tips help more than you think",
                    Subtitle = "For one, they help ensure we can keep offering perks like 0% APR on all B9 Advances. For another, they let us know you're happy with our service.",
                    ButtonText = "CONFIRM",
                },
                TipSuggestions = new GetAfterAdvanceTipDialogViewModel.TipSuggestionsSection
                {
                    Tips = PaidUserServices.Prices.TipsAmounts,
                    DefaultTipIndex = PaidUserServices.Prices.DefaultTipIndex,
                    Disclaimer = "Suggested tip is $6",
                },
                PictureUrl = "https://b9prodaccount.blob.core.windows.net/static-documents-container/Misc/tips_image.png",
            },
            Screen2 = new GetAfterAdvanceTipDialogViewModel.TipDialogScreen2
            {
                Header = new GenericDialogHeaderViewModel
                {
                    Title = DialogTexts.Headers.EarlyPaydayTitle,
                    Subtitle = DialogTexts.Headers.EarlyPaydaySub,
                },
                Body = new GenericDialogBodyViewModel
                {
                    Title = "Done",
                    Subtitle = $"{CurrencyFormattingHelper.AsRounded(bankLoan.DisbursedAmount)} has been added to your B9 wallet. " +
                               "Remember, any time you do a direct deposit into your B9 account those funds are automatically applied to " +
                               "paying off the balance on your advance—even if it’s before the scheduled repayment date.",
                    ButtonText = "DONE",
                },
                TipSection = new GetAfterAdvanceTipDialogViewModel.TipSectionWithDropdown
                {
                    Tips = PaidUserServices.Prices.TipsAmounts,
                    DefaultTipIndex = PaidUserServices.Prices.DefaultTipIndex,
                    Disclaimer1 = "Please consider leaving a tip.",
                    Disclaimer2 = "Optional tips:",
                    Disclaimer3 = "Tips are optional and do not impact eligibility.",
                }
            }
        };
    }
}
