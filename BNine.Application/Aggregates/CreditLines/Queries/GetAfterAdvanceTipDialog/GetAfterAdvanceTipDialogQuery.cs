﻿namespace BNine.Application.Aggregates.CreditLines.Queries.GetAfterAdvanceTipDialog;

using Abstractions;using MediatR;
using Models;

public class GetAfterAdvanceTipDialogQuery : MBanqSelfServiceUserRequest, IRequest<GetAfterAdvanceTipDialogViewModel>
{
}
