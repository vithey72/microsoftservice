﻿namespace BNine.Application.Aggregates.CreditLines.Queries.AdvanceSimulation.Mappers;

using System.Globalization;
using CommonModels.Buttons;
using CommonModels.Dialog;
using CommonModels.DialogElements;
using CommonModels.Text;
using Enums.TariffPlan;
using Models;
using Users.Models.AdvanceSimulation;

public class AdvanceSimulationViewModelMapper : IAdvanceSimulationViewModelMapper
{
    private const string GreenMilestoneColor = "009C3D";
    private const string LimeMilestoneColor = "8BC446";
    private const string MaxMilestoneColor = "14273A";
    private const string GrayCriteriaColor = "14273A";


    private const string YellowMilestoneColor = "FFB819";
    private const string OrangeMilestoneColor = "FFB819";
    private const string DarkBlueMilestoneColor = "14273A";


    private const string DateFormat = "MM/dd/yyyy";

    public AdvanceSimulationChartViewModel MapAdvanceSimulationChartModel(DwhAdvanceSimulationViewModel input,
        TariffPlanFamily currentUserTariffPlan)
    {
        var simulatedLimitText = input.IsSimulate ? "\n(simulated)" : string.Empty;

        var isPremiumPlan = input.CalculatorState.TariffPlanFamily == TariffPlanFamily.Premium.ToString();

        var isTariffPlanSimulated = currentUserTariffPlan.ToString() != input.CalculatorState.TariffPlanFamily;

        var advanceLimitColor = GetAdvanceLimitColor(input.CalculatorState.TariffPlanFamily, input.IsSimulate, currentUserTariffPlan.ToString());
        var nextAdvanceLimitColor = GetNextAdvanceLimitColor(input.CalculatorState.TariffPlanFamily, input.IsSimulate, currentUserTariffPlan.ToString());

        var resultViewModel = new AdvanceSimulationChartViewModel()
        {
            MaxChartValue = input.CalculatorState.MaxNextAdvanceOnPremium ?? 500,
            Milestones =
                new AdvanceSimulationChartMilestones()
                {
                    Hundred = new AdvanceSimulationChartMilestoneValue()
                    {
                        Title = "Max in\nBasic Plan",
                        ValueString = input.CalculatorState.MaxNextAdvanceOnBasic.HasValue ?
                            $"${input.CalculatorState.MaxNextAdvanceOnBasic}" : "$100",
                        Value = input.CalculatorState.MaxNextAdvanceOnBasic ?? 100,
                        ValueColor = MaxMilestoneColor
                    },
                    FiveHundred = new AdvanceSimulationChartMilestoneValue()
                    {
                        Title = "Max in\nPremium Plan",
                        ValueString = input.CalculatorState.MaxNextAdvanceOnPremium.HasValue ?
                            $"${input.CalculatorState.MaxNextAdvanceOnPremium}" : "$500",
                        Value = input.CalculatorState.MaxNextAdvanceOnPremium ?? 500,
                        ValueColor = MaxMilestoneColor
                    }
                },
            CalculationOutput = new AdvanceSimulationChartCalculationOutput()
            {
                Title = "Advance Limit",
                CurrentLimit = new AdvanceSimulationChartMilestoneValue()
                {
                    Title = $"Your current\nlimit{simulatedLimitText}:",
                    Value = input.CalculatorState.CurrentAdvanceLimit.GetValueOrDefault(),
                    ValueString = $"${input.CalculatorState.CurrentAdvanceLimit.GetValueOrDefault()}",
                    ValueColor = advanceLimitColor
                },
                NextLimit = new AdvanceSimulationChartMilestoneValue()
                {
                    Title = $"Your next\nlimit{simulatedLimitText}:",
                    Value = input.CalculatorState.NextAdvanceLimit.GetValueOrDefault(),
                    ValueString = $"${input.CalculatorState.NextAdvanceLimit.GetValueOrDefault()}",
                    ValueColor = nextAdvanceLimitColor
                }
            }
        };

        return resultViewModel;
    }

    private string GetAdvanceLimitColor(string calculatorTariffPlan, bool isSimulation, string currentTariffPlan)
    {
        var isTariffPlanToggled = currentTariffPlan != calculatorTariffPlan;

        if (isTariffPlanToggled)
        {
            return OrangeMilestoneColor;
        }

        //tariff plan toggle is untouched but some other toggle is used
        if (!isTariffPlanToggled && isSimulation)
        {
            return GreenMilestoneColor;
        }

        return GreenMilestoneColor;
    }

    private string GetNextAdvanceLimitColor(string calculatorTariffPlan, bool isSimulation, string currentTariffPlan)
    {
        var isTariffPlanToggled = currentTariffPlan != calculatorTariffPlan;

        //current is basic but toggled to premium
        if (currentTariffPlan == TariffPlanFamily.Advance.ToString() && isTariffPlanToggled)
        {
            return DarkBlueMilestoneColor;
        }

        if (isTariffPlanToggled)
        {
            return GreenMilestoneColor;
        }

        //tariff plan toggle is untouched but some other toggle is used
        if (!isTariffPlanToggled && isSimulation)
        {
            return OrangeMilestoneColor;
        }

        return LimeMilestoneColor;
    }

    public AdvanceSimulationViewModel MapAdvanceSimulationViewModel(DwhAdvanceSimulationViewModel input,
        TariffPlanFamily currentUserTariffPlan)
    {
        var responseViewModel = new AdvanceSimulationViewModel()
        {
            Body = new AdvanceSimulationBody()
            {
                Chart = this.MapAdvanceSimulationChartModel(input, currentUserTariffPlan),
                Criteria = MapCriteria(input.CalculatorState),
                Calculator = MapCalculator(input.CalculatorState)
            },
            Header = MapHeader(),
            HeaderHint = MapHeaderHint()
        };

        return responseViewModel;
    }

    private GenericDialogHeaderViewModel MapHeader()
    {
        var header = new GenericDialogHeaderViewModel() { Title = "B9 Advance", Subtitle = null };

        return header;
    }

    private HintWithDialogViewModel MapHeaderHint()
    {
        var headerHint = new HintWithDialogViewModel()
        {
            Subtitle = new LinkedText(
                "Optional service that is provided at no additional cost for B9 Basic and B9 Premium members. It is a no interest advance pay based on your earned but not yet paid wages to help you cover expenses, avoid mismatches between day of expenses and a paycheck, and provide you with a steady, predictable cash-flows.\n\nAll B9 Advance℠ members start with up to $100 early pay advance, with a potential for it to increase up to $500. B9 Members are informed of their current available maxes in the B9 mobile app. Their limit may change at any time, at B9's discretion. See Terms of Services and contact B9 via support@bnine.com for additional terms, conditions, and eligibility requirements.\n\nMay not be available in all states."
            ),
            IsCloseButtonHidden = false,
            Buttons = new[] { new ButtonViewModel() { Text = "GOT IT", Style = ButtonStyleEnum.Solid } },
            Title = "B9 Advance Service",
        };

        return headerHint;
    }

    private AdvanceSimulationCriteria MapCriteria(AdvanceSimulationCalculatorState input)
    {
        var criteria = new AdvanceSimulationCriteria()
        {
            Title = "Things to do to get your next advance amount:",
            Items = new[]
            {
                new AdvanceSimulationCriteriaItem()
                {
                    Title = "Wait at least 21 days between limit increases",
                    Subtitle =
                        input.HadAdvanceIncreaseInLast7Days && input.LastAdvanceLimitIncreaseDate.HasValue
                            ? $"Last limit increase {input.LastAdvanceLimitIncreaseDate.Value.ToString(DateFormat, new CultureInfo("en-US"))}"
                            : null,
                    IsSatisfied = !input.HadAdvanceIncreaseInLast7Days,
                    SubtitleColor = GrayCriteriaColor
                },
                new AdvanceSimulationCriteriaItem()
                {
                    Title = "Use your current limit",
                    Subtitle = null,
                    IsSatisfied = input.CurrentLimitIsUsed,
                    SubtitleColor = GrayCriteriaColor
                },
                new AdvanceSimulationCriteriaItem()
                {
                    Title = "Pay back your current limit",
                    Subtitle = input.CurrentLimitIsPaid ? null : "Hasn't been repaid yet",
                    IsSatisfied = input.CurrentLimitIsPaid,
                    SubtitleColor = GrayCriteriaColor
                },
                new AdvanceSimulationCriteriaItem()
                {
                    Title = "Deposit at least one more paycheck",
                    Subtitle =
                        input.AtLeastOnePayrollWasReceived && input.LastPayrollReceivedDate.HasValue
                            ? $"Last paycheck deposited {input.LastPayrollReceivedDate.Value.ToString(DateFormat, new CultureInfo("en-US"))}"
                            : null,
                    IsSatisfied = input.AtLeastOnePayrollWasReceived,
                    SubtitleColor = GrayCriteriaColor
                },
                new AdvanceSimulationCriteriaItem()
                {
                    Title = "Make sure your 14-day sum of paychecks is more than $300",
                    Subtitle = input.PaycheckAmountSumForLast14DaysOver300Usd
                        ? $"Current sum of 14 day paychecks = ${input.PayrollTransfersAmountSumForLast14Days}"
                        : null,
                    IsSatisfied = input.PaycheckAmountSumForLast14DaysOver300Usd,
                    SubtitleColor = GrayCriteriaColor
                }
            }
        };

        return criteria;
    }

    private AdvanceSimulatorCalculator MapCalculator(AdvanceSimulationCalculatorState input)
    {
        var calculator = new AdvanceSimulatorCalculator()
        {
            Title = "Tap the buttons below to see how you can affect your Advance Limit",
            Factors = MapFactors(input),
            ResetButton = new AdvanceSimulatorCalculatorResetButton()
            {
                Text = "Reset",
                Style = "solid",
            }
        };

        return calculator;
    }

    private AdvanceSimulatorFactors MapFactors(AdvanceSimulationCalculatorState input)
    {
        var isUserOnPremiumPlan = input.TariffPlanFamily == TariffPlanFamily.Premium.ToString();

        var premiumPlanTitlePlaceholder = "{Switch Now}  ";
        var tariffPlanTitle = "B9 Premium";
        var tariffPlanFullTitle = !isUserOnPremiumPlan ? premiumPlanTitlePlaceholder : tariffPlanTitle;

        var premiumPlanUrls = new[]
        {
            new DeeplinkEmbeddedUrlViewModel()
            {
                Text = $"Upgrade to {tariffPlanTitle}", Deeplink = "premium-tariff-plan", Placeholder = "Switch Now"
            }
        };

        var factors = new AdvanceSimulatorFactors()
        {
            Title = "Tap the buttons below to see how you can affect your Advance Limit",
            Premium = new AdvanceSimulatorFactor()
            {
                Title = new TextWithDeeplink(tariffPlanFullTitle,
                    !isUserOnPremiumPlan ? premiumPlanUrls : null),
                IsOn = isUserOnPremiumPlan
            },
            GroceryStores =
                new AdvanceSimulatorFactor()
                {
                    Title = new TextWithDeeplink("Spend at least $100 at a B9 affiliated grocery store."),
                    IsOn = input.HasMonthlySpendingOnGroceriesAbove100Usd,
                    Hint = new GenericDialogRichBodyViewModel()
                    {
                        Buttons = new[]
                        {
                            new ButtonViewModel("TRANSACTIONS", ButtonStyleEnum.Solid)
                                .WithDeeplink("transaction-analytics",
                                    new
                                    {
                                        startDateUtc = DateTime.UtcNow.AddDays(-30),
                                        endDateUtc = DateTime.UtcNow,
                                        categoryKey = "supermarkets-online-stores"
                                    })
                        },
                        Title = "To up your advance limit, spend at least\n$100 a month using your B9 Card at a\nsupermarket or grocery store",
                        Subtitle = new LinkedText("To up your advance limit, spend at least $100 a month in the Supermarket & Groceries category.\n\nWhich also includes, but is not limited to: bakeries, convenience stores, markets, candy & nut confectionery stores, and locker meat provisioners."),
                        IsCloseButtonHidden = false,
                        ImageUrl = "https://b9prodaccount.blob.core.windows.net/static-documents-container/AdvanceSimulator/icon-grocery.png",
                    }
                },
            NonPaidAdvances =
                new AdvanceSimulatorFactor()
                {
                    Title = new TextWithDeeplink("Past due advances for last 30 days"),
                    IsOn = input.HasOverdueRepaymentsIn30Days
                },
            SumOfPaychecks = new SumOfPaychecksFactor()
            {
                Title = new TextWithDeeplink("Increase the amount of your 14-day paycheck sum  {Deposit}",
                    new[]
                    {
                        new DeeplinkEmbeddedUrlViewModel()
                        {
                            Text = "Deposit", Deeplink = "switch_deposit_page", Placeholder = "Deposit"
                        }
                    }),
                IsOn = input.PaycheckAmountSumForLast14DaysOver300Usd,
                Steps = MapSumOfPaychecksFactorSteps(input)
            }
        };

        return factors;
    }

    private static SumOfPaychecksFactorStep[] MapSumOfPaychecksFactorSteps(AdvanceSimulationCalculatorState input)
    {
        var steps = new SumOfPaychecksFactorStep[]
        {
            new()
            {
                Title = "$300-$800",
                RangeStart = 300,
                RangeEnd = 800,
                IsSelected = input.PayrollTransfersAmountSumForLast14Days <= 800
            },
            new()
            {
                Title = "$800-$1500",
                RangeStart = 800,
                RangeEnd = 1500,
                IsSelected = input.PayrollTransfersAmountSumForLast14Days > 800 &&
                             input.PayrollTransfersAmountSumForLast14Days < 1500
            },
            new()
            {
                Title = "$1500+",
                RangeStart = 1500,
                IsSelected = input.PayrollTransfersAmountSumForLast14Days >= 1500
            }
        };

        return steps;
    }
}
