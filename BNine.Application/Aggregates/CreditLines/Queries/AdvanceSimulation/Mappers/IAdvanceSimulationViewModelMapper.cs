﻿namespace BNine.Application.Aggregates.CreditLines.Queries.AdvanceSimulation.Mappers;

using Enums.TariffPlan;
using Models;
using Users.Models.AdvanceSimulation;

public interface IAdvanceSimulationViewModelMapper
{
    AdvanceSimulationChartViewModel MapAdvanceSimulationChartModel(DwhAdvanceSimulationViewModel input,
        TariffPlanFamily currentUserTariffPlan);

    AdvanceSimulationViewModel MapAdvanceSimulationViewModel(DwhAdvanceSimulationViewModel input,
        TariffPlanFamily currentUserTariffPlan);
}
