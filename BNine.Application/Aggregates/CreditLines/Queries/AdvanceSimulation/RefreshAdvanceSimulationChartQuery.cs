﻿namespace BNine.Application.Aggregates.CreditLines.Queries.AdvanceSimulation;

using Enums.TariffPlan;
using MediatR;
using Models;

public class RefreshAdvanceSimulationChartQuery : IRequest<AdvanceSimulationChartViewModel>
{
    public TariffPlanFamily TariffPlanFamily
    {
        get;
        set;
    }

    public int PayrollAmountRangeStart
    {
        get;
        set;
    }

    public int? PayrollAmountRangeEnd
    {
        get;
        set;
    }

    public bool HasMonthlySpendingOnGroceriesAbove100Usd
    {
        get;
        set;
    }

    public bool HasRepaymentOverdueFor30Days
    {
        get;
        set;
    }
}
