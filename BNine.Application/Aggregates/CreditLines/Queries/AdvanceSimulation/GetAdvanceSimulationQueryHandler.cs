﻿namespace BNine.Application.Aggregates.CreditLines.Queries.AdvanceSimulation;

using Abstractions;
using AutoMapper;
using Domain.Entities.User;
using Enums.TariffPlan;
using Exceptions;
using Interfaces;
using Interfaces.DwhIntegration;
using Mappers;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Models;
using Users.Models.AdvanceSimulation;

public class GetAdvanceSimulationQueryHandler : AbstractRequestHandler
    , IRequestHandler<GetAdvanceSimulationQuery, AdvanceSimulationViewModel>
{
    private readonly IAdvanceSimulationService _advanceSimulationService;
    private readonly IAdvanceSimulationViewModelMapper _viewModelMapper;

    public GetAdvanceSimulationQueryHandler(IMediator mediator, IBNineDbContext dbContext, IMapper mapper,
        ICurrentUserService currentUser,
        IAdvanceSimulationService advanceSimulationService,
        IAdvanceSimulationViewModelMapper viewModelMapper) : base(mediator, dbContext, mapper, currentUser)
    {
        _advanceSimulationService = advanceSimulationService;
        _viewModelMapper = viewModelMapper;
    }

    public async Task<AdvanceSimulationViewModel> Handle(GetAdvanceSimulationQuery request,
        CancellationToken cancellationToken)
    {
        var user = await DbContext.Users
            .Include(u => u.UserTariffPlans)
            .ThenInclude(t => t.TariffPlan)
            .FirstOrDefaultAsync(x => x.Id == CurrentUser.UserId.GetValueOrDefault());

        if (user == null)
        {
            throw new NotFoundException(nameof(User));
        }

        var assignedUserTariffPlan = user.UserTariffPlans.FirstOrDefault(x =>
            x.Status == TariffPlanStatus.Active && x.TariffPlan.Type == TariffPlanFamily.Premium)?.TariffPlan?.Type;

        var currentUserTariffPlan =
            assignedUserTariffPlan.HasValue ? assignedUserTariffPlan.Value : TariffPlanFamily.Advance;

        var dwhAdvanceSimulationViewModel = await GetAdvanceSimulationFromDwh(request);

        var dwhCalculatorState = dwhAdvanceSimulationViewModel.CalculatorState;

        var responseViewModel = _viewModelMapper.MapAdvanceSimulationViewModel(dwhAdvanceSimulationViewModel, currentUserTariffPlan);

        return responseViewModel;
    }

    private async Task<DwhAdvanceSimulationViewModel> GetAdvanceSimulationFromDwh(GetAdvanceSimulationQuery request)
    {
        var dwhAdvanceSimulationViewModel =
            await _advanceSimulationService.GetAdvanceSimulation(CurrentUser.UserId.Value);

        return dwhAdvanceSimulationViewModel;
    }
}
