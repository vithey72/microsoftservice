﻿namespace BNine.Application.Aggregates.CreditLines.Queries.AdvanceSimulation;

using Abstractions;
using AutoMapper;
using Domain.Entities.User;
using Enums.TariffPlan;
using Exceptions;
using Interfaces;
using Interfaces.DwhIntegration;
using Mappers;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Models;
using Users.Models.AdvanceSimulation;

public class RefreshAdvanceSimulationChartQueryHandler: AbstractRequestHandler
    , IRequestHandler<RefreshAdvanceSimulationChartQuery, AdvanceSimulationChartViewModel>
{
    private readonly IAdvanceSimulationService _advanceSimulationService;
    private readonly IAdvanceSimulationViewModelMapper _viewModelMapper;

    public RefreshAdvanceSimulationChartQueryHandler(IMediator mediator, IBNineDbContext dbContext, IMapper mapper,
        ICurrentUserService currentUser,
        IAdvanceSimulationService advanceSimulationService,
        IAdvanceSimulationViewModelMapper viewModelMapper) : base(mediator, dbContext, mapper, currentUser)
    {
        _advanceSimulationService = advanceSimulationService;
        _viewModelMapper = viewModelMapper;
    }

    public async Task<AdvanceSimulationChartViewModel> Handle(RefreshAdvanceSimulationChartQuery request, CancellationToken cancellationToken)
    {
        var user = await DbContext.Users
            .Include(u => u.UserTariffPlans)
            .ThenInclude(t => t.TariffPlan)
            .FirstOrDefaultAsync(x => x.Id == CurrentUser.UserId.GetValueOrDefault());

        if (user == null)
        {
            throw new NotFoundException(nameof(User));
        }

        var assignedUserTariffPlan = user.UserTariffPlans.FirstOrDefault(x =>
            x.Status == TariffPlanStatus.Active && x.TariffPlan.Type == TariffPlanFamily.Premium)?.TariffPlan?.Type;

        var currentUserTariffPlan =
            assignedUserTariffPlan.HasValue ? assignedUserTariffPlan.Value : TariffPlanFamily.Advance;

        var dwhAdvanceSimulationViewModel = await GetAdvanceSimulationFromDwh(request);

        var responseViewModel = _viewModelMapper.MapAdvanceSimulationChartModel(dwhAdvanceSimulationViewModel, currentUserTariffPlan);

        return responseViewModel;
    }

    private async Task<DwhAdvanceSimulationViewModel> GetAdvanceSimulationFromDwh(RefreshAdvanceSimulationChartQuery request)
    {
        var requestParameters = new DwhAdvanceSimulationRequestParameters()
        {
            TariffPlanFamily = request.TariffPlanFamily.ToString(),
            PayrollAmountRangeStart = request.PayrollAmountRangeStart,
            PayrollAmountRangeEnd = request.PayrollAmountRangeEnd,
            HasRepaymentOverdueFor30Days = request.HasRepaymentOverdueFor30Days,
            HasMonthlySpendingOnGroceriesAbove100Usd = request.HasMonthlySpendingOnGroceriesAbove100Usd
        };

        var dwhAdvanceSimulationViewModel =
            await _advanceSimulationService.GetAdvanceSimulation(CurrentUser.UserId.Value, requestParameters);

        return dwhAdvanceSimulationViewModel;
    }
}
