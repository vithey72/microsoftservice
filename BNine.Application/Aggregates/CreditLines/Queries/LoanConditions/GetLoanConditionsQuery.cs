﻿namespace BNine.Application.Aggregates.CreditLines.Queries.LoanConditions
{
    using MediatR;
    using Models.LoanConditions;

    public class GetLoanConditionsQuery : IRequest<GetLoanConditionsResponse>
    {
    }
}
