﻿namespace BNine.Application.Aggregates.CreditLines.Queries.LoanConditions
{
    using System.Threading;
    using System.Threading.Tasks;
    using Abstractions;
    using AutoMapper;
    using Exceptions;
    using Interfaces;
    using MediatR;
    using Microsoft.EntityFrameworkCore;
    using Models.LoanConditions;

    public class GetLoanConditionsQueryHandler
        : AbstractRequestHandler,
          IRequestHandler<GetLoanConditionsQuery, GetLoanConditionsResponse>
    {
        public GetLoanConditionsQueryHandler(
            IMediator mediator,
            IBNineDbContext dbContext,
            IMapper mapper,
            ICurrentUserService currentUser)
            : base(mediator, dbContext, mapper, currentUser)
        {
        }

        public async Task<GetLoanConditionsResponse> Handle(GetLoanConditionsQuery request, CancellationToken cancellationToken)
        {
            var loanSettings = await DbContext
                .LoanSettings
                .FirstOrDefaultAsync(cancellationToken);

            if (loanSettings == null)
            {
                throw new NotFoundException("LoanSettings table is empty.");
            }

            var response = new GetLoanConditionsResponse
            {
                RequiredDeposit = loanSettings.RequiredMinimalDeposit,
            };

            return response;
        }
    }
}
