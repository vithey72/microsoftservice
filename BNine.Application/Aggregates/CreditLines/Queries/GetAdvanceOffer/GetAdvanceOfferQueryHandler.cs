﻿namespace BNine.Application.Aggregates.CreditLines.Queries.GetAdvanceOffer;

using Abstractions;
using AutoMapper;
using BNine.Application.Aggregates.CommonModels;
using BNine.Enums.Advance;
using CommonModels.Dialog;
using CommonModels.Static;
using Configuration.Queries.GetConfiguration;
using Constants;
using Domain.Entities.User;
using Enums.TariffPlan;
using Helpers;
using Interfaces;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Models;

public class GetAdvanceOfferQueryHandler
    : AbstractRequestHandler
    , IRequestHandler<GetAdvanceOfferQuery, OptionViewModelResult<GetAdvanceOfferResponseViewModel>>
{
    private readonly IAdvanceWidgetStateProvider _advanceWidgetStateProvider;
    private readonly ITariffPlanService _tariffPlanService;
    private readonly IAdvancesCompletedCheckupService _advancesCompletedCheckupService;

    public GetAdvanceOfferQueryHandler(
        IMediator mediator,
        IBNineDbContext dbContext,
        IMapper mapper,
        ICurrentUserService currentUser,
        IAdvanceWidgetStateProvider advanceWidgetStateProvider,
        ITariffPlanService tariffPlanService,
        IAdvancesCompletedCheckupService advancesCompletedCheckupService
        )
        : base(mediator, dbContext, mapper, currentUser)
    {
        _advanceWidgetStateProvider = advanceWidgetStateProvider;
        _tariffPlanService = tariffPlanService;
        _advancesCompletedCheckupService = advancesCompletedCheckupService;
    }

    public async Task<OptionViewModelResult<GetAdvanceOfferResponseViewModel>> Handle(GetAdvanceOfferQuery request, CancellationToken cancellationToken)
    {
        var userWithOffer = await DbContext.Users
            .Include(u => u.AdvanceStats)
            .Include(u => u.UserSyncedStats)
            .Include(u => u.LoanAccounts)
            .SingleAsync(u => u.Id == CurrentUser.UserId!.Value, cancellationToken);

        var widgetState = await _advanceWidgetStateProvider.GetWidgetState(userWithOffer);
        if (widgetState != AdvanceWidgetState.AdvanceAvailable && widgetState != AdvanceWidgetState.BoostAvailable)
        {
            return OptionViewModelResult<GetAdvanceOfferResponseViewModel>.Empty;
        }

        var offer = userWithOffer!.AdvanceStats;
        var isModalWindowRequired = await _advancesCompletedCheckupService.IsModalWindowRequired(userWithOffer.Id);

        var screenOne = BuildScreenOne();
        var screenTwo = BuildScreenTwo(offer, isModalWindowRequired);
        var experimentScreen = await BuildExperimentScreen(userWithOffer);

        if (offer.HasBoostLimit)
        {
            var maxBoost = new[]
                {
                    offer.BoostLimit1,
                    offer.BoostLimit2,
                    offer.BoostLimit3
                }.Max();
            screenTwo.Body.AlternativeButtonText = $"BOOST               TO {CurrencyFormattingHelper.AsRounded(offer.AvailableLimit + maxBoost)}";
        }

        return new(new GetAdvanceOfferResponseViewModel
        {
            Screen1 = screenOne,
            ExperimentScreen = experimentScreen,
            Screen2 = screenTwo,
        });
    }

    private static GenericDialogWithPicture BuildScreenOne()
    {
        var screenOne = new GenericDialogWithPicture
        {
            PictureUrl = "https://b9prodaccount.blob.core.windows.net/static-documents-container/Misc/rechange.png",
            Header = new GenericDialogHeaderViewModel
            {
                Title = "Early Payday",
                Subtitle = "Get paid earlier with B9 Advance",
            },
            Body = new GenericDialogBodyViewModel
            {
                Title = "Get paid earlier with B9 Advance",
                Subtitle = "B9 Advance is an optional, 0% Interest service " +
                           "offered to its members by B9. The amount " +
                           "of B9 Advance you are eligible to receive is " +
                           "determined by B9, and may change from " +
                           "time to time.",
                ButtonText = "CONTINUE",
            }
        };
        return screenOne;
    }

    private AdvanceConfirmationDialog BuildScreenTwo(AdvanceStats offer, bool isModalWindowRequired = false)
    {

        var screenTwo = new AdvanceConfirmationDialog
        {
            Header = new GenericDialogHeaderViewModel
            {
                Title = "Early Payday",
                Subtitle = "Get paid earlier with B9 Advance",
            },
            MidSection = new MidDialogSection
            {
                Title = CurrencyFormattingHelper.AsRounded(offer.AvailableLimit),
                Subtitle = "Available limit",
            },
            Body = new GenericDialogTwoButtonBodyViewModel
            {
                Title = "The above advance amount will be automatically deducted from your next direct deposit, " +
                        "credited to your B9 account,\n" +
                        "or\nfrom your reserve repayment method (linked debit card).",
                ButtonText = $"CONFIRM & TAKE MY B9 ADVANCE",
            }
        };

        if (isModalWindowRequired)
        {
            screenTwo.ModalWindow = ModalWindows.AddExternalCardModalWindow;
        }

        return screenTwo;
    }

    private async Task<GetAdvanceOfferResponseViewModel.PreAdvancePremiumOfferScreen> BuildExperimentScreen(User userWithOffer)
    {
        var features = await Mediator.Send(new GetConfigurationQuery(CurrentUser.UserId!.Value));

        if (!features.HasFeatureEnabled(FeaturesNames.Features.BeforeAdvancePremiumMotivationExperiment))
        {
            return null;
        }

        var tariffFamily = await _tariffPlanService.GetCurrentTariffPlanFamily(CurrentUser.UserId!.Value, CancellationToken.None);
        if (tariffFamily != TariffPlanFamily.Advance)
        {
            return null;
        }

        var offers = userWithOffer?.UserSyncedStats;
        if (offers == null)
        {
            return null;
        }

        if (offers.CurrentBasicAdvanceAmount > 100)
        {
            return null;
        }

        var random = new Random();
        var currentBasicAdvanceAmount = offers.CurrentBasicAdvanceAmount;
        var currentPremiumAdvanceAmount = offers.CurrentPremiumAdvanceAmount;
        var experimentScreen = new GetAdvanceOfferResponseViewModel.PreAdvancePremiumOfferScreen
        {
            ScreenOption = random.Next(1, 3),
            CurrentBasicAdvance = currentBasicAdvanceAmount,
            CurrentPremiumAdvance = currentPremiumAdvanceAmount,
        };

        // if the stats are not yet synced, revert to the option that does not use them
        if (currentBasicAdvanceAmount == 0 && currentPremiumAdvanceAmount == 0)
        {
            experimentScreen.ScreenOption = 1;
        }

        return experimentScreen;
    }
}
