﻿namespace BNine.Application.Aggregates.CreditLines.Queries.GetAdvanceOffer;

using BNine.Application.Aggregates.CommonModels;
using MediatR;
using Models;

public record GetAdvanceOfferQuery : IRequest<OptionViewModelResult<GetAdvanceOfferResponseViewModel>>;
