﻿namespace BNine.Application.Aggregates.CreditLines.Queries.GetCreditLineStats;

using BNine.Common;

public static class LoanHelpers
{
    public const int DurationInDays = 14;

    public static IEnumerable<DateTime> GetLoanDays()
    {
        List<DateTime> result = new List<DateTime>();
        var today = TimeZoneHelper.UtcToEasternStandartTime(DateTime.UtcNow).Date;

        for (var i = 0; i <= DurationInDays; i++)
        {
            result.Add(today.AddDays(i));
        }

        return result;
    }
}
