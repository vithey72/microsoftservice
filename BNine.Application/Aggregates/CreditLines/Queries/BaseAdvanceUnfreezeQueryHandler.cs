﻿namespace BNine.Application.Aggregates.CreditLines.Queries;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Abstractions;
using AutoMapper;
using Constants;
using Domain.Entities.PaidUserServices;
using Domain.Entities.User;
using Interfaces;
using MediatR;
using Microsoft.EntityFrameworkCore;

public class BaseAdvanceUnfreezeQueryHandler : AbstractRequestHandler
{
    public BaseAdvanceUnfreezeQueryHandler(IMediator mediator, IBNineDbContext dbContext, IMapper mapper, ICurrentUserService currentUser) : base(mediator, dbContext, mapper, currentUser)
    {
    }


    protected async Task<(PaidUserService Service, int UnfreezeCost)> SelectPaidServiceEntity(
        CancellationToken token, User user)
    {
        var unfreezeCost = Convert.ToInt32(user.AdvanceStats?.ExtensionPrice ?? 10M);
        var unfreezeChargeId = unfreezeCost switch
        {
            5 => PaidUserServices.AdvanceUnfreeze5,
            10 => PaidUserServices.AdvanceUnfreeze10,
            15 => PaidUserServices.AdvanceUnfreeze15,
            _ => PaidUserServices.AdvanceUnfreeze10, // TODO: sure?
        };

        var paidService = await DbContext
            .PaidUserServices
            .FirstOrDefaultAsync(x => x.Id == unfreezeChargeId, token);

        if (paidService == null)
        {
            throw new KeyNotFoundException($"Charge {unfreezeChargeId} is not present in DB.");
        }

        return (paidService, unfreezeCost);
    }

    protected async Task<UsedPaidUserService> GetIfAlreadyPurchased()
    {
        var existingPurchase = await DbContext.UsedPaidUserServices
            .Include(x => x.Service)
            .Include(x => x.User)
            .FirstOrDefaultAsync(x =>
                (new[]
                    {
                        PaidUserServices.AdvanceUnfreeze5,
                        PaidUserServices.AdvanceUnfreeze10,
                        PaidUserServices.AdvanceUnfreeze15
                    }
                    .Contains(x.ServiceId)
                )
                && x.ValidTo > DateTime.Now);

        return existingPurchase;
    }
}
