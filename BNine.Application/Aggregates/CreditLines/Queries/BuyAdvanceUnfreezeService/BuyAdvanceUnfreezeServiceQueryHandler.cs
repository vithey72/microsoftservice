﻿namespace BNine.Application.Aggregates.CreditLines.Queries.BuyAdvanceUnfreezeService;

using Application.Models.MBanq;
using AutoMapper;
using Constants;
using Domain.Entities.PaidUserServices;
using Domain.Entities.User;
using Interfaces;
using Interfaces.Bank.Administration;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Models;
using ServiceBusEvents.Commands.PublishB9Event;

public class BuyAdvanceUnfreezeServiceQueryHandler
    : BaseAdvanceUnfreezeQueryHandler
    , IRequestHandler<BuyAdvanceUnfreezeServiceQuery, BuyAdvanceUnfreezeServiceResponse>
{
    private readonly IBankChargesService _chargesService;

    public BuyAdvanceUnfreezeServiceQueryHandler(
        IMediator mediator,
        IBNineDbContext dbContext,
        IMapper mapper,
        ICurrentUserService currentUser,
        IBankChargesService chargesService
        )
        : base(mediator, dbContext, mapper, currentUser)
    {
        _chargesService = chargesService;
    }

    public async Task<BuyAdvanceUnfreezeServiceResponse> Handle(BuyAdvanceUnfreezeServiceQuery request, CancellationToken token)
    {
        var user = await DbContext.Users
            .Include(u => u.CurrentAccount)
            .Include(u => u.AdvanceStats)
            .FirstOrDefaultAsync(u => u.Id == CurrentUser.UserId!.Value, token);

        if (user == null)
        {
            throw new KeyNotFoundException("User is not present in DB");
        }

        if (user.AdvanceStats == null || !user.AdvanceStats.IsEligibleForExtension())
        {
            throw new InvalidOperationException("User does not have Extension option available");
        }

        var existingPurchase = await base.GetIfAlreadyPurchased();
        if (existingPurchase != null)
        {
            return new BuyAdvanceUnfreezeServiceResponse
            {
                UsedPaidUserService = existingPurchase,
            };
        }

        var (paidService, unfreezeCost) = await base.SelectPaidServiceEntity(token, user);

        var accountCharge = await _chargesService.CreateAndCollectAccountCharge(
            user.CurrentAccount.ExternalId,
            paidService.ChargeTypeId,
            user.AdvanceStats.ExtensionExpiresAt!.Value);

        if (accountCharge != null)
        {
            return await PerformPurchase(token, user, accountCharge, paidService, unfreezeCost);
        }

        return new BuyAdvanceUnfreezeServiceResponse
        {
            UsedPaidUserService = null,
        };
    }

    private async Task<BuyAdvanceUnfreezeServiceResponse> PerformPurchase(CancellationToken token, User user, SavingAccountCharge accountCharge,
        PaidUserService paidService, int unfreezeCost)
    {
        var usedChargeEntry = new UsedPaidUserService
        {
            UserId = user.Id,
            ChargeExternalId = accountCharge.Id,
            CreatedAt = DateTime.Now,
            ServiceId = paidService.Id,
            ValidTo = DateTime.Now,
        };
        var entry = DbContext.UsedPaidUserServices.Add(usedChargeEntry);

        await DbContext.SaveChangesAsync(token);
        var eventType = ServiceBusEvents.UsedPaidServiceEventPrefix + paidService.ServiceBusName +
                        ServiceBusEvents.UserPaidServicePurchasePostfix;
        await Mediator.Send(new PublishB9EventCommand(
            new UsedExtensionServiceBusMessage(entry.Entity,
                user.AdvanceStats.ExtensionExpiresAt!.Value,
                eventType, unfreezeCost, isPurchase: true),
            ServiceBusTopics.B9UsedPaidServices,
            eventType
        ), token);

        return new BuyAdvanceUnfreezeServiceResponse
        {
            UsedPaidUserService = entry.Entity,
        };
    }
}
