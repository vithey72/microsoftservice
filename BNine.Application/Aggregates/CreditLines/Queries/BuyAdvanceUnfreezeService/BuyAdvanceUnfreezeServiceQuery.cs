﻿namespace BNine.Application.Aggregates.CreditLines.Queries.BuyAdvanceUnfreezeService;

using Abstractions;
using Behaviours;
using MediatR;
using Models;

[Sequential]
public class BuyAdvanceUnfreezeServiceQuery
    : MBanqSelfServiceUserRequest
    , IRequest<BuyAdvanceUnfreezeServiceResponse>
{
}
