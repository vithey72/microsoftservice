﻿namespace BNine.Application.Aggregates.CreditLines.Queries.UnfreezeAdvanceExtraAmount;

using Abstractions;
using MediatR;
using Models;

public class UnfreezeAdvanceExtraAmountQuery
    : MBanqSelfServiceUserRequest
    , IRequest<UnfreezeAdvanceExtraAmountResultViewModel>
{
}
