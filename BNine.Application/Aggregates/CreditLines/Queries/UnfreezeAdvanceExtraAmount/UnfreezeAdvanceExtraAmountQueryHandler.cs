﻿namespace BNine.Application.Aggregates.CreditLines.Queries.UnfreezeAdvanceExtraAmount;

using Abstractions;
using AutoMapper;
using BuyAdvanceUnfreezeService;
using Commands.CreateLoanTransfer;
using Constants;
using Helpers;
using Interfaces;
using Interfaces.Bank.Administration;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Models;
using ServiceBusEvents.Commands.PublishB9Event;

public class UnfreezeAdvanceExtraAmountQueryHandler
    : AbstractRequestHandler
    , IRequestHandler<UnfreezeAdvanceExtraAmountQuery, UnfreezeAdvanceExtraAmountResultViewModel>
{
    private readonly IBankChargesService _chargesService;

    public UnfreezeAdvanceExtraAmountQueryHandler(
        IMediator mediator,
        IBNineDbContext dbContext,
        IMapper mapper,
        ICurrentUserService currentUser,
        IBankChargesService chargesService
        )
        : base(mediator, dbContext, mapper, currentUser)
    {
        _chargesService = chargesService;
    }

    public async Task<UnfreezeAdvanceExtraAmountResultViewModel> Handle(UnfreezeAdvanceExtraAmountQuery request, CancellationToken token)
    {
        var user = await DbContext.Users
            .Include(u => u.AdvanceStats)
            .Include(u => u.UsedPaidUserServices)
            .FirstOrDefaultAsync(u => u.Id == CurrentUser.UserId!.Value, token);

        var purchaseQuery = new BuyAdvanceUnfreezeServiceQuery
        {
            Ip = request.Ip,
            MbanqAccessToken = request.MbanqAccessToken,
        };
        var purchaseResult = await Mediator.Send(purchaseQuery);
        if (!purchaseResult.IsSuccess)
        {
            return new UnfreezeAdvanceExtraAmountResultViewModel
            {
                IsSuccess = false,
                Header = "Insufficient funds",
                Subtitle = $"To proceed with unlocking your B9 Advance you need to have {CurrencyFormattingHelper.AsRegular(user!.AdvanceStats.ExtensionPrice)} " +
                           "available in your account.",
                ButtonText = "ADD MONEY",
            };
        }

        var response = await Mediator.Send(new CreateLoanTransferCommand
        {
            IsExtension = true,
            IpAddress = request.Ip,
        }, token);

        var eventType = ServiceBusEvents.UsedPaidServiceEventPrefix
                        + purchaseResult.UsedPaidUserService.Service.ServiceBusName
                        + ServiceBusEvents.UserPaidServiceActivationPostfix;
        await Mediator.Send(new PublishB9EventCommand(
            new UsedExtensionServiceBusMessage(purchaseResult.UsedPaidUserService,
                user.AdvanceStats.ExtensionExpiresAt!.Value,
                eventType, response.CreditAmount, isPurchase: false),
            ServiceBusTopics.B9UsedPaidServices,
            eventType
        ), token);

        return new UnfreezeAdvanceExtraAmountResultViewModel
        {
            IsSuccess = true,
            Header = "Done",
            Subtitle = $"${response.CreditAmount} has been added to your B9 wallet. " +
                       "Remember, any time you do a direct deposit into your B9 account those funds are automatically applied to " +
                       "paying off the balance on your advance—even if it’s before the scheduled repayment date.",
            ButtonText = "DONE",
        };
    }
}
