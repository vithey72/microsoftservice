﻿namespace BNine.Application.Aggregates.CreditLines.Queries.GetAdvanceStats;

using Abstractions;
using MediatR;
using Models;

public class GetAdvanceWidgetViewModelQuery : MBanqSelfServiceUserRequest, IRequest<AdvanceWidgetViewModel>
{
}
