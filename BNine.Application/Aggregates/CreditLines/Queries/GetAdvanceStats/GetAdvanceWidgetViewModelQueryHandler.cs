﻿namespace BNine.Application.Aggregates.CreditLines.Queries.GetAdvanceStats;

using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Abstractions;
using AutoMapper;
using BNine.Application.Helpers;
using CommonModels.Buttons;
using CommonModels.Dialog;
using CommonModels.DialogElements;
using CommonModels.Text;
using Configuration.Queries.GetConfiguration;
using Constants;
using Domain.Entities.User;
using Enums.Advance;
using Enums.TariffPlan;
using Exceptions;
using Interfaces;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Models;

public class GetAdvanceWidgetViewModelQueryHandler
    : AbstractRequestHandler
        , IRequestHandler<GetAdvanceWidgetViewModelQuery, AdvanceWidgetViewModel>
{
    private const decimal MaximumPremiumAdvanceAmount = 500;
    private const decimal NextBasicAdvanceAmount = 50;
    private const decimal MaximumBasicAdvanceAmount = 100;
    private const decimal NextPremiumAdvanceAmount = 75;

    private const string DepositActionKey = "deposit";
    private const string GetAdvanceActionKey = "getAdvance";
    private const string RepaymentActionKey = "repayment";

    private const string GetAdvanceButtonElementName = "button";
    private const string RepayButtonClickAppsflyerEvent = "Wallet_Repay";
    private const string PlusDepositButtonClickAppsflyerEvent = "Wallet_Deposit";
    private const string GetAdvanceButtonClickAppsflyerEvent = "Wallet_Get_Money2";

    private readonly ITariffPlanService _tariffPlanService;
    private readonly ILoanAccountProvider _loanAccountProvider;
    private readonly IAdvanceWidgetStateProvider _advanceWidgetStateProvider;

    public GetAdvanceWidgetViewModelQueryHandler(
        IMediator mediator,
        IBNineDbContext dbContext,
        IMapper mapper,
        ICurrentUserService currentUser,
        ITariffPlanService tariffPlanService,
        ILoanAccountProvider loanAccountProvide,
        IAdvanceWidgetStateProvider advanceWidgetStateProvider
        )
        : base(mediator, dbContext, mapper, currentUser)
    {
        _tariffPlanService = tariffPlanService;
        _loanAccountProvider = loanAccountProvide;
        _advanceWidgetStateProvider = advanceWidgetStateProvider;
    }

    public async Task<AdvanceWidgetViewModel> Handle(GetAdvanceWidgetViewModelQuery request,
        CancellationToken cancellationToken)
    {
        var user = await DbContext.Users
            .Include(x => x.AdvanceStats)
            .Include(x => x.UserSyncedStats)
            .Include(u => u.LoanAccounts)
            .Where(x => x.Id == CurrentUser.UserId.Value)
            .FirstOrDefaultAsync(cancellationToken);

        if (user == null)
        {
            throw new NotFoundException(nameof(User), CurrentUser.UserId);
        }

        var planType = await _tariffPlanService.GetCurrentTariffPlanFamily(user.Id, cancellationToken);
        var response = new AdvanceWidgetViewModel();

        var widgetState = await _advanceWidgetStateProvider.GetWidgetState(user);
        var features = await Mediator.Send(new GetConfigurationQuery(), cancellationToken);
        var featureIsOn = features.HasFeatureEnabled(FeaturesNames.Features.AdvanceWidgetBanner);

        var bannerState = featureIsOn
            ? InferBannerState(user.AdvanceStats, user.UserSyncedStats)
            : AdvanceWidgetBannerState.Default;

        response.WidgetState = widgetState;
        response.UnderWidgetBanners.State = bannerState;

        await AddAdvanceActions(user, response, widgetState, bannerState);
        await AddWidgetBanner(user, response, bannerState, planType);
        AddBottomButton(response, bannerState);

        return response;
    }

    private async Task AddAdvanceActions(User user, AdvanceWidgetViewModel response, AdvanceWidgetState widgetState,
        AdvanceWidgetBannerState bannerState)
    {
        var advanceStats = user.AdvanceStats ?? new AdvanceStats();
        var generalStats = user.UserSyncedStats ?? new UserSyncedStats();
        var tooltips = BuildTooltips(bannerState);

        var depositAction = BuildDepositAction(tooltips.Last14DaysTooltip, bannerState, generalStats);

        var actionsList = new List<AdvanceWidgetViewModel.AdvanceWidgetAction>(2)
        {
            depositAction,
        };

        if (widgetState is AdvanceWidgetState.AdvanceDisbursed
            or AdvanceWidgetState.RepaymentOverdue)
        {
            var usedAdvanceAction = await BuildUsedAdvanceAction(user, widgetState);
            actionsList.Add(usedAdvanceAction);
        }
        else
        {
            var getAdvanceAction = BuildGetAdvanceAction(advanceStats, generalStats,
                tooltips.AvailableAdvanceTooltip, widgetState, bannerState);
            actionsList.Add(getAdvanceAction);
        }

        response.AdvanceWidgetActions = actionsList.ToArray();
    }

    private AdvanceWidgetViewModel.AdvanceWidgetAction BuildDepositAction(HintWithDialogViewModel hint,
        AdvanceWidgetBannerState bannerState, UserSyncedStats generalStats)
    {
        if (hint != null)
        {
            hint.EventTracking =
                new EventTrackingObject(
                    new UserEventInstructionsObject("deposit-hint", new { source = bannerState }),
                    null);
        }
        return new AdvanceWidgetViewModel.AdvanceWidgetAction
        {
            ActionKey = DepositActionKey,
            Title = "Direct Deposit (last 14 days)",
            Hint = hint,
            Amount = new AdvanceWidgetViewModel.AdvanceAmount
            {
                Value = generalStats.PayrollSumFor14Days,
                Color = AdvanceWidgetViewModel.AdvanceAmountColorEnum.Black,
            },
            Button = new ButtonViewModel("+ Deposit", ButtonStyleEnum.Bordered)
                .WithDeeplink(DeepLinkRoutes.SwitchDeposit)
                .EmitsUserEvent(null, PlusDepositButtonClickAppsflyerEvent, null),
        };
    }

    private AdvanceWidgetViewModel.AdvanceWidgetAction BuildGetAdvanceAction(
        AdvanceStats advanceStats, UserSyncedStats generalStats, HintWithDialogViewModel hint,
        AdvanceWidgetState widgetState, AdvanceWidgetBannerState bannerState)
    {
        if (hint != null)
        {
            hint.EventTracking =
                new EventTrackingObject(
                    new UserEventInstructionsObject("advance-hint", new { source = bannerState }),
                    null);
        }

        var getAdvanceAction = new AdvanceWidgetViewModel.AdvanceWidgetAction
        {
            ActionKey = GetAdvanceActionKey,
            Title = $"Advance Available",
            Hint = hint,
            Amount = new AdvanceWidgetViewModel.AdvanceAmount
            {
                Value = advanceStats.AvailableLimit,
                Color = AdvanceWidgetViewModel.AdvanceAmountColorEnum.Gray,
            },
            Button = new ButtonViewModel("Get Advance", ButtonStyleEnum.Bordered)
                .WithDeeplink(DeepLinkRoutes.Argyle)
                .EmitsUserEvent(GetAdvanceButtonElementName,
                    GetAdvanceButtonClickAppsflyerEvent,
                    new
                    {
                        source = AdvanceWidgetGetAdvanceButtonState.Fallback
                    }),
        };

        if (widgetState is AdvanceWidgetState.NoAdvance)
        {
            getAdvanceAction.Amount.Value = 0;
            if (generalStats.IsWaitingForArgyle)
            {
                getAdvanceAction.Button = getAdvanceAction.Button.Value
                    .WithDeeplink(DeepLinkRoutes.ExpectingFirstPayrollDialog)
                    with
                {
                    EventTracking = BuildGetAdvanceButtonEventTrackingObject(
                        AdvanceWidgetGetAdvanceButtonState.WaitingForArgyleData),
                };
            }
            else if (IsWaitingForFirstPaycheck(advanceStats, generalStats))
            {
                getAdvanceAction.Button = getAdvanceAction.Button.Value
                    .WithDeeplink(DeepLinkRoutes.ExpectingFirstPayrollDialog)
                    with
                {
                    EventTracking = BuildGetAdvanceButtonEventTrackingObject(
                        AdvanceWidgetGetAdvanceButtonState.WaitingForFirstPaycheck),
                };
            }
            else if (generalStats.HasPayAllocation && generalStats.PayrollSumFor30Days > 0)
            {
                getAdvanceAction.Button = getAdvanceAction.Button.Value
                    .WithDeeplink(DeepLinkRoutes.InsufficientDepositAmountDialog)
                    with
                {
                    EventTracking = BuildGetAdvanceButtonEventTrackingObject(
                        AdvanceWidgetGetAdvanceButtonState.PayAllocated),
                };
            }
        }
        else if (widgetState is AdvanceWidgetState.DelayedBoost)
        {
            getAdvanceAction.Title = "B9 Advance is on the way";
            getAdvanceAction.IconUrl = "https://b9prodaccount.blob.core.windows.net/static-documents-container/Misc/delayed-boost-clock-72-72.png";
            getAdvanceAction.Button = getAdvanceAction.Button
                    .Value
                    .WithDeeplink(DeepLinkRoutes.ImmediateBoost)
                with
                {
                    Text = "Get info",
                    EventTracking = BuildGetAdvanceButtonEventTrackingObject(
                        AdvanceWidgetGetAdvanceButtonState.DelayedBoostInfo),
                };

            var boostAmountChosenAsDelayed = advanceStats.DelayedBoostSettings.DelayedBoostId switch
            {
                3 => advanceStats.BoostLimit3,
                2 => advanceStats.BoostLimit2,
                _ => advanceStats.BoostLimit1
            };

            getAdvanceAction.Amount.Value = advanceStats.AvailableLimit + boostAmountChosenAsDelayed;
            getAdvanceAction.Amount.Color = AdvanceWidgetViewModel.AdvanceAmountColorEnum.Gray;

        }
        else if (widgetState is AdvanceWidgetState.AdvanceAvailable or AdvanceWidgetState.BoostAvailable)
        {
            getAdvanceAction.Button = getAdvanceAction.Button
                .Value
                .WithDeeplink(DeepLinkRoutes.AdvanceDisbursal)
                with
            {
                Text = "Cash Out",
                EventTracking = BuildGetAdvanceButtonEventTrackingObject(
                    AdvanceWidgetGetAdvanceButtonState.CashOut),
            };
            getAdvanceAction.Amount.Color = AdvanceWidgetViewModel.AdvanceAmountColorEnum.Green;
        }
        else if (widgetState is AdvanceWidgetState.ExtensionAvailable)
        {
            getAdvanceAction.Amount.Color = AdvanceWidgetViewModel.AdvanceAmountColorEnum.Black;
            getAdvanceAction.AlternativeAmount = new AdvanceWidgetViewModel.AdvanceAmount
            {
                Value = advanceStats.AvailableLimit + advanceStats.ExtensionLimit,
                Color = AdvanceWidgetViewModel.AdvanceAmountColorEnum.Green,
            };
            getAdvanceAction.Button = getAdvanceAction.Button.Value
                .WithDeeplink(DeepLinkRoutes.AdvanceExtension)
                with
            {
                Text = "+ Unfreeze",
                EventTracking = BuildGetAdvanceButtonEventTrackingObject(
                    AdvanceWidgetGetAdvanceButtonState.Extension),
            };
        }

        return getAdvanceAction;
    }

    private static EventTrackingObject BuildGetAdvanceButtonEventTrackingObject(AdvanceWidgetGetAdvanceButtonState state)
    {
        return new EventTrackingObject(
            new UserEventInstructionsObject(GetAdvanceButtonElementName, new
            {
                source = state
            }), GetAdvanceButtonClickAppsflyerEvent);
    }

    private async Task<AdvanceWidgetViewModel.AdvanceWidgetAction> BuildUsedAdvanceAction(
        User user, AdvanceWidgetState widgetState)
    {
        var externalLoanAccount = await _loanAccountProvider.TryGetActiveLoanAccount(user);

        var hasBoost = externalLoanAccount is { FeeAmount: > 0m };

        var principalOutstandingAmount = Convert.ToDecimal(externalLoanAccount?.PrincipalOutstandingAmount);
        var feeOutstandingAmount = externalLoanAccount?.FeeOutstandingAmount ?? 0m;
        var totalOutstandingAmount = principalOutstandingAmount + feeOutstandingAmount;

        var usedAdvanceAction = new AdvanceWidgetViewModel.AdvanceWidgetAction
        {
            ActionKey = RepaymentActionKey,
            Title = hasBoost ? "Total Due" : "Advance Used",
            Amount = new AdvanceWidgetViewModel.AdvanceAmount
            {
                Value = totalOutstandingAmount,
                Color = AdvanceWidgetViewModel.AdvanceAmountColorEnum.Green,
            },
            Button = new ButtonViewModel("Repay", ButtonStyleEnum.Bordered)
                .WithDeeplink(DeepLinkRoutes.AdvanceRepayment)
                .EmitsUserEvent(null, RepayButtonClickAppsflyerEvent, null),
        };

        if (widgetState == AdvanceWidgetState.RepaymentOverdue)
        {
            usedAdvanceAction.Amount.Color = AdvanceWidgetViewModel.AdvanceAmountColorEnum.Red;
            usedAdvanceAction.Amount.Value = -usedAdvanceAction.Amount.Value;
            usedAdvanceAction.Button = usedAdvanceAction.Button.Value with { Style = ButtonStyleEnum.Solid };
        }

        if (hasBoost)
        {
            var advanceToRepay = Math.Max(0m, totalOutstandingAmount - externalLoanAccount.FeeAmount);
            var feeToRepay = totalOutstandingAmount - advanceToRepay;

            var titlePlaceholder = "{TITLE}";
            var repayAmountPlaceholder = "{REPAY}";
            var feeAmountPlaceholder = "{FEE}";
            usedAdvanceAction.DropdownText = new FormattedText(
                $"{titlePlaceholder} \nAdvance Repayment: {repayAmountPlaceholder} + Express Fee: {feeAmountPlaceholder}",
                new FormattedTextFragmentViewModel[]
                {
                    new(titlePlaceholder, "Total Due includes:", Color: ThematicColor.BrandPrimary),
                    new(repayAmountPlaceholder, CurrencyFormattingHelper.AsRegular(advanceToRepay), Color: ThematicColor.BrandPrimary),
                    new(feeAmountPlaceholder, CurrencyFormattingHelper.AsRegular(feeToRepay), Color: ThematicColor.BrandPrimary),
                }
                );
        }

        return usedAdvanceAction;
    }

    private AdvanceWidgetBannerState InferBannerState(AdvanceStats advanceStats, UserSyncedStats generalStats)
    {
        advanceStats ??= new AdvanceStats();
        generalStats ??= new UserSyncedStats();

        var newUserState = !generalStats.HasPayAllocation
                           && generalStats.AdvancesTakenCount == 0
                           && advanceStats.AvailableLimit == 0;
        if (newUserState)
        {
            return AdvanceWidgetBannerState.AttentionRequiredNewUser;
        }

        var waitingForArgyle = generalStats.HasPayAllocation
                               && generalStats.IsWaitingForArgyle;
        if (waitingForArgyle)
        {
            return AdvanceWidgetBannerState.WaitingForArgyleData;
        }

        if (IsWaitingForFirstPaycheck(advanceStats, generalStats))
        {
            return AdvanceWidgetBannerState.WaitingForFirstPaycheck;
        }

        var insufficientPayroll = generalStats.PayrollSumFor14Days is > 0 and < 300;
        if (insufficientPayroll)
        {
            return AdvanceWidgetBannerState.AttentionRequiredInsufficientDeposit;
        }

        return AdvanceWidgetBannerState.NextAdvanceMotivation;
    }

    private (HintWithDialogViewModel Last14DaysTooltip,
        HintWithDialogViewModel AvailableAdvanceTooltip) BuildTooltips(AdvanceWidgetBannerState state)
    {
        return state switch
        {
            AdvanceWidgetBannerState.AttentionRequiredNewUser or AdvanceWidgetBannerState.WaitingForArgyleData or AdvanceWidgetBannerState.WaitingForFirstPaycheck
                => (BuildYouNeedToStartDialog(state), BuildYouDontQualifyYetDialog(state)),

            AdvanceWidgetBannerState.AttentionRequiredInsufficientDeposit or AdvanceWidgetBannerState.NextAdvanceMotivationBeforeDeposit
                => (BuildKeepYourDepositLinkedDialog(state), BuildTapIntoMoreCashDialog(state)),

            AdvanceWidgetBannerState.NextAdvanceMotivation
                => (BuildKeepYourDepositLinkedDialog(state, singeButton: true), BuildTapIntoMoreCashDialog(state, singeButton: true)),

            _ => (null, null),
        };
    }

    private async Task AddWidgetBanner(User user, AdvanceWidgetViewModel response, AdvanceWidgetBannerState state, TariffPlanFamily planType)
    {
        if (state is AdvanceWidgetBannerState.AttentionRequiredNewUser)
        {
            // remove when V2 banner is forced
            response.UnderWidgetBanners.RequiresActionBanner = BuildInsufficientDepositBanner(state);
            response.UnderWidgetBanners.NewUserBanner = BuildNewUserBanner();
        }

        if (state is AdvanceWidgetBannerState.AttentionRequiredInsufficientDeposit)
        {
            response.UnderWidgetBanners.RequiresActionBanner = BuildInsufficientDepositBanner(state);
        }

        if (state == AdvanceWidgetBannerState.WaitingForArgyleData)
        {
            response.UnderWidgetBanners.WaitingForArgyleResponseBanner = BuildWaitingForArgyleBanner();
        }

        if (state == AdvanceWidgetBannerState.WaitingForFirstPaycheck)
        {
            response.UnderWidgetBanners.WaitingForFirstPaycheckBanner = BuildWaitingForFirstPaycheckBanner();
        }

        if (state is AdvanceWidgetBannerState.NextAdvanceMotivation or AdvanceWidgetBannerState.NextAdvanceMotivationBeforeDeposit)
        {
            response.UnderWidgetBanners.NextAdvanceMotivationSection = await BuildNextAdvanceMotivationSection(user, planType, state);
        }
    }

    private void AddBottomButton(AdvanceWidgetViewModel response, AdvanceWidgetBannerState bannerState)
    {
        var text = "How to increase your Advance";

        response.BottomLink = new ButtonViewModel(text, ButtonStyleEnum.Unknown)
            .WithDeeplink(DeepLinkRoutes.B9Score)
            .EmitsUserEvent("b9-score", null, new
            {
                source = bannerState
            });
    }

    private HintWithDialogViewModel BuildYouNeedToStartDialog(AdvanceWidgetBannerState state)
    {
        return new HintWithDialogViewModel
        {
            Title = "You need to start using your B9 Account to unlock your Advance!",
            Subtitle = new LinkedText(
                "If you are employed, we need to see that your paycheck is regularly deposited to your B9 Account. Each deposit must be at least $300. We require your total deposits to be at least $600 a month.\n\n" +
                "Multiple employers and side hustles count! Government benefits also qualify! Please inform our Customer Support Team if you already have any of these on your Account."),
            Buttons = new[]
            {
                new ButtonViewModel("GOT IT!", ButtonStyleEnum.Solid),
                new ButtonViewModel("GET SUPPORT", ButtonStyleEnum.Bordered)
                    .WithDeeplink(DeepLinkRoutes.Support)
                    .EmitsUserEvent(
                        "deposit-hint-support",
                        null,
                        new { source = state }),
            },
        };
    }

    private HintWithDialogViewModel BuildYouDontQualifyYetDialog(AdvanceWidgetBannerState state)
    {
        return new HintWithDialogViewModel
        {
            Title = "You do not qualify for an Advance yet.\nBut you're so close!",
            Subtitle = new LinkedText(
                "B9 members qualify for an Advance of up to $500 after they connect their recurring direct deposit and start using their B9 account. Each deposit must be at least $300. We require your total deposits to be at least $600 a month.\n\n" +
                "Multiple employers and side hustles count! Government benefits also qualify! Please inform our Customer Support Team if you already have any of these on your account."),
            Buttons = new[]
            {
                new ButtonViewModel("GOT IT!", ButtonStyleEnum.Solid),
                new ButtonViewModel("GET SUPPORT", ButtonStyleEnum.Bordered)
                    .WithDeeplink(DeepLinkRoutes.Support)
                    .EmitsUserEvent(
                        "advance-hint-support",
                        null,
                        new { source = state }),
            }
        };
    }

    private HintWithDialogViewModel BuildKeepYourDepositLinkedDialog(AdvanceWidgetBannerState state, bool singeButton = false)
    {
        var subtitleText =
            "If you are employed, we need to see that your paycheck is regularly deposited to your B9 Account. Each deposit must be at least $300.\n" +
            "We require your total deposits to be at least $600 a month.\n\n" +
            "Multiple employers and side hustles count! Government benefits also qualify!";
        var buttons = new List<ButtonViewModel>
        {
            new ("GOT IT!", ButtonStyleEnum.Solid),
        };

        if (!singeButton)
        {
            subtitleText += " Please inform our Customer Support Team if you already have any of these on your account.";
            buttons.Add(new ButtonViewModel("GET SUPPORT", ButtonStyleEnum.Bordered)
                .WithDeeplink(DeepLinkRoutes.Support)
                .EmitsUserEvent(
                    "deposit-hint-support",
                    null,
                    new { source = state })
                );
        }

        return new HintWithDialogViewModel
        {
            Title = "Keep your direct deposit linked to your B9 Account!",
            Subtitle = new LinkedText(subtitleText),
            Buttons = buttons.ToArray(),
        };
    }

    private HintWithDialogViewModel BuildTapIntoMoreCashDialog(AdvanceWidgetBannerState state, bool singeButton = false)
    {
        var subtitleText =
            "After you connect your recurring direct deposit, you can increase your Advance limit and cash out more next time.Then, add more funds in recurring direct deposits into your B9 account.\n" +
            "We require your total deposits to be at least $600 a month.\n" +
            "Deposits from multiple employers count! Government benefits also qualify!";
        var buttons = new List<ButtonViewModel>
        {
            new ButtonViewModel("+ DEPOSIT", ButtonStyleEnum.Solid)
                .WithDeeplink(DeepLinkRoutes.SwitchDeposit)
                .EmitsUserEvent(
                    "advance-hint-deposit",
                    null,
                    new { source = state }),
        };

        if (!singeButton)
        {
            subtitleText += " Please inform our Customer Support Team if you already have any of these on your account.";
            buttons.Add(new ButtonViewModel("GET SUPPORT", ButtonStyleEnum.Bordered)
                .WithDeeplink(DeepLinkRoutes.Support)
                .EmitsUserEvent(
                    "advance-hint-support",
                    null,
                    new { source = state })
            );
        }

        return new HintWithDialogViewModel
        {
            Title = "Tap into more cash for you!",
            Subtitle = new LinkedText(subtitleText),
            Buttons = buttons.ToArray(),
        };
    }

    private AdvanceWidgetViewModel.RequiresActionBanner BuildInsufficientDepositBanner(AdvanceWidgetBannerState state)
    {
        return new AdvanceWidgetViewModel.RequiresActionBanner
        {
            ButtonText = "\u26A0\uFE0F Action Required!", // ⚠ warning sign
            RequiresActionDialog = new GenericDialogRichBodyViewModel
            {
                Title = "Action Required!",
                Subtitle = new LinkedText(
                    "You need to add more funds to your recurring direct deposit to qualify for an Advance.\n" +
                    "If you are employed, we need to see that your paycheck is regularly deposited to your B9 account. Each deposit must be at least $300. We require your total deposits to be at least $600 a month.\n" +
                    "Any recurring direct deposits from multiple employers, side hustles, income from rentals or tutoring, and government benefits also qualify! Please inform our Customer Support Team if you already have any of these on your account."),
                Buttons = new[]
                {
                    new ButtonViewModel("+ DEPOSIT", ButtonStyleEnum.Solid)
                        .WithDeeplink(DeepLinkRoutes.SwitchDeposit)
                        .EmitsUserEvent(
                            "action-required-deposit",
                            null,
                            new { source = state }),
                    new ButtonViewModel("GET SUPPORT", ButtonStyleEnum.Bordered)
                        .WithDeeplink(DeepLinkRoutes.Support)
                        .EmitsUserEvent(
                            "action-required-support",
                            null,
                            new { source = state }),
                }
            }
        };
    }

    private AdvanceWidgetViewModel.RequiresActionBannerV2 BuildNewUserBanner()
    {
        var placeholder = "{UPTO500}";
        return new AdvanceWidgetViewModel.RequiresActionBannerV2
        {
            FormattedText = new FormattedText($"You're approved for\n{placeholder} Cash Advance",
                new FormattedTextFragmentViewModel[]
                    {
                        new(placeholder, "up to $500", TypographicalEmphasis.Black, ThematicColor.BrandPrimary)
                    }),
            IconUrl = "https://b9prodaccount.blob.core.windows.net/static-documents-container/AdvanceWidget/awb_speedometer_icon.png",
            Button = new ButtonViewModel("Activate Your Advance", ButtonStyleEnum.Solid)
                .WithDeeplink(DeepLinkRoutes.WalletAdvanceDescription)
                .EmitsUserEvent("new-users-banner", null, null),
        };
    }

    private GenericDialogRichBodyViewModel BuildWaitingForArgyleBanner()
    {
        return new GenericDialogRichBodyViewModel
        {
            ImageUrl = "https://b9prodaccount.blob.core.windows.net/static-documents-container/AdvanceWidget/waiting_for_agyle.png",
            Title = "Your Advance is being unlocked!",
            Subtitle = new LinkedText("It usually takes just a few seconds but sometimes it may take up to one hour. " +
                                      "We are verifying a few things about you. Please wait."),
            Buttons = null,
        };
    }

    private GenericDialogRichBodyViewModel BuildWaitingForFirstPaycheckBanner()
    {
        return new GenericDialogRichBodyViewModel
        {
            ImageUrl = "https://b9prodaccount.blob.core.windows.net/static-documents-container/AdvanceWidget/waiting_for_paycheck.png",
            Title = "Wait for your deposit to arrive in your B9 account!",
            Subtitle = new LinkedText("To unlock your first Advance we need to wait until your first direct deposit hits your B9 Account."),
            Buttons = null,
        };
    }

    private async Task<AdvanceWidgetViewModel.NextAdvanceMotivationSection> BuildNextAdvanceMotivationSection(
        User user, TariffPlanFamily planType, AdvanceWidgetBannerState state)
    {
        var generalStats = user.UserSyncedStats;

        var response = new AdvanceWidgetViewModel.NextAdvanceMotivationSection
        {
            DisclaimerText = "Your Next Advance Limit",
            IconUrl = "https://b9prodaccount.blob.core.windows.net/static-documents-container/AdvanceWidget/nextMotivationBannerSpeedometerIcon.png"
        };

        // Fill amounts with 0 in case DWH sent no update to the UserSyncedStats
        var nextBasicAdvanceAmountMilestone = generalStats == null
            ? (int)NextBasicAdvanceAmount
            : (int)generalStats.NextBasicAdvanceAmount;
        var nextPremiumAdvanceAmountMilestone = generalStats == null
            ? (int)NextPremiumAdvanceAmount
            : (int)generalStats.NextPremiumAdvanceAmount;
        var nextBasicAdvanceAmount = generalStats?.NextBasicAdvanceAmount ?? NextBasicAdvanceAmount;
        var nextPremiumAdvanceAmount = generalStats?.NextPremiumAdvanceAmount ?? NextPremiumAdvanceAmount;
        var maximumBasicAdvanceAmount = generalStats?.MaximumBasicAdvanceAmount ?? MaximumBasicAdvanceAmount;

        var basicBlock = new AdvanceViewModelBase()
        {
            Title = nextBasicAdvanceAmountMilestone == maximumBasicAdvanceAmount ? "Max for Basic" : "Basic Plan",
            NextAmount = nextBasicAdvanceAmount,
            Color = "009C3D",
            TariffPlanFamilyType = TariffPlanFamily.Advance
        };

        var premiumBlock = new AdvanceViewModelBase
        {
            Title = "Premium Plan",
            DeeplinkObject = new DeeplinkObject(DeepLinkRoutes.PremiumTariffPlan, new { }),
            EventTracking = new EventTrackingObject(
                new UserEventInstructionsObject("premium-switch-now", new { source = state }), null),
            NextAmount = nextPremiumAdvanceAmount,
            Color = "14273A",
            TariffPlanFamilyType = TariffPlanFamily.Premium
        };

        var premiumSimplifiedBlock = new AdvanceViewModelBase
        {
            NextAmount = nextPremiumAdvanceAmount,
            Color = "14273A",
            TariffPlanFamilyType = TariffPlanFamily.Premium,
            Subtitle = "Your next advance unlocks when direct deposit hits your B9 account"
        };

        if (planType == TariffPlanFamily.Premium)
        {
            response.NextAdvanceInfoSections = new[] { premiumSimplifiedBlock };
            return response;
        }

        response.NextAdvanceInfoSections = new[] { basicBlock, premiumBlock };

        return response;
    }

     private static bool IsWaitingForFirstPaycheck(AdvanceStats advanceStats, UserSyncedStats generalStats)
     {
         return generalStats.HasPayAllocation
                && generalStats.PayrollsTotalCount == 0
                && generalStats.AdvancesTakenCount == 0
                && advanceStats.AvailableLimit == 0;
     }
}
