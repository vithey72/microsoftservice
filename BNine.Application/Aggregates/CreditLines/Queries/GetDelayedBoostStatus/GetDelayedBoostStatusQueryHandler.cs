﻿namespace BNine.Application.Aggregates.CreditLines.Queries.GetDelayedBoostStatus;

using Abstractions;
using AutoMapper;
using CommonModels;
using CommonModels.Buttons;
using CommonModels.Text;
using Enums.Advance;
using Helpers;
using Interfaces;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Models;
using Services.DelayedBoostDateTimeCalculator;

public class GetDelayedBoostStatusQueryHandler : AbstractRequestHandler,
    IRequestHandler<GetDelayedBoostStatusQuery, OptionViewModelResult<DelayedBoostStatusDialog>>
{
    private readonly ICurrentAppVersionService _currentAppVersionService;
    private readonly IAdvanceWidgetStateProvider _advanceWidgetStateProvider;

    public GetDelayedBoostStatusQueryHandler(
        IMediator mediator,
        IBNineDbContext dbContext,
        IMapper mapper,
        ICurrentUserService currentUser,
        ICurrentAppVersionService currentAppVersionService,
        IAdvanceWidgetStateProvider advanceWidgetStateProvider
        )
        :base(mediator, dbContext, mapper, currentUser)
    {
        _currentAppVersionService = currentAppVersionService;
        _advanceWidgetStateProvider = advanceWidgetStateProvider;
    }

    public async Task<OptionViewModelResult<DelayedBoostStatusDialog>> Handle(GetDelayedBoostStatusQuery request,
        CancellationToken cancellationToken)
    {
        var userId = CurrentUser.UserId!.Value;

        var userWithOffer = await DbContext.Users
            .Include(u => u.AdvanceStats)
            .SingleAsync(u => u.Id == userId, cancellationToken);

        var widgetState = await _advanceWidgetStateProvider.GetWidgetState(userWithOffer);
        if (widgetState != AdvanceWidgetState.DelayedBoost)
        {
            return OptionViewModelResult<DelayedBoostStatusDialog>.Empty;
        }

        var offer = userWithOffer.AdvanceStats;
        var delayedBoostSettings = userWithOffer.AdvanceStats.DelayedBoostSettings;


        var (boostAmount, boostPrice) = delayedBoostSettings.DelayedBoostId switch
        {
            2 => (offer.BoostLimit2, offer.BoostLimit2Price),
            3 => (offer.BoostLimit3, offer.BoostLimit3Price),
            _ => (offer.BoostLimit1, offer.BoostLimit1Price)
        };

        var advanceAmount = offer.AvailableLimit;
        var daysToPayOff = DelayedBoostDateTimeCalculationService.CalculateBusinessDaysToPayOff(DateTime.UtcNow, delayedBoostSettings.ActivationDate);
        var viewModelToReturn = new  DelayedBoostStatusDialog()
        {
            Title = "Your B9 Advance will be credited to your B9 account in",
            Subtitle = new LinkedText($"{daysToPayOff} business days ({delayedBoostSettings.ActivationDate:MM.dd.yyyy})"),
            Text = "To get it sooner, choose the option:",
            SubText = "You will be charge immediately",
            ImageUrl = "https://b9prodaccount.blob.core.windows.net/static-documents-container/Misc/delayed-boost-calendar-363-347.png",
            Options = BuildBoostOptionModelsClassified(advanceAmount, boostAmount, boostPrice, delayedBoostSettings.DelayedBoostId)
        };

        return new OptionViewModelResult<DelayedBoostStatusDialog>(viewModelToReturn);
    }

    private BoostOptionModelClassified[] BuildBoostOptionModelsClassified(decimal advanceAmount, decimal boostAmount, decimal boostPrice, int boostId)
    {

        var finalAmount = advanceAmount + boostAmount;

        // is required for backwards compatibility
        var freeOptionButton = _currentAppVersionService.AppVersion != null && _currentAppVersionService.AppVersion >= Version.Parse("2.46.0")
            ? new ButtonViewModel($"CANCEL MY B9 BOOST ADVANCE",
                ButtonStyleEnum.Bordered).EmitsUserEvent("cancel-free-boost-advance", null, null)
            : new ButtonViewModel($"GET {CurrencyFormattingHelper.AsRounded(advanceAmount)} NOW (FOR FREE)",
                ButtonStyleEnum.Bordered).EmitsUserEvent("get-free-pay-button", null, null);

        return new BoostOptionModelClassified[]
        {
            new BoostOptionModelClassified()
            {
                Id = boostId,
                IsFree = false,
                Amount = finalAmount,
                Button = new ButtonViewModel($"GET {CurrencyFormattingHelper.AsRounded(finalAmount)} " +
                                             $"NOW ({CurrencyFormattingHelper.AsRegular(boostPrice)} EXPRESS FEE)",
                    ButtonStyleEnum.Solid).EmitsUserEvent("get-express-pay-button", null, null),
                Disclaimer = "You will be charge immediately."

            },
            new BoostOptionModelClassified()
            {
                Id = boostId,
                IsFree = true,
                Amount = advanceAmount,
                Button = freeOptionButton
            }
        };
    }
}
