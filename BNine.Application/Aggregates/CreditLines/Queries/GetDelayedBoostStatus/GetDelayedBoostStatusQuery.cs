﻿namespace BNine.Application.Aggregates.CreditLines.Queries.GetDelayedBoostStatus;

using CommonModels;
using MediatR;
using Models;

public record GetDelayedBoostStatusQuery(): IRequest<OptionViewModelResult<DelayedBoostStatusDialog>>;
