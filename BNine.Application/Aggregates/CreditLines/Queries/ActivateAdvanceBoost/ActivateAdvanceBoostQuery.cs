﻿namespace BNine.Application.Aggregates.CreditLines.Queries.ActivateAdvanceBoost;

using Abstractions;
using CommonModels.Dialog;
using MediatR;


public class ActivateAdvanceBoostQuery
    : MBanqSelfServiceUserRequest
    , IRequest<EitherDialog<GenericSuccessDialog, GenericFailDialog>>
{
    public int BoostId
    {
        get;
        set;
    } = 1;

    public bool IsReadyToDisburse
    {
        get;
        set;
    } = true;

    public bool IsDelayedBoost
    {
        get;
        set;
    } = false;

    public Guid? UserId
    {
        get;
        set;
    } = null;
}
