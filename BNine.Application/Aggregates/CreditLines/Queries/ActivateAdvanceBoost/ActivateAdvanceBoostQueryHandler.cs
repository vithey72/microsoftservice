﻿namespace BNine.Application.Aggregates.CreditLines.Queries.ActivateAdvanceBoost;

using Abstractions;
using AutoMapper;
using Commands.CreateLoanTransfer;
using CommonModels.Dialog;
using Constants;
using Domain.Entities.User;
using Enums;
using Helpers;
using Interfaces;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using ServiceBusEvents.Commands.PublishB9Event;

public class ActivateAdvanceBoostQueryHandler
    : AbstractRequestHandler
    , IRequestHandler<ActivateAdvanceBoostQuery, EitherDialog<GenericSuccessDialog, GenericFailDialog>>
{
    private readonly IDateTimeCalculationService _dateTimeCalculationService;


    public ActivateAdvanceBoostQueryHandler(
        IMediator mediator,
        IBNineDbContext dbContext,
        IMapper mapper,
        ICurrentUserService currentUser,
        IDateTimeCalculationService dateTimeCalculationService
        )
        : base(mediator, dbContext, mapper, currentUser)
    {
        _dateTimeCalculationService = dateTimeCalculationService;
    }

    public async Task<EitherDialog<GenericSuccessDialog, GenericFailDialog>> Handle(ActivateAdvanceBoostQuery request, CancellationToken cancellationToken)
    {
        var userId = request.UserId ?? CurrentUser.UserId!.Value;

        var userWithOffer = await DbContext.Users
            .Include(u => u.AdvanceStats)
            .Include(u => u.CurrentAccount)
            .SingleAsync(u => u.Id == userId, cancellationToken);
        var offer = userWithOffer!.AdvanceStats;

        if (offer == null || !offer.HasBoostLimit)
        {
            throw new InvalidOperationException("User does not have a Boost offer");
        }

        if (request.IsReadyToDisburse && request.IsDelayedBoost && offer.DelayedBoostSettings == null)
        {
            throw new InvalidOperationException("User does not have a Delayed Boost offer");
        }

        var existingLoan = await DbContext.LoanAccounts
            .FirstOrDefaultAsync(x =>
                x.UserId == userWithOffer.Id &&
                x.Status == LoanAccountStatus.Active,
                cancellationToken);
        if (existingLoan != null)
        {
            throw new InvalidOperationException($"The loan with Id = {existingLoan.ExternalId} is already active for user. Cannot disburse a new one.");
        }

        var (amount, price, hoursToDelay) = request.BoostId switch
        {
            2 => (offer.BoostLimit2, offer.BoostLimit2Price, offer.BoostLimit2HoursToDelay),
            3 => (offer.BoostLimit3, offer.BoostLimit3Price, offer.BoostLimit3HoursToDelay),
            _ => (offer.BoostLimit1, offer.BoostLimit1Price, offer.BoostLimit1HoursToDelay),
        };

        var boostHoursToDelayPairs = new[]
        {
            (request.BoostId, hoursToDelay),
        };

        // Initialize the service with the requested boost id and hoursToDelay pair
        // This is required for consistent result in all places where the datetime calculation is required
        _dateTimeCalculationService.Initialize(boostHoursToDelayPairs);

        if (amount == 0 || price == 0)
        {
            throw new InvalidOperationException(
                $"Advance Boost Option {request.BoostId} is not offered to the client");
        }

        var targetDate = _dateTimeCalculationService.GetTargetDate(request.BoostId);

        if (!request.IsReadyToDisburse)
        {
            if (hoursToDelay == 0)
            {
                throw new InvalidOperationException(
                    $"Delayed Advance Boost Option {request.BoostId} is not offered to the client");
            }

            await Mediator.Send(new PublishB9EventCommand(
                new UsedAdvanceBoostServiceBusMessage
                {
                    BoostId = request.BoostId,
                    BoostAmount = amount,
                    BoostPrice = price,
                    BoostActivatedAt = DateTime.UtcNow,
                    BoostActivatedAtSchedule = targetDate,
                    OfferId = offer.OfferId,
                    UserId = userWithOffer.Id,
                    LoanId = null,
                },
                ServiceBusTopics.B9AdvanceBoost,
                ServiceBusEvents.B9AdvanceBoostActivatedSchedule
            ), cancellationToken);

            userWithOffer.AdvanceStats.DelayedBoostSettings = new DelayedBoostSettings()
            {
                DelayedBoostId = request.BoostId,
                ReadyToDisburse = false,
                ActivationDate = targetDate,
                SyncedWithData = false
            };

            await DbContext.SaveChangesAsync(cancellationToken);
        }
        else
        {
            var createLoanCommand = new CreateLoanTransferCommand
            {
                BoostAmount = Convert.ToInt32(amount),
                BoostPrice = 0m,
                IpAddress = request.Ip,
            };

            if (request.IsDelayedBoost)
            {
                createLoanCommand.UserId = userId;
                createLoanCommand.IsDelayedBoost = true;
            }
            else
            {
                createLoanCommand.BoostPrice = price;
            }

            var loan = await Mediator.Send(createLoanCommand, cancellationToken);

            await Mediator.Send(new PublishB9EventCommand(
                new UsedAdvanceBoostServiceBusMessage
                {
                    BoostAmount = amount,
                    BoostPrice = price,
                    BoostActivatedAt = DateTime.UtcNow,
                    OfferId = offer.OfferId,
                    UserId = userWithOffer.Id,
                    LoanId = loan.ExternalLoanId,
                },
                ServiceBusTopics.B9AdvanceBoost,
                ServiceBusEvents.B9AdvanceBoostActivated
            ), cancellationToken);
        }

        var amtToDisburse = CurrencyFormattingHelper.AsRounded(userWithOffer.AdvanceStats.AvailableLimit + amount);
        var subtitle = !request.IsReadyToDisburse ?
            $"{amtToDisburse} will be added to your B9 Account in {_dateTimeCalculationService.GetDaysToPayOff(request.BoostId)} business days.  Remember, your next paycheck direct deposit will automatically go towards repaying it."
            :$"{amtToDisburse} has been added to your B9 Account.  Remember, your next paycheck direct deposit will automatically go towards repaying it.";

        return new EitherDialog<GenericSuccessDialog, GenericFailDialog>(
            new GenericSuccessDialog
            {
                Header = new GenericDialogHeaderViewModel
                {
                    Title = "Early Payday",
                    Subtitle = $"Get paid earlier with B9 Advance",
                },
                Body = new GenericDialogBodyViewModel
                {
                    Title = "Well Done!",
                    Subtitle = subtitle,
                    ButtonText = "DONE",
                }
            });
    }
}

public struct UsedAdvanceBoostServiceBusMessage
{

    [JsonProperty("boostId")]
    public int? BoostId
    {
        get;
        set;
    }

    [JsonProperty("userId")]
    public Guid UserId
    {
        get;
        set;
    }

    [JsonProperty("offerId")]
    public string OfferId
    {
        get;
        set;
    }

    [JsonProperty("loanId")]
    public int? LoanId
    {
        get;
        set;
    }

    // should not be sent when the boost is canceled
    // use canceledAt instead
    [JsonProperty("boostActivatedAt")]
    public DateTime? BoostActivatedAt
    {
        get;
        set;
    }

    [JsonProperty("boostActivatedAtSchedule")]
    public DateTime? BoostActivatedAtSchedule
    {
        get;
        set;
    }

    [JsonProperty("boostPrice")]
    public decimal BoostPrice
    {
        get;
        set;
    }

    [JsonProperty("boostAmount")]
    public decimal BoostAmount
    {
        get;
        set;
    }

    [JsonProperty("canceledAt")]
    public DateTime? CanceledAt
    {
        get;
        set;
    }
}
