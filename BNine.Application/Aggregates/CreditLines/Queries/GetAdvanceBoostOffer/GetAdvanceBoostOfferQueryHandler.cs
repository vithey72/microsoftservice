﻿namespace BNine.Application.Aggregates.CreditLines.Queries.GetAdvanceBoostOffer;

using Abstractions;
using AutoMapper;
using Domain.Entities.User;
using Helpers;
using Interfaces;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Models;

public class GetAdvanceBoostOfferQueryHandler
    : AbstractRequestHandler
        , IRequestHandler<GetAdvanceBoostOfferQuery, GetAdvanceBoostResponseViewModel>
{
    public GetAdvanceBoostOfferQueryHandler(
        IMediator mediator,
        IBNineDbContext dbContext,
        IMapper mapper,
        ICurrentUserService currentUser
    ) : base(mediator, dbContext, mapper, currentUser)
    {
    }

    public async Task<GetAdvanceBoostResponseViewModel> Handle(GetAdvanceBoostOfferQuery request,
        CancellationToken cancellationToken)
    {
        var userWithOffer = await DbContext.Users
            .Include(u => u.AdvanceStats)
            .SingleAsync(u => u.Id == CurrentUser.UserId!.Value, cancellationToken);
        var offer = userWithOffer!.AdvanceStats;
        if (offer == null || !offer.HasBoostLimit)
        {
            throw new InvalidOperationException("User has no Boost offered.");
        }

        return new GetAdvanceBoostResponseViewModel
        {
            Header = "Boost your limit",
            Disclaimer = "You will be charged immediately",
            Options = BuildBoostOptions(offer),
        };
    }

    private static GetAdvanceBoostResponseViewModel.BoostOption[] BuildBoostOptions(AdvanceStats offer)
    {
        var boostOptions = new List<GetAdvanceBoostResponseViewModel.BoostOption>
        {
            CreateBoostOption(
                offer.AvailableLimit,
                offer.BoostLimit1,
                offer.BoostLimit1Price,
                1),
        };
        if (offer.BoostLimit2 != 0 && offer.BoostLimit2Price != 0)
        {
            boostOptions.Add(CreateBoostOption(
                offer.AvailableLimit,
                offer.BoostLimit2,
                offer.BoostLimit2Price,
                2));
        }

        if (offer.BoostLimit3 != 0 && offer.BoostLimit3Price != 0)
        {
            boostOptions.Add(CreateBoostOption(
                offer.AvailableLimit,
                offer.BoostLimit3,
                offer.BoostLimit3Price,
                3));
        }

        return boostOptions.ToArray();
    }

    private static GetAdvanceBoostResponseViewModel.BoostOption CreateBoostOption(
        decimal advanceLimit, decimal boostLimit, decimal boostPrice, int boostId, string discountBadge = null)
    {
        var sumLimit = advanceLimit + boostLimit;
        return new()
        {
            Amount = advanceLimit + boostLimit,
            Price = boostPrice,
            Id = boostId,
            DiscountBadge = discountBadge,
            ButtonText = $"PAY {CurrencyFormattingHelper.AsRegular(boostPrice)} & " +
                         $"TAKE {CurrencyFormattingHelper.AsRounded(sumLimit)} Advance"
        };
    }
}
