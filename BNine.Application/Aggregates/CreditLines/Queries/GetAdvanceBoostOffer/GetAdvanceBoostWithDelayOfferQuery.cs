﻿namespace BNine.Application.Aggregates.CreditLines.Queries.GetAdvanceBoostOffer;

using MediatR;
using Models;

public record GetAdvanceBoostWithDelayOfferQuery : IRequest<GetAdvanceBoostWithDelayResponseViewModel>;
