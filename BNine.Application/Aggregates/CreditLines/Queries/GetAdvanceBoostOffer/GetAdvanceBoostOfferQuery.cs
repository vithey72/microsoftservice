﻿namespace BNine.Application.Aggregates.CreditLines.Queries.GetAdvanceBoostOffer;

using MediatR;
using Models;

public record GetAdvanceBoostOfferQuery : IRequest<GetAdvanceBoostResponseViewModel>;
