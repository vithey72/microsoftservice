﻿namespace BNine.Application.Aggregates.CreditLines.Queries.GetAdvanceBoostOffer
{
    using System;
    using System.Threading.Tasks;
    using Abstractions;
    using AutoMapper;
    using CommonModels.Buttons;
    using CommonModels.Dialog;
    using CommonModels.Static;
    using CommonModels.Text;
    using Domain.Entities.User;
    using Helpers;
    using Interfaces;
    using MediatR;
    using Microsoft.EntityFrameworkCore;
    using Models;


    public class GetAdvanceBoostWithDelayOfferQueryHandler
        : AbstractRequestHandler
        , IRequestHandler<GetAdvanceBoostWithDelayOfferQuery, GetAdvanceBoostWithDelayResponseViewModel>
    {
        private readonly IDateTimeCalculationService _dateTimeCalculationService;
        private readonly IAdvancesCompletedCheckupService _advancesCompletedCheckupService;

        public GetAdvanceBoostWithDelayOfferQueryHandler(
            IMediator mediator,
            IBNineDbContext dbContext,
            IMapper mapper,
            ICurrentUserService currentUser,
            IDateTimeCalculationService dateTimeCalculationService,
            IAdvancesCompletedCheckupService advancesCompletedCheckupService
            ) : base(mediator, dbContext, mapper, currentUser)
        {
            _dateTimeCalculationService = dateTimeCalculationService;
            _advancesCompletedCheckupService = advancesCompletedCheckupService;
        }

        public async Task<GetAdvanceBoostWithDelayResponseViewModel> Handle(GetAdvanceBoostWithDelayOfferQuery request, CancellationToken cancellationToken)
        {
            var userWithOffer = await DbContext.Users
                .Include(u => u.AdvanceStats)
                .SingleAsync(u => u.Id == CurrentUser.UserId!.Value, cancellationToken);

            var offer = userWithOffer!.AdvanceStats;

            if (offer == null || !offer.HasBoostLimit)
            {
                throw new InvalidOperationException("User has no Boost offered.");
            }

            var boostHoursToDelayPairs = new[]
            {
                (1, offer.BoostLimit1HoursToDelay), (2, offer.BoostLimit2HoursToDelay),
                (3, offer.BoostLimit3HoursToDelay)
            };

            // Initialize the service with the boost id and hours to delay pairs
            // This is required for consistent result in all places where the datetime calculation is required
            _dateTimeCalculationService.Initialize(boostHoursToDelayPairs);

            var respModel = new GetAdvanceBoostWithDelayResponseViewModel
            {
                OptionsHeader = "Boost your B9 Advance limit",
                TimeProcessingHeader = "Advance Boosted limit:",
                TimeProcessingTitle = "How should we send your cash?",
                Options = BuildBoostOptionModels(offer),
                TimeProcessing = BuildTimeProcessingItems(offer),
                FreeOptionConfirmationDialogs = BuildFreeOptionConfirmationDialogs(offer)
            };

            var isModalWindowRequired = await _advancesCompletedCheckupService.IsModalWindowRequired(userWithOffer.Id);

            if (isModalWindowRequired)
            {
                respModel.ModalWindow = ModalWindows.AddExternalCardModalWindow;
            }

            return respModel;
        }

        private BoostOptionModel[] BuildBoostOptionModels(AdvanceStats offer)
        {

            const string priceText = "$0 and Up";

            var boostOptionModels = new List<BoostOptionModel>
            {
                CreateBoostOptionModel(
                    offer.AvailableLimit,
                    offer.BoostLimit1,
                    priceText,
                    1),
            };
            if (offer.BoostLimit2 != 0 && offer.BoostLimit2Price != 0)
            {
                boostOptionModels.Add(CreateBoostOptionModel(
                    offer.AvailableLimit,
                    offer.BoostLimit2,
                    priceText,
                    2));
            }
            if (offer.BoostLimit3 != 0 && offer.BoostLimit3Price != 0)
            {
                boostOptionModels.Add(CreateBoostOptionModel(
                    offer.AvailableLimit,
                    offer.BoostLimit3,
                    priceText,
                    3));
            }

            return boostOptionModels.ToArray();
        }

        private static BoostOptionModel CreateBoostOptionModel(
            decimal advanceLimit, decimal boostLimit, string boostPrice, int boostId, string discountBadge = null)
        {
            var sumLimit = advanceLimit + boostLimit;
            return new()
            {
                Amount = sumLimit,
                Price = boostPrice,
                Id = boostId,
                DiscountBadge = discountBadge,
                Button = new ButtonViewModel(Text: $"TAKE {CurrencyFormattingHelper.AsRounded(sumLimit)} ADVANCE", ButtonStyleEnum.Solid)
            };
        }

        private List<TimeProcessingPair> BuildTimeProcessingItems(AdvanceStats offer)
        {
            var timeProcessingItems = new List<TimeProcessingPair>();

            timeProcessingItems.Add(
                CreateTimeProcessing(
                boostId: 1,
                boostPrice: offer.BoostLimit1Price,
                advanceLimit: offer.AvailableLimit,
                boostLimit: offer.BoostLimit1));

            if (offer.BoostLimit2 != 0 && offer.BoostLimit2Price != 0)
            {
                timeProcessingItems.Add(
                    CreateTimeProcessing(
                        boostId: 2,
                        boostPrice: offer.BoostLimit2Price,
                        advanceLimit: offer.AvailableLimit,
                        boostLimit: offer.BoostLimit2));
            }

            if (offer.BoostLimit3 != 0 && offer.BoostLimit3Price != 0)
            {
                timeProcessingItems.Add(
                    CreateTimeProcessing(
                        boostId: 3,
                        boostPrice: offer.BoostLimit3Price,
                        advanceLimit: offer.AvailableLimit,
                        boostLimit: offer.BoostLimit3));
                         }

            return timeProcessingItems;

        }

        private TimeProcessingPair CreateTimeProcessing(int boostId, decimal boostPrice, decimal advanceLimit, decimal boostLimit)
        {
            var sumLimit = advanceLimit + boostLimit;
            var businessDaysToPayOff = _dateTimeCalculationService.GetDaysToPayOff(boostId);
            var timeProcessingPair =
                new TimeProcessingPair()
                {
                    Id = boostId,
                    Paid =
                        new TimeProcessing()
                        {
                            Price = boostPrice,
                            PriceHint = "Express fee",
                            Text = "To my B9 Account",
                            IsFree = false,
                            Button = new ButtonViewModel($"PAY {CurrencyFormattingHelper.AsRegular(boostPrice)} & TAKE {CurrencyFormattingHelper.AsRounded(sumLimit)} ADVANCE",
                                ButtonStyleEnum.Solid).EmitsUserEvent("express-pay-button", null, null),
                            DiscountBadge = "immediately"
                        },
                    Free = new TimeProcessing()
                    {
                        Price = 0,
                        IsFree = true,
                        Text = "To my B9 Account",
                        Button = new ButtonViewModel($"TAKE {CurrencyFormattingHelper.AsRounded(sumLimit)} ADVANCE (IN {businessDaysToPayOff} DAYS)",
                            ButtonStyleEnum.Solid).EmitsUserEvent("free-pay-button", null, null),
                        DiscountBadge = $"in {businessDaysToPayOff} business days"
                    }
                };

            return timeProcessingPair;
        }

        private List<FreeOptionConfirmationDialog> BuildFreeOptionConfirmationDialogs(AdvanceStats offer)
        {
            var freeOptionConfirmationDialogs = new List<FreeOptionConfirmationDialog>();

            freeOptionConfirmationDialogs
                .Add(CreateFreeOptionConfirmationDialog(1, offer.BoostLimit1HoursToDelay));

            if(offer.BoostLimit2 != 0 && offer.BoostLimit2Price != 0)
            {
                freeOptionConfirmationDialogs
                    .Add(CreateFreeOptionConfirmationDialog(2, offer.BoostLimit2HoursToDelay));

            }

            if (offer.BoostLimit3 != 0 && offer.BoostLimit3Price != 0)
            {
                freeOptionConfirmationDialogs
                    .Add(CreateFreeOptionConfirmationDialog(3, offer.BoostLimit3HoursToDelay));
            }

            return freeOptionConfirmationDialogs;

        }


        private FreeOptionConfirmationDialog CreateFreeOptionConfirmationDialog(int boostId, int hoursToDelayOffer)
        {

            var dateToPayOff = _dateTimeCalculationService.GetTargetDateFormatted(boostId);
            var businessDaysToPayOff = _dateTimeCalculationService.GetDaysToPayOff(boostId);

            return new FreeOptionConfirmationDialog()
            {
                Id = boostId,
                Dialog = new GenericDialogRichBodyViewModel()
                {
                    Title = $"Your B9 Advance will be credited to your B9 account in {businessDaysToPayOff} business days ({dateToPayOff})",
                    Subtitle = new LinkedText("To get it sooner, choose the Express Fee option"),
                    ImageUrl = "https://b9prodaccount.blob.core.windows.net/static-documents-container/Misc/delayed-boost-calendar-363-347.png",
                    Buttons = new []
                    {
                        new ButtonViewModel($"I'LL WAIT THE {businessDaysToPayOff} DAYS",
                            ButtonStyleEnum.Solid).EmitsUserEvent("awaiting-payment-button", null, null),
                        new ButtonViewModel("BACK", ButtonStyleEnum.Bordered).EmitsUserEvent("back-button", null, null),
                    }
                }
            };
        }

    }
}
