﻿namespace BNine.Application.Aggregates.LoanSpecialOffers.Commands.RemoveLoanSpecialOffer
{
    using System;
    using System.Collections.Generic;
    using MediatR;
    using Newtonsoft.Json;

    public class RemoveLoanSpecialOfferCommand : IRequest<Unit>
    {
        [JsonProperty("userIds")]
        public IEnumerable<Guid> Ids
        {
            get; set;
        } = new List<Guid>();
    }
}
