﻿namespace BNine.Application.Aggregates.LoanSpecialOffers.Commands.RemoveLoanSpecialOffer
{
    using System;
    using System.Linq;
    using System.Threading;
    using System.Threading.Tasks;
    using AutoMapper;
    using BNine.Application.Abstractions;
    using BNine.Application.Interfaces;
    using MediatR;
    using Microsoft.EntityFrameworkCore;

    public class RemoveLoanSpecialOfferCommandHandler
        : AbstractRequestHandler, IRequestHandler<RemoveLoanSpecialOfferCommand>
    {
        public RemoveLoanSpecialOfferCommandHandler(IMediator mediator, IBNineDbContext dbContext, IMapper mapper, ICurrentUserService currentUser) : base(mediator, dbContext, mapper, currentUser)
        {
        }

        public async Task<Unit> Handle(RemoveLoanSpecialOfferCommand request, CancellationToken cancellationToken)
        {
            var offers = await DbContext.LoanSpecialOffers
                .Where(x => request.Ids.Contains(x.UserId))
                .ToListAsync(cancellationToken);

            if (offers.Any())
            {
                foreach (var offer in offers)
                {
                    offer.DeletedAt = DateTime.UtcNow;
                }

                DbContext.LoanSpecialOffers.RemoveRange(offers);

                await DbContext.SaveChangesAsync(cancellationToken);
            }

            return Unit.Value;
        }
    }
}
