﻿namespace BNine.Application.Aggregates.LoanSpecialOffers.Commands.RemoveLoanSpecialOffer
{
    using FluentValidation;

    public class RemoveLoanSpecialOfferCommandValidator
        : AbstractValidator<RemoveLoanSpecialOfferCommand>
    {
        public RemoveLoanSpecialOfferCommandValidator()
        {
            RuleFor(x => x.Ids)
                .NotEmpty();
        }
    }
}
