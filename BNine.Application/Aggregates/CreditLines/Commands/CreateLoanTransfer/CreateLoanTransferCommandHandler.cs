﻿namespace BNine.Application.Aggregates.CreditLines.Commands.CreateLoanTransfer
{
    using System;
    using System.Linq;
    using System.Threading;
    using System.Threading.Tasks;
    using Abstractions;
    using AutoMapper;
    using BNine.Common;
    using BNine.Domain.Events.AdvanceTaken;
    using Domain.Entities.User;
    using Enums;
    using Exceptions;
    using Interfaces;
    using Interfaces.Bank.Client;
    using MediatR;
    using Microsoft.EntityFrameworkCore;
    using Models;
    using Queries.GetCreditLineStats;

    public class CreateLoanTransferCommandHandler
        : AbstractRequestHandler
        , IRequestHandler<CreateLoanTransferCommand, CreditSimpleModel>
    {
        private const int MinAllowedLimit = 10;

        private readonly Interfaces.Bank.Administration.IBankLoanAccountsService _bankLoanAccountsService;
        private readonly IBankSavingsTransactionsService _bankTransfersService;
        private readonly IBankAuthorizationService _bankAuthorizationService;
        private readonly INotificationUsersService _notificationUsersService;
        private readonly IBankSavingAccountsService _bankSavingAccountsService;

        public CreateLoanTransferCommandHandler(
            INotificationUsersService notificationUsersService,
            IMediator mediator,
            IBNineDbContext dbContext,
            IMapper mapper,
            ICurrentUserService currentUser,
            IBankSavingAccountsService bankSavingAccountsService,
            Interfaces.Bank.Administration.IBankLoanAccountsService bankLoanAccountsService,
            IBankSavingsTransactionsService bankTransfersService,
            IBankAuthorizationService bankAuthorizationService)
            : base(mediator, dbContext, mapper, currentUser)
        {
            _bankLoanAccountsService = bankLoanAccountsService;
            _bankTransfersService = bankTransfersService;
            _bankAuthorizationService = bankAuthorizationService;
            _notificationUsersService = notificationUsersService;
            _bankSavingAccountsService = bankSavingAccountsService;
        }

        public async Task<CreditSimpleModel> Handle(CreateLoanTransferCommand request, CancellationToken cancellationToken)
        {
            var userId = request.UserId ?? CurrentUser.UserId!.Value;

            var user = DbContext.Users
                .Include(x => x.CurrentAccount)
                .Include(x => x.AdvanceStats)
                .FirstOrDefault(x => x.Id == userId);

            if (user?.ExternalClientId == null)
            {
                throw new NotFoundException(nameof(User), CurrentUser.UserId);
            }

            var advanceAmount = user.AdvanceStats?.AvailableLimit ?? 0;
            var extensionAmount = user.AdvanceStats?.ExtensionLimit ?? 0;
            var disbursementAmount = request.IsExtension
                ? Convert.ToInt32(extensionAmount)
                : Convert.ToInt32(advanceAmount + request.BoostAmount);
            if (disbursementAmount < MinAllowedLimit)
            {
                throw new InvalidOperationException("Cannot disburse Advance below $10.");
            }

            var expressFeeAmount = GetExpressFeeAmount(request);

            var loanAccount = await GetOrCreateLoanAccount(disbursementAmount, expressFeeAmount, user, cancellationToken);

            var externalLoanId = await _bankLoanAccountsService
                .DisburseLoanAccountToSaving(loanAccount.ExternalId, disbursementAmount);

            var advanceMaturityDate = TimeZoneHelper.UtcToEasternStandartTime(DateTime.UtcNow)
                .Date.AddDays(LoanHelpers.DurationInDays);

            await SendPushNotification(request, user, disbursementAmount, advanceMaturityDate);

            user.DomainEvents.Add(new AdvanceTakenEvent(user.Id));
            await DbContext.SaveChangesAsync(cancellationToken);

            return new CreditSimpleModel
            {
                CreditAmount = disbursementAmount,
                PaybackDate = advanceMaturityDate.ToString("MM/dd/yyyy"),
                ExternalLoanId = externalLoanId,
            };
        }

        private decimal GetExpressFeeAmount(CreateLoanTransferCommand command)
        {
            if (command.IsExtension)
            {
                return 0m;
            }

            return command.BoostPrice;
        }

        private async Task<Domain.Entities.LoanAccount> GetOrCreateLoanAccount(
            int disbursementAmount,
            decimal expressFeeAmount,
            User user,
            CancellationToken token)
        {
            var loanAccount = await DbContext.LoanAccounts
                .Where(x => x.Status != LoanAccountStatus.Closed && x.UserId == user.Id)
                .FirstOrDefaultAsync(token);

            if (loanAccount != null)
            {
                var externalLoanAccount = await _bankLoanAccountsService.GetLoan(loanAccount.ExternalId);
                if (externalLoanAccount.IsClosed())
                {
                    loanAccount.Status = LoanAccountStatus.Closed;
                    loanAccount.UpdatedAt = DateTime.UtcNow;
                }
            }

            if (loanAccount != null && loanAccount.Status != LoanAccountStatus.Closed)
            {
                return loanAccount;
            }

            var loanAccountId = await _bankLoanAccountsService.CreateLoanAccount(
                user.ExternalClientId!.Value,
                user.CurrentAccount.ExternalId,
                disbursementAmount,
                expressFeeAmount,
                token);

            await _bankLoanAccountsService.ApproveLoanAccount(loanAccountId);

            var newLoanAccount = new Domain.Entities.LoanAccount
            {
                ExternalId = loanAccountId,
                ProductId = 2,
                Status = LoanAccountStatus.Active,
                UserId = user.Id,
            };
            await DbContext.LoanAccounts.AddAsync(newLoanAccount, CancellationToken.None);
            await DbContext.SaveChangesAsync(CancellationToken.None);

            return newLoanAccount;
        }

        private async Task SendPushNotification(CreateLoanTransferCommand request, User user, int advanceAmount,
            DateTime advanceMaturityDate)
        {

            var mBanqToken = request.IsDelayedBoost  ?
                (await _bankAuthorizationService.GetTokens(null, null)).AccessToken
                : CurrentUser.UserMbanqToken;


            var savingAccountInfo = await _bankSavingAccountsService
                .GetSavingAccountInfo(
                    user.CurrentAccount.ExternalId,
                    mBanqToken,
                    request.IpAddress);

            await _notificationUsersService.SendAdvanceIssued(
                user.Id,
                advanceAmount,
                savingAccountInfo.AvailableBalance,
                advanceMaturityDate.ToString("dd MMM"));
        }
    }
}
