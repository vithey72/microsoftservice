﻿namespace BNine.Application.Aggregates.CreditLines.Commands.CreateLoanTransfer;

using Models;
using Behaviours;
using MediatR;
using Newtonsoft.Json;

[Sequential]
public class CreateLoanTransferCommand : IRequest<CreditSimpleModel>
{
    [JsonIgnore]
    public string IpAddress
    {
        get; set;
    }

    [JsonIgnore]
    public bool IsExtension
    {
        get;
        set;
    }

    [JsonIgnore]
    public int BoostAmount
    {
        get;
        set;
    }

    [JsonIgnore]
    public decimal BoostPrice
    {
        get;
        set;
    }

    [JsonIgnore]
    public Guid? UserId
    {
        get;
        set;
    }

    [JsonIgnore]
    public bool IsDelayedBoost
    {
        get;
        set;
    } = false;
}
