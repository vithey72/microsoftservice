﻿namespace BNine.Application.Aggregates.CreditLines.Commands.AfterAdvanceTip;

using System;
using System.Linq;
using System.Threading.Tasks;
using Abstractions;
using AutoMapper;
using Constants;
using Interfaces;
using Interfaces.Bank.Administration;
using MediatR;
using Microsoft.EntityFrameworkCore;
using IBankLoanAccountsService = Interfaces.Bank.Client.IBankLoanAccountsService;
using IBankSavingAccountsService = Interfaces.Bank.Client.IBankSavingAccountsService;

public class AfterAdvanceTipCommandHandler
    : AbstractRequestHandler
    , IRequestHandler<AfterAdvanceTipCommand, Unit>
{
    private readonly IBankSavingAccountsService _savingAccountsService;
    private readonly IPaidUserServicesService _paidUserServices;
    private readonly IBankLoanAccountsService _loanAccountsService;

    public AfterAdvanceTipCommandHandler(
        IMediator mediator,
        IBNineDbContext dbContext,
        IMapper mapper,
        ICurrentUserService currentUser,
        IBankSavingAccountsService savingAccountsService,
        IPaidUserServicesService paidUserServices,
        IBankLoanAccountsService loanAccountsService
    ) : base(mediator, dbContext, mapper, currentUser)
    {
        _savingAccountsService = savingAccountsService;
        _paidUserServices = paidUserServices;
        _loanAccountsService = loanAccountsService;
    }

    public async Task<Unit> Handle(AfterAdvanceTipCommand request, CancellationToken cancellationToken)
    {
        if (request.Amount == 0)
        {
            return Unit.Value;
        }

        var paidServiceId = request.Amount switch
        {
            2 => PaidUserServices.AfterAdvanceTip2,
            3 => PaidUserServices.AfterAdvanceTip3,
            5 => PaidUserServices.AfterAdvanceTip5,
            6 => PaidUserServices.AfterAdvanceTip6,
            8 => PaidUserServices.AfterAdvanceTip8,
            10 => PaidUserServices.AfterAdvanceTip10,
            12 => PaidUserServices.AfterAdvanceTip12,
            20 => PaidUserServices.AfterAdvanceTip20,
            _ => throw new NotImplementedException($"The charge for tip with amount of {request.Amount} does not exist.")
        };

        var user = await DbContext.Users
            .Include(u => u.CurrentAccount)
            .SingleAsync(u => u.Id == CurrentUser.UserId, cancellationToken);
        var currentBalance = await _savingAccountsService
            .GetSavingAccountInfo(user.CurrentAccount.ExternalId, request.MbanqAccessToken, request.Ip);
        var latestAdvance = await DbContext.LoanAccounts
            .OrderByDescending(l => l.CreatedAt)
            .FirstOrDefaultAsync(l => l.UserId == CurrentUser.UserId, cancellationToken);
        if (latestAdvance == null)
        {
            throw new InvalidOperationException("Cannot tip without ever taking an advance.");
        }

        var bankLoan = await _loanAccountsService.GetLoan(request.MbanqAccessToken, request.Ip, latestAdvance.ExternalId);

        if (!await _paidUserServices.ServiceIsAlreadyPurchased(paidServiceId))
        {
            if (currentBalance.AvailableBalance < request.Amount)
            {
                throw new InvalidOperationException("User does not have the necessary amount on their account.");
            }

            await _paidUserServices.EnableService(paidServiceId, idempotencyKey: $"tipfor{bankLoan.ExternalId}");
        }

        await _paidUserServices.MarkServiceAsUsed(paidServiceId);
        return Unit.Value;
    }
}
