﻿namespace BNine.Application.Aggregates.CreditLines.Commands.AfterAdvanceTip;

using Abstractions;
using Behaviours;
using MediatR;

[Sequential]
public class AfterAdvanceTipCommand : MBanqSelfServiceUserRequest, IRequest<Unit>
{
    public int Amount
    {
        get;
        set;
    }
}
