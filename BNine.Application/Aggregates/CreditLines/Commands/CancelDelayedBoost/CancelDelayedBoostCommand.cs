﻿namespace BNine.Application.Aggregates.CreditLines.Commands.CancelDelayedBoost;

using CommonModels.Dialog;
using MediatR;

public record CancelDelayedBoostCommand() : IRequest<GenericDialogBodyViewModel>;
