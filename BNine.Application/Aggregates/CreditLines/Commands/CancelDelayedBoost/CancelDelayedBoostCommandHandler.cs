﻿namespace BNine.Application.Aggregates.CreditLines.Commands.CancelDelayedBoost;

using Abstractions;
using AutoMapper;
using CommonModels.Dialog;
using BNine.Constants;
using Exceptions;
using Interfaces;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Queries.ActivateAdvanceBoost;
using ServiceBusEvents.Commands.PublishB9Event;

public class CancelDelayedBoostCommandHandler : AbstractRequestHandler,
    IRequestHandler<CancelDelayedBoostCommand, GenericDialogBodyViewModel>

{
    public CancelDelayedBoostCommandHandler(
        IMediator mediator,
        IBNineDbContext dbContext,
        IMapper mapper,
        ICurrentUserService currentUser
        )
        : base(mediator, dbContext, mapper, currentUser)
    {
    }

    public async Task<GenericDialogBodyViewModel> Handle(CancelDelayedBoostCommand request, CancellationToken cancellationToken)
    {
        var userWithOffer =
            await DbContext.Users
            .Include(u => u.AdvanceStats)
            .Include(u => u.CurrentAccount)
            .SingleAsync(u => u.Id == CurrentUser.UserId, cancellationToken);

        var offer = userWithOffer.AdvanceStats;

        if (offer == null)
        {
            throw new NotFoundException($"Offer not found for user with id: {CurrentUser.UserId}");
        }

        if (offer.DelayedBoostSettings == null)
        {
            throw new NotFoundException($"Delayed boost was not found for user with id: {CurrentUser.UserId}");
        }

        var boostId = offer.DelayedBoostSettings.DelayedBoostId;

        var (amount, price) = boostId switch
        {
            2 => (offer.BoostLimit2, offer.BoostLimit2Price),
            3 => (offer.BoostLimit3, offer.BoostLimit3Price),
            _ => (offer.BoostLimit1, offer.BoostLimit1Price),
        };

        await Mediator.Send(new PublishB9EventCommand(
            new UsedAdvanceBoostServiceBusMessage
            {
                BoostId = boostId,
                BoostAmount = amount,
                BoostPrice = price,
                BoostActivatedAt = null,
                BoostActivatedAtSchedule = offer.DelayedBoostSettings.ActivationDate,
                OfferId = offer.OfferId,
                UserId = userWithOffer.Id,
                LoanId = null,
                CanceledAt = DateTime.UtcNow

            },
            ServiceBusTopics.B9AdvanceBoost,
            ServiceBusEvents.B9AdvanceBoostDeactivatedSchedule
        ), cancellationToken);

        return new GenericDialogBodyViewModel
        {
            Title = "Your Advance will be canceled soon",
            ButtonText = "OK"
        };

    }


}
