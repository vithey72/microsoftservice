﻿namespace BNine.Application.Aggregates.LoanSpecialOffers.Commands.CreateLoanSpecialOffer
{
    using System.Collections.Generic;
    using System.Linq;
    using FluentValidation;

    public class CreateLoanSpecialOfferCommandValidator
        : AbstractValidator<CreateLoanSpecialOfferCommand>
    {
        public CreateLoanSpecialOfferCommandValidator()
        {
            RuleFor(x => x.Offers)
                .NotEmpty()
                .Must(DoesnotContainDuplicateIds)
                .WithMessage("The list contains duplicate members");
        }

        private bool DoesnotContainDuplicateIds(IEnumerable<CreateLoanListItem> list)
        {
            return list.Select(x => x.UserId).Count() == list.Select(x => x.UserId).Distinct().Count();
        }
    }
}
