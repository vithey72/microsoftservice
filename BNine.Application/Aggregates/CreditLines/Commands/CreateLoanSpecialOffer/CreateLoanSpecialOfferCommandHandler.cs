﻿namespace BNine.Application.Aggregates.LoanSpecialOffers.Commands.CreateLoanSpecialOffer
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading;
    using System.Threading.Tasks;
    using AutoMapper;
    using BNine.Application.Abstractions;
    using BNine.Application.Interfaces;
    using BNine.Application.Models.Notifications;
    using BNine.Constants;
    using BNine.Domain.Entities;
    using BNine.Enums;
    using MediatR;
    using Microsoft.EntityFrameworkCore;

    public class CreateLoanSpecialOfferCommandHandler
        : AbstractRequestHandler, IRequestHandler<CreateLoanSpecialOfferCommand, Unit>
    {
        private IPushNotificationService NotificationService
        {
            get;
        }

        public CreateLoanSpecialOfferCommandHandler(IPushNotificationService notificationService, IMediator mediator, IBNineDbContext dbContext, IMapper mapper, ICurrentUserService currentUser) : base(mediator, dbContext, mapper, currentUser)
        {
            NotificationService = notificationService;
        }

        public async Task<Unit> Handle(CreateLoanSpecialOfferCommand request, CancellationToken cancellationToken)
        {
            var existedOffers = await DbContext.LoanSpecialOffers
                .Where(x => request.Offers.Select(o => o.UserId).Contains(x.UserId))
                .ToListAsync(cancellationToken);

            DbContext.LoanSpecialOffers.RemoveRange(existedOffers);

            var newOffers = Mapper.Map<IEnumerable<LoanSpecialOffer>>(request.Offers);

            await DbContext.LoanSpecialOffers.AddRangeAsync(newOffers, cancellationToken);

            await DbContext.SaveChangesAsync(cancellationToken);

            if (request.Notification == null || string.IsNullOrWhiteSpace(request.Notification.Message))
            {
                return Unit.Value;

            }

            await SendNotifications(request, cancellationToken);

            return Unit.Value;
        }

        private async Task SendNotifications(CreateLoanSpecialOfferCommand request, CancellationToken cancellationToken)
        {
            var devices = DbContext.Users
                .Where(x => request.Offers.Select(c => c.UserId).Contains(x.Id))
                .Where(x => x.Settings.IsNotificationsEnabled)
                .SelectMany(x => x.Devices)
                .Select(x => new
                {
                    x.UserId,
                    Target = new NotificationTarget(x.Id.ToString(), x.Type)
                })
                .ToList();

            if (devices.Count == 0)
            {
                return;
            }

            await NotificationService.SendNotification(
                new Models.Notifications.Notification(
                    request.Notification.Message,
                    devices.Select(x => x.Target).ToList(),
                    request.Notification.Deeplink,
                    request.Notification.AppsflyerEventType,
                    string.IsNullOrWhiteSpace(request.Notification.Title) ? "Advance Activated" : request.Notification.Title)
                );

            var notifications = devices.Select(x => x.UserId).Distinct().Select(x => new Domain.Entities.Notification.Notification
            {
                CreatedAt = DateTime.UtcNow,
                Text = request.Notification.Message,
                IsRead = false,
                UserId = x,
                Category = string.IsNullOrWhiteSpace(request.Notification.Category) ? PushNotificationCategory.AdvanceActivated : request.Notification.Category,
                Title = string.IsNullOrWhiteSpace(request.Notification.Title) ? "Advance Activated" : request.Notification.Title,
                Type = NotificationTypeEnum.Marketing,
                IsImportant = true,
                Deeplink = request.Notification.Deeplink,
            });

            await DbContext.Notifications.AddRangeAsync(notifications, cancellationToken);

            await DbContext.SaveChangesAsync(cancellationToken);
        }
    }
}
