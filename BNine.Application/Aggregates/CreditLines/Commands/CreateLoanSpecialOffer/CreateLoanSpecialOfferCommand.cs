﻿namespace BNine.Application.Aggregates.LoanSpecialOffers.Commands.CreateLoanSpecialOffer
{
    using System;
    using System.Collections.Generic;
    using AutoMapper;
    using BNine.Application.Mappings;
    using BNine.Domain.Entities;
    using CommonModels;
    using Constants;
    using MediatR;
    using Newtonsoft.Json;

    public class CreateLoanSpecialOfferCommand : IRequest<Unit>
    {
        [JsonProperty("offers")]
        public IEnumerable<CreateLoanListItem> Offers
        {
            get; set;
        }

        [JsonProperty("notification")]
        public SpecialOfferNotification Notification
        {
            get; set;
        }
    }

    public class SpecialOfferNotification : GenericPushNotification
    {
        [JsonProperty("deeplink")]
        public new string Deeplink
        {
            get;
            set;
        } = DeepLinkRoutes.Credit;
    }

    public class CreateLoanListItem : IMapTo<LoanSpecialOffer>
    {
        [JsonProperty("userId")]
        public Guid UserId
        {
            get; set;
        }

        [JsonProperty("value")]
        public decimal Value
        {
            get; set;
        }

        public void Mapping(Profile profile) => profile.CreateMap<CreateLoanListItem, LoanSpecialOffer>()
            .ForMember(d => d.UserId, opt => opt.MapFrom(s => s.UserId))
            .ForMember(d => d.Value, opt => opt.MapFrom(s => s.Value));
    }
}
