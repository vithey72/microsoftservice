﻿namespace BNine.Application.Aggregates.CreditLines.Commands.MarkLoanAsRepaid
{
    using System.Linq;
    using System.Threading;
    using System.Threading.Tasks;
    using AutoMapper;
    using BNine.Application.Abstractions;
    using BNine.Application.Interfaces;
    using BNine.Application.Interfaces.Bank.Administration;
    using MediatR;

    public class MarkLoanAsRepaidCommandHandler
        : AbstractRequestHandler, IRequestHandler<MarkLoanAsRepaidCommand, Unit>
    {
        private IBankLoanAccountsService BankLoanAccountsService
        {
            get;
        }

        public MarkLoanAsRepaidCommandHandler(IBankLoanAccountsService bankLoanAccountsService, IMediator mediator, IBNineDbContext dbContext, IMapper mapper, ICurrentUserService currentUser) : base(mediator, dbContext, mapper, currentUser)
        {
            BankLoanAccountsService = bankLoanAccountsService;
        }

        public async Task<Unit> Handle(MarkLoanAsRepaidCommand request, CancellationToken cancellationToken)
        {
            var user = DbContext.Users
                .Where(x => x.CurrentAccount != null)
                .Where(x => x.CurrentAccount.ExternalId == request.SavingAccountId)
                .Select(x => new
                {
                    x.Id
                })
                .FirstOrDefault();

            if (user == null)
            {
                return Unit.Value;
            }

            if (!DbContext.LoanAccounts.Where(x => x.UserId == user.Id).Any())
            {

                return Unit.Value;
            }

            var activeLoanAccount = DbContext.LoanAccounts
                .Where(x => x.Status == Enums.LoanAccountStatus.Active)
                .Where(x => x.UserId == user.Id)
                .FirstOrDefault();

            if (activeLoanAccount == null)
            {
                return Unit.Value;
            }

            var externalLoanAccount = await BankLoanAccountsService
                .GetLoan(activeLoanAccount.ExternalId);

            if (externalLoanAccount.ClosedObligationsMet)
            {
                activeLoanAccount.Status = Enums.LoanAccountStatus.Closed;
                await DbContext.SaveChangesAsync(cancellationToken);
            }

            return Unit.Value;
        }
    }
}
