﻿namespace BNine.Application.Aggregates.CreditLines.Commands.MarkLoanAsRepaid
{
    using MediatR;

    public class MarkLoanAsRepaidCommand : IRequest<Unit>
    {
        public MarkLoanAsRepaidCommand(int savingAccountId)
        {
            SavingAccountId = savingAccountId;
        }

        public int SavingAccountId
        {
            get; set;
        }
    }
}
