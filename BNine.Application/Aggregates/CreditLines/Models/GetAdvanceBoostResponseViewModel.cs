﻿namespace BNine.Application.Aggregates.CreditLines.Models;

public class GetAdvanceBoostResponseViewModel
{
    public string Header
    {
        get;
        set;
    }

    public BoostOption[] Options
    {
        get;
        set;
    }

    public string Disclaimer
    {
        get;
        set;
    }

    public class BoostOption
    {
        public int Id
        {
            get;
            set;
        }

        public decimal Price
        {
            get;
            set;
        }

        public decimal Amount
        {
            get;
            set;
        }

        public string ButtonText
        {
            get;
            set;
        }

        public string DiscountBadge
        {
            get;
            set;
        }
    }
}
