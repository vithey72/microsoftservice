﻿namespace BNine.Application.Aggregates.CreditLines.Models
{
    using Newtonsoft.Json;

    public class CreditSimpleModel
    {
        [JsonProperty("creditAmount")]
        public int CreditAmount
        {
            get; set;
        }

        [JsonProperty("paybackDate")]
        public string PaybackDate
        {
            get; set;
        }

        [JsonIgnore]
        public int ExternalLoanId
        {
            get;
            set;
        }
    }
}
