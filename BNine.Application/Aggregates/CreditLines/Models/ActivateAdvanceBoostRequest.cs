﻿namespace BNine.Application.Aggregates.CreditLines.Queries.ActivateAdvanceBoost.Models;

public class ActivateAdvanceBoostRequest
{
    public int BoostId
    {
        get;
        set;
    }

    public bool IsFree
    {
        get;
        set;
    } = false;
}
