﻿namespace BNine.Application.Aggregates.CreditLines.Models;

using CommonModels.Dialog;

public class GetAfterAdvanceTipDialogViewModel
{
    public TipDialogScreen1 Screen1
    {
        get;
        set;
    }

    public TipDialogScreen2 Screen2
    {
        get;
        set;
    }

    public bool IsFirstAdvanceFlow
    {
        get;
        set;
    }

    public class TipDialogScreen1 : GenericDialog
    {
        public TipSuggestionsSection TipSuggestions
        {
            get;
            set;
        }

        public string PictureUrl
        {
            get;
            set;
        }
    }

    public class TipDialogScreen2 : GenericDialog
    {
        public TipSectionWithDropdown TipSection
        {
            get;
            set;
        }
    }

    public class TipSuggestionsSection
    {
        public int[] Tips
        {
            get;
            set;
        }

        public int DefaultTipIndex
        {
            get;
            set;
        }

        public string Disclaimer
        {
            get;
            set;
        }
    }

    public class TipSectionWithDropdown
    {
        public int[] Tips
        {
            get;
            set;
        }

        public int DefaultTipIndex
        {
            get;
            set;
        }

        public string Disclaimer1
        {
            get;
            set;
        }

        public string Disclaimer2
        {
            get;
            set;
        }

        public string Disclaimer3
        {
            get;
            set;
        }
    }
}
