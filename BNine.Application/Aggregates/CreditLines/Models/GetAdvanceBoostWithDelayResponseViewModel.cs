﻿namespace BNine.Application.Aggregates.CreditLines.Models;

using CommonModels.Buttons;
using CommonModels.Dialog;
using Newtonsoft.Json;


public class GetAdvanceBoostWithDelayResponseViewModel
{
    [JsonProperty("optionsHeader")]
    public string OptionsHeader
    {
        get;
        set;
    }

    [JsonProperty("timeProcessingHeader")]
    public string TimeProcessingHeader
    {
        get;
        set;
    }

    [JsonProperty("timeProcessingTitle")]
    public string TimeProcessingTitle
    {
        get;
        set;
    }

    [JsonProperty("options")]
    public BoostOptionModel[] Options
    {
        get;
        set;
    }

    [JsonProperty("modalWindow")]
    public GenericDialogRichBodyViewModel ModalWindow
    {
        get;
        set;
    }

    [JsonProperty("timeProcessing")]
    public List<TimeProcessingPair> TimeProcessing
    {
        get;
        set;
    }

    [JsonProperty("freeOptionConfirmationDialogs")]
    public List<FreeOptionConfirmationDialog> FreeOptionConfirmationDialogs
    {
        get;
        set;
    }
}


public class TimeProcessingPair
{

    [JsonProperty("id")]
    public int Id
    {
        get;
        set;
    }

    [JsonProperty("paid")]
    public TimeProcessing Paid
    {
        get;
        set;
    }

    [JsonProperty("free")]
    public TimeProcessing Free
    {
        get;
        set;
    }
}

public class TimeProcessing
{

    [JsonProperty("price")]
    public decimal Price
    {
        get;
        set;
    }

    [JsonProperty("isFree")]
    public bool IsFree
    {
        get;
        set;
    }

    [JsonProperty("text")]
    public string Text
    {
        get;
        set;
    }

    [JsonProperty("priceHint")]
    public string PriceHint
    {
        get;
        set;
    }

    [JsonProperty("button")]
    public ButtonViewModel Button
    {
        get;
        set;
    }

    [JsonProperty("discountBadge")]
    public string DiscountBadge
    {
        get;
        set;
    }
}
