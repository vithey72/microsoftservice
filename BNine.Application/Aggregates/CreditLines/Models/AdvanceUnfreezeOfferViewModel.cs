﻿namespace BNine.Application.Aggregates.CreditLines.Models;

public class AdvanceUnfreezeOfferViewModel
{
    public bool AlreadyPaid
    {
        get;
        set;
    }

    public AdvanceBox NormalAdvance
    {
        get;
        set;
    }

    public AdvanceBox UnfreezeAdvance
    {
        get;
        set;
    }

    public string Text
    {
        get;
        set;
    }

    public string ButtonNextText
    {
        get;
        set;
    }

    public string ButtonBackText
    {
        get;
        set;
    }

    public class AdvanceBox
    {
        public string ValueHeader
        {
            get;
            set;
        }

        public string Subtitle
        {
            get;
            set;
        }
    }
}
