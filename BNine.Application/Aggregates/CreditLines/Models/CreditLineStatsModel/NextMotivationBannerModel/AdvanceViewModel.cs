﻿namespace BNine.Application.Aggregates.CreditLines.Models;

using Enums.TariffPlan;

public class AdvanceViewModel
{
    public string Title
    {
        get;
        set;
    }

    public decimal NextAmount
    {
        get;
        set;
    }

    public string ButtonText
    {
        get;
        set;
    }

    public bool IsChecked
    {
        get;
        set;
    }

    public TariffPlanFamily TariffPlanFamilyType
    {
        get;
        set;
    }

    public decimal? MaximumAdvance
    {
        get;
        set;
    }
}
