﻿namespace BNine.Application.Aggregates.CreditLines.Models;

using CommonModels.DialogElements;
using Enums.TariffPlan;

public class AdvanceViewModelBase
{
    public string Title
    {
        get;
        set;
    }

    public string Subtitle
    {
        get;
        set;
    }

    public decimal NextAmount
    {
        get;
        set;
    }

    public TariffPlanFamily TariffPlanFamilyType
    {
        get;
        set;
    }

    public DeeplinkObject DeeplinkObject
    {
        get;
        set;
    } = null;

    public string Color
    {
        get;
        set;
    }

    public EventTrackingObject EventTracking
    {
        get;
        set;
    }
}
