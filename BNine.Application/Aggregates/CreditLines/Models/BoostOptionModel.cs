﻿namespace BNine.Application.Aggregates.CreditLines.Models;

using CommonModels.Buttons;
using Newtonsoft.Json;

public class BoostOptionModel
{
    [JsonProperty("id")]
    public int Id
    {
        get;
        set;
    }

    [JsonProperty("price")]
    public string Price
    {
        get;
        set;
    }

    [JsonProperty("amount")]
    public decimal Amount
    {
        get;
        set;
    }

    [JsonProperty("button")]
    public ButtonViewModel Button
    {
        get;
        set;
    }

    [JsonProperty("discountBadge")]
    public string DiscountBadge
    {
        get;
        set;
    }
}
