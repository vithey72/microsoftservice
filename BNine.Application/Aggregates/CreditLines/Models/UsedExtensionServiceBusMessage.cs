﻿namespace BNine.Application.Aggregates.CreditLines.Models
{
    using CommonModels;
    using Domain.Entities.PaidUserServices;

    public class UsedExtensionServiceBusMessage : UsedPaidServiceBusMessage
    {
        public UsedExtensionServiceBusMessage(UsedPaidUserService paidUserService, DateTime expiryDate,
            string eventType, int amount, bool isPurchase)
            : base(paidUserService)
        {
            ValidTo = expiryDate;
            EventType = eventType;
            if (isPurchase)
            {
                AmountPaid = amount;
            }
            else
            {
                ExtensionAmount = amount;
            }
        }

        public decimal? AmountPaid
        {
            get;
            set;
        }

        public int? ExtensionAmount
        {
            get;
            set;
        }

        /// <summary>
        /// Duplicates topic info.
        /// </summary>
        public string EventType
        {
            get;
            set;
        }
    }
}
