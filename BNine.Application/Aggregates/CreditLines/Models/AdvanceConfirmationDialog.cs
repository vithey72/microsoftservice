﻿#nullable enable
namespace BNine.Application.Aggregates.CreditLines.Models;

using CommonModels.Dialog;

public class AdvanceConfirmationDialog : GenericDialog
{
    public MidDialogSection MidSection
    {
        get;
        set;
    }

    public new GenericDialogTwoButtonBodyViewModel Body
    {
        get;
        set;
    }

    public GenericDialogRichBodyViewModel? ModalWindow
    {
        get;
        set;
    }

}

public class MidDialogSection
{
    public string Title
    {
        get;
        set;
    }

    public string Subtitle
    {
        get;
        set;
    }
}
