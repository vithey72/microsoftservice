﻿namespace BNine.Application.Aggregates.CreditLines.Models;

public class BoostOptionModelClassified : BoostOptionModel
{
    public bool IsFree
    {
        get;
        set;
    }

    public string Disclaimer
    {
        get;
        set;
    }
}
