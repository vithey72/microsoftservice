﻿namespace BNine.Application.Aggregates.CreditLines.Models;

using CommonModels.Dialog;

public class FreeOptionConfirmationDialog
{
    public int Id
    {
        get;
        set;
    }

    public GenericDialogRichBodyViewModel Dialog
    {
        get;
        set;
    }
}
