﻿namespace BNine.Application.Aggregates.CreditLines.Models;

using Domain.Entities.PaidUserServices;

public class BuyAdvanceUnfreezeServiceResponse
{
    public bool IsSuccess => UsedPaidUserService != null;

    public UsedPaidUserService UsedPaidUserService
    {
        get;
        set;
    }
}
