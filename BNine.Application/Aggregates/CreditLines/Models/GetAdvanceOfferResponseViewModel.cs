﻿namespace BNine.Application.Aggregates.CreditLines.Models;

using CommonModels.Dialog;

public class GetAdvanceOfferResponseViewModel
{
    public GenericDialogWithPicture Screen1
    {
        get;
        set;
    }

    public PreAdvancePremiumOfferScreen ExperimentScreen
    {
        get;
        set;
    }

    public AdvanceConfirmationDialog Screen2
    {
        get;
        set;
    }

    public class PreAdvancePremiumOfferScreen
    {
        public decimal CurrentBasicAdvance
        {
            get;
            set;
        }

        public decimal CurrentPremiumAdvance
        {
            get;
            set;
        }

        public int ScreenOption
        {
            get;
            set;
        }
    }
}
