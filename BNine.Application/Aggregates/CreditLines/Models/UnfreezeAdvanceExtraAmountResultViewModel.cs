﻿namespace BNine.Application.Aggregates.CreditLines.Models;

using CommonModels.Dialog;

public class UnfreezeAdvanceExtraAmountResultViewModel : GenericDialogBasicViewModel
{
    public bool IsSuccess
    {
        get;
        set;
    }
}
