﻿namespace BNine.Application.Aggregates.CreditLines.Models;

using System.Runtime.Serialization;
using CommonModels.Buttons;
using CommonModels.Dialog;
using CommonModels.DialogElements;
using CommonModels.Text;
using Enums.Advance;
using Newtonsoft.Json;

public class AdvanceWidgetViewModel
{
    /// <summary>
    /// List of lines to be displayed on the widget.
    /// </summary>
    [JsonProperty("advanceWidgetActions")]
    public AdvanceWidgetAction[] AdvanceWidgetActions
    {
        get;
        set;
    }

    public UnderWidgetBannerViewModel UnderWidgetBanners
    {
        get;
        set;
    } = new();

    public ButtonViewModel BottomLink
    {
        get;
        set;
    }

    public AdvanceWidgetState WidgetState
    {
        get;
        set;
    }

    public class AdvanceWidgetAction
    {
        /// <summary>
        /// Technical field to tell apart different Actions.
        /// </summary>
        public string ActionKey
        {
            get;
            set;
        }

        public string Title
        {
            get;
            set;
        }

        public HintWithDialogViewModel Hint
        {
            get;
            set;
        } = null;

        public AdvanceAmount Amount
        {
            get;
            set;
        } = new();

        /// <summary>
        /// Used for Extension to render the offer. Null in other cases.
        /// </summary>
        public AdvanceAmount AlternativeAmount
        {
            get;
            set;
        } = null;

        public string IconUrl
        {
            get;
            set;
        } = null;

        public ButtonViewModel? Button
        {
            get;
            set;
        }

        public FormattedText DropdownText
        {
            get;
            set;
        }
    }

    public class AdvanceAmount
    {
        public decimal Value
        {
            get;
            set;
        }

        public AdvanceAmountColorEnum Color
        {
            get;
            set;
        }
    }

    public enum AdvanceAmountColorEnum
    {
        [EnumMember(Value = "black")]
        Black = 0,

        [EnumMember(Value = "green")]
        Green = 1,

        [EnumMember(Value = "red")]
        Red = 2,

        [EnumMember(Value = "gray")]
        Gray = 3,
    }

    public class UnderWidgetBannerViewModel
    {
        public AdvanceWidgetBannerState State
        {
            get;
            set;
        }

        [JsonProperty("requiresActionBanner")]
        public RequiresActionBanner RequiresActionBanner
        {
            get;
            set;
        }

        [JsonProperty("requiresActionBannerV2")]
        public RequiresActionBannerV2 NewUserBanner
        {
            get;
            set;
        }

        public GenericDialogRichBodyViewModel WaitingForArgyleResponseBanner
        {
            get;
            set;
        }

        public GenericDialogRichBodyViewModel WaitingForFirstPaycheckBanner
        {
            get;
            set;
        }

        public NextAdvanceMotivationSection NextAdvanceMotivationSection
        {
            get;
            set;
        }
    }

    public class RequiresActionBanner
    {
        public string ButtonText
        {
            get;
            set;
        }

        public GenericDialogRichBodyViewModel RequiresActionDialog
        {
            get;
            set;
        }
    }

    public class RequiresActionBannerV2
    {
        public string IconUrl
        {
            get;
            set;
        }

        public FormattedText FormattedText
        {
            get;
            set;
        }

        public ButtonViewModel Button
        {
            get;
            set;
        }
    }

    public class NextAdvanceMotivationSection
    {
        public string DisclaimerText
        {
            get;
            set;
        }

        public AdvanceViewModelBase[] NextAdvanceInfoSections
        {
            get;
            set;
        }

        public string IconUrl
        {
            get;
            set;
        }
    }
}
