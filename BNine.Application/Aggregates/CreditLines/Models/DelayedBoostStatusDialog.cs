﻿namespace BNine.Application.Aggregates.CreditLines.Models;

using CommonModels.Dialog;
using Newtonsoft.Json;

public class DelayedBoostStatusDialog : GenericDialogRichBodyViewModel
{


    [JsonProperty("text")]
    public string Text
    {
        get;
        set;
    }

    [JsonProperty("subText")]

    public string SubText
    {
        get;
        set;
    }

    [JsonProperty("options")]
    public BoostOptionModelClassified[] Options
    {
        get;
        set;
    }
}
