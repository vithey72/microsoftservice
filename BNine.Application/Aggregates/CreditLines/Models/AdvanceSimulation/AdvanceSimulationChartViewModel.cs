﻿namespace BNine.Application.Aggregates.CreditLines.Models;

public class AdvanceSimulationChartViewModel
{
    public int MaxChartValue
    {
        get;
        set;
    }

    public AdvanceSimulationChartMilestones Milestones
    {
        get;
        set;
    }

    public AdvanceSimulationChartCalculationOutput CalculationOutput
    {
        get;
        set;
    }
}

public class AdvanceSimulationChartMilestones
{
    public AdvanceSimulationChartMilestoneValue Hundred
    {
        get;
        set;
    }
    public AdvanceSimulationChartMilestoneValue FiveHundred
    {
        get;
        set;
    }
}

public class AdvanceSimulationChartMilestoneValue
{
    public string Title
    {
        get;
        set;
    }

    public string ValueString
    {
        get;
        set;
    }

    public int Value
    {
        get;
        set;
    }

    public string ValueColor
    {
        get;
        set;
    }
}

public class AdvanceSimulationChartCalculationOutput
{
    public string Title
    {
        get;
        set;
    }

    public AdvanceSimulationChartMilestoneValue CurrentLimit
    {
        get;
        set;
    }

    public AdvanceSimulationChartMilestoneValue NextLimit
    {
        get;
        set;
    }
}
