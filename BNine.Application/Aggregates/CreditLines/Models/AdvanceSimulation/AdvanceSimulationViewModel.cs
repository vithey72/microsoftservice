﻿namespace BNine.Application.Aggregates.CreditLines.Models;

using CommonModels.Dialog;
using CommonModels.DialogElements;
using CommonModels.Text;

public class AdvanceSimulationViewModel
{
    public GenericDialogHeaderViewModel Header
    {
        get;
        set;
    }

    public HintWithDialogViewModel HeaderHint
    {
        get;
        set;
    }

    public AdvanceSimulationBody Body
    {
        get;
        set;
    }
}

public class AdvanceSimulationBody
{
    public AdvanceSimulationChartViewModel Chart
    {
        get;
        set;
    }

    public AdvanceSimulationCriteria Criteria
    {
        get;
        set;
    }

    public AdvanceSimulatorCalculator Calculator
    {
        get;
        set;
    }
}

public class AdvanceSimulationCriteria
{
    public string Title
    {
        get;
        set;
    }

    public AdvanceSimulationCriteriaItem[] Items
    {
        get;
        set;
    }
}

public class AdvanceSimulatorCalculator
{
    public string Title
    {
        get;
        set;
    }

    public AdvanceSimulatorCalculatorResetButton ResetButton
    {
        get;
        set;
    }

    public AdvanceSimulatorFactors Factors
    {
        get;
        set;
    }
}

public class SumOfPaychecksFactor : AdvanceSimulatorFactor
{
    public SumOfPaychecksFactorStep[] Steps
    {
        get;
        set;
    }
}

public class SumOfPaychecksFactorStep
{
    public string Title
    {
        get;
        set;
    }

    public bool IsSelected
    {
        get;
        set;
    }

    public int RangeStart
    {
        get;
        set;
    }

    public int? RangeEnd
    {
        get;
        set;
    }
}

public class AdvanceSimulatorFactor
{
    public bool IsOn
    {
        get;
        set;
    }

    public TextWithDeeplink Title
    {
        get;
        set;
    }

    public GenericDialogRichBodyViewModel Hint
    {
        get;
        set;
    }
}

public class AdvanceSimulatorFactors
{
    public string Title
    {
        get;
        set;
    }

    public AdvanceSimulatorFactor Premium
    {
        get;
        set;
    }

    public SumOfPaychecksFactor SumOfPaychecks
    {
        get;
        set;
    }

    public AdvanceSimulatorFactor GroceryStores
    {
        get;
        set;
    }

    public AdvanceSimulatorFactor NonPaidAdvances
    {
        get;
        set;
    }
}



public class AdvanceSimulatorCalculatorResetButton
{
    public string Style
    {
        get;
        set;
    }

    public string Text
    {
        get;
        set;
    }
}

public class AdvanceSimulationCriteriaItem
{
    public string Title
    {
        get;
        set;
    }

    public string Subtitle
    {
        get;
        set;
    }

    public string SubtitleColor
    {
        get;
        set;
    }

    public bool IsSatisfied
    {
        get;
        set;
    }
}
