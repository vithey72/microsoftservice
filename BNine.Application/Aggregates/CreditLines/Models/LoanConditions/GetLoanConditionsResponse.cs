﻿namespace BNine.Application.Aggregates.CreditLines.Models.LoanConditions
{
    using Newtonsoft.Json;

    public class GetLoanConditionsResponse
    {
        [JsonProperty("requiredDeposit")]
        public decimal RequiredDeposit
        {
            get; set;
        }
    }
}
