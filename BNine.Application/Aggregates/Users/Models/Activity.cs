﻿namespace BNine.Application.Aggregates.Users.Models
{
    using System.Collections.Generic;
    using Newtonsoft.Json;

    public class Activity
    {
        [JsonProperty("frequenciesOfPayments")]
        public IEnumerable<LocalizedFrequencyOfPayments> LocalizedFrequenciesOfPayments
        {
            get; set;
        }

        [JsonProperty("payMethods")]
        public IEnumerable<LocalizedPayMethod> LocalizedPayMethods
        {
            get; set;
        }

        [JsonProperty("sourcesOfIncome")]
        public IEnumerable<LocalizedSourceOfIncome> LocalizedSourcesOfIncome
        {
            get; set;
        }
    }
}
