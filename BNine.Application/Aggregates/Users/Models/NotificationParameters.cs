﻿namespace BNine.Application.Aggregates.Users.Models;

using Newtonsoft.Json;

public class NotificationParameters
{
    [JsonProperty("message")]
    public string Message
    {
        get;
        set;
    }

    [JsonProperty("deeplink")]
    public string Deeplink
    {
        get;
        set;
    }

    [JsonProperty("appsflyerEventType")]
    public string AppsflyerEventType
    {
        get;
        set;
    }

    [JsonProperty("title")]
    public string Title
    {
        get;
        set;
    }

    [JsonProperty("category")]
    public string Category
    {
        get;
        set;
    }
}
