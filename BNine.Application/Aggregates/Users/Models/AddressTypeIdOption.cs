﻿namespace BNine.Application.Aggregates.Users.Models
{
    public class AddressTypeIdOption
    {
        public int Id
        {
            get; set;
        }

        public string Name
        {
            get; set;
        }
    }
}
