﻿namespace BNine.Application.Aggregates.Users.Models;

using System.Linq;
using Enums;
using Evaluation.Models;

internal class SocureOverrideRule
{
    /// <summary>
    /// Build a rule that overrides a Socure decision.
    /// </summary>
    /// <param name="ruleName">unique-name-of-the-rule</param>
    /// <param name="detectionCodes">Any of these codes triggers the rule.</param>
    /// <param name="excludedCodes">Any of these codes invalidates the triggering of the rule.</param>
    /// <param name="outcome">New status that should be applied if code conditions are satisfied.</param>
    public SocureOverrideRule(string ruleName, string[] detectionCodes, string[] excludedCodes, KycStatusEnum outcome)
    {
        RuleName = ruleName;
        DetectionCodes = detectionCodes;
        ExcludedCodes = excludedCodes;
        Outcome = outcome;
    }

    public string RuleName
    {
        get;
    }

    private string[] DetectionCodes
    {
        get;
    }

    private string[] ExcludedCodes
    {
        get;
    }

    private KycStatusEnum Outcome
    {
        get;
    }

    public bool TryOverride(SocureTransactionResponse kycResult, out KycStatusEnum outcome)
    {
        if (kycResult.DocVerification.ReasonCodes.Any(rc => DetectionCodes.Contains(rc)) &&
            !kycResult.DocVerification.ReasonCodes.Any(rc => ExcludedCodes.Contains(rc)))
        {
            outcome = Outcome;
            return true;
        }

        outcome = kycResult.DecisionEnum;

        return false;
    }
}
