﻿namespace BNine.Application.Aggregates.Users.Models.CardLimits;

using Mappings;

public class UserCardLimitsViewModel : IMapFrom<UserCardLimits>
{
    public List<Card> Cards
    {
        get;
        set;
    }

    public CardLimit CardLimits
    {
        get;
        set;
    }

    public void Mapping(AutoMapper.Profile profile) => profile.CreateMap<UserCardLimits, UserCardLimitsViewModel>()
        .ForMember(d => d.Cards, o => o.MapFrom(s => s.Card))
        .ForMember(d => d.CardLimits, o => o.MapFrom(s => s.CardLimits));

}
