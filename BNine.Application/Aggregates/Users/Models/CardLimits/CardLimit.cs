﻿namespace BNine.Application.Aggregates.Users.Models.CardLimits;

public class CardLimit
{
    public decimal AllCardOperationsDailyLimit
    {
        get;
        set;
    }

    public decimal AllCardOperationsWeeklyLimit
    {
        get;
        set;
    }

    public decimal AllCardOperationsMonthlyLimit
    {
        get;
        set;
    }

    public decimal WithdrawlDailyLimit
    {
        get;
        set;
    }


    public decimal WithdrawlWeeklyLimit
    {
        get;
        set;
    }

    public decimal WithdrawlMonthlyLimit
    {
        get;
        set;
    }
}
