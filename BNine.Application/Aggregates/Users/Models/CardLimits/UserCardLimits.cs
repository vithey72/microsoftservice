﻿namespace BNine.Application.Aggregates.Users.Models.CardLimits;

public class UserCardLimits
{
    public string UserId
    {
        get;
        set;
    }

    public List<Card> Card
    {
        get;
        set;
    }

    public CardLimit CardLimits
    {
        get;
        set;
    }
}
