﻿namespace BNine.Application.Aggregates.Users.Models.CardLimits;

public class Card
{
    public int CardId
    {
        get;
        set;
    }

    public bool IsVirtual
    {
        get;
        set;
    }

    public string CardLast4Digits
    {
        get;
        set;
    }

    public decimal AllCardOperationsDaily
    {
        get;
        set;
    }

    public decimal AllCardOperationsWeekly
    {
        get;
        set;
    }

    public decimal AllCardOperationsMonthly
    {
        get;
        set;
    }

    public decimal WithdrawlDaily
    {
        get;
        set;
    }

    public decimal WithdrawlWeekly
    {
        get;
        set;
    }

    public decimal WithdrawlMonthly
    {
        get;
        set;
    }
}
