﻿namespace BNine.Application.Aggregates.Users.Models
{
    public class BaseLocalizedListItem
    {
        public string Key
        {
            get; set;
        }

        public string LocalizedValue
        {
            get; set;
        }

        public int Order
        {
            get; set;
        }
    }
}
