﻿namespace BNine.Application.Aggregates.Users.Models
{
    using System;
    using System.Collections.Generic;
    using BNine.Application.Aggregates.Users.Queries.GetUserDetails;
    using BNine.Application.Mappings;
    using BNine.Domain.Entities;
    using BNine.Domain.Entities.Common;
    using BNine.Domain.Entities.User;
    using BNine.Enums;
    using Newtonsoft.Json;

    public class UserDetails : IMapFrom<User>
    {
        public Guid Id
        {
            get; set;
        }

        public string FirstName
        {
            get; set;
        }

        public string LastName
        {
            get; set;
        }

        public string PhoneNumber
        {
            get; set;
        }

        public string Email
        {
            get; set;
        }

        public IdentityType? IdentityType
        {
            get; set;
        }

        public string IdentityValue
        {
            get; set;
        }

        public DateTime? DateOfBirth
        {
            get; set;
        }

        public UserStatus Status
        {
            get; set;
        }

        public BlockingReason BlockingReason
        {
            get; set;
        }

        public BlockingReason ClosingReason
        {
            get; set;
        }

        public DateTime? ClosedAt
        {
            get; set;
        }

        [JsonProperty("address")]
        public AddressInfo AddressInfo
        {
            get; set;
        }

        public IEnumerable<PayAllocationsListItem> PayAllocations
        {
            get; set;
        }

        public decimal? FixedLimit
        {
            get; set;
        }

        public decimal? AdvancedLimit
        {
            get; set;
        }

        public decimal? Rating
        {
            get; set;
        }

        public decimal? RecentPayrolls
        {
            get; set;
        }

        public string CurrentAdvanceUsed
        {
            get; set;
        }

        public IEnumerable<AchTranferDetails> AchTransfers
        {
            get;
            internal set;
        }

        public void Mapping(AutoMapper.Profile profile) => profile.CreateMap<User, UserDetails>()
            .ForMember(d => d.Id, opt => opt.MapFrom(s => s.Id))
            .ForMember(d => d.FirstName, opt => opt.MapFrom(s => s.FirstName))
            .ForMember(d => d.LastName, opt => opt.MapFrom(s => s.LastName))
            .ForMember(d => d.PhoneNumber, opt => opt.MapFrom(s => s.Phone))
            .ForMember(d => d.AddressInfo, opt => opt.MapFrom(s => s.Address))
            .ForMember(d => d.Email, opt => opt.MapFrom(s => s.Email))
            .ForMember(d => d.DateOfBirth, opt => opt.MapFrom(s => s.DateOfBirth))
            .ForMember(d => d.IdentityType, opt => opt.MapFrom(s => s.Identity.Type))
            .ForMember(d => d.IdentityValue, opt => opt.MapFrom(s => s.Identity.Value))
            .ForMember(d => d.BlockingReason, opt => opt.MapFrom(s => s.UserBlockReason))
            .ForMember(d => d.ClosingReason, opt => opt.MapFrom(s => s.UserCloseReason))
            .ForMember(d => d.ClosedAt, opt => opt.MapFrom(s => s.ClosedAt))
            .ForMember(d => d.Rating, opt => opt.MapFrom(s => s.AdvanceStats == null ? 0 : s.AdvanceStats.Rating))
            .ForMember(d => d.Status,
                opt => opt.MapFrom(s => s.IsBlocked && s.Status != UserStatus.Closed ? UserStatus.Blocked : s.Status));
    }

    public class BlockingReason : IMapFrom<BlockUserReason>
    {
        [JsonProperty("id")]
        public Guid Id
        {
            get; set;
        }

        [JsonProperty("value")]
        public string Value
        {
            get; set;
        }

        public void Mapping(AutoMapper.Profile profile) => profile.CreateMap<BlockUserReason, BlockingReason>()
            .ForMember(d => d.Id, o => o.MapFrom(s => s.Id))
            .ForMember(d => d.Value, o => o.MapFrom(s => s.Value));
    }

    public class PayAllocationsListItem
    {
        [JsonProperty("status")]
        public string Status
        {
            get; set;
        }

        [JsonProperty("allocationType")]
        public string AllocationType
        {
            get; set;
        }

        [JsonProperty("allocationValue")]
        public string AllocationValue
        {
            get; set;
        }

        [JsonProperty("argyleUserId")]
        public Guid ArgyleUserId
        {
            get; set;
        }
    }

    public class LimitInfo : IMapFrom<Address>
    {
        [JsonProperty("value")]
        public decimal? Value
        {
            get; set;
        }

        public void Mapping(AutoMapper.Profile profile) => profile.CreateMap<LoanSpecialOffer, LimitInfo>()
          .ForMember(d => d.Value, opt => opt.MapFrom(s => s.Value));
    }

    public class AddressInfo : IMapFrom<Address>
    {
        [JsonProperty("zipCode")]
        public string ZipCode
        {
            get; set;
        }

        [JsonProperty("city")]
        public string City
        {
            get; set;
        }

        [JsonProperty("state")]
        public string State
        {
            get; set;
        }

        [JsonProperty("addressLine")]
        public string AddressLine
        {
            get; set;
        }

        [JsonProperty("unit")]
        public string Unit
        {
            get; set;
        }

        public void Mapping(AutoMapper.Profile profile) => profile.CreateMap<Address, AddressInfo>()
            .ForMember(d => d.ZipCode, opt => opt.MapFrom(s => s.ZipCode))
            .ForMember(d => d.AddressLine, opt => opt.MapFrom(s => s.AddressLine))
            .ForMember(d => d.City, opt => opt.MapFrom(s => s.City))
            .ForMember(d => d.State, opt => opt.MapFrom(s => s.State))
            .ForMember(d => d.Unit, opt => opt.MapFrom(s => s.Unit));
    }
}
