﻿namespace BNine.Application.Aggregates.Users.Models;

public class CashbackProgramSlideshowViewModel
{
    public SlideshowScreen[] Screens
    {
        get;
        set;
    }

    public class SlideshowScreen
    {
        public string Header
        {
            get;
            set;
        }

        public string Text
        {
            get;
            set;
        }

        public string ImageUrl
        {
            get;
            set;
        }

        public string ButtonText
        {
            get;
            set;
        }

        // Can be null
        public string Link
        {
            get;
            set;
        }

        // Can be null
        public string LinkText
        {
            get;
            set;
        }
    }
}
