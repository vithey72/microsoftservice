﻿namespace BNine.Application.Aggregates.Users.Models.UserAdvancesHistory;

using AutoMapper;
using BNine.Application.Mappings;
using Enums;
using Queries.GetAdvancesHistory;

public class AdvancesHistoryViewModel
{
    public List<AdminAdvancesHistoryEntry> AdvancesHistoryEntries
    {
        get;
        set;
    }
}
public class AdminAdvancesHistoryEntry : IMapFrom<DwhAdvancesHistoryEntry>
{
    public int LoanId
    {
        get; set;
    }

    public DateTime? DisbursedAt
    {
        get; set;
    }

    public double? AmountUsed
    {
        get; set;
    }

    public double? OriginalAmount
    {
        get; set;
    }

    public double? Boost
    {
        get; set;
    }

    public double? RemainingAmount
    {
        get; set;
    }

    public DateTime? DueDate
    {
        get; set;
    }

    public DateTime? ClosedDate
    {
        get; set;
    }

    public int? Dpd
    {
        get; set;
    }

    public AdvanceAdminStatusEnum Status
    {
        get;set;
    }

    public void Mapping(Profile profile) =>
        profile.CreateMap<DwhAdvancesHistoryEntry, AdminAdvancesHistoryEntry>();
}
