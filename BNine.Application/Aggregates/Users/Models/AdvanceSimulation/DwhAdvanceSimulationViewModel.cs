﻿namespace BNine.Application.Aggregates.Users.Models.AdvanceSimulation;

public class DwhAdvanceSimulationViewModel
{
    public bool IsSimulate
    {
        get;
        set;
    }

    public string UserId
    {
        get;
        set;
    }

    public AdvanceSimulationCalculatorState CalculatorState
    {
        get;
        set;
    }
}
