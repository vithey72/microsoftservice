﻿namespace BNine.Application.Aggregates.Users.Models.AdvanceSimulation;

using Newtonsoft.Json;

public class AdvanceSimulationCalculatorState
{
    public DateTime? LastAdvanceLimitIncreaseDate
    {
        get;
        set;
    }

    public bool HadAdvanceIncreaseInLast7Days
    {
        get;
        set;
    }

    public bool CurrentLimitIsUsed
    {
        get;
        set;
    }

    public bool CurrentLimitIsPaid
    {
        get;
        set;
    }

    public DateTime? LastPayrollReceivedDate
    {
        get;
        set;
    }

    public bool AtLeastOnePayrollWasReceived
    {
        get;
        set;
    }

    public bool PaycheckAmountSumForLast14DaysOver300Usd
    {
        get;
        set;
    }

    public string TariffPlanFamily
    {
        get;
        set;
    }

    public int PayrollTransfersAmountSumForLast14Days
    {
        get;
        set;
    }

    [JsonProperty("hasMonthlySpendingsOnGroceriesAbove100Usd")]
    public bool HasMonthlySpendingOnGroceriesAbove100Usd
    {
        get;
        set;
    }

    public bool HasOverdueRepaymentsIn30Days
    {
        get;
        set;
    }

    public int? CurrentAdvanceLimit
    {
        get;
        set;
    }

    public int? NextAdvanceLimit
    {
        get;
        set;
    }

    public int? MaxNextAdvanceOnBasic
    {
        get;
        set;
    }

    public int? MaxNextAdvanceOnPremium
    {
        get;
        set;
    }
}
