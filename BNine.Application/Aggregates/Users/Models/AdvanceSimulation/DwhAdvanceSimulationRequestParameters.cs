﻿namespace BNine.Application.Aggregates.Users.Models.AdvanceSimulation;

using Newtonsoft.Json;

public class DwhAdvanceSimulationRequestParameters
{
    [JsonProperty("tariffPlanFamily")]
    public string TariffPlanFamily
    {
        get;
        set;
    }

    [JsonProperty("payrollAmountRangeStart")]
    public int PayrollAmountRangeStart
    {
        get;
        set;
    }

    [JsonProperty("payrollAmountRangeEnd")]
    public int? PayrollAmountRangeEnd
    {
        get;
        set;
    }

    [JsonProperty("hasMonthlySpendingsOnGroceriesAbove100Usd")]
    public bool HasMonthlySpendingOnGroceriesAbove100Usd
    {
        get;
        set;
    }

    [JsonProperty("hasRepaymentOverdueFor30Days")]
    public bool HasRepaymentOverdueFor30Days
    {
        get;
        set;
    }
}
