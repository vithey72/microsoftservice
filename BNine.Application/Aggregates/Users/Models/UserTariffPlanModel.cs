﻿namespace BNine.Application.Aggregates.Users.Models
{
    using BNine.Enums.TariffPlan;
    using Newtonsoft.Json;

    public class UserTariffPlanModel
    {
        public Guid Id
        {
            get; set;
        }

        public string Name
        {
            get; set;
        }

        public string ShortName
        {
            get; set;
        }

        public TariffPlanFamily Type
        {
            get; set;
        }

        public decimal MonthlyFee
        {
            get; set;
        }

        public string ImageUrl
        {
            get; set;
        }

        public string Title
        {
            get; set;
        }

        public string PriceText
        {
            get; set;
        }

        [JsonProperty("initialTariffIsPurchased")]
        public bool? InitialTariffIsPurchased
        {
            get; set;
        }
    }
}
