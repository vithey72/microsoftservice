﻿namespace BNine.Application.Aggregates.Users.Models;

using System.Text;
using Enums;

public record ClientAccountClosureValidationResult(IEnumerable<ClientAccountClosureResultCode> Reasons)
{
    public bool CanClose => Reasons == null || !Reasons.Any();

    public string GetTextForException()
    {
        var stringBuilder = new StringBuilder();
        foreach (var reason in Reasons)
        {
            var description = reason switch
            {
                ClientAccountClosureResultCode.NegativeBalance or ClientAccountClosureResultCode.PositiveBalance =>
                    "Balance of the savings account is not 0, the account can not be closed",

                ClientAccountClosureResultCode.PendingTransfersPresent =>
                    "There are pending transactions, the account can not be closed",

                ClientAccountClosureResultCode.ArgyleConnected =>
                    "There is an active Argyle connection",

                ClientAccountClosureResultCode.DisbursedAdvance =>
                    "There is an open advance, the account can not be closed",

                _ => "Unknown issue when closing account",
            };
            stringBuilder.AppendLine(description);
        }

        return stringBuilder.ToString();
    }
}
