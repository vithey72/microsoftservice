﻿namespace BNine.Application.Aggregates.Users.Models
{
    using Newtonsoft.Json;

    public class AlloyDocumentDetails
    {
        [JsonProperty("document_token")]
        public string DocumentToken
        {
            get;
            set;
        }

        [JsonProperty("type")]
        public string Type
        {
            get;
            set;
        }

        [JsonProperty("name")]
        public string Name
        {
            get;
            set;
        }

        [JsonProperty("extension")]
        public string Extension
        {
            get;
            set;
        }

        [JsonProperty("uploaded")]
        public bool Uploaded
        {
            get;
            set;
        }

        [JsonProperty("timestamp")]
        public long TimeStamp
        {
            get;
            set;
        }

        [JsonProperty("approved")]
        public bool? Approved
        {
            get;
            set;
        }

        [JsonProperty("approval_agent_email")]
        public string ApprovalAgentEmail
        {
            get;
            set;
        }

        [JsonProperty("approval_timestamp")]
        public long? ApprovalTimeStamp
        {
            get;
            set;
        }
    }
}
