﻿namespace BNine.Application.Aggregates.Users.Models
{
    public class UserApplicationEvaluationSettings
    {
        public int AdvanceTakenCounter
        {
            get; set;
        }

        public bool IsActive
        {
            get; set;
        }
    }
}
