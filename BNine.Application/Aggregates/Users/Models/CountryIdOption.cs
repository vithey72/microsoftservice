﻿namespace BNine.Application.Aggregates.Users.Models
{
    public class CountryIdOption
    {
        public int Id
        {
            get; set;
        }

        public string Name
        {
            get; set;
        }

        public int ParentId
        {
            get; set;
        }
    }
}
