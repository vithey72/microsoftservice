﻿namespace BNine.Application.Aggregates.Users.Models
{
    using System;
    using Domain.Entities.CashbackProgram;
    using Mappings;

    public class AvailableCashbackCategoriesResponse
    {
        public CashbackCategoryViewModel[] Categories
        {
            get;
            set;
        }

        public byte MinCashbackCategories
        {
            get;
            set;
        } = 1;

        public byte MaxCashbackCategories
        {
            get;
            set;
        } = 4;
    }

    public class CashbackCategoryViewModel : IMapFrom<CashbackCategory>
    {
        public Guid Id
        {
            get;
            set;
        }

        public string Name
        {
            get;
            set;
        }

        public string ReadableName
        {
            get;
            set;
        }

        public string CategoryDescription
        {
            get;
            set;
        }

        public string IconUrl
        {
            get;
            set;
        }

        public decimal Percentage
        {
            get;
            set;
        }

        public int[] MccCodes
        {
            get;
            set;
        }

        public void Mapping(AutoMapper.Profile profile) => profile.CreateMap<CashbackCategory, CashbackCategoryViewModel>()
            .ForMember(d => d.Id, o => o.MapFrom(s => s.Id))
            .ForMember(d => d.ReadableName, o => o.MapFrom(s => s.ReadableName))
            .ForMember(d => d.CategoryDescription, o => o.MapFrom(s => s.CategoryDescription))
            .ForMember(d => d.IconUrl, o => o.MapFrom(s => s.IconUrl))
            .ForMember(d => d.Percentage, o => o.MapFrom(s => s.Percentage))
            .ForMember(d => d.MccCodes, o => o.MapFrom(s => s.MccCodes))
            .ForMember(d => d.Name, o => o.MapFrom(s => s.Name));
    }
}
