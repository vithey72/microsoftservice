﻿namespace BNine.Application.Aggregates.Users.Models
{
    using System;
    using BNine.Domain.Entities.User.UserDetails.Settings;
    using Domain.Entities.User;
    using Enums;
    using Mappings;
    using Newtonsoft.Json;

    public class Profile : IMapFrom<User>
    {
        [JsonProperty("id")]
        public Guid Id
        {
            get; set;
        }

        [JsonProperty("firstName")]
        public string FirstName
        {
            get; set;
        }

        [JsonProperty("lastName")]
        public string LastName
        {
            get; set;
        }

        [JsonProperty("dateOfBirth")]
        public DateTime? DateOfBirth
        {
            get; set;
        }

        [JsonProperty("phone")]
        public string Phone
        {
            get; set;
        }

        [JsonProperty("email")]
        public string Email
        {
            get; set;
        }

        [JsonProperty("zipCode")]
        public string ZipCode
        {
            get; set;
        }

        [JsonProperty("city")]
        public string City
        {
            get; set;
        }

        [JsonProperty("state")]
        public string State
        {
            get; set;
        }

        [JsonProperty("address")]
        public string Address
        {
            get; set;
        }

        [JsonProperty("unit")]
        public string Unit
        {
            get; set;
        }

        [JsonProperty("SSN")]
        public string SSN
        {
            get; set;
        }

        [JsonProperty("ITIN")]
        public string ITIN
        {
            get; set;
        }

        [JsonProperty("occupation")]
        public string Occupation
        {
            get; set;
        }

        [JsonProperty("appsflyerId")]
        public Guid AppsflyerId
        {
            get; set;
        }

        [JsonProperty("frequencyOfPayments")]
        public FrequencyOfPayments FrequencyOfPayments
        {
            get; set;
        }

        [JsonProperty("sourceOfIncome")]
        public SourceOfIncome SourceOfIncome
        {
            get; set;
        }

        [JsonProperty("grossMonthlyIncome")]
        public double? GrossMonthlyIncome
        {
            get; set;
        }

        [JsonProperty("payMethod")]
        public PayMethod PayMethod
        {
            get; set;
        }

        [JsonProperty("isActiveDutyMilitaryOrDependentOf")]
        public bool? IsActiveDutyMilitaryOrDependentOf
        {
            get; set;
        }

        [JsonProperty("documentType")]
        public DocumentType DocumentType
        {
            get; set;
        }

        [JsonProperty("status")]
        public UserStatus Status
        {
            get; set;
        }

        [JsonProperty("statusDetailed")]
        public UserStatusDetailed StatusDetailed
        {
            get; set;
        }

        [JsonProperty("customErrorMessage")]
        public string CustomErrorMessage
        {
            get; set;
        }

        [JsonProperty("isEarlySalaryEnabled")]
        public bool IsEarlySalaryEnabled
        {
            get; set;
        }

        [JsonProperty("isNotificationsEnabled")]
        public bool IsNotificationsEnabled
        {
            get; set;
        }

        [JsonProperty("isCardDelivered")]
        public bool IsCardDelivered
        {
            get; set;
        }

        [JsonProperty("tariffPlan")]
        public UserTariffPlanModel TariffPlan
        {
            get; set;
        }

        [JsonProperty("isCreditScorePurchased")]
        public bool IsCreditScoreServicePurchased
        {
            get; set;
        }

        [JsonProperty("paycheckSwitchProvider")]
        public PaycheckSwitchProvider PaycheckSwitchProvider
        {
            get; set;
        }

        public void Mapping(AutoMapper.Profile profile)
        {
            profile.CreateMap<User, Profile>()
                .ForMember(dst => dst.Id, opt => opt.MapFrom(s => s.Id))
                .ForMember(dst => dst.Address, opt => opt.MapFrom(src => src.Address.AddressLine))
                .ForMember(dst => dst.Phone, opt => opt.MapFrom(src => src.Phone))
                .ForMember(dst => dst.Email, opt => opt.MapFrom(src => src.Email))
                .ForMember(dst => dst.City, opt => opt.MapFrom(src => src.Address.City))
                .ForMember(dst => dst.Unit, opt => opt.MapFrom(src => src.Address.Unit))
                .ForMember(dst => dst.State, opt => opt.MapFrom(src => src.Address.State))
                .ForMember(dst => dst.DateOfBirth, opt => opt.MapFrom(src => src.DateOfBirth))
                .ForMember(dst => dst.FirstName, opt => opt.MapFrom(src => src.FirstName))
                .ForMember(dst => dst.LastName, opt => opt.MapFrom(src => src.LastName))
                .ForMember(dst => dst.ZipCode, opt => opt.MapFrom(src => src.Address.ZipCode))
                .ForMember(dst => dst.Status, opt => opt.MapFrom(src => src.Status))
                .ForMember(dst => dst.StatusDetailed, opt => opt.MapFrom(src => src.StatusDetailed))
                .ForMember(dst => dst.SSN, opt => opt.MapFrom(src => src.Identity.Type == IdentityType.SSN ? src.Identity.Value : null))
                .ForMember(dst => dst.ITIN, opt => opt.MapFrom(src => src.Identity.Type == IdentityType.ITIN ? src.Identity.Value : null))
                .ForMember(dst => dst.DocumentType, opt => opt.Ignore())
                .ForMember(dst => dst.IsEarlySalaryEnabled, opt => opt.MapFrom(src => src.EarlySalarySettings != null && src.EarlySalarySettings.IsEarlySalaryEnabled))
                .ForMember(dst => dst.IsNotificationsEnabled, opt => opt.MapFrom(src => src.Settings.IsNotificationsEnabled))
                .ForMember(dst => dst.AppsflyerId, opt => opt.MapFrom(src => src.AppsflyerId))
                .ForMember(dst => dst.PaycheckSwitchProvider, opt => opt.MapFrom(src => src.Settings.PaycheckSwitchProvider))
                ;
        }
    }
}
