﻿namespace BNine.Application.Aggregates.Users.Models.TransactionLimits;

public class TransactionLimit
{
    public TransactionLimitBreakdown DailyLimit
    {
        get;
        set;
    }

    public TransactionLimitBreakdown WeeklyLimit
    {
        get;
        set;
    }

    public TransactionLimitBreakdown MonthlyLimit
    {
        get;
        set;
    }
}
