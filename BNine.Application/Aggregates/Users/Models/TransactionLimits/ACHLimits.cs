﻿namespace BNine.Application.Aggregates.Users.Models.TransactionLimits;

public class ACHLimits
{
    public TransactionLimit Deposit
    {
        get;
        set;
    }

    public TransactionLimit Withdrawl
    {
        get;
        set;
    }
}
