﻿namespace BNine.Application.Aggregates.Users.Models.TransactionLimits;

public class ExternalCardLimits
{
    public TransactionLimit Pull
    {
        get;
        set;
    }

    public TransactionLimit Push
    {
        get;
        set;
    }
}
