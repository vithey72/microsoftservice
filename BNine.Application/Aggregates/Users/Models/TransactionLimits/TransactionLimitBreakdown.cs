﻿namespace BNine.Application.Aggregates.Users.Models.TransactionLimits;

public class TransactionLimitBreakdown
{
    public decimal? UsedAmount
    {
        get;
        set;
    }

    public int? TransactionCount
    {
        get;
        set;
    }

    public decimal? LimitAmount
    {
        get;
        set;
    }

    public int? TransactionCountLimit
    {
        get;
        set;
    }
}
