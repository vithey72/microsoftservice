﻿namespace BNine.Application.Aggregates.Users.Models.TransactionLimits;

public class UserTransactionLimitsViewModel
{
    public string UserId
    {
        get;
        set;
    }

    public ExternalCardLimits ExternalCardCurrentLimits
    {
        get;
        set;
    }

    public ACHLimits ACHCurrentLimits
    {
        get;
        set;
    }
}
