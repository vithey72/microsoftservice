﻿namespace BNine.Application.Aggregates.Users.Models;

using Newtonsoft.Json;

/// <summary>
/// Viewmodel of cashback program status screen.
/// </summary>
public class CashbackProgramStatusViewModel
{
    /// <summary>
    /// Section of screen that shows total cashback progress in numbers.
    /// </summary>
    [JsonProperty("totalStatistics")]
    public CashbackTotalStatistics TotalStatistics
    {
        get;
        set;
    }

    /// <summary>
    /// Section of screen that shows current month cashback progress in numbers.
    /// </summary>
    [JsonProperty("currentStatistics")]
    public CashbackCurrentStatistics CurrentStatistics
    {
        get;
        set;
    }

    /// <summary>
    /// Section of screen that shows categories selected for current cashback month. Null if cashback not enabled.
    /// </summary>
    [JsonProperty("currentCategories")]
    public CashbackMonthCategories CurrentCategories
    {
        get;
        set;
    }

    /// <summary>
    /// Section of screen that shows categories selected for upcoming cashback month. Null if cashback not enabled.
    /// </summary>
    [JsonProperty("upcomingCategories")]
    public CashbackMonthCategories UpcomingCategories
    {
        get;
        set;
    }

    /// <summary>
    /// If there's no cashback debit transfers, it's hidden.
    /// </summary>
    [JsonProperty("showTransfersDetailsButton")]
    public bool ShowTransfersDetailsButton
    {
        get;
        set;
    } = true;

    public class CashbackTotalStatistics
    {
        [JsonProperty("header")]
        public string Header
        {
            get;
            set;
        }

        [JsonProperty("subtitle")]
        public string Subtitle
        {
            get;
            set;
        }

        [JsonProperty("upcomingPaymentHeader")]
        public string UpcomingPaymentHeader
        {
            get;
            set;
        }
    }

    public class CashbackCurrentStatistics
    {
        [JsonProperty("header")]
        public string Header
        {
            get;
            set;
        }

        [JsonProperty("subtitle")]
        public string Subtitle
        {
            get;
            set;
        }

        /// <summary>
        /// How much user has spent on cashback categories. Null if enough to activate cashback.
        /// </summary>
        [JsonProperty("spentAmount")]
        public decimal? SpentAmount
        {
            get;
            set;
        }

        /// <summary>
        /// How much user has to spend in cashback categories. Null if user spent enough.
        /// </summary>
        [JsonProperty("goalAmount")]
        public decimal? GoalAmount
        {
            get;
            set;
        }

        /// <summary>
        /// How much left to spend.
        /// </summary>
        [JsonProperty("missingAmount")]
        public decimal? MissingAmount
        {
            get;
            set;
        }
    }

    public class CashbackMonthCategories
    {
        [JsonProperty("header")]
        public string Header
        {
            get;
            set;
        }

        [JsonProperty("subtitle")]
        public string Subtitle
        {
            get;
            set;
        }

        [JsonProperty("selectedCategories")]
        public CashbackCategoryViewModel[] SelectedCategories
        {
            get;
            set;
        }

        [JsonProperty("canSelectCategories")]
        public bool CanSelectCategories
        {
            get;
            set;
        }
    }
}

