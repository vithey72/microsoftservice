﻿namespace BNine.Application.Aggregates.Users.Models;

using Enums;

public class ClientDocument
{
    // Id field is required by Admin Tool UI app tables
    public string Id => FileName;

    public string FileName
    {
        get;
        set;
    }

    public string DocumentTypeKey
    {
        get;
        set;
    }

    public DocumentSide DocumentSide
    {
        get;
        set;
    }

    public DateTimeOffset? CreatedOn
    {
        get;
        set;
    }
}
