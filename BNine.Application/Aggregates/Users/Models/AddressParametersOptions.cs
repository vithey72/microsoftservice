﻿namespace BNine.Application.Aggregates.Users.Models
{
    using System.Collections.Generic;

    public class AddressParametersOptions
    {
        public List<AddressTypeIdOption> AddressTypeIdOptions
        {
            get; set;
        }

        public List<CountryIdOption> StateIdOptions
        {
            get; set;
        }
    }
}
