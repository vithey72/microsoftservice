﻿namespace BNine.Application.Aggregates.Users.Models
{
    using Domain.Entities.User;
    using Mappings;

    public class UserExternalIds : IMapFrom<User>
    {
        public int? ExternalUserId
        {
            get;
            set;
        }

        public int? ExternalSavingsAccountId
        {
            get;
            set;
        }

        public int? ExternalClientId
        {
            get;
            set;
        }

        public int? ExternalDocumentId
        {
            get;
            set;
        }

        public int? ExternalIdentityId
        {
            get;
            set;
        }

        public void Mapping(AutoMapper.Profile profile)
        {
            profile.CreateMap<User, UserExternalIds>()
                .ForMember(d => d.ExternalClientId, opt => opt.MapFrom(s => s.ExternalClientId))
                .ForMember(d => d.ExternalIdentityId, opt => opt.MapFrom(s => s.Identity.ExternalId))
                .ForMember(d => d.ExternalUserId, opt => opt.MapFrom(s => s.ExternalId))
                .ForMember(d => d.ExternalSavingsAccountId, opt => opt.MapFrom(s => s.CurrentAccount.ExternalId))
                .ForMember(d => d.ExternalDocumentId, opt => opt.MapFrom(s => s.Document.ExternalId));
        }
    }
}
