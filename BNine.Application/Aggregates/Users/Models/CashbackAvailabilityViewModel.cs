﻿namespace BNine.Application.Aggregates.Users.Models;

using Newtonsoft.Json;

public class CashbackAvailabilityViewModel
{
    [JsonProperty("cashbackSwitch")]
    public CashbackSwitchViewModel CashbackSwitch
    {
        get;
        set;
    }

    public class CashbackSwitchViewModel
    {
        [JsonProperty("entityId")]
        public Guid EntityId
        {
            get;
            set;
        }

        [JsonProperty("title")]
        public string Title
        {
            get;
            set;
        }

        [JsonProperty("subtitle")]
        public string Subtitle
        {
            get;
            set;
        }

        [JsonProperty("isEnabled")]
        public bool IsEnabled
        {
            get;
            set;
        }
    }
}
