﻿#nullable enable
namespace BNine.Application.Aggregates.Users.Models.UserDelayedBoostInfo;

public class UserDelayedBoostInfoViewModel
{
    public DelayedBoostInfo? DelayedBoostInfo
    {
        get;
        set;
    }
}

public class DelayedBoostInfo
{

    public DateTime ActivationDate
    {
        get;
        set;
    }

    public decimal OriginalAmount
    {
        get;
        set;
    }

    public decimal PlannedForDisburseAmount
    {
        get;
        set;
    }

    public decimal BoostAmount
    {
        get;
        set;
    }

}

