﻿namespace BNine.Application.Aggregates.Users.Models;

using System;
using Newtonsoft.Json;

public class UserCashbackSwitchRequest
{
    [JsonProperty("userId")]
    public Guid UserId
    {
        get;
        set;
    }

    [JsonProperty("isCashbackEnabled")]
    public bool IsCashbackEnabled
    {
        get;
        set;
    }

    /// <summary>
    /// Date with year and month representing what period cashback should be enabled/disabled for.
    /// </summary>
    [JsonProperty("yearMonth")]
    public DateTime YearMonth
    {
        get;
        set;
    }
}
