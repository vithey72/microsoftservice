﻿namespace BNine.Application.Aggregates.Users.Models.UserCashback;

public class UserCashbackHistoryViewModel
{
    public CashbackInfoViewModel[] CashbackInfo
    {
        get;
        set;
    }
}

public class CashbackInfoViewModel
{
    public DateTime PeriodStart
    {
        get;
        set;
    }

    public decimal TransactionAmount
    {
        get;
        set;
    }

    public decimal CashbackAmount
    {
        get;
        set;
    }

    public string CashbackDate
    {
        get;
        set;
    }
}
