﻿namespace BNine.Application.Aggregates.Users.Models
{
    using System;
    using Newtonsoft.Json;

    public class ConfirmUpdateEmailResponseModel
    {
        [JsonProperty("userId")]
        public Guid UserId
        {
            get; set;
        }

        [JsonProperty("isEmailUpdated")]
        public bool IsEmailUpdated
        {
            get; set;
        }

    }
}
