﻿namespace BNine.Application.Aggregates.Users.Commands.ResetPassword
{
    using AutoMapper;
    using BNine.Application.Mappings;
    using BNine.Common.TableConnect.Common;
    using Domain.Entities.User;
    using MediatR;
    using Newtonsoft.Json;

    public class ResetPasswordCommand : IRequest<Unit>, IMapTo<User>
    {
        [JsonProperty("phoneNumber")]
        public string PhoneNumber
        {
            get; set;
        }

        [JsonProperty("newPassword")]
        public string NewPassword
        {
            get; set;
        }

        [JsonProperty("passwordConfirmation")]
        public string PasswordConfirmation
        {
            get; set;
        }

        public void Mapping(Profile profile)
        {
            profile.CreateMap<ResetPasswordCommand, User>()
                .ForMember(dst => dst.Password, opt => opt.MapFrom(src => CryptoHelper.HashPassword(src.NewPassword)));
        }
    }
}
