﻿namespace BNine.Application.Aggregates.Users.Commands.ResetPassword
{
    using System.Linq;
    using BNine.Application.Interfaces;
    using BNine.Common;
    using BNine.ResourceLibrary;
    using FluentValidation;
    using Microsoft.Extensions.Localization;

    public class ResetPasswordCommandValidator : AbstractValidator<ResetPasswordCommand>
    {
        private IBNineDbContext DbContext
        {
            get;
        }

        private IStringLocalizer<SharedResource> SharedLocalizer
        {
            get;
        }

        public ResetPasswordCommandValidator(
            IBNineDbContext dbContext,
            IStringLocalizer<SharedResource> sharedLocalizer
            )
        {
            DbContext = dbContext;

            SharedLocalizer = sharedLocalizer;

            RuleFor(x => x.PhoneNumber)
                .Must(IsUserExists)
                .WithMessage(SharedLocalizer["noUserFoundValidationErrorMessage"].Value)
                ;

            RuleFor(x => x.NewPassword)
                .Must(PasswordsHelper.IsMeetsRequirements)
                .WithMessage(SharedLocalizer["passwordPoliciesValidationErrorMessage"].Value)
                ;

            RuleFor(x => x.PasswordConfirmation)
                .NotEmpty()
                .Equal(x => x.NewPassword);
        }

        private bool IsUserExists(string phoneNumber)
        {
            return DbContext.Users.Any(x => x.Phone.Equals(phoneNumber));
        }
    }
}
