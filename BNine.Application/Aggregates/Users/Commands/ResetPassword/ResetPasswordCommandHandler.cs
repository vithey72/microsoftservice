﻿namespace BNine.Application.Aggregates.Users.Commands.ResetPassword
{
    using System.Linq;
    using System.Threading;
    using System.Threading.Tasks;
    using Abstractions;
    using AutoMapper;
    using Common.TableConnect.Common;
    using Domain.Entities.User;
    using Exceptions;
    using Interfaces;
    using Interfaces.Bank.Administration;
    using MediatR;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.Extensions.Localization;
    using ResourceLibrary;

    public class ResetPasswordCommandHandler
        : AbstractRequestHandler
        , IRequestHandler<ResetPasswordCommand, Unit>
    {
        private IBankUsersService BankUsersService
        {
            get;
        }

        private IStringLocalizer<SharedResource> SharedLocalizer
        {
            get;
        }

        public ResetPasswordCommandHandler(
            IMediator mediator,
            IBNineDbContext dbContext,
            IMapper mapper,
            ICurrentUserService currentUser,
            IBankUsersService bankUsersService,
            IStringLocalizer<SharedResource> sharedLocalizer)
            : base(mediator, dbContext, mapper, currentUser)
        {
            BankUsersService = bankUsersService;
            SharedLocalizer = sharedLocalizer;
        }

        public async Task<Unit> Handle(ResetPasswordCommand request, CancellationToken cancellationToken)
        {
            var user = DbContext.Users.FirstOrDefault(x => x.Phone.Equals(request.PhoneNumber));
            if (user == null)
            {
                throw new ValidationException(nameof(request.PhoneNumber), SharedLocalizer["noUserFoundValidationErrorMessage"].Value);
            }

            if (CryptoHelper.VerifyHashedPassword(user.Password, request.NewPassword))
            {
                throw new ValidationException(nameof(user.Password), "New password should not be the same as the current password");
            }

            if (user.Settings.IsResetPasswordAllowed)
            {
                var externalUser = await DbContext.Users
                    .Where(x => x.Id == user.Id)
                    .Select(x => new { x.ExternalId })
                    .FirstOrDefaultAsync();

                if (externalUser == null)
                {
                    throw new NotFoundException(nameof(User), user.Id);
                }

                var response = await BankUsersService.UpdateUser(
                    externalUser.ExternalId,
                    user.FirstName ?? "pending",
                    request.NewPassword,
                    request.PasswordConfirmation);

                Mapper.Map(request, user);

                user.Settings.IsResetPasswordAllowed = false;

                await DbContext.SaveChangesAsync(cancellationToken);
            }
            else
            {
                throw new ValidationException(nameof(User), SharedLocalizer["userDidntRequestedPasswordRecoveryError"].Value);
            }

            return Unit.Value;
        }
    }
}
