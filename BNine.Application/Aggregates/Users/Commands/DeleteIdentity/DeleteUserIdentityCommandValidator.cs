﻿namespace BNine.Application.Aggregates.Users.Commands.DeleteIdentity
{
    using System;
    using FluentValidation;

    public class DeleteUserIdentityCommandValidator : AbstractValidator<DeleteUserIdentityCommand>
    {
        private readonly Guid privateKey = Guid.Parse("594a6e42-c5ac-446f-8966-8ee00acda341");

        public DeleteUserIdentityCommandValidator()
        {
            RuleFor(x => x.PhoneNumber).NotEmpty();
            RuleFor(x => x.PrivateKey)
                .NotEmpty()
                .Equal(privateKey)
                .WithMessage("Invalid private key");
        }
    }
}
