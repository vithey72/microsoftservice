﻿namespace BNine.Application.Aggregates.Users.Commands.DeleteIdentity
{
    using System;
    using MediatR;
    using Newtonsoft.Json;

    public class DeleteUserIdentityCommand : IRequest<Unit>
    {
        [JsonProperty("privateKey")]
        public Guid PrivateKey
        {
            get;
            set;
        }

        [JsonProperty("phoneNumber")]
        public string PhoneNumber
        {
            get;
            set;
        }
    }
}
