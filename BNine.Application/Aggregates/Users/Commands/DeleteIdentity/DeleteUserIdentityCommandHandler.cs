﻿namespace BNine.Application.Aggregates.Users.Commands.DeleteIdentity
{
    using System.Threading;
    using System.Threading.Tasks;
    using AutoMapper;
    using BNine.Application.Abstractions;
    using BNine.Application.Exceptions;
    using BNine.Application.Interfaces;
    using BNine.Application.Interfaces.Bank.Administration;
    using BNine.Domain.Entities.User;
    using MediatR;
    using Microsoft.EntityFrameworkCore;

    public class DeleteUserIdentityCommandHandler : AbstractRequestHandler, IRequestHandler<DeleteUserIdentityCommand>
    {
        private IBankIdentityService IdentityService
        {
            get;
        }

        public DeleteUserIdentityCommandHandler(
            IMediator mediator,
            IBNineDbContext dbContext,
            IMapper mapper,
            ICurrentUserService currentUser,
            IBankIdentityService identityService)
            : base(mediator, dbContext, mapper, currentUser)
        {
            IdentityService = identityService;
        }

        public async Task<Unit> Handle(DeleteUserIdentityCommand request, CancellationToken cancellationToken)
        {
            var user = await DbContext.Users
                .Include(x => x.Document)
                .FirstOrDefaultAsync(x => x.Phone.Equals(request.PhoneNumber));

            if (user == null)
            {
                throw new NotFoundException(nameof(User), request.PhoneNumber);
            }

            await DeleteUserIdentityFromMbanq(user.ExternalClientId, user.Document.ExternalId);

            user.Document.Number = "";

            await DbContext.SaveChangesAsync(cancellationToken);

            return Unit.Value;
        }

        private async Task DeleteUserIdentityFromMbanq(int? externalClientId, int? documentExternalId)
        {
            if (documentExternalId.HasValue)
            {
                try
                {
                    await IdentityService.DeleteIdentity(externalClientId.Value, documentExternalId.Value);
                }
                catch
                {

                }
            }
        }
    }
}
