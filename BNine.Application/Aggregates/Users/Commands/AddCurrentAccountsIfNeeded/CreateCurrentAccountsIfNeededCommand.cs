﻿namespace BNine.Application.Aggregates.Users.Commands.AddCurrentAccountsIfNeeded;

using MediatR;

public record CreateCurrentAccountsIfNeededCommand(Guid UserId) : IRequest<Unit>;

