﻿namespace BNine.Application.Aggregates.Users.Commands.AddCurrentAccountsIfNeeded;

using Interfaces.Bank.Administration;
using MediatR;

public class CreateCurrentAccountsIfNeededCommandHandler : IRequestHandler<CreateCurrentAccountsIfNeededCommand, Unit>
{
    private readonly IBankCurrentAccountsService _bankCurrentAccountsService;

    public CreateCurrentAccountsIfNeededCommandHandler(
        IBankCurrentAccountsService bankCurrentAccountsService
        )
    {
        _bankCurrentAccountsService = bankCurrentAccountsService;
    }

    public async Task<Unit> Handle(CreateCurrentAccountsIfNeededCommand request, CancellationToken cancellationToken)
    {
        await _bankCurrentAccountsService.AddCurrentBankAccountsIfNeeded(request.UserId, cancellationToken);
        return Unit.Value;
    }
}
