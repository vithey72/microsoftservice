﻿namespace BNine.Application.Aggregates.Users.Commands.SignupCommand
{
    using System.Linq;
    using Common;
    using FluentValidation;
    using Interfaces;
    using Microsoft.Extensions.Localization;
    using ResourceLibrary;

    public class SignupCommandValidator
        : AbstractValidator<SignupCommand>
    {
        private IBNineDbContext DbContext
        {
            get;
        }

        public SignupCommandValidator(
            IBNineDbContext dbContext,
            IStringLocalizer<SharedResource> sharedLocalizer
            )
        {
            DbContext = dbContext;

            RuleFor(x => x.Phone)
                .NotEmpty()
                .Must(PhoneHelper.IsValidPhone)
                .WithMessage("Phone number entered incorrectly. Please try again");

            RuleFor(x => x.Phone)
                .Must(IsPhoneUnique)
                .WithMessage(sharedLocalizer["accountExistsValidationErrorMessage"].Value);

            RuleFor(x => x.Email)
                .NotEmpty()
                .EmailAddress()
                .Must(IsUnique)
                .WithMessage(sharedLocalizer["emailRegisteredValidationErrorMessage"].Value);

            RuleFor(x => x.Password)
                .Must(PasswordsHelper.IsMeetsRequirements)
                .WithMessage(sharedLocalizer["passwordPoliciesValidationErrorMessage"].Value);

            RuleFor(x => x.Confirmation)
                .NotEmpty()
                .Equal(x => x.Password);
        }

        private bool IsUnique(string email)
        {
            return !DbContext.Users.Any(x => x.Email.Equals(email));
        }

        private bool IsPhoneUnique(string phone)
        {
            return !DbContext.Users.Any(x => x.Phone.Equals(phone));
        }
    }
}
