﻿namespace BNine.Application.Aggregates.Users.Commands.SignupCommand
{
    using System;
    using AutoMapper;
    using BNine.Application.Behaviours;
    using BNine.Application.Mappings;
    using BNine.Common.TableConnect.Common;
    using BNine.Enums;
    using Domain.Entities.User;
    using MediatR;
    using Newtonsoft.Json;

    [Sequential]
    public class SignupCommand
        : IRequest<Guid>, IMapTo<User>
    {
        [JsonProperty("phone")]
        public string Phone
        {
            get; set;
        }

        [JsonProperty("email")]
        public string Email
        {
            get; set;
        }

        [JsonProperty("password")]
        public string Password
        {
            get; set;
        }

        [JsonProperty("confirmation")]
        public string Confirmation
        {
            get; set;
        }

        [JsonProperty("code")]
        public string Code
        {
            get; set;
        }

        public override bool Equals(object obj)
        {
            return obj is SignupCommand command &&
                   Phone == command.Phone &&
                   Email == command.Email &&
                   Password == command.Password &&
                   Confirmation == command.Confirmation &&
                   Code == command.Code;
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(Phone, Email, Password, Confirmation, Code);
        }

        public void Mapping(Profile profile)
        {
            profile.CreateMap<SignupCommand, User>()
                .ForMember(dst => dst.Phone, opt => opt.MapFrom(src => src.Phone))
                .ForMember(dst => dst.Email, opt => opt.MapFrom(src => src.Email.ToLower()))
                .ForMember(dst => dst.Status, opt => opt.MapFrom(src => UserStatus.PendingPersonalInformation))
                .ForMember(dst => dst.Password, opt => opt.MapFrom(src => CryptoHelper.HashPassword(src.Password)));
        }

    }
}
