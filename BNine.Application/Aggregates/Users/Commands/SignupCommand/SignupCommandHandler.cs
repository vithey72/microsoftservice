﻿namespace BNine.Application.Aggregates.Users.Commands.SignupCommand
{
    using System;
    using System.Linq;
    using System.Threading;
    using System.Threading.Tasks;
    using Abstractions;
    using AutoMapper;
    using BNine.Application.Aggregates.Configuration.Queries.GetConfiguration;
    using BNine.Application.Aggregates.PaycheckSwitch;
    using BNine.Application.Aggregates.ServiceBusEvents.Commands.PublishB9Event;
    using BNine.Constants;
    using Common.TableConnect.Common;
    using Domain.Entities;
    using Domain.Entities.User;
    using Domain.Entities.User.UserDetails.Settings;
    using Enums;
    using Exceptions;
    using Interfaces;
    using Interfaces.Bank.Administration;
    using MediatR;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.Extensions.Localization;
    using ResourceLibrary;

    public class SignupCommandHandler
        : AbstractRequestHandler
        , IRequestHandler<SignupCommand, Guid>
    {
        private readonly IBankUsersService _bankUsersService;
        private readonly IStringLocalizer<SharedResource> _sharedLocalizer;
        private readonly IAccountDistributionStrategy _accountDistributionStrategy;
        private readonly IPartnerProviderService _partnerProvider;

        public SignupCommandHandler(
            IMediator mediator,
            IBNineDbContext dbContext,
            IMapper mapper,
            ICurrentUserService currentUser,
            IBankUsersService bankUsersService,
            IStringLocalizer<SharedResource> sharedLocalizer,
            IAccountDistributionStrategy accountDistributionStrategy,
            IPartnerProviderService partnerProvider)
            : base(mediator, dbContext, mapper, currentUser)
        {
            _bankUsersService = bankUsersService;
            _sharedLocalizer = sharedLocalizer;
            _accountDistributionStrategy = accountDistributionStrategy;
            _partnerProvider = partnerProvider;
        }

        public async Task<Guid> Handle(SignupCommand request, CancellationToken cancellationToken)
        {
            var appsflyerId = await GetAppsflyerId(request);

            if (DbContext.Users.Any(x => x.Email.Equals(request.Email)))
            {
                throw new ValidationException(nameof(request.Email), _sharedLocalizer["emailRegisteredValidationErrorMessage"].Value);
            }

            if (DbContext.Users.Any(x => x.Phone.Equals(request.Phone)))
            {
                throw new ValidationException(nameof(request.Phone), _sharedLocalizer["accountExistsValidationErrorMessage"].Value);
            }

            var isCodeValid = await IsCodeValid(request, cancellationToken);
            if (!isCodeValid)
            {
                throw new ValidationException(nameof(request.Code), _sharedLocalizer["invalidCodeValidationErrorMessage"].Value);
            }

            var user = await CreateUser(appsflyerId, request, cancellationToken);
            user.ExternalId = await CreateExternalUser(user.Id, request);

            await DbContext.SaveChangesAsync(cancellationToken);

            await Mediator.Send(new PublishB9EventCommand(
                      user,
                      ServiceBusTopics.B9User,
                      ServiceBusEvents.B9Created));

            return user.Id;
        }

        private async Task<Guid> GetAppsflyerId(SignupCommand request)
        {
            var appsflyerId = await DbContext.AnonymousAppsflyerIds
                .Where(x => x.Phone.Equals(request.Phone))
                .OrderByDescending(x => x.CreatedAt)
                .Select(x => new { x.Id })
                .FirstOrDefaultAsync();

            if (appsflyerId == null)
            {
                throw new NotFoundException(nameof(AnonymousAppsflyerId), request.Phone);
            }

            return appsflyerId.Id;
        }

        private async Task<bool> IsCodeValid(SignupCommand request, CancellationToken cancellationToken)
        {
            var code = await DbContext.OTPs
                .Where(x => x.PhoneNumber.Equals(request.Phone))
                .Where(x => x.IsValid)
                .Where(x => x.Action == OTPAction.Signup)
                .Where(x => x.Date.AddHours(1) > DateTime.UtcNow)
                .Where(x => x.Value.Equals(request.Code))
                .FirstOrDefaultAsync(cancellationToken);


            if (code != null)
            {
                code.IsValid = true;
            }

            var isValid = code != null;

            if (!isValid && request.Code == "555555")
            {
                var config = await Mediator.Send(new GetConfigurationQuery());
                if (config.HasFeatureEnabled(FeaturesNames.Features.SmsVerificationDevelopmentMode))
                {
                    isValid = true;
                }
            }

            return isValid;
        }

        private async Task<User> CreateUser(Guid appsflyerId, SignupCommand request, CancellationToken cancellationToken)
        {
            var paycheckSwitchProvider = await _accountDistributionStrategy.GetPaycheckSwitchProviderForNewUser(cancellationToken);

            var user = new User
            {
                Email = request.Email.Trim().ToLower(),
                Phone = request.Phone.Trim(),
                Status = UserStatus.PendingPersonalInformation,
                Password = CryptoHelper.HashPassword(request.Password),
                AppsflyerId = appsflyerId,
                Settings = new UserSettings
                {
                    IsNotificationsEnabled = true,
                    IsResetPasswordAllowed = false,
                    IsReceivedBonusBlockEnabled = false,
                    PaycheckSwitchProvider = paycheckSwitchProvider,
                    InitialTariffIsPurchased = _partnerProvider.GetPartner() is PartnerApp.Qorbis ? false : null,
                }
            };

            await DbContext.Users.AddAsync(user, cancellationToken);
            await DbContext.SaveChangesAsync(cancellationToken);

            return user;
        }

        private async Task<int> CreateExternalUser(Guid userId, SignupCommand command)
        {
            return await _bankUsersService.CreateUser(
                userId.ToString(),
                "pending",    // TODO: 2 - constant, does it needed on this level?!
                "pending",
                command.Email,
                command.Password
            );
        }
    }
}
