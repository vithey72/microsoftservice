﻿namespace BNine.Application.Aggregates.Users.Commands.Delete
{
    using System;
    using System.Linq;
    using System.Threading;
    using System.Threading.Tasks;
    using Abstractions;
    using AutoMapper;
    using AutoMapper.QueryableExtensions;
    using BNine.Settings.Blob;
    using Constants;
    using Domain.Entities.User;
    using Enums;
    using Exceptions;
    using Interfaces;
    using Interfaces.Bank.Administration;
    using MediatR;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.Extensions.Options;
    using Models;

    [Obsolete("TODO: Consider removing handler, found no references")]
    public class DeleteUserCommandHandler : AbstractRequestHandler, IRequestHandler<DeleteUserCommand>
    {
        private BlobStorageSettings BlobSettings
        {
            get;
        }
        private IBlobStorageService BlobStorageService
        {
            get;
        }
        private IBankClientsService ClientsService
        {
            get;
        }
        private IBankSavingAccountsService SavingAccountsService
        {
            get;
        }
        private IBankIdentityService IdentityService
        {
            get;
        }
        private IBankUsersService BankUsersService
        {
            get;
        }

        public DeleteUserCommandHandler(
            IOptions<BlobStorageSettings> options,
            IBlobStorageService blobStorageService,
            IBankClientsService clientsService,
            IBankSavingAccountsService savingAccountsService,
            IBankIdentityService identityService,
            IBankUsersService bankUsersService,
            IMediator mediator,
            IBNineDbContext dbContext,
            IMapper mapper,
            ICurrentUserService currentUser) : base(mediator, dbContext, mapper, currentUser)
        {
            BlobSettings = options.Value;
            BlobStorageService = blobStorageService;
            ClientsService = clientsService;
            SavingAccountsService = savingAccountsService;
            IdentityService = identityService;
            BankUsersService = bankUsersService;
        }

        public async Task<Unit> Handle(DeleteUserCommand request, CancellationToken cancellationToken)
        {
            var user = await DbContext.Users
                .Include(x=> x.Document)
                .FirstOrDefaultAsync(x => x.Phone.Equals(request.PhoneNumber), cancellationToken);

            if (user == null)
            {
                throw new NotFoundException(nameof(User), request.PhoneNumber);
            }

            await DeleteUserFromMbanq(user);

            await RemoveFilesFromBlob(user);

            //var bonusCodes = DbContext.BonusCodes
            //    .Where(x => x.ReceiverId == user.Id || x.SenderId == user.Id)
            //    .ToList();

            //DbContext.BonusCodes.RemoveRange(bonusCodes);
            //DbContext.Users.Remove(user);

            user.Document.Number = "";

            await DbContext.SaveChangesAsync(cancellationToken);

            return Unit.Value;
        }

        private async Task RemoveFilesFromBlob(User user)
        {
            if (user.ProfilePhotoUrl != null)
            {
                await BlobStorageService.RemoveFile(ContainerNames.BnineContainer, user.ProfilePhotoUrl);
            }
        }

        private async Task DeleteUserFromMbanq(User user)
        {
            var externalIds = await DbContext.Users
                .Where(x => x.Id == user.Id)
                .ProjectTo<UserExternalIds>(Mapper.ConfigurationProvider)
                .FirstAsync();

            if (externalIds.ExternalSavingsAccountId.HasValue)
            {
                try
                {
                    await SavingAccountsService.CloseSavingAccount(externalIds.ExternalSavingsAccountId.Value, string.Empty);
                }
                catch
                {

                }
            }
            try
            {
                await DeleteClient(user, externalIds);
            }
            catch
            {

            }
            if (externalIds.ExternalUserId.HasValue)
            {
                try
                {
                    await BankUsersService.DeleteUser(externalIds.ExternalUserId.Value);
                }
                catch
                {

                }
            }
        }

        private async Task DeleteClient(User user, UserExternalIds externalIds)
        {
            if (!externalIds.ExternalClientId.HasValue)
            {
                return;
            }

            if (externalIds.ExternalDocumentId.HasValue)
            {
                try
                {
                    await IdentityService.DeleteIdentity(externalIds.ExternalClientId.Value, externalIds.ExternalDocumentId.Value);
                }
                catch
                {

                }
            }

            if (externalIds.ExternalIdentityId.HasValue)
            {
                try
                {
                    await IdentityService.DeleteIdentity(externalIds.ExternalClientId.Value, externalIds.ExternalIdentityId.Value);
                }
                catch
                {

                }
            }
            try
            {
                await ClientsService.ResetClientUniqueData(externalIds.ExternalClientId!.Value);
            }
            catch
            {

            }

            var properStatus = await ClientsService.GetVerificationStatus(user.ExternalClientId!.Value);
            if (properStatus.Equals(VerificationStatus.NOTSTARTED.ToString(), StringComparison.OrdinalIgnoreCase))
            {
                try
                {
                    await ClientsService.DeleteClient(externalIds.ExternalClientId.Value);
                }
                catch
                {

                }
            }
            else
            {
                try
                {
                    await ClientsService.CloseClient(externalIds.ExternalClientId.Value);
                }
                catch
                {

                }
            }
        }
    }
}
