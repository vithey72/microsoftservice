﻿namespace BNine.Application.Aggregates.Users.Commands.Delete
{
    using System;
    using FluentValidation;

    public class DeleteUserCommandValidator : AbstractValidator<DeleteUserCommand>
    {
        private readonly Guid privateKey = Guid.Parse("594a6e42-c5ac-446f-8966-8ee4ecb0a341");

        public DeleteUserCommandValidator()
        {
            RuleFor(x => x.PhoneNumber).NotEmpty();
            RuleFor(x => x.PrivateKey)
                .NotEmpty()
                .Equal(privateKey)
                .WithMessage("Invalid private key");
        }
    }
}
