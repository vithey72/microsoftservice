﻿namespace BNine.Application.Aggregates.Users.Commands.Delete
{
    using System;
    using MediatR;
    using Newtonsoft.Json;

    public class DeleteUserCommand : IRequest
    {
        [JsonProperty("privateKey")]
        public Guid PrivateKey
        {
            get;
            set;
        }

        [JsonProperty("phoneNumber")]
        public string PhoneNumber
        {
            get;
            set;
        }
    }
}
