﻿namespace BNine.Application.Aggregates.Users.Commands.Block
{
    using System;
    using MediatR;
    using Newtonsoft.Json;

    public class BlockUserCommand : IRequest<Unit>
    {
        [JsonIgnore]
        public Guid Id
        {
            get; set;
        }

        [JsonProperty("reasonId")]
        public Guid ReasonId
        {
            get; set;
        }
    }
}
