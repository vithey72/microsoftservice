﻿namespace BNine.Application.Aggregates.Users.Commands.Block
{
    using System.Linq;
    using BNine.Application.Interfaces;
    using FluentValidation;

    public class BlockUserCommandValidator
        : AbstractValidator<BlockUserCommand>
    {
        public BlockUserCommandValidator(IBNineDbContext dbContext)
        {
            RuleFor(x => x.Id)
                .NotEmpty();

            RuleFor(x => x.ReasonId)
                .Must(x => dbContext.UserBlockReasons.Any(r => r.Id == x));
        }
    }
}
