﻿namespace BNine.Application.Aggregates.Users.Commands.Unblock
{
    using System.Linq;
    using System.Threading;
    using System.Threading.Tasks;
    using AutoMapper;
    using BNine.Application.Abstractions;
    using BNine.Application.Aggregates.LogAdminAction.Commands;
    using BNine.Application.Aggregates.Users.AccountStateMachine;
    using BNine.Application.Exceptions;
    using BNine.Application.Interfaces;
    using BNine.Domain.Entities.User;
    using BNine.Enums;
    using MediatR;
    using Microsoft.EntityFrameworkCore;
    using Newtonsoft.Json;

    public class UnblockUserCommandHandler
        : AbstractRequestHandler, IRequestHandler<UnblockUserCommand, Unit>
    {
        private readonly IClientAccountBlockedStateProvider _accountStateProvider;

        public UnblockUserCommandHandler(
            IMediator mediator,
            IBNineDbContext dbContext,
            IMapper mapper,
            ICurrentUserService currentUser,
            IClientAccountBlockedStateProvider accountStateProvider
            ) : base(mediator, dbContext, mapper, currentUser)
        {
            _accountStateProvider = accountStateProvider;
        }

        public async Task<Unit> Handle(UnblockUserCommand request, CancellationToken cancellationToken)
        {
            var user = await DbContext.Users
                .Where(x => x.Id == request.Id)
                .FirstOrDefaultAsync();

            if (user == null)
            {
                throw new NotFoundException(nameof(User), request.Id);
            }

            if (!user.IsBlocked)
            {
                return Unit.Value;
            }

            var userOldValue = JsonConvert.SerializeObject(user, Formatting.None,
                new JsonSerializerSettings() { ReferenceLoopHandling = ReferenceLoopHandling.Ignore });

            var accountState = await _accountStateProvider.GetCustomAccountBlockedState(request.Id, cancellationToken);

            await accountState.UnblockAccount(cancellationToken);

            var userNewValue = JsonConvert.SerializeObject(user, Formatting.None,
                new JsonSerializerSettings() { ReferenceLoopHandling = ReferenceLoopHandling.Ignore });

            await SendLogAdminActionCommand(userOldValue, userNewValue, user.Id.ToString());

            return Unit.Value;
        }

        private async Task SendLogAdminActionCommand(string userOldValue, string userNewValue, string clientId)
        {
            await Mediator.Send(new LogAdminActionCommand()
            {
                EntityName = nameof(Domain.Entities.User.User),
                Type = AdminActionType.UnblockClient,
                EntityId = clientId,
                BeforeChanges = userOldValue,
                AfterChanges = userNewValue
            });
        }
    }
}
