﻿namespace BNine.Application.Aggregates.Users.Commands.Unblock
{
    using System;
    using MediatR;

    public class UnblockUserCommand : IRequest<Unit>
    {
        public UnblockUserCommand(Guid id)
        {
            Id = id;
        }

        public Guid Id
        {
            get;
        }
    }
}
