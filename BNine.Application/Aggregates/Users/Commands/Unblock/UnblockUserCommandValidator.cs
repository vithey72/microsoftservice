﻿namespace BNine.Application.Aggregates.Users.Commands.Unblock
{
    using FluentValidation;

    public class UnblockUserCommandValidator
     : AbstractValidator<UnblockUserCommand>
    {
        public UnblockUserCommandValidator()
        {
            RuleFor(x => x.Id)
                .NotEmpty();
        }
    }
}
