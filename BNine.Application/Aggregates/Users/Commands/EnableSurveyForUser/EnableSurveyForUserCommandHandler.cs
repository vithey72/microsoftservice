﻿namespace BNine.Application.Aggregates.Users.Commands.EnableSurveyForUser
{
    using System.Threading.Tasks;
    using Abstractions;
    using AutoMapper;
    using BNine.Enums;
    using Constants;
    using Interfaces;
    using MediatR;
    using Microsoft.EntityFrameworkCore;

    public class EnableSurveyForUserCommandHandler
        : AbstractRequestHandler,
        IRequestHandler<EnableSurveyForUserCommand, Unit>
    {
        private readonly IFeatureConfigurationService _featuresService;
        private readonly INotificationUsersService _notificationService;

        public EnableSurveyForUserCommandHandler(
            IMediator mediator,
            IBNineDbContext dbContext,
            IMapper mapper,
            IFeatureConfigurationService featuresService,
            INotificationUsersService notificationService,
            ICurrentUserService currentUser)
            : base(mediator, dbContext, mapper, currentUser)
        {
            _featuresService = featuresService;
            _notificationService = notificationService;
        }

        public async Task<Unit> Handle(EnableSurveyForUserCommand request, CancellationToken cancellationToken)
        {
            if (request.Users == null)
            {
                return Unit.Value;
            }

            foreach (var requestedUser in request.Users)
            {
                var entity = await DbContext
                    .Users
                    .Include(u => u.Settings)
                    .Include(u => u.Devices)
                    .FirstOrDefaultAsync(u => u.Id == requestedUser.UserId, cancellationToken);

                if (entity == null) { continue; }

                if (requestedUser.IsSurveyVisible)
                {
                    await _featuresService.AddUserToGroup(entity.Id, FeaturesNames.Groups.ShowManualFormSurvey, cancellationToken);
                    await _notificationService.SendNotification(
                        entity.Id,
                        request.Notification.Message,
                        entity.Settings.IsNotificationsEnabled,
                        NotificationTypeEnum.Information,
                        true,
                        entity.Devices,
                        request.Notification.Deeplink,
                        request.Notification.AppsflyerEventType,
                        request.Notification.Category,
                        request.Notification.Title);
                }
                else
                {
                    await _featuresService.RemoveUserFromGroup(entity.Id, FeaturesNames.Groups.ShowManualFormSurvey, cancellationToken);
                }
            }

            return Unit.Value;
        }
    }
}
