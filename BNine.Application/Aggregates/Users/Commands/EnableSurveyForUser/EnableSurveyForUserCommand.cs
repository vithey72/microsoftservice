﻿namespace BNine.Application.Aggregates.Users.Commands.EnableSurveyForUser
{
    using System;
    using MediatR;
    using Models;
    using Newtonsoft.Json;

    public class EnableSurveyForUserCommand : IRequest<Unit>
    {
        [JsonProperty("manualFormSurvey")]
        public ManualFormSurveyUser[] Users
        {
            get;
            set;
        }

        [JsonProperty("notification")]
        public NotificationParameters Notification
        {
            get;
            set;
        }

        public class ManualFormSurveyUser
        {
            [JsonProperty("userId")]
            public Guid UserId
            {
                get;
                set;
            }

            [JsonProperty("isSurveyVisible")]
            public bool IsSurveyVisible
            {
                get;
                set;
            }
        }
    }
}
