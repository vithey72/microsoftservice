﻿namespace BNine.Application.Aggregates.Users.Commands.CloseAccount.CloseClientAccountInternal;

using Abstractions;
using AutoMapper;
using BNine.Application.Aggregates.Cards.Commands.SyncCardStatuses;
using Interfaces.Bank.Administration;
using Domain.Entities.User;
using Enums.DebitCard;
using Enums;
using Exceptions;
using Interfaces;
using MediatR;
using Microsoft.EntityFrameworkCore;
using BNine.Application.Aggregates.Users.Closure;
using BNine.Application.Aggregates.PaycheckSwitch;

public class CloseClientAccountInternalCommandHandler
    : AbstractRequestHandler
    , IRequestHandler<CloseClientAccountInternalCommand, Unit>
{
    private readonly IBankSavingAccountsService _bankSavingAccountsService;
    private readonly IBankExternalCardsService _bankExternalCardsService;
    private readonly IBankInternalCardsService _bankInternalCardsService;
    private readonly IPaycheckSwitchDiscovery _paycheckSwitchDiscovery;
    private readonly IBankClientsService _bankClientsService;
    private readonly IAccountClosurePrerequisitesProvider _accountClosurePrerequisitesProvider;

    public CloseClientAccountInternalCommandHandler(
        IMediator mediator,
        IBNineDbContext dbContext,
        IMapper mapper,
        ICurrentUserService currentUser,
        IBankInternalCardsService bankInternalCardsService,
        IBankExternalCardsService bankExternalCardsService,
        IBankClientsService bankClientsService,
        IBankSavingAccountsService bankSavingAccountsService,
        IAccountClosurePrerequisitesProvider accountClosurePrerequisitesProvider,
        IPaycheckSwitchDiscovery paycheckSwitchDiscovery
        )
        : base(mediator, dbContext, mapper, currentUser)
    {
        _bankInternalCardsService = bankInternalCardsService;
        _bankExternalCardsService = bankExternalCardsService;
        _bankClientsService = bankClientsService;
        _bankSavingAccountsService = bankSavingAccountsService;
        _accountClosurePrerequisitesProvider = accountClosurePrerequisitesProvider;
        _paycheckSwitchDiscovery = paycheckSwitchDiscovery;
    }

    public async Task<Unit> Handle(CloseClientAccountInternalCommand request, CancellationToken cancellationToken)
    {
        var prerequisites = await _accountClosurePrerequisitesProvider.GetPrerequisites(request.UserId, cancellationToken);
        if (!prerequisites.CanCloseAccount())
        {
            throw new InvalidOperationException("Cannot close account until all conditions are satisfied.");
        }

        var user = DbContext.Users.Single(x => x.Id == request.UserId);

        var closingReason = await DbContext
            .UserBlockReasons
            .FirstOrDefaultAsync(x => x.Id == request.ReasonId, cancellationToken);

        if (closingReason is null)
        {
            throw new NotFoundException(nameof(BlockUserReason), request.ReasonId);
        }

        var paycheckSwitch = await _paycheckSwitchDiscovery.DiscoverUserPaycheckSwitch(request.UserId, cancellationToken);
        await paycheckSwitch.DeleteAccount(cancellationToken);

        if (user.ExternalClientId.HasValue)
        {
            await TerminateInternalCards(user);

            await CloseSavingAccount(user, closingReason);

            await DeleteExternalCards(user);
        }

        await CloseAccount(request.ReasonId, user);

        await DbContext.SaveChangesAsync(cancellationToken);

        return Unit.Value;
    }

    private async Task DeleteExternalCards(User user)
    {
        //Delete all external cards
        var externalCards = user.ExternalCards.Where(x => !x.DeletedAt.HasValue).ToList();

        try
        {
            var mbanqExternalCards = await _bankExternalCardsService.GetExternalCards(user.ExternalClientId.Value);

            foreach (var mbanqExternalCard in mbanqExternalCards)
            {
                var b9ExternalCard = externalCards?.FirstOrDefault(x => x.ExternalId == mbanqExternalCard.Id);

                if (!mbanqExternalCard.IsDeleted)
                {
                    await _bankExternalCardsService.DeleteCard(user.ExternalClientId.Value, mbanqExternalCard.Id);
                }

                b9ExternalCard?.Delete();
            }
        }
        catch (Exception ex)
        {
            throw new ValidationException("externalCard",
                $"An error has occurred during deleting external cards, MBanq: {ex.Message}");
        }
    }

    private async Task CloseSavingAccount(User user, BlockUserReason closingReason)
    {
        if (user.CurrentAccount is not null)
        {
            try
            {
                var savingsAccount = await _bankSavingAccountsService.GetSavingAccountInfo(user.CurrentAccount.ExternalId);

                if (savingsAccount.Status != SavingsAccountStatus.Closed.ToString())
                {
                    await _bankSavingAccountsService.CloseSavingAccount(user.CurrentAccount.ExternalId, closingReason.Value);
                }
            }
            catch (Exception ex)
            {
                throw new ValidationException("savingAccount",
                    $"An error has occurred during closing saving account operation, MBanq: {ex.Message}");
            }
        }
    }

    private async Task CloseAccount(Guid reasonId, User user)
    {
        try
        {
            //Close account in mbanq
            await _bankClientsService.CloseClient(user.ExternalClientId.Value);
        }
        catch (Exception ex)
        {
            throw new ValidationException("clientAccount",
                $"An error has occurred during closing client account operation, MBanq: {ex.Message}");
        }

        user.CloseClient(reasonId);
    }

    private async Task TerminateInternalCards(User user)
    {
        await Mediator.Send(new SyncDebitCardStatusesCommand()
        {
            UserId = user.Id
        });

        //Terminate internal cards
        var internalCards =
            user.DebitCards.Where(x => x.Status is CardStatus.ACTIVE or CardStatus.SUSPENDED);

        try
        {
            foreach (var internalCard in internalCards)
            {
                await _bankInternalCardsService.TerminateClientCard(internalCard.ExternalId);

                internalCard.Terminate();
            }
        }
        catch (Exception ex)
        {
            throw new ValidationException("internalCard",
                $"An error has occurred during terminate internal card operation, MBanq: {ex.Message}");
        }
    }
}
