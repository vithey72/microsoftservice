﻿namespace BNine.Application.Aggregates.Users.Commands.CloseAccount.CloseClientAccountInternal;

using MediatR;
using Newtonsoft.Json;

public class CloseClientAccountInternalCommand : IRequest<Unit>
{
    public Guid UserId
    {
        get;
        set;
    }

    [JsonProperty("reasonId")]
    public Guid ReasonId
    {
        get; set;
    }
}
