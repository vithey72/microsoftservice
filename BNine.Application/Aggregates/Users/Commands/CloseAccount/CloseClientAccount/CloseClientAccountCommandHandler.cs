﻿namespace BNine.Application.Aggregates.Users.Commands.CloseAccount.CloseClientAccount;

using Abstractions;
using AutoMapper;
using BNine.Application.Aggregates.Users.AccountStateMachine;
using Domain.Entities.User;
using Enums;
using Exceptions;
using Interfaces;
using LogAdminAction.Commands;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;

public class CloseClientAccountCommandHandler
    : AbstractRequestHandler
    , IRequestHandler<CloseClientAccountCommand, Unit>
{
    private readonly IClientAccountBlockedStateProvider _accountStateProvider;

    public CloseClientAccountCommandHandler(
        IMediator mediator,
        IBNineDbContext dbContext,
        IMapper mapper,
        ICurrentUserService currentUser,
        IClientAccountBlockedStateProvider accountStateProvider
        )
        : base(mediator, dbContext, mapper, currentUser)
    {
        _accountStateProvider = accountStateProvider;
    }

    public async Task<Unit> Handle(CloseClientAccountCommand request, CancellationToken cancellationToken)
    {
        var user = await DbContext.Users
            .Include(x => x.EarlySalaryWidgetConfiguration)
            .Include(x => x.DebitCards)
            .Include(x => x.CurrentAccount)
            .FirstOrDefaultAsync(x => x.Id == request.UserId, cancellationToken);

        if (user is null)
        {
            throw new NotFoundException(nameof(User), request.UserId);
        }

        var userOldValue = SerializeUser(user);

        var accountState = await _accountStateProvider.GetCustomAccountBlockedState(request.UserId, cancellationToken);

        await accountState.CloseAccount(request.ReasonId, cancellationToken);

        var userNewValue = SerializeUser(user);

        await SendLogAdminActionCommand(userOldValue, userNewValue, user.Id.ToString());

        return Unit.Value;
    }

    private static string SerializeUser(User user)
    {
        return JsonConvert.SerializeObject(user, Formatting.None,
            new JsonSerializerSettings { ReferenceLoopHandling = ReferenceLoopHandling.Ignore });
    }

    private async Task SendLogAdminActionCommand(string userOldValue, string userNewValue, string clientId)
    {
        try
        {
            await Mediator.Send(new LogAdminActionCommand()
            {
                EntityName = nameof(User),
                Type = AdminActionType.CloseAccount,
                EntityId = clientId,
                BeforeChanges = userOldValue,
                AfterChanges = userNewValue
            });
        }
        catch (Exception ex)
        {
            throw new ValidationException("logAdminAction",
                $"An error has been occured during sending log admin action event to DWH Integration, MBanq: {ex.Message}");
        }
    }
}
