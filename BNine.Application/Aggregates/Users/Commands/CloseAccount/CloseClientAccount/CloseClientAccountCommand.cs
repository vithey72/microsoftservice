﻿namespace BNine.Application.Aggregates.Users.Commands.CloseAccount.CloseClientAccount;

using MediatR;
using Newtonsoft.Json;

public class CloseClientAccountCommand : IRequest<Unit>
{
    public Guid UserId
    {
        get;
        set;
    }

    [JsonProperty("reasonId")]
    public Guid ReasonId
    {
        get; set;
    }
}
