﻿namespace BNine.Application.Aggregates.Users.Commands.CloseAccount.MarkLoansAsClosedIfNeeded;

using MediatR;

public record MarkLoansAsClosedIfNeededCommand : IRequest<Unit>
{
    public Guid UserId
    {
        get;
        set;
    }
}
