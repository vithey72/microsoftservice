﻿namespace BNine.Application.Aggregates.Users.Commands.CloseAccount.MarkLoansAsClosedIfNeeded;

using Abstractions;
using AutoMapper;
using Domain.Entities.User;
using Enums;
using Exceptions;
using Interfaces;
using Interfaces.Bank.Administration;
using MediatR;
using Microsoft.EntityFrameworkCore;

public class MarkLoansAsClosedIfNeededCommandHandler
    : AbstractRequestHandler
    , IRequestHandler<MarkLoansAsClosedIfNeededCommand, Unit>
{
    private readonly IBankLoanAccountsService _bankLoanAccountsService;

    public MarkLoansAsClosedIfNeededCommandHandler(
        IMediator mediator,
        IBNineDbContext dbContext,
        IMapper mapper,
        ICurrentUserService currentUser,
        IBankLoanAccountsService bankLoanAccountsService
        )
        : base(mediator, dbContext, mapper, currentUser)
    {
        _bankLoanAccountsService = bankLoanAccountsService;
    }

    public async Task<Unit> Handle(MarkLoansAsClosedIfNeededCommand request, CancellationToken cancellationToken)
    {
        var user = await DbContext
            .Users
            .Include(u => u.LoanAccounts)
            .FirstOrDefaultAsync(
                u => u.Id == request.UserId,
                    cancellationToken);

        if (user is null)
        {
            throw new NotFoundException(nameof(User), request.UserId);
        }

        if (user.LoanAccounts.Count == 0)
        {
            return Unit.Value;
        }

        foreach (var loanAcc in user.LoanAccounts.Where(x => x.Status == LoanAccountStatus.Active))
        {
            var loanRetrieved = await _bankLoanAccountsService.GetLoan(loanAcc.ExternalId);

            if (loanRetrieved.IsClosedById())
            {
                loanAcc.Status = LoanAccountStatus.Closed;
                await DbContext.SaveChangesAsync(CancellationToken.None);
            }
        }

        return Unit.Value;
    }
}
