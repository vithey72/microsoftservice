﻿namespace BNine.Application.Aggregates.Users.Commands.SwitchCashbackForUsers;

using MediatR;
using Models;
using Newtonsoft.Json;

public class SwitchCashbackForUsersCommand : IRequest<Unit>
{
    [JsonProperty("requests")]
    public UserCashbackSwitchRequest[] Requests
    {
        get;
        set;
    }
}
