﻿namespace BNine.Application.Aggregates.Users.Commands.SwitchCashbackForUsers;

using AutoMapper;
using Domain.Entities.CashbackProgram;
using Interfaces;
using Interfaces.ServiceBus;
using MediatR;
using Queries.Cashback;

public class SwitchCashbackForUsersCommandHandler
    : BaseCashbackQueryHandler,
      IRequestHandler<SwitchCashbackForUsersCommand, Unit>
{
    public SwitchCashbackForUsersCommandHandler(
        IMediator mediator,
        IBNineDbContext dbContext,
        IMapper mapper,
        ICurrentUserService currentUser,
        IServiceBus serviceBus,
        IDateTimeProviderService dateTimeProvider
    )
        : base(mediator, dbContext, mapper, currentUser, serviceBus, dateTimeProvider)
    {
    }

    public async Task<Unit> Handle(SwitchCashbackForUsersCommand request, CancellationToken cancellationToken)
    {
        foreach (var switchRequest in request.Requests)
        {
            var (month, year) = ExtractMonthAndYear(switchRequest.YearMonth);
            var existing = await GetCashbackEntity(month, year, CancellationToken.None, switchRequest.UserId);

            // do not alter historical records
            if (year < DateTime.Now.Year)
            {
                continue;
            }
            if (month < DateTime.Now.Month && year == DateTime.Now.Year)
            {
                continue;
            }

            if (existing != null)
            {
                if (switchRequest.IsCashbackEnabled != false)
                {
                    continue;
                }

                DbContext.UsedCashbacks.Remove(existing);
                await DbContext.SaveChangesAsync(cancellationToken);
                await base.SendServiceBusMessage(existing, false, true);
            }
            else
            {
                if (switchRequest.IsCashbackEnabled != true)
                {
                    continue;
                }

                var cashbackEntry = new UsedCashback
                {
                    UserId = switchRequest.UserId,
                    EnabledAt = DateTime.Now,
                    ExpiredAt = GetExpirationDate(year, month),
                    Month = month,
                    Year = year,
                };
                DbContext.UsedCashbacks.Add(cashbackEntry);
                await DbContext.SaveChangesAsync(cancellationToken);
                await base.SendServiceBusMessage(cashbackEntry, true, true);
            }
        }

        return Unit.Value;
    }
}
