﻿namespace BNine.Application.Aggregates.Users.Commands.Update.ForceUpdateAddress
{
    using System;
    using System.Linq;
    using System.Threading;
    using System.Threading.Tasks;
    using Abstractions;
    using AutoMapper;
    using BNine.Application.Aggregates.LogAdminAction.Commands;
    using BNine.Application.Aggregates.Users.Models;
    using BNine.Application.Interfaces.Bank.Administration;
    using BNine.Application.Interfaces.Bank.Client;
    using BNine.Common;
    using BNine.Constants;
    using Domain.Entities.User;
    using Enums;
    using Exceptions;
    using Interfaces;
    using MediatR;
    using Microsoft.EntityFrameworkCore;
    using Newtonsoft.Json;

    public class ForceUpdateAddressCommandHandler
        : AbstractRequestHandler, IRequestHandler<ForceUpdateAddressCommand>
    {
        private readonly IBankClientAddressService _bankClientAddressService;
        private readonly IBankTemplatesService _bankTemplatesService;

        public ForceUpdateAddressCommandHandler(
            IMediator mediator,
            IBNineDbContext dbContext,
            IMapper mapper,
            ICurrentUserService currentUser,
            IBankClientAddressService bankClientAddressService,
            IBankTemplatesService bankTemplatesService)
            : base(mediator, dbContext, mapper, currentUser)
        {
            _bankClientAddressService = bankClientAddressService;
            _bankTemplatesService = bankTemplatesService;
        }

        public async Task<Unit> Handle(ForceUpdateAddressCommand request, CancellationToken cancellationToken)
        {
            var user = DbContext.Users
                .Include(x => x.Devices)
                .FirstOrDefault(x => x.Id == request.UserId);

            if (user.Status != UserStatus.Active)
            {
                throw new Exception("Sorry, this request doesn't work for a non active users");
            }

            if (user == null)
            {
                throw new NotFoundException(nameof(User), CurrentUser.UserId);
            }

            var userOldValue = JsonConvert.SerializeObject(user, Formatting.None,
                new JsonSerializerSettings() { ReferenceLoopHandling = ReferenceLoopHandling.Ignore });

            var addressExternalId = user.Address.ExternalId;

            user.Address = Mapper.Map(request, user.Address);

            var userNewValue = JsonConvert.SerializeObject(user, Formatting.None,
                new JsonSerializerSettings() { ReferenceLoopHandling = ReferenceLoopHandling.Ignore });

            await SetMbanqClientAddress(user, addressExternalId.Value);

            await DbContext.SaveChangesAsync(cancellationToken);

            await SendLogAdminActionCommand(userOldValue, userNewValue, user.Id.ToString());

            return Unit.Value;
        }

        private async Task SendLogAdminActionCommand(string userOldValue, string userNewValue, string clientId)
        {
            await Mediator.Send(new LogAdminActionCommand()
            {
                EntityName = nameof(Domain.Entities.User.User),
                Type = AdminActionType.ChangeAddress,
                EntityId = clientId,
                BeforeChanges = userOldValue,
                AfterChanges = userNewValue
            });
        }

        private async Task SetMbanqClientAddress(User user, int addressExternalId)
        {
            var addressParametersOptions = await _bankTemplatesService.GetAddressParametersOptions();
            var userStateInfo = addressParametersOptions.StateIdOptions.FirstOrDefault(x =>
                x.Name.Equals(StatesHelper.MapToFullStateName(user.Address.State)));

            if (userStateInfo == null)
            {
                throw new NotFoundException(nameof(CountryIdOption), user.Address.State);
            }

            var userAddressType =
                addressParametersOptions.AddressTypeIdOptions.FirstOrDefault(x =>
                    x.Name.Equals(ClientAddressTypes.Primary));
            if (userAddressType == null)
            {
                throw new NotFoundException(nameof(AddressTypeIdOption), ClientAddressTypes.Primary);
            }

            var addresses =
                await _bankClientAddressService.GetAddresses(user.ExternalClientId.GetValueOrDefault(),
                    userAddressType.Id);

            var inactiveAddress =
                addresses?.FirstOrDefault(x => x.AddressId == addressExternalId && x.IsActive == false);

            if (inactiveAddress is not null)
            {
                throw new ValidationException("address",
                        $"Cannot change address due to address is disabled by the compliance team");
            }

            try
            {
                user.Address.ExternalId = await _bankClientAddressService.UpdateAddress(
                    addressExternalId,
                    user.Address.City,
                    userStateInfo.Id,
                    user.Address.AddressLine,
                    user.Address.ZipCode,
                    user.Address.Unit,
                    userStateInfo.ParentId);
            }
            catch (Exception ex)
            {
                throw new ValidationException("address",
                    $"An error has been occured during update address, {ex.Message}");
            }
        }
    }
}
