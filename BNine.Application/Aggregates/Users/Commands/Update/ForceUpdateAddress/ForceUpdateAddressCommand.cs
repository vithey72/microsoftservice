﻿namespace BNine.Application.Aggregates.Users.Commands.Update.ForceUpdateAddress
{
    using System;
    using AutoMapper;
    using BNine.Application.Mappings;
    using Domain.Entities.Common;
    using MediatR;

    public class ForceUpdateAddressCommand : IRequest<Unit>, IMapTo<Address>
    {
        public Guid UserId
        {
            get; set;
        }

        public string ZipCode
        {
            get; set;
        }

        public string City
        {
            get; set;
        }

        public string State
        {
            get; set;
        }

        public string Address
        {
            get; set;
        }

        public string Unit
        {
            get; set;
        }

        public void Mapping(Profile profile)
        {
            profile.CreateMap<ForceUpdateAddressCommand, Address>()
                .ForMember(dst => dst.AddressLine, opt => opt.MapFrom(src => src.Address))
                .ForMember(dst => dst.City, opt => opt.MapFrom(src => src.City))
                .ForMember(dst => dst.State, opt => opt.MapFrom(src => src.State))
                .ForMember(dst => dst.ZipCode, opt => opt.MapFrom(src => src.ZipCode))
                .ForMember(dst => dst.Unit, opt => opt.MapFrom(src => src.Unit));
        }
    }
}
