﻿namespace BNine.Application.Aggregates.Users.Commands.Update.UpdateAddress
{
    using System.Text.RegularExpressions;
    using BNine.Application.Interfaces;
    using BNine.Application.Models.Validators;
    using BNine.Common;
    using BNine.ResourceLibrary;
    using FluentValidation;
    using Microsoft.Extensions.Localization;

    public class ForceUpdateAddressCommandValidator
        : AbstractOnboardingStepValidator<UpdateAddressCommand>
    {
        private IStringLocalizer<SharedResource> SharedLocalizer
        {
            get;
        }

        private readonly Regex cityRegex = new Regex("^[a-zA-Z- ]+$");

        public ForceUpdateAddressCommandValidator(
            IStringLocalizer<SharedResource> sharedLocalizer,
            ICurrentUserService currentUserService, IBNineDbContext dbContext
            )
            : base(dbContext, currentUserService)
        {
            SharedLocalizer = sharedLocalizer;
            Step = Enums.UserStatus.PendingAddress;

            RuleFor(x => x.Address)
                .NotEmpty();

            RuleFor(x => x.City)
                .NotEmpty()
                .Must(x => cityRegex.IsMatch(x))
                .WithMessage(SharedLocalizer["invalidCityValidationErrorMessage"].Value);

            RuleFor(x => x.ZipCode)
                .NotEmpty()
                .MaximumLength(5);

            RuleFor(x => x.State)
                .NotEmpty()
                .Must(StatesHelper.IsInList)
                .WithMessage(SharedLocalizer["invalidStateValidationErrorMessage"].Value);
        }
    }
}
