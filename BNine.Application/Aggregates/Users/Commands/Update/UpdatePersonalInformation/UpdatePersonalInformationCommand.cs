﻿namespace BNine.Application.Aggregates.Users.Commands.Update.UpdatePersonalInformation
{
    using System;
    using AutoMapper;
    using Domain.Entities.User;
    using Mappings;
    using MediatR;
    using Newtonsoft.Json;

    public class UpdatePersonalInformationCommand : IRequest<Unit>, IMapTo<User>
    {
        [JsonProperty("firstName")]
        public string FirstName
        {
            get; set;
        }

        [JsonProperty("lastName")]
        public string LastName
        {
            get; set;
        }

        [JsonProperty("dateOfBirth")]
        public DateTime DateOfBirth
        {
            get; set;
        }

        public void Mapping(Profile profile)
        {
            profile.CreateMap<UpdatePersonalInformationCommand, User>()
                .ForMember(dest => dest.DateOfBirth, opt => opt.MapFrom(src => src.DateOfBirth));
        }
    }
}
