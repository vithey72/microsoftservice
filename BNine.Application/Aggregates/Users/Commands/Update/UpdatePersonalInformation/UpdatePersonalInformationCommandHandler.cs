﻿namespace BNine.Application.Aggregates.Users.Commands.Update.UpdatePersonalInformation
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading;
    using System.Threading.Tasks;
    using Abstractions;
    using AutoMapper;
    using BNine.Application.Aggregates.ServiceBusEvents.Commands.PublishB9Event;
    using BNine.Constants;
    using BNine.Domain.Events.Users;
    using Domain.Entities.User;
    using Enums;
    using Exceptions;
    using Interfaces;
    using MediatR;
    using Newtonsoft.Json;

    public class UpdatePersonalInformationCommandHandler :
        AbstractRequestHandler,
        IRequestHandler<UpdatePersonalInformationCommand>
    {
        public UpdatePersonalInformationCommandHandler(
            IMediator mediator,
            IBNineDbContext dbContext,
            IMapper mapper,
            ICurrentUserService currentUser) : base(mediator, dbContext, mapper, currentUser)
        {
        }

        public async Task<Unit> Handle(UpdatePersonalInformationCommand request, CancellationToken cancellationToken)
        {
            var user = DbContext.Users.FirstOrDefault(x => x.Id == CurrentUser.UserId);
            if (user == null)
            {
                throw new NotFoundException(nameof(User), CurrentUser.UserId);
            }

            Mapper.Map(request, user);
            user.FirstName = UppercaseFirstLetters(request.FirstName);
            user.LastName = UppercaseFirstLetters(request.LastName);

            var statusChanged = false;
            if (user.Status == UserStatus.PendingPersonalInformation)
            {
                user.Status = UserStatus.PendingAddress;
                statusChanged = true;
            }

            user.DomainEvents.Add(new OnboardingStepSuccessEvent(user, UserStatus.PendingPersonalInformation, UserStatusDetailed.PendingPersonalInformation));

            await DbContext.SaveChangesAsync(cancellationToken);

            await Mediator.Send(new PublishB9EventCommand(
                 user,
                 ServiceBusTopics.B9User,
                 ServiceBusEvents.B9Updated,
                 new Dictionary<string, object>
                 {
                    { ServiceBusDefaults.UpdatedFieldsAdditionalProperty , JsonConvert.SerializeObject(new List<string> { nameof(user.FirstName), nameof(user.LastName) }) }
                 }));


            if (statusChanged)
            {
                await Mediator.Send(new PublishB9EventCommand(
                 user,
                 ServiceBusTopics.B9User,
                 ServiceBusEvents.B9Updated,
                 new Dictionary<string, object>
                 {
                    { ServiceBusDefaults.UpdatedFieldsAdditionalProperty , JsonConvert.SerializeObject(new List<string> { nameof(user.Status) }) }
                 }));
            }

            return Unit.Value;
        }

        private string UppercaseFirstLetters(string name)
        {
            var result = string.Empty;
            var parts = name.Trim().Split(" ");
            foreach (var part in parts)
            {
                if (!string.IsNullOrWhiteSpace(part))
                {
                    result += part.First().ToString().ToUpper() + part.Substring(1) + " ";
                }
            }

            return result.Remove(result.Length - 1);
        }
    }
}
