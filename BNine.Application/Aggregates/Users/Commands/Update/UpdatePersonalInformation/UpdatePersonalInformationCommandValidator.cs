﻿namespace BNine.Application.Aggregates.Users.Commands.Update.UpdatePersonalInformation
{
    using System;
    using BNine.Application.Interfaces;
    using BNine.Application.Models.Validators;
    using BNine.ResourceLibrary;
    using FluentValidation;
    using Helpers;
    using Microsoft.Extensions.Localization;

    public class UpdatePersonalInformationCommandValidator
        : AbstractOnboardingStepValidator<UpdatePersonalInformationCommand>
    {
        private readonly IPartnerProviderService _partnerProvider;

        private IStringLocalizer<SharedResource> SharedLocalizer
        {
            get;
        }

        public UpdatePersonalInformationCommandValidator(
            ICurrentUserService currentUserService,
            IBNineDbContext dbContext,
            IStringLocalizer<SharedResource> sharedLocalizer,
            IPartnerProviderService partnerProvider
            ) : base(dbContext, currentUserService)
        {
            _partnerProvider = partnerProvider;
            SharedLocalizer = sharedLocalizer;
            Step = Enums.UserStatus.PendingPersonalInformation;

            RuleFor(x => x.DateOfBirth)
                .NotEmpty()
                .Must(MoreThen18)
                .WithMessage(StringProvider.ReplaceCustomizedStrings(
                    SharedLocalizer["lowAgeValidationErrorMessage"].Value, _partnerProvider));

            RuleFor(x => x.FirstName)
               .NotEmpty();

            RuleFor(x => x.LastName)
               .NotEmpty();
        }

        private bool MoreThen18(DateTime date)
        {
            return DateTime.UtcNow.AddYears(-18) >= date;
        }
    }
}
