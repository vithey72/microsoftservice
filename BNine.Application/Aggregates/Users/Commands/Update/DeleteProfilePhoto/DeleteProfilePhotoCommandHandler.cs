﻿namespace BNine.Application.Aggregates.Users.Commands.Update.DeleteProfilePhoto;

using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using BNine.Application.Abstractions;
using BNine.Application.Aggregates.ServiceBusEvents.Commands.PublishB9Event;
using BNine.Application.Exceptions;
using BNine.Application.Interfaces;
using BNine.Constants;
using BNine.Domain.Entities.User;
using MediatR;
using Newtonsoft.Json;

public class DeleteProfilePhotoCommandHandler
    : AbstractRequestHandler
    , IRequestHandler<DeleteProfilePhotoCommand>
{
    private readonly IBlobStorageService _blobStorageService;

    public DeleteProfilePhotoCommandHandler(
        IBlobStorageService blobStorageService,
        IMediator mediator,
        IBNineDbContext dbContext,
        IMapper mapper,
        ICurrentUserService currentUser
        )
        : base(mediator, dbContext, mapper, currentUser)
    {
        _blobStorageService = blobStorageService;
    }

    public async Task<Unit> Handle(DeleteProfilePhotoCommand request, CancellationToken cancellationToken)
    {
        var user = DbContext.Users.FirstOrDefault(x => x.Id == CurrentUser.UserId);
        if (user == null)
        {
            throw new NotFoundException(nameof(User), CurrentUser.UserId);
        }

        if (user.ProfilePhotoUrl == null)
        {
            return Unit.Value;
        }

        await _blobStorageService.RemoveFile(ContainerNames.BnineContainer, user.ProfilePhotoUrl, cancellationToken);

        user.ProfilePhotoUrl = null;

        await DbContext.SaveChangesAsync(cancellationToken);

        await Mediator.Send(new PublishB9EventCommand(
            user,
            ServiceBusTopics.B9User,
            ServiceBusEvents.B9Updated,
            new Dictionary<string, object>
            {
                {
                    ServiceBusDefaults.UpdatedFieldsAdditionalProperty,
                    JsonConvert.SerializeObject(new List<string> { nameof(user.ProfilePhotoUrl) })
                }
            }));

        return Unit.Value;
    }
}
