﻿namespace BNine.Application.Aggregates.Users.Commands.Update.DeleteProfilePhoto;

using MediatR;

public record DeleteProfilePhotoCommand() : IRequest<Unit>;
