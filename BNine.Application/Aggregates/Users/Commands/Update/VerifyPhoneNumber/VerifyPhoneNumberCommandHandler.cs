﻿namespace BNine.Application.Aggregates.Users.Commands.Update.VerifyPhoneNumber
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading;
    using System.Threading.Tasks;
    using Abstractions;
    using AutoMapper;
    using BNine.Application.Aggregates.Configuration.Queries.GetConfiguration;
    using BNine.Application.Aggregates.ServiceBusEvents.Commands.PublishB9Event;
    using BNine.Constants;
    using Domain.Entities.User;
    using Domain.Entities.User.UpdateInfoProcessing;
    using Emails.Commands.User.PhoneUpdated;
    using Enums;
    using Helpers;
    using Interfaces;
    using Interfaces.Bank.Administration;
    using MediatR;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.Extensions.Logging;
    using Newtonsoft.Json;

    public class VerifyPhoneNumberCommandHandler
        : AbstractRequestHandler, IRequestHandler<VerifyPhoneNumberCommand, bool>
    {
        private const string DevelopmentOTPCode = "555555";

        private readonly IPartnerProviderService _partnerProvider;
        private readonly ILogger<VerifyPhoneNumberCommandHandler> _logger;

        private ISmsService SmsService
        {
            get;
        }
        private INotificationUsersService NotificationUsersService
        {
            get;
        }
        private IBankClientsService BankClientsService
        {
            get;
        }

        public VerifyPhoneNumberCommandHandler(
            ISmsService smsService,
            INotificationUsersService notificationUsersService,
            IMediator mediator,
            IBNineDbContext dbContext,
            IMapper mapper,
            ICurrentUserService currentUser,
            IBankClientsService bankClientsService,
            IPartnerProviderService partnerProvider,
            ILogger<VerifyPhoneNumberCommandHandler> logger
            )
            : base(mediator, dbContext, mapper, currentUser)
        {
            _partnerProvider = partnerProvider;
            _logger = logger;
            SmsService = smsService;
            NotificationUsersService = notificationUsersService;
            BankClientsService = bankClientsService;
        }

        public async Task<bool> Handle(VerifyPhoneNumberCommand request, CancellationToken cancellationToken)
        {
            var changePhoneRequest = await GetChangePhoneRequest(request, cancellationToken);
            if (changePhoneRequest == null)
            {
                return false;
            }

            var user = DbContext.Users
                .Include(x => x.Devices)
                .FirstOrDefault(x => x.Id == CurrentUser.UserId.Value);

            changePhoneRequest.Status = RequestProcessingStatus.COMPLETED;

            var previousPhoneNumber = user.Phone;
            user.Phone = changePhoneRequest.UpdatedFieldValue;

            if (user.ExternalClientId != null)
            {
                var response = await BankClientsService.UpdateClientPhoneNumber(
                    user.ExternalClientId.Value,
                    changePhoneRequest.UpdatedFieldValue);
            }

            await DbContext.SaveChangesAsync(cancellationToken);

            await SendSmsToBothNumbers(previousPhoneNumber, user);

            return true;
        }

        private async Task<UpdateUserInfoRequest> GetChangePhoneRequest(VerifyPhoneNumberCommand request, CancellationToken cancellationToken)
        {
            var changePhoneRequestQuery = DbContext.UpdateUserInfoRequests
                .Where(x => x.UserId == CurrentUser.UserId.Value)
                .Where(x => x.Status == RequestProcessingStatus.OPENED)
                .Where(x => x.UpdatedFieldType == UserInfoField.PHONE)
                .Where(x => x.RequestDate > DateTime.UtcNow.AddMinutes(-5));

            var changePhoneRequest = await changePhoneRequestQuery
                .Where(x => x.ConfirmationCode.Equals(request.VerificationCode))
                .FirstOrDefaultAsync(cancellationToken);

            if (changePhoneRequest == null && request.VerificationCode == DevelopmentOTPCode)
            {
                var config = await Mediator.Send(new GetConfigurationQuery());
                if (config.HasFeatureEnabled(FeaturesNames.Features.SmsVerificationDevelopmentMode))
                {
                    changePhoneRequest = await changePhoneRequestQuery.FirstOrDefaultAsync(cancellationToken);
                }
            }

            return changePhoneRequest;
        }

        private async Task SendSmsToBothNumbers(string previousPhoneNumber, User user)
        {
            var partnerName = StringProvider.GetPartnerName(_partnerProvider);
            var message = $"Your phone associated with {partnerName} account was changed. " +
                          $"If you did not do this, call {StringProvider.GetSupportPhone(_partnerProvider)} to lock your account. " +
                          $"The {partnerName} Team";
            try
            {
                await SmsService.SendMessageAsync(previousPhoneNumber, message);
            }
            catch (Exception ex)
            {
                _logger.LogError($"Error when sending sms: {ex.Message}", ex);
            }

            try
            {
                await SmsService.SendMessageAsync(user.Phone, message);
            }
            catch (Exception ex)
            {
                _logger.LogError($"Error when sending sms: {ex.Message}", ex);
            }

            try
            {
                await NotificationUsersService.SendNotification(user.Id, message, user.Settings.IsNotificationsEnabled, NotificationTypeEnum.Information, true, user.Devices, null, null, PushNotificationCategory.YourPhoneAssociatedWithB9AccountWasChanged);
            }
            catch (Exception ex)
            {
                _logger.LogError($"Error when sending push notification: {ex.Message}", ex);

            }

            await Mediator.Send(new PublishB9EventCommand(
                      user,
                      ServiceBusTopics.B9User,
                      ServiceBusEvents.B9Updated,
                      new Dictionary<string, object>
                      {
                          { ServiceBusDefaults.UpdatedFieldsAdditionalProperty , JsonConvert.SerializeObject(new List<string> { nameof(user.Phone) }) }
                      }));

            await Mediator.Send(new PhoneUpdatedEmailCommand(user.Email, user.FirstName));
        }
    }
}
