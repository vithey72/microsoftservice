﻿namespace BNine.Application.Aggregates.Users.Commands.Update.VerifyPhoneNumber
{
    using MediatR;
    using Newtonsoft.Json;

    public class VerifyPhoneNumberCommand : IRequest<bool>
    {
        [JsonProperty("verificationCode")]
        public string VerificationCode
        {
            get; set;
        }
    }
}
