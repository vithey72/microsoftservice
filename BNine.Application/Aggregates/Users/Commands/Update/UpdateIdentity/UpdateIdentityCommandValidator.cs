﻿namespace BNine.Application.Aggregates.Users.Commands.Update.UpdateIdentity
{
    using System.Linq;
    using System.Text.RegularExpressions;
    using BNine.Application.Models.Validators;
    using FluentValidation;
    using Helpers;
    using Interfaces;
    using Microsoft.Extensions.Localization;
    using ResourceLibrary;

    public class UpdateIdentityCommandValidator
        : AbstractOnboardingStepValidator<UpdateIdentityCommand>
    {
        private readonly IStringLocalizer<SharedResource> _sharedLocalizer;
        private readonly IPartnerProviderService _partnerProvider;

        public UpdateIdentityCommandValidator(
            ICurrentUserService currentUserService,
            IBNineDbContext dbContext,
            IStringLocalizer<SharedResource> sharedLocalizer,
            IPartnerProviderService partnerProvider
            ) : base(dbContext, currentUserService)
        {
            _sharedLocalizer = sharedLocalizer;
            _partnerProvider = partnerProvider;
            Step = Enums.UserStatus.PendingSSN;

            RuleFor(x => x)
                .Must(SSNorITIN)
                .WithMessage(GetResourceString("ssnOrITINRequiredValidationErrorMessage"));

            RuleFor(x => x.SSNNumber)
                .Must(IsValidSSN)
                .When(x => !string.IsNullOrEmpty(x.SSNNumber))
                .WithMessage(GetResourceString("IncorrectEnteredDataValidationErrorMessage"));

            RuleFor(x => x)
                .Must(IsIdentityUnique)
                .When(x => !string.IsNullOrEmpty(x.SSNNumber))
                .WithMessage(GetResourceString("ssnAlreadyRegisteredValiadtionErrorMessage"));

            RuleFor(x => x)
                .Must(IsIdentityUnique)
                .When(x => !string.IsNullOrEmpty(x.ITINNumber))
                .WithMessage(GetResourceString("itinAlreadyRegisteredValiadtionErrorMessage"));

            RuleFor(x => x.ITINNumber)
               .Must(IsValidITIN)
               .When(x => !string.IsNullOrEmpty(x.ITINNumber))
               .WithMessage(GetResourceString("IncorrectEnteredDataValidationErrorMessage"));
        }

        private string GetResourceString(string key)
        {
            return StringProvider.ReplaceCustomizedStrings(_sharedLocalizer[key].Value, _partnerProvider);
        }

        private bool SSNorITIN(UpdateIdentityCommand command)
        {
            return !string.IsNullOrEmpty(command.ITINNumber) || !string.IsNullOrEmpty(command.SSNNumber);
        }

        private bool IsValidSSN(string ssn)
        {
            return Regex.IsMatch(ssn, @"^(?!219099999|078051120)(?!666|000|9\d{2})\d{3}(?!00)\d{2}(?!0{4})\d{4}$")
                   && Regex.IsMatch(ssn, @"^\d{3}(?!00)\d{6}");
        }

        private bool IsValidITIN(string itin)
        {
            return Regex.IsMatch(itin, @"^(9\d{2})([5-5][0-9]|[6-6][0-5]|[8-8][3-8]|[9-9][0-2]|[9-9][4-9])\d{4}$");
        }


        private bool IsIdentityUnique(UpdateIdentityCommand command)
        {
            return !DbContext.Users
                .Where(x => x.Id != CurrentUserService.UserId)
                .Where(x => command.Value.Equals(x.Identity.Value) && command.Type == x.Identity.Type)
                .Any();
        }
    }
}
