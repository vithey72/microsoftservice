﻿namespace BNine.Application.Aggregates.Users.Commands.Update.UpdateIdentity
{
    using System.Collections.Generic;
    using System.Threading;
    using System.Threading.Tasks;
    using Abstractions;
    using AutoMapper;
    using BNine.Application.Aggregates.ServiceBusEvents.Commands.PublishB9Event;
    using BNine.Constants;
    using BNine.Domain.Events.Users;
    using Domain.Entities.User;
    using Enums;
    using Exceptions;
    using Interfaces;
    using MediatR;
    using Microsoft.EntityFrameworkCore;
    using Newtonsoft.Json;

    public class UpdateIdentityCommandHandler : AbstractRequestHandler, IRequestHandler<UpdateIdentityCommand>
    {
        public UpdateIdentityCommandHandler(
            IMediator mediator,
            IBNineDbContext dbContext,
            IMapper mapper,
            ICurrentUserService currentUser) : base(mediator, dbContext, mapper, currentUser)
        {
        }

        public async Task<Unit> Handle(UpdateIdentityCommand request, CancellationToken cancellationToken)
        {
            var user = await DbContext.Users
                .Include(x => x.Identity)
                .FirstOrDefaultAsync(x => x.Id == CurrentUser.UserId, cancellationToken);

            if (user == null)
            {
                throw new NotFoundException(nameof(User), CurrentUser.UserId);
            }

            user.Identity = Mapper.Map(request, user.Identity);

            var statusChanged = false;

            if (user.Status == UserStatus.PendingSSN)
            {
                user.Status = UserStatus.PendingDocumentVerification;
                statusChanged = true;
            }
            user.DomainEvents.Add(new OnboardingStepSuccessEvent(user, UserStatus.PendingSSN, UserStatusDetailed.PendingSSN));
            await DbContext.SaveChangesAsync(cancellationToken);

            await Mediator.Send(new PublishB9EventCommand(
                      user,
                      ServiceBusTopics.B9User,
                      ServiceBusEvents.B9UserIdentityCreated));

            if (statusChanged)
            {
                await Mediator.Send(new PublishB9EventCommand(
                 user,
                 ServiceBusTopics.B9User,
                 ServiceBusEvents.B9Updated,
                 new Dictionary<string, object>
                 {
                    { ServiceBusDefaults.UpdatedFieldsAdditionalProperty , JsonConvert.SerializeObject(new List<string> { nameof(user.Status) }) }
                 }));
            }

            return Unit.Value;
        }
    }
}
