﻿namespace BNine.Application.Aggregates.Users.Commands.Update.UpdateIdentity
{
    using System;
    using AutoMapper;
    using BNine.Application.Mappings;
    using BNine.Domain.Entities.User;
    using BNine.Enums;
    using MediatR;
    using Newtonsoft.Json;

    public class UpdateIdentityCommand : IRequest<Unit>, IMapTo<Identity>
    {
        [JsonIgnore]
        public Guid UserId
        {
            get; set;
        }

        [JsonProperty("ssnNumber")]
        public string SSNNumber
        {
            get; set;
        }

        [JsonProperty("itinNumber")]
        public string ITINNumber
        {
            get; set;
        }

        [JsonIgnore]
        public IdentityType Type
        {
            get
            {
                return string.IsNullOrEmpty(SSNNumber)
                    ? IdentityType.ITIN
                    : IdentityType.SSN;
            }
        }

        [JsonIgnore]
        public string Value
        {
            get
            {
                return Type == IdentityType.SSN
                    ? SSNNumber
                    : ITINNumber;
            }
        }

        public void Mapping(Profile profile)
        {
            profile.CreateMap<UpdateIdentityCommand, Identity>()
                .ForMember(dst => dst.Type, opt => opt.MapFrom(src => src.Type))
                .ForMember(dst => dst.Value, opt => opt.MapFrom(src => src.Value))
                .ForMember(dst => dst.UserId, opt => opt.MapFrom(src => src.UserId));
        }
    }
}
