﻿namespace BNine.Application.Aggregates.Users.Commands.Update.UpdateChangePhoneConfirmationCode
{
    using System;
    using System.Linq;
    using System.Threading;
    using System.Threading.Tasks;
    using AutoMapper;
    using BNine.Application.Abstractions;
    using BNine.Application.Exceptions;
    using BNine.Application.Interfaces;
    using BNine.Common;
    using BNine.Domain.Entities.User.UpdateInfoProcessing;
    using Helpers;
    using MediatR;
    using Microsoft.Extensions.Logging;

    public class UpdateChangePhoneConfirmationCodeCommandHandler
        : AbstractRequestHandler
        , IRequestHandler<UpdateChangePhoneConfirmationCodeCommand, Unit>
    {
        private readonly ISmsService _smsService;
        private readonly IPartnerProviderService _partnerProvider;
        private readonly ILogger<UpdateChangePhoneConfirmationCodeCommandHandler> _logger;

        public UpdateChangePhoneConfirmationCodeCommandHandler(
            IMediator mediator,
            IBNineDbContext dbContext,
            IMapper mapper,
            ICurrentUserService currentUser,
            ISmsService smsService,
            IPartnerProviderService partnerProvider,
            ILogger<UpdateChangePhoneConfirmationCodeCommandHandler> logger
            )
            : base(mediator, dbContext, mapper, currentUser)
        {
            _smsService = smsService;
            _partnerProvider = partnerProvider;
            _logger = logger;
        }

        public async Task<Unit> Handle(UpdateChangePhoneConfirmationCodeCommand request, CancellationToken cancellationToken)
        {
            var changePhoneRequest = DbContext.UpdateUserInfoRequests
                .FirstOrDefault(x => x.Id == request.ChangePhoneRequestId);

            if (changePhoneRequest == null
                || changePhoneRequest.UpdatedFieldType != Enums.UserInfoField.PHONE
                || changePhoneRequest.Status != Enums.RequestProcessingStatus.OPENED)
            {
                throw new NotFoundException(nameof(UpdateUserInfoRequest), request.ChangePhoneRequestId);
            }

            var newConfirmationCode = CodeGenerator.GenerateCode();

            changePhoneRequest.ConfirmationCode = newConfirmationCode;
            changePhoneRequest.RequestDate = DateTime.UtcNow;

            await DbContext.SaveChangesAsync(cancellationToken);

            try
            {
                await _smsService.SendMessageAsync(
                    changePhoneRequest.UpdatedFieldValue,
                    StringProvider.GetOTPCodeMessage(newConfirmationCode, _partnerProvider));
            }
            catch (Exception ex)
            {
                _logger.LogError($"Error when sending sms: {ex.Message}", ex);
            }

            return Unit.Value;
        }
    }
}
