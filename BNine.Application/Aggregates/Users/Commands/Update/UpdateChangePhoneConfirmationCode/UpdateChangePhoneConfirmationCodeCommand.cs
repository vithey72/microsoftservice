﻿namespace BNine.Application.Aggregates.Users.Commands.Update.UpdateChangePhoneConfirmationCode
{
    using System;
    using MediatR;
    using Newtonsoft.Json;

    public class UpdateChangePhoneConfirmationCodeCommand : IRequest<Unit>
    {
        [JsonProperty("changePhoneRequestId")]
        public Guid ChangePhoneRequestId
        {
            get; set;
        }
    }
}
