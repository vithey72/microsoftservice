﻿namespace BNine.Application.Aggregates.Users.Commands.Update.UpdatePhoneNumber
{
    using System;
    using MediatR;
    using Newtonsoft.Json;

    public class UpdatePhoneNumberCommand : IRequest<Guid>
    {
        [JsonProperty("newPhoneNumber")]
        public string NewPhoneNumber
        {
            get; set;
        }

        [JsonProperty("tinLastDigits")]
        public string TinLastDigits
        {
            get; set;
        }
    }
}
