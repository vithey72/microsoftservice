﻿namespace BNine.Application.Aggregates.Users.Commands.Update.UpdatePhoneNumber
{
    using System;
    using System.Linq;
    using System.Threading;
    using System.Threading.Tasks;
    using AutoMapper;
    using BNine.Application.Abstractions;
    using BNine.Application.Exceptions;
    using BNine.Application.Interfaces;
    using BNine.Common;
    using BNine.Domain.Entities.User;
    using BNine.Domain.Entities.User.UpdateInfoProcessing;
    using BNine.ResourceLibrary;
    using Helpers;
    using MediatR;
    using Microsoft.Extensions.Localization;
    using Microsoft.Extensions.Logging;

    public class UpdatePhoneNumberCommandHandler
        : AbstractRequestHandler
        , IRequestHandler<UpdatePhoneNumberCommand, Guid>
    {
        private readonly IStringLocalizer<SharedResource> _sharedLocalizer;
        private readonly ISmsService _smsService;
        private readonly IPartnerProviderService _partnerProvider;
        private readonly ILogger<UpdatePhoneNumberCommandHandler> _logger;

        public UpdatePhoneNumberCommandHandler(
            IMediator mediator,
            IBNineDbContext dbContext,
            IMapper mapper,
            ICurrentUserService currentUser,
            IStringLocalizer<SharedResource> sharedLocalizer,
            ISmsService smsService,
            IPartnerProviderService partnerProvider,
            ILogger<UpdatePhoneNumberCommandHandler> logger
            )
            : base(mediator, dbContext, mapper, currentUser)
        {
            _sharedLocalizer = sharedLocalizer;
            _smsService = smsService;
            _partnerProvider = partnerProvider;
            _logger = logger;
        }

        public async Task<Guid> Handle(UpdatePhoneNumberCommand request, CancellationToken cancellationToken)
        {
            var user = DbContext.Users.FirstOrDefault(x => x.Id == CurrentUser.UserId.Value);

            if (!user.Identity.Value.Substring(user.Identity.Value.Length - 4, 4).Equals(request.TinLastDigits))
            {
                throw new ValidationException(nameof(Identity), _sharedLocalizer["invalidTINInfoValidationErrorMessage"].Value);
            }

            var confirmationCode = CodeGenerator.GenerateCode();

            var changePhoneRequest = new UpdateUserInfoRequest()
            {
                UserId = CurrentUser.UserId.Value,
                UpdatedFieldType = Enums.UserInfoField.PHONE,
                UpdatedFieldValue = request.NewPhoneNumber,
                RequestDate = DateTime.UtcNow,
                ConfirmationCode = confirmationCode,
                Status = Enums.RequestProcessingStatus.OPENED
            };

            DbContext.UpdateUserInfoRequests.Add(changePhoneRequest);
            await DbContext.SaveChangesAsync(cancellationToken);

            try
            {
                await _smsService.SendMessageAsync(
                    request.NewPhoneNumber,
                    StringProvider.GetOTPCodeMessage(confirmationCode, _partnerProvider));
            }
            catch (Exception ex)
            {
                _logger.LogError($"Error when sending sms: {ex.Message}", ex);
            }

            return changePhoneRequest.Id;
        }
    }
}
