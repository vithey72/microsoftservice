﻿namespace BNine.Application.Aggregates.Users.Commands.Update.UpdatePhoneNumber
{
    using System.Linq;
    using BNine.Application.Interfaces;
    using BNine.Common;
    using BNine.ResourceLibrary;
    using FluentValidation;
    using Microsoft.Extensions.Localization;

    public class UpdatePhoneNumberCommandValidator
        : AbstractValidator<UpdatePhoneNumberCommand>
    {
        private IBNineDbContext DbContext
        {
            get;
        }

        public UpdatePhoneNumberCommandValidator(
            IBNineDbContext dbContext,
            IStringLocalizer<SharedResource> sharedLocalizer)
        {
            DbContext = dbContext;

            RuleFor(x => x.NewPhoneNumber)
                .NotEmpty()
                .Must(PhoneHelper.IsValidPhone)
                .WithMessage(sharedLocalizer["IncorrectEnteredDataValidationErrorMessage"].Value);

            RuleFor(x => x.NewPhoneNumber)
                .NotEmpty()
                .Must(IsUnique)
                .WithMessage(sharedLocalizer["accountExistsValidationErrorMessage"].Value);
        }

        private bool IsUnique(string phone)
        {
            return !DbContext.Users.Any(x => x.Phone.Equals(phone));
        }
    }
}
