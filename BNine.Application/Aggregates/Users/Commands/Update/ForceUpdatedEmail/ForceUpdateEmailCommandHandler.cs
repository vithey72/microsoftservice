﻿namespace BNine.Application.Aggregates.Users.Commands.Update.ForceUpdatedEmail
{
    using System.Linq;
    using System.Threading;
    using System.Threading.Tasks;
    using Abstractions;
    using AutoMapper;
    using BNine.Application.Aggregates.LogAdminAction.Commands;
    using BNine.Application.Interfaces.Bank.Administration;
    using BNine.Enums;
    using Interfaces;
    using MediatR;
    using Newtonsoft.Json;

    public class ForceUpdateEmailCommandHandler
        : AbstractRequestHandler, IRequestHandler<ForceUpdateEmailCommand, Unit>
    {
        public ForceUpdateEmailCommandHandler(
            IMediator mediator,
            IBNineDbContext dbContext,
            IMapper mapper,
            IBankUsersService bankUsersService,
            IBankClientsService bankClientsService,
            ICurrentUserService currentUser)
            : base(mediator, dbContext, mapper, currentUser)
        {
            BankUsersService = bankUsersService;
            BankClientsService = bankClientsService;
        }

        public IBankUsersService BankUsersService
        {
            get;
        }

        public IBankClientsService BankClientsService
        {
            get;
        }

        public async Task<Unit> Handle(ForceUpdateEmailCommand request, CancellationToken cancellationToken)
        {
            var user = DbContext.Users
                .FirstOrDefault(x => x.Id == request.UserId);

            var userOldValue = JsonConvert.SerializeObject(user, Formatting.None,
                new JsonSerializerSettings() { ReferenceLoopHandling = ReferenceLoopHandling.Ignore });

            user.Email = request.NewEmail;

            await BankUsersService.UpdateUserEmail(
                user.ExternalId,
                request.NewEmail);

            if (user.ExternalClientId != null)
            {
                await BankClientsService.UpdateClientEmail(user.ExternalClientId.Value,
                    request.NewEmail);
            }

            await DbContext.SaveChangesAsync(cancellationToken);

            var userNewValue = JsonConvert.SerializeObject(user, Formatting.None,
                new JsonSerializerSettings() { ReferenceLoopHandling = ReferenceLoopHandling.Ignore });

            await SendLogAdminActionCommand(userOldValue, userNewValue, user.Id.ToString());

            return Unit.Value;
        }

        private async Task SendLogAdminActionCommand(string userOldValue, string userNewValue, string clientId)
        {
            await Mediator.Send(new LogAdminActionCommand()
            {
                EntityName = nameof(Domain.Entities.User.User),
                Type = AdminActionType.ChangeEmail,
                EntityId = clientId,
                BeforeChanges = userOldValue,
                AfterChanges = userNewValue
            });
        }
    }
}
