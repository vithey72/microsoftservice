﻿namespace BNine.Application.Aggregates.Users.Commands.Update.ForceUpdatedEmail
{
    using System;
    using MediatR;

    public class ForceUpdateEmailCommand : IRequest<Unit>
    {
        public Guid UserId
        {
            get; set;
        }

        public string NewEmail
        {
            get; set;
        }
    }
}
