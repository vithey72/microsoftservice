﻿namespace BNine.Application.Aggregates.Users.Commands.Update.UpdateAgreements
{
    using Behaviours;
    using MediatR;
    using Newtonsoft.Json;

    [Sequential]
    public class UpdateAgreementsCommand : IRequest<Unit>
    {
        [JsonIgnore]
        public string IpAddress
        {
            get; set;
        }
    }
}
