﻿namespace BNine.Application.Aggregates.Users.Commands.Update.UpdateAgreements
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Reflection;
    using System.Threading;
    using System.Threading.Tasks;
    using Abstractions;
    //using Application.Models.Dto;
    using AutoMapper;
    using BNine.Application.Aggregates.ServiceBusEvents.Commands.PublishB9Event;
    using BNine.Application.Aggregates.Users.Models;
    using BNine.Application.Interfaces.Alloy;
    using BNine.Application.Interfaces.Bank.Administration;
    using BNine.Application.Interfaces.Bank.Client;
    using BNine.Application.Models.MBanq;
    using BNine.Common;
    using BNine.Constants;
    using BNine.Domain.Entities.User;
    using BNine.Domain.Events.Users;
    using BNine.MBanq.Constants;
    using Domain.Entities.User.UserDetails;
    using Enums;
    using Exceptions;
    using Helpers;
    using Interfaces;
    using Interfaces.Socure;
    using MediatR;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.Extensions.Logging;
    using Newtonsoft.Json;
    using DocumentType = Enums.DocumentType;
    using IBankAdministrationIdentityService = Interfaces.Bank.Administration.IBankIdentityService;
    using IBankClientIdentityService = Interfaces.Bank.Client.IBankIdentityService;


    public class UpdateAgreementsCommandHandler : AbstractRequestHandler, IRequestHandler<UpdateAgreementsCommand>
    {
        private readonly ISocureKycService _socureKycService;
        private readonly IClientDocumentsOriginalsPersistenceService _clientDocumentsOriginalsPersistenceService;
        private readonly IPartnerProviderService _partnerProvider;
        private readonly ITariffPlanService _tariffPlansService;
        private readonly IAlloyDocumentService _alloyDocumentService;
        private readonly IBankUsersService _bankUsersService;
        private readonly IBankKYCService _bankKycService;
        private readonly IBankDocumentsService _bankDocumentsService;
        private readonly IBankClientIdentityService _bankClientIdentityService;
        private readonly IBankAdministrationIdentityService _bankAdministrationIdentityService;
        private readonly IBankClientAddressService _bankClientAddressService;
        private readonly IBankClientsService _bankClientsService;
        private readonly IBankTemplatesService _bankTemplatesService;
        private readonly IBankCurrentAccountsService _bankCurrentAccountsService;
        private readonly ILogger<UpdateAgreementsCommandHandler> _logger;
        private readonly ITariffPlanService _tariffPlanService;

        public UpdateAgreementsCommandHandler(
            IMediator mediator,
            IBNineDbContext dbContext,
            IMapper mapper,
            ICurrentUserService currentUser,
            IAlloyDocumentService alloyDocumentService,
            ISocureKycService socureKycService,
            IBankUsersService bankUsersService,
            IBankDocumentsService bankDocumentsService,
            IBankClientIdentityService bankClientIdentityService,
            IBankAdministrationIdentityService bankAdministrationIdentityService,
            IBankClientAddressService bankClientAddressService,
            IBankClientsService bankClientsService,
            IBankTemplatesService bankTemplatesService,
            IBankKYCService bankKycService,
            IBankCurrentAccountsService bankCurrentAccountsService,
            ILogger<UpdateAgreementsCommandHandler> logger,
            ITariffPlanService tariffPlanService,
            IClientDocumentsOriginalsPersistenceService clientDocumentsOriginalsPersistenceService,
            IPartnerProviderService partnerProvider
        ) : base(mediator, dbContext, mapper, currentUser)
        {
            _socureKycService = socureKycService;
            _alloyDocumentService = alloyDocumentService;
            _bankUsersService = bankUsersService;
            _bankDocumentsService = bankDocumentsService;
            _bankClientIdentityService = bankClientIdentityService;
            _bankAdministrationIdentityService = bankAdministrationIdentityService;
            _bankClientAddressService = bankClientAddressService;
            _bankClientsService = bankClientsService;
            _bankTemplatesService = bankTemplatesService;
            _bankKycService = bankKycService;
            _bankCurrentAccountsService = bankCurrentAccountsService;
            _logger = logger;
            _tariffPlanService = tariffPlanService;
            _clientDocumentsOriginalsPersistenceService = clientDocumentsOriginalsPersistenceService;
            _partnerProvider = partnerProvider;
        }

        public async Task<Unit> Handle(UpdateAgreementsCommand request, CancellationToken cancellationToken)
        {
            var user = DbContext.Users
                .Include(x => x.CurrentAccount)
                .Include(x => x.UserTariffPlans)
                .FirstOrDefault(x => x.Id == CurrentUser.UserId);

            if (user == null)
            {
                throw new NotFoundException(nameof(User), CurrentUser.UserId);
            }

            var kycResult = await DbContext.KYCVerificationResults
                .Where(x => x.UserId == user.Id)
                .OrderByDescending(x => x.CreatedAt)
                .Select(x => new { x.EntityToken })
                .FirstOrDefaultAsync(cancellationToken);

            if (kycResult == null)
            {
                user.DomainEvents.Add(new OnboardingStepFailedEvent(user, UserStatus.PendingAgreement, UserStatusDetailed.PendingAgreement,
                    new List<string> { $"KYC result not found, user id: {CurrentUser.UserId}" }));
                await DbContext.SaveChangesAsync(cancellationToken);
                throw new NotFoundException(nameof(KYCVerificationResult), user.Id);
            }

            user.ToSAcceptance = new ToSAcceptance
            {
                Ip = request.IpAddress,
                Time = DateTime.UtcNow
            };
            await DbContext.SaveChangesAsync(cancellationToken);

            if (user.Status == UserStatus.Declined)
            {
                return Unit.Value;
            }

            try
            {
                await _bankUsersService.UpdateUser(user.ExternalId, user.Id.ToString(), user.FirstName, user.LastName, user.Email);

                await AddClientToMbanq(kycResult.EntityToken, user, cancellationToken);

                if (user.Status == UserStatus.PendingAgreement)
                {
                    try
                    {
                        await _bankKycService.StartKYC(user.ExternalClientId!.Value);
                    }
                    catch (Exception ex)
                    {
                        _logger.LogError(ex.Message);
                    }

                    var properStatus = await _bankClientsService.GetVerificationStatus(user.ExternalClientId.Value);

                    if (Enum.TryParse<VerificationStatus>(properStatus, out var status) && status == VerificationStatus.APPROVED)
                    {
                        await _bankClientsService.ActivateClient(user.ExternalClientId.Value);

                        await _bankClientsService.LinkClientToUser(user.ExternalClientId.Value, user.ExternalId);

                        if (_partnerProvider.GetPartner() == PartnerApp.Qorbis)
                        {
                            user.Status = UserStatus.PreActivation;
                        }
                        else
                        {
                            user.Status = UserStatus.Active;
                            user.ActivatedAt = DateTime.UtcNow;
                        }
                    }
                    else
                    {
                        user.DomainEvents.Add(new OnboardingStepFailedEvent(
                            user,
                            UserStatus.PendingAgreement, UserStatusDetailed.PendingAgreement,
                            new List<string> { $"User's verification status value is '{properStatus}'. Expected '{VerificationStatus.APPROVED}'" }));
                        await DbContext.SaveChangesAsync(cancellationToken);
                        return Unit.Value;
                    }
                }
            }
            catch (Exception ex)
            {
                user.DomainEvents.Add(new OnboardingStepFailedEvent(user, UserStatus.PendingAgreement, UserStatusDetailed.PendingAgreement, new List<string> { ex.Message }));
                await DbContext.SaveChangesAsync(cancellationToken);
                throw;
            }

            try
            {
                if (user.Status is UserStatus.Active or UserStatus.PreActivation)
                {
                    await _bankCurrentAccountsService.AddCurrentBankAccountsIfNeeded(user.Id, cancellationToken);
                    var activeTariff = await _tariffPlanService.GetCurrentTariff(user.Id, cancellationToken);
                    if (activeTariff == null)
                    {
                        await _tariffPlanService.ActivateStarterTariffPlan(user.Id, cancellationToken);
                    }
                }
            }
            catch (Exception ex)
            {
                user.DomainEvents.Add(new OnboardingStepFailedEvent(user, UserStatus.PendingAgreement, UserStatusDetailed.PendingAgreement, new List<string> { ex.Message }));

                await DbContext.SaveChangesAsync(cancellationToken);

                throw;
            }

            user.DomainEvents.Add(new OnboardingStepSuccessEvent(user, UserStatus.PendingAgreement, UserStatusDetailed.PendingAgreement));

            await DbContext.SaveChangesAsync(cancellationToken);

            await PublishIntegrationEvents(user);

            return Unit.Value;
        }

        private async Task PublishIntegrationEvents(User user)
        {
            await Mediator.Send(new PublishB9EventCommand(
                 user,
                 ServiceBusTopics.B9User,
                 ServiceBusEvents.B9UserAddressUpdated));

            await Mediator.Send(new PublishB9EventCommand(
                 user,
                 ServiceBusTopics.B9User,
                 ServiceBusEvents.B9UserIdentityUpdated));

            await Mediator.Send(new PublishB9EventCommand(
                user,
                ServiceBusTopics.B9User,
                ServiceBusEvents.B9UserDocumentsUpdated));

            await Mediator.Send(new PublishB9EventCommand(
                 user,
                 ServiceBusTopics.B9User,
                 ServiceBusEvents.B9Updated,
                 new Dictionary<string, object>
                 {
                    { ServiceBusDefaults.UpdatedFieldsAdditionalProperty , JsonConvert.SerializeObject(new List<string> { nameof(user.Status) }) }
                 }));
        }

        private async Task AddClientToMbanq(string evaluationEntityToken, User user, CancellationToken token)
        {
            if (!user.ExternalClientId.HasValue)
            {
                user.ExternalClientId = await _bankClientsService.CreateClient(user.FirstName,
                    user.LastName,
                    user.Phone,
                    user.Email,
                    user.DateOfBirth!.Value,
                    idempotencyKey: "create-client-" + user.Id + DateTime.UtcNow.ToString("HH-mm-ss"));
                await DbContext.SaveChangesAsync(token);
            }
            else
            {
                await _bankClientsService.UpdateClient(
                    user.ExternalClientId.Value,
                    user.FirstName,
                    user.LastName,
                    user.Phone,
                    user.Email,
                    user.DateOfBirth!.Value
                );
            }

            await SetClientAddress(user);
            var mbanqIdentifierTypes = await _bankClientIdentityService.GetClientIdentifierDocumentTypes();
            await SetClientIdentity(user, mbanqIdentifierTypes);
            await ProofIdentity(user, mbanqIdentifierTypes);
            if (user.Document.OcrProvider == EvaluationProviderEnum.Alloy)
            {
                await SetMbanqDocumentsFromAlloy(evaluationEntityToken, user, token);
            }
            else if (user.Document.OcrProvider == EvaluationProviderEnum.Socure)
            {
                await SetMbanqProofOfIdentityFromSocure(user.Document, user, token);
            }
        }

        private async Task SetClientAddress(User user)
        {
            var addressParametersOptions = await _bankTemplatesService.GetAddressParametersOptions();
            var userStateInfo = addressParametersOptions.StateIdOptions.FirstOrDefault(x => x.Name.Equals(StatesHelper.MapToFullStateName(user.Address.State)));

            if (userStateInfo == null)
            {
                throw new NotFoundException(nameof(CountryIdOption), user.Address.State);
            }

            var userAddressType = addressParametersOptions.AddressTypeIdOptions.FirstOrDefault(x => x.Name.Equals(ClientAddressTypes.Primary));
            if (userAddressType == null)
            {
                throw new NotFoundException(nameof(AddressTypeIdOption), ClientAddressTypes.Primary);
            }

            if (!user.Address.ExternalId.HasValue)
            {
                user.Address.ExternalId = await _bankClientAddressService.SetAddress(
                    user.ExternalClientId.Value,
                    user.Address.City,
                    userStateInfo.Id,
                    user.Address.AddressLine,
                    user.Address.ZipCode,
                    user.Address.Unit,
                    userStateInfo.ParentId,
                    userAddressType.Id);
            }
            else
            {
                await _bankClientAddressService.UpdateAddress(
                    user.Address.ExternalId.Value,
                    user.Address.City,
                    userStateInfo.Id,
                    user.Address.AddressLine,
                    user.Address.ZipCode,
                    user.Address.Unit,
                    userStateInfo.ParentId);
            }
        }

        private async Task SetClientIdentity(User user, List<ClientIdentifierType> mbanqIdentifierTypes)
        {
            var identificationTypeKey = user.Identity.Type == IdentityType.ITIN ? DocumentType.ITIN : DocumentType.SSN;
            var documentExternalName = DocumentTypes.GetExternalName(identificationTypeKey);

            var clientIdentifierType = mbanqIdentifierTypes.FirstOrDefault(x => x.Name.Equals(documentExternalName));
            if (clientIdentifierType == null)
            {
                throw new NotFoundException(nameof(ClientIdentifierType), documentExternalName);
            }

            if (!user.Identity.ExternalId.HasValue)
            {
                user.Identity.ExternalId = await _bankAdministrationIdentityService.CreateIdentity(
                    user.ExternalClientId.Value,
                    clientIdentifierType.ExternalId,
                    user.Identity.Value,
                    //TODO: issuingBy parameter
                    string.Empty);
            }
            else
            {
                await _bankAdministrationIdentityService.UpdateIdentity(
                    user.ExternalClientId.Value,
                    user.Identity.ExternalId.Value,
                    clientIdentifierType.ExternalId,
                    user.Identity.Value,
                    //TODO: issuingBy parameter
                    string.Empty);
            }
        }

        private async Task ProofIdentity(User user, List<ClientIdentifierType> mbanqIdentifierTypes)
        {
            var documentType = ClientDocumentsHelper.GetDocumentType(user.Document.TypeKey);
            var externalDocumentName = DocumentTypes.GetExternalName(documentType);

            var userMbanqDocumentType = mbanqIdentifierTypes.FirstOrDefault(x => x.Name.Equals(externalDocumentName));
            if (userMbanqDocumentType == null)
            {
                throw new NotFoundException(nameof(ClientIdentifierType));
            }

            if (!user.Document.ExternalId.HasValue)
            {
                user.Document.ExternalId = await _bankAdministrationIdentityService.CreateIdentity(
                    user.ExternalClientId.Value,
                    userMbanqDocumentType.ExternalId,
                    user.Document.Number,
                    user.Document.IssuingState + ", " + user.Document.IssuingCountry);
            }
            else
            {
                await _bankAdministrationIdentityService.UpdateIdentity(
                    user.ExternalClientId.Value,
                    user.Document.ExternalId.Value,
                    userMbanqDocumentType.ExternalId,
                    user.Document.Number,
                    user.Document.IssuingState + ", " + user.Document.IssuingCountry);
            };
        }

        private async Task SetMbanqDocumentsFromAlloy(string evaluationEntityToken, User user, CancellationToken cancellationToken)
        {
            await WipeExistingUserDocuments(user);

            IEnumerable<AlloyDocumentDetails> documents = new List<AlloyDocumentDetails>();

            try
            {
                documents = await _alloyDocumentService.GetDocuments(evaluationEntityToken);
            }
            catch (Exception ex)
            {
                _logger.LogError("Exception retrieving alloy documents, {exception}", ex);
            }

            if (documents.Count() == 0) // TODO: IMPORTANT - remove after Melissa answer
            {
                await AddDocumentWithDummyScan(user);
            }

            foreach (var document in documents.Where(x => x.Uploaded))
            {
                var contentType = ContentTypeHelper.GetContentType(document.Extension);

                var fileData = await _alloyDocumentService.GetDocument(evaluationEntityToken, document.DocumentToken);

                var documentExtension = document.Extension.StartsWith(".") ? document.Extension : $".{document.Extension}";

                var fileName = document.Name + documentExtension;

                try
                {
                    await _clientDocumentsOriginalsPersistenceService.UploadClientDocumentFromAlloy(user.Id, user.Document.TypeKey,
                        fileName, contentType, fileData, documentExtension, cancellationToken);

                    await _clientDocumentsOriginalsPersistenceService.MarkClientDocumentAsUploaded(user.Id,
                        cancellationToken);
                }
                catch (Exception ex)
                {
                    _logger.LogError($"An error has been occured during file saving into B9 storage, message: {ex.Message}");
                }

                await _bankDocumentsService.UploadDocument(
                    fileData,
                    fileName,
                    contentType,
                    user.Document.TypeKey,
                    user.Document.ExternalId!.Value);
            }
        }

        private async Task SetMbanqProofOfIdentityFromSocure(Document document, User user, CancellationToken token)
        {
            await WipeExistingUserDocuments(user);

            var socureDocuments = await _socureKycService.DownloadAllDocumentSides(document.DocumentScansExternalId!.Value, token);

            var extension = ".jpg";
            var contentType = ContentTypeHelper.GetContentType(extension);

            foreach (var socureDocument in socureDocuments)
            {
                try
                {
                    await _clientDocumentsOriginalsPersistenceService.UploadClientDocumentFromSocure(user.Id, document.TypeKey,
                        socureDocument.Key.FileName, contentType, socureDocument.Key.DocumentSide, socureDocument.Value, extension, token);

                    await _clientDocumentsOriginalsPersistenceService.MarkClientDocumentAsUploaded(user.Id,
                        token);
                }
                catch (Exception ex)
                {
                    _logger.LogError(
                        $"An error has been occured during file saving into B9 storage, message: {ex.Message}");
                }

                //Skip selfie and upload only identity documents with front/back side identifier
                if (socureDocument.Key.DocumentSide == DocumentSide.Default)
                {
                    continue;
                }

                var fileName =
                    $"{user.FirstName.ToUpper()}_{user.LastName.ToUpper()}_{document.TypeKey.ToLower()}_{socureDocument.Key.DocumentSide}{extension}";

                await _bankDocumentsService.UploadDocument(
                    socureDocument.Value,
                    fileName,
                    ContentTypeHelper.GetContentType(extension),
                    user.Document.TypeKey,
                    user.Document.ExternalId!.Value);
            }

            if (!socureDocuments.Keys.Any(x => x.DocumentSide == DocumentSide.FrontSide))
            {
                await AddDocumentWithDummyScan(user);
            }
        }

        private async Task WipeExistingUserDocuments(User user)
        {
            var externalDocuments = await _bankDocumentsService.GetDocuments(user.Document.ExternalId.Value);
            foreach (var document in externalDocuments)
            {
                await _bankDocumentsService.DeleteDocument(user.Document.ExternalId.Value, document.Id);
            }
        }

        private async Task AddDocumentWithDummyScan(User user)
        {
            var buildDir = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
            var filePath = buildDir + @"/Images/BNineLogo.JPG";

            var fileData = System.IO.File.ReadAllBytes(filePath);
            await _bankDocumentsService.UploadDocument(
                fileData,
                "file.jpg",
                ContentTypeHelper.GetContentType(".jpg"),
                user.Document.TypeKey,
                user.Document.ExternalId.Value);
        }
    }
}
