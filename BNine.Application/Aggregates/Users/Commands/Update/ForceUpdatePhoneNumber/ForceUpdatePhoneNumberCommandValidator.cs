﻿namespace BNine.Application.Aggregates.Users.Commands.Update.ForceUpdatePhoneNumber
{
    using BNine.Application.Interfaces;
    using BNine.Common;
    using BNine.ResourceLibrary;
    using FluentValidation;
    using Microsoft.Extensions.Localization;

    public class ForceUpdatePhoneNumberCommandValidator
        : AbstractValidator<ForceUpdatePhoneNumberCommand>
    {
        private IBNineDbContext DbContext
        {
            get;
        }

        public ForceUpdatePhoneNumberCommandValidator(
            IBNineDbContext dbContext,
            IStringLocalizer<SharedResource> sharedLocalizer)
        {
            DbContext = dbContext;

            RuleFor(x => x.PhoneNumber)
                .NotEmpty()
                .Must(PhoneHelper.IsValidPhone)
                .WithMessage(sharedLocalizer["IncorrectEnteredDataValidationErrorMessage"].Value);
        }
    }
}
