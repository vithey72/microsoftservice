﻿namespace BNine.Application.Aggregates.Users.Commands.Update.ForceUpdatePhoneNumber
{
    using System;
    using MediatR;

    public class ForceUpdatePhoneNumberCommand : IRequest<Unit>
    {
        public ForceUpdatePhoneNumberCommand(Guid userId, string phoneNumber)
        {
            UserId = userId;
            PhoneNumber = phoneNumber;
        }

        public string PhoneNumber
        {
            get; set;
        }

        public Guid UserId
        {
            get; set;
        }
    }
}
