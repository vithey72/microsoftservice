﻿namespace BNine.Application.Aggregates.Users.Commands.Update.ForceUpdatePhoneNumber
{
    using System.Linq;
    using System.Threading;
    using System.Threading.Tasks;
    using Abstractions;
    using AutoMapper;
    using BNine.Application.Aggregates.LogAdminAction.Commands;
    using BNine.Application.Exceptions;
    using Enums;
    using Interfaces;
    using Interfaces.Bank.Administration;
    using MediatR;
    using Microsoft.EntityFrameworkCore;
    using Newtonsoft.Json;

    public class ForceUpdatePhoneNumberCommandHandler
        : AbstractRequestHandler, IRequestHandler<ForceUpdatePhoneNumberCommand, Unit>
    {
        private IBankClientsService BankClientsService
        {
            get;
        }

        public ForceUpdatePhoneNumberCommandHandler(
            IMediator mediator,
            IBNineDbContext dbContext,
            IMapper mapper,
            ICurrentUserService currentUser,
            IBankClientsService bankClientsService)
            : base(mediator, dbContext, mapper, currentUser)
        {
            BankClientsService = bankClientsService;
        }

        public async Task<Unit> Handle(ForceUpdatePhoneNumberCommand request, CancellationToken cancellationToken)
        {
            var user = DbContext.Users
                .Include(x => x.Devices)
                .FirstOrDefault(x => x.Id == request.UserId);

            if (user.Phone == request.PhoneNumber)
            {
                return Unit.Value;
            }

            var existingRegistration = DbContext.Users
                .Include(x => x.Devices)
                .Include(x => x.KycVerificationResults)
                .FirstOrDefault(x => x.Phone == request.PhoneNumber);

            if (existingRegistration != null)
            {
                if (existingRegistration.Status < UserStatus.Active)
                {
                    DbContext.Devices.RemoveRange(existingRegistration.Devices);
                    DbContext.KYCVerificationResults.RemoveRange(existingRegistration.KycVerificationResults);
                    // all dependent entities will be cascadely deleted.
                    DbContext.Users.Remove(existingRegistration);
                }
                else
                {
                    throw new ValidationException("newPhone", $"User with the phone number already exists in {existingRegistration.Status} state.");
                }
            }

            if (user.ExternalClientId != null)
            {
                var response = await BankClientsService.UpdateClientPhoneNumber(
                    user.ExternalClientId.Value,
                    request.PhoneNumber);
            }

            var userOldValue = JsonConvert.SerializeObject(user, Formatting.None,
                new JsonSerializerSettings() { ReferenceLoopHandling = ReferenceLoopHandling.Ignore });

            user.Phone = request.PhoneNumber;

            await DbContext.SaveChangesAsync(cancellationToken);

            var userNewValue = JsonConvert.SerializeObject(user, Formatting.None,
                new JsonSerializerSettings() { ReferenceLoopHandling = ReferenceLoopHandling.Ignore });

            await SendLogAdminActionCommand(userOldValue, userNewValue, user.Id.ToString());

            return Unit.Value;
        }

        private async Task SendLogAdminActionCommand(string userOldValue, string userNewValue, string clientId)
        {
            await Mediator.Send(new LogAdminActionCommand()
            {
                EntityName = nameof(Domain.Entities.User.User),
                Type = AdminActionType.ChangePhoneNumber,
                EntityId = clientId,
                BeforeChanges = userOldValue,
                AfterChanges = userNewValue
            });
        }
    }
}
