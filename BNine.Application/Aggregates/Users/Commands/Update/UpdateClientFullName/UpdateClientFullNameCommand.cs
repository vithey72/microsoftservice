﻿namespace BNine.Application.Aggregates.Users.Commands.UpdateClientFullName;

using MediatR;

public class UpdateClientFullNameCommand: IRequest<Unit>
{
    public string LastName
    {
        get;
        set;
    }

    public string FirstName
    {
        get;
        set;
    }

    public Guid UserId
    {
        get; set;
    }
}
