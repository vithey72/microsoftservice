﻿namespace BNine.Application.Aggregates.Users.Commands.UpdateClientFullName;

using Abstractions;
using AutoMapper;
using Exceptions;
using Interfaces;
using MediatR;
using Microsoft.EntityFrameworkCore;
using BNine.Application.Interfaces.Bank.Administration;
using Domain.Entities.User;
using Enums;
using FluentValidation.Results;
using LogAdminAction.Commands;
using Newtonsoft.Json;

public class UpdateClientFullNameCommandHandler
    : AbstractRequestHandler, IRequestHandler<UpdateClientFullNameCommand, Unit>
{
    private readonly IBankClientsService _bankClientsService;

    public UpdateClientFullNameCommandHandler(
        IMediator mediator,
        IBNineDbContext dbContext,
        IMapper mapper,
        ICurrentUserService currentUser,
        IBankClientsService bankClientsService) : base(mediator, dbContext, mapper, currentUser)
    {
        _bankClientsService = bankClientsService;
    }

    public async Task<Unit> Handle(UpdateClientFullNameCommand request, CancellationToken cancellationToken)
    {
        var user = await DbContext.Users.FirstOrDefaultAsync(x => x.Id == request.UserId);

        if (user == null)
        {
            throw new NotFoundException($"User with id {request.UserId} not found in B9");
        }

        if (!user.ExternalClientId.HasValue)
        {
            throw new ValidationException(new[]
            {
                new ValidationFailure(nameof(User), $"User has no connection with MBanq, {nameof(user.ExternalClientId)} id is empty")
            });
        }

        var newFirstName = user.FirstName;
        if (!string.IsNullOrEmpty(request.FirstName) && request.FirstName != user.FirstName)
        {
            newFirstName = request.FirstName;
        }

        var newLastName = user.LastName;
        if (!string.IsNullOrEmpty(request.LastName) && request.LastName != user.LastName)
        {
            newLastName = request.LastName;
        }

        await _bankClientsService.UpdateClientFullName(user.ExternalClientId.Value, newFirstName, newLastName);

        var userOldValue = JsonConvert.SerializeObject(user, Formatting.None,
            new JsonSerializerSettings() { ReferenceLoopHandling = ReferenceLoopHandling.Ignore });

        user.FirstName = newFirstName;
        user.LastName = newLastName;

        await DbContext.SaveChangesAsync(cancellationToken);

        var userNewValue = JsonConvert.SerializeObject(user, Formatting.None,
            new JsonSerializerSettings() { ReferenceLoopHandling = ReferenceLoopHandling.Ignore });

        await SendLogAdminActionCommand(userOldValue, userNewValue, user.Id.ToString());

        return Unit.Value;
    }

    private async Task SendLogAdminActionCommand(string userOldValue, string userNewValue, string clientId)
    {
        await Mediator.Send(new LogAdminActionCommand()
        {
            EntityName = nameof(Domain.Entities.User.User),
            Type = AdminActionType.ChangeName,
            EntityId = clientId,
            BeforeChanges = userOldValue,
            AfterChanges = userNewValue
        });
    }
}
