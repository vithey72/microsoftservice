﻿namespace BNine.Application.Aggregates.Users.Commands.Update.UpdateAddress
{
    using System.Text.RegularExpressions;
    using FluentValidation;
    using UpdateClientFullName;

    public class UpdateClientFullNameCommandValidator
        : AbstractValidator<UpdateClientFullNameCommand>
    {
        private readonly string _multipleWordsRegex = @"^[A-Za-z'’]+(?:[-'’\s]+[A-Za-z'’]+)*$";

        private readonly string _validationMessage =
            "field should contain several words with only Latin letters, dashes, apostrophes, but without other spec symbols or numbers";

        public UpdateClientFullNameCommandValidator()
        {
            RuleFor(x => x.FirstName)
                .NotEmpty()
                .When(x => string.IsNullOrEmpty(x.LastName));

            RuleFor(x => x.LastName)
                .NotEmpty()
                .When(x => string.IsNullOrEmpty(x.FirstName));

            RuleFor(x => x.FirstName)
                .MaximumLength(512)
                .Must(x => Regex.IsMatch(x, _multipleWordsRegex))
                .When(x => !string.IsNullOrEmpty(x.FirstName))
                .WithMessage($"{nameof(UpdateClientFullNameCommand.FirstName)} "+_validationMessage);

            RuleFor(x => x.LastName)
                .MaximumLength(512)
                .Must(x => Regex.IsMatch(x, _multipleWordsRegex))
                .When(x => !string.IsNullOrEmpty(x.LastName))
                .WithMessage($"{nameof(UpdateClientFullNameCommand.LastName)} "+_validationMessage);
        }
    }
}
