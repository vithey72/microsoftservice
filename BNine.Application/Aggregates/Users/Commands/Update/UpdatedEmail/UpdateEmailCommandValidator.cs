﻿namespace BNine.Application.Aggregates.Users.Commands.Update.UpdatedEmail
{
    using System.Linq;
    using BNine.Application.Interfaces;
    using BNine.ResourceLibrary;
    using FluentValidation;
    using Microsoft.Extensions.Localization;

    public class UpdateEmailCommandValidator
        : AbstractValidator<UpdateEmailCommand>
    {
        private IBNineDbContext DbContext
        {
            get;
        }

        public UpdateEmailCommandValidator(
            IBNineDbContext dbContext,
            IStringLocalizer<SharedResource> sharedLocalizer)
        {
            DbContext = dbContext;

            RuleFor(x => x.NewEmail)
                .NotEmpty()
                .EmailAddress()
                .Must(IsUnique)
                .WithMessage(sharedLocalizer["emailRegisteredValidationErrorMessage"].Value);
        }

        private bool IsUnique(string email)
        {
            return !DbContext.Users.Any(x => x.Email.Equals(email));
        }
    }
}
