﻿namespace BNine.Application.Aggregates.Users.Commands.Update.UpdatedEmail
{
    using MediatR;
    using Newtonsoft.Json;

    public class UpdateEmailCommand : IRequest<Unit>
    {
        [JsonProperty("newEmail")]
        public string NewEmail
        {
            get; set;
        }
    }
}
