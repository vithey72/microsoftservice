﻿namespace BNine.Application.Aggregates.Users.Commands.Update.UpdatedEmail
{
    using System;
    using System.Threading;
    using System.Threading.Tasks;
    using Abstractions;
    using AutoMapper;
    using Common;
    using Domain.Entities.User.UpdateInfoProcessing;
    using Emails.Commands.User.ConfirmEmailUpdate;
    using Enums;
    using Interfaces;
    using MediatR;

    public class UpdateEmailCommandHandler
        : AbstractRequestHandler, IRequestHandler<UpdateEmailCommand, Unit>
    {
        public UpdateEmailCommandHandler(
            IMediator mediator,
            IBNineDbContext dbContext,
            IMapper mapper,
            ICurrentUserService currentUser)
            : base(mediator, dbContext, mapper, currentUser)
        {
        }

        public async Task<Unit> Handle(UpdateEmailCommand request, CancellationToken cancellationToken)
        {
            var confirmationCode = CodeGenerator.GenerateCode();

            var changeEmailRequest = new UpdateUserInfoRequest()
            {
                UserId = CurrentUser.UserId.Value,
                UpdatedFieldType = UserInfoField.EMAIL,
                UpdatedFieldValue = request.NewEmail,
                RequestDate = DateTime.UtcNow,
                ConfirmationCode = confirmationCode,
                Status = RequestProcessingStatus.OPENED
            };

            DbContext.UpdateUserInfoRequests.Add(changeEmailRequest);
            await DbContext.SaveChangesAsync(cancellationToken);

            await Mediator.Send(new ConfirmEmailUpdateEmailCommand(changeEmailRequest.UserId, request.NewEmail, changeEmailRequest.Id));

            return Unit.Value;
        }
    }
}
