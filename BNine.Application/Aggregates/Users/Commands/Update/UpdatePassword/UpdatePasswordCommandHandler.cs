﻿namespace BNine.Application.Aggregates.Users.Commands.Update.UpdatePassword
{
    using System.Linq;
    using System.Threading;
    using System.Threading.Tasks;
    using Abstractions;
    using AutoMapper;
    using BNine.Constants;
    using BNine.Enums;
    using Common.TableConnect.Common;
    using Domain.Entities.User;
    using Emails.Commands.User.PasswordUpdated;
    using Exceptions;
    using Helpers;
    using Interfaces;
    using Interfaces.Bank.Administration;
    using MediatR;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.Extensions.Localization;
    using Microsoft.Extensions.Logging;
    using ResourceLibrary;

    public class UpdatePasswordCommandHandler
        : AbstractRequestHandler
        , IRequestHandler<UpdatePasswordCommand, Unit>
    {
        private readonly ISmsService _smsService;
        private readonly INotificationUsersService _notificationUsersService;
        private readonly IBankUsersService _bankUsersService;
        private readonly IStringLocalizer<SharedResource> _sharedLocalizer;
        private readonly IPartnerProviderService _partnerProvider;
        private readonly ILogger<UpdatePasswordCommandHandler> _logger;

        public UpdatePasswordCommandHandler(
            ISmsService smsService,
            INotificationUsersService notificationUsersService,
            IMediator mediator,
            IBNineDbContext dbContext,
            IMapper mapper,
            ICurrentUserService currentUser,
            IBankUsersService bankUsersService,
            IStringLocalizer<SharedResource> sharedLocalizer,
            IPartnerProviderService partnerProvider,
            ILogger<UpdatePasswordCommandHandler> logger
            )
            : base(mediator, dbContext, mapper, currentUser)
        {
            _smsService = smsService;
            _notificationUsersService = notificationUsersService;
            _bankUsersService = bankUsersService;
            _sharedLocalizer = sharedLocalizer;
            _partnerProvider = partnerProvider;
            _logger = logger;
        }

        public async Task<Unit> Handle(UpdatePasswordCommand request, CancellationToken cancellationToken)
        {
            var user = DbContext.Users
                .Include(x => x.Devices)
                .FirstOrDefault(x => x.Id == CurrentUser.UserId);
            if (user == null)
            {
                throw new ValidationException(nameof(CurrentUser.UserId), _sharedLocalizer["noUserFoundValidationErrorMessage"].Value);
            }

            if (!CryptoHelper.VerifyHashedPassword(user.Password, request.OldPassword))
            {
                throw new ValidationException(nameof(request.OldPassword), _sharedLocalizer["incorrectCredentialsValidationErrorMessage"].Value);
            }

            var response = await _bankUsersService.UpdateUser(
                user.ExternalId,
                user.FirstName,
                request.NewPassword,
                request.PasswordConfirmation);

            Mapper.Map(request, user);

            await DbContext.SaveChangesAsync(cancellationToken);

            await Notify(user);

            return Unit.Value;
        }

        private async Task Notify(User user)
        {
            await Mediator.Send(new PasswordUpdatedEmailCommand(user.Email));

            var notificationText = $"Your {StringProvider.GetPartnerName(_partnerProvider)} password was changed. " +
                                   $"If you did not do this, call {StringProvider.GetSupportPhone(_partnerProvider)} to lock your account. " +
                                   $"The {StringProvider.GetPartnerName(_partnerProvider)} Team";

            try
            {
                await _notificationUsersService.SendNotification(
                    user.Id, notificationText,
                    user.Settings.IsNotificationsEnabled,
                    NotificationTypeEnum.Information,
                    true, user.Devices,
                    null,
                    null,
                    PushNotificationCategory.YourB9PasswordWasChanged);
            }
            catch (Exception ex)
            {
                _logger.LogError($"Error when sending push notification: {ex.Message}", ex);
            }

            try
            {
                await _smsService.SendMessageAsync(user.Phone, notificationText);
            }
            catch (Exception ex)
            {
                _logger.LogError($"Error when sending sms: {ex.Message}", ex);
            }
        }
    }
}
