﻿namespace BNine.Application.Aggregates.Users.Commands.Update.UpdatePassword
{
    using AutoMapper;
    using BNine.Application.Mappings;
    using BNine.Common.TableConnect.Common;
    using Domain.Entities.User;
    using MediatR;
    using Newtonsoft.Json;

    public class UpdatePasswordCommand : IRequest<Unit>, IMapTo<User>
    {
        [JsonProperty("oldPassword")]
        public string OldPassword
        {
            get; set;
        }

        [JsonProperty("newPassword")]
        public string NewPassword
        {
            get; set;
        }

        [JsonProperty("passwordConfirmation")]
        public string PasswordConfirmation
        {
            get; set;
        }

        public void Mapping(Profile profile)
        {
            profile.CreateMap<UpdatePasswordCommand, User>()
                .ForMember(dst => dst.Password, opt => opt.MapFrom(src => CryptoHelper.HashPassword(src.NewPassword)));
        }
    }
}
