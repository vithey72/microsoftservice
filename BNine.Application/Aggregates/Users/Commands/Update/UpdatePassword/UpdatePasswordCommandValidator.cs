﻿namespace BNine.Application.Aggregates.Users.Commands.Update.UpdatePassword
{
    using BNine.Application.Interfaces;
    using BNine.Common;
    using BNine.ResourceLibrary;
    using FluentValidation;
    using Microsoft.Extensions.Localization;

    public class UpdatePasswordCommandValidator : AbstractValidator<UpdatePasswordCommand>
    {
        private IBNineDbContext DbContext
        {
            get;
        }

        private IStringLocalizer<SharedResource> SharedLocalizer;

        public UpdatePasswordCommandValidator(
            IBNineDbContext dbContext,
            IStringLocalizer<SharedResource> sharedLocalizer
            )
        {
            DbContext = dbContext;

            SharedLocalizer = sharedLocalizer;

            RuleFor(x => x.NewPassword)
                .Must(PasswordsHelper.IsMeetsRequirements)
                .WithMessage(SharedLocalizer["passwordPoliciesValidationErrorMessage"].Value)
                ;

            RuleFor(x => x.PasswordConfirmation)
                .NotEmpty()
                .Equal(x => x.NewPassword);
        }


    }
}
