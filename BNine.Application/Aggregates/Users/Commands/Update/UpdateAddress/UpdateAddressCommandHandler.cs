﻿namespace BNine.Application.Aggregates.Users.Commands.Update.UpdateAddress
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading;
    using System.Threading.Tasks;
    using Abstractions;
    using AutoMapper;
    using BNine.Application.Aggregates.ServiceBusEvents.Commands.PublishB9Event;
    using BNine.Application.Aggregates.Users.Models;
    using BNine.Application.Exceptions.ApiResponse;
    using BNine.Application.Interfaces.Bank.Administration;
    using BNine.Application.Interfaces.Bank.Client;
    using BNine.Common;
    using BNine.Constants;
    using BNine.Domain.Events.Users;
    using Domain.Entities.User;
    using Emails.Commands.User.AddressUpdated;
    using Enums;
    using Exceptions;
    using Helpers;
    using Interfaces;
    using MediatR;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.Extensions.Logging;
    using Newtonsoft.Json;

    public class UpdateAddressCommandHandler
        : AbstractRequestHandler, IRequestHandler<UpdateAddressCommand>
    {
        private readonly ISmsService _smsService;
        private readonly INotificationUsersService _notificationUsersService;
        private readonly IBankClientAddressService _bankClientAddressService;
        private readonly IBankTemplatesService _bankTemplatesService;
        private readonly IPartnerProviderService _partnerProvider;
        private readonly ILogger<UpdateAddressCommandHandler> _logger;

        public UpdateAddressCommandHandler(
            ISmsService smsService,
            INotificationUsersService notificationUsersService,
            IMediator mediator,
            IBNineDbContext dbContext,
            IMapper mapper,
            ICurrentUserService currentUser,
            IBankClientAddressService bankClientAddressService,
            IBankTemplatesService bankTemplatesService,
            IPartnerProviderService partnerProvider,
            ILogger<UpdateAddressCommandHandler> logger
        )
            : base(mediator, dbContext, mapper, currentUser)
        {
            _smsService = smsService;
            _notificationUsersService = notificationUsersService;
            _bankClientAddressService = bankClientAddressService;
            _bankTemplatesService = bankTemplatesService;
            _partnerProvider = partnerProvider;
            _logger = logger;
        }

        public async Task<Unit> Handle(UpdateAddressCommand request, CancellationToken cancellationToken)
        {
            var user = DbContext.Users
                .Include(x => x.Devices)
                .Include(x => x.Address)
                .FirstOrDefault(x => x.Id == CurrentUser.UserId);

            if (user.Status == UserStatus.Active)
            {
                throw new ApiResponseException(ApiResponseErrorCodes.UpdateAddressDisabled, "Sorry, this request doesn't work for a while");
            }

            if (user == null)
            {
                throw new NotFoundException(nameof(User), CurrentUser.UserId);
            }

            var hasAddress = user.Address != null;
            var statusChanged = false;
            if (user.Status != UserStatus.Active)
            {
                user.Address = Mapper.Map(request, user.Address);

                if (user.Status == UserStatus.PendingAddress)
                {
                    user.Status = UserStatus.PendingSSN;
                    statusChanged = true;
                }
            }
            else
            {
                var addressExternalId = user.Address.ExternalId;

                user.Address = Mapper.Map(request, user.Address);

                await SetMbanqClientAddress(user, addressExternalId.Value);

                await NotifyUser(user);
            }
            user.DomainEvents.Add(new OnboardingStepSuccessEvent(user, UserStatus.PendingAddress, UserStatusDetailed.PendingAddress));
            await DbContext.SaveChangesAsync(cancellationToken);

            if (hasAddress)
            {
                await Mediator.Send(new PublishB9EventCommand(
                     user,
                     ServiceBusTopics.B9User,
                     ServiceBusEvents.B9UserAddressUpdated));
            }
            else
            {
                await Mediator.Send(new PublishB9EventCommand(
                    user,
                    ServiceBusTopics.B9User,
                    ServiceBusEvents.B9UserAddressCreated));
            }

            if (statusChanged)
            {
                await Mediator.Send(new PublishB9EventCommand(
                 user,
                 ServiceBusTopics.B9User,
                 ServiceBusEvents.B9Updated,
                 new Dictionary<string, object>
                 {
                    { ServiceBusDefaults.UpdatedFieldsAdditionalProperty , JsonConvert.SerializeObject(new List<string> { nameof(user.Status) }) }
                 }));
            }

            return Unit.Value;
        }

        private async Task NotifyUser(User user)
        {
            await Mediator.Send(new AddressUpdatedEmailCommand(user.Email, user.FirstName));

            var notificationText = $"Your address associated with {StringProvider.GetPartnerName(_partnerProvider)} account was changed. " +
                                   $"If you did not do this, call {StringProvider.GetSupportPhone(_partnerProvider)} to lock your account. " +
                                   $"The {StringProvider.GetPartnerName(_partnerProvider)} Team";

            try
            {
                await _notificationUsersService.SendNotification(
                    user.Id,
                    notificationText,
                    user.Settings.IsNotificationsEnabled,
                    NotificationTypeEnum.Information,
                    true,
                    user.Devices,
                    null,
                    null,
                    PushNotificationCategory.AddressAssociatedWithB9AccountWasChanged);
            }
            catch (Exception ex)
            {
                _logger.LogError($"Error when sending push notification: {ex.Message}", ex);
            }

            try
            {
                await _smsService.SendMessageAsync(user.Phone, notificationText);
            }
            catch (Exception ex)
            {
                _logger.LogError($"Error when sending sms: {ex.Message}", ex);
            }
        }

        private async Task SetMbanqClientAddress(User user, int addressExternalId)
        {
            var addressParametersOptions = await _bankTemplatesService.GetAddressParametersOptions();
            var userStateInfo = addressParametersOptions.StateIdOptions.FirstOrDefault(x => x.Name.Equals(StatesHelper.MapToFullStateName(user.Address.State)));

            if (userStateInfo == null)
            {
                throw new NotFoundException(nameof(CountryIdOption), user.Address.State);
            }

            var userAddressType = addressParametersOptions.AddressTypeIdOptions.FirstOrDefault(x => x.Name.Equals(ClientAddressTypes.Primary));
            if (userAddressType == null)
            {
                throw new NotFoundException(nameof(AddressTypeIdOption), ClientAddressTypes.Primary);
            }

            user.Address.ExternalId = await _bankClientAddressService.UpdateAddress(
                addressExternalId,
                user.Address.City,
                userStateInfo.Id,
                user.Address.AddressLine,
                user.Address.ZipCode,
                user.Address.Unit,
                userStateInfo.ParentId);
        }
    }
}
