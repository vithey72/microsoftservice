﻿namespace BNine.Application.Aggregates.Users.Commands.Update.UpdateAddress
{
    using AutoMapper;
    using BNine.Application.Mappings;
    using Domain.Entities.Common;
    using MediatR;
    using Newtonsoft.Json;

    public class UpdateAddressCommand : IRequest<Unit>, IMapTo<Address>
    {
        [JsonProperty("zipCode")]
        public string ZipCode
        {
            get; set;
        }

        [JsonProperty("city")]
        public string City
        {
            get; set;
        }

        [JsonProperty("state")]
        public string State
        {
            get; set;
        }

        [JsonProperty("address")]
        public string Address
        {
            get; set;
        }

        [JsonProperty("unit")]
        public string Unit
        {
            get; set;
        }

        public void Mapping(Profile profile)
        {
            profile.CreateMap<UpdateAddressCommand, Address>()
                .ForMember(dst => dst.AddressLine, opt => opt.MapFrom(src => src.Address))
                .ForMember(dst => dst.City, opt => opt.MapFrom(src => src.City))
                .ForMember(dst => dst.State, opt => opt.MapFrom(src => src.State))
                .ForMember(dst => dst.ZipCode, opt => opt.MapFrom(src => src.ZipCode))
                .ForMember(dst => dst.Unit, opt => opt.MapFrom(src => src.Unit));
        }
    }
}
