﻿namespace BNine.Application.Aggregates.Users.Commands.Update.UpdateProfilePhoto
{
    using Microsoft.AspNetCore.Http;
    using MediatR;

    public class UpdateProfilePhotoCommand : IRequest<string>
    {
        public IFormFile ProfilePhoto
        {
            get;
            set;
        }
    }
}
