﻿namespace BNine.Application.Aggregates.Users.Commands.Update.UpdateProfilePhoto
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading;
    using System.Threading.Tasks;
    using Abstractions;
    using AutoMapper;
    using BNine.Application.Aggregates.ServiceBusEvents.Commands.PublishB9Event;
    using BNine.Settings.Blob;
    using Constants;
    using Domain.Entities.User;
    using Exceptions;
    using Interfaces;
    using MediatR;
    using Microsoft.Extensions.Options;
    using Newtonsoft.Json;

    public class UpdateProfilePhotoCommandHandler :
        AbstractRequestHandler,
        IRequestHandler<UpdateProfilePhotoCommand, string>
    {
        private BlobStorageSettings BlobSettings
        {
            get;
        }
        private IBlobStorageService BlobStorageService
        {
            get;
        }

        public UpdateProfilePhotoCommandHandler(
            IOptions<BlobStorageSettings> options,
            IBlobStorageService blobStorageService,
            IMediator mediator,
            IBNineDbContext dbContext,
            IMapper mapper,
            ICurrentUserService currentUser) : base(mediator, dbContext, mapper, currentUser)
        {
            BlobStorageService = blobStorageService;
            BlobSettings = options.Value;
        }

        public async Task<string> Handle(UpdateProfilePhotoCommand request, CancellationToken cancellationToken)
        {
            var user = DbContext.Users.FirstOrDefault(x => x.Id == CurrentUser.UserId);
            if (user == null)
            {
                throw new NotFoundException(nameof(User), CurrentUser.UserId);
            }

            if (user.ProfilePhotoUrl != null)
            {
                await BlobStorageService.RemoveFile(ContainerNames.BnineContainer, user.ProfilePhotoUrl, cancellationToken);
            }

            var profilePhotoUrl = await BlobStorageService.UploadFile(ContainerNames.BnineContainer, request.ProfilePhoto, cancellationToken: cancellationToken);
            user.ProfilePhotoUrl = profilePhotoUrl;

            await DbContext.SaveChangesAsync(cancellationToken);

            await Mediator.Send(new PublishB9EventCommand(
                 user,
                 ServiceBusTopics.B9User,
                 ServiceBusEvents.B9Updated,
                 new Dictionary<string, object>
                 {
                                  { ServiceBusDefaults.UpdatedFieldsAdditionalProperty , JsonConvert.SerializeObject(new List<string> { nameof(user.ProfilePhotoUrl) }) }
                 }));

            return profilePhotoUrl;
        }
    }
}
