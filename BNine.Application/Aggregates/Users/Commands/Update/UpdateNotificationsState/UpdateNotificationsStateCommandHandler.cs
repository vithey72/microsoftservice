﻿namespace BNine.Application.Aggregates.Users.Commands.Update.UpdateNotificationsState
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading;
    using System.Threading.Tasks;
    using Abstractions;
    using AutoMapper;
    using BNine.Application.Aggregates.ServiceBusEvents.Commands.PublishB9Event;
    using BNine.Constants;
    using Domain.Entities.User;
    using Exceptions;
    using Interfaces;
    using MediatR;
    using Newtonsoft.Json;

    public class UpdateNotificationsStateCommandHandler : AbstractRequestHandler, IRequestHandler<UpdateNotificationsStateCommand>
    {
        public UpdateNotificationsStateCommandHandler(IMediator mediator, IBNineDbContext dbContext, IMapper mapper, ICurrentUserService currentUser) : base(mediator, dbContext, mapper, currentUser)
        {
        }

        public async Task<Unit> Handle(UpdateNotificationsStateCommand request, CancellationToken cancellationToken)
        {
            var user = DbContext.Users.FirstOrDefault(x => x.Id == CurrentUser.UserId);
            if (user == null)
            {
                throw new NotFoundException(nameof(User), CurrentUser.UserId);
            }

            user.Settings.IsNotificationsEnabled = request.IsNotificationsEnabled;

            await DbContext.SaveChangesAsync(cancellationToken);

            await Mediator.Send(new PublishB9EventCommand(
                user,
                ServiceBusTopics.B9User,
                ServiceBusEvents.B9UserSettingsUpdated,
                new Dictionary<string, object>
                {
                    {
                        ServiceBusDefaults.UpdatedFieldsAdditionalProperty,
                        JsonConvert.SerializeObject(new List<string> { nameof(user.Settings.IsNotificationsEnabled) })
                    }
                }));

            return Unit.Value;
        }
    }
}
