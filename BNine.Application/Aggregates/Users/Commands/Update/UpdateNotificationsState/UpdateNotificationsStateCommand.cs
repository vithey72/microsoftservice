﻿namespace BNine.Application.Aggregates.Users.Commands.Update.UpdateNotificationsState
{
    using MediatR;
    using Newtonsoft.Json;

    public class UpdateNotificationsStateCommand : IRequest<Unit>
    {
        [JsonProperty("isNotificationsEnabled")]
        public bool IsNotificationsEnabled
        {
            get; set;
        }
    }
}
