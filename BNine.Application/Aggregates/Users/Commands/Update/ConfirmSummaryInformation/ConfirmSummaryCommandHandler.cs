﻿namespace BNine.Application.Aggregates.Users.Commands.Update.ConfirmSummaryInformation
{
    using System.Linq;
    using System.Threading;
    using System.Threading.Tasks;
    using Abstractions;
    using AutoMapper;
    using Domain.Entities.User;
    using Emails.Commands.Registration.Denied;
    using Enums;
    using Evaluation.Commands;
    using Evaluation.Models;
    using Exceptions;
    using Interfaces;
    using Interfaces.Alloy;
    using MediatR;
    using Microsoft.EntityFrameworkCore;
    using BNine.Domain.Events.Users;
    using System.Collections.Generic;
    using System;
    using BNine.Constants;
    using BNine.Application.Aggregates.ServiceBusEvents.Commands.PublishB9Event;
    using Microsoft.Extensions.Logging;
    using Newtonsoft.Json;

    [Obsolete]
    public class ConfirmSummaryCommandHandler :
        AbstractRequestHandler,
        IRequestHandler<ConfirmSummaryCommand>
    {
        private const string AlloyApprovedStatus = "Approved";
        private const string ManualReviewResponse = "Manual Review";
        private const string DeniedStatus = "Denied";

        private const string Socure30ServiceKey = "Socure 30";
        private const string Socure30ServiceSummaryFailed = "failed";

        private IDocsAlloyOnboardingService DocsAlloyOnboardingService
        {
            get;
        }

        public ILogger<ConfirmSummaryCommandHandler> Logger
        {
            get;
        }

        public ConfirmSummaryCommandHandler(
            IDocsAlloyOnboardingService docsAlloyOnboardingService,
            IMediator mediator,
            IBNineDbContext dbContext,
            IMapper mapper,
            ICurrentUserService currentUser,
            ILogger<ConfirmSummaryCommandHandler> logger) : base(mediator, dbContext, mapper, currentUser)
        {
            DocsAlloyOnboardingService = docsAlloyOnboardingService;
            Logger = logger;
        }

        public async Task<Unit> Handle(ConfirmSummaryCommand request, CancellationToken cancellationToken)
        {
            var user = DbContext.Users.FirstOrDefault(x => x.Id == CurrentUser.UserId);
            if (user == null)
            {
                throw new NotFoundException(nameof(User), CurrentUser.UserId);
            }
            var statusChanged = false;
            try
            {
                var kycVerificationResult = await DbContext.KYCVerificationResults
                   .OrderByDescending(x => x.CreatedAt)
                   .FirstOrDefaultAsync(x => x.UserId == CurrentUser.UserId);

                if (kycVerificationResult == null)
                {
                    var evaluation = await EvaluateUser(request, user);

                    // Socure 30 fails to present any results sometimes,
                    // that leads to Manual Review. Retry should help
                    if (evaluation.Summary.OutCome.Equals(ManualReviewResponse) &&
                        evaluation.Summary.Services != null &&
                        evaluation.Summary.Services.ContainsKey(Socure30ServiceKey) &&
                        evaluation.Summary.Services[Socure30ServiceKey] == Socure30ServiceSummaryFailed &&
                        evaluation.RawResponses != null &&
                        evaluation.RawResponses.ContainsKey(Socure30ServiceKey) &&
                        evaluation.RawResponses[Socure30ServiceKey].Length == 0)
                    {
                        Logger.LogInformation("Alloy re-avaluation due to Socure 30 scoring outage");
                        evaluation = await EvaluateUser(request, user);
                    }

                    var enumStatus = evaluation.Summary.OutCome switch
                    {
                        AlloyApprovedStatus => KycStatusEnum.Approved,
                        DeniedStatus => KycStatusEnum.Denied,
                        ManualReviewResponse => KycStatusEnum.ManualReview,
                        _ => KycStatusEnum.Unknown,
                    };

                    kycVerificationResult = new KYCVerificationResult
                    {
                        EntityToken = evaluation.EntityToken,
                        EvaluationToken = evaluation.EvaluationToken,
                        Status = evaluation.Summary.OutCome,
                        StatusEnum = enumStatus,
                        EvaluationProvider = EvaluationProviderEnum.Alloy,
                        RawEvaluation = evaluation.RawResult,
                        UserId = user.Id,
                        Id = Guid.NewGuid()
                    };

                    await DbContext.KYCVerificationResults.AddAsync(kycVerificationResult, cancellationToken);

                    await Mediator.Send(new PublishB9EventCommand(kycVerificationResult, ServiceBusTopics.B9KYCVerifications, ServiceBusEvents.B9Created));
                }

                if (kycVerificationResult.StatusEnum != KycStatusEnum.Approved)
                {
                    user.Status = UserStatus.Declined;
                    user.DomainEvents.Add(new OnboardingStepFailedEvent(user, UserStatus.PendingSummary, UserStatusDetailed.PendingSummary, new List<string> { "Alloy status: Declined" }));
                }
                else if (user.Status == UserStatus.PendingSummary)
                {
                    user.Status = UserStatus.PendingAgreement;
                    statusChanged = true;
                    user.DomainEvents.Add(new OnboardingStepSuccessEvent(user, UserStatus.PendingSummary, UserStatusDetailed.PendingSummary));
                }

                await UpdateUserAddress(kycVerificationResult.RawEvaluation, user);
                await DbContext.SaveChangesAsync(cancellationToken);
            }
            catch (Exception ex)
            {
                user.DomainEvents = new List<Domain.Infrastructure.DomainEvent>
                {
                    new OnboardingStepFailedEvent(user, UserStatus.PendingSummary, UserStatusDetailed.PendingSummary, new List<string> { ex.Message })
                };

                await DbContext.SaveChangesAsync(cancellationToken);

                throw;
            }

            if (user.Status == UserStatus.Declined)
            {
                await Mediator.Send(new RegistrationDeniedEmailCommand { UserId = user.Id });
            }

            if (statusChanged)
            {
                await Mediator.Send(new PublishB9EventCommand(
                 user,
                 ServiceBusTopics.B9User,
                 ServiceBusEvents.B9Updated,
                 new Dictionary<string, object>
                 {
                    { ServiceBusDefaults.UpdatedFieldsAdditionalProperty , JsonConvert.SerializeObject(new List<string> { nameof(user.Status) }) }
                 }));
            }

            return Unit.Value;
        }

        private async Task UpdateUserAddress(string rawAlloyAnswer, User user)
        {
            var parsedJson = JsonConvert.DeserializeObject<EvaluationSuccessResponse>(rawAlloyAnswer);
            user.Address.City = parsedJson.FormattedAddress.City;
            user.Address.ZipCode = parsedJson.FormattedAddress.PostalCode.Substring(0, 5); //Because Alloy return zip+4, nine length postal code symbols, Mbanq couldn't parse it.
            user.Address.AddressLine = parsedJson.FormattedAddress.AddressLine1;
            user.Address.Unit = parsedJson.FormattedAddress.AddressLine2; // As I understand, Unit will return from Alloy as a part of Address Line 2 field.
        }

        private async Task<EvaluationSuccessResponse> EvaluateUser(ConfirmSummaryCommand request, User user)
        {
            if (request.IpAddress == "::1") // TODO: rewrite: move to model. should be applicable only for IWebHostEnvironment.IsDevelopment() == true
            {
                request.IpAddress = "127.0.0.1";
            }

            var docsAlloyOnboardingCommand = new EvaluateUserCommand
            {
                PhoneNumber = user.Phone,
                FirstName = user.FirstName,
                LastName = user.LastName,
                Email = user.Email,
                BirthDate = user.DateOfBirth!.Value.ToString("s").Split('T')[0], // TODO: really?!
                Address = user.Address.AddressLine + (string.IsNullOrEmpty(user.Address.Unit) ? string.Empty : $", {user.Address.Unit}"),
                City = user.Address.City,
                State = user.Address.State,
                SsnNumber = user.Identity.Value,
                PostalCode = user.Address.ZipCode,
                CountryCode = "US",
                IpAddress = request.IpAddress,
                IovationBlackBox = request.IovationBlackBox,
                JobId = user.Document.VouchedJobId
            };

            var kycResult = await DbContext.KYCVerificationResults
                .Where(x => x.UserId == CurrentUser.UserId)
                .OrderByDescending(x => x.CreatedAt)
                .Select(x => new { x.EntityToken })
                .FirstOrDefaultAsync();

            return await DocsAlloyOnboardingService.EvaluateUser(docsAlloyOnboardingCommand, kycResult?.EntityToken);
        }
    }
}
