﻿namespace BNine.Application.Aggregates.Users.Commands.Update.ConfirmSummaryInformation
{
    using BNine.Application.Behaviours;
    using MediatR;
    using Newtonsoft.Json;

    [Sequential]
    [Obsolete]
    public class ConfirmSummaryCommand : IRequest<Unit>
    {
        [JsonIgnore]
        public string IpAddress
        {
            get; set;
        }

        [JsonProperty("iovationBlackBox")]
        public string IovationBlackBox
        {
            get; set;
        }
    }
}
