﻿namespace BNine.Application.Aggregates.Users.Closure;

public interface IAccountClosurePrerequisites
{
    bool CanCloseAccount();
}
