﻿namespace BNine.Application.Aggregates.Users.Closure;

public interface IAccountClosurePrerequisitesProvider
{
    Task<IAccountClosurePrerequisites> GetPrerequisites(Guid userId, CancellationToken cancellationToken);
}
