﻿namespace BNine.Application.Aggregates.Users.Closure;

using BNine.Application.Aggregates.Users.Models;
using BNine.Enums;

internal class AccountClosurePrerequisites : IAccountClosurePrerequisites
{
    private readonly ClientAccountClosureValidationResult _validationResult;
    private readonly bool _canClose;

    public AccountClosurePrerequisites(ClientAccountClosureValidationResult validationResult)
    {
        _validationResult = validationResult;
        _canClose = CanCloseAccountImpl();
    }

    public bool CanCloseAccount() => _canClose;

    private bool CanCloseAccountImpl()
    {
        var hasBlockingErrors = _validationResult.Reasons.Any(x => x != ClientAccountClosureResultCode.ArgyleConnected);
        return !hasBlockingErrors;
    }
}
