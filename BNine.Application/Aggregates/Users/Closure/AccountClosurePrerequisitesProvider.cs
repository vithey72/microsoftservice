﻿namespace BNine.Application.Aggregates.Users.Closure;

using BNine.Application.Aggregates.Users.Queries.CloseAccount.ValidateCloseClientAccount;
using MediatR;

public class AccountClosurePrerequisitesProvider : IAccountClosurePrerequisitesProvider
{
    private readonly IMediator _mediator;

    public AccountClosurePrerequisitesProvider(IMediator mediator)
    {
        _mediator = mediator;
    }

    public async Task<IAccountClosurePrerequisites> GetPrerequisites(Guid userId, CancellationToken cancellationToken)
    {
        var validationResult = await _mediator.Send(new ValidateCloseClientAccountQuery { UserId = userId },
            cancellationToken);

        var prerequisites = new AccountClosurePrerequisites(validationResult);

        return prerequisites;
    }
}
