﻿namespace BNine.Application.Aggregates.Users.Queries.GetUserGroups;

using System;
using System.Collections.Generic;
using BNine.Domain.Entities.Features;
using MediatR;

public record GetUserGroupsQuery(Guid UserId) : IRequest<IEnumerable<Group>>;
