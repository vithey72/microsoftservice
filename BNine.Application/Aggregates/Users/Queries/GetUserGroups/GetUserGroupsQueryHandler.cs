﻿namespace BNine.Application.Aggregates.Users.Queries.GetUserGroups
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading;
    using System.Threading.Tasks;
    using AutoMapper;
    using BNine.Application.Abstractions;
    using BNine.Application.Algorithms;
    using BNine.Application.Interfaces;
    using BNine.Domain.Entities.Features;
    using BNine.Domain.Entities.Features.Groups;
    using Constants;
    using MediatR;
    using Microsoft.EntityFrameworkCore;
    using Newtonsoft.Json;

    public class GetUserGroupsQueryHandler
        : AbstractRequestHandler, IRequestHandler<GetUserGroupsQuery, IEnumerable<Group>>
    {
        public GetUserGroupsQueryHandler(
            IMediator mediator,
            IBNineDbContext dbContext,
            IMapper mapper,
            ICurrentUserService currentUser)
            : base(mediator, dbContext, mapper, currentUser)
        {
        }

        public async Task<IEnumerable<Group>> Handle(GetUserGroupsQuery request, CancellationToken cancellationToken)
        {
            var featureGroups = await
                      (from fg in DbContext.FeatureGroups
                       join g in DbContext.Groups.Where(gr => gr.Kind == GroupKind.Default ||
                                 gr.Kind == GroupKind.Sample) on fg.GroupId equals g.Id
                       select fg)
                       .Union
                      (from fg in DbContext.FeatureGroups
                       join g in DbContext.Groups.Where(gr => gr.UserGroups.Any(ug => ug.UserId == request.UserId)) on fg.GroupId equals g.Id
                       select fg)
                       .Include(f => f.Feature)
                       .Include(fg => fg.Group)
                      .ToListAsync(cancellationToken);

            var groups = featureGroups
            .Select(x => x.Group)
            .OrderBy(x => x.Kind);

            var result = new List<Group>();
            foreach (Group group in groups)
            {
                switch (group)
                {
                    case DefaultGroup defaultGroup: result.Add(defaultGroup); break;
                    case UserListGroup userListGroup: result.Add(userListGroup); break;
                    case PersonalGroup personalGroup: result.Add(personalGroup); break;
                    case SampleGroup sampleGroup:
                        var rdn = GuidFloatAbHashAlgorithm.Calculate(request.UserId, sampleGroup.Seed);
                        if (rdn > sampleGroup.Rollout || rdn <= sampleGroup.SubRangeStart * sampleGroup.Rollout || rdn > sampleGroup.SubRangeEnd * sampleGroup.Rollout)
                        {
                            continue;
                        }
                        result.Add(sampleGroup);
                        break;
                }
            }

            await ExcludeUsersFromExperimentGroupsIfNecessary(result);

            await SaveGroupsToUserEntity(request.UserId, result, cancellationToken);

            await DbContext.SaveChangesAsync(cancellationToken);

            return result;
        }

        private async Task SaveGroupsToUserEntity(Guid userId, List<Group> result, CancellationToken cancellationToken)
        {
            var user = await DbContext.Users
                .FirstAsync(u => u.Id == userId, cancellationToken);

            var groupsCalculated = result
                .Select(g => g.Id)
                .Distinct()
                .OrderByDescending(x => x)
                .ToArray();
            var groupsSerialized = JsonConvert.SerializeObject(groupsCalculated);
            if (user.Groups != groupsSerialized)
            {
                user.Groups = groupsSerialized;
            }
        }

        private async Task ExcludeUsersFromExperimentGroupsIfNecessary(List<Group> groups)
        {
            if (CurrentUser.UserId == null)
            {
                return;
            }

            var experimentGroups = groups.Where(g => g.Kind == GroupKind.Sample).ToArray();
            foreach (var group in experimentGroups)
            {
                if (group.Name is FeaturesNames.Groups.AdvanceWidgetBannerGroupA
                    or FeaturesNames.Groups.AdvanceWidgetBannerGroupB)
                {
                    if (await UserNotFittingAdvanceWidgetExperimentCriteria())
                    {
                        groups.Remove(group);
                    }
                }
            }
        }

        private async Task<bool> UserNotFittingAdvanceWidgetExperimentCriteria()
        {
            var user = await DbContext.Users
                .Include(u => u.AdvanceStats)
                .Include(u => u.Settings)
                .Include(u => u.LoanAccounts)
                .FirstOrDefaultAsync(u => u.Id == CurrentUser.UserId);

            return (user!.AdvanceStats != null && user.AdvanceStats.AvailableLimit > 10) ||
                   user.LoanAccounts.Any() ||
                   user.Settings.IsPayAllocationEnabled;
        }
    }
}
