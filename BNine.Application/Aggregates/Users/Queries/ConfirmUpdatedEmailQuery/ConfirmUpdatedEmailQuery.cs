﻿namespace BNine.Application.Aggregates.Users.Queries.ConfirmUpdatedEmailQuery;

using MediatR;
using Newtonsoft.Json;

public class ConfirmUpdatedEmailQuery : IRequest<Unit>
{
    [JsonProperty("emailChangeRequestId")]
    public Guid EmailChangeRequestId
    {
        get; set;
    }

    [JsonProperty("userId")]
    public Guid UserId
    {
        get; set;
    }
}
