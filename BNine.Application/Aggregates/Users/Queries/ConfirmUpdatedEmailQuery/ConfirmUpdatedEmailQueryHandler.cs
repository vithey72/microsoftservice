﻿namespace BNine.Application.Aggregates.Users.Queries.ConfirmUpdatedEmailQuery;

using BNine.Application.Abstractions;
using BNine.Application.Interfaces.Bank.Administration;
using BNine.Application.Interfaces;
using MediatR;
using Microsoft.Extensions.Logging;
using AutoMapper;
using Microsoft.EntityFrameworkCore;
using BNine.Enums;
using BNine.Constants;
using BNine.Application.Aggregates.Emails.Commands.User.EmailUpdated;
using BNine.Application.Helpers;
using BNine.Domain.Entities.User;

public class ConfirmUpdatedEmailQueryHandler : AbstractRequestHandler,
     IRequestHandler<ConfirmUpdatedEmailQuery, Unit>
{
    private readonly INotificationUsersService _notificationUsersService;
    private readonly ISmsService _smsService;
    private readonly IBankUsersService _bankUsersService;
    private readonly IBankClientsService _bankClientsService;
    private readonly IPartnerProviderService _partnerProvider;
    private readonly ILogger<ConfirmUpdatedEmailQueryHandler> _logger;

    public ConfirmUpdatedEmailQueryHandler(
        INotificationUsersService notificationUsersService,
        ISmsService smsService,
        IMediator mediator,
        IBNineDbContext dbContext,
        IMapper mapper,
        ICurrentUserService currentUser,
        IBankUsersService bankUsersService,
        IBankClientsService bankClientsService,
        IPartnerProviderService partnerProvider,
        ILogger<ConfirmUpdatedEmailQueryHandler> logger
        )
        : base(mediator, dbContext, mapper, currentUser)
    {
        _notificationUsersService = notificationUsersService;
        _smsService = smsService;
        _bankUsersService = bankUsersService;
        _bankClientsService = bankClientsService;
        _partnerProvider = partnerProvider;
        _logger = logger;
    }

    public async Task<Unit> Handle(ConfirmUpdatedEmailQuery request, CancellationToken cancellationToken)
    {
        var changeEmailRequest = await DbContext.UpdateUserInfoRequests
            .Where(x => x.UserId == request.UserId)
            .Where(x => x.Id == request.EmailChangeRequestId)
            .Where(x => x.Status == RequestProcessingStatus.OPENED)
            .FirstOrDefaultAsync(cancellationToken);

        if (changeEmailRequest == null)
        {
            return Unit.Value;
        }

        var user = DbContext.Users
            .Include(x => x.Devices)
            .FirstOrDefault(x => x.Id == changeEmailRequest.UserId);

        changeEmailRequest.Status = RequestProcessingStatus.COMPLETED;

        user.Email = changeEmailRequest.UpdatedFieldValue;

        await _bankUsersService.UpdateUserEmail(
            user.ExternalId,
            changeEmailRequest.UpdatedFieldValue);

        if (user.ExternalClientId != null)
        {
            await _bankClientsService.UpdateClientEmail(user.ExternalClientId.Value,
                changeEmailRequest.UpdatedFieldValue);
        }

        await DbContext.SaveChangesAsync(cancellationToken);

        await NotifyUser(user);

        return Unit.Value;
    }

    private async Task NotifyUser(User user)
    {
        var message = $"Your email associated with {StringProvider.GetPartnerName(_partnerProvider)} account was changed. " +
                      $"If you did not do this, call {StringProvider.GetSupportPhone(_partnerProvider)} to lock your account. " +
                      $"The {StringProvider.GetPartnerName(_partnerProvider)} Team";
        try
        {
            await _smsService.SendMessageAsync(user.Phone, message);

        }
        catch (Exception ex)
        {
            _logger.LogError($"Error when sending sms: {ex.Message}", ex);
        }

        try
        {
            await _notificationUsersService.SendNotification(
                user.Id,
                message,
                user.Settings.IsNotificationsEnabled,
                NotificationTypeEnum.Information,
                true,
                user.Devices,
                null,
                null,
                PushNotificationCategory.YourEmailAssociatedWithB9WasChanged);

        }
        catch (Exception ex)
        {
            _logger.LogError($"Error when sending push notification: {ex.Message}", ex);
        }

        await Mediator.Send(new EmailUpdatedEmailCommand(user.Email, user.FirstName));
    }
}
