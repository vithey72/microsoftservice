﻿namespace BNine.Application.Aggregates.Users.Queries.GetDelayedBoostInfo;

using Domain.Entities.User;
using Exceptions;
using Interfaces;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Models.UserDelayedBoostInfo;

public class GetDelayedBoostInfoQueryHandler : IRequestHandler<GetDelayedBoostInfoQuery, UserDelayedBoostInfoViewModel>
{
    private readonly IBNineDbContext _context;

    public GetDelayedBoostInfoQueryHandler(IBNineDbContext context)
    {
        _context = context;
    }

    public async Task<UserDelayedBoostInfoViewModel> Handle(GetDelayedBoostInfoQuery request, CancellationToken cancellationToken)
    {
        var userWithOffer =
            await _context.Users.Include(u => u.AdvanceStats)
                .FirstOrDefaultAsync(u => u.Id == request.UserId, cancellationToken: cancellationToken);

        if (userWithOffer == null)
        {
            throw new NotFoundException();
        }

        var delayedBoostInfo = userWithOffer.AdvanceStats?.DelayedBoostSettings != null
            ? BuildDelayedBoostInfo(userWithOffer.AdvanceStats)
            : null;

        return new UserDelayedBoostInfoViewModel() { DelayedBoostInfo = delayedBoostInfo };


    }

    private static DelayedBoostInfo BuildDelayedBoostInfo(AdvanceStats stats)
    {
        var currentBoostAmount = stats.DelayedBoostSettings.DelayedBoostId switch
        {
            1 => stats.BoostLimit1,
            2 => stats.BoostLimit2,
            3 => stats.BoostLimit3,
            _ => throw new ArgumentOutOfRangeException(nameof(stats.DelayedBoostSettings.DelayedBoostId))
        };

        var currentOriginalAmount = stats.AvailableLimit;

        return new DelayedBoostInfo
        {
            ActivationDate = stats.DelayedBoostSettings.ActivationDate,
            PlannedForDisburseAmount = currentBoostAmount + currentOriginalAmount,
            BoostAmount = currentBoostAmount,
            OriginalAmount = currentOriginalAmount
        };
    }
}
