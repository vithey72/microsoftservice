﻿namespace BNine.Application.Aggregates.Users.Queries.GetDelayedBoostInfo;

using MediatR;
using Models.UserDelayedBoostInfo;

public record GetDelayedBoostInfoQuery(Guid UserId) : IRequest<UserDelayedBoostInfoViewModel>;
