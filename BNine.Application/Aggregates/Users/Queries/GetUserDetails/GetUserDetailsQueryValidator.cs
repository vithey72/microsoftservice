﻿namespace BNine.Application.Aggregates.Users.Queries.GetUserDetails
{
    using FluentValidation;

    public class GetUserDetailsQueryValidator : AbstractValidator<GetUserDetailsQuery>
    {
        public GetUserDetailsQueryValidator()
        {
            RuleFor(x => x.Id)
                .NotEmpty();
        }
    }
}
