namespace BNine.Application.Aggregates.Users.Queries.GetUserDetails;

using BNine.Application.Mappings;
using BNine.Domain.Entities.BankAccount;

public class AchTranferDetails : IMapFrom<ACHCreditTransfer>
{
    public int ExternalId
    {
        get; set;
    }
    public DateTime CreatedAt
    {
        get; set;
    }
    public string Reference
    {
        get; set;
    }
    public string Amount
    {
        get; set;
    }
    public void Mapping(AutoMapper.Profile profile) => profile.CreateMap<ACHCreditTransfer, AchTranferDetails>();
}