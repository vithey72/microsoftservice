﻿namespace BNine.Application.Aggregates.Users.Queries.GetUserDetails
{
    using System;
    using System.Linq;
    using System.Threading;
    using System.Threading.Tasks;
    using Abstractions;
    using AutoMapper;
    using AutoMapper.QueryableExtensions;
    using EarlySalary.EarlySalary.Queries.GetArgyleAllocations;
    using Enums;
    using Helpers;
    using Interfaces;
    using Interfaces.Bank.Administration;
    using MediatR;
    using Microsoft.EntityFrameworkCore;
    using Models;

    public class GetUserDetailsQueryHandler
        : AbstractRequestHandler, IRequestHandler<GetUserDetailsQuery, UserDetails>
    {
        private readonly IBankLoanAccountsService _loanAccountsService;

        public GetUserDetailsQueryHandler(
            IMediator mediator,
            IBNineDbContext dbContext,
            IMapper mapper,
            ICurrentUserService currentUser,
            IBankLoanAccountsService loanAccountsService
        ) : base(mediator, dbContext, mapper, currentUser)
        {
            _loanAccountsService = loanAccountsService;
        }

        public async Task<UserDetails> Handle(GetUserDetailsQuery request, CancellationToken cancellationToken)
        {
            var userEntity = await DbContext
                .Users
                .Include(x => x.AdvanceStats)
                .Include(x => x.UserSyncedStats)
                .Include(x => x.EarlySalaryWidgetConfiguration)
                .Include(x => x.UserCloseReason)
                .Include(x => x.UserBlockReason)
                .Where(x => x.Id == request.Id)
                .FirstOrDefaultAsync(cancellationToken);

            var userDetails = Mapper.Map<UserDetails>(userEntity);

            userDetails.IdentityValue = StringHelper.TakeLast(userDetails.IdentityValue, 4);

            var userAllocations = await Mediator.Send(
                new GetArgyleAllocationsQuery(userDetails.Id), cancellationToken);

            var argyleUserId = userEntity.EarlySalaryWidgetConfiguration != null &&
                               userEntity.EarlySalaryWidgetConfiguration.WidgetUserId != default &&
                               !userEntity.EarlySalaryWidgetConfiguration.DeletedAt.HasValue
                ? userEntity.EarlySalaryWidgetConfiguration.WidgetUserId
                : default;

            if (userAllocations.Any())
            {
                userDetails.PayAllocations = userAllocations;
            }
            else if (argyleUserId != default)
            {
                userDetails.PayAllocations =
                    new List<PayAllocationsListItem>() { new() { ArgyleUserId = argyleUserId } };
            }

            userDetails.FixedLimit = userDetails.AdvancedLimit = userEntity.AdvanceStats?.AvailableLimit ?? 0;
            userDetails.RecentPayrolls = userEntity.UserSyncedStats?.PayrollSumFor14Days ?? 0;
            userDetails.Rating = userDetails.Rating;
            userDetails.CurrentAdvanceUsed = await CalculateUsedAdvances(request.Id);

            userDetails.AchTransfers = await DbContext.ACHCreditTransfers
                .Where(x => x.UserId == request.Id)
                .ProjectTo<AchTranferDetails>(Mapper.ConfigurationProvider)
                .OrderByDescending(x => x.CreatedAt)
                .ToArrayAsync(cancellationToken);

            return userDetails;
        }

        private async Task<string> CalculateUsedAdvances(Guid userId)
        {
            var activeAdvance = await DbContext.LoanAccounts
                .FirstOrDefaultAsync(l => l.UserId == userId
                       && l.Status == LoanAccountStatus.Active);

            if (activeAdvance == null)
            {
                return "$0";
            }

            var bankEntity = await _loanAccountsService.GetLoan(activeAdvance.ExternalId);
            if (bankEntity == null)
            {
                return "$0";
            }

            return $"${bankEntity.Total}";
        }
    }
}
