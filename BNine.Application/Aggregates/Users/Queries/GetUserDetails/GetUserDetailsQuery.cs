﻿namespace BNine.Application.Aggregates.Users.Queries.GetUserDetails
{
    using System;
    using BNine.Application.Aggregates.Users.Models;
    using MediatR;

    public class GetUserDetailsQuery : IRequest<UserDetails>
    {
        public GetUserDetailsQuery(Guid id)
        {
            Id = id;
        }

        public Guid Id
        {
            get;
        }
    }
}
