﻿using BNine.Application.Aggregates.Users.Queries.GetUsersInGroup;

using AutoMapper;
using BNine.Application.Abstractions;
using BNine.Application.Aggregates.Features.Models;
using BNine.Application.Interfaces;
using MediatR;
using Microsoft.EntityFrameworkCore;

public class GetUsersInGroupQueryHandler
    : AbstractRequestHandler
    , IRequestHandler<GetUsersInGroupQuery, GroupViewModel>
{
    public GetUsersInGroupQueryHandler(
        IMediator mediator,
        IBNineDbContext dbContext,
        IMapper mapper,
        ICurrentUserService currentUser
        )
        : base(mediator, dbContext, mapper, currentUser)
    {
    }

    public async Task<GroupViewModel> Handle(GetUsersInGroupQuery request, CancellationToken cancellationToken)
    {
        var group = await DbContext.Groups
            .Include(g => g.UserGroups)
            .ThenInclude(ug => ug.User)
            .FirstOrDefaultAsync(g => g.Id == request.GroupId, cancellationToken);

        if (group == null)
        {
            throw new KeyNotFoundException($"Group with Id = {request.GroupId} is not present in DB.");
        }

        return Mapper.Map<GroupViewModel>(group);
    }
}
