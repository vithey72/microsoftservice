﻿namespace BNine.Application.Aggregates.Users.Queries.GetUsersInGroup;

using System;
using Features.Models;
using MediatR;

public record GetUsersInGroupQuery(Guid GroupId) : IRequest<GroupViewModel>;
