﻿namespace BNine.Application.Aggregates.Users.Queries.CheckUserIsActive;

using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using BNine.Application.Abstractions;
using BNine.Application.Interfaces;
using BNine.Enums;
using GetUserStatus;
using MediatR;
using Microsoft.EntityFrameworkCore;

public class CheckUserIsActiveQueryHandler
    : AbstractRequestHandler, IRequestHandler<CheckUserIsActiveQuery, bool>
{
    public CheckUserIsActiveQueryHandler(
        IMediator mediator,
        IBNineDbContext dbContext,
        IMapper mapper,
        ICurrentUserService currentUser)
        : base(mediator, dbContext, mapper, currentUser)
    {
    }

    public async Task<bool> Handle(CheckUserIsActiveQuery request, CancellationToken cancellationToken)
    {
        var user = await DbContext.Users.AsNoTracking()
                .FirstOrDefaultAsync(x => x.Id == CurrentUser.UserId
                                          && x.Status == UserStatus.Active,
                    cancellationToken);

        return user is not null;
    }
}
