﻿namespace BNine.Application.Aggregates.Users.Queries.GetUserStatus;
using MediatR;

public class CheckUserIsActiveQuery : IRequest<bool>
{
}
