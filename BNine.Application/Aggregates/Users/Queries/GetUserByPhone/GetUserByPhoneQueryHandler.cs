﻿namespace BNine.Application.Aggregates.Users.Queries.GetUserByPhone
{
    using System.Linq;
    using System.Threading;
    using System.Threading.Tasks;
    using AutoMapper;
    using BNine.Application.Abstractions;
    using BNine.Application.Interfaces;
    using BNine.Application.Models.Dto;
    using GetUserGroups;
    using MediatR;
    using Microsoft.EntityFrameworkCore;

    public class GetUserByPhoneQueryHandler
        : AbstractRequestHandler, IRequestHandler<GetUserByPhoneQuery, User>
    {
        public GetUserByPhoneQueryHandler(
            IMediator mediator,
            IBNineDbContext dbContext,
            IMapper mapper,
            ICurrentUserService currentUser)
            : base(mediator, dbContext, mapper, currentUser)
        {
        }

        public async Task<User> Handle(GetUserByPhoneQuery request, CancellationToken cancellationToken)
        {
            var result = await DbContext.Users
                .Where(x => x.Phone == request.PhoneNumber)
                .Select(x => new User
                {
                    Id = x.Id,
                    PasswordHash = x.Password,
                    Status = x.IsBlocked ? Enums.UserStatus.Blocked : x.Status,
                })
                .FirstOrDefaultAsync(cancellationToken);

            if (result != null)
            {
                var groups = await Mediator.Send(new GetUserGroupsQuery(result.Id), cancellationToken);
                result.Groups = groups.Select(ug => ug.Id).OrderBy(x => x).ToArray();
            }

            return result;
        }
    }
}
