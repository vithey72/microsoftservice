﻿namespace BNine.Application.Aggregates.Users.Queries.GetUserByPhone
{
    using BNine.Application.Models.Dto;
    using MediatR;

    public class GetUserByPhoneQuery : IRequest<User>
    {
        public string PhoneNumber
        {
            get; set;
        }
    }
}
