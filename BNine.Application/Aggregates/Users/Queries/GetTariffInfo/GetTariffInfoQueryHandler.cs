﻿namespace BNine.Application.Aggregates.Users.Queries.GetTariffInfo;

using Abstractions;
using AutoMapper;
using Interfaces;
using MediatR;

public class GetTariffInfoQueryHandler : AbstractRequestHandler, IRequestHandler<GetTariffInfoQuery, TariffPlansInfoAdminDto>
{
    private readonly ITariffPlanService _tariffPlanService;

    public GetTariffInfoQueryHandler(IMediator mediator,
        IBNineDbContext dbContext,
        IMapper mapper,
        ICurrentUserService currentUser,
        ITariffPlanService tariffPlanService
        ):
        base(mediator, dbContext, mapper, currentUser)
    {
        _tariffPlanService = tariffPlanService;
    }

    public async Task<TariffPlansInfoAdminDto> Handle(GetTariffInfoQuery request, CancellationToken cancellationToken)
    {
        var (current, cyclePaymentDate, upcoming, switchDate) =
            await _tariffPlanService.GetCurrentAndUpcomingTariffs(request.UserId, cancellationToken);

        var nextPaymentDate = cyclePaymentDate ?? switchDate;

        return new TariffPlansInfoAdminDto()
        {
            CurrentTariffName = current?.Name,
            NextTariffName = upcoming?.Name,
            NextPaymentDate = nextPaymentDate
        };
    }
}
