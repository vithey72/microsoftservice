﻿namespace BNine.Application.Aggregates.Users.Queries.GetTariffInfo;

using MediatR;

public record GetTariffInfoQuery(Guid UserId) : IRequest<TariffPlansInfoAdminDto>;
