﻿namespace BNine.Application.Aggregates.Users.Queries.GetTariffInfo;

public class TariffPlansInfoAdminDto
{
    public string CurrentTariffName
    {
        get;
        set;
    }

    public string NextTariffName
    {
        get;
        set;
    }

    public DateTime? NextPaymentDate
    {
        get;
        set;
    }
}
