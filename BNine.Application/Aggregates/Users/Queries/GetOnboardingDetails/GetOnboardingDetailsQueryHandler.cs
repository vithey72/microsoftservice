﻿namespace BNine.Application.Aggregates.Users.Queries.GetOnboardingDetails
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading;
    using System.Threading.Tasks;
    using AutoMapper;
    using AutoMapper.QueryableExtensions;
    using BNine.Application.Abstractions;
    using BNine.Application.Helpers;
    using BNine.Application.Interfaces;
    using BNine.Enums;
    using MediatR;
    using Microsoft.EntityFrameworkCore;

    public class GetOnboardingDetailsQueryHandler
        : AbstractRequestHandler, IRequestHandler<GetOnboardingDetailsQuery, OnboardingDetails>
    {
        private readonly IEnumerable<UserStatus> onboardingStatuses = new List<UserStatus> {
            UserStatus.PendingPersonalInformation,
            UserStatus.PendingAddress,
            UserStatus.PendingSSN,
            UserStatus.PendingDocumentVerification,
            UserStatus.PendingSummary,
            UserStatus.PendingAgreement
        };

        public GetOnboardingDetailsQueryHandler(IMediator mediator, IBNineDbContext dbContext, IMapper mapper, ICurrentUserService currentUser) : base(mediator, dbContext, mapper, currentUser)
        {
        }

        public async Task<OnboardingDetails> Handle(GetOnboardingDetailsQuery request, CancellationToken cancellationToken)
        {
            var onboardingInfo = await DbContext.Users
                .Where(x => x.Id == request.Id)
                .ProjectTo<OnboardingDetails>(Mapper.ConfigurationProvider)
                .FirstOrDefaultAsync(cancellationToken);

            if (!string.IsNullOrEmpty(onboardingInfo.Identity?.Number))
            {
                onboardingInfo.Identity.Number = StringHelper.TakeLast(onboardingInfo.Identity.Number, 4);
            }

            var stepsInfo = await DbContext.OnboardingEvents
                    .Where(x => x.UserId == request.Id)
                    .ToListAsync(cancellationToken);

            foreach (var status in onboardingStatuses)
            {
                var failedEvents = stepsInfo.Where(x => x.Step == status).Where(x => !x.Succeeded);

                var isSucceeded = stepsInfo.Where(x => x.Step == status).OrderByDescending(x => x.CreatedAt).FirstOrDefault()?.Succeeded == true;

                var isCurrent = status == onboardingInfo.UserStatus;

                onboardingInfo.StepsInfo
                    .Add(new StepInfo
                    {
                        Step = status,
                        IsCurrent = isCurrent,
                        SubmitTime = stepsInfo.Where(x => x.Step == status).OrderByDescending(x => x.CreatedAt).FirstOrDefault()?.CreatedAt,
                        Errors = isSucceeded || !failedEvents.Any() ? null : failedEvents.Select(x => x.Message)
                    });
            }

            return onboardingInfo;
        }
    }
}
