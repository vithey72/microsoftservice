﻿namespace BNine.Application.Aggregates.Users.Queries.GetOnboardingDetails
{
    using System;
    using MediatR;

    public class GetOnboardingDetailsQuery
        : IRequest<OnboardingDetails>
    {
        public GetOnboardingDetailsQuery(Guid id)
        {
            Id = id;
        }

        public Guid Id
        {
            get; set;
        }
    }
}
