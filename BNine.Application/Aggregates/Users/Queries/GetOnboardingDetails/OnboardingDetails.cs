﻿namespace BNine.Application.Aggregates.Users.Queries.GetOnboardingDetails
{
    using System;
    using System.Collections.Generic;
    using AutoMapper;
    using BNine.Application.Mappings;
    using BNine.Domain.Entities.User;
    using BNine.Enums;
    using Newtonsoft.Json;

    public class OnboardingDetails : IMapFrom<User>
    {
        [JsonProperty("stepsInfo")]
        public IList<StepInfo> StepsInfo
        {
            get; set;
        } = new List<StepInfo>();

        [JsonProperty("personalInformation")]
        public PersonalInformation PersonalInformation
        {
            get; set;
        }

        [JsonProperty("address")]
        public AddressInfo Address
        {
            get; set;
        }

        [JsonProperty("identity")]
        public Identity Identity
        {
            get; set;
        }

        [JsonProperty("proofOfIdentity")]
        public ProofOfIdentity ProofOfIdentity
        {
            get; set;
        }

        [JsonIgnore]
        public UserStatus UserStatus
        {
            get; set;
        }

        public void Mapping(Profile profile) => profile.CreateMap<User, OnboardingDetails>()
            .ForMember(d => d.PersonalInformation, opt => opt.MapFrom(s => s))
            .ForMember(d => d.Address, opt => opt.MapFrom(s => s.Address))
            .ForMember(d => d.Identity, opt => opt.MapFrom(s => s.Identity))
            .ForMember(d => d.ProofOfIdentity, opt => opt.MapFrom(s => s.Document))
            .ForMember(d => d.UserStatus, opt => opt.MapFrom(s => s.Status));
    }

    public class StepInfo
    {
        [JsonProperty("step")]
        public UserStatus Step
        {
            get; set;
        }

        [JsonProperty("submitTime")]
        public DateTime? SubmitTime
        {
            get; set;
        }

        [JsonProperty("errors")]
        public IEnumerable<string> Errors
        {
            get; set;
        }

        [JsonProperty("isCurrent")]
        public bool IsCurrent
        {
            get; set;
        }
    }

    public class PersonalInformation : IMapFrom<User>
    {
        [JsonProperty("firstName")]
        public string FirstName
        {
            get; set;
        }

        [JsonProperty("lastName")]
        public string LastName
        {
            get; set;
        }

        [JsonProperty("dateOfBirth")]
        public DateTime? DateOfBirth
        {
            get; set;
        }

        public void Mapping(Profile profile) => profile.CreateMap<User, PersonalInformation>()
            .ForMember(d => d.FirstName, opt => opt.MapFrom(s => s.FirstName))
            .ForMember(d => d.LastName, opt => opt.MapFrom(s => s.LastName))
            .ForMember(d => d.DateOfBirth, opt => opt.MapFrom(s => s.DateOfBirth));
    }

    public class AddressInfo : IMapFrom<Domain.Entities.Common.Address>
    {
        [JsonProperty("zipCode")]
        public string ZipCode
        {
            get; set;
        }

        [JsonProperty("city")]
        public string City
        {
            get; set;
        }

        [JsonProperty("state")]
        public string State
        {
            get; set;
        }

        [JsonProperty("address")]
        public string Address
        {
            get; set;
        }

        [JsonProperty("unit")]
        public string Unit
        {
            get; set;
        }

        public void Mapping(Profile profile) => profile.CreateMap<Domain.Entities.Common.Address, AddressInfo>()
            .ForMember(d => d.Address, opt => opt.MapFrom(s => s.AddressLine))
            .ForMember(d => d.City, opt => opt.MapFrom(s => s.City))
            .ForMember(d => d.State, opt => opt.MapFrom(s => s.State))
            .ForMember(d => d.ZipCode, opt => opt.MapFrom(s => s.ZipCode))
            .ForMember(d => d.Unit, opt => opt.MapFrom(s => s.Unit));
    }

    public class Identity : IMapFrom<Domain.Entities.User.Identity>
    {
        [JsonProperty("identityType")]
        public IdentityType IdentityType
        {
            get; set;
        }

        [JsonProperty("number")]
        public string Number
        {
            get; set;
        }

        public void Mapping(Profile profile) => profile.CreateMap<Domain.Entities.User.Identity, Identity>()
           .ForMember(d => d.IdentityType, opt => opt.MapFrom(s => s.Type))
           .ForMember(d => d.Number, opt => opt.MapFrom(s => s.Value));
    }

    public class ProofOfIdentity : IMapFrom<Domain.Entities.User.Document>
    {
        [JsonProperty("jobId")]
        public string JobId
        {
            get; set;
        }

        [JsonProperty("proofOfIdentityKey")]
        public string ProofOfIdentityKey
        {
            get; set;
        }

        [JsonProperty("documentNumber")]
        public string DocumentNumber
        {
            get; set;
        }

        [JsonProperty("issuingState")]
        public string IssuingState
        {
            get; set;
        }

        [JsonProperty("issuingCountry")]
        public string IssuingCountry
        {
            get; set;
        }

        public void Mapping(Profile profile) => profile.CreateMap<Domain.Entities.User.Document, ProofOfIdentity>()
           .ForMember(d => d.JobId, opt => opt.MapFrom(s => s.VouchedJobId))
           .ForMember(d => d.DocumentNumber, opt => opt.MapFrom(s => s.Number))
           .ForMember(d => d.IssuingState, opt => opt.MapFrom(s => s.IssuingState))
           .ForMember(d => d.IssuingCountry, opt => opt.MapFrom(s => s.IssuingCountry))
           .ForMember(d => d.ProofOfIdentityKey, opt => opt.MapFrom(s => s.TypeKey))
           ;
    }
}
