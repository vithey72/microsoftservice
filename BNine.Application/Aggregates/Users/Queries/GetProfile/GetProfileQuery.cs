﻿namespace BNine.Application.Aggregates.Users.Queries.GetProfile
{
    using BNine.Application.Aggregates.Users.Models;
    using MediatR;

    public class GetProfileQuery : IRequest<Profile>
    {
    }
}
