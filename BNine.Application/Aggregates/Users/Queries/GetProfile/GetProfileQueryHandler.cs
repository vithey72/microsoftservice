﻿namespace BNine.Application.Aggregates.Users.Queries.GetProfile
{
    using System.Linq;
    using System.Threading;
    using System.Threading.Tasks;
    using Abstractions;
    using AutoMapper;
    using AutoMapper.QueryableExtensions;
    using BNine.Application.Aggregates.CreditScore.Models;
    using BNine.Application.Aggregates.Users.Models;
    using BNine.Application.Helpers;
    using BNine.Enums.TariffPlan;
    using Cards.Queries.GetMostRecentCard;
    using Collections;
    using Configuration.Queries.GetConfiguration;
    using Constants;
    using Domain.Entities.User;
    using Enums;
    using Exceptions;
    using Interfaces;
    using MediatR;
    using Microsoft.EntityFrameworkCore;
    using Profile = Models.Profile;

    public class GetProfileQueryHandler
        : AbstractRequestHandler
        , IRequestHandler<GetProfileQuery, Profile>
    {
        private readonly IPartnerProviderService _partnerProvider;
        private readonly ITariffPlanService _tariffPlanService;
        private readonly ICheckCreditScorePaidService _creditScorePaidService;
        private readonly IClientAccountSettingsProvider _clientAccountSettingsProvider;

        public GetProfileQueryHandler(
            IMediator mediator,
            IBNineDbContext dbContext,
            IMapper mapper,
            ICurrentUserService currentUser,
            ITariffPlanService tariffPlanService,
            IPartnerProviderService partnerProvider,
            ICheckCreditScorePaidService creditScorePaidService,
            IClientAccountSettingsProvider clientAccountSettingsProvider
            )
            : base(mediator, dbContext, mapper, currentUser)
        {
            _partnerProvider = partnerProvider;
            _tariffPlanService = tariffPlanService;
            _creditScorePaidService = creditScorePaidService;
            _clientAccountSettingsProvider = clientAccountSettingsProvider;
        }

        public async Task<Profile> Handle(GetProfileQuery request, CancellationToken cancellationToken)
        {
            var profile = await DbContext
                .Users
                .Where(x => x.Id == CurrentUser.UserId)
                .ProjectTo<Profile>(Mapper.ConfigurationProvider)
                .AsNoTracking()
                .FirstOrDefaultAsync(cancellationToken);

            if (profile == null)
            {
                throw new NotFoundException(nameof(User), CurrentUser.UserId);
            }

            var card = await Mediator.Send(new GetMostRecentDebitCardQuery(null, null), cancellationToken);

            profile.IsCardDelivered = card is not null && CardDeliveredStatuses.Contains(card.ShippingStatus);
            profile.SSN = StringHelper.TakeLast(profile.SSN, 4) ?? string.Empty;
            profile.ITIN = StringHelper.TakeLast(profile.ITIN, 4) ?? string.Empty;

            await AddTariffPlanInformation(profile, cancellationToken);

            await ApplyChristmasFeature(cancellationToken, profile);

            profile.IsCreditScoreServicePurchased = await IsCreditScorePaidServiceEnabled(cancellationToken);

            await UpdateDetailedStatusForBlockedState(profile, cancellationToken);
            await FillCustomErrorMessageIfNeeded(profile, cancellationToken);

            return profile;
        }

        private async Task UpdateDetailedStatusForBlockedState(Profile profile, CancellationToken cancellationToken)
        {
            var user = await DbContext
                .Users
                .Where(x => x.Id == profile.Id)
                .AsNoTracking()
                .FirstOrDefaultAsync(cancellationToken);

            if (user.IsBlocked && user.StatusDetailed != UserStatusDetailed.PendingClosure)
            {
                profile.StatusDetailed = UserStatusDetailed.Blocked;
            }
        }

        private async Task FillCustomErrorMessageIfNeeded(Profile profile, CancellationToken cancellationToken)
        {
            string customErrorMessage;

            if (profile.StatusDetailed == UserStatusDetailed.Blocked)
            {
                customErrorMessage = $"Your account has been deactivated. If you believe this is in error, " +
                    "contact support@bnine.com";
            }
            else if (profile.StatusDetailed == UserStatusDetailed.PendingClosure)
            {
                var settings = await _clientAccountSettingsProvider.GetClientAccountSettings(cancellationToken);
                customErrorMessage = $"Your account is blocked and is scheduled to close in {settings.VoluntaryClosureDelayInDays}" +
                    " days if no new transaction posted";
            }
            else if (profile.StatusDetailed == UserStatusDetailed.Closed)
            {
                customErrorMessage = $"Looks like your account's been closed. If you feel it's an error, " +
                    "contact us at support@bnine.com";
            }
            else
            {
                return;
            }

            profile.CustomErrorMessage = StringProvider.ReplaceCustomizedStrings(customErrorMessage, _partnerProvider);
        }

        private async Task AddTariffPlanInformation(Profile profile, CancellationToken cancellationToken)
        {
            await ActivateTariffsIfNecessary();

            var activeTariffModel = await _tariffPlanService.GetCurrentTariffModel(
                CurrentUser.UserId.Value,
                cancellationToken);
            if (activeTariffModel is null)
            {
                return;
            }
            profile.TariffPlan = await CreateTariffResponseModel(activeTariffModel);
        }

        private async Task<UserTariffPlanModel> CreateTariffResponseModel(IUserTariffModel activeTariffModel)
        {
            var userSetting = await DbContext.Users
                .Select(x => new { x.Id, x.Settings.InitialTariffIsPurchased })
                .AsNoTracking()
                .FirstOrDefaultAsync(x => x.Id == CurrentUser.UserId);
            var activeTariff = activeTariffModel.Entity;

            var name = activeTariff.Name;
            var shortName = activeTariff.ShortName;
            if (_partnerProvider.GetPartner() == PartnerApp.MBanqApp)
            {
                name = StringProvider.ReplaceCustomizedStrings(activeTariff.Name, _partnerProvider);
                shortName = activeTariff.Type == TariffPlanFamily.Advance
                    ? "Basic Plan"
                    : "Premium Plan";
            }

            var tariffResponseModel = new UserTariffPlanModel
            {
                Id = activeTariff.Id,
                Name = name,
                ShortName = shortName,
                Type = activeTariff.Type,
                MonthlyFee = activeTariff.MonthlyFee,
                InitialTariffIsPurchased = _partnerProvider.GetPartner() is PartnerApp.Qorbis ? userSetting.InitialTariffIsPurchased : null
            };

            tariffResponseModel.Title = activeTariffModel.GetTariffTitle();
            tariffResponseModel.PriceText = activeTariffModel.GetTariffPriceText();
            tariffResponseModel.ImageUrl = activeTariffModel.GetTariffImageUrl();

            return tariffResponseModel;
        }

        private async Task ActivateTariffsIfNecessary()
        {
            var tariffsCount = await DbContext
                .UserTariffPlans
                .Where(x => x.UserId == CurrentUser.UserId)
                .CountAsync();

            if (tariffsCount == 0)
            {
                await _tariffPlanService.ActivateStarterTariffPlan(CurrentUser.UserId!.Value, CancellationToken.None);
            }
        }

        private async Task ApplyChristmasFeature(CancellationToken cancellationToken, Profile profile)
        {
            var configuration = await Mediator.Send(new GetConfigurationQuery(), cancellationToken);
            if (configuration.HasFeatureEnabled(FeaturesNames.Features.ChristmasTime)
                && profile.Status == UserStatus.Active)
            {
                profile.FirstName += " 🎄☃️";
            }
        }

        private Task<bool> IsCreditScorePaidServiceEnabled(CancellationToken cancellationToken)
        {
            return _creditScorePaidService.IsAlreadyPurchased(CurrentUser.UserId.Value, cancellationToken);
        }
    }
}
