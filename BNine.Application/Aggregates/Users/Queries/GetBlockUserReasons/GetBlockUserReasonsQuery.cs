﻿namespace BNine.Application.Aggregates.Users.Queries.GetBlockUserReasons
{
    using System.Collections.Generic;
    using MediatR;

    public class GetBlockUserReasonsQuery
        : IRequest<IEnumerable<BlockUserReasonListItem>>
    {
    }
}
