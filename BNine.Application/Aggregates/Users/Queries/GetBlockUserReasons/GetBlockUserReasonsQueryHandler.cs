﻿namespace BNine.Application.Aggregates.Users.Queries.GetBlockUserReasons
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading;
    using System.Threading.Tasks;
    using AutoMapper;
    using AutoMapper.QueryableExtensions;
    using BNine.Application.Abstractions;
    using BNine.Application.Interfaces;
    using MediatR;
    using Microsoft.EntityFrameworkCore;

    public class GetBlockUserReasonsQueryHandler
        : AbstractRequestHandler, IRequestHandler<GetBlockUserReasonsQuery, IEnumerable<BlockUserReasonListItem>>
    {
        public GetBlockUserReasonsQueryHandler(IMediator mediator, IBNineDbContext dbContext, IMapper mapper, ICurrentUserService currentUser) : base(mediator, dbContext, mapper, currentUser)
        {
        }

        public async Task<IEnumerable<BlockUserReasonListItem>> Handle(GetBlockUserReasonsQuery request, CancellationToken cancellationToken)
        {
            var reasons = await DbContext.UserBlockReasons
                .OrderBy(x => x.Order)
                .ProjectTo<BlockUserReasonListItem>(Mapper.ConfigurationProvider)
                .ToListAsync(cancellationToken);

            return reasons;
        }
    }
}
