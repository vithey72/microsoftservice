﻿namespace BNine.Application.Aggregates.Users.Queries.GetBlockUserReasons
{
    using System;
    using AutoMapper;
    using BNine.Application.Mappings;
    using BNine.Domain.Entities.User;
    using Newtonsoft.Json;

    public class BlockUserReasonListItem : IMapFrom<BlockUserReason>
    {
        [JsonProperty("id")]
        public Guid Id
        {
            get; set;
        }

        [JsonProperty("value")]
        public string Value
        {
            get; set;
        }

        [JsonProperty("order")]
        public int Order
        {
            get; set;
        }

        public void Mapping(Profile profile) =>
            profile.CreateMap<BlockUserReason, BlockUserReasonListItem>()
            .ForMember(d => d.Id, opt => opt.MapFrom(s => s.Id))
            .ForMember(d => d.Value, opt => opt.MapFrom(s => s.Value))
            .ForMember(d => d.Order, opt => opt.MapFrom(s => s.Order));
    }
}
