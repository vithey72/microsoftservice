﻿namespace BNine.Application.Aggregates.Users.Queries.ConfirmSummaryInformationNew
{
    using Abstractions;
    using AutoMapper;
    using BNine.Settings;
    using CommonModels.Dialog;
    using Constants;
    using Domain.Entities.User;
    using Domain.Events.Users;
    using Emails.Commands.Registration.Denied;
    using Enums;
    using Evaluation.Commands;
    using Evaluation.Models;
    using Exceptions;
    using Helpers;
    using Interfaces;
    using Interfaces.Socure;
    using MediatR;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.Extensions.Logging;
    using Microsoft.Extensions.Options;
    using Models;
    using Newtonsoft.Json;
    using ServiceBusEvents.Commands.PublishB9Event;

    public class ConfirmSummaryNewQueryHandler :
        AbstractRequestHandler,
        IRequestHandler<ConfirmSummaryNewQuery, EitherDialog<GenericSuccessDialog, GenericFailDialog>>
    {
        private const string SocureSandboxSampleDocumentNumber = "11223344";
        private const int MaximumEvaluationsPerUser = 3;

        private static readonly string[] StopCodesArray = {
            "R106", "R110", "R111", "R113", "R180", "R181", "R182", "R183", "R184", "R185", "R186", "R201", "R202",
            "R203", "R406", "R551", "R557", "R558", "R563", "R564", //"R822", "R825",
            "R841", "R901", "R907", "R909",
            "R911", "R913", "R922", "R927", "R930", "R932", "R933", "R947", "R976", "R977", "R978", "R979", "R980",
            "R998", "R999", "R827", "R836"
        };

        private readonly ISocureKycService _socureOnboardingService;
        private readonly IPartnerProviderService _partnerProvider;
        private readonly SocureSettings _socureOptions;

        private static readonly SocureOverrideRule[] DecisionOverrideRules =
        {
            new ("document-pattern-fail-should-be-resubmit",
                new[] { "R810" },
                StopCodesArray,
                KycStatusEnum.Resubmit),

            new ("selfie-fails-liveliness-check",
                new[] { "R834" },
                StopCodesArray,
                KycStatusEnum.Resubmit),

            new ("no-face-found-in-the-selfie-frame",
                new[] { "R857" },
                StopCodesArray,
                KycStatusEnum.Resubmit),

            new ("barcode-reading-fail",
                new[] { "R831" },
                StopCodesArray,
                KycStatusEnum.Resubmit),
        };

        public ConfirmSummaryNewQueryHandler(
            IMediator mediator,
            IBNineDbContext dbContext,
            IMapper mapper,
            ICurrentUserService currentUser,
            ILogger<ConfirmSummaryNewQueryHandler> logger,
            ISocureKycService socureOnboardingService,
            IOptions<SocureSettings> options,
            IPartnerProviderService partnerProvider
            )
            : base(mediator, dbContext, mapper, currentUser)
        {
            _socureOnboardingService = socureOnboardingService;
            _partnerProvider = partnerProvider;
            _socureOptions = options.Value;
        }

        public async Task<EitherDialog<GenericSuccessDialog, GenericFailDialog>> Handle(ConfirmSummaryNewQuery request, CancellationToken cancellationToken)
        {
            var user = DbContext
                .Users
                .Include(u => u.Document)
                .Include(u => u.Identity)
                .FirstOrDefault(x => x.Id == CurrentUser.UserId);

            if (user == null)
            {
                throw new NotFoundException(nameof(User), CurrentUser.UserId);
            }
            try
            {
                var (kycVerificationResult, failResult) = await GetKycResult(request, cancellationToken, user);
                if (user.Status != UserStatus.Declined)
                {
                    switch (kycVerificationResult.StatusEnum)
                    {
                        case KycStatusEnum.Approved:
                            if (user.Status == UserStatus.PendingSummary)
                            {
                                user.Status = UserStatus.PendingAgreement;
                                user.StatusDetailed = UserStatusDetailed.PendingAgreement;
                                user.DomainEvents.Add(new OnboardingStepSuccessEvent(user, UserStatus.PendingSummary, UserStatusDetailed.PendingSummary));
                                await SendStatusUpdatedEvent(user);
                            }
                            break;
                        case KycStatusEnum.Resubmit:
                            user.DomainEvents.Add(SocureFailEvent(user, "Socure status: " + SocureTransactionResponse.ResubmitStatus));
                            user.Status = UserStatus.PendingDocumentVerification;
                            user.StatusDetailed = UserStatusDetailed.PendingDocumentVerification;
                            user.Document = null;
                            await SendStatusUpdatedEvent(user);
                            await DbContext.SaveChangesAsync(cancellationToken);
                            return new EitherDialog<GenericSuccessDialog, GenericFailDialog>(
                                new GenericFailDialog
                                {
                                    ErrorCode = ApiResponseErrorCodes.NeedToRetry,
                                    Body = new GenericDialogBodyViewModel
                                    {
                                        Title = "Unfortunately, the images could not be recognized",
                                        Subtitle = "Please make sure that:\n" +
                                                   "• document is placed on a flat dark background\n" +
                                                   "• all information on the document, including the barcode, is visible, glare free and not blurred\n" +
                                                   "• document is fully in frame\n" +
                                                   "• no fingers or other foreign objects are in frame\n" +
                                                   "• full face must be visible, no shadows or clothing obscure the face",
                                        ButtonText = "TRY AGAIN",
                                    },
                                });
                        case KycStatusEnum.Denied:
                        case KycStatusEnum.ManualReview:
                            user.DomainEvents.Add(SocureFailEvent(user, "Socure status: " + SocureTransactionResponse.DeniedStatus));
                            await RejectUser(user, cancellationToken);
                            break;
                        case KycStatusEnum.Unknown:
                        default:
                            throw new NotImplementedException("Unexpected Socure decision: " + kycVerificationResult.Status);
                    }
                }
                else
                {
                    await DbContext.SaveChangesAsync(cancellationToken);
                    return new EitherDialog<GenericSuccessDialog, GenericFailDialog>(failResult);
                }

                await DbContext.SaveChangesAsync(cancellationToken);
            }
            catch (Exception ex)
            {
                user.DomainEvents.Add(SocureFailEvent(user, ex.Message));
                await DbContext.SaveChangesAsync(cancellationToken);
                throw;
            }

            return new EitherDialog<GenericSuccessDialog, GenericFailDialog>(new GenericSuccessDialog());
        }

        private async Task<(KYCVerificationResult, GenericFailDialog)> GetKycResult(ConfirmSummaryNewQuery request, CancellationToken cancellationToken, User user)
        {
            var existingKycVerificationResults = await DbContext.KYCVerificationResults
                .OrderByDescending(x => x.CreatedAt)
                .Where(x => x.UserId == CurrentUser.UserId)
                .ToListAsync(cancellationToken);
            var latestEvaluation = existingKycVerificationResults.FirstOrDefault();
            if (latestEvaluation != null && latestEvaluation.StatusEnum != KycStatusEnum.Resubmit)
            {
                return (latestEvaluation, null);
            }

            return await EvaluateUserAndUpdateDocument(existingKycVerificationResults, user,
                request.SigmaDeviceSessionId, cancellationToken);
        }

        private async Task<(KYCVerificationResult, GenericFailDialog)> EvaluateUserAndUpdateDocument(List<KYCVerificationResult> existingKycVerificationResults,
            User user, string deviceSessionId, CancellationToken token)
        {
            var evaluationModel = BuildTransactionRequest(user, deviceSessionId);
            var idPlusResponse = await _socureOnboardingService.PerformKycTransaction(evaluationModel, token);

            var kycResult = new KYCVerificationResult
            {
                UserId = user.Id,
                EvaluationProvider = EvaluationProviderEnum.Socure,
                EvaluationExternalId = idPlusResponse.ReferenceId,
                Status = idPlusResponse.Decision?.Value,
                StatusEnum = idPlusResponse.DecisionEnum,
                RawEvaluation = idPlusResponse.RawResult,
                CreatedAt = DateTime.Now,
                EntityToken = "none",
                EvaluationToken = "none",
            };

            user.KycVerificationResults.Add(kycResult);

            if (idPlusResponse.DeviceData.IsIos() && idPlusResponse.DocVerification.DecisionEnum == KycStatusEnum.Denied
             || idPlusResponse.DeviceData.IsAndroid() && idPlusResponse.DecisionEnum == KycStatusEnum.Denied)
            {
                OverrideSocureDecisionIfApplicable(kycResult, idPlusResponse);
            }

            if (kycResult.StatusEnum == KycStatusEnum.Resubmit)
            {
                var attemptsCount = existingKycVerificationResults.Count + 1;
                if (attemptsCount >= MaximumEvaluationsPerUser)
                {
                    user.DomainEvents.Add(SocureFailEvent(user, $"Document could not be parsed after {attemptsCount} attempts."));
                    await RejectUser(user, token);
                    await DbContext.SaveChangesAsync(token);
                    await Mediator.Send(new PublishB9EventCommand(kycResult,
                        ServiceBusTopics.B9KYCVerifications,
                        ServiceBusEvents.B9Created), token);

                    return new(null, new GenericFailDialog
                    {
                        ErrorCode = ApiResponseErrorCodes.TooManyTries,
                        Body = new GenericDialogBodyViewModel
                        {
                            Subtitle = "This document could not be processed due to too many attempts. " +
                                       $"Please contact us at {StringProvider.GetSupportEmail(_partnerProvider)}.",
                            ButtonText = "OK",
                        },
                    });
                }

                await DbContext.SaveChangesAsync(token);
                await Mediator.Send(new PublishB9EventCommand(kycResult,
                    ServiceBusTopics.B9KYCVerifications,
                    ServiceBusEvents.B9Created), token);

                return (kycResult, null);
            }

            var problem = await AddMissingDocumentDetails(user, idPlusResponse, token);
            await DbContext.SaveChangesAsync(token);
            await Mediator.Send(new PublishB9EventCommand(kycResult,
                ServiceBusTopics.B9KYCVerifications,
                ServiceBusEvents.B9Created), token);

            return (kycResult, problem);
        }

        private SocureTransactionRequest BuildTransactionRequest(User user, string deviceSessionId)
        {
            var request = new SocureTransactionRequest
            {
                FirstName = user.FirstName,
                DeviceSessionId = deviceSessionId,
                LastName = user.LastName,
                DateOfBirth = user.DateOfBirth!.Value.ToString(DateFormat.SocureDate),
                NationalId = user.Identity.Value,
                Email = user.Email,
                MobileNumber = user.Phone,
                PhysicalAddress = user.Address.AddressLine,
                PhysicalAddress2 = user.Address.Unit,
                City = user.Address.City,
                State = user.Address.State,
                Zip = user.Address.ZipCode,
                UserConsent = true,
                ConsentTimestamp = DateTime.Now.ToUniversalIso8601(),
                DocumentId = user.Document.DocumentScansExternalId ?? Guid.Empty,
                DriverLicense = user.Document.Number,
                DriverLicenseState = user.Document.IssuingState,
            };

            return request;
        }

        private void OverrideSocureDecisionIfApplicable(KYCVerificationResult kycResult, SocureTransactionResponse idPlusResponse)
        {
            if (!_socureOptions.OverrideSocureDecisions)
            {
                return;
            }

            foreach (var rule in DecisionOverrideRules)
            {
                if (!rule.TryOverride(idPlusResponse, out var newOutcome))
                {
                    continue;
                }

                kycResult.OverridenByRule = rule.RuleName;
                kycResult.StatusEnum = newOutcome;
                kycResult.Status = newOutcome switch
                {
                    KycStatusEnum.Approved => SocureTransactionResponse.ApprovedStatus,
                    KycStatusEnum.Denied => SocureTransactionResponse.DeniedStatus,
                    KycStatusEnum.Resubmit => SocureTransactionResponse.ResubmitStatus,
                    _ => "unknown",
                };
                break;
            }
        }

        private async Task RejectUser(User user, CancellationToken cancellationToken)
        {
            user.Status = UserStatus.Declined;
            user.StatusDetailed = UserStatusDetailed.Declined;
            await Mediator.Send(new RegistrationDeniedEmailCommand { UserId = user.Id }, cancellationToken);
            await SendStatusUpdatedEvent(user);
        }

        private async Task SendStatusUpdatedEvent(User user)
        {
            await Mediator.Send(new PublishB9EventCommand(
                user,
                ServiceBusTopics.B9User,
                ServiceBusEvents.B9Updated,
                new Dictionary<string, object>
                {
                    {
                        ServiceBusDefaults.UpdatedFieldsAdditionalProperty,
                        JsonConvert.SerializeObject(new List<string> { nameof(user.Status) })
                    }
                }));
        }

        private async Task<GenericFailDialog> AddMissingDocumentDetails(User user, SocureTransactionResponse response, CancellationToken token)
        {
            var country = response.DocVerification.DocType.Country;
            var state = response.DocVerification.DocType.State;
            var docNumber = response.DocVerification.DocData.DocumentNumber;
            user.Document.TypeKey = response.DocVerification.DocType.TypeParsed.ToString();
            user.Document.IssuingCountry = country;
            user.Document.IssuingState = state;

            if (response.DocVerification.DocData != null)
            {
                if (docNumber == SocureSandboxSampleDocumentNumber)
                {
                    docNumber = new Random().Next(10000, 100000000).ToString();
                }

                var existingUserWithDocument = await DbContext
                    .Users
                    .Include(u => u.Document)
                    .FirstOrDefaultAsync(u =>
                            u.Id != user.Id &&
                            !string.IsNullOrEmpty(docNumber) && u.Document.Number.Trim() == docNumber &&
                            // different USA states may have colliding document numbers
                            !string.IsNullOrEmpty(state) && u.Document.IssuingState == state,
                        token);

                if (existingUserWithDocument?.Document == null)
                {
                    // might force replace user's first name as well
                    user.Document.Number = docNumber;
                    await DbContext.SaveChangesAsync(token);
                }
                else
                {
                    user.DomainEvents.Add(SocureFailEvent(user,
                        $"Document number collision: {docNumber} {country} {state}. Colliding user's ID: {existingUserWithDocument.Id}"));
                    await RejectUser(user, token);
                    user.Document = null;
                    await DbContext.SaveChangesAsync(token);
                    return new GenericFailDialog
                    {
                        ErrorCode = ApiResponseErrorCodes.DuplicateKey,
                        Body = new GenericDialogBodyViewModel
                        {
                            Subtitle = "This document is already used by another user who either is registered successfully or was denied registration." +
                                       $" We can quickly rectify this if you contact our Customer Support team ({StringProvider.GetSupportEmail(_partnerProvider)}).",
                            ButtonText = "OK",
                        },
                    };
                }
            }
            else if (string.IsNullOrEmpty(user.Document.Number))
            {
                throw new NotImplementedException("This is not supposed to happen.");
            }

            return null;
        }

        private static OnboardingStepFailedEvent SocureFailEvent(User user, string text) =>
            new(user, UserStatus.PendingSummary, UserStatusDetailed.PendingSummary, new List<string> { $"{text}" });

    }
}
