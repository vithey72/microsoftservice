﻿namespace BNine.Application.Aggregates.Users.Queries.ConfirmSummaryInformationNew;

using Behaviours;
using CommonModels.Dialog;
using MediatR;
using Newtonsoft.Json;

[Sequential]
public class ConfirmSummaryNewQuery : IRequest<EitherDialog<GenericSuccessDialog, GenericFailDialog>>
{
    /// <summary>
    /// Socure Sigma Device Id associated with user's request.
    /// </summary>
    public string SigmaDeviceSessionId
    {
        get;
        set;
    }

    [JsonIgnore]
    public string IpAddress
    {
        get; set;
    }
}
