﻿namespace BNine.Application.Aggregates.Users.Queries.GetAdvancesHistory;

using MediatR;
using Models.UserAdvancesHistory;

public record GetAdvancesHistoryQuery(Guid UserId) : IRequest<AdvancesHistoryViewModel>;
