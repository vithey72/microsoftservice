﻿namespace BNine.Application.Aggregates.Users.Queries.GetAdvancesHistory;

using Enums;
using Interfaces.DwhIntegration;
using MediatR;
using Models.UserAdvancesHistory;

public class GetAdvancesHistoryQueryHandler : IRequestHandler<GetAdvancesHistoryQuery, AdvancesHistoryViewModel>
{
    private readonly IDwhIntegrationService _dwhIntegrationService;

    public GetAdvancesHistoryQueryHandler(IDwhIntegrationService dwhIntegrationService)

    {
        _dwhIntegrationService = dwhIntegrationService;
    }

    public async Task<AdvancesHistoryViewModel> Handle(GetAdvancesHistoryQuery request,
        CancellationToken cancellationToken)
    {
        var res = await _dwhIntegrationService.GetAdvancesHistory(request.UserId);
        foreach (var entry in res.AdvancesHistoryEntries)
        {
            if (entry.ClosedDate == null)
            {
                if (entry.DueDate != null && DateTime.UtcNow.Date > entry.DueDate.Value.Date)
                {
                    entry.Status = AdvanceAdminStatusEnum.Overdue;
                }
                else
                {
                    entry.Status = AdvanceAdminStatusEnum.Active;
                }
            }
            else
            {
                if (entry.DueDate != null && entry.ClosedDate.Value.Date > entry.DueDate.Value.Date)
                {
                    entry.Status = AdvanceAdminStatusEnum.ClosedWithOverdue;
                }
                else
                {
                    entry.Status = AdvanceAdminStatusEnum.Closed;
                }
            }
        }

        return res;
    }


}
