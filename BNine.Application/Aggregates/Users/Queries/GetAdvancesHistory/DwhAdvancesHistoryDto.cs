﻿namespace BNine.Application.Aggregates.Users.Queries.GetAdvancesHistory;

using Newtonsoft.Json;

public class DwhAdvancesHistoryDto
{
    [JsonProperty("Advance")]
    public List<DwhAdvancesHistoryEntry> AdvancesHistoryEntries
    {
        get;
        set;
    } = new List<DwhAdvancesHistoryEntry>();
}

public class DwhAdvancesHistoryEntry
{
    public int LoanId
    {
        get; set;
    }

    public DateTime? DisbursedAt
    {
        get; set;
    }

    public double? AmountUsed
    {
        get; set;
    }

    public double? OriginalAmount
    {
        get; set;
    }

    public double? Boost
    {
        get; set;
    }

    public double? RemainingAmount
    {
        get; set;
    }

    public DateTime? DueDate
    {
        get; set;
    }

    public DateTime? ClosedDate
    {
        get; set;
    }

    public int? Dpd
    {
        get; set;
    }
}

