﻿namespace BNine.Application.Aggregates.Users.Queries.GetUserName
{
    using System.Linq;
    using System.Threading;
    using System.Threading.Tasks;
    using AutoMapper;
    using BNine.Application.Abstractions;
    using BNine.Application.Interfaces;
    using Exceptions;
    using Helpers;
    using MediatR;
    using Microsoft.EntityFrameworkCore;

    public class GetUserNameQueryHandler
        : AbstractRequestHandler, IRequestHandler<GetUserNameQuery, string>
    {
        private readonly IPartnerProviderService _partnerProviderService;

        public GetUserNameQueryHandler(
            IMediator mediator,
            IBNineDbContext dbContext,
            IMapper mapper,
            ICurrentUserService currentUser,
            IPartnerProviderService partnerProviderService
            )
            :
            base(mediator, dbContext, mapper, currentUser)
        {
            _partnerProviderService = partnerProviderService;
        }

        public async Task<string> Handle(GetUserNameQuery request, CancellationToken cancellationToken)
        {
            var user = await DbContext.Users
                .Where(x => !x.IsBlocked)
                .Where(x => x.Status == Enums.UserStatus.Active)
                .Where(x => x.Phone == request.PhoneNumber)
                .Select(x => new { x.FirstName, x.LastName })
                .FirstOrDefaultAsync();

            if (user == null)
            {
                throw new NotFoundException($"Looks like this user is not a member of {StringProvider.GetPartnerName(_partnerProviderService)}");
            }

            return $"{user.FirstName} {user.LastName}";
        }
    }
}
