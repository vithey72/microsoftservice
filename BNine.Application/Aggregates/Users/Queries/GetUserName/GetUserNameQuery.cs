﻿namespace BNine.Application.Aggregates.Users.Queries.GetUserName
{
    using MediatR;
    using Newtonsoft.Json;

    public class GetUserNameQuery : IRequest<string>
    {
        [JsonProperty("phone")]
        public string PhoneNumber
        {
            get; set;
        }
    }
}
