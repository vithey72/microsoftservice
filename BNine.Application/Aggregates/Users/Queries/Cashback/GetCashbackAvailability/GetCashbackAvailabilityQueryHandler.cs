﻿namespace BNine.Application.Aggregates.Users.Queries.GetCashbackAvailability;

using AutoMapper;
using Cashback;
using Cashback.GetCashbackAvailability;
using Interfaces;
using Interfaces.ServiceBus;
using MediatR;
using Models;

public class GetCashbackAvailabilityQueryHandler
    : BaseCashbackQueryHandler,
      IRequestHandler<GetCashbackAvailabilityQuery, CashbackAvailabilityViewModel>
{
    public GetCashbackAvailabilityQueryHandler(
        IMediator mediator,
        IBNineDbContext dbContext,
        IMapper mapper,
        ICurrentUserService currentUser,
        IServiceBus serviceBus,
        IDateTimeProviderService dateTimeProvider
        )
        : base(mediator, dbContext, mapper, currentUser, serviceBus, dateTimeProvider)
    {
    }

    public async Task<CashbackAvailabilityViewModel> Handle(GetCashbackAvailabilityQuery request, CancellationToken cancellationToken)
    {
        var (month, year) = base.InferCashbackMonthAndYear();
        var cashbackEntity = await base.GetCashbackEntity(month, year, cancellationToken);
        if (cashbackEntity == null)
        {
            return new CashbackAvailabilityViewModel
            {
                CashbackSwitch = new CashbackAvailabilityViewModel.CashbackSwitchViewModel
                {
                    IsEnabled = false,
                    Title = CashbackTitle,
                    Subtitle = ActivateYourCashback,
                }
            };
        }

        return base.ToActivatedViewModel(cashbackEntity);
    }
}
