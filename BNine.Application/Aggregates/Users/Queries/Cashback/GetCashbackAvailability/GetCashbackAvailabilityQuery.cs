﻿namespace BNine.Application.Aggregates.Users.Queries.Cashback.GetCashbackAvailability;

using MediatR;
using Models;

public class GetCashbackAvailabilityQuery : IRequest<CashbackAvailabilityViewModel>
{
}
