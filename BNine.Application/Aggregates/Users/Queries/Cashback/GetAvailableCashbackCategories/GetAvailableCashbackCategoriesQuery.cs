﻿namespace BNine.Application.Aggregates.Users.Queries.Cashback.GetAvailableCashbackCategories;

using MediatR;
using Models;

public class GetAvailableCashbackCategoriesQuery : IRequest<AvailableCashbackCategoriesResponse>
{
}
