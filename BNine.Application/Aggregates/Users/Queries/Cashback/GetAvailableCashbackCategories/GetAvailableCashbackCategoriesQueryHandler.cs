﻿namespace BNine.Application.Aggregates.Users.Queries.Cashback.GetAvailableCashbackCategories;

using Abstractions;
using AutoMapper;
using Interfaces;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Models;

public class GetAvailableCashbackCategoriesQueryHandler
    : AbstractRequestHandler
    , IRequestHandler<GetAvailableCashbackCategoriesQuery, AvailableCashbackCategoriesResponse>
{
    public GetAvailableCashbackCategoriesQueryHandler(
        IMediator mediator,
        IBNineDbContext dbContext,
        IMapper mapper,
        ICurrentUserService currentUser
        )
        : base(mediator, dbContext, mapper, currentUser)
    {
    }

    public async Task<AvailableCashbackCategoriesResponse> Handle(GetAvailableCashbackCategoriesQuery request, CancellationToken cancellationToken)
    {
        var categories = await DbContext.CashbackCategories
            .Where(cc => !cc.IsDisabled)
            .ToListAsync(cancellationToken);
        var settings = await DbContext.CashbackProgramSettings.SingleAsync(cancellationToken);
        return new AvailableCashbackCategoriesResponse
        {
            Categories = categories
                .Select(x => Mapper.Map<CashbackCategoryViewModel>(x))
                .OrderByDescending(x => x.Percentage)
                .ThenBy(x => x.ReadableName)
                .ToArray(),
            MinCashbackCategories = (byte)settings.MinCashbackCategories,
            MaxCashbackCategories = (byte)settings.MaxCashbackCategories,
        };
    }
}
