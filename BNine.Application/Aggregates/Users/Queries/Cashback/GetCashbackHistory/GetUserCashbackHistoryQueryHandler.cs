﻿namespace BNine.Application.Aggregates.Users.Queries;

using Abstractions;
using AutoMapper;
using Domain.Entities.User;
using Exceptions;
using Interfaces;
using Interfaces.DwhIntegration;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Models.UserCashback;

public class GetUserCashbackHistoryQueryHandler : AbstractRequestHandler,
    IRequestHandler<GetUserCashbackHistoryQuery, UserCashbackHistoryViewModel>
{
    private readonly IDwhIntegrationService _dwhIntegrationService;

    public GetUserCashbackHistoryQueryHandler(
        IMediator mediator, IBNineDbContext dbContext, IMapper mapper,
        ICurrentUserService currentUser,
        IDwhIntegrationService dwhIntegrationService) : base(
        mediator, dbContext, mapper, currentUser)
    {
        _dwhIntegrationService = dwhIntegrationService;
    }

    public async Task<UserCashbackHistoryViewModel> Handle(GetUserCashbackHistoryQuery request,
        CancellationToken cancellationToken)
    {
        var user = await DbContext.Users
            .FirstOrDefaultAsync(x => x.Id == request.UserId, cancellationToken: cancellationToken);

        if (user == null)
        {
            throw new NotFoundException(nameof(User), request.UserId);
        }

        var cashbackHistory = await _dwhIntegrationService.GetUserCashbackInfo(user.Id);

        return cashbackHistory;
    }
}
