﻿namespace BNine.Application.Aggregates.Users.Queries;

using MediatR;
using Models.UserCashback;

public class GetUserCashbackHistoryQuery : IRequest<UserCashbackHistoryViewModel>
{
    public Guid UserId
    {
        get;
        set;
    }
}
