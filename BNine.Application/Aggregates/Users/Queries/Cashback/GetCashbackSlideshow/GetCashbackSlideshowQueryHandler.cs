﻿namespace BNine.Application.Aggregates.Users.Queries.Cashback.GetCashbackSlideshow;

using Abstractions;
using AutoMapper;
using Helpers;
using Interfaces;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Models;
using Newtonsoft.Json;

public class GetCashbackSlideshowQueryHandler
    : AbstractRequestHandler,
      IRequestHandler<GetCashbackSlideshowQuery, CashbackProgramSlideshowViewModel>
{
    private readonly IPartnerProviderService _partnerProvider;

    public GetCashbackSlideshowQueryHandler(
        IMediator mediator,
        IBNineDbContext dbContext,
        IMapper mapper,
        ICurrentUserService currentUser,
        IPartnerProviderService partnerProvider
        )
        : base(mediator, dbContext, mapper, currentUser)
    {
        _partnerProvider = partnerProvider;
    }

    public async Task<CashbackProgramSlideshowViewModel> Handle(GetCashbackSlideshowQuery request, CancellationToken cancellationToken)
    {
        var slidesJson = (await DbContext.CashbackProgramSettings.SingleAsync(cancellationToken)).SlideshowScreens;

        var response = JsonConvert.DeserializeObject<CashbackProgramSlideshowViewModel>(slidesJson);
        foreach (var screen in response.Screens)
        {
            screen.Text = StringProvider.ReplaceCustomizedStrings(screen.Text, _partnerProvider);
            screen.LinkText = StringProvider.ReplaceCustomizedStrings(screen.LinkText, _partnerProvider);
        }

        return response;
    }
}
