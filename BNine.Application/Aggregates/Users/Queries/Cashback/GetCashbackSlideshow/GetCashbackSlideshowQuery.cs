﻿namespace BNine.Application.Aggregates.Users.Queries.Cashback.GetCashbackSlideshow;

using MediatR;
using Models;

public class GetCashbackSlideshowQuery : IRequest<CashbackProgramSlideshowViewModel>
{
}
