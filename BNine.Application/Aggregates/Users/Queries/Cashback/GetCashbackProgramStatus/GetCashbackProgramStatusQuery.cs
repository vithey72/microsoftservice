﻿namespace BNine.Application.Aggregates.Users.Queries.Cashback.GetCashbackProgramStatus;

using Abstractions;
using MediatR;
using Models;

public class GetCashbackProgramStatusQuery : MBanqSelfServiceUserRequest, IRequest<CashbackProgramStatusViewModel>
{
}
