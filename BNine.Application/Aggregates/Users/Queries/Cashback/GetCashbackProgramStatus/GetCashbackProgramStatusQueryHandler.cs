﻿namespace BNine.Application.Aggregates.Users.Queries.Cashback.GetCashbackProgramStatus;

using System.Globalization;
using Abstractions;
using AutoMapper;
using CheckingAccounts.Models;
using CheckingAccounts.Queries.GetFilteredTransfers;
using Domain.Entities.CashbackProgram;
using Enums.Transfers.Controller;
using Helpers;
using Interfaces;
using Interfaces.ServiceBus;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Models;

public class GetCashbackProgramStatusQueryHandler
    : BaseCashbackQueryHandler,
      IRequestHandler<GetCashbackProgramStatusQuery, CashbackProgramStatusViewModel>
{
    public GetCashbackProgramStatusQueryHandler(
        IMediator mediator,
        IBNineDbContext dbContext,
        IMapper mapper,
        ICurrentUserService currentUser,
        IServiceBus serviceBus,
        IDateTimeProviderService dateTimeProvider
    )
        : base(mediator, dbContext, mapper, currentUser, serviceBus, dateTimeProvider)
    {
    }

    public async Task<CashbackProgramStatusViewModel> Handle(GetCashbackProgramStatusQuery request, CancellationToken cancellationToken)
    {
        var now = DateTimeProvider.Now();
        var nextMonth = now.AddMonths(1);
        var user = await DbContext.Users
            .Include(u => u.CashbackStatistic)
            .FirstOrDefaultAsync(x => x.Id == CurrentUser.UserId!.Value, cancellationToken);

        if (user == null)
        {
            throw new KeyNotFoundException("User is not present in DB.");
        }


        user.CashbackStatistic ??= new UserCashbackStatistic
        {
            EarnedAmountUpdatedAt = DateTime.MinValue,
            TotalEarnedAmount = 0M,
        };

        var currentCashback = await ExtractUsedCashbackEntity(now);
        var nextMonthCashback = await ExtractUsedCashbackEntity(nextMonth);

        var response = new CashbackProgramStatusViewModel
        {
            TotalStatistics = new CashbackProgramStatusViewModel.CashbackTotalStatistics
            {
                Header = CurrencyFormattingHelper.AsRegular(user.CashbackStatistic.TotalEarnedAmount),
                Subtitle = "Earned in total",
            }
        };
        if (currentCashback != null
            && CashbackPaymentSoon()
            && currentCashback.EarnedAmount != Decimal.Zero)
        {
            response.TotalStatistics.Subtitle =
                $"{CurrencyFormattingHelper.AsRegular(currentCashback.EarnedAmount)} to be paid to you on {MonthName(DateTimeProvider.Now())} 5th";
        }

        await HideTransfersDetailsButtonIfNecessary(request, response);
        FillCurrentStatistics(currentCashback, response, now);
        await FillCurrentMonthCategories(cancellationToken, currentCashback, response, now);
        await FillUpcomingMonthCategories(cancellationToken, nextMonthCashback, response, nextMonth);

        return response;
    }

    private async Task HideTransfersDetailsButtonIfNecessary(MBanqSelfServiceUserRequest request,
        CashbackProgramStatusViewModel viewModel)
    {
        var query = new GetFilteredTransfersQuery
        {
            Filter = new GetFilteredTransfersExpandedFilter { SpecialType = B9SpecialBankTransferType.Cashback },
            Paging = new GetFilteredTransfersPagingParameters { PageSize = 1 },
            MbanqAccessToken = request.MbanqAccessToken,
            Ip = request.MbanqAccessToken,
        };
        var response = await Mediator.Send(query);

        viewModel.ShowTransfersDetailsButton = response.Items.Count != 0;
    }

    private bool CashbackPaymentSoon()
    {
        var day = DateTimeProvider.Now().Day;
        return day <= 4;
    }

    private void FillCurrentStatistics(UsedCashback currentCashback, CashbackProgramStatusViewModel response, DateTime now)
    {
        if (currentCashback == null)
        {
            // hide this block altogether
            return;
        }

        var header = $"{MonthName(now)} cashback: " + CurrencyFormattingHelper.AsRegular(currentCashback.EarnedAmount);
        if (currentCashback.SpentAmount >= Settings.MinimumSpentAmount)
        {
            response.CurrentStatistics = new CashbackProgramStatusViewModel.CashbackCurrentStatistics
            {
                Header = header,
                Subtitle = "You've spent enough to get cashback",
            };
        }
        else
        {
            var missingAmount = Settings.MinimumSpentAmount - currentCashback.SpentAmount;
            response.CurrentStatistics = new CashbackProgramStatusViewModel.CashbackCurrentStatistics
            {
                Header = header,
                Subtitle = $"Spend more than ${missingAmount} to activate cashback",
                SpentAmount = currentCashback.SpentAmount,
                GoalAmount = Settings.MinimumSpentAmount,
                MissingAmount = missingAmount,
            };
        }
    }

    private async Task FillCurrentMonthCategories(CancellationToken cancellationToken, UsedCashback currentCashback,
        CashbackProgramStatusViewModel response, DateTime now)
    {
        if (currentCashback != null)
        {
            if (currentCashback.CashbackCategoriesIds != null)
            {
                response.CurrentCategories = await GetResponseActiveCategories(currentCashback, now, cancellationToken);
            }
            else
            {
                // backwards compatibility (hide this block)
                response.CurrentCategories = null;
            }
        }
        else if (IsCurrentMonth())
        {
            response.CurrentCategories = GetResponseVacantCategories(now);
        }
    }

    private async Task FillUpcomingMonthCategories(CancellationToken cancellationToken, UsedCashback nextMonthCashback,
        CashbackProgramStatusViewModel response, DateTime nextMonth)
    {
        if (nextMonthCashback != null)
        {
            response.UpcomingCategories = await GetResponseActiveCategories(nextMonthCashback, nextMonth, cancellationToken);
        }
        else if (!IsCurrentMonth())
        {
            response.UpcomingCategories = GetResponseVacantCategories(nextMonth);
        }
    }

    private static string MonthName(DateTime dateTime)
    {
        return dateTime.ToString("MMMM", CultureInfo.InvariantCulture);
    }

    private async Task<UsedCashback> ExtractUsedCashbackEntity(DateTime dateTime)
    {
        var (month, year) = base.ExtractMonthAndYear(dateTime);
        var currentMonthCashback = await DbContext
            .UsedCashbacks
            .FirstOrDefaultAsync(uc => uc.UserId == CurrentUser.UserId!.Value
                                       && uc.Month == month
                                       && uc.Year == year);
        return currentMonthCashback;
    }

    private async Task<CashbackProgramStatusViewModel.CashbackMonthCategories> GetResponseActiveCategories(
        UsedCashback nextMonthCashback, DateTime dateTime, CancellationToken cancellationToken)
    {
        return new CashbackProgramStatusViewModel.CashbackMonthCategories
        {
            Header = GetYourCategoriesHeader(dateTime),
            Subtitle = YourCashbackCategories,
            SelectedCategories = await GetCategoriesViewModels(cancellationToken, nextMonthCashback),
        };
    }

    private static CashbackProgramStatusViewModel.CashbackMonthCategories GetResponseVacantCategories(DateTime dateTime)
    {
        return new CashbackProgramStatusViewModel.CashbackMonthCategories
        {
            Header = MonthName(dateTime),
            Subtitle = $"Activate your cashback for {MonthName(dateTime)} by selecting categories",
            CanSelectCategories = true,
        };
    }

    private async Task<CashbackCategoryViewModel[]> GetCategoriesViewModels(CancellationToken cancellationToken, UsedCashback currentCashback)
    {
        var categories = await DbContext
            .CashbackCategories
            .Where(cc => currentCashback.CashbackCategoriesIds != null
                         && currentCashback.CashbackCategoriesIds.Contains(cc.Id))
            .ToListAsync(cancellationToken);
        var viewModels = categories
            .Select(c => Mapper.Map<CashbackCategoryViewModel>(c))
            .OrderByDescending(c => c.Percentage)
            .ThenBy(c => c.Name)
            .ToArray();
        return viewModels;
    }

    private static string GetYourCategoriesHeader(DateTime dateTime)
    {
        return "Your " + MonthName(dateTime) + " cashback categories:";
    }
}
