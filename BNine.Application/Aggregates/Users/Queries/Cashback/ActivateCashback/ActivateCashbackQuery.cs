﻿namespace BNine.Application.Aggregates.Users.Queries.Cashback.ActivateCashback;

using System;
using MediatR;
using Models;

public class ActivateCashbackQuery : IRequest<CashbackAvailabilityViewModel>
{
    public string[] CashbackCategoriesNames
    {
        get;
        set;
    } = Array.Empty<string>();
}
