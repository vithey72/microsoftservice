﻿namespace BNine.Application.Aggregates.Users.Queries.Cashback.ActivateCashback;

using AutoMapper;
using Domain.Entities.CashbackProgram;
using Exceptions;
using Interfaces;
using Interfaces.ServiceBus;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Models;

public class ActivateCashbackQueryHandler
    : BaseCashbackQueryHandler,
      IRequestHandler<ActivateCashbackQuery, CashbackAvailabilityViewModel>
{
    public ActivateCashbackQueryHandler(
        IMediator mediator,
        IBNineDbContext dbContext,
        IMapper mapper,
        ICurrentUserService currentUser,
        IServiceBus serviceBus,
        IDateTimeProviderService dateTimeProvider
        )
        : base(mediator, dbContext, mapper, currentUser, serviceBus, dateTimeProvider)
    {
    }

    public async Task<CashbackAvailabilityViewModel> Handle(ActivateCashbackQuery request, CancellationToken cancellationToken)
    {
        var categories = await DbContext
            .CashbackCategories
            .Where(cc => !cc.IsDisabled && request.CashbackCategoriesNames.Contains(cc.Name))
            .ToArrayAsync(cancellationToken);

        var categoriesNames = categories
            .Select(x => x.Name)
            .ToArray();
        var categoriesNotPresent = request
            .CashbackCategoriesNames
            .Except(categoriesNames)
            .ToArray();

        if (categoriesNotPresent.Length != 0)
        {
            throw new NotFoundException("Some of the passed categories were not present in DB: " + string.Join(',', categoriesNotPresent));
        }

        var (month, year) = InferCashbackMonthAndYear();
        if (await base.GetCashbackEntity(month, year, cancellationToken) != null)
        {
            return null;
        }

        var cashbackEntry = new UsedCashback
        {
            UserId = CurrentUser.UserId!.Value,
            EnabledAt = DateTime.Now,
            ExpiredAt = GetExpirationDate(year, month),
            Month = month,
            Year = year,
            CashbackCategoriesIds = categories.Select(c => c.Id).ToArray(),
            CashbackCategoriesNames = categoriesNames,
        };

        var savedEntry = await DbContext.UsedCashbacks.AddAsync(cashbackEntry, cancellationToken);
        await DbContext.SaveChangesAsync(cancellationToken);

        await SendServiceBusMessage(savedEntry.Entity, true, false);

        return base.ToActivatedViewModel(savedEntry.Entity);
    }
}
