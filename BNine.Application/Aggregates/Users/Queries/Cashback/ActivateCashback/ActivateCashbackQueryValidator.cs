﻿namespace BNine.Application.Aggregates.Users.Queries.Cashback.ActivateCashback;

using FluentValidation;
using Interfaces;

public class ActivateCashbackQueryValidator
    : AbstractValidator<ActivateCashbackQuery>
{
    public ActivateCashbackQueryValidator(IBNineDbContext dbContext)
    {
        var settings = dbContext.CashbackProgramSettings.Single();

        RuleFor(x => x.CashbackCategoriesNames.Length)
            .GreaterThanOrEqualTo(settings.MinCashbackCategories)
            .WithMessage("Select at least 1 category")
            .LessThanOrEqualTo(settings.MaxCashbackCategories)
            .WithMessage("Select 4 categories or less");
    }
}
