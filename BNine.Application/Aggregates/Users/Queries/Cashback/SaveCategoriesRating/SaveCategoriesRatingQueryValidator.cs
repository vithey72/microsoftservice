﻿namespace BNine.Application.Aggregates.Users.Queries.Cashback.SaveCategoriesRating;

using FluentValidation;

public class SaveCategoriesRatingQueryValidator : AbstractValidator<SaveCategoriesRatingQuery>
{
    public SaveCategoriesRatingQueryValidator()
    {
        RuleFor(x => x.Id)
            .NotEmpty();

        RuleFor(x => x.Rating)
            .LessThanOrEqualTo(5)
            .WithMessage("Rating should be less than 5 stars")
            .GreaterThanOrEqualTo(1)
            .WithMessage("Rating should be at least 1 star");
    }
}
