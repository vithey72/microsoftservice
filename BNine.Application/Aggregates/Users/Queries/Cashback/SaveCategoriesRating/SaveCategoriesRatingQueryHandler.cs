﻿namespace BNine.Application.Aggregates.Users.Queries.Cashback.SaveCategoriesRating;

using System.Threading.Tasks;
using Abstractions;
using AutoMapper;
using Interfaces;
using MediatR;
using Microsoft.EntityFrameworkCore;

public class SaveCategoriesRatingQueryHandler
    : AbstractRequestHandler
        , IRequestHandler<SaveCategoriesRatingQuery, bool>
{
    public SaveCategoriesRatingQueryHandler(
        IMediator mediator,
        IBNineDbContext dbContext,
        IMapper mapper,
        ICurrentUserService currentUser
        )
        : base(mediator, dbContext, mapper, currentUser)
    {
    }

    /// <summary>
    /// Save user's rating of selected cashback categories. Returns flag of success.
    /// </summary>
    public async Task<bool> Handle(SaveCategoriesRatingQuery request, CancellationToken cancellationToken)
    {
        var entity = await DbContext
            .UsedCashbacks
            .FirstOrDefaultAsync(uc => uc.Id == request.Id
                                       && uc.UserId == CurrentUser.UserId,
                cancellationToken);

        if (entity == null || entity.UsersEvaluation != null) { return false; }

        entity.UsersEvaluation = request.Rating;

        await DbContext.SaveChangesAsync(cancellationToken);

        return true;
    }
}
