﻿namespace BNine.Application.Aggregates.Users.Queries.Cashback.SaveCategoriesRating;

using MediatR;

public class SaveCategoriesRatingQuery : IRequest<bool>
{
    public Guid Id
    {
        get;
        set;
    }

    public int Rating
    {
        get;
        set;
    }
}
