﻿namespace BNine.Application.Aggregates.Users.Queries.Cashback;

using Abstractions;
using AutoMapper;
using Constants;
using Domain.Entities.CashbackProgram;
using Domain.Entities.Settings;
using Interfaces;
using Interfaces.ServiceBus;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Models;
using Newtonsoft.Json;

public class BaseCashbackQueryHandler : AbstractRequestHandler
{

    public const string CashbackTitle = "Cashback";
    public const string ActivateYourCashback = "ACTIVATE";
    public const string YouHaveActivatedYourCashback = "ACTIVE";
    public const string YourCashbackCategories = "Your cashback categories";

    protected readonly IDateTimeProviderService DateTimeProvider;
    private readonly IServiceBus _serviceBus;

    protected CashbackProgramSettings Settings => DbContext.CashbackProgramSettings.Single();

    public BaseCashbackQueryHandler(
        IMediator mediator,
        IBNineDbContext dbContext,
        IMapper mapper,
        ICurrentUserService currentUser,
        IServiceBus serviceBus,
        IDateTimeProviderService dateTimeProvider
        )
        : base(mediator, dbContext, mapper, currentUser)
    {
        _serviceBus = serviceBus;
        DateTimeProvider = dateTimeProvider;
    }

    protected CashbackAvailabilityViewModel ToActivatedViewModel(UsedCashback entity) =>
        new()
        {
            CashbackSwitch = new CashbackAvailabilityViewModel.CashbackSwitchViewModel
            {
                IsEnabled = true,
                Title = CashbackTitle,
                Subtitle = YouHaveActivatedYourCashback,
                EntityId = entity.Id,
            }
        };

    protected (byte month, int year) InferCashbackMonthAndYear()
    {
        var now = DateTimeProvider.Now();
        if (IsCurrentMonth())
        {
            return ((byte)now.Month, now.Year);
        }

        if (now.Month != 12)
        {
            return ((byte)(now.Month + 1), now.Year);
        }

        return (1, now.Year + 1);
    }

    protected (byte month, int year) ExtractMonthAndYear(DateTime dateTime) => ((byte)dateTime.Month, dateTime.Year);

    protected bool IsCurrentMonth() => DateTimeProvider.Now().Day < 25; // may be subject to change

    protected async Task<UsedCashback> GetCashbackEntity(byte month, int year, CancellationToken cancellationToken, Guid? userId = null)
    {
        userId ??= CurrentUser.UserId;
        return await DbContext.UsedCashbacks.FirstOrDefaultAsync(uc =>
            uc.UserId == userId &&
            uc.Month == month &&
            uc.Year == year, cancellationToken);
    }

    protected DateTime GetExpirationDate(int year, byte month)
    {
        return new DateTime(year, month, DateTime.DaysInMonth(year, month), 23, 59, 59);
    }

    protected async Task SendServiceBusMessage(UsedCashback usedCashback, bool enabled, bool adminTriggered)
    {
        var body = JsonConvert.SerializeObject(
            new
            {
                userId = usedCashback.UserId,
                isCashbackEnabled = enabled,
                cashbackMonth = new DateTime(usedCashback.Year, usedCashback.Month, 1).ToString("yyyy-MM-dd"),
                triggeredBy = adminTriggered ? "admin" : "user",
                selectedCategoriesIds = usedCashback.CashbackCategoriesIds,
                selectedCategoriesNames = usedCashback.CashbackCategoriesNames,
            });
        await _serviceBus.Publish(new Application.Models.ServiceBus.IntegrationEvent
        {
            Body = body,
            EventName = ServiceBusEvents.B9CashbackMonth + "." + (enabled ? ServiceBusEvents.B9Created : ServiceBusEvents.B9Updated),
            TopicName = ServiceBusTopics.B9UserV2,
        });
    }
}
