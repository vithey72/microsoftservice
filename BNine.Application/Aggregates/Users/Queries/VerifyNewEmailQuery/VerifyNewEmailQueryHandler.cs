﻿namespace BNine.Application.Aggregates.Users.Queries.VerifyNewEmailQuery
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading;
    using System.Threading.Tasks;
    using Abstractions;
    using AutoMapper;
    using BNine.Application.Aggregates.ServiceBusEvents.Commands.PublishB9Event;
    using BNine.Constants;
    using Domain.Entities.User;
    using Emails.Commands.User.EmailUpdated;
    using Enums;
    using Helpers;
    using Interfaces;
    using Interfaces.Bank.Administration;
    using MediatR;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.Extensions.Logging;
    using Models;
    using Newtonsoft.Json;

    public class VerifyNewEmailQueryHandler
        : AbstractRequestHandler, IRequestHandler<VerifyNewEmailQuery, ConfirmUpdateEmailResponseModel>
    {
        private readonly INotificationUsersService _notificationUsersService;
        private readonly ISmsService _smsService;
        private readonly IBankUsersService _bankUsersService;
        private readonly IBankClientsService _bankClientsService;
        private readonly IPartnerProviderService _partnerProvider;
        private readonly ILogger<VerifyNewEmailQueryHandler> _logger;

        public VerifyNewEmailQueryHandler(
            INotificationUsersService notificationUsersService,
            ISmsService smsService,
            IMediator mediator,
            IBNineDbContext dbContext,
            IMapper mapper,
            ICurrentUserService currentUser,
            IBankUsersService bankUsersService,
            IBankClientsService bankClientsService,
            IPartnerProviderService partnerProvider,
            ILogger<VerifyNewEmailQueryHandler> logger
            )
            : base(mediator, dbContext, mapper, currentUser)
        {
            _notificationUsersService = notificationUsersService;
            _smsService = smsService;
            _bankUsersService = bankUsersService;
            _bankClientsService = bankClientsService;
            _partnerProvider = partnerProvider;
            _logger = logger;
        }

        public async Task<ConfirmUpdateEmailResponseModel> Handle(VerifyNewEmailQuery request, CancellationToken cancellationToken)
        {
            var changeEmailRequest = await DbContext.UpdateUserInfoRequests
                .Where(x => x.UserId == CurrentUser.UserId)
                .Where(x => x.Id == request.EmailChangeRequestId)
                .FirstOrDefaultAsync(cancellationToken);

            if (changeEmailRequest == null)
            {
                return new ConfirmUpdateEmailResponseModel
                {
                    IsEmailUpdated = false
                };
            }

            var user = DbContext.Users
                .Include(x => x.Devices)
                .FirstOrDefault(x => x.Id == changeEmailRequest.UserId);

            changeEmailRequest.Status = RequestProcessingStatus.COMPLETED;

            user.Email = changeEmailRequest.UpdatedFieldValue;

            await _bankUsersService.UpdateUserEmail(
                user.ExternalId,
                changeEmailRequest.UpdatedFieldValue);

            if (user.ExternalClientId != null)
            {
                await _bankClientsService.UpdateClientEmail(user.ExternalClientId.Value,
                    changeEmailRequest.UpdatedFieldValue);
            }

            await DbContext.SaveChangesAsync(cancellationToken);

            await Mediator.Send(new PublishB9EventCommand(
                     user,
                     ServiceBusTopics.B9User,
                     ServiceBusEvents.B9Updated,
                     new Dictionary<string, object>
                     {
                          { ServiceBusDefaults.UpdatedFieldsAdditionalProperty , JsonConvert.SerializeObject(new List<string> { nameof(user.Email) }) }
                          }));

            await NotifyUser(user);

            return new ConfirmUpdateEmailResponseModel
            {
                UserId = user.Id,
                IsEmailUpdated = true
            };
        }

        private async Task NotifyUser(User user)
        {
            var message = $"Your email associated with {StringProvider.GetPartnerName(_partnerProvider)} was changed. " +
                          $"If you did not do this, call {StringProvider.GetSupportPhone(_partnerProvider)} to lock your account. " +
                          $"The {StringProvider.GetSupportEmail(_partnerProvider)} Team";
            try
            {
                await _smsService.SendMessageAsync(user.Phone, message);

            }
            catch (Exception ex)
            {
                _logger.LogError($"Error when sending sms: {ex.Message}", ex);
            }

            try
            {
                await _notificationUsersService.SendNotification(
                    user.Id,
                    message,
                    user.Settings.IsNotificationsEnabled,
                    NotificationTypeEnum.Information,
                    true,
                    user.Devices,
                    null,
                    null,
                    PushNotificationCategory.YourEmailAssociatedWithB9WasChanged);

            }
            catch (Exception ex)
            {
                _logger.LogError($"Error when sending push notification: {ex.Message}", ex);
            }

            await Mediator.Send(new EmailUpdatedEmailCommand(user.Email, user.FirstName));
        }
    }
}
