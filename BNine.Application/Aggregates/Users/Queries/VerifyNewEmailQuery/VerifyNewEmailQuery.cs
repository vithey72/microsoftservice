﻿namespace BNine.Application.Aggregates.Users.Queries.VerifyNewEmailQuery
{
    using System;
    using BNine.Application.Aggregates.Users.Models;
    using MediatR;
    using Newtonsoft.Json;

    public class VerifyNewEmailQuery : IRequest<ConfirmUpdateEmailResponseModel>
    {
        [JsonProperty("emailChangeRequestId")]
        public Guid EmailChangeRequestId
        {
            get; set;
        }
    }
}
