﻿namespace BNine.Application.Aggregates.Users.Queries.GetUsers
{
    using System.Linq;
    using System.Threading;
    using System.Threading.Tasks;
    using AutoMapper;
    using AutoMapper.QueryableExtensions;
    using BNine.Application.Abstractions;
    using BNine.Application.Extensions.IQueryableExtensions;
    using BNine.Application.Interfaces;
    using BNine.Application.Models;
    using MediatR;

    public class GetUsersQueryHandler
        : AbstractRequestHandler, IRequestHandler<GetUsersQuery, PaginatedList<UserInfoListItem>>
    {
        public GetUsersQueryHandler(IMediator mediator, IBNineDbContext dbContext, IMapper mapper, ICurrentUserService currentUser) : base(mediator, dbContext, mapper, currentUser)
        {
        }

        public Task<PaginatedList<UserInfoListItem>> Handle(GetUsersQuery request, CancellationToken cancellationToken)
        {
            var usersQuery = DbContext.Users
                .OrderByDescending(x => x.CreatedAt)
                .FilterByStatuses(request.Statuses)
                .FilterBySearchString(request.Search)
                .ProjectTo<UserInfoListItem>(Mapper.ConfigurationProvider);

            var result = new PaginatedList<UserInfoListItem>(usersQuery, request.PageIndex, request.PageSize).AddOrder();

            return Task.FromResult(result);
        }
    }
}
