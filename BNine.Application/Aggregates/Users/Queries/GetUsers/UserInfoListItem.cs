﻿namespace BNine.Application.Aggregates.Users.Queries.GetUsers
{
    using System;
    using AutoMapper;
    using BNine.Application.Interfaces;
    using BNine.Application.Mappings;
    using BNine.Domain.Entities.User;
    using BNine.Enums;
    using Helpers;

    public class UserInfoListItem : IMapFrom<User>, IOrderedListItem
    {
        public Guid Id
        {
            get; set;
        }

        public string FirstName
        {
            get; set;
        }

        public string LastName
        {
            get; set;
        }

        public UserStatus Status
        {
            get; set;
        }

        public string PhoneNumber
        {
            get; set;
        }

        public string Email
        {
            get; set;
        }

        public long Order
        {
            get; set;
        }

        public string DocumentNumber
        {
            get; set;
        }

        public string IdentityNumber
        {
            get; set;
        }

        public void Mapping(Profile profile) => profile.CreateMap<User, UserInfoListItem>()
            .ForMember(d => d.Id, opt => opt.MapFrom(s => s.Id))
            .ForMember(d => d.FirstName, opt => opt.MapFrom(s => s.FirstName))
            .ForMember(d => d.Email, opt => opt.MapFrom(s => s.Email))
            .ForMember(d => d.LastName, opt => opt.MapFrom(s => s.LastName))
            .ForMember(d => d.PhoneNumber, opt => opt.MapFrom(s => s.Phone))
            .ForMember(d => d.DocumentNumber, opt => opt.MapFrom(s => s.Document.Number))
            .ForMember(d => d.IdentityNumber, opt => opt.MapFrom(s => StringHelper.TakeLast(s.Identity.Value, 4)))
            .ForMember(d => d.Status,
                opt => opt.MapFrom(s => s.IsBlocked && s.Status != UserStatus.Closed ? UserStatus.Blocked : s.Status));
    }
}
