﻿namespace BNine.Application.Aggregates.Users.Queries.GetUsers
{
    using System.Collections.Generic;
    using BNine.Application.Models.Requests;
    using BNine.Enums;

    public class GetUsersQuery : AbstractPaginatedQuery<UserInfoListItem>
    {
        public GetUsersQuery(string search, IEnumerable<UserStatus> statuses, int? pageIndex, int? pageSize) : base(pageIndex, pageSize)
        {
            Search = search;
            Statuses = statuses;
        }

        public string Search
        {
            get;
        }

        public IEnumerable<UserStatus> Statuses
        {
            get;
        }
    }
}
