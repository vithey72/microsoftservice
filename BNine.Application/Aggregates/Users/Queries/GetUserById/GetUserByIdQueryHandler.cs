﻿namespace BNine.Application.Aggregates.Users.Queries.GetUserById
{
    using System.Linq;
    using System.Threading;
    using System.Threading.Tasks;
    using AutoMapper;
    using BNine.Application.Abstractions;
    using GetUserGroups;
    using BNine.Application.Interfaces;
    using BNine.Application.Models.Dto;
    using MediatR;
    using Microsoft.EntityFrameworkCore;

    public class GetUserByIdQueryHandler
        : AbstractRequestHandler, IRequestHandler<GetUserByIdQuery, User>
    {
        public GetUserByIdQueryHandler(
            IMediator mediator,
            IBNineDbContext dbContext,
            IMapper mapper,
            ICurrentUserService currentUser)
            : base(mediator, dbContext, mapper, currentUser)
        {
        }

        public async Task<User> Handle(GetUserByIdQuery request, CancellationToken cancellationToken)
        {
            var user = DbContext.Users
                .Include(x => x.UserGroups)
                .FirstOrDefault(x => x.Id == request.Id);

            var groups = await Mediator.Send(new GetUserGroupsQuery(user.Id), cancellationToken);

            var result = new User
            {
                Id = user.Id,
                Status = user.IsBlocked ? Enums.UserStatus.Blocked : user.Status,
                Groups = groups.Select(ug => ug.Id).OrderBy(x => x).ToArray(),
            };

            return result;
        }
    }
}
