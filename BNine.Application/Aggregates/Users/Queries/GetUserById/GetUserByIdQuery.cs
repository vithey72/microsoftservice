﻿namespace BNine.Application.Aggregates.Users.Queries.GetUserById
{
    using System;
    using BNine.Application.Models.Dto;
    using MediatR;

    public class GetUserByIdQuery : IRequest<User>
    {
        public Guid Id
        {
            get; set;
        }
    }
}
