﻿namespace BNine.Application.Aggregates.Users.Queries.GetProfilePhoto
{
    using System.Linq;
    using System.Threading;
    using System.Threading.Tasks;
    using Abstractions;
    using AutoMapper;
    using BNine.Settings.Blob;
    using Constants;
    using Domain.Entities.User;
    using Exceptions;
    using Interfaces;
    using MediatR;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.Extensions.Options;

    public class GetProfilePhotoQueryHandler
        : AbstractRequestHandler, IRequestHandler<GetProfilePhotoQuery, byte[]>
    {
        private BlobStorageSettings BlobSettings
        {
            get;
        }

        private IBlobStorageService BlobStorageService
        {
            get;
        }

        public GetProfilePhotoQueryHandler(
            IOptions<BlobStorageSettings> options,
            IMediator mediator,
            IBNineDbContext dbContext,
            IMapper mapper,
            ICurrentUserService currentUser,
            IBlobStorageService blobStorageService
            ) : base(mediator, dbContext, mapper, currentUser)
        {
            BlobStorageService = blobStorageService;
            BlobSettings = options.Value;
        }

        public async Task<byte[]> Handle(GetProfilePhotoQuery request, CancellationToken cancellationToken)
        {
            var user = await DbContext.Users
                .Where(x => x.Id == CurrentUser.UserId)
                .Select(x => new { x.ProfilePhotoUrl })
                .FirstOrDefaultAsync(cancellationToken);

            if (user == null)
            {
                throw new NotFoundException(nameof(User), CurrentUser.UserId);
            }

            if (user.ProfilePhotoUrl == null)
            {
                throw new NotFoundException("Photo not found");
            }

            var response = await BlobStorageService.GetFile(ContainerNames.BnineContainer, user.ProfilePhotoUrl, cancellationToken);

            return response;
        }
    }
}
