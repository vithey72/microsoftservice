﻿namespace BNine.Application.Aggregates.Users.Queries.GetProfilePhoto
{
    using MediatR;

    public class GetProfilePhotoQuery : IRequest<byte[]>
    {
    }
}
