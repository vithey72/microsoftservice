﻿namespace BNine.Application.Aggregates.Users.Queries.CloseAccount.ValidateCloseClientAccount;

using Abstractions;
using AutoMapper;
using BNine.Application.Aggregates.Payroll;
using Commands.CloseAccount.MarkLoansAsClosedIfNeeded;
using Domain.Entities.User;
using Enums;
using Exceptions;
using Interfaces;
using Interfaces.Bank.Administration;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Models;

public class ValidateCloseClientAccountQueryHandler
    : AbstractRequestHandler
    , IRequestHandler<ValidateCloseClientAccountQuery, ClientAccountClosureValidationResult>
{
    private readonly IBankSavingAccountsService _bankSavingAccountsService;
    private readonly IPayrollConnectionsProvider _payrollConnectionsProvider;

    public ValidateCloseClientAccountQueryHandler(
        IMediator mediator,
        IBNineDbContext dbContext,
        IMapper mapper,
        ICurrentUserService currentUser,
        IBankSavingAccountsService bankSavingAccountsService,
        IPayrollConnectionsProvider payrollConnectionsProvider
        )
        : base(mediator, dbContext, mapper, currentUser)
    {
        _bankSavingAccountsService = bankSavingAccountsService;
        _payrollConnectionsProvider = payrollConnectionsProvider;
    }

    public async Task<ClientAccountClosureValidationResult> Handle(ValidateCloseClientAccountQuery request,
        CancellationToken cancellationToken)
    {
        var issuesList = new List<ClientAccountClosureResultCode>();
        var user = await DbContext.Users
            .Include(x => x.CurrentAccount)
            .Include(x => x.LoanAccounts)
            .FirstOrDefaultAsync(x => x.Id == request.UserId, cancellationToken);

        if (user == null)
        {
            throw new NotFoundException(nameof(User), CurrentUser.UserId);
        }

        if (user.Status == UserStatus.Closed)
        {
            throw new ValidationException("user",
                $"Account with id {request.UserId} is already closed");
        }

        if (user.CurrentAccount is not null)
        {
            var savingsAccount = await _bankSavingAccountsService.GetSavingAccountInfo(user.CurrentAccount.ExternalId);

            if (savingsAccount.AvailableBalance < 0)
            {
                issuesList.Add(ClientAccountClosureResultCode.NegativeBalance);
            }
            else if (savingsAccount.AvailableBalance > 5)
            {
                issuesList.Add(ClientAccountClosureResultCode.PositiveBalance);
            }

            if (savingsAccount.SavingsAmountOnHold > 0)
            {
                issuesList.Add(ClientAccountClosureResultCode.PendingTransfersPresent);
            }
        }

        await Mediator.Send(new MarkLoansAsClosedIfNeededCommand { UserId = user.Id }, cancellationToken);

        var loanAccount = user.LoanAccounts
            .Where(x => !x.DeletedAt.HasValue)
            .FirstOrDefault(x => x.Status == LoanAccountStatus.Active);

        if (loanAccount != null)
        {
            issuesList.Add(ClientAccountClosureResultCode.DisbursedAdvance);
        }

        var payrollConnections = await _payrollConnectionsProvider.GetPayrollConnections(request.UserId, cancellationToken);
        if (payrollConnections.TotalCount > 0)
        {
            issuesList.Add(ClientAccountClosureResultCode.ArgyleConnected);
        }

        return new(issuesList);
    }
}
