﻿namespace BNine.Application.Aggregates.Users.Queries.CloseAccount.ValidateCloseClientAccount;

using MediatR;
using Models;

public class ValidateCloseClientAccountQuery : IRequest<ClientAccountClosureValidationResult>
{
    public Guid UserId
    {
        get;
        set;
    }
}
