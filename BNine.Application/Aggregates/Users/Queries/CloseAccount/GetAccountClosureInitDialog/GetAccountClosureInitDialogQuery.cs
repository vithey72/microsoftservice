﻿namespace BNine.Application.Aggregates.Users.Queries.CloseAccount.GetAccountClosureInitDialog;

using CommonModels.Dialog;
using MediatR;

public record GetAccountClosureInitDialogQuery : IRequest<GenericDialogRichBodyViewModel>;
