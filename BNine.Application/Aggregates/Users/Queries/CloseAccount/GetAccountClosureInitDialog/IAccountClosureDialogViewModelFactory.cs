﻿namespace BNine.Application.Aggregates.Users.Queries.CloseAccount.GetAccountClosureInitDialog;

using BNine.Application.Aggregates.CommonModels.Dialog;

public interface IAccountClosureDialogViewModelFactory
{
    GenericDialogRichBodyViewModel GetSuccessDialog();
    GenericDialogRichBodyViewModel GetBlockedDueToSettingUpInProgressDialog();
}
