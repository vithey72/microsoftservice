﻿namespace BNine.Application.Aggregates.Users.Queries.CloseAccount.GetAccountClosureInitDialog;

using BNine.Application.Aggregates.CommonModels.Buttons;
using BNine.Application.Aggregates.CommonModels.Dialog;
using BNine.Application.Aggregates.CommonModels.Text;
using BNine.Constants;

public class AccountClosureDialogViewModelFactory : IAccountClosureDialogViewModelFactory
{
    private const string BaseContainerUrl =
        "https://b9prodaccount.blob.core.windows.net/static-documents-container/Misc/";
    private const string ClosureImageFileName = "pensive_closure.png";
    private const string BlockedImageFileName = "account_closure_locked.png";

    public GenericDialogRichBodyViewModel GetSuccessDialog()
    {
        return new GenericDialogRichBodyViewModel
        {
            Title = "Closure happens",
            Subtitle = new LinkedText(
                "We're just sorry it had to be with you. Just so you're aware, " +
                "once we part ways your account can't be reopened."),
            ImageUrl = string.Concat(BaseContainerUrl, ClosureImageFileName),
            Buttons = new[]
            {
                new ButtonViewModel("GO BACK", ButtonStyleEnum.Solid)
                    .EmitsUserEvent(EventNames.UserAccountClosure.GoBackButton, null, null),
                new ButtonViewModel("CLOSE ACCOUNT", ButtonStyleEnum.Bordered)
                    .WithDeeplink(DeepLinkRoutes.AccountClosureChecklist)
                    .EmitsUserEvent(EventNames.UserAccountClosure.CloseAccountButton, null, null),
            },
        };
    }

    public GenericDialogRichBodyViewModel GetBlockedDueToSettingUpInProgressDialog()
    {
        return new GenericDialogRichBodyViewModel
        {
            Title = "Your account is still being set up",
            Subtitle = new LinkedText(
                "New accounts take a few days to set up.\nOnce that process is complete you'll be able to close it."),
            ImageUrl = string.Concat(BaseContainerUrl, BlockedImageFileName),
            Buttons = new[]
            {
                new ButtonViewModel("GO BACK", ButtonStyleEnum.Solid)
                    .EmitsUserEvent(EventNames.UserAccountClosure.GoBackButton, null, null),
            },
        };
    }
}
