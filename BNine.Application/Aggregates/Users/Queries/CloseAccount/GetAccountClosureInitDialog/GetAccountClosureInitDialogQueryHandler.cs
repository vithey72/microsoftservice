﻿namespace BNine.Application.Aggregates.Users.Queries.CloseAccount.GetAccountClosureInitDialog;

using Abstractions;
using AutoMapper;
using BNine.Application.Exceptions;
using BNine.Domain.Entities.User;
using CommonModels.Dialog;
using Interfaces;
using MediatR;
using Microsoft.EntityFrameworkCore;

public class GetAccountClosureInitDialogQueryHandler
    : AbstractRequestHandler
    , IRequestHandler<GetAccountClosureInitDialogQuery, GenericDialogRichBodyViewModel>
{
    private readonly IAccountClosureDialogViewModelFactory _accountClosureDialogViewModelFactory;
    private readonly IDateTimeUtcProviderService _dateTimeProvider;
    private readonly IClientAccountSettingsProvider _settingsProvider;

    public GetAccountClosureInitDialogQueryHandler(
        IMediator mediator,
        IBNineDbContext dbContext,
        IMapper mapper,
        ICurrentUserService currentUser,
        IAccountClosureDialogViewModelFactory accountClosureDialogViewModelFactory,
        IDateTimeUtcProviderService dateTimeProvider,
        IClientAccountSettingsProvider settingsProvider
        )
        : base(mediator, dbContext, mapper, currentUser)
    {
        _accountClosureDialogViewModelFactory = accountClosureDialogViewModelFactory;
        _dateTimeProvider = dateTimeProvider;
        _settingsProvider = settingsProvider;
    }

    public async Task<GenericDialogRichBodyViewModel> Handle(GetAccountClosureInitDialogQuery request, CancellationToken cancellationToken)
    {
        var user = await GetCurrentActiveUserOrThrow();

        var settings = await _settingsProvider.GetClientAccountSettings(cancellationToken);
        var latestDateToBecomeActivated = _dateTimeProvider.UtcNow().AddDays(-settings.BlockClosureOffsetFromActivationInDays);
        if (user.ActivatedAt.Value > latestDateToBecomeActivated)
        {
            return _accountClosureDialogViewModelFactory.GetBlockedDueToSettingUpInProgressDialog();
        }

        return _accountClosureDialogViewModelFactory.GetSuccessDialog();
    }

    private async Task<User> GetCurrentActiveUserOrThrow()
    {
        if (!CurrentUser.UserId.HasValue)
        {
            throw new NotFoundException($"User not found");
        }

        var userId = CurrentUser.UserId.Value;
        var user = await DbContext.Users.FirstOrDefaultAsync(x => x.Id == userId);

        if (user is null)
        {
            throw new NotFoundException($"User with id {userId} not found");
        }

        if (!user.ActivatedAt.HasValue)
        {
            throw new NotFoundException($"User with id {userId} not found");
        }

        return user;
    }
}
