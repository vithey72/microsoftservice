﻿namespace BNine.Application.Aggregates.Users.Queries.CloseAccount.GetAccountClosureChecklistScreen;

using BNine.Application.Abstractions;
using CommonModels.Dialog;
using MediatR;

public class GetAccountClosureChecklistScreenQuery
    : MBanqSelfServiceUserRequest
    , IRequest<GenericDialogWithBulletPoints>
{
}
