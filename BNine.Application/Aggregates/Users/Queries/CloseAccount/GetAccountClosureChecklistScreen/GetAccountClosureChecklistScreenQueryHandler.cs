﻿namespace BNine.Application.Aggregates.Users.Queries.CloseAccount.GetAccountClosureChecklistScreen;

using System.Threading;
using Abstractions;
using AutoMapper;
using BNine.Application.Aggregates.AgreementDocuments.AgreementScreen.Models;
using BNine.Application.Aggregates.Payroll;
using BNine.Application.Extensions;
using CommonModels.Buttons;
using CommonModels.Dialog;
using CommonModels.DialogElements;
using CommonModels.Text;
using Constants;
using Enums;
using Helpers;
using Interfaces;
using Interfaces.Bank.Client;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Models;
using ValidateCloseClientAccount;

#nullable enable

public class GetAccountClosureChecklistScreenQueryHandler
    : AbstractRequestHandler
    , IRequestHandler<GetAccountClosureChecklistScreenQuery, GenericDialogWithBulletPoints>
{
    private readonly IBankSavingAccountsService _bankSavingAccounts;
    private readonly IPayrollConnectionsProvider _payrollConnectionsProvider;

    public GetAccountClosureChecklistScreenQueryHandler(
        IMediator mediator,
        IBNineDbContext dbContext,
        IMapper mapper,
        ICurrentUserService currentUser,
        IBankSavingAccountsService bankSavingAccounts,
        IPayrollConnectionsProvider payrollConnectionsProvider
        )
        : base(mediator, dbContext, mapper, currentUser)
    {
        _bankSavingAccounts = bankSavingAccounts;
        _payrollConnectionsProvider = payrollConnectionsProvider;
    }

    public async Task<GenericDialogWithBulletPoints> Handle(GetAccountClosureChecklistScreenQuery request, CancellationToken cancellationToken)
    {
        var validationResult = await Mediator.Send(new ValidateCloseClientAccountQuery
        {
            UserId = CurrentUser.UserId!.Value
        }, cancellationToken);

        var advanceBulletPoint = AddAdvanceBulletPoint(validationResult);
        var balanceBulletPoint = await BuildBalanceBulletPoint(validationResult, request);
        var pendingTransfersPoint = BuildPendingTransfersBulletPoint(validationResult);
        var argyleBulletPoint = await BuildArgyleBulletPoint(validationResult, cancellationToken);

        var bulletPoints = new[]
        {
            advanceBulletPoint,
            balanceBulletPoint,
            pendingTransfersPoint,
            argyleBulletPoint.bullet,
        };

        var viewModel = new GenericDialogWithBulletPoints
        {
            Header = new GenericDialogHeaderViewModel { Title = "Closing Your Account", },
            Body = new GenericDialogBulletPointedBodyViewModel
            {
                Title = "Before we can close your account",
                Subtitle = new LinkedText("Make sure nothing from the list below applies to you"),
                BulletPoints = bulletPoints,
                Buttons = new[]
                {
                    new ButtonViewModel("GO BACK", ButtonStyleEnum.Bordered)
                        .EmitsUserEvent(EventNames.UserAccountClosure.GoBackButton, null, null),
                    new ButtonViewModel("CLOSE ACCOUNT", ButtonStyleEnum.Solid).Disable()
                        .EmitsUserEvent(EventNames.UserAccountClosure.CloseAccountButton, null, null),
                },
                ExplicitPayrollDisconnectionConfirmationCheckBox = argyleBulletPoint.checkBox,
            }
        };

        return viewModel;
    }

    private static DeeplinkedBulletPointViewModel AddAdvanceBulletPoint(ClientAccountClosureValidationResult validationResult)
    {
        if (validationResult.Reasons.Contains(ClientAccountClosureResultCode.DisbursedAdvance))
        {
            var placeholder = "{REPAYNOW}";
            var entry = BuildGenericWarningBulletPoint(
                "An Open Advance",
                $"You'll need to repay any advances you've received. {placeholder}",
                placeholder,
                "Repay now",
                DeepLinkRoutes.AdvanceRepayment);
            return entry;
        }
        else
        {
            var entry = new DeeplinkedBulletPointViewModel(
                    new TextWithDeeplink("All advances have been repaid"), "Advance")
                .WithKnownType(BulletPointTypeEnum.CircledCheckmark);
            return entry;
        }
    }

    private async Task<DeeplinkedBulletPointViewModel> BuildBalanceBulletPoint(ClientAccountClosureValidationResult validationResult,
        MBanqSelfServiceUserRequest request)
    {
        var user = DbContext
            .Users
            .Include(x => x.CurrentAccount)
            .Single(x => x.Id == CurrentUser.UserId);

        var accountInfo = await _bankSavingAccounts.GetSavingAccountInfo(user.CurrentAccount.ExternalId, request.MbanqAccessToken, request.Ip);
        var balance = accountInfo.AvailableBalance;

        if (validationResult.Reasons.Contains(ClientAccountClosureResultCode.NegativeBalance)
            || validationResult.Reasons.Contains(ClientAccountClosureResultCode.PositiveBalance))
        {
            if (validationResult.Reasons.Contains(ClientAccountClosureResultCode.NegativeBalance))
            {
                var placeholder = "{ADDMONEY}";
                return BuildGenericWarningBulletPoint(
                    "A Negative Balance",
                    $"We can only close accounts with a balance of $0. {placeholder}",
                    placeholder,
                    "Add money",
                    DeepLinkRoutes.ReplenishmentMethods);
            }
            else
            {
                var placeholder = "{SENDMONEY}";
                return BuildGenericWarningBulletPoint(
                    "Positive Balance",
                    "Please remove all funds from your B9 account. You'll be able to close it once the " +
                    $"transaction has been settled/posted. We recommend using our free ACH transfers. {placeholder}",
                    placeholder,
                    "Remove my money",
                    DeepLinkRoutes.SendMoney);
            }
        }

        if (balance == 0)
        {
            return new DeeplinkedBulletPointViewModel(
                    new TextWithDeeplink("Account is at $0"), "Balance")
                .WithKnownType(BulletPointTypeEnum.CircledCheckmark);
        }

        return new DeeplinkedBulletPointViewModel(
                new TextWithDeeplink(
                    $"You have an available balance of {CurrencyFormattingHelper.AsRegular(balance)}, which you can either withdraw or continue with the account closure. " +
                    "If you choose to proceed with closing the account, the funds will not be refunded to you."),
                "Balance")
            .WithKnownType(BulletPointTypeEnum.CircledCheckmark);
    }

    private DeeplinkedBulletPointViewModel BuildPendingTransfersBulletPoint(ClientAccountClosureValidationResult validationResult)
    {
        if (validationResult.Reasons.Contains(ClientAccountClosureResultCode.PendingTransfersPresent))
        {
            var placeholder = "{HISTORY}";
            var entry = BuildGenericWarningBulletPoint(
                "A Pending Transaction",
                $"You'll need to wait until it, as well as all others, have been settled/posted. {placeholder}",
                placeholder,
                "History",
                DeepLinkRoutes.TransfersHistory);
            return entry;
        }
        else
        {
            var entry = new DeeplinkedBulletPointViewModel(
                    new TextWithDeeplink("All transactions have been settled/posted"), "Transactions")
                .WithKnownType(BulletPointTypeEnum.CircledCheckmark);
            return entry;
        }
    }

    private async Task<(DeeplinkedBulletPointViewModel bullet, CheckBox checkBox)> BuildArgyleBulletPoint(
        ClientAccountClosureValidationResult validationResult,
        CancellationToken cancellationToken)
    {
        if (validationResult.Reasons.Contains(ClientAccountClosureResultCode.ArgyleConnected))
        {
            var payrollConnections = await _payrollConnectionsProvider.GetPayrollConnections(CurrentUser.UserId(), cancellationToken);
            if (payrollConnections.ExternalCount > 0)
            {
                //return (BuildArgyleBulletPoint1(payrollConnections), null);
                return BuildArgyleBulletPoint2();
            }
            else
            {
                return BuildArgyleBulletPoint2();
            }
        }
        else
        {
            return BuildArgyleBulletPoint3();
        }
    }

    private DeeplinkedBulletPointViewModel BuildArgyleBulletPoint1(IPayrollConnections payrollConnections)
    {
        var depositDestinationIds = payrollConnections.InternalDestinationIds.Select(x => x.ToString()).ToArray();

        var placeholder = "{DISCONNECT_PAYROLL}";
        var bullet = BuildGenericWarningBulletPoint(
            "Payroll connection",
            $"Please cancel your direct deposit to us. {placeholder}\n\n" +
            "Your changes will be effective in 1-2 payment cycles depending on your employer",
            placeholder,
            "Disconnect payroll",
            DeepLinkRoutes.DisconnectPayroll,
            new
            {
                depositDestinationIds,
            });

        return bullet;
    }

    private (DeeplinkedBulletPointViewModel bullet, CheckBox checkBox) BuildArgyleBulletPoint2()
    {
        var bullet = new DeeplinkedBulletPointViewModel(
                new TextWithDeeplink(
                    "Login directly to your payroll platform or reach out to your employer to move your funds to another bank account.\n\n" +
                    "Your changes will be effective in 1-2 payment cycles depending on your employer."),
                "Payroll connection")
            .WithKnownType(BulletPointTypeEnum.CircledWarning);

        var checkBox = BuildPayrollDisconnectionConfirmationCheckBox();

        return (bullet, checkBox);
    }

    private (DeeplinkedBulletPointViewModel bullet, CheckBox checkBox) BuildArgyleBulletPoint3()
    {
        var bullet = new DeeplinkedBulletPointViewModel(
                new TextWithDeeplink(
                    "We don't see any payroll connection. Please make sure that your employer is not sending " +
                    "your payroll to B9 account. If account is closed, it no longer accepts deposits, and any incoming funds are returned to the sender " +
                    "within 3-5 business days."),
                "Payroll connection")
            .WithKnownType(BulletPointTypeEnum.CircledWarning);

        var checkBox = BuildPayrollDisconnectionConfirmationCheckBox();

        return (bullet, checkBox);
    }

    private CheckBox BuildPayrollDisconnectionConfirmationCheckBox()
    {
        return new CheckBox
        {
            IsRequired = true,
            IsSelected = false,
            LinkedText = new LinkedText("I confirm the above"),
        };
    }

    private static DeeplinkedBulletPointViewModel BuildGenericWarningBulletPoint(string header, string subtitle,
        string subtitlePlaceholder, string placeholderText, string deeplink, object deepLinkArguments = null)
    {
        return new DeeplinkedBulletPointViewModel(
                new TextWithDeeplink(subtitle,
                    new[]
                    {
                        new DeeplinkEmbeddedUrlViewModel(subtitlePlaceholder, placeholderText)
                            .WithDeeplink(deeplink, deepLinkArguments),
                    }), header)
            .WithKnownType(BulletPointTypeEnum.CircledWarning);
    }

}
