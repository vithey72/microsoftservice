﻿namespace BNine.Application.Aggregates.Users.Queries.CloseAccount.CloseClientAccountSelfServiced;

using CommonModels.Dialog;
using MediatR;

public record CloseClientAccountSelfServicedQuery : IRequest<GenericDialogBodyViewModel>;
