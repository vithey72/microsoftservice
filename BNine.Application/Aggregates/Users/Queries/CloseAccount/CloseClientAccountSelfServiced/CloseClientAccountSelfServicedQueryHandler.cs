﻿namespace BNine.Application.Aggregates.Users.Queries.CloseAccount.CloseClientAccountSelfServiced;

using Abstractions;
using AutoMapper;
using BNine.Application.Aggregates.Users.AccountStateMachine;
using BNine.Application.Aggregates.Users.Closure;
using BNine.Application.Extensions;
using CommonModels.Dialog;
using Interfaces;
using MediatR;

public class CloseClientAccountSelfServicedQueryHandler
    : AbstractRequestHandler
    , IRequestHandler<CloseClientAccountSelfServicedQuery, GenericDialogBodyViewModel>
{
    private readonly IClientAccountBlockedStateProvider _accountStateProvider;
    private readonly IClientAccountSettingsProvider _clientAccountSettingsProvider;
    private readonly IAccountClosurePrerequisitesProvider _accountClosurePrerequisitesProvider;

    public CloseClientAccountSelfServicedQueryHandler(
        IMediator mediator,
        IBNineDbContext dbContext,
        IMapper mapper,
        ICurrentUserService currentUser,
        IClientAccountBlockedStateProvider accountStateProvider,
        IClientAccountSettingsProvider clientAccountSettingsProvider,
        IAccountClosurePrerequisitesProvider accountClosurePrerequisitesProvider
        )
        : base(mediator, dbContext, mapper, currentUser)
    {
        _accountStateProvider = accountStateProvider;
        _clientAccountSettingsProvider = clientAccountSettingsProvider;
        _accountClosurePrerequisitesProvider = accountClosurePrerequisitesProvider;
    }

    public async Task<GenericDialogBodyViewModel> Handle(CloseClientAccountSelfServicedQuery request, CancellationToken cancellationToken)
    {
        var prerequisites = await _accountClosurePrerequisitesProvider.GetPrerequisites(CurrentUser.UserId(), cancellationToken);
        if (!prerequisites.CanCloseAccount())
        {
            throw new InvalidOperationException("Cannot close account until all conditions are satisfied.");
        }

        var accountState = await _accountStateProvider.GetCurrentAccountBlockedState(cancellationToken);

        await accountState.InitiateAccountClosure(cancellationToken);

        var settings = await _clientAccountSettingsProvider.GetClientAccountSettings(cancellationToken);
        return new GenericDialogBodyViewModel
        {
            Title = $"Your account has been temporarily blocked",
            Subtitle = $"It is scheduled to be permanently closed in {settings.VoluntaryClosureDelayInDays}" +
                " days if no new transactions are posted.",
            ButtonText = "Goodbye, B9",
        };
    }
}
