﻿namespace BNine.Application.Aggregates.Users.Queries.GetTransactionLimitsQuery;

using AutoMapper;
using BNine.Application.Abstractions;
using BNine.Application.Aggregates.Users.Models.TransactionLimits;
using BNine.Application.Interfaces;
using BNine.Application.Interfaces.DwhIntegration;
using MediatR;

public class GetUserTransactionLimitsQueryHandler
        : AbstractRequestHandler
        , IRequestHandler<GetUserTransactionLimitsQuery, UserTransactionLimitsViewModel>
{
    private IDwhIntegrationService _dwhIntegrationService;

    public GetUserTransactionLimitsQueryHandler(
        IMediator mediator,
        IBNineDbContext dbContext,
        IMapper mapper,
        ICurrentUserService currentUser,
        IDwhIntegrationService dwhIntegrationService) : base(mediator, dbContext, mapper, currentUser)
    {
        _dwhIntegrationService = dwhIntegrationService;
    }

    public async Task<UserTransactionLimitsViewModel> Handle(GetUserTransactionLimitsQuery request, CancellationToken cancellationToken)
    {
        var transactionLimits = await _dwhIntegrationService.GetUserTransactionLimits(request.UserId);

        return transactionLimits;
    }
}
