﻿using BNine.Application.Aggregates.Users.Models.TransactionLimits;
using MediatR;

namespace BNine.Application.Aggregates.Users.Queries.GetTransactionLimitsQuery;

public class GetUserTransactionLimitsQuery
    : IRequest<UserTransactionLimitsViewModel>
{
    public GetUserTransactionLimitsQuery(Guid userId)
    {
        UserId = userId;
    }

    public Guid UserId
    {
        get; set;
    }
}
