﻿namespace BNine.Application.Aggregates.Users.Queries.GetClientDocuments;

using MediatR;
using Models;

public class GetClientDocumentsQuery : IRequest<List<ClientDocument>>
{
    public Guid ClientId
    {
        get;
        set;
    }
}
