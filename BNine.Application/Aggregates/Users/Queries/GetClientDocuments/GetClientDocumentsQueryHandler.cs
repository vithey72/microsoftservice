﻿namespace BNine.Application.Aggregates.Users.Queries.GetClientDocuments;

using Abstractions;
using AutoMapper;
using Exceptions;
using Interfaces;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Models;

public class GetClientDocumentsQueryHandler : AbstractRequestHandler,
    IRequestHandler<GetClientDocumentsQuery, List<ClientDocument>>
{
    private readonly IClientDocumentsOriginalsPersistenceService _clientDocumentsOriginalsPersistenceService;

    public GetClientDocumentsQueryHandler(IMediator mediator, IBNineDbContext dbContext, IMapper mapper,
        ICurrentUserService currentUser,
        IClientDocumentsOriginalsPersistenceService clientDocumentsOriginalsPersistenceService) : base(mediator, dbContext, mapper, currentUser)
    {
        _clientDocumentsOriginalsPersistenceService = clientDocumentsOriginalsPersistenceService;
    }

    public async Task<List<ClientDocument>> Handle(GetClientDocumentsQuery request, CancellationToken cancellationToken)
    {
        var user = await DbContext
            .Users
            .Where(x => x.Id == request.ClientId)
            .FirstOrDefaultAsync(cancellationToken);

        if (user == null)
        {
            throw new NotFoundException($"Can't find user with id: {request.ClientId}");
        }

        var documents = await _clientDocumentsOriginalsPersistenceService.GetClientDocuments(request.ClientId);

        return documents;
    }
}
