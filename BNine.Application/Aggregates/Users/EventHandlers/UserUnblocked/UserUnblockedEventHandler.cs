﻿namespace BNine.Application.Aggregates.Users.EventHandlers.UserBlocked
{
    using System.Threading;
    using System.Threading.Tasks;
    using BNine.Application.Aggregates.ServiceBusEvents.Commands.PublishB9Event;
    using BNine.Application.Interfaces;
    using BNine.Application.Models;
    using BNine.Constants;
    using BNine.Domain.Events;
    using MediatR;

    public class UserUnblockedEventHandler : INotificationHandler<DomainEventNotification<UserUnblockedEvent>>
    {
        private IBNineDbContext DbContext
        {
            get;
        }

        private IMediator Mediator
        {
            get;
        }

        public UserUnblockedEventHandler(IMediator mediator, IBNineDbContext dbContext)
        {
            DbContext = dbContext;
            Mediator = mediator;
        }

        public async Task Handle(DomainEventNotification<UserUnblockedEvent> notification, CancellationToken cancellationToken)
        {
            var changeBlockStatusEvent = new Domain.Entities.User.ChangeBlockStatusEvent
            {
                Action = Enums.UserBlockAction.Unblock,
                UserId = notification.DomainEvent.User.Id
            };

            DbContext.ChangeBlockStatusEvents
                 .Add(changeBlockStatusEvent);

            await DbContext.SaveChangesAsync(cancellationToken);

            await Mediator.Send(new PublishB9EventCommand(changeBlockStatusEvent, ServiceBusTopics.B9User, ServiceBusEvents.B9Unblocked));
        }
    }
}
