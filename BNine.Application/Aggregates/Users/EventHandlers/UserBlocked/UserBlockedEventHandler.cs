﻿namespace BNine.Application.Aggregates.Users.EventHandlers.UserBlocked
{
    using System.Threading;
    using System.Threading.Tasks;
    using BNine.Application.Aggregates.ServiceBusEvents.Commands.PublishB9Event;
    using BNine.Application.Interfaces;
    using BNine.Application.Models;
    using BNine.Constants;
    using BNine.Domain.Events;
    using MediatR;

    public class UserBlockedEventHandler : INotificationHandler<DomainEventNotification<UserBlockedEvent>>
    {
        private IBNineDbContext DbContext
        {
            get;
        }

        private IMediator Mediator
        {
            get;
        }

        public UserBlockedEventHandler(IMediator mediator, IBNineDbContext dbContext)
        {
            DbContext = dbContext;
            Mediator = mediator;
        }

        public async Task Handle(DomainEventNotification<UserBlockedEvent> notification, CancellationToken cancellationToken)
        {
            var changeBlockStatusEvent = new Domain.Entities.User.ChangeBlockStatusEvent
            {
                Action = Enums.UserBlockAction.Block,
                UserId = notification.DomainEvent.User.Id,
                UserBlockReasonId = notification.DomainEvent.User.UserBlockReasonId
            };

            DbContext.ChangeBlockStatusEvents
                 .Add(changeBlockStatusEvent);

            await DbContext.SaveChangesAsync(cancellationToken);

            await Mediator.Send(new PublishB9EventCommand(changeBlockStatusEvent, ServiceBusTopics.B9User, ServiceBusEvents.B9Blocked));
        }
    }
}
