﻿namespace BNine.Application.Aggregates.Users.EventHandlers.UserSyncedStatsUpdated;

using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Domain.Entities.User;
using Interfaces;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;

public class UserSyncedStatsUpdatedEventHandler : INotificationHandler<UserSyncedStatsUpdatedEvent>
{
    private readonly IBNineDbContext _dbContext;

    public UserSyncedStatsUpdatedEventHandler(
        IBNineDbContext dbContext
        )
    {
        _dbContext = dbContext;
    }

    public async Task Handle(UserSyncedStatsUpdatedEvent statsUpdatedEvent, CancellationToken cancellationToken)
    {
        var user = await _dbContext.Users
            .Include(u => u.UserSyncedStats)
            .Include(u => u.AdvanceStats)
            .Include(u => u.Settings)
            .Where(x => x.Id == statsUpdatedEvent.UserId)
            .FirstOrDefaultAsync(cancellationToken);

        if (user == null)
        {
            throw new KeyNotFoundException($"User with Id = {statsUpdatedEvent.UserId} does not exist.");
        }

        // prevent updating the entity with stale data
        if (user.UserSyncedStats != null && user.UserSyncedStats.UpdatedAt > statsUpdatedEvent.CreatedAt)
        {
            return;
        }

        user.UserSyncedStats = new UserSyncedStats
        {
            HasPayAllocation = statsUpdatedEvent.HasPayAllocation,
            IsWaitingForArgyle = statsUpdatedEvent.IsWaitingForArgyle,
            AdvancesTakenCount = statsUpdatedEvent.AdvancesTakenCount,
            PayrollSumFor14Days = statsUpdatedEvent.PayrollSumFor14Days,
            PayrollSumFor30Days = statsUpdatedEvent.PayrollSumFor30Days,
            PayrollsTotalCount = statsUpdatedEvent.PayrollsTotalCount,
            LastAdvanceTakenAmount = statsUpdatedEvent.LastAdvanceTakenAmount,
            MaximumBasicAdvanceAmount = statsUpdatedEvent.MaxNextAdvanceBasic,
            CurrentBasicAdvanceAmount = statsUpdatedEvent.CurrentAdvanceLimit,
            CurrentPremiumAdvanceAmount = statsUpdatedEvent.CurrentAdvancePremium,
            NextBasicAdvanceAmount = statsUpdatedEvent.NextAdvanceBasic,
            NextPremiumAdvanceAmount = statsUpdatedEvent.NextAdvancePremium,
            UpdatedAt = statsUpdatedEvent.CreatedAt,
        };

        if (statsUpdatedEvent.ActivePayAllocationCount.HasValue)
        {
            user.UserSyncedStats.ActivePayAllocationCount = statsUpdatedEvent.ActivePayAllocationCount.Value;
        }

        if (statsUpdatedEvent.ActivePayAllocationCountExternal.HasValue)
        {
            user.UserSyncedStats.ActiveExternalPayAllocationCount = statsUpdatedEvent.ActivePayAllocationCountExternal.Value;
        }

        if (statsUpdatedEvent.ActivePayAllocationIdsInternal is not null)
        {
            user.UserSyncedStats.ActiveInternalPayAllocationIds = JsonConvert.SerializeObject(statsUpdatedEvent.ActivePayAllocationIdsInternal);
        }

        user.Settings.IsPayAllocationEnabled = statsUpdatedEvent.HasPayAllocation;

        await _dbContext.SaveChangesAsync(cancellationToken);
    }
}
