﻿namespace BNine.Application.Aggregates.Users.EventHandlers.UserSyncedStatsUpdated;

using Common.Attributes;
using Constants;
using MediatR;
using Newtonsoft.Json;

[ServiceBusEvent(TopicName = ServiceBusTopics.ItUserStats)]
public class UserSyncedStatsUpdatedEvent : INotification
{
    [JsonProperty("id")]
    public int Id
    {
        get;
        set;
    }

    [JsonProperty("userId")]
    public Guid UserId
    {
        get;
        set;
    }

    /// <summary>
    /// User's Argyle connection is being established.
    /// </summary>
    [JsonProperty("payrollDataAnalysis")]
    public bool IsWaitingForArgyle
    {
        get;
        set;
    }

    [JsonProperty("hasPayAllocation")]
    public bool HasPayAllocation
    {
        get;
        set;
    }

    [JsonProperty("advanceTakenCount")]
    public int AdvancesTakenCount
    {
        get;
        set;
    }

    [JsonProperty("payrollSumFor14Days")]
    public decimal PayrollSumFor14Days
    {
        get;
        set;
    }

    [JsonProperty("payrollSumFor30Days")]
    public decimal PayrollSumFor30Days
    {
        get;
        set;
    }

    [JsonProperty("currentAdvanceLimit")]
    public decimal CurrentAdvanceLimit
    {
        get;
        set;
    }

    /// <summary>
    /// 0 means advance was never taken.
    /// </summary>
    [JsonProperty("lastAdvanceTakenAmount")]
    public decimal LastAdvanceTakenAmount
    {
        get;
        set;
    }

    [JsonProperty("payrollsTotalCount")]
    public int PayrollsTotalCount
    {
        get;
        set;
    }

    [JsonProperty("activePayAllocationCount")]
    public int? ActivePayAllocationCount
    {
        get;
        set;
    }

    [JsonProperty("activePayAllocationCountExternal")]
    public int? ActivePayAllocationCountExternal
    {
        get;
        set;
    }

    [JsonProperty("activePayAllocationIdsInternal")]
    public Guid[] ActivePayAllocationIdsInternal
    {
        get;
        set;
    }

    [JsonProperty("maxNextAdvanceBasic")]
    public decimal MaxNextAdvanceBasic
    {
        get;
        set;
    }

    [JsonProperty("currentAdvanceBasic")]
    public decimal CurrentAdvanceBasic
    {
        get;
        set;
    }

    [JsonProperty("currentAdvancePremium")]
    public decimal CurrentAdvancePremium
    {
        get;
        set;
    }

    [JsonProperty("nextAdvanceBasic")]
    public decimal NextAdvanceBasic
    {
        get;
        set;
    }

    [JsonProperty("nextAdvancePremium")]
    public decimal NextAdvancePremium
    {
        get;
        set;
    }

    /// <summary>
    /// Message generation date. Outdated messages are ignored.
    /// </summary>
    [JsonProperty("createdAt")]
    public DateTime CreatedAt
    {
        get;
        set;
    }
}
