﻿namespace BNine.Application.Aggregates.Users.EventHandlers.UpdatedUserAdvanceExtenstion;

using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using BNine.Application.Interfaces;
using BNine.Settings;
using CommonModels;
using Constants;
using CreditLines.Queries.ActivateAdvanceBoost;
using Domain.Entities.User;
using Interfaces.Bank.Client;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using Polly;
using ServiceBusEvents.Commands.PublishB9Event;

public class UpdatedUserAdvanceStatsEventHandler : INotificationHandler<UpdatedUserAdvanceStatsEvent>
{
    private readonly IBNineDbContext _dbContext;
    private readonly INotificationUsersService _pushNotificationService;
    private readonly IMapper _mapper;
    private readonly IMediator _mediator;
    private readonly ILogger<UpdatedUserAdvanceStatsEventHandler> _logger;
    private readonly IBankAuthorizationService _bankAuthorizationService;
    private readonly IBNineAuditDbContext _auditDbContext;
    private readonly IOptions<AuditEventSettings> _auditEventSettings;
    private readonly int _maxRetryAttempts = 3;

    public UpdatedUserAdvanceStatsEventHandler(
        IBNineDbContext dbContext,
        INotificationUsersService pushNotificationService,
        IMapper mapper,
        IMediator mediator,
        ILogger<UpdatedUserAdvanceStatsEventHandler> logger,
        IBankAuthorizationService bankAuthorizationService,
        IBNineAuditDbContext auditDbContext,
        IOptions<AuditEventSettings> auditEventSettings)
    {
        _dbContext = dbContext;
        _pushNotificationService = pushNotificationService;
        _mapper = mapper;
        _mediator = mediator;
        _logger = logger;
        _bankAuthorizationService = bankAuthorizationService;
        _auditDbContext = auditDbContext;
        _auditEventSettings = auditEventSettings;
    }

    public async Task Handle(UpdatedUserAdvanceStatsEvent updateEvent, CancellationToken cancellationToken)
    {
        var offer = updateEvent.Offer;
        var user = await _dbContext.Users
            .Include(u => u.AdvanceStats)
            .Include(u => u.Devices)
            .Include(u => u.Settings)
            .Where(x => x.Id == offer.UserId)
            .FirstOrDefaultAsync(cancellationToken);

        if (user == null)
        {
            throw new KeyNotFoundException($"User with Id = {offer.UserId} does not exist.");
        }

        #region EventAuditFlow
        // this is the 2nd step of the EventAuditFlow, where the UserId and EvenTime are populated
        // we expect the audit record with the corresponding EventID to exist already


        if (_auditEventSettings.Value.IsEnabled)
        {
            var existingAuditRecord = _auditDbContext
                .EventAuditEntries
                .FirstOrDefault(a => a.EventId == updateEvent.EventId);

            if (existingAuditRecord != null)
            {
                existingAuditRecord.UserId = user.Id;
                existingAuditRecord.EventTime = updateEvent.Offer.UpdatedAt ?? DateTime.UtcNow;
                existingAuditRecord.ParsedEventModel = JsonConvert.SerializeObject(offer);

                try
                {
                    await _auditDbContext.SaveChangesAsync(cancellationToken);
                }catch (Exception e)
                {
                    _logger.LogError(e, $"Error while trying to update EventAudit record with EventId = {updateEvent.EventId}");
                }
            }
            else
            {
                _logger.LogError($"EventAudit record with EventId = {updateEvent.EventId} does not exist.");
            }
        }
        #endregion

        // prevent updating the entity with stale data
        if (user.AdvanceStats != null && user.AdvanceStats.UpdatedAt > offer.UpdatedAt)
        {
            return;
        }

        user.AdvanceStats = new AdvanceStats
        {
            EventId = updateEvent.EventId,
            OfferId = offer.OfferId,
            AvailableLimit = offer.AvailableLimit,
            ExtensionLimit = offer.ExtensionLimit,
            ExtensionPrice = offer.ExtensionPrice,
            ExtensionExpiresAt = offer.ExtensionExpireDate,
            Rating = offer.Rating,
            Slider1 = offer.Slider.Slider1,
            Slider2 = offer.Slider.Slider2,
            Slider3 = offer.Slider.Slider3,
            Slider4 = offer.Slider.Slider4,
            UpdatedAt = offer.UpdatedAt ?? DateTime.UtcNow,
        };

        if (offer.Boost != null && offer.Boost.Settings != null && offer.Boost.Settings.Any())
        {
            user.AdvanceStats.HasBoostLimit = true;

            user.AdvanceStats.BoostLimit1 = offer.Boost.Option1.Amount;
            user.AdvanceStats.BoostLimit1Price = offer.Boost.Option1.Price;
            user.AdvanceStats.BoostLimit1HoursToDelay = offer.Boost.Option1.DelayedHours;

            user.AdvanceStats.BoostLimit2 = offer.Boost.Option2?.Amount ?? 0;
            user.AdvanceStats.BoostLimit2Price = offer.Boost.Option2?.Price ?? 0;
            user.AdvanceStats.BoostLimit2HoursToDelay = offer.Boost.Option2?.DelayedHours ?? 0;

            user.AdvanceStats.BoostLimit3 = offer.Boost.Option3?.Amount ?? 0;
            user.AdvanceStats.BoostLimit3Price = offer.Boost.Option3?.Price ?? 0;
            user.AdvanceStats.BoostLimit3HoursToDelay = offer.Boost.Option3?.DelayedHours ?? 0;

            var delayedBoost = offer.Boost.DelayedBoost;

            if (delayedBoost != null)
            {

                WriteDelayedBoostSettingsToEntity(delayedBoost, user);

                // disburse boost if it's ready
                if (delayedBoost.ReadyToDisburse)
                {

                    var policy = Policy.Handle<Exception>()
                        .WaitAndRetryAsync(_maxRetryAttempts, (attemptCount) => TimeSpan.FromSeconds(attemptCount * 2),
                            onRetry: (exception, _, attemptNumber, _) =>
                            {
                                _logger.LogError(exception,
                                    $"Error while trying to activate delayed boost for user {user.Id} on attempt {attemptNumber}");
                            });

                    try
                    {
                        await policy.ExecuteAsync(async () =>
                        {
                            var mBanqAccessToken = (await _bankAuthorizationService.GetTokens(null, null)).AccessToken;

                            //TODO do we need to validate the BoostID here?
                            //TODO it will be mapped to default ID=1 in handler if it's any other then available options 1,2,3

                            var res = await _mediator.Send(new ActivateAdvanceBoostQuery()
                            {
                                BoostId = delayedBoost.BoostId,
                                MbanqAccessToken = mBanqAccessToken,
                                UserId = user.Id,
                                IsReadyToDisburse = true,
                                IsDelayedBoost = true
                            }, cancellationToken);

                            if (res.IsSuccess)
                            {
                                await _pushNotificationService.SendNotification(user,
                                    new GenericPushNotification()
                                    {
                                        Title = "Delayed advance with boost activated",
                                        Message = "Your delayed advance with boost has been activated."
                                    }, true);
                            }
                            else
                            {
                                await _pushNotificationService.SendNotification(user,
                                    new GenericPushNotification()
                                    {
                                        Title = $"Delayed advance with boost activation failed due to {res.Fail.Body.Title}",
                                        Message = res.Fail.Body.Subtitle
                                    }, true);
                            }

                        });


                    } catch (Exception e)
                    {

                        await _mediator.Send(new PublishB9EventCommand(
                            new AdvanceWithBoostActivationFailed(UserId: user.Id, offer.OfferId, e.Message),
                            ServiceBusTopics.ItAdvanceWithDelayedBoostFailed,
                            ServiceBusEvents.B9AdvanceBoostAutoActivationFailed
                        ), cancellationToken);

                        _logger.LogError(e, "Delayed advance with boost activation failed for user {UserId}", user.Id);

                        await _pushNotificationService.SendNotification(user,
                            new GenericPushNotification()
                            {
                                Title = "Delayed advance with boost activation failed",
                                Message = "Your delayed advance with boost activation failed. We will review the issue and try to resolve it."
                            }, true);
                    }
                }
            }
            else
            {
                // it's not necessary here since we have already set this to null in the beginning of the method,
                // but for explicitness we do it here again
                user.AdvanceStats.DelayedBoostSettings = null;
            }
        }


        await _dbContext.SaveChangesAsync(cancellationToken);
        if (updateEvent.Notification != null)
        {
            var notification = updateEvent.Notification;
            await _pushNotificationService.SendNotification(user, notification, true);

        }
    }

    private void WriteDelayedBoostSettingsToEntity(UpdatedUserAdvanceStatsEvent.AdvanceOffer.DelayedBoost delayedBoost, User user)
    {
        var delayedBoostSettings = _mapper.Map<DelayedBoostSettings>(delayedBoost);
        user.AdvanceStats.DelayedBoostSettings = delayedBoostSettings;
        user.AdvanceStats.DelayedBoostSettings.SyncedWithData = true;
    }

    private record AdvanceWithBoostActivationFailed(Guid UserId, string OfferId, string ErrorMessage);

}
