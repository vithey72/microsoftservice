﻿namespace BNine.Application.Aggregates.Users.EventHandlers.UpdatedUserAdvanceExtenstion;

using System;
using AutoMapper;
using BNine.Common.Attributes;
using BNine.Constants;
using CommonModels;
using Domain.Entities.User;
using Domain.Interfaces.EventAudit;
using Mappings;
using MediatR;
using Newtonsoft.Json;

[ServiceBusEvent(TopicName = ServiceBusTopics.ItAdvanceStats)]
public class UpdatedUserAdvanceStatsEvent : INotification, IBusEventAuditable
{

    [JsonIgnore]
    public Guid? EventId
    {
        get;
        set;
    }

    [JsonProperty("offer")]
    public AdvanceOffer Offer
    {
        get;
        set;
    }

    [JsonProperty("notification")]
    public GenericPushNotification Notification
    {
        get;
        set;
    }

    public class AdvanceOffer
    {
        [JsonProperty("userId")]
        public Guid UserId
        {
            get; set;
        }

        [JsonProperty("offerId")]
        public string OfferId
        {
            get;
            set;
        }

        [JsonProperty("availableLimit")]
        public decimal AvailableLimit
        {
            get; set;
        }

        [JsonProperty("extensionPrice")]
        public decimal ExtensionPrice
        {
            get; set;
        }

        [JsonProperty("extensionLimit")]
        public decimal ExtensionLimit
        {
            get; set;
        }

        [JsonProperty("extensionExpireDate")]
        public DateTime? ExtensionExpireDate
        {
            get; set;
        }

        [JsonProperty("rating")]
        public decimal Rating
        {
            get; set;
        }

        [JsonProperty("slider")]
        public Sliders Slider
        {
            get; set;
        }

        /// <summary>
        /// Date when the message was generated. Nullable for backwards compatibility on rollout.
        /// </summary>
        [JsonProperty("updatedAt")]
        public DateTime? UpdatedAt
        {
            get; set;
        }

        [JsonProperty("boost")]
        public LimitBoost Boost
        {
            get; set;
        }

        public class Sliders
        {
            [JsonProperty("slider1")]
            public decimal Slider1
            {
                get; set;
            }

            [JsonProperty("slider2")]
            public decimal Slider2
            {
                get; set;
            }

            [JsonProperty("slider3")]
            public decimal Slider3
            {
                get; set;
            }

            [JsonProperty("slider4")]
            public decimal Slider4
            {
                get; set;
            }
        }

        public class LimitBoost
        {
            [JsonProperty("settings")]
            public LimitBoostSettings[] Settings
            {
                get; set;
            }

            [JsonProperty("delayedBoost")]
            public DelayedBoost DelayedBoost
            {
                get; set;
            }

            [JsonIgnore]
            public LimitBoostSettings Option1 => Settings.ElementAtOrDefault(0);

            [JsonIgnore]
            public LimitBoostSettings Option2 => Settings.ElementAtOrDefault(1);

            [JsonIgnore]
            public LimitBoostSettings Option3 => Settings.ElementAtOrDefault(2);
        }

        public class LimitBoostSettings
        {
            [JsonProperty("price")]
            public decimal Price
            {
                get;
                set;
            }

            [JsonProperty("amount")]
            public decimal Amount
            {
                get;
                set;
            }

            [JsonProperty("delayedHours")]
            public int DelayedHours
            {
                get;
                set;
            }
        }

        public class DelayedBoost : IMapTo<DelayedBoostSettings>
        {
            [JsonProperty("activatedAtSchedule")]
            public DateTime ActivatedAtSchedule
            {
                get;
                set;
            }

            [JsonProperty("readyToDisburse")]
            public bool ReadyToDisburse
            {
                get;
                set;
            }

            [JsonProperty("id")]
            public int BoostId
            {
                get;
                set;
            }

            public void Mapping(Profile profile)
            {
                profile.CreateMap<DelayedBoost, DelayedBoostSettings>()
                    .ForMember(ds => ds.DelayedBoostId,
                        opt =>
                            opt.MapFrom(d => d.BoostId))
                    .ForMember(ds => ds.ReadyToDisburse,
                        opt =>
                            opt.MapFrom(d => d.ReadyToDisburse))
                    .ForMember(ds => ds.ActivationDate,
                        opt =>
                            opt.MapFrom(d => d.ActivatedAtSchedule));
            }
        }
    }



}
