﻿namespace BNine.Application.Aggregates.Users.EventHandlers.AdvanceTaken
{
    using System.Threading;
    using System.Threading.Tasks;
    using BNine.Application.Aggregates.Configuration.Queries.GetConfiguration;
    using BNine.Application.Aggregates.Users.Models;
    using BNine.Application.Interfaces;
    using BNine.Application.Models;
    using BNine.Constants;
    using BNine.Domain.Events.AdvanceTaken;
    using MediatR;
    using Newtonsoft.Json;

    public class AdvanceTakenEventHandler : INotificationHandler<DomainEventNotification<AdvanceTakenEvent>>
    {
        private IBNineDbContext DbContext
        {
            get;
        }

        private IFeatureConfigurationService FeatureConfigurationService
        {
            get;
        }

        private IMediator Mediator
        {
            get;
        }

        public AdvanceTakenEventHandler(IMediator mediator, IFeatureConfigurationService featureConfigurationService, IBNineDbContext dbContext)
        {
            DbContext = dbContext;
            FeatureConfigurationService = featureConfigurationService;
            Mediator = mediator;
        }

        public async Task Handle(DomainEventNotification<AdvanceTakenEvent> notification, CancellationToken cancellationToken)
        {
            var featureGroup = await FeatureConfigurationService.GetPersonalFeatureConfigAsync(notification.DomainEvent.UserId,
                                                                                                    FeaturesNames.Features.UserApplicationEvaluation,
                                                                                                    FeaturesNames.Settings.UserApplicationEvaluation.ConfigDefaultSettingsValue,
                                                                                                    cancellationToken);
            var configuration = await Mediator.Send(new GetConfigurationQuery(notification.DomainEvent.UserId));
            if (configuration.HasFeatureEnabled(FeaturesNames.Features.UserApplicationEvaluation))
            {
                var settings = JsonConvert.DeserializeObject<UserApplicationEvaluationSettings>(featureGroup.Settings);
                if (settings != null)
                {
                    settings.AdvanceTakenCounter = settings.AdvanceTakenCounter == 0 ? 6 : settings.AdvanceTakenCounter - 1;
                    settings.IsActive = settings.AdvanceTakenCounter == 0;

                    featureGroup.Settings = JsonConvert.SerializeObject(settings);
                    await DbContext.SaveChangesAsync(cancellationToken);
                }
            }
        }
    }
}
