﻿namespace BNine.Application.Aggregates.Users.EventHandlers.UserBalanceReached
{
    using System.Threading;
    using System.Threading.Tasks;
    using BNine.Application.Aggregates.Users.Events;
    using BNine.Application.Exceptions;
    using BNine.Application.Interfaces;
    using BNine.Application.Models.Dto;
    using BNine.Application.Models.Feature.InfoPopup;
    using BNine.Constants;
    using MediatR;
    using JsonSerializer = System.Text.Json.JsonSerializer;

    public class UserBalanceReachedEventHandler : INotificationHandler<UserBalanceReachedEvent>
    {
        private const string popupId = "PremiumTariffPlan";
        private readonly IBNineDbContext _dbContext;
        private readonly IFeatureConfigurationService _featureConfigurationService;

        public UserBalanceReachedEventHandler(IBNineDbContext dbContext, IFeatureConfigurationService featureConfigurationService)
        {
            _dbContext = dbContext;
            _featureConfigurationService = featureConfigurationService;
        }

        public async Task Handle(UserBalanceReachedEvent notification, CancellationToken cancellationToken)
        {
            var isValidId = Guid.TryParse(notification.UserId, out var userId);
            if (!isValidId)
            {
                throw new ArgumentException(notification.UserId, nameof(notification));
            }
            if (!_dbContext.Users.Any(u => u.Id == userId))
            {
                throw new NotFoundException(nameof(User), userId);
            }

            await EnablePremiumTariffPlanPopup(userId, cancellationToken);
        }

        private async Task EnablePremiumTariffPlanPopup(Guid userId, CancellationToken cancellationToken)
        {
            var popup = new InfoPopup
            {
                ValidFrom = DateTime.UtcNow.ToString("yyyy-MM-ddTHH:mm:ss")
            };

            var infoPopupSettings = new InfoPopupSettings
            {
                Popup = new Dictionary<string, InfoPopup> { { popupId, popup } }
            };

            var featurePersonalGroup = await _featureConfigurationService.GetPersonalFeatureConfigAsync(userId,
                                                                                                    FeaturesNames.Features.InfoPopup,
                                                                                                    null,
                                                                                                    cancellationToken);
            featurePersonalGroup.Settings = JsonSerializer.Serialize(infoPopupSettings);
            await _dbContext.SaveChangesAsync(cancellationToken);
        }
    }
}