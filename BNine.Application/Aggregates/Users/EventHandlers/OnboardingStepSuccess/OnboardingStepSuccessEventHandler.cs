﻿namespace BNine.Application.Aggregates.Users.EventHandlers.OnboardingStepSuccess
{
    using System.Threading;
    using System.Threading.Tasks;
    using BNine.Application.Interfaces;
    using BNine.Application.Models;
    using BNine.Domain.Events.Users;
    using MediatR;

    public class OnboardingStepSuccessEventHandler : INotificationHandler<DomainEventNotification<OnboardingStepSuccessEvent>>
    {
        private readonly IBNineDbContext _dbContext;

        public OnboardingStepSuccessEventHandler(IBNineDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task Handle(DomainEventNotification<OnboardingStepSuccessEvent> notification, CancellationToken cancellationToken)
        {
            await _dbContext.OnboardingEvents.AddAsync(new Domain.Entities.OnboardingEvent
            {
                UserId = notification.DomainEvent.User.Id,
                Step = notification.DomainEvent.Status,
                StepDetailed = notification.DomainEvent.StatusDetailed,
                Succeeded = true,
                SourcePlatform = notification.DomainEvent.SourcePlatform,
            }, cancellationToken);

            await _dbContext.SaveChangesAsync(cancellationToken);
        }
    }
}
