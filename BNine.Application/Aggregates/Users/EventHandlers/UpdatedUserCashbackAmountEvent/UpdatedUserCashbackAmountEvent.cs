﻿namespace BNine.Application.Aggregates.Users.EventHandlers.SetWaitingForFirstPaycheckBannerAvailability;

using System;
using BNine.Common.Attributes;
using BNine.Constants;
using MediatR;
using Newtonsoft.Json;

[ServiceBusEvent(TopicName = ServiceBusTopics.ItCashbackAmount)]
public class UpdatedUserCashbackAmountEvent : INotification
{
    public UpdatedUserCashbackAmountEvent()
    {

    }

    [JsonProperty("userId")]
    public Guid UserId
    {
        get; set;
    }

    [JsonProperty("cashbackTotal")]
    public decimal CashbackTotal
    {
        get; set;
    }

    [JsonProperty("cashbackMonth")]
    public DateTime CashbackMonthAndYear
    {
        get; set;
    }

    [JsonProperty("cashbackMonthSum")]
    public decimal CashbackMonthSum
    {
        get; set;
    }

    [JsonProperty("cashbackMonthTransactionSum")]
    public decimal CashbackMonthTransactionSum
    {
        get; set;
    }

    [JsonProperty("recalcualteDatetimeUtc")]
    public DateTime CreatedAt
    {
        get; set;
    }
}
