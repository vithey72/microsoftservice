﻿namespace BNine.Application.Aggregates.Users.EventHandlers.SetWaitingForFirstPaycheckBannerAvailability;

using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using BNine.Application.Interfaces;
using Domain.Entities.CashbackProgram;
using MediatR;
using Microsoft.EntityFrameworkCore;

public class UpdatedUserCashbackAmountEventHandler : INotificationHandler<UpdatedUserCashbackAmountEvent>
{
    private readonly IBNineDbContext _dbContext;

    public UpdatedUserCashbackAmountEventHandler(IBNineDbContext dbContext)
    {
        _dbContext = dbContext;
    }

    public async Task Handle(UpdatedUserCashbackAmountEvent notification, CancellationToken cancellationToken)
    {
        var user = await _dbContext.Users
            .Include(u => u.CashbackStatistic)
            .Where(x => x.Id == notification.UserId)
            .FirstOrDefaultAsync(cancellationToken);

        if (user == null)
        {
            throw new KeyNotFoundException($"User with Id = {notification.UserId} does not exist.");
        }
        user.CashbackStatistic ??= new UserCashbackStatistic
        {
            EarnedAmountUpdatedAt = DateTime.MinValue,
            TotalEarnedAmount = 0M,
        };

        var month = notification.CashbackMonthAndYear.Month;
        var year = notification.CashbackMonthAndYear.Year;

        var usedCashback = await _dbContext.UsedCashbacks
            .FirstOrDefaultAsync(x => x.UserId == notification.UserId
                                      && x.Month == month
                                      && x.Year == year,
                cancellationToken);

        if (usedCashback == null)
        {
            // no need to save the values if user is not using cashback
            return;
        }

        // failsafe to prevent update with expired or duplicate data
        if (notification.CreatedAt > usedCashback.EarnedAmountUpdatedAt)
        {
            usedCashback.EarnedAmount = notification.CashbackMonthSum;
            usedCashback.SpentAmount = notification.CashbackMonthTransactionSum;
            usedCashback.EarnedAmountUpdatedAt = notification.CreatedAt;
        }
        if (notification.CreatedAt > user.CashbackStatistic.EarnedAmountUpdatedAt)
        {
            user.CashbackStatistic.TotalEarnedAmount = notification.CashbackTotal;
            user.CashbackStatistic.EarnedAmountUpdatedAt = notification.CreatedAt;
        }

        await _dbContext.SaveChangesAsync(cancellationToken);
    }
}
