﻿namespace BNine.Application.Aggregates.Users.EventHandlers.OnboardingStepFailed;

using Application.Models;
using Domain.Entities;
using Domain.Events.Users;
using Interfaces;
using MediatR;

public class OnboardingStepFailedEventHandler : INotificationHandler<DomainEventNotification<OnboardingStepFailedEvent>>
{
    private readonly IBNineDbContext _dbContext;

    public OnboardingStepFailedEventHandler(IBNineDbContext dbContext)
    {
        _dbContext = dbContext;
    }

    public async Task Handle(DomainEventNotification<OnboardingStepFailedEvent> notification,
        CancellationToken cancellationToken)
    {
        var onboardingErrors = notification.DomainEvent.Errors
            .Select(x => new OnboardingEvent
            {
                UserId = notification.DomainEvent.User.Id,
                Message = x,
                Step = notification.DomainEvent.Status,
                StepDetailed = notification.DomainEvent.StatusDetailed,
                Succeeded = false,
                SourcePlatform = notification.DomainEvent.SourcePlatform,
            });

        await _dbContext.OnboardingEvents.AddRangeAsync(onboardingErrors, cancellationToken);

        await _dbContext.SaveChangesAsync(cancellationToken);
    }
}
