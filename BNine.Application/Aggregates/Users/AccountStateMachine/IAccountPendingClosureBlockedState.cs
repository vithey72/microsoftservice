﻿namespace BNine.Application.Aggregates.Users.AccountStateMachine;

public interface IAccountPendingClosureBlockedState : IClientAccountState
{
    Task InvokeScheduledClosure(CancellationToken cancellationToken);

    Task CancelScheduledClosure(CancellationToken cancellationToken);
}
