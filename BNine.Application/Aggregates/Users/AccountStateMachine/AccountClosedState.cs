﻿namespace BNine.Application.Aggregates.Users.AccountStateMachine;

using System.Threading;
using System.Threading.Tasks;
using BNine.Application.Aggregates.Users.Commands.CloseAccount.CloseClientAccountInternal;
using MediatR;

public class AccountClosedState : AccountStateBase, IAccountClosedState
{
    private readonly Guid _closureReasonId;
    private readonly IMediator _mediator;

    public AccountClosedState(
        Guid userId,
        Guid closureReasonId,
        IMediator mediator
        ) : base(userId)
    {
        _closureReasonId = closureReasonId;
        _mediator = mediator;
    }

    public override async Task Enter(CancellationToken cancellationToken)
    {
        await _mediator.Send(new CloseClientAccountInternalCommand
        {
            UserId = UserId,
            ReasonId = _closureReasonId,
        }, cancellationToken);
    }
}
