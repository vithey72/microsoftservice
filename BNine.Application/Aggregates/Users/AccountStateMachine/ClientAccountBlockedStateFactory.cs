﻿namespace BNine.Application.Aggregates.Users.AccountStateMachine;

using BNine.Application.Aggregates.Users.Closure;
using BNine.Application.Interfaces;
using BNine.Application.Interfaces.Bank.Administration;
using MediatR;

public class ClientAccountBlockedStateFactory : IClientAccountBlockedStateFactory
{
    private readonly IMediator _mediator;
    private readonly IBNineDbContext _dbContext;
    private readonly IAccountClosureScheduler _accountClosureScheduler;
    private readonly IBankSavingAccountsService _bankSavingAccountsService;
    private readonly IAccountClosurePrerequisitesProvider _accountClosurePrerequisitesProvider;

    public ClientAccountBlockedStateFactory(
        IMediator mediator,
        IBNineDbContext dbContext,
        IAccountClosureScheduler accountClosureScheduler,
        IBankSavingAccountsService bankSavingAccountsService,
        IAccountClosurePrerequisitesProvider accountClosurePrerequisitesProvider
        )
    {
        _mediator = mediator;
        _dbContext = dbContext;
        _accountClosureScheduler = accountClosureScheduler;
        _bankSavingAccountsService = bankSavingAccountsService;
        _accountClosurePrerequisitesProvider = accountClosurePrerequisitesProvider;
    }

    public IAccountActiveState CreateActiveState(Guid userId, IAccountStateMachine stateMachine)
    {
        return new AccountActiveState(userId, stateMachine, this, _dbContext);
    }

    public IAccountGenericBlockedState CreateBlockedState(Guid userId, Guid blockReasonId, IAccountStateMachine stateMachine)
    {
        return new AccountGenericBlockedState(userId, blockReasonId, stateMachine, this, _dbContext);
    }

    public IAccountClosedState CreateClosedState(Guid userId, Guid closureReasonId, IAccountStateMachine stateMachine)
    {
        return new AccountClosedState(userId, closureReasonId, _mediator);
    }

    public IAccountPendingClosureBlockedState CreatePendingClosureState(Guid userId, IAccountStateMachine stateMachine)
    {
        return new AccountPendingClosureBlockedState(
            userId,
            stateMachine,
            this,
            _dbContext,
            _accountClosureScheduler,
            _bankSavingAccountsService,
            _accountClosurePrerequisitesProvider);
    }
}
