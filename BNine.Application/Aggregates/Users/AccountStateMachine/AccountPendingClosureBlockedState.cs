﻿namespace BNine.Application.Aggregates.Users.AccountStateMachine;

using System.Threading;
using System.Threading.Tasks;
using BNine.Application.Aggregates.Users.Closure;
using BNine.Application.Exceptions;
using BNine.Application.Interfaces;
using BNine.Application.Interfaces.Bank.Administration;
using BNine.Constants;
using BNine.Domain.Entities.User;
using BNine.Enums;
using Microsoft.EntityFrameworkCore;

public class AccountPendingClosureBlockedState : AccountBlockedStateBase, IAccountPendingClosureBlockedState
{
    private readonly IAccountStateMachine _stateMachine;
    private readonly IClientAccountBlockedStateFactory _stateFactory;
    private readonly IBNineDbContext _dbContext;
    private readonly IAccountClosureScheduler _accountClosureScheduler;
    private readonly IBankSavingAccountsService _bankSavingAccountsService;
    private readonly IAccountClosurePrerequisitesProvider _accountClosurePrerequisitesProvider;

    public AccountPendingClosureBlockedState(
        Guid userId,
        IAccountStateMachine stateMachine,
        IClientAccountBlockedStateFactory stateFactory,
        IBNineDbContext dbContext,
        IAccountClosureScheduler accountClosureScheduler,
        IBankSavingAccountsService bankSavingAccountsService,
        IAccountClosurePrerequisitesProvider accountClosurePrerequisitesProvider
        ) : base(userId, ClientAccount.UserBlockReasonIdForPendingClosure, dbContext)
    {
        _stateMachine = stateMachine;
        _stateFactory = stateFactory;
        _dbContext = dbContext;
        _accountClosureScheduler = accountClosureScheduler;
        _bankSavingAccountsService = bankSavingAccountsService;
        _accountClosurePrerequisitesProvider = accountClosurePrerequisitesProvider;
    }

    public override async Task Enter(CancellationToken cancellationToken)
    {
        await SetUserDetailedStatus(UserStatusDetailed.PendingClosure, cancellationToken);

        await base.Enter(cancellationToken);

        await _accountClosureScheduler.ScheduleAccountClosure(UserId, cancellationToken);
    }

    public override async Task Exit(CancellationToken cancellationToken)
    {
        await _accountClosureScheduler.RemoveAccountClosureSchedule(UserId, cancellationToken);

        //  fill user DetailedStatus to a ( % 10 == 0) value so it could fallback it's value after next Status change 
        await SetUserDetailedStatus(UserStatusDetailed.Blocked, cancellationToken);

        await base.Exit(cancellationToken);
    }

    public override async Task UnblockAccount(CancellationToken cancellationToken)
    {
        var activeState = _stateFactory.CreateActiveState(UserId, _stateMachine);

        await _stateMachine.MoveTo(activeState, cancellationToken);
    }

    public override Task InitiateAccountClosure(CancellationToken cancellationToken)
    {
        return Task.CompletedTask;
    }

    public override async Task CloseAccount(Guid closureReasonId, CancellationToken cancellationToken)
    {
        var closedState = _stateFactory.CreateClosedState(UserId, closureReasonId, _stateMachine);

        await _stateMachine.MoveTo(closedState, cancellationToken);
    }

    public Task CancelScheduledClosure(CancellationToken cancellationToken)
    {
        return UnblockAccount(cancellationToken);
    }

    public async Task InvokeScheduledClosure(CancellationToken cancellationToken)
    {
        await CheckPrerequisites(cancellationToken);

        var userActionReason = await _dbContext
            .UserBlockReasons
            .FirstAsync(x => x.Value == "Voluntary self-serviced request", cancellationToken);

        var closedState = _stateFactory.CreateClosedState(UserId, userActionReason.Id, _stateMachine);

        await _stateMachine.MoveTo(closedState, cancellationToken);
    }

    private async Task CheckPrerequisites(CancellationToken cancellationToken)
    {
        var prerequisites = await _accountClosurePrerequisitesProvider.GetPrerequisites(UserId, cancellationToken);
        if (!prerequisites.CanCloseAccount())
        {
            throw new InvalidOperationException("Cannot close account until all conditions are satisfied.");
        }
    }

    private async Task SetUserDetailedStatus(UserStatusDetailed detailedStatus, CancellationToken cancellationToken)
    {
        var user = await _dbContext.Users
            .Where(x => x.Id == UserId)
            .FirstOrDefaultAsync(cancellationToken);

        if (user == null)
        {
            throw new NotFoundException(nameof(User), UserId);
        }

        user.StatusDetailed = detailedStatus;

        await _dbContext.SaveChangesAsync(cancellationToken);
    }
}
