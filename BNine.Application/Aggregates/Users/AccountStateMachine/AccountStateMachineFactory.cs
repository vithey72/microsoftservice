﻿namespace BNine.Application.Aggregates.Users.AccountStateMachine;

public class AccountStateMachineFactory : IAccountStateMachineFactory
{
    public AccountStateMachineFactory()
    {
    }

    public IAccountStateMachine CreateAccountStateMachine()
    {
        return new AccountStateMachine();
    }
}
