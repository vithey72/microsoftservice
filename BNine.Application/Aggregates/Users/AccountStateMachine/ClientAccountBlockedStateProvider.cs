﻿namespace BNine.Application.Aggregates.Users.AccountStateMachine;

using System.Threading;
using System.Threading.Tasks;
using BNine.Application.Exceptions;
using BNine.Application.Extensions;
using BNine.Application.Interfaces;
using BNine.Constants;
using BNine.Domain.Entities.User;
using Microsoft.EntityFrameworkCore;

#nullable enable

public class ClientAccountBlockedStateProvider : IClientAccountBlockedStateProvider
{
    private readonly IBNineDbContext _dbContext;
    private readonly ICurrentUserService _currentUserService;
    private readonly IAccountStateMachineFactory _accountStateMachineFactory;
    private readonly IClientAccountBlockedStateFactory _accountBlockedStateFactory;

    public ClientAccountBlockedStateProvider(
        IBNineDbContext dbContext,
        ICurrentUserService currentUserService,
        IAccountStateMachineFactory accountStateMachineFactory,
        IClientAccountBlockedStateFactory accountBlockedStateFactory
        )
    {
        _dbContext = dbContext;
        _currentUserService = currentUserService;
        _accountStateMachineFactory = accountStateMachineFactory;
        _accountBlockedStateFactory = accountBlockedStateFactory;
    }

    public async Task<IClientAccountState> GetCurrentAccountBlockedState(CancellationToken cancellationToken)
    {
        var state = await CreateUserAccountState(_currentUserService.UserId());
        return state;
    }

    public async Task<IClientAccountState> GetCustomAccountBlockedState(Guid userId, CancellationToken cancellationToken)
    {
        var state = await CreateUserAccountState(userId);
        return state;
    }

    public async Task<IAccountPendingClosureBlockedState?> TryGetCustomAccountPendingClosureBlockedState(Guid userId, CancellationToken cancellationToken)
    {
        var state = await CreateUserAccountState(userId) as IAccountPendingClosureBlockedState;
        return state;
    }

    private async Task<IClientAccountState> CreateUserAccountState(Guid userId)
    {
        var stateMachine = _accountStateMachineFactory.CreateAccountStateMachine();
        var user = await GetUser(userId);
        var state = CreateUserAccountState(user, stateMachine);
        stateMachine.WithState(state);

        return state;
    }

    private async Task<User> GetUser(Guid userId)
    {
        var user = await _dbContext.Users.FirstOrDefaultAsync(x => x.Id == userId);

        if (user is null)
        {
            throw new NotFoundException(nameof(User), userId);
        }

        return user;
    }

    private IClientAccountState CreateUserAccountState(User user, IAccountStateMachine stateMachine)
    {
        if (user.ClosedAt.HasValue && user.UserCloseReasonId.HasValue)
        {
            return _accountBlockedStateFactory.CreateClosedState(user.Id, user.UserCloseReasonId.Value, stateMachine);
        }

        if (user.IsBlocked && user.UserBlockReasonId.HasValue)
        {
            if (user.UserBlockReasonId.Value == ClientAccount.UserBlockReasonIdForPendingClosure)
            {
                return _accountBlockedStateFactory.CreatePendingClosureState(user.Id, stateMachine);
            }

            return _accountBlockedStateFactory.CreateBlockedState(user.Id, user.UserBlockReasonId.Value, stateMachine);
        }

        if (user.IsBlocked || user.UserBlockReasonId.HasValue || user.ClosedAt.HasValue || user.UserCloseReasonId.HasValue)
        {
            throw new InvalidOperationException($"user {user.Id} status is invalid");
        }

        return _accountBlockedStateFactory.CreateActiveState(user.Id, stateMachine);
    }
}
