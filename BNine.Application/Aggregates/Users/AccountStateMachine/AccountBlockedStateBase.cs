﻿namespace BNine.Application.Aggregates.Users.AccountStateMachine;

using BNine.Application.Exceptions;
using BNine.Application.Interfaces;
using BNine.Domain.Entities.User;
using BNine.Domain.Events;
using Microsoft.EntityFrameworkCore;

public class AccountBlockedStateBase : AccountStateBase
{
    private readonly Guid _blockReasonId;
    private readonly IBNineDbContext _dbContext;

    public AccountBlockedStateBase(
        Guid userId,
        Guid blockReasonId,
        IBNineDbContext dbContext
        ) : base(userId)
    {
        _blockReasonId = blockReasonId;
        _dbContext = dbContext;
    }

    public override async Task Enter(CancellationToken cancellationToken)
    {
        var user = await _dbContext.Users
            .Where(x => x.Id == UserId)
            .FirstOrDefaultAsync(cancellationToken);

        if (user == null)
        {
            throw new NotFoundException(nameof(User), UserId);
        }

        user.IsBlocked = true;
        user.UserBlockReasonId = _blockReasonId;

        user.DomainEvents.Add(new UserBlockedEvent(user));

        await _dbContext.SaveChangesAsync(cancellationToken);
    }
}
