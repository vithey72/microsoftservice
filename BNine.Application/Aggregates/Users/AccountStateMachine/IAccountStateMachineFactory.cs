﻿namespace BNine.Application.Aggregates.Users.AccountStateMachine;

public interface IAccountStateMachineFactory
{
    IAccountStateMachine CreateAccountStateMachine();
}
