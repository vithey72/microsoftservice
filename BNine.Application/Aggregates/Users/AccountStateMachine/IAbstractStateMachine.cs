﻿namespace BNine.Application.Aggregates.Users.AccountStateMachine;

public interface IAbstractStateMachine<State> where State : IAsbtractState
{
    void WithState(State initialState);

    State CurrentState
    {
        get;
    }

    Task MoveTo(State nextState, CancellationToken cancellationToken);
}
