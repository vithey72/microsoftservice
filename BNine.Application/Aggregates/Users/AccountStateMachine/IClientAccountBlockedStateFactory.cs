﻿namespace BNine.Application.Aggregates.Users.AccountStateMachine;

public interface IClientAccountBlockedStateFactory
{
    IAccountActiveState CreateActiveState(Guid userId, IAccountStateMachine stateMachine);

    IAccountGenericBlockedState CreateBlockedState(Guid userId, Guid blockReasonId, IAccountStateMachine stateMachine);

    IAccountPendingClosureBlockedState CreatePendingClosureState(Guid userId, IAccountStateMachine stateMachine);

    IAccountClosedState CreateClosedState(Guid userId, Guid closureReasonId, IAccountStateMachine stateMachine);
}
