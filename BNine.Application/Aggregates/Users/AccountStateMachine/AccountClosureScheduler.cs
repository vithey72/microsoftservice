﻿namespace BNine.Application.Aggregates.Users.AccountStateMachine;

using System;
using System.Threading.Tasks;
using BNine.Application.Interfaces;
using BNine.Domain.Entities.User;
using Microsoft.EntityFrameworkCore;

public class AccountClosureScheduler : IAccountClosureScheduler
{
    private readonly IBNineDbContext _dbContext;
    private readonly IDateTimeUtcProviderService _dateTimeUtcProviderService;
    private readonly IClientAccountSettingsProvider _clientAccountSettingsProvider;

    public AccountClosureScheduler(
        IBNineDbContext dbContext,
        IDateTimeUtcProviderService dateTimeUtcProviderService,
        IClientAccountSettingsProvider clientAccountSettingsProvider
        )
    {
        _dbContext = dbContext;
        _dateTimeUtcProviderService = dateTimeUtcProviderService;
        _clientAccountSettingsProvider = clientAccountSettingsProvider;
    }

    public async Task RemoveAccountClosureSchedule(Guid userId, CancellationToken cancellationToken)
    {
        var schedule = await _dbContext.AccountClosureSchedules.FirstOrDefaultAsync(x => x.UserId == userId, cancellationToken);
        if (schedule is null)
        {
            return;
        }

        _dbContext.AccountClosureSchedules.Remove(schedule);

        await _dbContext.SaveChangesAsync(cancellationToken);
    }

    public async Task ScheduleAccountClosure(Guid userId, CancellationToken cancellationToken)
    {
        var schedule = await _dbContext.AccountClosureSchedules.FirstOrDefaultAsync(x => x.UserId == userId, cancellationToken);
        if (schedule is not null)
        {
            return;
        }

        schedule = new AccountClosureSchedule
        {
            UserId = userId,
        };

        _dbContext.AccountClosureSchedules.Add(schedule);

        await _dbContext.SaveChangesAsync(cancellationToken);
    }

    public async Task<Guid[]> GetUsersWhoseTimeHasCome(CancellationToken cancellationToken)
    {
        var date = _dateTimeUtcProviderService.UtcNow();
        var settings = await _clientAccountSettingsProvider.GetClientAccountSettings(cancellationToken);
        var lastScheduledDateToCheck = date.AddDays(-settings.VoluntaryClosureDelayInDays);

        var userIds = await _dbContext.AccountClosureSchedules
            .Where(x => x.CreatedAt <= lastScheduledDateToCheck)
            .Select(x => x.UserId)
            .ToArrayAsync();

        return userIds;
    }
}
