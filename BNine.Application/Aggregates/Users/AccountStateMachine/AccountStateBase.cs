﻿namespace BNine.Application.Aggregates.Users.AccountStateMachine;

public abstract class AccountStateBase : IClientAccountState
{
    public AccountStateBase(Guid userId)
    {
        UserId = userId;
    }

    protected Guid UserId
    {
        get;
    }

    public virtual Task Enter(CancellationToken cancellationToken)
    {
        return Task.CompletedTask;
    }

    public virtual Task Exit(CancellationToken cancellationToken)
    {
        return Task.CompletedTask;
    }

    public virtual Task BlockAccount(Guid blockReasonId, CancellationToken cancellationToken)
    {
        throw new NotSupportedException();
    }

    public virtual Task CloseAccount(Guid closureReasonId, CancellationToken cancellationToken)
    {
        throw new NotSupportedException();
    }

    public virtual Task InitiateAccountClosure(CancellationToken cancellationToken)
    {
        throw new NotSupportedException();
    }

    public virtual Task UnblockAccount(CancellationToken cancellationToken)
    {
        throw new NotSupportedException();
    }
}
