﻿namespace BNine.Application.Aggregates.Users.AccountStateMachine;

using BNine.Application.Interfaces;

public class AccountGenericBlockedState : AccountBlockedStateBase, IAccountGenericBlockedState
{
    private readonly IAccountStateMachine _stateMachine;
    private readonly IClientAccountBlockedStateFactory _stateFactory;

    public AccountGenericBlockedState(
        Guid userId,
        Guid blockReasonId,
        IAccountStateMachine stateMachine,
        IClientAccountBlockedStateFactory stateFactory,
        IBNineDbContext dbContext
        ) : base(userId, blockReasonId, dbContext)
    {
        _stateMachine = stateMachine;
        _stateFactory = stateFactory;
    }

    public override async Task UnblockAccount(CancellationToken cancellationToken)
    {
        var activeState = _stateFactory.CreateActiveState(UserId, _stateMachine);

        await _stateMachine.MoveTo(activeState, cancellationToken);
    }

    public override Task BlockAccount(Guid blockReasonId, CancellationToken cancellationToken)
    {
        return Task.CompletedTask;
    }

    public override async Task InitiateAccountClosure(CancellationToken cancellationToken)
    {
        var pendingClosureState = _stateFactory.CreatePendingClosureState(UserId, _stateMachine);

        await _stateMachine.MoveTo(pendingClosureState, cancellationToken);
    }

    public override async Task CloseAccount(Guid closureReasonId, CancellationToken cancellationToken)
    {
        var closedState = _stateFactory.CreateClosedState(UserId, closureReasonId, _stateMachine);

        await _stateMachine.MoveTo(closedState, cancellationToken);
    }
}
