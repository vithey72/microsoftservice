﻿namespace BNine.Application.Aggregates.Users.AccountStateMachine;

public interface IClientAccountState : IAsbtractState
{
    Task InitiateAccountClosure(CancellationToken cancellationToken);

    Task UnblockAccount(CancellationToken cancellationToken);

    Task BlockAccount(Guid blockReasonId, CancellationToken cancellationToken);

    Task CloseAccount(Guid closureReasonId, CancellationToken cancellationToken);
}
