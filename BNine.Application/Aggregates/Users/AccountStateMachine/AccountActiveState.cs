﻿namespace BNine.Application.Aggregates.Users.AccountStateMachine;

using BNine.Application.Exceptions;
using BNine.Application.Interfaces;
using BNine.Domain.Entities.User;
using BNine.Domain.Events;
using Microsoft.EntityFrameworkCore;

public class AccountActiveState : AccountStateBase, IAccountActiveState
{
    private readonly IAccountStateMachine _stateMachine;
    private readonly IClientAccountBlockedStateFactory _stateFactory;
    private readonly IBNineDbContext _dbContext;

    public AccountActiveState(
        Guid userId,
        IAccountStateMachine stateMachine,
        IClientAccountBlockedStateFactory stateFactory,
        IBNineDbContext dbContext
        ) : base(userId)
    {
        _stateMachine = stateMachine;
        _stateFactory = stateFactory;
        _dbContext = dbContext;
    }

    public override async Task Enter(CancellationToken cancellationToken)
    {
        var user = await _dbContext.Users
            .Where(x => x.Id == UserId)
            .FirstOrDefaultAsync();

        if (user == null)
        {
            throw new NotFoundException(nameof(User), UserId);
        }

        user.Status = Enums.UserStatus.Active;
        user.IsBlocked = false;
        user.UserBlockReasonId = null;

        user.DomainEvents.Add(new UserUnblockedEvent(user));

        await _dbContext.SaveChangesAsync(cancellationToken);
    }

    public override Task UnblockAccount(CancellationToken cancellationToken)
    {
        return Task.CompletedTask;
    }

    public override async Task BlockAccount(Guid blockReasonId, CancellationToken cancellationToken)
    {
        var blockedState = _stateFactory.CreateBlockedState(UserId, blockReasonId, _stateMachine);

        await _stateMachine.MoveTo(blockedState, cancellationToken);
    }

    public override async Task InitiateAccountClosure(CancellationToken cancellationToken)
    {
        var pendingClosureState = _stateFactory.CreatePendingClosureState(UserId, _stateMachine);

        await _stateMachine.MoveTo(pendingClosureState, cancellationToken);
    }

    public override async Task CloseAccount(Guid closureReasonId, CancellationToken cancellationToken)
    {
        var closedState = _stateFactory.CreateClosedState(UserId, closureReasonId, _stateMachine);

        await _stateMachine.MoveTo(closedState, cancellationToken);
    }
}
