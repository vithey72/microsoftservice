﻿namespace BNine.Application.Aggregates.Users.AccountStateMachine;

using System.Threading;
using System.Threading.Tasks;

public class AccountStateMachine : IAccountStateMachine
{
    public AccountStateMachine()
    {
    }

    public void WithState(IAsbtractState initialState)
    {
        if (CurrentState != null)
        {
            throw new InvalidOperationException();
        }

        CurrentState = initialState;
    }

    public IAsbtractState CurrentState
    {
        get; private set;
    }

    public async Task MoveTo(IAsbtractState nextState, CancellationToken cancellationToken)
    {
        await CurrentState.Exit(cancellationToken);
        CurrentState = nextState;
        await CurrentState.Enter(cancellationToken);
    }
}
