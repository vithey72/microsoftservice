﻿namespace BNine.Application.Aggregates.Users.AccountStateMachine;

public interface IAsbtractState
{
    Task Enter(CancellationToken cancellationToken);

    Task Exit(CancellationToken cancellationToken);
}
