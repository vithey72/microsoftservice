﻿namespace BNine.Application.Aggregates.Users.AccountStateMachine;

public interface IAccountClosureScheduler
{
    Task ScheduleAccountClosure(Guid userId, CancellationToken cancellationToken);

    Task RemoveAccountClosureSchedule(Guid userId, CancellationToken cancellationToken);

    Task<Guid[]> GetUsersWhoseTimeHasCome(CancellationToken cancellationToken);
}
