﻿namespace BNine.Application.Aggregates.Users.AccountStateMachine;

#nullable enable

public interface IClientAccountBlockedStateProvider
{
    Task<IClientAccountState> GetCurrentAccountBlockedState(CancellationToken cancellationToken);

    Task<IClientAccountState> GetCustomAccountBlockedState(Guid userId, CancellationToken cancellationToken);

    Task<IAccountPendingClosureBlockedState?> TryGetCustomAccountPendingClosureBlockedState(Guid userId, CancellationToken cancellationToken);
}
