﻿namespace BNine.Application.Aggregates.Users.Events
{
    using MediatR;
    using Newtonsoft.Json;

    public class UserBalanceReachedEvent : INotification
    {
        [JsonProperty("userId")]
        public string UserId
        {
            get; set;
        }
    }
}
