﻿namespace BNine.Application.Aggregates.Devices.Commands.AddDetails
{
    using System.Dynamic;
    using Abstractions;
    using AutoMapper;
    using Constants;
    using Domain.Entities;
    using Exceptions;
    using Interfaces;
    using MediatR;
    using Microsoft.EntityFrameworkCore;
    using Newtonsoft.Json;
    using ServiceBusEvents.Commands.PublishB9Event;

    public class AddDeviceDetailsCommandHandler : AbstractRequestHandler, IRequestHandler<AddDeviceDetailsCommand, Unit>
    {
        public AddDeviceDetailsCommandHandler(
            IMediator mediator,
            IBNineDbContext dbContext,
            IMapper mapper,
            ICurrentUserService currentUser) : base(mediator, dbContext, mapper, currentUser)
        {
        }

        public async Task<Unit> Handle(AddDeviceDetailsCommand request, CancellationToken cancellationToken)
        {
            var detailsWithIp = AddIpToDetails(request.Details, request.ExternalIpAddress);
            var command = new PublishB9EventCommand(new
            {
                Details = detailsWithIp,
                UserId = CurrentUser.UserId!.Value,
            }, ServiceBusTopics.B9DeviceDetails, ServiceBusEvents.B9Created);

            // cannot identify device
            if (string.IsNullOrEmpty(request.ExternalId))
            {
                await Mediator.Send(command);
                return Unit.Value;
            }

            var device = await DbContext.Devices
                .Where(x => x.ExternalDeviceId.Equals(request.ExternalId) && x.Type == request.Platform)
                .FirstOrDefaultAsync();

            if (device == null)
            {
                await Mediator.Send(command);
                throw new NotFoundException(nameof(Device), request.ExternalId);
            }

            await Mediator.Send(new PublishB9EventCommand(new
            {
                Details = detailsWithIp,
                UserId = CurrentUser.UserId.Value,
                DeviceId = device.Id,
                ExternalDeviceId = device.ExternalDeviceId,
            }, ServiceBusTopics.B9DeviceDetails, ServiceBusEvents.B9Created));

            return Unit.Value;
        }

        private object AddIpToDetails(object detailsJson, string externalIpAddress)
        {
            dynamic details = detailsJson == null ? new ExpandoObject() : JsonConvert.DeserializeObject<ExpandoObject>(JsonConvert.SerializeObject(detailsJson));
            details.externalIpAddress = externalIpAddress;
            return details;
        }
    }
}
