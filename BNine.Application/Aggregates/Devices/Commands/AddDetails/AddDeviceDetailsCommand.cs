﻿namespace BNine.Application.Aggregates.Devices.Commands.AddDetails
{
    using Enums;
    using MediatR;
    using Newtonsoft.Json;

    public class AddDeviceDetailsCommand : IRequest
    {
        [JsonProperty("platform")]
        public MobilePlatform Platform
        {
            get;
            set;
        }

        [JsonProperty("externalId")]
        public string ExternalId
        {
            get;
            set;
        }

        [JsonProperty("details")]
        public object Details
        {
            get;
            set;
        }

        [JsonProperty("externalIpAddress")]
        public string ExternalIpAddress
        {
            get;
            set;
        }
    }
}
