﻿namespace BNine.Application.Aggregates.Devices.Commands.AddDetails
{
    using FluentValidation;

    public class AddDeviceDetailsCommandValidator : AbstractValidator<AddDeviceDetailsCommand>
    {
        public AddDeviceDetailsCommandValidator()
        {
            RuleFor(x => x.Platform).IsInEnum();
            RuleFor(x => x.Details).NotEmpty();
        }
    }
}
