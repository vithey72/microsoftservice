﻿namespace BNine.Application.Aggregates.Devices.Commands.RegisterDevice
{
    using System;
    using System.Linq;
    using System.Threading;
    using System.Threading.Tasks;
    using AutoMapper;
    using BNine.Application.Abstractions;
    using BNine.Application.Interfaces;
    using BNine.Application.Models.Notifications;
    using MediatR;

    public class RegisterDeviceCommandHandler
        : AbstractRequestHandler
        , IRequestHandler<RegisterDeviceCommand, Guid>
    {
        private IPushNotificationService PushNotificationsService
        {
            get;
        }

        public RegisterDeviceCommandHandler(
            IMediator mediator,
            IBNineDbContext dbContext,
            IMapper mapper,
            ICurrentUserService currentUser,
            IPushNotificationService pushNotificationService
            )
            : base(mediator, dbContext, mapper, currentUser)
        {
            PushNotificationsService = pushNotificationService;
        }

        public async Task<Guid> Handle(RegisterDeviceCommand request, CancellationToken cancellationToken)
        {
            if (string.IsNullOrEmpty(request.ExternalDeviceId))
            {
                return Guid.Empty;
            }

            var device = DbContext.Devices
                .FirstOrDefault(x => x.ExternalDeviceId == request.ExternalDeviceId && x.Type == request.DeviceType);

            if (device == null)
            {
                var dal = Mapper.Map<RegisterDeviceCommand, Domain.Entities.Device>(request,
                    opts => opts.AfterMap(
                        (src, dest) =>
                        {
                            dest.UserId = CurrentUser.UserId.Value;
                        }));
                device = DbContext.Devices.Add(dal).Entity;
            }
            else
            {
                device.UserId = CurrentUser.UserId.Value;
                device.UpdatedAt = DateTime.UtcNow;
                DbContext.Devices.Update(device);
            }
            await DbContext.SaveChangesAsync(cancellationToken);

            if (string.IsNullOrEmpty(device.RegistrationId))
            {
                var registrationId = await PushNotificationsService.CreateRegistrationId();
                await PushNotificationsService.RegisterForPushNotifications(registrationId, new DeviceRegistration
                {
                    Handle = request.ExternalDeviceId,
                    Platform = request.DeviceType,
                    Tags = new string[1] { device.Id.ToString() }
                });

                device.RegistrationId = registrationId;

                await DbContext.SaveChangesAsync(cancellationToken);
            }

            return device.Id;
        }
    }
}
