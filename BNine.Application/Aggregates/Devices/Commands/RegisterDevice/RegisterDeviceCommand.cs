﻿namespace BNine.Application.Aggregates.Devices.Commands.RegisterDevice
{
    using System;
    using AutoMapper;
    using BNine.Application.Mappings;
    using BNine.Enums;
    using MediatR;

    public class RegisterDeviceCommand : IRequest<Guid>, IMapTo<Domain.Entities.Device>
    {
        public MobilePlatform DeviceType
        {
            get; set;
        }

        public string ExternalDeviceId
        {
            get; set;
        }

        public void Mapping(Profile profile)
        {
            profile.CreateMap<RegisterDeviceCommand, Domain.Entities.Device>()
                .ForMember(dst => dst.Type, opt => opt.MapFrom(src => src.DeviceType))
                .ForMember(dst => dst.ExternalDeviceId, opt => opt.MapFrom(src => src.ExternalDeviceId));
        }
    }
}
