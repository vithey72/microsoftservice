﻿namespace BNine.Application.Aggregates.Devices.Commands.UnregisterDevice
{
    using System;
    using System.Linq;
    using System.Threading;
    using System.Threading.Tasks;
    using AutoMapper;
    using BNine.Application.Abstractions;
    using BNine.Application.Interfaces;
    using MediatR;

    public class UnregisterDeviceCommandHandler
        : AbstractRequestHandler, IRequestHandler<UnregisterDeviceCommand, Unit>
    {
        private IPushNotificationService PushNotificationsService
        {
            get;
        }

        public UnregisterDeviceCommandHandler(IMediator mediator, IBNineDbContext dbContext, IMapper mapper, ICurrentUserService currentUser, IPushNotificationService pushNotificationsService)
            : base(mediator, dbContext, mapper, currentUser)
        {
            PushNotificationsService = pushNotificationsService;
        }

        public async Task<Unit> Handle(UnregisterDeviceCommand request, CancellationToken cancellationToken)
        {
            var device = DbContext.Devices
                .FirstOrDefault(x => x.ExternalDeviceId == request.ExternalDeviceId);

            if (device == null)
            {
                throw new Exceptions.NotFoundException(nameof(Domain.Entities.Device), request.ExternalDeviceId);
            }

            if (device.UserId != CurrentUser.UserId)
            {
                throw new Exceptions.NotFoundException(nameof(Domain.Entities.Device), device.Id.ToString());
            }

            try
            {
                await PushNotificationsService.DeleteRegistration(device.RegistrationId);
            }
            catch (Exception)
            {

            }
            finally
            {
                device.RegistrationId = null;
                device.UpdatedAt = DateTime.UtcNow;

                DbContext.Devices.Update(device);

                await DbContext.SaveChangesAsync(cancellationToken);
            }

            return Unit.Value;
        }
    }
}
