﻿namespace BNine.Application.Aggregates.Devices.Commands.UnregisterDevice
{
    using MediatR;

    public class UnregisterDeviceCommand : IRequest<Unit>
    {
        public UnregisterDeviceCommand(string externalDeviceId)
        {
            ExternalDeviceId = externalDeviceId;
        }

        public string ExternalDeviceId
        {
            get; set;
        }
    }
}
