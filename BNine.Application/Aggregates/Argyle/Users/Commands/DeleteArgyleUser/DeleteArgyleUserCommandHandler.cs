﻿namespace BNine.Application.Aggregates.Argyle.Users.Commands.DeleteArgyleUser
{
    using System;
    using System.Linq;
    using System.Threading;
    using System.Threading.Tasks;
    using AutoMapper;
    using BNine.Application.Abstractions;
    using BNine.Application.Aggregates.LogAdminAction.Commands;
    using BNine.Application.Exceptions;
    using BNine.Application.Interfaces;
    using BNine.Application.Interfaces.Argyle;
    using BNine.Domain.Entities.User.EarlySalary;
    using BNine.Enums;
    using MediatR;
    using Microsoft.EntityFrameworkCore;
    using Newtonsoft.Json;

    public class DeleteArgyleUserCommandHandler
        : AbstractRequestHandler, IRequestHandler<DeleteArgyleUserCommand, Unit>
    {
        private IArgyleUsersService ArgyleUsersService
        {
            get;
        }

        public DeleteArgyleUserCommandHandler(IArgyleUsersService argyleUsersService, IMediator mediator,
            IBNineDbContext dbContext, IMapper mapper, ICurrentUserService currentUser) : base(mediator, dbContext,
            mapper, currentUser)
        {
            ArgyleUsersService = argyleUsersService;
        }

        public async Task<Unit> Handle(DeleteArgyleUserCommand request, CancellationToken cancellationToken)
        {
            var user = await DbContext.Users
                .Include(x => x.EarlySalaryWidgetConfiguration)
                .Where(x => x.Id == request.UserId)
                .FirstOrDefaultAsync(cancellationToken);

            if (user == null || user.EarlySalaryWidgetConfiguration == null ||
                user.EarlySalaryWidgetConfiguration.DeletedAt.HasValue)
            {
                throw new NotFoundException(nameof(EarlySalaryWidgetConfiguration), new { userId = request.UserId });
            }

            var userOldValue = JsonConvert.SerializeObject(user, Formatting.None,
                new JsonSerializerSettings() { ReferenceLoopHandling = ReferenceLoopHandling.Ignore });

            await ArgyleUsersService.DeleteUser(user.EarlySalaryWidgetConfiguration.WidgetUserId);

            user.EarlySalaryWidgetConfiguration.DeletedAt = DateTime.UtcNow;

            await DbContext.SaveChangesAsync(cancellationToken);

            var userNewValue = JsonConvert.SerializeObject(user, Formatting.None,
                new JsonSerializerSettings() { ReferenceLoopHandling = ReferenceLoopHandling.Ignore });

            await SendLogAdminActionCommand(userOldValue, userNewValue, user.Id.ToString());

            return Unit.Value;
        }

        private async Task SendLogAdminActionCommand(string userOldValue, string userNewValue, string clientId)
        {
            await Mediator.Send(new LogAdminActionCommand()
            {
                EntityName = nameof(Domain.Entities.User.User),
                Type = AdminActionType.DeleteArgyleData,
                EntityId = clientId,
                BeforeChanges = userOldValue,
                AfterChanges = userNewValue
            });
        }
    }
}
