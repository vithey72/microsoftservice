﻿namespace BNine.Application.Aggregates.Argyle.Users.Commands.DeleteArgyleUser
{
    using System;
    using MediatR;

    public class DeleteArgyleUserCommand : IRequest<Unit>
    {
        public DeleteArgyleUserCommand(Guid userId)
        {
            UserId = userId;
        }

        public Guid UserId
        {
            get;
        }
    }
}
