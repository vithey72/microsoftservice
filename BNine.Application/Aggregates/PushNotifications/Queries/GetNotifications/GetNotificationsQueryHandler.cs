﻿namespace BNine.Application.Aggregates.Notifications.Queries.GetNotifications
{
    using System.Linq;
    using System.Threading;
    using System.Threading.Tasks;
    using AutoMapper;
    using BNine.Application.Abstractions;
    using BNine.Application.Aggregates.Notifications.Models;
    using BNine.Application.Interfaces;
    using BNine.Application.Models;
    using Enums;
    using Helpers;
    using MediatR;
    using Microsoft.EntityFrameworkCore;

    public class GetNotificationsQueryHandler
        : AbstractRequestHandler
        , IRequestHandler<GetNotificationsQuery, PaginatedList<Notification>>
    {
        private readonly IPartnerProviderService _partnerProvider;

        public GetNotificationsQueryHandler(
            IMediator mediator,
            IBNineDbContext dbContext,
            IMapper mapper,
            ICurrentUserService currentUser,
            IPartnerProviderService partnerProvider
            )
            : base(mediator, dbContext, mapper, currentUser)
        {
            _partnerProvider = partnerProvider;
        }

        public async Task<PaginatedList<Notification>> Handle(GetNotificationsQuery request, CancellationToken cancellationToken)
        {
            var userNotifications = DbContext.Notifications
                                        .Include(n => n.NotificationType)
                                        .Where(x => x.UserId == CurrentUser.UserId
                                                    && x.IsImportant
                                                    && x.Type != NotificationTypeEnum.Transaction
                                                    && (request.Filter.Count == 0 || request.Filter.Contains(x.Type)));

            if (request.SkipReadNotificationsBefore.HasValue)
            {
                userNotifications = userNotifications.Where(n => !n.IsRead || n.CreatedAt > request.SkipReadNotificationsBefore);
            }

            var dalNotifications = await userNotifications
                .OrderByDescending(x => x.CreatedAt)
                .Skip(request.Skip)
                .Take(request.Take)
                .ToListAsync(cancellationToken);

            var notificationsList = dalNotifications
                .Select(Mapper.Map<Notification>)
                .ToList();

            foreach (var notification in notificationsList)
            {
                notification.Title = StringProvider.ReplaceCustomizedStrings(notification.Text, _partnerProvider);
                notification.Text = StringProvider.ReplaceCustomizedStrings(notification.Text, _partnerProvider);
            }

            return new PaginatedList<Notification>(
                notificationsList,
                userNotifications.Count(),
                request.Skip,
                request.Take);
        }
    }
}
