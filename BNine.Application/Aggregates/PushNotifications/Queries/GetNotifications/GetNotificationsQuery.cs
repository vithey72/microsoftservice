﻿namespace BNine.Application.Aggregates.Notifications.Queries.GetNotifications
{
    using System;
    using BNine.Application.Aggregates.Notifications.Models;
    using BNine.Application.Models;
    using BNine.Enums;
    using MediatR;
    using Newtonsoft.Json;

    public class GetNotificationsQuery : IRequest<PaginatedList<Notification>>
    {
        [JsonProperty("skip")]
        public int Skip
        {
            get; set;
        }

        [JsonProperty("take")]
        public int Take
        {
            get; set;
        }

        [JsonProperty("filter")]
        public List<NotificationTypeEnum> Filter
        {
            get; set;
        }

        public DateTime? SkipReadNotificationsBefore
        {
            get; set;
        } = DateTime.UtcNow.AddDays(-10);
    }
}
