﻿namespace BNine.Application.Aggregates.Notifications.Models
{
    using System;
    using Newtonsoft.Json;
    using AutoMapper;
    using BNine.Application.Mappings;
    using BNine.Enums;

    public class Notification : IMapFrom<Domain.Entities.Notification.Notification>
    {
        [JsonProperty("id")]
        public Guid Id
        {
            get; set;
        }

        [JsonProperty("text")]
        public string Text
        {
            get; set;
        }

        [JsonProperty("title")]
        public string Title
        {
            get; set;
        }

        [JsonProperty("isRead")]
        public bool IsRead
        {
            get; set;
        }

        [JsonProperty("createdAt")]
        public DateTime CreatedAt
        {
            get; set;
        }

        [JsonProperty("type")]
        public NotificationTypeEnum Type
        {
            get; set;
        }

        [JsonProperty("icon")]
        public string Icon
        {
            get; set;
        }

        [JsonProperty("deeplink")]
        public string Deeplink
        {
            get; set;
        }

        public void Mapping(Profile profile)
        {
            profile.CreateMap<Domain.Entities.Notification.Notification, Notification>()
                  .ForMember(s => s.Icon, e => e.MapFrom(en => en.NotificationType.Icon));
        }
    }
}
