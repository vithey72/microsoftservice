﻿namespace BNine.Application.Aggregates.Notifications.Models
{
    using System;
    using System.Collections.Generic;
    using Newtonsoft.Json;

    public class RecipientInfo
    {
        [JsonProperty("id")]
        public Guid UserId
        {
            get;
            set;
        }

        [JsonProperty("params")]
        public IDictionary<string, string> Parameters
        {
            get;
            set;
        } = new Dictionary<string, string>();
    }
}
