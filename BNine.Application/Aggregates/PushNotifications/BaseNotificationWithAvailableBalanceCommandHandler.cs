﻿namespace BNine.Application.Aggregates.PushNotifications
{
    using System.Threading.Tasks;
    using AutoMapper;
    using Interfaces;
    using Interfaces.Bank.Administration;
    using MediatR;

    public abstract class BaseNotificationWithAvailableBalanceCommandHandler : BaseSendNotificationCommandHandler
    {
        protected IBankSavingAccountsService BankAccountService
        {
            get;
        }

        protected BaseNotificationWithAvailableBalanceCommandHandler(
            IBankSavingAccountsService bankAccountService,
            IPushNotificationService pushNotificationService,
            IMediator mediator,
            IBNineDbContext dbContext,
            IMapper mapper,
            ICurrentUserService currentUser
            ) : base(pushNotificationService, mediator, dbContext, mapper, currentUser)
        {
            BankAccountService = bankAccountService;
        }

        protected async Task<decimal> GetAvailableBalance(decimal? availableBalance, int savingAccountId)
        {
            if (availableBalance.HasValue)
            {
                return availableBalance.Value;
            }

            var accountInfo = await BankAccountService.GetSavingAccountInfo(savingAccountId);

            return accountInfo.AvailableBalance;
        }
    }
}
