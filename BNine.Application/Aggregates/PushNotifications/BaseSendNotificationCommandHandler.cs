﻿namespace BNine.Application.Aggregates.PushNotifications
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using AutoMapper;
    using BNine.Application.Abstractions;
    using BNine.Application.Interfaces;
    using BNine.Application.Models.Notifications;
    using BNine.Domain.Entities;
    using BNine.Enums;
    using MediatR;

    public abstract class BaseSendNotificationCommandHandler
        : AbstractRequestHandler
    {
        private IPushNotificationService PushNotificationService
        {
            get;
        }

        protected BaseSendNotificationCommandHandler(
            IPushNotificationService pushNotificationService,
            IMediator mediator,
            IBNineDbContext dbContext,
            IMapper mapper,
            ICurrentUserService currentUser
            ) : base(mediator, dbContext, mapper, currentUser)
        {
            PushNotificationService = pushNotificationService;
        }

        protected async Task NotifyUser(Guid userId, string notificationText, bool isNotificationsEnabled, NotificationTypeEnum type, bool isImportant, IEnumerable<Device> devices, string deeplink, string appsflyerEventType, string category, string title = null, string sub1 = null, string hookId = null)
        {
            // to prevent sending duplicate notifications
            if (Guid.TryParse(hookId, out var hookIdGuid))
            {
                if (DbContext.Notifications.Any(n => n.HookId == hookIdGuid))
                {
                    return;
                }
            }

            if (isNotificationsEnabled)
            {
                var notificationKit = devices.Select(x => new NotificationTarget(x.Id.ToString(), x.Type)).ToList();

                var notification = new Models.Notifications.Notification(notificationText, notificationKit, deeplink, appsflyerEventType, title, sub1);

                await PushNotificationService.SendNotification(notification);
            }

            DbContext.Notifications.Add(new Domain.Entities.Notification.Notification
            {
                CreatedAt = DateTime.UtcNow,
                Text = notificationText,
                Title = title,
                IsRead = false,
                UserId = userId,
                Category = category,
                Type = type,
                IsImportant = isImportant,
                Deeplink = deeplink,
                HookId = hookId == null ? null : Guid.Parse(hookId)
            });

            await DbContext.SaveChangesAsync(CancellationToken.None);
        }
    }
}
