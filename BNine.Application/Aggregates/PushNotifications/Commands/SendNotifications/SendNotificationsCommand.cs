﻿namespace BNine.Application.Aggregates.Notifications.Commands.SendNotifications
{
    using System.Collections.Generic;
    using BNine.Enums;
    using Constants;
    using MediatR;
    using Models;
    using Newtonsoft.Json;

    public class SendNotificationsCommand : IRequest
    {
        [JsonProperty("message")]
        public string Message
        {
            get;
            set;
        }

        [JsonProperty("recipients")]
        public IEnumerable<RecipientInfo> Recipients
        {
            get;
            set;
        } = new List<RecipientInfo>();

        [JsonProperty("privateKey")]
        public string PrivateKey
        {
            get;
            set;
        }

        [JsonProperty("forcedSend")]
        public bool ForcedSend
        {
            get;
            set;
        }

        /// <summary>
        /// One of strings from <see cref="DeepLinkRoutes"/>.
        /// </summary>
        [JsonProperty("deeplink")]
        public string Deeplink
        {
            get;
            set;
        }

        [JsonProperty("appsflyerEventType")]
        public string AppsflyerEventType
        {
            get;
            set;
        }

        [JsonProperty("title")]
        public string Title
        {
            get;
            set;
        }

        [JsonProperty("category")]
        public string Category
        {
            get;
            set;
        }

        [JsonProperty("type")]
        public NotificationTypeEnum? Type
        {
            get;
            set;
        }
    }
}
