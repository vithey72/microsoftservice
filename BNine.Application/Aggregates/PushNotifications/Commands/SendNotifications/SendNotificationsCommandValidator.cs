﻿namespace BNine.Application.Aggregates.Notifications.Commands.SendNotifications
{
    using FluentValidation;
    using Microsoft.Extensions.Configuration;

    public class SendNotificationsCommandValidator : AbstractValidator<SendNotificationsCommand>
    {
        public SendNotificationsCommandValidator(IConfiguration configuration)
        {
            RuleFor(x => x.Message).NotEmpty();
            RuleFor(x => x.Recipients).NotEmpty();

            RuleFor(x => x.PrivateKey)
                .Equal(configuration["APIKey"])
                .WithMessage("Invalid private key");
        }
    }
}
