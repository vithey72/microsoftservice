﻿namespace BNine.Application.Aggregates.Notifications.Commands.SendNotifications
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text.RegularExpressions;
    using System.Threading;
    using System.Threading.Tasks;
    using Abstractions;
    using AutoMapper;
    using BNine.Application.Aggregates.Notifications.Commands.SendBulkNotifications;
    using BNine.Enums;
    using Domain.Entities;
    using Interfaces;
    using MediatR;

    public class SendNotificationsCommandHandler : AbstractRequestHandler, IRequestHandler<SendNotificationsCommand>
    {
        private readonly Regex regex = new Regex(@"\{(\w+)\}", RegexOptions.Compiled);

        private INotificationUsersService NotificationService
        {
            get;
        }

        public SendNotificationsCommandHandler(
            INotificationUsersService notificationService,
            IMediator mediator,
            IBNineDbContext dbContext,
            IMapper mapper,
            ICurrentUserService currentUser) : base(mediator, dbContext, mapper, currentUser)
        {
            NotificationService = notificationService;
        }


        public async Task<Unit> Handle(SendNotificationsCommand request, CancellationToken cancellationToken)
        {
            if (!request.Recipients.Any(x => x.Parameters.Any()))
            {
                await Mediator.Send(new SendBulkNotificationsCommand(request.Message, request.Recipients.Select(x => x.UserId),
                                                                          request.ForcedSend, request.Deeplink, request.AppsflyerEventType,
                                                                          request.Category, request.Title, request.Type ?? NotificationTypeEnum.Others, true));
                return Unit.Value;
            }

            var usersIds = request.Recipients.Select(r => r.UserId).Distinct().ToList();
            var settings = GetUserSettings(usersIds, request.ForcedSend);
            var userDevices = GetDevices(usersIds);

            foreach (var recipient in request.Recipients)
            {
                var devicesIds = userDevices.GetValueOrDefault(recipient.UserId);
                if (devicesIds == null)
                {
                    continue;
                }

                try
                {
                    var message = regex.Replace(request.Message, match => recipient.Parameters[match.Groups[1].Value]);
                    var sub1 = recipient.Parameters.FirstOrDefault(x => x.Key == "sub1").Value;
                    await NotificationService.SendNotification(recipient.UserId, message, settings[recipient.UserId], request.Type ?? NotificationTypeEnum.Others,
                                                                    true, devicesIds, request.Deeplink, request.AppsflyerEventType, request.Category,
                                                                    request.Title, sub1);
                }
                catch (Exception)
                {
                }
            }

            return Unit.Value;
        }

        private Dictionary<Guid, bool> GetUserSettings(List<Guid> usersIds, bool forcedSend)
        {
            if (forcedSend)
            {
                return usersIds.ToDictionary(x => x, x => true);
            }

            return DbContext.Users
                .Where(x => usersIds.Contains(x.Id))
                .Select(x => new { x.Id, x.Settings.IsNotificationsEnabled })
                .ToDictionary(x => x.Id, x => x.IsNotificationsEnabled);
        }

        private Dictionary<Guid, IEnumerable<Device>> GetDevices(IEnumerable<Guid> usersIds)
        {
            var devices = DbContext.Devices
                .Where(x => usersIds.Contains(x.UserId))
                .ToList();

            var userDevices = devices
                .GroupBy(x => x.UserId)
                .ToDictionary(x => x.Key, x => x.Select(r => r));

            return userDevices;
        }
    }
}
