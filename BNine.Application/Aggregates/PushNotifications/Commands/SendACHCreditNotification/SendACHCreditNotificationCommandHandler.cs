﻿namespace BNine.Application.Aggregates.PushNotifications.Commands.SendACHCreditNotification
{
    using System.Globalization;
    using System.Linq;
    using System.Threading;
    using System.Threading.Tasks;
    using AutoMapper;
    using BNine.Application.Interfaces;
    using BNine.Application.Interfaces.Bank.Administration;
    using BNine.Constants;
    using BNine.Enums;
    using Helpers;
    using MediatR;
    using Microsoft.EntityFrameworkCore;

    public class SendACHCreditNotificationCommandHandler
        : BaseSendNotificationCommandHandler, IRequestHandler<SendACHCreditNotificationCommand, Unit>
    {
        private IBankSavingAccountsService BankAccountService
        {
            get;
        }

        public SendACHCreditNotificationCommandHandler(IBankSavingAccountsService bankAccountService, IPushNotificationService pushNotificationService, IMediator mediator, IBNineDbContext dbContext, IMapper mapper, ICurrentUserService currentUser) : base(pushNotificationService, mediator, dbContext, mapper, currentUser)
        {
            BankAccountService = bankAccountService;
        }

        public async Task<Unit> Handle(SendACHCreditNotificationCommand request, CancellationToken cancellationToken)
        {
            var user = await DbContext.Users
                   .Include(x => x.Devices)
                   .Where(x => x.CurrentAccount.ExternalId == request.CreditorAccountId)
                   .FirstOrDefaultAsync();

            var accountInfo = await BankAccountService.GetSavingAccountInfo(request.CreditorAccountId);

            var sender = request.Reference.FirstOrDefault()?.Substring(0, request.Reference.FirstOrDefault().IndexOf(":"));

            var notification = $"{CurrencyFormattingHelper.AsRegular(request.Amount)} (ACH) from {sender} has posted to your account. Balance {accountInfo.AvailableBalance.ToString("C", CultureInfo.CreateSpecificCulture("en-us"))}";

            await NotifyUser(user.Id, notification, user.Settings.IsNotificationsEnabled, NotificationTypeEnum.Transaction, false, user.Devices, null, null, PushNotificationCategory.MoneyReceived, "Money received");

            return Unit.Value;
        }
    }
}
