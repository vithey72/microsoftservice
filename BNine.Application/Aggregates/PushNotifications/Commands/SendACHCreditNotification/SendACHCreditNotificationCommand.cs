﻿namespace BNine.Application.Aggregates.PushNotifications.Commands.SendACHCreditNotification
{
    using System.Collections.Generic;
    using MediatR;

    public class SendACHCreditNotificationCommand : IRequest<Unit>
    {
        public SendACHCreditNotificationCommand(int creditorAccountId, decimal amount, IEnumerable<string> reference)
        {
            CreditorAccountId = creditorAccountId;
            Amount = amount;
            Reference = reference;
        }

        public int CreditorAccountId
        {
            get;
        }

        public decimal Amount
        {
            get;
        }

        public IEnumerable<string> Reference
        {
            get;
        }
    }
}
