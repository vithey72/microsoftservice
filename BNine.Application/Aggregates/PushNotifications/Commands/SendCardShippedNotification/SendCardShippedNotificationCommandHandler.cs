﻿namespace BNine.Application.Aggregates.PushNotifications.Commands.SendCardShippedNotification
{
    using System.Linq;
    using System.Threading;
    using System.Threading.Tasks;
    using AutoMapper;
    using BNine.Enums;
    using Constants;
    using Domain.Entities.User;
    using Exceptions;
    using Helpers;
    using Interfaces;
    using MediatR;
    using Microsoft.EntityFrameworkCore;

    public class SendCardShippedNotificationCommandHandler
        : BaseSendNotificationCommandHandler
        , IRequestHandler<SendCardShippedNotificationCommand, Unit>
    {
        private readonly IPartnerProviderService _partnerProvider;

        public SendCardShippedNotificationCommandHandler(
            IPushNotificationService pushNotificationService,
            IMediator mediator,
            IBNineDbContext dbContext,
            IMapper mapper,
            ICurrentUserService currentUser,
            IPartnerProviderService partnerProvider
        )
            : base(pushNotificationService, mediator, dbContext, mapper, currentUser)
        {
            _partnerProvider = partnerProvider;
        }

        public async Task<Unit> Handle(SendCardShippedNotificationCommand request, CancellationToken cancellationToken)
        {
            var user = await DbContext.Users
                .Where(x => x.ExternalClientId == request.ClientId)
                .Include(x => x.CurrentAccount)
                .Include(x => x.Devices)
                .FirstOrDefaultAsync(cancellationToken);

            if (user == null)
            {
                throw new NotFoundException(nameof(User), new { externalClientId = request.ClientId });
            }

            var category = PushNotificationCategory.AccountCharge;
            var notification = $"{user.FirstName}, your {StringProvider.GetPartnerName(_partnerProvider)} debit card is on its way";

            await NotifyUser(
                user.Id,
                notification,
                user.Settings.IsNotificationsEnabled,
                NotificationTypeEnum.Information,
                true,
                user.Devices,
                null,
                null,
                category,
                StringProvider.GetPartnerName(_partnerProvider));

            return Unit.Value;
        }
    }
}
