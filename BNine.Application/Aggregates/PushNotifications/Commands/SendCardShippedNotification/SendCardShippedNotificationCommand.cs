﻿namespace BNine.Application.Aggregates.PushNotifications.Commands.SendCardShippedNotification
{
    using MediatR;

    public class SendCardShippedNotificationCommand : IRequest<Unit>
    {
        public int ClientId
        {
            get;
            set;
        }
    }
}
