﻿namespace BNine.Application.Aggregates.PushNotifications.Commands.InitUnemploymentProtectionDialog;

using Abstractions;
using AutoMapper;
using Constants;
using Interfaces;
using MediatR;
using Microsoft.EntityFrameworkCore;

public class InitUnemploymentProtectionDialogCommandHandler
    : AbstractRequestHandler
    , IRequestHandler<InitUnemploymentProtectionDialogCommand, Unit>
{
    private readonly INotificationUsersService _notificationService;

    public InitUnemploymentProtectionDialogCommandHandler(
        IMediator mediator,
        IBNineDbContext dbContext,
        IMapper mapper,
        ICurrentUserService currentUser,
        INotificationUsersService notificationService
        )
        : base(mediator, dbContext, mapper, currentUser)
    {
        _notificationService = notificationService;
    }


    public async Task<Unit> Handle(InitUnemploymentProtectionDialogCommand request, CancellationToken cancellationToken)
    {
        var user = DbContext.Users
            .Include(u => u.Devices)
            .Single(x => x.Id == CurrentUser.UserId);

        await _notificationService.SendSilentPushNotification(user, DeepLinkRoutes.UnemploymentProtectionMarketResearch);

        return Unit.Value;
    }
}
