﻿namespace BNine.Application.Aggregates.PushNotifications.Commands.InitUnemploymentProtectionDialog;

using MediatR;

public record InitUnemploymentProtectionDialogCommand : IRequest<Unit>;
