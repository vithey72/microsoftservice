﻿namespace BNine.Application.Aggregates.PushNotifications.Commands.SendUnemploymentProtectionAlertSilentPush;

using MediatR;

public record SendUnemploymentProtectionAlertSilentPushCommand : IRequest<Unit>;
