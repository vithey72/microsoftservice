﻿namespace BNine.Application.Aggregates.PushNotifications.Commands.SendUnemploymentProtectionAlertSilentPush;

using System.Threading.Tasks;
using Abstractions;
using AutoMapper;
using Constants;
using Interfaces;
using MediatR;
using Microsoft.EntityFrameworkCore;

public class SendUnemploymentProtectionAlertSilentPushCommandHandler
    : AbstractRequestHandler
    , IRequestHandler<SendUnemploymentProtectionAlertSilentPushCommand, Unit>
{
    private readonly INotificationUsersService _notificationService;

    public SendUnemploymentProtectionAlertSilentPushCommandHandler(
        IMediator mediator,
        IBNineDbContext dbContext,
        IMapper mapper,
        ICurrentUserService currentUser,
        INotificationUsersService notificationService
        ) : base(mediator, dbContext, mapper, currentUser)
    {
        _notificationService = notificationService;
    }

    public async Task<Unit> Handle(SendUnemploymentProtectionAlertSilentPushCommand request, CancellationToken cancellationToken)
    {
        var user = DbContext
            .Users
            .Include(u => u.Devices)
            .Single(x => x.Id == CurrentUser.UserId);

        await _notificationService.SendSilentPushNotification(user, DeepLinkRoutes.ThanksForYourInterestAlert);

        return Unit.Value;
    }
}
