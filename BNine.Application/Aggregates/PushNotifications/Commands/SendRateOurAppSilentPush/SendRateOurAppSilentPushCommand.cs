﻿namespace BNine.Application.Aggregates.PushNotifications.Commands.SendRateOurAppSilentPush;

using System;
using MediatR;

public record SendRateOurAppSilentPushCommand(Guid UserId) : IRequest<Unit>;
