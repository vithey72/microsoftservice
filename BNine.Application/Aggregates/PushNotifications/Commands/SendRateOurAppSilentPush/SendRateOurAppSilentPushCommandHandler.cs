﻿namespace BNine.Application.Aggregates.PushNotifications.Commands.SendRateOurAppSilentPush;

using System.Threading.Tasks;
using Abstractions;
using AutoMapper;
using Constants;
using Interfaces;
using MediatR;
using Microsoft.EntityFrameworkCore;

public class SendRateOurAppSilentPushCommandHandler
    : AbstractRequestHandler
    , IRequestHandler<SendRateOurAppSilentPushCommand, Unit>
{
    private readonly INotificationUsersService _pushNotificationsService;
    private readonly ICurrentUserService _currentUser;
    private readonly IBNineDbContext _dbContext;

    public SendRateOurAppSilentPushCommandHandler(
        IMediator mediator,
        IBNineDbContext dbContext,
        IMapper mapper,
        ICurrentUserService currentUser,
        INotificationUsersService pushNotificationsService
        )
        : base(mediator, dbContext, mapper, currentUser)
    {
        _pushNotificationsService = pushNotificationsService;
        _currentUser = currentUser;
        _dbContext = dbContext;
    }

    public async Task<Unit> Handle(SendRateOurAppSilentPushCommand request, CancellationToken cancellationToken)
    {
        var user = await DbContext.Users
            .Include(u => u.Devices)
            .SingleAsync(x => x.Id == request.UserId,
                cancellationToken);

        var defaultTipValue = PaidUserServices.Prices.TipsAmounts[PaidUserServices.Prices.DefaultTipIndex];
        var defaultTipGuid = PaidUserServices.AfterAdvanceTipsValuesWithIds.Data[defaultTipValue];
        var nonDefaultTipIds = PaidUserServices.AfterAdvanceTipsValuesWithIds.Data.Values
            .Where(g => g != defaultTipGuid).ToArray();

        if (await UserJustTippedCustom(nonDefaultTipIds, cancellationToken))
        {
            await _pushNotificationsService.SendSilentPushNotification(
                user,
                DeepLinkRoutes.RateUsDialog,
                "Your 5-star rating keeps our team motivated to continuously improve B9.");
        }

        return Unit.Value;
    }

    private async Task<bool> UserJustTippedCustom(Guid[] targetGuids, CancellationToken cancellationToken)
    {
        var threshold = DateTime.Now - TimeSpan.FromMinutes(2);

        var recentTip = await _dbContext.UsedPaidUserServices
            .OrderByDescending(us => us.CreatedAt)
            .FirstOrDefaultAsync(u =>
                    u.UserId == _currentUser.UserId &&
                    targetGuids.Contains(u.ServiceId)
                    && u.CreatedAt > threshold,
                cancellationToken);

        return recentTip != null;
    }
}
