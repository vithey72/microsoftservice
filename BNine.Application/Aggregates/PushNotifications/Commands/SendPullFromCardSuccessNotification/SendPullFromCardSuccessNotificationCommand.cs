﻿namespace BNine.Application.Aggregates.PushNotifications.Commands
{
    using MediatR;

    public class SendPullFromCardSuccessNotificationCommand : IRequest<Unit>
    {
        public SendPullFromCardSuccessNotificationCommand(int clientId, int cardId, decimal amount, string debtorName, string hookId)
        {
            ClientId = clientId;
            CardId = cardId;
            Amount = amount;
            DebtorName = debtorName;
            HookId = hookId;
        }

        public int ClientId
        {
            get;
        }

        public int CardId
        {
            get; set;
        }

        public decimal Amount
        {
            get; set;
        }

        public string DebtorName
        {
            get; set;
        }

        public string HookId
        {
            get; set;
        }
    }
}
