﻿namespace BNine.Application.Aggregates.PushNotifications.Commands.SendPullFromCardSuccessNotification
{
    using System.Threading;
    using System.Threading.Tasks;
    using AutoMapper;
    using BNine.Application.Interfaces;
    using BNine.Application.Interfaces.Bank.Administration;
    using BNine.Constants;
    using BNine.Enums;
    using Helpers;
    using MediatR;
    using Microsoft.EntityFrameworkCore;

    public class SendPullFromCardSuccessNotificationCommandHandler
        : BaseSendNotificationCommandHandler, IRequestHandler<SendPullFromCardSuccessNotificationCommand, Unit>
    {
        private IBankExternalCardsService BankExternalCardsService
        {
            get;
        }

        private IBankSavingAccountsService BankSavingAccountsService
        {
            get;
        }

        public SendPullFromCardSuccessNotificationCommandHandler(
            IBankExternalCardsService bankExternalCardsService,
            IBankSavingAccountsService bankSavingAccountsService,
            IPushNotificationService pushNotificationService,
            IMediator mediator,
            IBNineDbContext dbContext,
            IMapper mapper,
            ICurrentUserService currentUser
            ) : base(pushNotificationService, mediator, dbContext, mapper, currentUser)
        {
            BankExternalCardsService = bankExternalCardsService;
            BankSavingAccountsService = bankSavingAccountsService;
        }

        public async Task<Unit> Handle(SendPullFromCardSuccessNotificationCommand request, CancellationToken cancellationToken)
        {
            var user = await DbContext.Users
               .Include(x => x.Devices)
               .Include(x => x.CurrentAccount)
               .FirstAsync(x => x.ExternalClientId == request.ClientId);

            var savingAccount = await BankSavingAccountsService.GetSavingAccountInfo(user.CurrentAccount.ExternalId);
            var card = await BankExternalCardsService.GetExternalCard(request.ClientId, request.CardId);

            var title = "Money received";
            var category = PushNotificationCategory.MoneyReceived;
            var notification = $"{CurrencyFormattingHelper.AsRegular(request.Amount)} from {card.LastDigits} card has posted to your account. " +
                               $"Balance {CurrencyFormattingHelper.AsRegular(savingAccount.AvailableBalance)}";

            if (!string.IsNullOrEmpty(notification))
            {
                await NotifyUser(user.Id, notification, user.Settings.IsNotificationsEnabled, NotificationTypeEnum.Transaction, false, user.Devices, null, null, category, title, request.HookId);
            }

            return Unit.Value;
        }
    }
}
