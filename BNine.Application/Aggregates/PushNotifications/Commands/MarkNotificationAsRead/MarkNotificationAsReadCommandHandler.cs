﻿namespace BNine.Application.Aggregates.PushNotifications.Commands.MarkNotificationAsRead
{
    using System.Linq;
    using System.Threading;
    using System.Threading.Tasks;
    using Abstractions;
    using AutoMapper;
    using BNine.Application.Exceptions;
    using BNine.Domain.Entities.Notification;
    using Interfaces;
    using MediatR;

    public class MarkNotificationAsReadCommandHandler : AbstractRequestHandler, IRequestHandler<MarkNotificationAsReadCommand>
    {
        public MarkNotificationAsReadCommandHandler(
            IMediator mediator,
            IBNineDbContext dbContext,
            IMapper mapper,
            ICurrentUserService currentUser) : base(mediator, dbContext, mapper, currentUser)
        {
        }


        public async Task<Unit> Handle(MarkNotificationAsReadCommand request, CancellationToken cancellationToken)
        {
            var notificationToUpdate = DbContext.Notifications.FirstOrDefault(x => x.UserId == CurrentUser.UserId && x.Id == request.Id);

            if (notificationToUpdate == null)
            {
                throw new NotFoundException(nameof(Notification), request.Id);
            }

            if (!notificationToUpdate.IsRead)
            {
                notificationToUpdate.IsRead = true;
                DbContext.Notifications.Update(notificationToUpdate);
                await DbContext.SaveChangesAsync(cancellationToken);
            }
            return Unit.Value;
        }
    }
}
