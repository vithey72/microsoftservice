﻿namespace BNine.Application.Aggregates.PushNotifications.Commands.MarkNotificationAsRead
{
    using System;
    using MediatR;
    using Newtonsoft.Json;

    public class MarkNotificationAsReadCommand : IRequest
    {
        [JsonProperty("id")]
        public Guid Id
        {
            get; set;
        }
    }
}
