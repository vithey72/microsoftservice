﻿namespace BNine.Application.Aggregates.PushNotifications.Commands.ZendeskPushNotification;

using Abstractions;
using AutoMapper;
using Constants;
using Domain.Entities;
using Interfaces;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Models.Notifications;

public class ZendeskPushNotificationCommandHandler :
    AbstractRequestHandler, IRequestHandler<ZendeskPushNotificationCommand, Unit>
{
    private readonly IPushNotificationService _pushNotificationService;
    private readonly ILogger<ZendeskPushNotificationCommandHandler> _logger;

    public ZendeskPushNotificationCommandHandler(
        IPushNotificationService pushNotificationService,
        IMediator mediator,
        IBNineDbContext dbContext,
        IMapper mapper,
        ICurrentUserService currentUser,
        ILogger<ZendeskPushNotificationCommandHandler> logger) : base(mediator, dbContext, mapper, currentUser)
    {
        _pushNotificationService = pushNotificationService;
        _logger = logger;
    }

    public async Task<Unit> Handle(ZendeskPushNotificationCommand request, CancellationToken cancellationToken)
    {
        _logger.LogDebug("Notification event from Zendesk for ticket {TicketId}" +
                         $" has been received", request.TicketId);
        var user = await DbContext.Users
            .Include(u => u.Devices).Select(u =>
            new{
                u.FirstName,
                u.Devices,
            })
            .FirstOrDefaultAsync(u =>
                u.Devices.Any(d => request.ExternalDeviceIds.Contains(d.ExternalDeviceId)),
                cancellationToken: cancellationToken);

        if (user == null)
        {
            _logger.LogDebug("No registered devices where found in DB for ids provided by Zendesk: {DeviceIds}",
                string.Join(",", request.ExternalDeviceIds));
            return Unit.Value;
        }



        var title = $"{user.FirstName}, B9 support replied to your message";
        var body = "The issue you wrote to us about has been answered in our App under “Support”";
        var devicesToSend = user.Devices
            .Where(d => request.ExternalDeviceIds.Contains(d.ExternalDeviceId))
            .ToList();

        await SendZendeskNotification(devicesToSend, title, body, request.TicketId);
        return Unit.Value;

    }

    private async Task SendZendeskNotification(List<Device> devices, string title, string body, string ticketId)
    {
        var notificationTargets =
            devices.Select(x => new NotificationTarget(x.Id.ToString(), x.Type)).ToList();
        var notification = new Notification(notificationTargets: notificationTargets, title: title,
            body: body, deeplink: DeepLinkRoutes.Support, sub1: ticketId);
        await _pushNotificationService.SendNotification(notification: notification);
    }
}
