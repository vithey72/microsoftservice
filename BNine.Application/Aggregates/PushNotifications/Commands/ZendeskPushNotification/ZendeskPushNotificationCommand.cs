﻿namespace BNine.Application.Aggregates.PushNotifications.Commands.ZendeskPushNotification;

using MediatR;

public class ZendeskPushNotificationCommand: IRequest<Unit>
{
    public List<string> ExternalDeviceIds
    {
        get;
        set;
    }

    public string TicketId
    {
        get;
        set;
    }

}
