﻿namespace BNine.Application.Aggregates.PushNotifications.Commands.MarkAllNotificationsAsRead;

using MediatR;

public record MarkAllNotificationsAsReadCommand : IRequest<Unit>;
