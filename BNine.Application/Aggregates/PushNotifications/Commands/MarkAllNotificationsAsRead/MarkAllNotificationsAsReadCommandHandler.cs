﻿namespace BNine.Application.Aggregates.PushNotifications.Commands.MarkAllNotificationsAsRead;

using Abstractions;
using AutoMapper;
using Interfaces;
using MediatR;
using Microsoft.EntityFrameworkCore;

public class MarkAllNotificationsAsReadCommandHandler
    : AbstractRequestHandler
    , IRequestHandler<MarkAllNotificationsAsReadCommand, Unit>
{
    public MarkAllNotificationsAsReadCommandHandler(
        IMediator mediator,
        IBNineDbContext dbContext,
        IMapper mapper,
        ICurrentUserService currentUser
        ) : base(mediator, dbContext, mapper, currentUser)
    {
    }

    public async Task<Unit> Handle(MarkAllNotificationsAsReadCommand request, CancellationToken cancellationToken)
    {
        var notificationsToUpdate = await DbContext.Notifications
            .Where(x => x.UserId == CurrentUser.UserId && !x.IsRead)
            .ToListAsync(cancellationToken);

        notificationsToUpdate.ForEach(x => x.IsRead = true);
        await DbContext.SaveChangesAsync(cancellationToken);
        return Unit.Value;
    }
}
