﻿namespace BNine.Application.Aggregates.PushNotifications.Commands.SendReturnedPaymentNotification
{
    using MediatR;

    public class SendReturnedPaymentNotificationCommand : IRequest<Unit>
    {
        public int ClientId
        {
            get;
        }

        public string ResponseCode
        {
            get;
        }

        public decimal Amount
        {
            get;
        }

        public int AuthorizationId
        {
            get;
        }

        public string MerchantName
        {
            get;
        }

        public decimal? AvailableBalance
        {
            get;
        }

        public SendReturnedPaymentNotificationCommand(int clientId, string responseCode, decimal amount, int authorizationId, string merchantName, decimal? availableBalance)
        {
            ClientId = clientId;
            ResponseCode = responseCode;
            Amount = amount;
            AuthorizationId = authorizationId;
            MerchantName = merchantName;
            AvailableBalance = availableBalance;
        }
    }
}
