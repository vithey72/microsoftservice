﻿namespace BNine.Application.Aggregates.PushNotifications.Commands.SendReturnedPaymentNotification
{
    using System.Linq;
    using System.Threading;
    using System.Threading.Tasks;
    using AutoMapper;
    using BNine.Enums;
    using Constants;
    using Helpers;
    using Interfaces;
    using Interfaces.Bank.Administration;
    using MediatR;
    using Microsoft.EntityFrameworkCore;
    public class SendReturnedPaymentNotificationCommandHandler : BaseNotificationWithAvailableBalanceCommandHandler, IRequestHandler<SendReturnedPaymentNotificationCommand, Unit>
    {
        private IBankCardAuthorizationsService BankCardAuthorizationsService
        {
            get;
        }
        public SendReturnedPaymentNotificationCommandHandler(
            IBankSavingAccountsService bankAccountService,
            IPushNotificationService pushNotificationService,
            IBankCardAuthorizationsService bankCardAuthorizationsService,
            IMediator mediator,
            IBNineDbContext dbContext,
            IMapper mapper,
            ICurrentUserService currentUser
        ) : base(bankAccountService, pushNotificationService, mediator, dbContext, mapper, currentUser)
        {
            BankCardAuthorizationsService = bankCardAuthorizationsService;
        }
        public async Task<Unit> Handle(SendReturnedPaymentNotificationCommand request, CancellationToken cancellationToken)
        {
            var user = await DbContext.Users
                .Include(x => x.Devices)
                .Include(x => x.CurrentAccount)
                .FirstAsync(x => x.ExternalClientId == request.ClientId);
            var notification = string.Empty;
            var title = string.Empty;
            var category = string.Empty;
            var authorization = await BankCardAuthorizationsService.GetCardAuthorization(request.AuthorizationId);
            var availableBalance = await GetAvailableBalance(request.AvailableBalance, user.CurrentAccount.ExternalId);
            var cardNumber = authorization?.Card?.PrimaryAccountNumber == null || authorization?.Card?.PrimaryAccountNumber.Length < 5 ? null : $"*{authorization.Card.PrimaryAccountNumber.Substring(authorization.Card.PrimaryAccountNumber.Length - 4, 4)}";
            var merchantName = GetFormattedMerchantName(request.MerchantName);
            title = "Return of Transaction";
            category = PushNotificationCategory.ReturnedPayment;
            notification = $"Reversed {CurrencyFormattingHelper.AsRounded(request.Amount)} Charge of Card {cardNumber} by {merchantName}. Balance {CurrencyFormattingHelper.AsRounded(availableBalance)}";
            if (!string.IsNullOrEmpty(notification))
            {
                await NotifyUser(user.Id, notification, user.Settings.IsNotificationsEnabled, NotificationTypeEnum.Transaction, false, user.Devices, null, null, category, title);
            }
            return Unit.Value;
        }

        private string GetFormattedMerchantName(string merchantName)
        {
            if (string.IsNullOrEmpty(merchantName))
            {
                return merchantName;
            }
            merchantName = merchantName.Substring(0, 23).Trim().Replace("*", string.Empty);
            var arr = merchantName.Split().Where(x => !string.IsNullOrWhiteSpace(x)).ToArray();
            merchantName = arr[0];
            if (arr.Length == 1)
            {
                return merchantName;
            }
            for (var i = 1; i < arr.Length; i++)
            {
                if (arr[i].Any(x => !char.IsLetter(x)))
                {
                    break;
                }
                merchantName += $" {arr[i]}";
            }
            return merchantName;
        }
    }
}
