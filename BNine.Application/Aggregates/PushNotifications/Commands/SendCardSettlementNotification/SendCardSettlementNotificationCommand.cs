﻿namespace BNine.Application.Aggregates.PushNotifications.Commands.SendCardSettlementNotification
{
    using MediatR;

    public class SendCardSettlementNotificationCommand : IRequest<Unit>
    {
        public SendCardSettlementNotificationCommand(int? transactionId, string transactionType)
        {
            TransactionId = transactionId;
            TransactionType = transactionType;
        }

        public int? TransactionId
        {
            get; set;
        }

        public string TransactionType
        {
            get; set;
        }
    }
}
