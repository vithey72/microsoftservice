﻿namespace BNine.Application.Aggregates.PushNotifications.Commands.SendCardSettlementNotification
{
    using System.Threading;
    using System.Threading.Tasks;
    using AutoMapper;
    using BNine.Application.Interfaces;
    using BNine.Application.Interfaces.Bank.Administration;
    using MediatR;
    using Microsoft.Extensions.Logging;

    public class SendCardSettlementNotificationCommandHandler
        : BaseSendNotificationCommandHandler, IRequestHandler<SendCardSettlementNotificationCommand, Unit>
    {
        private const string DOMESTIC_ATM_WITHDRAWAL_FEE_CODE = "DOMESTIC_ATM_WITHDRAWAL_FEE";
        private const string INTERNATIONAL_ATM_WITHDRAWAL_FEE_CODE = "INTERNATIONAL_ATM_WITHDRAWAL_FEE";
        private const string INTERNATIONAL_TRANSACTION_FEE_CODE = "PURCHASE_INTERNATIONAL_FEE";


        public IBankTransactionsService BankTransactionsService
        {
            get;
        }
        public ILogger<SendCardSettlementNotificationCommandHandler> Logger
        {
            get;
        }
        public IBankSavingAccountsService BankAccountsService
        {
            get;
        }

        public SendCardSettlementNotificationCommandHandler(
            IBankSavingAccountsService bankAccountsService,
            IPushNotificationService pushNotificationService,
            IBankTransactionsService bankTransactionsService,
            IMediator mediator,
            IBNineDbContext dbContext,
            IMapper mapper,
            ICurrentUserService currentUser,
            ILogger<SendCardSettlementNotificationCommandHandler> logger
            ) : base(pushNotificationService, mediator, dbContext, mapper, currentUser)
        {
            BankTransactionsService = bankTransactionsService;
            Logger = logger;
            BankAccountsService = bankAccountsService;
        }

        public async Task<Unit> Handle(SendCardSettlementNotificationCommand request, CancellationToken cancellationToken)
        {
            Logger.LogInformation("ATM fee notifications are disabled");
            return Unit.Value;

            //if (request.TransactionType != DOMESTIC_ATM_WITHDRAWAL_FEE_CODE && request.TransactionType != INTERNATIONAL_ATM_WITHDRAWAL_FEE_CODE && request.TransactionType != INTERNATIONAL_TRANSACTION_FEE_CODE)
            //{
            //    return Unit.Value;
            //}

            //var transaction = await BankTransactionsService.GetSavingAccountTransaction(request.TransactionId.Value);

            //var user = DbContext.Users
            //    .Include(x => x.Devices)
            //    .Where(x => x.CurrentAccount.ExternalId == transaction.AccountId)
            //    .FirstOrDefault();

            //if (user is null)
            //{
            //    throw new NotFoundException(nameof(User), new
            //    {
            //        savingAccountId = transaction.AccountId
            //    });
            //}

            //var account = await BankAccountsService.GetSavingAccountInfo(transaction.AccountId);

            //var message = request.TransactionType switch
            //{
            //    DOMESTIC_ATM_WITHDRAWAL_FEE_CODE => $"{CurrencyFormattingHelper.AsRegular(transaction.Amount)} ATM Fee is charged.",
            //    INTERNATIONAL_ATM_WITHDRAWAL_FEE_CODE => $"{CurrencyFormattingHelper.AsRegular(transaction.Amount)} ATM Fee is charged.",
            //    INTERNATIONAL_TRANSACTION_FEE_CODE => $"{CurrencyFormattingHelper.AsRegular(transaction.Amount)} Foreign Transaction Fee is charged.",
            //    _ => throw new ArgumentOutOfRangeException(request.TransactionType)
            //};

            //await NotifyUser(user.Id, message, user.Settings.IsNotificationsEnabled, NotificationTypeEnum.Transaction, false, user.Devices, null, null, PushNotificationCategory.AccountCharge, "Account Charge");

            //return Unit.Value;
        }
    }
}
