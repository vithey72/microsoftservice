﻿namespace BNine.Application.Aggregates.PushNotifications.Commands.SendCardSettlementNotification
{
    using FluentValidation;

    public class SendCardSettlementNotificationCommandValidator
        : AbstractValidator<SendCardSettlementNotificationCommand>
    {
        public SendCardSettlementNotificationCommandValidator()
        {
            RuleFor(x => x.TransactionId)
                .NotEmpty();

            RuleFor(x => x.TransactionType)
                .NotEmpty();
        }
    }
}
