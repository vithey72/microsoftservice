﻿namespace BNine.Application.Aggregates.PushNotifications.Commands.SendCardAuthorizationNotification
{
    using MediatR;

    public class SendCardNotificationCommand : IRequest<Unit>
    {
        public int ClientId
        {
            get;
        }

        public string ResponseCode
        {
            get;
        }

        public decimal Amount
        {
            get;
        }

        public int? AuthorizationId
        {
            get;
        }

        public string MerchantName
        {
            get;
        }

        public decimal? AvailableBalance
        {
            get;
        }

        public string TransactionType
        {
            get;
        }

        public string HookId
        {
            get; set;
        }

        public SendCardNotificationCommand(int clientId,
             string responseCode,
             decimal amount,
             int? authorizationId,
             string merchantName,
             decimal? availableBalance,
             string transactionType,
             string hookId)
        {
            ClientId = clientId;
            ResponseCode = responseCode;
            Amount = amount;
            AuthorizationId = authorizationId;
            MerchantName = merchantName;
            AvailableBalance = availableBalance;
            TransactionType = transactionType;
            HookId = hookId;
        }
    }
}
