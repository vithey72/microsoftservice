﻿namespace BNine.Application.Aggregates.PushNotifications.Commands.SendCardAuthorizationNotification
{
    using System.Linq;
    using System.Threading;
    using System.Threading.Tasks;
    using AutoMapper;
    using BNine.Application.Interfaces;
    using BNine.Application.Interfaces.Bank.Administration;
    using BNine.Constants;
    using BNine.Enums;
    using Helpers;
    using MediatR;
    using Microsoft.EntityFrameworkCore;

    public class SendCardNotificationCommandHandler
        : BaseNotificationWithAvailableBalanceCommandHandler, IRequestHandler<SendCardNotificationCommand, Unit>
    {
        private IBankCardAuthorizationsService BankCardAuthorizationsService
        {
            get;
        }

        public SendCardNotificationCommandHandler(
            IBankSavingAccountsService bankAccountService,
            IPushNotificationService pushNotificationService,
            IBankCardAuthorizationsService bankCardAuthorizationsService,
            IMediator mediator,
            IBNineDbContext dbContext,
            IMapper mapper,
            ICurrentUserService currentUser
            ) : base(bankAccountService, pushNotificationService, mediator, dbContext, mapper, currentUser)
        {
            BankCardAuthorizationsService = bankCardAuthorizationsService;
        }

        public async Task<Unit> Handle(SendCardNotificationCommand request, CancellationToken cancellationToken)
        {
            if (request.TransactionType is not CardAuthorizationTransactionType.AUTH and not
                CardAuthorizationTransactionType.PAYMENT and not CardAuthorizationTransactionType.ATM)
            {
                return Unit.Value;
            }

            var user = await DbContext.Users
                .Include(x => x.Devices)
                .Include(x => x.CurrentAccount)
                .FirstAsync(x => x.ExternalClientId == request.ClientId);

            var notification = string.Empty;
            var title = string.Empty;
            var category = string.Empty;

            var formattedMerchantName = request.TransactionType is CardAuthorizationTransactionType.ATM ? request.MerchantName : GetFormattedMerchantName(request.MerchantName);

            if (request.ResponseCode == "71")
            {
                title = "Card Declined";
                category = PushNotificationCategory.CardDeclined;
                notification = $"Your transaction at {formattedMerchantName} was declined for exceeding velocity limit.";
            }

            if (request.ResponseCode == "85" || request.ResponseCode == "111")
            {
                title = "Card Declined";
                category = PushNotificationCategory.CardDeclined;
                notification = $"Your transaction at {formattedMerchantName} was declined.";
            }

            if (request.ResponseCode == "110" || request.ResponseCode == "112")
            {
                title = "Card Declined";
                category = PushNotificationCategory.CardDeclined;
                notification = $"Your transaction at {formattedMerchantName} is rejected, incorrect PIN.";
            }

            if (!request.AuthorizationId.HasValue)
            {
                return Unit.Value;
            }
            var cardLast4Digits = await GetCardLast4Digits(request.AuthorizationId.Value);

            if (request.ResponseCode == "52")
            {
                title = "Card Declined";
                category = PushNotificationCategory.CardDeclined;
                notification = $"Your card {cardLast4Digits} is locked. Transaction at {formattedMerchantName} is rejected. Go to Cards section in the App and unlock or reissue your card.";
            }

            if (request.ResponseCode == "102")
            {
                title = "Card Declined";
                category = PushNotificationCategory.ActivateCard;
                notification = $"Your transaction at {formattedMerchantName} is rejected. Please make sure your card is active, try again or contact our customer support team.";
            }

            if (!string.IsNullOrEmpty(notification))
            {
                await NotifyUser(user.Id, notification, user.Settings.IsNotificationsEnabled, NotificationTypeEnum.Information, true, user.Devices, null, null, category, title);
                return Unit.Value;
            }

            var availableBalance = await GetAvailableBalance(request.AvailableBalance, user.CurrentAccount.ExternalId);

            if (request.ResponseCode == "70")
            {
                title = "Card Declined";
                category = PushNotificationCategory.CardDeclined;
                notification = $"Declined {CurrencyFormattingHelper.AsRegular(request.Amount)} Charge of Card {cardLast4Digits} by {formattedMerchantName}. Balance {CurrencyFormattingHelper.AsRegular(availableBalance)}";
            }

            if (request.ResponseCode == "00")
            {
                switch (request.TransactionType){
                    case CardAuthorizationTransactionType.AUTH:
                        {
                            title = "Card Charge";
                            category = PushNotificationCategory.CardCharge;
                            notification = $"Approved {CurrencyFormattingHelper.AsRegular(request.Amount)} Charge of Card {cardLast4Digits} by {formattedMerchantName}. Balance {CurrencyFormattingHelper.AsRegular(availableBalance)}";

                            break;
                        }
                    case CardAuthorizationTransactionType.PAYMENT:
                        {
                            title = "Money received";
                            category = PushNotificationCategory.MoneyReceived;
                            notification = $"{CurrencyFormattingHelper.AsRegular(request.Amount)} from {formattedMerchantName} has posted to your account. Balance {CurrencyFormattingHelper.AsRegular(availableBalance)}";

                            break;
                        }
                    case CardAuthorizationTransactionType.ATM:
                        {
                            title = "ATM withdrawal";
                            category = PushNotificationCategory.ATMWithdrawal;
                            notification = $"{CurrencyFormattingHelper.AsRegular(request.Amount)}, Card {cardLast4Digits}. Balance {CurrencyFormattingHelper.AsRegular(availableBalance)}";

                            break;
                        }
                }
            }

            if (!string.IsNullOrEmpty(notification))
            {
                await NotifyUser(user.Id, notification, user.Settings.IsNotificationsEnabled, NotificationTypeEnum.Transaction, false, user.Devices, null, null, category, title, hookId: request.HookId);
            }

            return Unit.Value;
        }

        private string GetFormattedMerchantName(string merchantName)
        {
            if (string.IsNullOrEmpty(merchantName))
            {
                return merchantName;
            }

            merchantName = merchantName.Substring(0, 23).Trim().Replace("*", string.Empty);

            var arr = merchantName.Split().Where(x => !string.IsNullOrWhiteSpace(x)).ToArray();

            merchantName = arr[0];

            if (arr.Length == 1)
            {
                return merchantName;
            }

            for (var i = 1; i < arr.Length; i++)
            {
                if (arr[i].Any(x => !char.IsLetter(x)))
                {
                    break;
                }
                merchantName += $" {arr[i]}";
            }

            return merchantName;
        }

        private async Task<string> GetCardLast4Digits(int cardAuthId)
        {
            var authorization = await BankCardAuthorizationsService.GetCardAuthorization(cardAuthId);

            var cardLast4Digits =
                authorization?.Card?.PrimaryAccountNumber == null ||
                authorization?.Card?.PrimaryAccountNumber.Length < 5
                    ? null
                    : $"*{authorization.Card.PrimaryAccountNumber.Substring(authorization.Card.PrimaryAccountNumber.Length - 4, 4)}";
            return cardLast4Digits;
        }
    }
}
