﻿namespace BNine.Application.Aggregates.PushNotifications.Commands.SendMonthlyChargeNotification;

using System;
using MediatR;

public record SendEmailIfMonthlyChargeFailedCommand(long ClientId, decimal Amount, decimal? AvailableBalance,
    decimal TotalChargeAmount, decimal DeferredAmount, DateTime FiredAt)
    : IRequest<Unit>;
