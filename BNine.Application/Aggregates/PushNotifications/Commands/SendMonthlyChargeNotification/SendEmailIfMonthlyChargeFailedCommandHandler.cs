﻿namespace BNine.Application.Aggregates.PushNotifications.Commands.SendMonthlyChargeNotification;

using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Abstractions;
using AutoMapper;
using BNine.Application.Models.Emails.EmailsWithTemplate;
using BNine.Application.Models.Emails.EmailsWithTemplate.Payment;
using BNine.Settings;
using Domain.Entities.User;
using Exceptions;
using Helpers;
using Interfaces;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;

public class SendEmailIfMonthlyChargeFailedCommandHandler
    : AbstractRequestHandler
    , IRequestHandler<SendEmailIfMonthlyChargeFailedCommand, Unit>
{
    private readonly IEmailService _emailService;
    private readonly EmailTemplatesSettings _templatesIds;

    public SendEmailIfMonthlyChargeFailedCommandHandler(
        IEmailService emailService,
        IMediator mediator,
        IBNineDbContext dbContext,
        IMapper mapper,
        ICurrentUserService currentUser,
        IOptions<EmailTemplatesSettings> templatesOptions
        )
        : base(mediator, dbContext, mapper, currentUser)
    {
        _emailService = emailService;
        _templatesIds = templatesOptions.Value;
    }

    public async Task<Unit> Handle(SendEmailIfMonthlyChargeFailedCommand request, CancellationToken cancellationToken)
    {
        var user = await DbContext.Users
            .Where(x => x.ExternalClientId == request.ClientId)
            .Include(x => x.CurrentAccount)
            .Include(x => x.UserTariffPlans)
            .ThenInclude(x => x.TariffPlan)
            .Include(x => x.Devices)
            .FirstOrDefaultAsync(cancellationToken);

        if (user == null)
        {
            throw new NotFoundException(nameof(User), new { externalClientId = request.ClientId });
        }

        if (request.Amount == decimal.Zero)
        {
            var templateId = _templatesIds.MonthlyPaymentDeclinedTemplate;
            var data = new PaymentDeclinedTemplateData
            {
                BillingDate = request.FiredAt.ToString("MMMM dd, yyyy"),
                FeeSize = CurrencyFormattingHelper.AsRegular(request.TotalChargeAmount)
            };

            var template = new EmailMessageTemplate
            {
                RecipientEmail = user.Email,
                TemplateId = templateId,
                TemplateData = data,
                Attachments = new List<EmailAttachmentData>()
            };

            await _emailService.SendEmail(template);
        }
        return Unit.Value;
    }
}
