﻿namespace BNine.Application.Aggregates.Notifications.Commands.SendBulkNotifications
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading;
    using System.Threading.Tasks;
    using AutoMapper;
    using BNine.Application.Abstractions;
    using BNine.Application.Interfaces;
    using BNine.Application.Models.Notifications;
    using BNine.Enums;
    using MediatR;
    using Microsoft.EntityFrameworkCore;

    public class SendBulkNotificationsCommandHandler : AbstractRequestHandler, IRequestHandler<SendBulkNotificationsCommand>
    {
        private readonly IEnumerable<MobilePlatform> platformsToSend = new List<MobilePlatform> { MobilePlatform.iOs, MobilePlatform.Android };

        private IPushNotificationService NotificationService
        {
            get;
        }

        public SendBulkNotificationsCommandHandler(
            IPushNotificationService notificationService,
            IMediator mediator,
            IBNineDbContext dbContext,
            IMapper mapper,
            ICurrentUserService currentUser) : base(mediator, dbContext, mapper, currentUser)
        {
            NotificationService = notificationService;
        }

        public async Task<Unit> Handle(SendBulkNotificationsCommand request, CancellationToken cancellationToken)
        {
            var targets = await DbContext.Users
                .Where(x => request.Recipients.Contains(x.Id))
                .Where(x => request.ForcedSend || x.Settings.IsNotificationsEnabled)
                .SelectMany(x => x.Devices)
                .Where(x => !string.IsNullOrWhiteSpace(x.RegistrationId))
                .Select(x => new NotificationTarget(x.Id, x.Type))
                .ToListAsync(cancellationToken);

            foreach (var platform in platformsToSend)
            {
                await NotificationService.SendBulkNotificationToSelectedPlatform(new BulkNotification(request.Message, targets.Where(x => x.MobilePlatform == platform).Select(x => x.Value), request.Deeplink, request.AppsflyerEventType, request.Title, request.Category), platform);
            }

            var notifiedUsers = await DbContext.Users
               .Where(x => request.Recipients.Contains(x.Id))
               .Where(x => request.ForcedSend || x.Settings.IsNotificationsEnabled)
               .Select(x => x.Id)
               .ToListAsync(cancellationToken);

            DbContext.Notifications
                .AddRange(notifiedUsers.Select(x => new Domain.Entities.Notification.Notification
                {
                    Text = request.Message,
                    UserId = x,
                    Category = request.Category,
                    Title = request.Title,
                    Type = request.Type,
                    IsImportant = request.IsImportant,
                    Deeplink = request.Deeplink,
                }));

            await DbContext.SaveChangesAsync(cancellationToken);

            return Unit.Value;
        }
    }
}
