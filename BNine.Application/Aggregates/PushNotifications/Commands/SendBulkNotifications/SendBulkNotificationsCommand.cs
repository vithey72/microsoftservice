﻿namespace BNine.Application.Aggregates.Notifications.Commands.SendBulkNotifications
{
    using System;
    using System.Collections.Generic;
    using BNine.Enums;
    using MediatR;
    using Newtonsoft.Json;

    public class SendBulkNotificationsCommand : IRequest<Unit>
    {
        public SendBulkNotificationsCommand(string message, IEnumerable<Guid> recipients, bool forcedSend, string deeplink,
                                            string appsflyerEventType, string category, string title, NotificationTypeEnum type,
                                            bool isImportant)
        {
            Message = message;
            Recipients = recipients;
            ForcedSend = forcedSend;
            Deeplink = deeplink;
            AppsflyerEventType = appsflyerEventType;
            Title = title;
            Category = category;
            Type = type;
            IsImportant = isImportant;
        }

        [JsonProperty("message")]
        public string Message
        {
            get;
            set;
        }

        [JsonProperty("recipients")]
        public IEnumerable<Guid> Recipients
        {
            get;
            set;
        } = new List<Guid>();

        [JsonProperty("forcedSend")]
        public bool ForcedSend
        {
            get;
            set;
        }

        [JsonProperty("deeplink")]
        public string Deeplink
        {
            get;
            set;
        }

        [JsonProperty("appsflyerEventType")]
        public string AppsflyerEventType
        {
            get;
            set;
        }

        [JsonProperty("title")]
        public string Title
        {
            get;
            set;
        }

        [JsonProperty("category")]
        public string Category
        {
            get;
            set;
        }

        [JsonProperty("type")]
        public NotificationTypeEnum Type
        {
            get;
            set;
        }

        [JsonProperty("isImportant")]
        public bool IsImportant
        {
            get;
            set;
        }
    }
}
