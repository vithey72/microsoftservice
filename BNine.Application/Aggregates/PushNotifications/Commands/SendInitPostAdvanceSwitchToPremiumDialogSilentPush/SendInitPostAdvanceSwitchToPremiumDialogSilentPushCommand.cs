﻿namespace BNine.Application.Aggregates.PushNotifications.Commands.SendInitPostAdvanceSwitchToPremiumDialogSilentPush;

using MediatR;

public record SendInitPostAdvanceSwitchToPremiumDialogSilentPushCommand : IRequest<Unit>;
