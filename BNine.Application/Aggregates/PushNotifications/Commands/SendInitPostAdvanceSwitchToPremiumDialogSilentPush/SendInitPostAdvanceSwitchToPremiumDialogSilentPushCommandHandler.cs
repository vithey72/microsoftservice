﻿namespace BNine.Application.Aggregates.PushNotifications.Commands.SendInitPostAdvanceSwitchToPremiumDialogSilentPush;

using System;
using System.Threading.Tasks;
using Abstractions;
using AutoMapper;
using BNine.Domain.Constants;
using Configuration.Queries.GetConfiguration;
using Constants;
using Interfaces;
using MediatR;
using Microsoft.EntityFrameworkCore;

public class SendInitPostAdvanceSwitchToPremiumDialogSilentPushCommandHandler
    : AbstractRequestHandler
    , IRequestHandler<SendInitPostAdvanceSwitchToPremiumDialogSilentPushCommand, Unit>
{
    private readonly IUserActivitiesCooldownService _cooldownService;
    private readonly INotificationUsersService _notificationsService;

    public SendInitPostAdvanceSwitchToPremiumDialogSilentPushCommandHandler(
        IMediator mediator,
        IBNineDbContext dbContext,
        IMapper mapper,
        ICurrentUserService currentUser,
        IUserActivitiesCooldownService cooldownService,
        INotificationUsersService notificationsService
        ) : base(mediator, dbContext, mapper, currentUser)
    {
        _cooldownService = cooldownService;
        _notificationsService = notificationsService;
    }

    public async Task<Unit> Handle(SendInitPostAdvanceSwitchToPremiumDialogSilentPushCommand request, CancellationToken cancellationToken)
    {
        var user = await DbContext
            .Users
            .Include(u => u.Devices)
            .SingleAsync(x => x.Id == CurrentUser.UserId, cancellationToken);

        await _notificationsService.SendSilentPushNotification(user, DeepLinkRoutes.PremiumAfterAdvanceOffer);

        var configuration = await Mediator.Send(
            new GetConfigurationQuery(CurrentUser.UserId!.Value), cancellationToken);

        if (!configuration.PropertyExists(FeaturesNames.Features.SwitchToPremiumAfterAdvance,
                FeaturesNames.Settings.SwitchToPremiumAfterAdvance.ShowCooldownHoursName))
        {
            return Unit.Value;
        }

        var cooldownHours = (int)(configuration.Features[FeaturesNames.Features.SwitchToPremiumAfterAdvance] as dynamic)
            .ShowCooldownHours;

        if (cooldownHours > 0)
        {
            await _cooldownService.AddCooldown(CurrentUser.UserId!.Value, CooldownActivitiesNames.Marketing.SwitchToPremiumPitch,
                TimeSpan.FromHours(cooldownHours), cancellationToken);
        }

        return Unit.Value;
    }
}
