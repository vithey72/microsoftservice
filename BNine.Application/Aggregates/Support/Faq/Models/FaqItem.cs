﻿namespace BNine.Application.Aggregates.Support.Faq.Models
{
    using System;

    public class FaqItem
    {
        public Guid Id
        {
            get; set;
        }

        public string Question
        {
            get; set;
        }

        public string Answer
        {
            get; set;
        }

    }
}
