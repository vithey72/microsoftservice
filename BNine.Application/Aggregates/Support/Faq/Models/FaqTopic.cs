﻿namespace BNine.Application.Aggregates.Support.Faq.Models
{
    using System;
    using System.Collections.Generic;

    public class FaqTopic
    {
        public Guid Id
        {
            get; set;
        }

        public string Topic
        {
            get; set;
        }

        public List<FaqItem> FaqItems
        {
            get; set;
        }
    }
}
