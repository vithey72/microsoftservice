﻿namespace BNine.Application.Aggregates.Support.Models;

public class SupportVipOfferConfirmationViewModel
{
    public string Header
    {
        get;
        set;
    }

    public VipSupportFeaturesEntryViewModel[] Features
    {
        get;
        set;
    }

    public string ConfirmationButtonText
    {
        get;
        set;
    }

    public string CancelButtonText
    {
        get;
        set;
    }
}
