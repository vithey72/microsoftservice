﻿namespace BNine.Application.Aggregates.Support.Models;

using CommonModels.Dialog;

public class SupportVipOfferActivateResponse : GenericDialogBasicViewModel
{
    public bool IsEnabled
    {
        get;
        set;
    }
}
