﻿namespace BNine.Application.Aggregates.Support.Models;

public class SupportGreetingsScreenViewModel
{
    public string Title
    {
        get;
        set;
    }

    public string Subtitle
    {
        get;
        set;
    }

    public bool IsVip
    {
        get;
        set;
    }

    public string VipAvailableToText
    {
        get;
        set;
    }
}
