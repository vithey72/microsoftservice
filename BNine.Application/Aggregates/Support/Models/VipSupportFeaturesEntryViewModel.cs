﻿namespace BNine.Application.Aggregates.Support.Models;

public class VipSupportFeaturesEntryViewModel
{
    public string Text
    {
        get;
        set;
    }

    public string IconType
    {
        get;
        set;
    }
}
