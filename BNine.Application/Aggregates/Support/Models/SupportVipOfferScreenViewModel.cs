﻿namespace BNine.Application.Aggregates.Support.Models;

public class SupportVipOfferScreenViewModel
{
    public SaleSection Sale
    {
        get;
        set;
    }

    public InfoLink[] InfoLinksContent
    {
        get;
        set;
    }

    public class SaleSection
    {
        public Block VipBlock
        {
            get;
            set;
        }

        public Block StandardBlock
        {
            get;
            set;
        }

        public class Block
        {
            public string TagLine
            {
                get;
                set;
            }

            public FeaturesEntry[] Features
            {
                get;
                set;
            }

            public string ButtonText
            {
                get;
                set;
            }
        }

        public class FeaturesEntry
        {
            public string Text
            {
                get;
                set;
            }

            public string InfoLink
            {
                get;
                set;
            }
        }
    }

    public class InfoLink
    {
        public string Name
        {
            get;
            set;
        }

        public string Title
        {
            get;
            set;
        }

        public string Subtitle
        {
            get;
            set;
        }

        public VipSupportFeaturesEntryViewModel[] Features
        {
            get;
            set;
        }

        public string Notice
        {
            get;
            set;
        }

        public string ButtonText
        {
            get;
            set;
        }
    }
}
