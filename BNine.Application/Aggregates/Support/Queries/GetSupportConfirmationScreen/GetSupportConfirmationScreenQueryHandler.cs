﻿namespace BNine.Application.Aggregates.Support.Queries.GetSupportConfirmationScreen;

using System.Threading.Tasks;
using Abstractions;
using AutoMapper;
using Interfaces;
using MediatR;
using Models;

public class GetSupportConfirmationScreenQueryHandler
    : AbstractRequestHandler
        , IRequestHandler<GetSupportConfirmationScreenQuery, SupportVipOfferConfirmationViewModel>
{
    public GetSupportConfirmationScreenQueryHandler(
        IMediator mediator,
        IBNineDbContext dbContext,
        IMapper mapper,
        ICurrentUserService currentUser)
        : base(mediator, dbContext, mapper, currentUser)
    {
    }

    public Task<SupportVipOfferConfirmationViewModel> Handle(GetSupportConfirmationScreenQuery request,
        CancellationToken token)
    {
        var response = new SupportVipOfferConfirmationViewModel
        {
            Header = "Buy Premium Support?",
            ConfirmationButtonText = "YES, CONTINUE",
            CancelButtonText = "NO, BACK",
            Features = new[]
            {
                new VipSupportFeaturesEntryViewModel
                {
                    Text = "$3 for 14 days",
                    IconType = "tag",
                }
            }
        };
        return Task.FromResult(response);
    }
}
