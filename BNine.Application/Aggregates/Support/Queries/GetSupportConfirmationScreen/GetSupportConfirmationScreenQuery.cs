﻿namespace BNine.Application.Aggregates.Support.Queries.GetSupportConfirmationScreen;

using MediatR;
using Models;

public class GetSupportConfirmationScreenQuery : IRequest<SupportVipOfferConfirmationViewModel>
{
}
