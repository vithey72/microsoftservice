﻿namespace BNine.Application.Aggregates.Support.Queries.GetSupportVipOfferScreen;

using MediatR;
using Models;

public class GetSupportVipOfferScreenQuery : IRequest<SupportVipOfferScreenViewModel>
{
}
