﻿namespace BNine.Application.Aggregates.Support.Queries.GetSupportVipOfferScreen;

using Abstractions;
using AutoMapper;
using Interfaces;
using MediatR;
using Models;

public class GetSupportVipOfferScreenQueryHandler
    : AbstractRequestHandler
    , IRequestHandler<GetSupportVipOfferScreenQuery, SupportVipOfferScreenViewModel>
{
    public GetSupportVipOfferScreenQueryHandler(
        IMediator mediator,
        IBNineDbContext dbContext,
        IMapper mapper,
        ICurrentUserService currentUser)
        : base(mediator, dbContext, mapper, currentUser)
    {
    }

    public Task<SupportVipOfferScreenViewModel> Handle(GetSupportVipOfferScreenQuery request, CancellationToken cancellationToken)
    {
        var workHoursLink = "work-hours";
        var response = new SupportVipOfferScreenViewModel
        {
            Sale = new SupportVipOfferScreenViewModel.SaleSection
            {
                VipBlock = new SupportVipOfferScreenViewModel.SaleSection.Block
                {
                    TagLine = "PREMIUM CUSTOMER SUPPORT",
                    ButtonText = "USE IT",
                    Features = new[]
                    {
                        new SupportVipOfferScreenViewModel.SaleSection.FeaturesEntry
                        {
                            Text = "We will answer you in an hour",
                            InfoLink = workHoursLink,
                        },
                        new SupportVipOfferScreenViewModel.SaleSection.FeaturesEntry
                        {
                            Text = "Dedicated priority answer line"
                        }
                    }
                },
                StandardBlock = new SupportVipOfferScreenViewModel.SaleSection.Block
                {
                    TagLine = "FREE",
                    ButtonText = "USE IT",
                    Features = new[]
                    {
                        new SupportVipOfferScreenViewModel.SaleSection.FeaturesEntry
                        {
                            Text = "We will answer you in 24 hours"
                        }
                    }
                }
            },
            InfoLinksContent = new[]
            {
                new SupportVipOfferScreenViewModel.InfoLink
                {
                    Name = workHoursLink,
                    Title = "Our Customer Support Team is active the following hours:",
                    Notice = "Time of response may depend on external factors.",
                    ButtonText = "GOT IT",
                    Features = new[]
                    {
                        new VipSupportFeaturesEntryViewModel
                        {
                            Text = "Mon-Fri: 6 AM to 9 PM Pacific Standard Time (9 AM to 12 AM Eastern Standard Time)",
                            IconType = "watch",
                        },
                        new VipSupportFeaturesEntryViewModel
                        {
                            Text = "Sat-Sun: 6 AM to 6 PM Pacific Standard Time (9 AM to 9 PM Eastern Standard Time)",
                            IconType = "watch",
                        },
                    }
                }
            }
        };
        return Task.FromResult(response);
    }
}
