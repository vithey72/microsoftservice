﻿namespace BNine.Application.Aggregates.Support.Queries.GetSupportGreetingsScreen
{
    using System;
    using System.Threading.Tasks;
    using Abstractions;
    using AutoMapper;
    using Constants;
    using Enums;
    using Enums.TariffPlan;
    using Helpers;
    using Interfaces;
    using MediatR;
    using Microsoft.EntityFrameworkCore;
    using Models;

    public class GetSupportGreetingsScreenQueryHandler
        : AbstractRequestHandler
        , IRequestHandler<GetSupportGreetingsScreenQuery, SupportGreetingsScreenViewModel>
    {
        private readonly ITariffPlanService _tariffPlanService;
        private readonly IPartnerProviderService _partnerProvider;

        public GetSupportGreetingsScreenQueryHandler(
            IMediator mediator,
            IBNineDbContext dbContext,
            IMapper mapper,
            ICurrentUserService currentUser,
            ITariffPlanService tariffPlanService,
            IPartnerProviderService partnerProvider
        )
            : base(mediator, dbContext, mapper, currentUser)
        {
            _tariffPlanService = tariffPlanService;
            _partnerProvider = partnerProvider;
        }

        public async Task<SupportGreetingsScreenViewModel> Handle(GetSupportGreetingsScreenQuery request, CancellationToken token)
        {
            var usedVipSupport = await DbContext
                .UsedPaidUserServices
                .Include(us => us.Service)
                .OrderByDescending(c => c.CreatedAt)
                .FirstOrDefaultAsync(c =>
                    c.UserId == CurrentUser.UserId!.Value &&
                    c.ServiceId == PaidUserServices.PremiumSupport,
                    token);

            var tariffPlanFamily = await _tariffPlanService.GetCurrentTariffPlanFamily(CurrentUser.UserId!.Value, token);
            var genericTitle = $"Say Hi! to the \n{StringProvider.GetPartnerName(_partnerProvider)} Customer Support Team";
            var title = _partnerProvider.GetPartner() switch
            {
                PartnerApp.MBanqApp
                or PartnerApp.Poetryy
                or PartnerApp.Eazy
                or PartnerApp.Qorbis
                or PartnerApp.ManaPacific
                or PartnerApp.P2P
                or PartnerApp.PayHammer => genericTitle,
                _ => tariffPlanFamily == TariffPlanFamily.Premium
                    ? "Say Hi to the \nB9 Premium Support Team"
                    : genericTitle,
            };
            var subtitle = "We are always here and ready to help. \n" +
                           "Please submit your request and a Team Member will get back to you as soon as possible";

            if (tariffPlanFamily == TariffPlanFamily.Premium || (usedVipSupport != null && usedVipSupport.ValidTo >= DateTime.Now))
            {
                var availableToText = tariffPlanFamily == TariffPlanFamily.Premium
                    ? null
                    : $"Premium Support available \nuntil {usedVipSupport!.ValidTo.ToString(DateFormat.MonthFirst)}";

                return new SupportGreetingsScreenViewModel
                {
                    IsVip = true,
                    Title = title,
                    Subtitle = subtitle,
                    VipAvailableToText = availableToText,
                };
            }
            else
            {
                return new SupportGreetingsScreenViewModel
                {
                    IsVip = false,
                    Title = title,
                    Subtitle = subtitle,
                };
            }
        }
    }
}
