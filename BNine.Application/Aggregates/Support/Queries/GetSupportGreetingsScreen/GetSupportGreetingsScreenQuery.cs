﻿namespace BNine.Application.Aggregates.Support.Queries.GetSupportGreetingsScreen;

using MediatR;
using Models;

public class GetSupportGreetingsScreenQuery : IRequest<SupportGreetingsScreenViewModel>
{
}
