﻿namespace BNine.Application.Aggregates.Support.Queries.UpdateSupportVipStatus;

using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Abstractions;
using AutoMapper;
using Constants;
using Enums.TariffPlan;
using Interfaces;
using Interfaces.Bank.Administration;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Models;
using IBankSavingAccountsService = Interfaces.Bank.Client.IBankSavingAccountsService;

public class UpdateSupportVipStatusQueryHandler
    : AbstractRequestHandler
    , IRequestHandler<UpdateSupportVipStatusQuery, SupportVipOfferActivateResponse>
{
    private const decimal VipSupportCost = 3M;
    private readonly IPaidUserServicesService _paidUserServices;
    private readonly IBankSavingAccountsService _bankSavingAccountsService;
    private readonly ITariffPlanService _tariffPlanService;

    public UpdateSupportVipStatusQueryHandler(
        IMediator mediator,
        IBNineDbContext dbContext,
        IMapper mapper,
        ICurrentUserService currentUser,
        IPaidUserServicesService paidUserServices,
        IBankSavingAccountsService bankSavingAccountsService,
        ITariffPlanService tariffPlanService
        )
        : base(mediator, dbContext, mapper, currentUser)
    {
        _paidUserServices = paidUserServices;
        _bankSavingAccountsService = bankSavingAccountsService;
        _tariffPlanService = tariffPlanService;
    }

    public async Task<SupportVipOfferActivateResponse> Handle(UpdateSupportVipStatusQuery request, CancellationToken token)
    {
        var user = await DbContext
            .Users
            .Include(u => u.CurrentAccount)
            .FirstOrDefaultAsync(u => u.Id == CurrentUser.UserId!.Value, token);

        if (user == null)
        {
            throw new KeyNotFoundException("User was not present in DB");
        }

        var currentTariffFamily = await _tariffPlanService.GetCurrentTariffPlanFamily(user.Id, token);
        if (currentTariffFamily == TariffPlanFamily.Premium)
        {
            throw new InvalidOperationException($"Cannot purchase VIP support. User {user.Id} already has Premium.");
        }

        var savingsAccount = await _bankSavingAccountsService
            .GetSavingAccountInfo(
                user.CurrentAccount.ExternalId,
                request.MbanqAccessToken,
                request.Ip);
        if (savingsAccount.AvailableBalance >= VipSupportCost)
        {
            var activatedServiceEntity = await _paidUserServices.EnableService(PaidUserServices.PremiumSupport);
            if (activatedServiceEntity != null)
            {
                return new SupportVipOfferActivateResponse
                {
                    IsEnabled = true,
                    Header = "You bought Premium Support",
                    Subtitle = $"It will be available for you \nuntil {activatedServiceEntity.ValidTo.ToString(DateFormat.MonthFirst)}",
                    ButtonText = "OK",
                };
            }
        }

        return new SupportVipOfferActivateResponse
        {
            IsEnabled = false,
            Header = "Insufficient funds",
            Subtitle = "To buy Premium Support you need \nto have at least $3 in your account",
            ButtonText = "ADD MONEY",
        };
    }
}
