﻿namespace BNine.Application.Aggregates.Support.Queries.UpdateSupportVipStatus;

using Abstractions;using Behaviours;
using MediatR;
using Models;

[Sequential]
public class UpdateSupportVipStatusQuery : MBanqSelfServiceUserRequest, IRequest<SupportVipOfferActivateResponse>
{
}
