﻿namespace BNine.Application.Aggregates.MbanqAuth.Queries.GetMbanqTokensByLoginAndPassword
{
    using BNine.Application.Models.MBanq;
    using MediatR;

    public class GetMbanqTokensByLoginAndPasswordQuery : IRequest<AuthorizationTokensResponseModel>
    {
        public string Login
        {
            get; set;
        }

        public string Password
        {
            get; set;
        }
    }
}
