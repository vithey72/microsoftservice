﻿namespace BNine.Application.Aggregates.MbanqAuth.Queries.GetMbanqTokensByLoginAndPassword
{
    using System.Threading;
    using System.Threading.Tasks;
    using AutoMapper;
    using BNine.Application.Abstractions;
    using BNine.Application.Interfaces;
    using BNine.Application.Interfaces.Bank.Client;
    using BNine.Application.Models.MBanq;
    using MediatR;

    public class GetMbanqTokensByLoginAndPasswordQueryHandler
        : AbstractRequestHandler, IRequestHandler<GetMbanqTokensByLoginAndPasswordQuery, AuthorizationTokensResponseModel>
    {
        private IBankAuthorizationService BankAuthorizationService
        {
            get;
        }

        public GetMbanqTokensByLoginAndPasswordQueryHandler(
            IMediator mediator,
            IBNineDbContext dbContext,
            IMapper mapper,
            ICurrentUserService currentUser,
            IBankAuthorizationService bankAuthorizationService)
            : base(mediator, dbContext, mapper, currentUser)
        {
            BankAuthorizationService = bankAuthorizationService;
        }

        public async Task<AuthorizationTokensResponseModel> Handle(GetMbanqTokensByLoginAndPasswordQuery request, CancellationToken cancellationToken)
        {
            return await BankAuthorizationService.GetTokens(request.Login, request.Password);
        }
    }
}
