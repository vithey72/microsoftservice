﻿namespace BNine.Application.Aggregates.MbanqAuth.Queries.GetMbanqTokensByRefreshToken
{
    using BNine.Application.Models.MBanq;
    using MediatR;

    public class GetMbanqTokensByRefreshTokenQuery : IRequest<AuthorizationTokensResponseModel>
    {
        public string RefreshToken
        {
            get; set;
        }

        public string UserId
        {
            get; set;
        }
    }
}
