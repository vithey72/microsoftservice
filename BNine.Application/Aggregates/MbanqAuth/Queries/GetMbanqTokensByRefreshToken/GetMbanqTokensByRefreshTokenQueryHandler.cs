﻿namespace BNine.Application.Aggregates.MbanqAuth.Queries.GetMbanqTokensByRefreshToken
{
    using System.Threading;
    using System.Threading.Tasks;
    using AutoMapper;
    using BNine.Application.Abstractions;
    using BNine.Application.Interfaces;
    using BNine.Application.Interfaces.Bank.Client;
    using BNine.Application.Models.MBanq;
    using MediatR;

    public class GetMbanqTokensByRefreshTokenQueryHandler
        : AbstractRequestHandler, IRequestHandler<GetMbanqTokensByRefreshTokenQuery, AuthorizationTokensResponseModel>
    {
        private IBankAuthorizationService BankAuthorizationService
        {
            get;
        }

        public GetMbanqTokensByRefreshTokenQueryHandler(
            IMediator mediator,
            IBNineDbContext dbContext,
            IMapper mapper,
            ICurrentUserService currentUser,
            IBankAuthorizationService bankAuthorizationService)
            : base(mediator, dbContext, mapper, currentUser)
        {
            BankAuthorizationService = bankAuthorizationService;
        }

        public async Task<AuthorizationTokensResponseModel> Handle(GetMbanqTokensByRefreshTokenQuery request, CancellationToken cancellationToken)
        {
            return await BankAuthorizationService.GetTokensByRefreshToken(request.RefreshToken, request.UserId);
        }
    }
}
