﻿namespace BNine.Application.Aggregates.MbanqAuth.Queries.GetMbanqToken
{
    using System.Threading;
    using System.Threading.Tasks;
    using AutoMapper;
    using BNine.Application.Abstractions;
    using BNine.Application.Interfaces;
    using MediatR;

    public class GetMbanqTokenQueryHandler
        : AbstractRequestHandler, IRequestHandler<GetMbanqTokenQuery, string>
    {
        public GetMbanqTokenQueryHandler(
            IMediator mediator,
            IBNineDbContext dbContext,
            IMapper mapper,
            ICurrentUserService currentUser)
            : base(mediator, dbContext, mapper, currentUser)
        {
        }

        public Task<string> Handle(GetMbanqTokenQuery request, CancellationToken cancellationToken)
        {
            var mbanqToken = CurrentUser.UserMbanqToken;
            return Task.FromResult(mbanqToken);
        }
    }
}
