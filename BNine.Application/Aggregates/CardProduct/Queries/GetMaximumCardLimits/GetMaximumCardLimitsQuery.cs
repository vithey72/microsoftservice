﻿namespace BNine.Application.Aggregates.CardProduct.Queries.GetMaximumCardLimits
{
    using BNine.Application.Aggregates.Cards.Models;
    using MediatR;

    public class GetMaximumCardLimitsQuery : IRequest<CardLimitsResponseModel>
    {
    }
}
