﻿namespace BNine.Application.Aggregates.CardProduct.Queries.GetMaximumCardLimits
{
    using System.Threading;
    using System.Threading.Tasks;
    using AutoMapper;
    using BNine.Application.Abstractions;
    using BNine.Application.Aggregates.Cards.Models;
    using BNine.Application.Interfaces;
    using MediatR;

    public class GetMaximumCardLimitsQueryHandler
        : AbstractRequestHandler, IRequestHandler<GetMaximumCardLimitsQuery, CardLimitsResponseModel>
    {
        public GetMaximumCardLimitsQueryHandler(
            IMediator mediator,
            IBNineDbContext dbContext,
            IMapper mapper,
            ICurrentUserService currentUser)
            : base(mediator, dbContext, mapper, currentUser)
        {
        }

        public Task<CardLimitsResponseModel> Handle(GetMaximumCardLimitsQuery request, CancellationToken cancellationToken)
        {
            var result = new CardLimitsResponseModel()
            {
                CardOperations = new CardLimits()
                {
                    DailyLimit = 2500,
                    WeeklyLimit = 5000,
                    MonthlyLimit = 10000
                },
                CashWithdrawal = new CardLimits()
                {
                    DailyLimit = 1010,
                    WeeklyLimit = 5050,
                    MonthlyLimit = 10100
                }
            };

            return Task.FromResult(result);
        }
    }
}
