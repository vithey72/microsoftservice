﻿namespace BNine.Application.Aggregates.Banners.Models;

using System.Collections.Generic;

public record GetInfoPopupBannerResponse(List<InfoPopupBannerInstance> Banners);
