﻿namespace BNine.Application.Aggregates.Banners.Models;

using System.Collections.Generic;

public record GetAllInfoPopupBannersResponse(List<InfoPopupBannerInstanceSettings> Banners);
