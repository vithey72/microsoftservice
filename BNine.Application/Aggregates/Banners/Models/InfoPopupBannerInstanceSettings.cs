﻿namespace BNine.Application.Aggregates.Banners.Models;

using AutoMapper;
using Domain.Entities.Banners;

public class InfoPopupBannerInstanceSettings : InfoPopupBannerInstance
{
    public Guid Id
    {
        get;
        set;
    }

    public bool IsEnabled
    {
        get;
        set;
    }

    public DateTime? MinUserRegistrationDate
    {
        get;
        set;
    }

    public DateTime? ShowFrom
    {
        get;
        set;
    }

    public DateTime? ShowTil
    {
        get;
        set;
    }

    public TimeSpan? ShowCooldown
    {
        get;
        set;
    }

    public TimeSpan[] ShowCooldownProgressive
    {
        get;
        set;
    }

    public DateTime CreatedAt
    {
        get;
        set;
    }

    public DateTime? UpdatedAt
    {
        get;
        set;
    }

    public void Mapping(Profile profile) => profile.CreateMap<InfoPopupBanner, InfoPopupBannerInstanceSettings>()
        .IncludeBase<InfoPopupBanner, InfoPopupBannerInstance>()
        .ForMember(d => d.Id, o => o.MapFrom(s => s.Id))
        .ForMember(d => d.IsEnabled, o => o.MapFrom(s => s.IsEnabled))
        .ForMember(d => d.MinUserRegistrationDate, o => o.MapFrom(s => s.MinUserRegistrationDate))
        .ForMember(d => d.ShowFrom, o => o.MapFrom(s => s.ShowFrom))
        .ForMember(d => d.ShowTil, o => o.MapFrom(s => s.ShowTil))
        .ForMember(d => d.ShowCooldown, o => o.MapFrom(s => s.ShowCooldown))
        .ForMember(d => d.ShowCooldownProgressive, o => o.MapFrom(s => s.ShowCooldownProgressive))
        .ForMember(d => d.CreatedAt, o => o.MapFrom(s => s.CreatedAt))
        .ForMember(d => d.UpdatedAt, o => o.MapFrom(s => s.UpdatedAt))
        .ReverseMap()
    ;
}
