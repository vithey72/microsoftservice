﻿namespace BNine.Application.Aggregates.Banners.Models;

using AutoMapper;
using CommonModels.DialogElements;
using Domain.Entities.Banners;
using Enums;
using Mappings;


public class InfoPopupBannerInstance : IMapFrom<InfoPopupBanner>, IMapTo<InfoPopupBanner>
{
    public string Name
    {
        get;
        set;
    }

    public AppScreens Screen
    {
        get;
        set;
    }

    public string Title
    {
        get;
        set;
    }

    public string Subtitle
    {
        get;
        set;
    }

    public string ButtonText
    {
        get;
        set;
    }

    /// <summary>
    /// Nullable.
    /// </summary>
    [Obsolete("Replaced by DeeplinkObject")]
    public string Deeplink
    {
        get;
        set;
    }

    /// <summary>
    /// Nullable.
    /// </summary>
    public DeeplinkObject DeeplinkObject
    {
        get;
        set;
    }

    /// <summary>
    /// Nullable.
    /// </summary>
    public string ExternalLink
    {
        get;
        set;
    }

    public LinkFollowMode ExternalLinkType
    {
        get;
        set;
    }

    public string PictureUrl
    {
        get;
        set;
    }

    public bool BlockFurtherActions
    {
        get;
        set;
    }

    public Guid[] GroupIds
    {
        get;
        set;
    }

    public void Mapping(Profile profile) => profile.CreateMap<InfoPopupBanner, InfoPopupBannerInstance>()
        .ForMember(d => d.Name, o => o.MapFrom(s => s.Name))
        .ForMember(d => d.Title, o => o.MapFrom(s => s.Title))
        .ForMember(d => d.Subtitle, o => o.MapFrom(s => s.Subtitle))
        .ForMember(d => d.ButtonText, o => o.MapFrom(bi => bi.ButtonText))
        .ForMember(d => d.PictureUrl, o => o.MapFrom(s => s.PictureUrl))
        .ForMember(d => d.BlockFurtherActions, o => o.MapFrom(s => s.CannotBeClosed))
        .ForMember(d => d.Deeplink, o => o.MapFrom(s => s.ButtonDeeplink))
        .ForMember(d => d.DeeplinkObject, o => o.MapFrom(bi => new DeeplinkObject(bi.ButtonDeeplink, bi.ButtonDeeplinkArguments)))
        .ForMember(d => d.ExternalLink, o => o.MapFrom(s => s.ExternalLink))
        .ForMember(d => d.ExternalLinkType, o => o.MapFrom(s => s.ExternalLinkType))
        .ForMember(d => d.Screen, o => o.MapFrom(s => s.AppScreen))
        .ForMember(d => d.GroupIds, o => o.MapFrom(s => s.Groups))
        .ReverseMap()
    ;
}
