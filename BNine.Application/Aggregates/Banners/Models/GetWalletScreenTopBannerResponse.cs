﻿namespace BNine.Application.Aggregates.Banners.Models;

using System;
using CommonModels.DialogElements;
using Constants;
using Domain.Entities.Banners;
using Mappings;
using Newtonsoft.Json;

/// <summary>
/// Model of the top of the wallet screen green banner.
/// </summary>
public class GetWalletScreenTopBannerResponse
{
    /// <summary>
    /// List of banners in the order in which they should be displayed. Closing one immediately reveals the next one, if present.
    /// </summary>
    [JsonProperty("banners")]
    public BannerInstance[] Banners
    {
        get;
        set;
    } = Array.Empty<BannerInstance>();

    /// <summary>
    /// Instance of a banner. Has one default button and one close/dismiss button.
    /// </summary>
    public class BannerInstance : IMapFrom<SeenMarketingBanner>
    {
        /// <summary>
        /// Unique name of the banner, passed in the UserEvent of click on said banner's control.
        /// </summary>
        [JsonProperty("id")]
        public string Id
        {
            get;
            set;
        }

        /// <summary>
        /// Big text.
        /// </summary>
        [JsonProperty("title")]
        public string Title
        {
            get;
            set;
        }

        /// <summary>
        /// Smaller text. Can be null.
        /// </summary>
        [JsonProperty("subtitle")]
        public string Subtitle
        {
            get;
            set;
        }

        /// <summary>
        /// Text inside the button.
        /// </summary>
        [JsonProperty("buttonTitle")]
        public string ButtonTitle
        {
            get;
            set;
        }

        /// <summary>
        /// Deeplink for place in the app to which clicking the button leads to.
        /// </summary>
        [Obsolete("Replaced by DeeplinkObject")]
        [JsonProperty("deeplink")]
        public string Deeplink
        {
            get;
            set;
        }

        [JsonProperty("deeplinkObject")]
        public DeeplinkObject DeeplinkObject
        {
            get;
            set;
        }

        /// <summary>
        /// Filled if type of deeplink is <see cref="DeepLinkRoutes.ExternalLink"/>.
        /// </summary>
        [JsonProperty("externalLink")]
        public string ExternalLink
        {
            get;
            set;
        }

        /// <summary>
        /// Whether close button should be rendered.
        /// </summary>
        [JsonProperty("canClose")]
        public bool CanClose
        {
            get;
            set;
        } = true;
    }
}
