﻿namespace BNine.Application.Aggregates.Banners.Queries;

using Abstractions;
using MediatR;
using Models;

public class GetWalletScreenTopBannerQuery : MBanqSelfServiceUserRequest, IRequest<GetWalletScreenTopBannerResponse>
{
}
