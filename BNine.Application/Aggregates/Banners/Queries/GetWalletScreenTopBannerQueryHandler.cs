﻿namespace BNine.Application.Aggregates.Banners.Queries;

using System.Threading.Tasks;
using AutoMapper;
using Interfaces;
using Interfaces.ServiceBus;
using MediatR;
using Models;
using Users.Queries.Cashback;
using Array = System.Array;

public class GetWalletScreenTopBannerQueryHandler
    : BaseCashbackQueryHandler
    , IRequestHandler<GetWalletScreenTopBannerQuery, GetWalletScreenTopBannerResponse>
{
    public GetWalletScreenTopBannerQueryHandler(
        IMediator mediator,
        IBNineDbContext dbContext,
        IMapper mapper,
        ICurrentUserService currentUser,
        IDateTimeProviderService dateTimeProvider,
        IServiceBus serviceBus
        )
        : base(mediator, dbContext, mapper, currentUser, serviceBus, dateTimeProvider)
    {
    }

    public Task<GetWalletScreenTopBannerResponse> Handle(GetWalletScreenTopBannerQuery request, CancellationToken cancellationToken)
    {
        // https://bninecom.atlassian.net/browse/B9-4981
        return Task.FromResult(new GetWalletScreenTopBannerResponse
        {
            Banners = Array.Empty<GetWalletScreenTopBannerResponse.BannerInstance>(),
        });
    }
}
