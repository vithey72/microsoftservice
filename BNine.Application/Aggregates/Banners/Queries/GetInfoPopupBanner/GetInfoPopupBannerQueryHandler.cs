﻿namespace BNine.Application.Aggregates.Banners.Queries.GetInfoPopupBanner;

using System;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Abstractions;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using Domain.Entities.Banners;
using Enums;
using Interfaces;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Models;

public class GetInfoPopupBannerQueryHandler
    : AbstractRequestHandler
    , IRequestHandler<GetInfoPopupBannerQuery, GetInfoPopupBannerResponse>
{
    private readonly IMarketingBannersService _bannersService;

    public GetInfoPopupBannerQueryHandler(
        IMediator mediator,
        IBNineDbContext dbContext,
        IMapper mapper,
        ICurrentUserService currentUser,
        IMarketingBannersService bannersService
        )
        : base(mediator, dbContext, mapper, currentUser)
    {
        _bannersService = bannersService;
    }

    public async Task<GetInfoPopupBannerResponse> Handle(GetInfoPopupBannerQuery request, CancellationToken cancellationToken)
    {
        if (request.MbanqAccessToken is null)
        {
            return await HandleNoAuthCase(cancellationToken);
        }

        var accessToken = CurrentUser.AccessToken;
        if (accessToken != null && accessToken.ValidTo < DateTime.UtcNow)
        {
            throw new UnauthorizedAccessException("Token has expired");
        }

        return await HandleUserCase(request, cancellationToken);
    }

    private async Task<GetInfoPopupBannerResponse> HandleNoAuthCase(CancellationToken cancellationToken)
    {
        var banners = await DbContext.InfoPopupMarketingBanners
            .Where(b => b.IsEnabled && b.AppScreen == AppScreens.Login)
            .ProjectTo<InfoPopupBannerInstance>(Mapper.ConfigurationProvider)
            .ToListAsync(cancellationToken);

        return new GetInfoPopupBannerResponse(banners);
    }

    private async Task<GetInfoPopupBannerResponse> HandleUserCase(GetInfoPopupBannerQuery request, CancellationToken cancellationToken)
    {
        var banners = await _bannersService
            .GetWalletScreenBanners<InfoPopupBanner, InfoPopupBannerInstance>();

        banners = banners
            .Where(b => request.Screen == AppScreens.Unknown || b.Screen == request.Screen)
            .ToList();

        await FillPlaceholders(banners, cancellationToken);
        return new GetInfoPopupBannerResponse(banners);
    }

    private async Task FillPlaceholders(List<InfoPopupBannerInstance> banners, CancellationToken token)
    {
        var regex = new Regex(@"\{(\w+)\}", RegexOptions.Compiled);
        var placeholders = await DbContext
            .UserPlaceholderTexts
            .Where(x => x.UserId == CurrentUser.UserId)
            .ToDictionaryAsync(x => x.Key, y => y.Value, token);

        var bannersToHide = new List<InfoPopupBannerInstance>();
        foreach (var banner in banners)
        {
            try
            {
                banner.Title = ReplacePlaceholders(banner.Title, regex, placeholders);
                banner.Subtitle = ReplacePlaceholders(banner.Subtitle, regex, placeholders);
                banner.ButtonText = ReplacePlaceholders(banner.ButtonText, regex, placeholders);
                banner.PictureUrl = ReplacePlaceholders(banner.PictureUrl, regex, placeholders);
                banner.ExternalLink = ReplacePlaceholders(banner.ExternalLink, regex, placeholders);
                banner.Deeplink = ReplacePlaceholders(banner.Deeplink, regex, placeholders);
            }
            catch
            {
                bannersToHide.Add(banner);
            }
        }

        // save ourselves from embarrassment of showing raw placeholder if we don't have the text for it
        banners.RemoveAll(b => bannersToHide.Contains(b));
    }

    private static string ReplacePlaceholders(string text, Regex regex, Dictionary<string, string> placeholders)
    {
        return text == null
            ? null
            : regex.Replace(text, match => placeholders[match.Groups[1].Value]);
    }
}
