﻿namespace BNine.Application.Aggregates.Banners.Queries.GetInfoPopupBanner;

using Abstractions;
using Enums;
using MediatR;
using Models;

public class GetInfoPopupBannerQuery
    : MBanqSelfServiceUserRequest
    , IRequest<GetInfoPopupBannerResponse>
{
    public AppScreens Screen
    {
        get;
        set;
    }
}
