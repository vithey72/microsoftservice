﻿namespace BNine.Application.Aggregates.Banners.Queries.UpdateInfoPopupBanner;

using MediatR;
using Models;

public class UpdateInfoPopupBannerQuery
    : InfoPopupBannerInstanceSettings
    , IRequest<InfoPopupBannerInstanceSettings>
{
}
