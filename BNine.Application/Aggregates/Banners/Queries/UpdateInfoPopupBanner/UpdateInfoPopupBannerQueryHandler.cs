﻿namespace BNine.Application.Aggregates.Banners.Queries.UpdateInfoPopupBanner;

using System.Threading.Tasks;
using Abstractions;
using AutoMapper;
using Interfaces;
using MediatR;
using Models;

public class UpdateInfoPopupBannerQueryHandler
    : AbstractRequestHandler
    , IRequestHandler<UpdateInfoPopupBannerQuery, InfoPopupBannerInstanceSettings>

{
    private readonly IBNineDbContext _dbContext;
    private readonly IMarketingBannersAdminService _bannersAdminService;

    public UpdateInfoPopupBannerQueryHandler(
        IMediator mediator,
        IBNineDbContext dbContext,
        IMapper mapper,
        ICurrentUserService currentUser,
        IMarketingBannersAdminService bannersAdminService
        )
        : base(mediator, dbContext, mapper, currentUser)
    {
        _dbContext = dbContext;
        _bannersAdminService = bannersAdminService;
    }

    public async Task<InfoPopupBannerInstanceSettings> Handle(UpdateInfoPopupBannerQuery request, CancellationToken token)
    {
        return await _bannersAdminService.UpdateBanner<InfoPopupBannerInstanceSettings>(request, token);
    }
}
