﻿namespace BNine.Application.Aggregates.Banners.Queries.CreateInfoPopupBanner;

using MediatR;
using Models;

public class CreateInfoPopupBannerQuery
    : InfoPopupBannerInstanceSettings
    , IRequest<InfoPopupBannerInstanceSettings>
{
}
