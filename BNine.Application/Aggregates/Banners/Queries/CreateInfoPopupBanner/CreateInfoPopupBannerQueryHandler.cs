﻿namespace BNine.Application.Aggregates.Banners.Queries.CreateInfoPopupBanner;

using System.Threading.Tasks;
using Abstractions;
using AutoMapper;
using Interfaces;
using MediatR;
using Models;

public class CreateInfoPopupBannerQueryHandler
    : AbstractRequestHandler
    , IRequestHandler<CreateInfoPopupBannerQuery, InfoPopupBannerInstanceSettings>

{
    private readonly IBNineDbContext _dbContext;
    private readonly IMarketingBannersAdminService _bannersAdminService;

    public CreateInfoPopupBannerQueryHandler(
        IMediator mediator,
        IBNineDbContext dbContext,
        IMapper mapper,
        ICurrentUserService currentUser,
        IMarketingBannersAdminService bannersAdminService
        )
        : base(mediator, dbContext, mapper, currentUser)
    {
        _dbContext = dbContext;
        _bannersAdminService = bannersAdminService;
    }

    public async Task<InfoPopupBannerInstanceSettings> Handle(CreateInfoPopupBannerQuery request, CancellationToken token)
    {
        return await _bannersAdminService.CreateBanner<InfoPopupBannerInstanceSettings>(request, token);
    }
}
