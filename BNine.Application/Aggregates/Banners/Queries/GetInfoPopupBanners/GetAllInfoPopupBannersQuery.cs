﻿namespace BNine.Application.Aggregates.Banners.Queries.GetInfoPopupBanners;

using MediatR;
using Models;

public record GetAllInfoPopupBannersQuery : IRequest<GetAllInfoPopupBannersResponse>;
