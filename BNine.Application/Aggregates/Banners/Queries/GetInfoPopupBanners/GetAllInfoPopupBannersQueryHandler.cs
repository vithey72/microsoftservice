﻿namespace BNine.Application.Aggregates.Banners.Queries.GetInfoPopupBanners;

using System.Threading.Tasks;
using Abstractions;
using AutoMapper;
using Domain.Entities.Banners;
using Interfaces;
using MediatR;
using Models;

public class GetAllInfoPopupBannersQueryHandler
    : AbstractRequestHandler
    , IRequestHandler<GetAllInfoPopupBannersQuery, GetAllInfoPopupBannersResponse>
{
    private readonly IMarketingBannersAdminService _bannersAdminService;

    public GetAllInfoPopupBannersQueryHandler(
        IMediator mediator,
        IBNineDbContext dbContext,
        IMapper mapper,
        ICurrentUserService currentUser,
        IMarketingBannersAdminService bannersAdminService
        )
        : base(mediator, dbContext, mapper, currentUser)
    {
        _bannersAdminService = bannersAdminService;
    }

    public async Task<GetAllInfoPopupBannersResponse> Handle(GetAllInfoPopupBannersQuery request, CancellationToken cancellationToken)
    {
        var banners = await _bannersAdminService
            .GetWalletScreenBanners<InfoPopupBanner, InfoPopupBannerInstanceSettings>();

        return new GetAllInfoPopupBannersResponse(banners);
    }
}
