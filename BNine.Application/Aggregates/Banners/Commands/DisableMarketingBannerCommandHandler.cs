﻿namespace BNine.Application.Aggregates.Banners.Commands;

using Abstractions;
using AutoMapper;
using Constants;
using Interfaces;
using MediatR;

public class DisableMarketingBannerCommandHandler
    : AbstractRequestHandler
    , IRequestHandler<DisableMarketingBannerCommand, Unit>
{
    private readonly IMarketingBannersService _marketingBannersService;
    private readonly IDateTimeProviderService _dateTimeProvider;

    public DisableMarketingBannerCommandHandler(
        IMediator mediator,
        IBNineDbContext dbContext,
        IMapper mapper,
        ICurrentUserService currentUser,
        IMarketingBannersService marketingBannersService,
        IDateTimeProviderService dateTimeProvider
        )
        : base(mediator, dbContext, mapper, currentUser)
    {
        _marketingBannersService = marketingBannersService;
        _dateTimeProvider = dateTimeProvider;
    }

    public async Task<Unit> Handle(DisableMarketingBannerCommand request, CancellationToken cancellationToken)
    {
        if (request.BannerId == BannersNames.CashbackBanner)
        {
            var bannerReactivationDate = InferReactivationDate();
            await _marketingBannersService.MarkBannerAsSeen(request.BannerId, bannerReactivationDate);
        }
        else
        {
            await _marketingBannersService.MarkBannerAsSeen(request.BannerId);
        }

        return Unit.Value;
    }

    private DateTime InferReactivationDate()
    {
        var now = _dateTimeProvider.Now();
        var bannerReactivationDate = new DateTime(now.Year, now.Month, 5);
        if (bannerReactivationDate < now)
        {
            bannerReactivationDate = bannerReactivationDate.AddMonths(1);
        }

        return bannerReactivationDate;
    }
}
