﻿namespace BNine.Application.Aggregates.Banners.Commands;

using MediatR;

public class DisableMarketingBannerCommand : IRequest<Unit>
{
    public string BannerId
    {
        get;
        set;
    }
}
