﻿namespace BNine.Application.Aggregates.Banners.Commands.DeleteInfoPopup;

using System.Threading.Tasks;
using Abstractions;
using AutoMapper;
using Domain.Entities.Banners;
using Interfaces;
using MediatR;

public class DeleteInfoPopupCommandHandler : AbstractRequestHandler, IRequestHandler<DeleteInfoPopupCommand>
{
    private readonly IMarketingBannersAdminService _bannersAdminService;

    public DeleteInfoPopupCommandHandler(
        IMediator mediator,
        IBNineDbContext dbContext,
        IMapper mapper,
        ICurrentUserService currentUser,
        IMarketingBannersAdminService bannersAdminService
    )
        : base(mediator, dbContext, mapper, currentUser)
    {
        _bannersAdminService = bannersAdminService;
    }

    public async Task<Unit> Handle(DeleteInfoPopupCommand request, CancellationToken cancellationToken)
    {
        await _bannersAdminService.DeleteBanner<InfoPopupBanner>(request.PopupId, cancellationToken);
        return Unit.Value;
    }
}
