﻿namespace BNine.Application.Aggregates.Banners.Commands.DeleteInfoPopup;

using System;
using MediatR;

public record DeleteInfoPopupCommand(Guid PopupId) : IRequest<Unit>;
