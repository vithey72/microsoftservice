﻿namespace BNine.Application.Aggregates.LoanSettings.Queries.GetACHPrincipalKeywords
{
    using System.Collections.Generic;
    using MediatR;

    public class GetACHPrincipalKeywordsQuery
        : IRequest<IEnumerable<string>>
    {
    }
}
