﻿namespace BNine.Application.Aggregates.LoanSettings.Queries.GetACHPrincipalKeywords
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading;
    using System.Threading.Tasks;
    using AutoMapper;
    using BNine.Application.Abstractions;
    using BNine.Application.Interfaces;
    using MediatR;
    using Microsoft.EntityFrameworkCore;

    public class GetACHPrincipalKeywordsQueryHandler
        : AbstractRequestHandler, IRequestHandler<GetACHPrincipalKeywordsQuery, IEnumerable<string>>
    {
        public GetACHPrincipalKeywordsQueryHandler(IMediator mediator, IBNineDbContext dbContext, IMapper mapper, ICurrentUserService currentUser) : base(mediator, dbContext, mapper, currentUser)
        {
        }

        public async Task<IEnumerable<string>> Handle(GetACHPrincipalKeywordsQuery request, CancellationToken cancellationToken)
        {
            var keywords = await DbContext.LoanSettings
                .Select(x => x.ACHPrincipalKeywords)
                .SingleOrDefaultAsync(cancellationToken);

            return keywords;
        }
    }
}
