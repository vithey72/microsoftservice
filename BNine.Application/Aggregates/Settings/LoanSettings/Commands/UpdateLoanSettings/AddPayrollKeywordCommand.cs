﻿namespace BNine.Application.Aggregates.Settings.LoanSettings.Commands.UpdateLoanSettings
{
    using MediatR;

    public class AddPayrollKeywordCommand
        : IRequest<Unit>
    {
        public string Keyword
        {
            get; set;
        }
    }
}
