﻿namespace BNine.Application.Aggregates.Settings.LoanSettings.Commands.UpdateLoanSettings
{
    using FluentValidation;

    public class AddPayrollKeywordCommandValidator
        : AbstractValidator<AddPayrollKeywordCommand>
    {
        public AddPayrollKeywordCommandValidator()
        {
            RuleFor(x => x.Keyword)
                .NotNull()
                .NotEmpty();
        }
    }
}
