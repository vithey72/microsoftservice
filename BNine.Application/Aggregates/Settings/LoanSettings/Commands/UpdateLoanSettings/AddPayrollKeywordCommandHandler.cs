﻿namespace BNine.Application.Aggregates.Settings.LoanSettings.Commands.UpdateLoanSettings
{
    using System.Threading;
    using System.Threading.Tasks;
    using AutoMapper;
    using BNine.Application.Abstractions;
    using BNine.Application.Aggregates.ServiceBusEvents.Commands.PublishB9Event;
    using BNine.Application.Interfaces;
    using BNine.Constants;
    using MediatR;
    using Microsoft.EntityFrameworkCore;

    public class AddPayrollKeywordCommandHandler
        : AbstractRequestHandler, IRequestHandler<AddPayrollKeywordCommand, Unit>
    {
        public AddPayrollKeywordCommandHandler(
            IMediator mediator,
            IBNineDbContext dbContext,
            IMapper mapper,
            ICurrentUserService currentUser
            ) : base(mediator, dbContext, mapper, currentUser)
        {
        }

        public async Task<Unit> Handle(AddPayrollKeywordCommand request, CancellationToken cancellationToken)
        {
            var loanSettings = await DbContext.LoanSettings
                .SingleOrDefaultAsync(cancellationToken);

            loanSettings.ACHPrincipalKeywords = loanSettings.ACHPrincipalKeywords.Add(request.Keyword.Trim());
            loanSettings.UpdatedAt = System.DateTime.UtcNow;

            await DbContext.SaveChangesAsync(cancellationToken);

            await Mediator.Send(new PublishB9EventCommand(loanSettings, ServiceBusTopics.B9LoanSettings, ServiceBusEvents.B9Updated));

            return Unit.Value;
        }
    }
}
