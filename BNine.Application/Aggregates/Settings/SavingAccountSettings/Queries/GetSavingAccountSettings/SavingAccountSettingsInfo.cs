﻿namespace BNine.Application.Aggregates.SavingAccountSettings.Queries.GetSavingAccountSettings
{
    using BNine.Application.Mappings;
    using Newtonsoft.Json;
    using Domain.Entities.Settings;
    using AutoMapper;

    public class SavingAccountSettingsInfo : IMapFrom<SavingAccountSettings>
    {
        [JsonProperty("pullFromCardEnabled")]
        public bool PullFromCardEnabled
        {
            get; set;
        }

        public void Mapping(Profile profile) => profile.CreateMap<SavingAccountSettings, SavingAccountSettingsInfo>()
            .ForMember(d => d.PullFromCardEnabled, opt => opt.MapFrom(s => s.IsPullFromExternalCardEnabled));
    }
}
