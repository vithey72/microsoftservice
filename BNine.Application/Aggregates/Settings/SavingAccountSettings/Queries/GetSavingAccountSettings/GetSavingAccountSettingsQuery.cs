﻿namespace BNine.Application.Aggregates.SavingAccountSettings.Queries.GetSavingAccountSettings
{
    using MediatR;

    public class GetSavingAccountSettingsQuery
        : IRequest<SavingAccountSettingsInfo>
    {
    }
}
