﻿namespace BNine.Application.Aggregates.SavingAccountSettings.Queries.GetSavingAccountSettings
{
    using System.Threading;
    using System.Threading.Tasks;
    using AutoMapper;
    using AutoMapper.QueryableExtensions;
    using BNine.Application.Abstractions;
    using BNine.Application.Exceptions;
    using BNine.Application.Interfaces;
    using MediatR;
    using Microsoft.EntityFrameworkCore;

    public class GetSavingAccountSettingsQueryHandler
        : AbstractRequestHandler, IRequestHandler<GetSavingAccountSettingsQuery, SavingAccountSettingsInfo>
    {
        public GetSavingAccountSettingsQueryHandler(IMediator mediator, IBNineDbContext dbContext, IMapper mapper, ICurrentUserService currentUser) : base(mediator, dbContext, mapper, currentUser)
        {
        }

        public async Task<SavingAccountSettingsInfo> Handle(GetSavingAccountSettingsQuery request, CancellationToken cancellationToken)
        {
            var settings = await DbContext.SavingAccountSettings
                .ProjectTo<SavingAccountSettingsInfo>(Mapper.ConfigurationProvider)
                .SingleOrDefaultAsync(cancellationToken);

            if (settings == null)
            {
                throw new NotFoundException(nameof(SavingAccountSettings), new { });
            }

            return settings;
        }
    }
}
