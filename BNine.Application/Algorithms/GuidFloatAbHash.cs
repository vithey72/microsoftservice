namespace BNine.Application.Algorithms
{
    using System;
    using System.Runtime.InteropServices;

    public static class GuidFloatAbHashAlgorithm
    {
        public static float Calculate(Guid id, int seed)
        {
            var ints = MemoryMarshal.Cast<byte, uint>(id.ToByteArray());
            return unchecked(ints[0] * 67867967 ^
                ints[1] * 86028121 ^
                ints[2] * 104395301 ^
                ints[3] * 122949823 ^
                (uint)seed * 141650939) / (float)uint.MaxValue;
        }
    }
}