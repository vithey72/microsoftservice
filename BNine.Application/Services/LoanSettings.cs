﻿namespace BNine.Application.Services;

using BNine.Application.Interfaces;

public record LoanSettings(int BoostExpressFeeChargeTypeId) : ILoanSettings;
