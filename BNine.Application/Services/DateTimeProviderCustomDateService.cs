﻿namespace BNine.Application.Services;

using Interfaces;
using Microsoft.Extensions.Configuration;

public class DateTimeProviderCustomDateService : IDateTimeProviderService
{
    private readonly DateTime? _customDate;

    public DateTimeProviderCustomDateService(IConfiguration configuration)
    {
        try
        {
            _customDate = DateTime.Parse(configuration["DateTimeProviderCustomDate"]);
        }
        catch
        {
            // ignored
        }
    }
    public DateTime Now() => _customDate == null
        ? DateTime.Now
        : CombineDateAndTime(_customDate.Value, DateTime.Now);

    private DateTime CombineDateAndTime(DateTime dateSource, DateTime timeSource)
    {
        return new DateTime(dateSource.Year, dateSource.Month, dateSource.Day,
            timeSource.Hour, timeSource.Minute, timeSource.Second);
    }
}
