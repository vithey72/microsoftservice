﻿namespace BNine.Application.Services;

using Interfaces;

public class DateTimeProviderService : IDateTimeProviderService, IDateTimeUtcProviderService
{
    public DateTime Now() => DateTime.Now;
    public DateTime UtcNow() => DateTime.UtcNow;
}
