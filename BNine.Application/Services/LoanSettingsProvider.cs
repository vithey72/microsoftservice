﻿namespace BNine.Application.Services;

using System.Threading;
using System.Threading.Tasks;
using BNine.Application.Exceptions;
using BNine.Application.Interfaces;
using Microsoft.EntityFrameworkCore;

public class LoanSettingsProvider : ILoanSettingsProvider
{
    private readonly IBNineDbContext _dbContext;

    public LoanSettingsProvider(
        IBNineDbContext dbContext
        )
    {
        _dbContext = dbContext;
    }

    public async Task<ILoanSettings> GetLoanSettings(CancellationToken cancellationToken)
    {
        var loanSettings = await _dbContext
            .LoanSettings
            .FirstOrDefaultAsync(cancellationToken);

        if (loanSettings is null)
        {
            throw new NotFoundException("LoanSettings table is empty.");
        }

        return new LoanSettings(loanSettings.BoostExpressFeeChargeTypeId);
    }
}
