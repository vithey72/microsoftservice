﻿namespace BNine.Application.Services;

using System.Globalization;
using System.Text.RegularExpressions;
using Aggregates.CommonModels;
using Common;
using Constants;
using Domain.Entities;
using Domain.Entities.User;
using Enums;
using Helpers;
using Interfaces;
using Microsoft.EntityFrameworkCore;
using Models.Notifications;

public class NotificationUsersService : INotificationUsersService
{
    private readonly IBNineDbContext _dbContext;
    private readonly IPushNotificationService _pushNotificationService;
    private readonly IPartnerProviderService _partnerProvider;

    public NotificationUsersService(
        IBNineDbContext dbContext,
        IPushNotificationService pushNotificationService,
        IPartnerProviderService partnerProvider
    )
    {
        _partnerProvider = partnerProvider;
        _dbContext = dbContext;
        _pushNotificationService = pushNotificationService;
    }

    public async Task SendAdvanceIssued(Guid userId, decimal amount, decimal balance, string date)
    {
        var user = await _dbContext.Users.Include(x => x.Devices)
            .Where(x => x.Id == userId)
            .FirstOrDefaultAsync();

        var notification =
            $"{CurrencyFormattingHelper.AsRegular(amount)} has posted to your account. Balance {CurrencyFormattingHelper.AsRegular(balance)}. We expect your next paycheck by {date}";
        await NotifyUser(user.Id, notification, user.Settings.IsNotificationsEnabled, NotificationTypeEnum.Transaction,
            false, user.Devices, null, null, PushNotificationCategory.AdvanceActivated, "Payday has come early!");
    }

    public async Task SendInternalTransferNotifications(string debtorUserSavingsId, string creditorUserSavingsId,
        decimal amount, decimal debtorBalance, decimal creditorBalance, string note)
    {
        var debtor = await _dbContext.Users.Include(x => x.Devices)
            .Where(x => x.CurrentAccount.ExternalId.ToString().Equals(debtorUserSavingsId))
            .FirstOrDefaultAsync();

        var creditor = await _dbContext.Users.Include(x => x.Devices)
            .Where(x => x.CurrentAccount.ExternalId.ToString().Equals(creditorUserSavingsId))
            .FirstOrDefaultAsync();

        var notificationPrefix = string.IsNullOrEmpty(note) ? string.Empty : $"{note}\r\n";
        if (creditor != null)
        {
            var debtorName = GetName(debtor);

            var notification = notificationPrefix +
                               $@"{CurrencyFormattingHelper.AsRegular(amount)} (Internal Transfer) from {debtorName} has posted to your account. Balance {CurrencyFormattingHelper.AsRegular(creditorBalance)}";
            await NotifyUser(creditor.Id, notification, creditor.Settings.IsNotificationsEnabled,
                NotificationTypeEnum.Transaction, false, creditor.Devices, null, null, "Money received",
                "Money received");
        }

        if (debtor != null)
        {
            var creditorName = GetName(creditor);

            var notification = notificationPrefix +
                               $@"You sent {CurrencyFormattingHelper.AsRegular(amount)} (Internal transfer) to {$"{creditorName}"}. Balance {CurrencyFormattingHelper.AsRegular(debtorBalance)}";
            await NotifyUser(debtor.Id, notification, debtor.Settings.IsNotificationsEnabled,
                NotificationTypeEnum.Transaction, false, debtor.Devices, null, null, "Payment started",
                "Payment started");
        }

        string GetName(User user)
        {
            return user == null ? StringProvider.GetPartnerName(_partnerProvider) : $"{user.FirstName} {user.LastName}";
        }
    }

    public async Task SendPINChangedNotification(int clientId)
    {
        var user = await _dbContext.Users
            .Include(x => x.Devices)
            .FirstAsync(x => x.ExternalClientId == clientId);

        var notification = "PIN was changed successfully";

        await NotifyUser(user.Id, notification, user.Settings.IsNotificationsEnabled, NotificationTypeEnum.Information,
            true, user.Devices, null, null,
            PushNotificationCategory.PinWasChangedSuccessfully);
    }

    public async Task SendPhysicalCardActivated(int clientId)
    {
        var user = await _dbContext.Users
            .Include(x => x.Devices)
            .FirstAsync(x => x.ExternalClientId == clientId);

        var notification = $"Your {StringProvider.GetPartnerName(_partnerProvider)} card was activated";

        await NotifyUser(user.Id, notification, user.Settings.IsNotificationsEnabled, NotificationTypeEnum.Information,
            true, user.Devices, null, null,
            PushNotificationCategory.YourB9CardWasActivated);
    }

    public async Task SendNotification(Guid userId, string notificationText, bool isNotificationsEnabled,
        NotificationTypeEnum type, bool isImportant, IEnumerable<Device> devices, string deeplink,
        string appsflyerEventType, string category, string title, string sub1)
    {
        await NotifyUser(userId, notificationText, isNotificationsEnabled, type, isImportant, devices, deeplink,
            appsflyerEventType, category, title, sub1);
    }

    public async Task SendNotification(User user, GenericPushNotification notification, bool isImportant)
    {
        var message = notification.Message;
        string sub1 = null;

        if (notification.Parameters != null)
        {
            var regex = new Regex(@"\{(\w+)\}", RegexOptions.Compiled);
            message = regex.Replace(notification.Message, match => notification.Parameters[match.Groups[1].Value]);
            sub1 = notification.Parameters.FirstOrDefault(x => x.Key == "sub1").Value;
        }

        await NotifyUser(
            user.Id,
            message,
            user.Settings.IsNotificationsEnabled,
            notification.NotificationType,
            isImportant,
            user.Devices,
            notification.Deeplink,
            notification.AppsflyerEventType,
            notification.Category,
            notification.Title,
            sub1);
    }

    public async Task SendACHDebitNotification(Guid userId, decimal availableBalance, decimal amount, string name)
    {
        var user = await _dbContext.Users
            .Include(x => x.Devices)
            .Where(x => x.Id == userId)
            .FirstOrDefaultAsync();

        var notification =
            $"You sent {CurrencyFormattingHelper.AsRegular(amount)} (ACH) to {name}. Balance {availableBalance.ToString("C", CultureInfo.CreateSpecificCulture("en-us"))}";

        await NotifyUser(user.Id, notification, user.Settings.IsNotificationsEnabled, NotificationTypeEnum.Transaction,
            false, user.Devices, null, null, PushNotificationCategory.PaymentStarted, "Payment started");
    }

    public async Task SendPushToExternalNotification(Guid userId, string accountNumber, decimal requestAmount,
        decimal accountBalance)
    {
        var user = await _dbContext.Users
            .Include(u => u.Devices)
            .Where(x => x.Id == userId)
            .FirstOrDefaultAsync();

        await NotifyUser(userId,
            $"You sent {CurrencyFormattingHelper.AsRegular(requestAmount)} " +
            $"from B9 account to your external card {SecretNumberHelper.GetLastFourDigits(accountNumber)}. " +
            $"Balance {CurrencyFormattingHelper.AsRegular(accountBalance)}",
            user.Settings.IsNotificationsEnabled,
            NotificationTypeEnum.Transaction,
            false,
            user.Devices,
            null,
            null,
            PushNotificationCategory.PaymentStarted,
            PushNotificationCategory.PaymentStarted);
    }

    public async Task SendCardReorderedNotification(int cardExternalId, string dataLastDigits)
    {
        var card = await _dbContext
            .DebitCards
            .Include(c => c.User)
            .ThenInclude(u => u.Devices)
            .FirstOrDefaultAsync(c => c.ExternalId == cardExternalId);

        var user = card?.User;
        if (user == null)
        {
            return;
        }

        var notification = $"Your {StringProvider.GetPartnerName(_partnerProvider)} Card {dataLastDigits} is ready for use. " +
                           $"The {StringProvider.GetPartnerName(_partnerProvider)} Team";
        await NotifyUser(user.Id, notification, user.Settings.IsNotificationsEnabled, NotificationTypeEnum.Information,
            true, user.Devices, null, null, PushNotificationCategory.ActivateCard);
    }

    public async Task SendStealthyPushNotification(User user, Device device, string notificationText, string deeplink, string sub)
    {
        var notificationKit =
            new List<NotificationTarget>() { new NotificationTarget(device.Id.ToString(), device.Type) };

        var notification =
            new Notification(notificationText,
                notificationKit,
                deeplink,
                null, null,
                sub);

        await _pushNotificationService.SendNotification(notification);
    }

    public async Task SendSilentPushNotification(User user, string deeplink, string sub = null)
    {
        var notificationKit = user.Devices
            .Select(GetTarget)
            .ToList();

        var notification = new Notification(
            null,
            notificationKit,
            deeplink,
            null,
            null,
            sub,
            isSilent: true);

        await _pushNotificationService.SendNotification(notification);
    }

    public async Task SendNotifications(IEnumerable<SendNotificationListItem> notifications)
    {
        foreach (var item in notifications)
        {
            if (item.IsNotificationsEnabled)
            {
                var notificationKit =
                    item.Devices.Select(x => new NotificationTarget(x.Id.ToString(), x.Type)).ToList();

                var notification = new Notification(item.NotificationText, notificationKit, item.Deeplink,
                    item.AppsflyerEventType);

                await _pushNotificationService.SendNotification(notification);
            }

            _dbContext.Notifications.Add(new Domain.Entities.Notification.Notification
            {
                CreatedAt = DateTime.UtcNow,
                Text = item.NotificationText,
                IsRead = false,
                UserId = item.UserId,
                Category = item.NotificationCategory,
                Title = item.NotificationTitle,
                Type = item.Type,
                IsImportant = item.IsImportant,
                Deeplink = item.Deeplink
            });
        }

        await _dbContext.SaveChangesAsync(CancellationToken.None);
    }

    private async Task NotifyUser(Guid userId, string notificationText, bool isNotificationsEnabled,
        NotificationTypeEnum type, bool isImportant, IEnumerable<Device> devices, string deeplink,
        string appsflyerEventType, string category, string title = null, string sub1 = null)
    {
        if (isNotificationsEnabled)
        {
            var notificationKit = devices.Select(x => new NotificationTarget(x.Id.ToString(), x.Type)).ToList();

            var notification = new Notification(notificationText, notificationKit, deeplink, appsflyerEventType, title,
                sub1);

            await _pushNotificationService.SendNotification(notification);
        }

        _dbContext.Notifications.Add(new Domain.Entities.Notification.Notification
        {
            CreatedAt = DateTime.UtcNow,
            Text = notificationText,
            Title = title,
            IsRead = false,
            UserId = userId,
            Category = category,
            Sub1 = sub1,
            Type = type,
            IsImportant = isImportant,
            Deeplink = deeplink
        });

        await _dbContext.SaveChangesAsync(CancellationToken.None);
    }

    private NotificationTarget GetTarget(Device device)
    {
        return new NotificationTarget(device.Id.ToString(), device.Type);
    }
}
