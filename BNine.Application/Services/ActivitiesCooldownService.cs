﻿namespace BNine.Application.Services;

using Domain.Entities;
using Interfaces;
using Microsoft.EntityFrameworkCore;

public class UserActivitiesCooldownService : IUserActivitiesCooldownService
{
    private readonly IBNineDbContext _dbContext;

    public UserActivitiesCooldownService(
        IBNineDbContext dbContext
        )
    {
        _dbContext = dbContext;
    }

    public async Task<bool> IsOnCooldown(Guid userId, string activityName, CancellationToken cancellationToken)
    {
        await FlushOutdatedEntries();
        var existingEntry = await FindCooldownEntry(userId, activityName, cancellationToken);
        return existingEntry != null;
    }

    public async Task<bool> AddCooldown(Guid userId, string activityName, TimeSpan duration, CancellationToken cancellationToken)
    {
        await FlushOutdatedEntries();
        var existingEntry = await FindCooldownEntry(userId, activityName, cancellationToken);
        if (existingEntry != null)
        {
            return false;
        }

        await AddCooldownInternal(userId, activityName, duration, cancellationToken);
        return true;
    }

    public async Task AddOrUpdateCooldown(Guid userId, string activityName, TimeSpan duration, CancellationToken cancellationToken)
    {
        var existingEntry = await FindCooldownEntry(userId, activityName, cancellationToken);
        if (existingEntry != null)
        {
            var now = DateTime.UtcNow;
            existingEntry.ExpiresAt = now.Add(duration);
            existingEntry.UpdatedAt = now;
            await _dbContext.SaveChangesAsync(cancellationToken);
        }
        else
        {
            await AddCooldownInternal(userId, activityName, duration, cancellationToken);
        }
    }

    private async Task FlushOutdatedEntries()
    {
        var outdatedEntries = await _dbContext
            .UserActivityCooldowns
            .Where(x => x.ExpiresAt < DateTime.UtcNow)
            .ToArrayAsync();

        _dbContext.UserActivityCooldowns.RemoveRange(outdatedEntries);

        await _dbContext.SaveChangesAsync(CancellationToken.None);
    }

    private async Task<UserActivityCooldown> FindCooldownEntry(Guid userId, string activityName, CancellationToken cancellationToken)
    {
        return await _dbContext
            .UserActivityCooldowns
            .FirstOrDefaultAsync(cd => cd.UserId == userId &&
                                       cd.ActivityKey == activityName,
                cancellationToken);
    }

    private async Task AddCooldownInternal(Guid userId, string activityName, TimeSpan duration, CancellationToken cancellationToken)
    {
        var now = DateTime.UtcNow;
        var newEntry = new UserActivityCooldown
        {
            UserId = userId,
            ActivityKey = activityName,
            CreatedAt = now,
            ExpiresAt = now.Add(duration),
        };

        await _dbContext.UserActivityCooldowns.AddAsync(newEntry, cancellationToken);
        await _dbContext.SaveChangesAsync(cancellationToken);
    }
}
