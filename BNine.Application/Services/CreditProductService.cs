﻿namespace BNine.Application.Services
{
    using System;
    using System.Threading.Tasks;
    using Interfaces;
    using Interfaces.Bank.Client;

    public class CreditProductService : ICreditProductService
    {
        private ICurrentUserService CurrentUser
        {
            get;
        }

        private IBNineDbContext DbContext
        {
            get;
        }

        private IBankSavingsTransactionsService BankSavingsTransactionsService
        {
            get;
        }

        private Interfaces.Bank.Administration.IBankSavingAccountsService BankAccountService
        {
            get;
        }

        public CreditProductService(
            ICurrentUserService currentUser,
            IBNineDbContext dbContext,
            IBankSavingsTransactionsService bankSavingsTransactionsService,
            Interfaces.Bank.Administration.IBankSavingAccountsService bankAccountService)
        {
            CurrentUser = currentUser;
            DbContext = dbContext;
            BankSavingsTransactionsService = bankSavingsTransactionsService;
            BankAccountService = bankAccountService;
        }

        public Task<bool> IsCreditAllowed()
        {
            throw new NotImplementedException();
            //var user = DbContext.Users.FirstOrDefault(x => x.Id == CurrentUser.UserId);

            ////replace when finish testing
            //var threeMonthBalanceChanges = await CalculateBalanceChangesForThePeriod(
            //    user.ExternalSavingsAccountId.Value,
            //    DateTime.UtcNow.AddDays(-90),
            //    DateTime.UtcNow);

            ////var threeMonthtransactions = await BankSavingsTransactionsService.GetTransactions(
            ////    user.ExternalSavingsAccountId.Value,
            ////    1,
            ////    1000,
            ////    DateTime.UtcNow.AddDays(-360),
            ////    DateTime.UtcNow.AddDays(-90));
            ////var isThreeMonthReplenishments = threeMonthtransactions.Items.Any(x => x.Direction == TransferDirection.Credit);

            //var twoMonthBalanceChanges = await CalculateBalanceChangesForThePeriod(
            //    user.ExternalSavingsAccountId.Value,
            //    DateTime.UtcNow.AddDays(-60),
            //    DateTime.UtcNow);

            //var twoWeeksBalanceChanges = await CalculateBalanceChangesForThePeriod(
            //    user.ExternalSavingsAccountId.Value,
            //    DateTime.UtcNow.AddDays(-14),
            //    DateTime.UtcNow);

            //if (user.State != "CA" ||
            //    threeMonthBalanceChanges !> 0 ||
            //    twoMonthBalanceChanges !> 0 ||
            //    twoWeeksBalanceChanges !> 0) { return false; }

            //return true;
        }

        public Task<int> CalculateCreditLimit()
        {
            throw new NotImplementedException();
            //var user = DbContext.Users.FirstOrDefault(x => x.Id == CurrentUser.UserId);

            //var balanceChanges = await CalculateBalanceChangesForThePeriod(
            //    user.ExternalSavingsAccountId.Value,
            //    DateTime.UtcNow.AddDays(-14),
            //    DateTime.UtcNow);

            //var limit = (int)(balanceChanges * 0.25);

            //return limit < 500 ? limit : 500;
        }

        private Task<int> CalculateBalanceChangesForThePeriod(int savingsId, DateTime startingDate, DateTime finishDate)
        {
            throw new NotImplementedException();
            ////TODO: calculate using credit transactions amount
            //var transactions = await BankSavingsTransactionsService.GetTransactions(
            //    savingsId,
            //    1,
            //    1000,
            //    startingDate,
            //    finishDate);

            //var info = await BankAccountService.GetSavingAccountInfo(savingsId);

            //if (transactions.Items.Count != 0)
            //{
            //    var earliestTransaction = transactions.Items.Last();
            //    var startingBalance = earliestTransaction.Direction.ToString() == "Credit" ?
            //        earliestTransaction.RunningBalance - earliestTransaction.Amount :
            //        earliestTransaction.RunningBalance + earliestTransaction.Amount;

            //    var balanceChanges = info.AccountBalance - (double)startingBalance;

            //    return (int)balanceChanges;
            //}

            //return 0;
        }
    }
}
