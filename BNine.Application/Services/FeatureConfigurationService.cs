﻿namespace BNine.Application.Services
{
    using System;
    using System.Linq;
    using System.Threading;
    using System.Threading.Tasks;
    using BNine.Application.Exceptions;
    using BNine.Application.Interfaces;
    using BNine.Domain.Entities.Features;
    using BNine.Domain.Entities.Features.Groups;
    using Microsoft.EntityFrameworkCore;

    public class FeatureConfigurationService : IFeatureConfigurationService
    {
        private IBNineDbContext DbContext
        {
            get;
        }

        public FeatureConfigurationService(IBNineDbContext dbContext)
        {
            DbContext = dbContext;
        }

        public async Task<FeatureGroup> GetPersonalFeatureConfigAsync(Guid userId, string featureName, string defaultSettingsValue, CancellationToken cancellationToken)
        {
            var feature = await DbContext.Features.FirstOrDefaultAsync(f => f.Name == featureName);
            if (feature == null)
            {
                throw new NotFoundException(nameof(Feature), featureName);
            }

            var group = await DbContext.Groups
                            .Where(g => g.UserGroups.Any(ug => ug.UserId == userId && g.Kind == GroupKind.Personal))
                            .Include(g => g.FeatureGroups)
                            .FirstOrDefaultAsync(cancellationToken);
            if (group == null)
            {
                var newGroup = new PersonalGroup
                {
                    Name = $"personal_{userId}",
                    Kind = GroupKind.Personal,
                    CreatedAt = DateTime.Now,
                    UpdatedAt = DateTime.Now,
                };

                group = (await DbContext.Groups.AddAsync(newGroup)).Entity;
                group.UserGroups.Add(new UserGroup { UserId = userId });

                await DbContext.SaveChangesAsync(cancellationToken);
            }

            var featureGroup = group.FeatureGroups.FirstOrDefault(fg => fg.FeatureId == feature.Id);
            if (featureGroup == null)
            {
                var now = DateTime.Now;
                featureGroup = new FeatureGroup
                {
                    CreatedAt = now,
                    UpdatedAt = now,
                    GroupId = group.Id,
                    FeatureId = feature.Id,
                    IsEnabled = null,
                    Settings = defaultSettingsValue,
                };

                await DbContext.FeatureGroups.AddAsync(featureGroup);
                await DbContext.SaveChangesAsync(cancellationToken);
            }

            return featureGroup;
        }

        /// <summary>
        /// Adds user to an existing group. If user is already in that group, then no action is taken.
        /// If group doesn't exist then no action is taken.
        /// </summary>
        public async Task AddUserToGroup(Guid userId, string groupName, CancellationToken cancellationToken)
        {
            var group = await DbContext.Groups
                .FirstOrDefaultAsync(x => x.Name == groupName, cancellationToken);

            if (group == null)
            {
                return;
            }

            var existingUserGroup = await DbContext.UserGroups
                .FirstOrDefaultAsync(ug => ug.GroupId == group.Id && ug.UserId == userId,
                    cancellationToken);
            if (existingUserGroup == null)
            {
                var now = DateTime.Now;
                var userGroup = new UserGroup
                {
                    CreatedAt = now,
                    UpdatedAt = now,
                    UserId = userId,
                    GroupId = group.Id,
                };
                await DbContext.UserGroups.AddAsync(userGroup, cancellationToken);
                await DbContext.SaveChangesAsync(cancellationToken);
            }
        }

        /// <summary>
        /// Remove user. If user is not in the group, then no action is taken.
        /// If group doesn't exist then no action is taken.
        /// </summary>
        public async Task RemoveUserFromGroup(Guid userId, string groupName, CancellationToken cancellationToken)
        {
            var group = await DbContext.Groups
                .FirstOrDefaultAsync(x => x.Name == groupName, cancellationToken);

            if (group == null)
            {
                return;
            }

            var existingUserGroup = await DbContext.UserGroups
                .FirstOrDefaultAsync(ug => ug.GroupId == group.Id && ug.UserId == userId,
                    cancellationToken);
            if (existingUserGroup != null)
            {
                DbContext.UserGroups.Remove(existingUserGroup);
                await DbContext.SaveChangesAsync(cancellationToken);
            }
        }
    }
}
