﻿namespace BNine.Application.Services.AdminAPI;

using BNine.Enums;
using Microsoft.AspNetCore.Http;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;

//TODO: Move on the AdminApi level
public class AdminApiCurrentUserService : IAdminApiCurrentUserService
{
    private readonly IHttpContextAccessor _httpContextAccessor;

    public AdminApiCurrentUserService(IHttpContextAccessor httpContextAccessor)
    {
        ArgumentNullException.ThrowIfNull(httpContextAccessor);
        _httpContextAccessor = httpContextAccessor;
    }

    public Guid? UserId
    {
        get
        {
            var stringId = _httpContextAccessor.HttpContext?.User?.FindFirst(ClaimTypes.NameIdentifier);

            if (stringId is null)
            {
                return null;
            }

            return Guid.TryParse(stringId.Value, out var guid) ? guid : null;
        }
    }

    public string UserName
    {
        get
        {
            var userName = _httpContextAccessor.HttpContext?.User?.FindFirst("username");

            return userName?.Value;
        }
    }

    public bool UsesApiKeyAuth
    {
        get
        {
            return _httpContextAccessor.HttpContext?.Request?.Headers.Any(x => x.Key == "X-Api-Key") ?? false;
        }
    }

    /// <summary>
    /// TODO: Remove this garbage from description.
    /// </summary>
    public string UserMbanqToken
    {
        get
        {
            return null;
        }
    }


    /// <summary>
    /// TODO: Remove this garbage from description.
    /// </summary>
    public Guid[] GroupIds
    {
        get
        {
            return new Guid[0];
        }
    }


    /// <summary>
    /// TODO: Remove this garbage from description.
    /// </summary>
    public JwtSecurityToken AccessToken
    {
        get
        {
            throw new NotImplementedException();
        }
    }


    /// <summary>
    /// TODO: Remove this garbage from description.
    /// </summary>
    public MobilePlatform Platform
    {
        get;
    }
}

