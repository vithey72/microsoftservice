﻿namespace BNine.Application.Services.AdminAPI;

using AutoMapper;
using BNine.Application.Aggregates.Banners.Models;
using BNine.Application.Interfaces;
using BNine.Domain.Entities.Banners;
using Microsoft.EntityFrameworkCore;

public class MarketingBannersAdminService : IMarketingBannersAdminService
{
    private readonly IBNineDbContext _dbContext;
    private readonly IMapper _mapper;

    public MarketingBannersAdminService(
        IBNineDbContext dbContext,
        IMapper mapper
    )
    {
        _dbContext = dbContext;
        _mapper = mapper;
    }

    public async Task<List<TViewModel>> GetWalletScreenBanners<TBanner, TViewModel>()
        where TBanner : BaseMarketingBanner
    {
        var banners = await _dbContext.AllMarketingBanners
            .AsQueryable()
            .OfType<TBanner>()
            .OrderBy(bs => bs.Name)
            .ToArrayAsync();

        return _mapper.Map<List<TViewModel>>(banners);
    }

    public async Task<T> CreateBanner<T>(T banner, CancellationToken cancellationToken) where T : InfoPopupBannerInstanceSettings
    {
        var bannerEntity = _mapper.Map<InfoPopupBanner>(banner);
        bannerEntity.CreatedAt = DateTime.UtcNow;
        bannerEntity.UpdatedAt = null;

        var existingBanner = await _dbContext
            .InfoPopupMarketingBanners
            .FirstOrDefaultAsync(b => b.Name == bannerEntity.Name, cancellationToken);

        if (existingBanner != null)
        {
            throw new InvalidOperationException($"The banner with name '{bannerEntity.Name}' already exists.");
        }

        if (banner.GroupIds != null && banner.GroupIds.Any())
        {
            var existingGroupsCount = _dbContext.Groups.Count(x => banner.GroupIds.Contains(x.Id));
            if (existingGroupsCount != banner.GroupIds.Length)
            {
                throw new InvalidOperationException("Some of the groups provided don't exist.");
            }
        }
        var entity = _dbContext.AllMarketingBanners.Add(bannerEntity);
        await _dbContext.SaveChangesAsync(cancellationToken);
        return _mapper.Map<T>(entity.Entity);
    }

    public async Task<T> UpdateBanner<T>(T banner, CancellationToken token) where T : InfoPopupBannerInstanceSettings
    {
        var entity = await _dbContext
            .InfoPopupMarketingBanners
            .FirstOrDefaultAsync(x => x.Id == banner.Id, cancellationToken: token);

        if (entity == null)
        {
            throw new KeyNotFoundException("No banner under such key.");
        }

        var newEntity = _mapper.Map<InfoPopupBanner>(banner);

        UpdateFields(entity, newEntity);
        await _dbContext.SaveChangesAsync(token);

        return _mapper.Map<T>(entity);
    }

    public async Task DeleteBanner<T>(Guid bannerId, CancellationToken cancellationToken) where T : BaseMarketingBanner
    {
        var banner = await _dbContext
            .AllMarketingBanners
            .OfType<T>()
            .FirstOrDefaultAsync(x => x.Id == bannerId, cancellationToken);

        if (banner == null)
        {
            throw new KeyNotFoundException($"Banner of type {typeof(T).Name} with Id {bannerId} was not found.");
        }

        _dbContext.AllMarketingBanners.Remove(banner);
        await _dbContext.SaveChangesAsync(cancellationToken);
    }

    private void UpdateFields(InfoPopupBanner target, InfoPopupBanner source)
    {
        target.Name = source.Name;
        target.Title = source.Title;
        target.Subtitle = source.Subtitle;
        target.ButtonText = source.ButtonText;
        target.ButtonDeeplink = source.ButtonDeeplink;
        target.PictureUrl = source.PictureUrl;
        target.IsEnabled = source.IsEnabled;
        target.CannotBeClosed = source.CannotBeClosed;
        target.ExternalLink = source.ExternalLink;
        target.Groups = source.Groups;
        target.Priority = source.Priority;
        target.MinUserRegistrationDate = source.MinUserRegistrationDate;
        target.ShowFrom = source.ShowFrom;
        target.ShowTil = source.ShowTil;
        target.ShowCooldown = source.ShowCooldown;
        target.ShowCooldownProgressive = source.ShowCooldownProgressive;
        target.UpdatedAt = DateTime.UtcNow;
    }
}
