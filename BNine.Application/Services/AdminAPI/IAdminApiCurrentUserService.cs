﻿namespace BNine.Application.Services.AdminAPI;

using BNine.Application.Interfaces;

public interface IAdminApiCurrentUserService : ICurrentUserService
{
    public string? UserName
    {
        get;
    }

    public bool UsesApiKeyAuth
    {
        get;
    }
}

