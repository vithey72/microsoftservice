﻿namespace BNine.Application.Services;

using BNine.Application.Interfaces;

public record ClientAccountSettings(
    int VoluntaryClosureDelayInDays,
    int BlockClosureOffsetFromActivationInDays,
    TimeSpan PendingClosureCheckGap,
    int TruvDistributionShareInPercents
    ) : IClientAccountSettings;
