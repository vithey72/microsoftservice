﻿namespace BNine.Application.Services.ClientDocument;

using Aggregates.Users.Models;
using Enums;
using Extensions;
using Interfaces;
using Microsoft.EntityFrameworkCore;

public class ClientDocumentsOriginalsPersistenceService : IClientDocumentsOriginalsPersistenceService
{
    private readonly IClientDocumentOriginalsStorageService _clientDocumentOriginalsStorageService;
    private readonly IBNineDbContext _bNineDbContext;

    public ClientDocumentsOriginalsPersistenceService(
        IClientDocumentOriginalsStorageService clientDocumentOriginalsStorageService,
        IBNineDbContext bNineDbContext)
    {
        _clientDocumentOriginalsStorageService = clientDocumentOriginalsStorageService;
        _bNineDbContext = bNineDbContext;
    }

    public async Task<List<ClientDocument>> GetClientDocuments(Guid clientId, CancellationToken cancellationToken = default)
    {
        var documents = await _clientDocumentOriginalsStorageService.GetClientDocumentsMetadata(clientId, cancellationToken);

        var clientDocuments = documents.Select(documentMetadata =>
        {
            var fileNameWithoutExtension = System.IO.Path.GetFileNameWithoutExtension(documentMetadata.Key);
            var documentMetadataArray = fileNameWithoutExtension.Split("_");
            if (documentMetadataArray?.Length != 3)
            {
                throw new Exception("Incorrect file name format");
            }

            var clientId = Guid.Parse(documentMetadataArray[0]);
            var documentTypeKey = documentMetadataArray[1];
            var documentSide = documentMetadataArray[2];

            return new ClientDocument()
            {
                FileName = documentMetadata.Key,
                DocumentSide = EnumExtensions.ParseByEnumMemberAttribute<DocumentSide>(documentSide),
                DocumentTypeKey = documentTypeKey,
                CreatedOn = documentMetadata.Value
            };
        }).ToList();

        return clientDocuments;
    }

    public async Task<string> DownloadClientDocumentFile(string fileName, CancellationToken cancellationToken = default)
    {
        var file = await _clientDocumentOriginalsStorageService.DownloadClientDocumentFile(fileName, cancellationToken);

        return Convert.ToBase64String(file);
    }

    public async Task UploadClientDocumentFromAlloy(Guid clientId, string documentTypeKey, string fileName,
        string contentType, byte[] fileData, string fileExtension, CancellationToken cancellationToken = default)
    {
        var documentSide = DocumentSide.FrontSide;
        if (fileName.Contains("back"))
        {
            documentSide = DocumentSide.BackSide;
        }

        await _clientDocumentOriginalsStorageService.UploadClientDocument(clientId, documentTypeKey, documentSide,
            fileData,
            contentType, fileExtension, cancellationToken);
    }

    public async Task UploadClientDocumentFromSocure(Guid clientId, string documentTypeKey, string fileName,
        string contentType, DocumentSide documentSide, byte[] fileData, string fileExtension, CancellationToken cancellationToken = default)
    {
        if (fileName.Contains("selfPortrait"))
        {
            documentTypeKey = "selfie";
        }

        await _clientDocumentOriginalsStorageService.UploadClientDocument(clientId, documentTypeKey, documentSide, fileData,
            contentType, fileExtension, cancellationToken);
    }

    public async Task MarkClientDocumentAsUploaded(Guid clientId, CancellationToken cancellationToken)
    {
        var user = await _bNineDbContext.Users
            .Include(x => x.Document)
            .FirstOrDefaultAsync(x => x.Id == clientId);

        user.Document.IsUploaded = true;

        await _bNineDbContext.SaveChangesAsync(cancellationToken);
    }
}
