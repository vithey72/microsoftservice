﻿namespace BNine.Application.Services;

using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Domain.Entities.PaidUserServices;
using Domain.Entities.User;
using Interfaces;
using Interfaces.Bank.Administration;
using Microsoft.EntityFrameworkCore;

public class PaidUserServicesService : IPaidUserServicesService
{
    private readonly IBNineDbContext _dbContext;
    private readonly ICurrentUserService _currentUserService;
    private readonly IBankChargesService _chargesService;
    private readonly IDateTimeProviderService _dateTimeProviderService;

    public PaidUserServicesService(
        IBNineDbContext dbContext,
        ICurrentUserService currentUserService,
        IBankChargesService chargesService,
        IDateTimeProviderService dateTimeProviderService
    )
    {
        _dbContext = dbContext;
        _currentUserService = currentUserService;
        _chargesService = chargesService;
        _dateTimeProviderService = dateTimeProviderService;
    }

    public async Task<UsedPaidUserService> EnableService(Guid paidServiceId, string idempotencyKey = null, Guid? userId = null)
    {
        var paidServiceEntity = await FetchPaidServiceEntity(paidServiceId);
        if (paidServiceEntity == null)
        {
            throw new KeyNotFoundException("Entity is missing from DB.");
        }

        userId ??= _currentUserService.UserId!.Value;

        var user = await _dbContext
            .Users
            .Include(u => u.CurrentAccount)
            .FirstOrDefaultAsync(u => u.Id == userId);

        if (user?.CurrentAccount == null)
        {
            throw new KeyNotFoundException("User is missing from DB.");
        }

        var hasActiveChargeOfSameType = await UserHasActiveCharge(user, paidServiceEntity);
        if (hasActiveChargeOfSameType)
        {
            throw new InvalidOperationException("This charge is already active.");
        }

        var response = await _chargesService.CreateAndCollectAccountCharge(
            user.CurrentAccount.ExternalId, paidServiceEntity.ChargeTypeId, _dateTimeProviderService.Now().AddDays(paidServiceEntity.DurationInDays),
            idempotencyKey);

        if (response == null)
        {
            return null;
        }

        var newUsedServiceEntity = new UsedPaidUserService
        {
            UserId = user.Id,
            ServiceId = paidServiceEntity.Id,
            ValidTo = _dateTimeProviderService.Now().AddDays(paidServiceEntity.DurationInDays),
            IsOverdue = response.TotalDeferredChargeAmount != decimal.Zero,
            ChargeExternalId = response.Id,
        };

        var entity = await _dbContext.UsedPaidUserServices.AddAsync(newUsedServiceEntity);
        await _dbContext.SaveChangesAsync(CancellationToken.None);

        return entity.Entity;
    }

    public async Task<decimal> GetServiceCost(Guid paidServiceId)
    {
        var service = await FetchPaidServiceEntity(paidServiceId);
        if (service == null)
        {
            throw new KeyNotFoundException($"Paid service with Id = {paidServiceId} does not exist in DB");
        }
        var chargeDetails = await _chargesService.GetChargeDetails(service.ChargeTypeId);
        if (chargeDetails == null)
        {
            throw new KeyNotFoundException($"Charge type with ExternalId = {service.ChargeTypeId} does not exist in MBanq");
        }
        return chargeDetails.Amount;
    }

    public async Task<bool> ServiceIsAlreadyPurchased(Guid paidServiceId, Guid? userId = null)
    {

        userId ??= _currentUserService.UserId!.Value;

        var user = await _dbContext
            .Users
            .FirstOrDefaultAsync(u => u.Id == userId);

        var paidServiceEntity = await FetchPaidServiceEntity(paidServiceId);

        return await UserHasActiveCharge(user, paidServiceEntity);
    }

    public async Task<UsedPaidUserService> GetEnabledService(Guid paidServiceId, Guid? userId = null)
    {
        userId ??= _currentUserService.UserId!.Value;

        var paidService = await FetchPaidServiceEntity(paidServiceId);

        var enabledPaidService = await GetActiveServices(userId.Value, paidService).FirstOrDefaultAsync();

        if (enabledPaidService is null)
        {
            throw new InvalidOperationException($"User doesn't have enabled {paidServiceId} service.");
        }

        return enabledPaidService;
    }

    public async Task MarkServiceAsUsed(Guid paidServiceId, Guid? userId = null)
    {
        userId ??= _currentUserService.UserId!.Value;

        var usedUserService = await _dbContext.UsedPaidUserServices
            .OrderByDescending(x => x.CreatedAt)
            .FirstOrDefaultAsync(x => x.ServiceId == paidServiceId && x.UserId == userId);

        if (usedUserService is null)
        {
            //  TODO    Fix this method clients to ensure instance of this used service exists
            return;
        }

        usedUserService.ValidTo = _dateTimeProviderService.Now();
        await _dbContext.SaveChangesAsync(CancellationToken.None);
    }

    private async Task<PaidUserService> FetchPaidServiceEntity(Guid paidServiceId)
    {
        return await _dbContext
            .PaidUserServices
            .FirstOrDefaultAsync(x => x.Id == paidServiceId);
    }

    private async Task<bool> UserHasActiveCharge(User user, PaidUserService paidServiceEntity)
    {
        return await GetActiveServices(user.Id, paidServiceEntity)
            .AnyAsync();
    }

    private IQueryable<UsedPaidUserService> GetActiveServices(Guid userId, PaidUserService paidServiceEntity)
    {
        return _dbContext
            .UsedPaidUserServices
            .Where(x =>
                x.UserId == userId &&
                x.ServiceId == paidServiceEntity.Id &&
                x.ValidTo > _dateTimeProviderService.Now());
    }
}
