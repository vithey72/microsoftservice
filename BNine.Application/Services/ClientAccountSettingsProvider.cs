﻿namespace BNine.Application.Services;

using System.Threading;
using System.Threading.Tasks;
using BNine.Application.Interfaces;
using Microsoft.EntityFrameworkCore;

public class ClientAccountSettingsProvider : IClientAccountSettingsProvider
{
    private readonly IBNineDbContext _dbContext;

    public ClientAccountSettingsProvider(
        IBNineDbContext dbContext
        )
    {
        _dbContext = dbContext;
    }

    public async Task<IClientAccountSettings> GetClientAccountSettings(CancellationToken cancellationToken)
    {
        var settingsEntity = await _dbContext.CommonAccountSettings.FirstAsync(cancellationToken);

        return new ClientAccountSettings(
            settingsEntity.VoluntaryClosureDelayInDays,
            settingsEntity.BlockClosureOffsetFromActivationInDays,
            TimeSpan.FromHours(settingsEntity.PendingClosureCheckGapInHours),
            settingsEntity.TruvDistributionShareInPercents);
    }
}
