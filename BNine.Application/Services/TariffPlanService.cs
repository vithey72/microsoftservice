﻿namespace BNine.Application.Services
{
    using System.Threading;
    using BNine.Application.Aggregates.TariffPlans.Models;
    using BNine.Application.Exceptions;
    using BNine.Application.Exceptions.ApiResponse;
    using BNine.Application.Interfaces;
    using BNine.Application.Interfaces.Bank.Administration;
    using BNine.Constants;
    using BNine.Domain.Entities.TariffPlan;
    using BNine.Domain.Entities.User.UserDetails.Settings;
    using BNine.Enums.TariffPlan;
    using Domain.Entities.User;
    using Enums;
    using Helpers;
    using Microsoft.EntityFrameworkCore;

    public class TariffPlanService : ITariffPlanService
    {
        private readonly IBNineDbContext _dbContext;
        private readonly IBankClientsService _bankClientsService;
        private readonly IBankChargesService _bankChargesService;
        private readonly IBankSavingAccountsService _savingAccountsService;
        private readonly IPartnerProviderService _partnerProvider;
        private readonly IBankClassificationsService _classificationsService;
        private readonly ICurrentUserService _currentUser;

        public TariffPlanService(IBNineDbContext dbContext,
            IBankClientsService bankClientsService,
            IBankChargesService bankChargesService,
            IBankSavingAccountsService savingAccountsService,
            IPartnerProviderService partnerProvider,
            IBankClassificationsService classificationsService,
            ICurrentUserService currentUser
            )
        {
            _bankChargesService = bankChargesService;
            _savingAccountsService = savingAccountsService;
            _dbContext = dbContext;
            _bankClientsService = bankClientsService;
            _partnerProvider = partnerProvider;
            _classificationsService = classificationsService;
            _currentUser = currentUser;
        }

        public async Task SwitchTariffPlan(Guid userId, Guid newTariffPlanId, bool skipBalanceCheck = false)
        {
            var user = await _dbContext.Users
                .Include(x => x.CurrentAccount)
                .Include(u => u.UserTariffPlans)
                .ThenInclude(t => t.TariffPlan)
                .Where(x => x.Id == userId)
                .FirstOrDefaultAsync();
            if (user == null)
            {
                throw new NotFoundException(nameof(User), userId);
            }
            var newTariff = await FetchLegitimateTariffPlan(userId, newTariffPlanId);

            var currentAccount = await _savingAccountsService.GetSavingAccountInfo(user.CurrentAccount.ExternalId);
            if (!skipBalanceCheck && newTariff.TotalPrice > currentAccount.AvailableBalance)
            {
                throw new ApiResponseException(ApiResponseErrorCodes.InsufficientFunds,
                    $"To switch to the {newTariff.Name} \nyou need to have {CurrencyFormattingHelper.AsRegular(newTariff.TotalPrice)} \nin your account",
                    "Insufficient funds");
            }

            var resourceId = await _bankClientsService.UpdateClientClassification(user.ExternalClientId.Value, newTariff.ExternalClassificationId);
            if (resourceId == -1)
            {
                throw new InvalidOperationException(
                    $"Failed to switch the classification to {newTariff.ExternalClassificationId}");
            }

            await _dbContext.SaveChangesAsync(CancellationToken.None);
        }

        public async Task ActivateStarterTariffPlan(Guid userId, CancellationToken cancellationToken)
        {
            var user = await _dbContext.Users
                .Include(x => x.CurrentAccount)
                .Where(x => x.Id == userId)
                .FirstOrDefaultAsync(cancellationToken);
            if (user == null)
            {
                throw new NotFoundException(nameof(User), userId);
            }

            if (user.CurrentAccount == null)
            {
                return;
            }

            var basicPlan = await _dbContext
                .TariffPlans
                .FirstAsync(t => t.Id == PaidUserServices.Basic1Month, cancellationToken);

            if (_partnerProvider.GetPartner() is PartnerApp.Default or PartnerApp.MBanqApp)
            {
                await SwitchTariffPlanDelayed(user.Id, basicPlan.Id, basicPlan.MonthsDuration);
            }
            else
            {
                await SwitchTariffPlan(user.Id, basicPlan.Id, skipBalanceCheck: true);
            }
        }

        public async Task UpdateUserTariffPurchase(decimal deferredAmount = 0)
        {
            var userSetting = await _dbContext.Users
             .Include(x => x.Settings)
             .FirstOrDefaultAsync(x => x.Id == _currentUser.UserId);


            var user = new User
            {
                Settings = new UserSettings
                {
                    InitialTariffIsPurchased = userSetting is not null
                                                && deferredAmount == 0
                                                && _partnerProvider.GetPartner() is PartnerApp.Qorbis
                                                && userSetting.Settings.InitialTariffIsPurchased == false ? true : null
                }
            };


            _dbContext.Users.Update(user);
            await _dbContext.SaveChangesAsync(CancellationToken.None);
        }
        public async Task UpdateUserTariffPlan(long externalClientId, decimal deferredAmount = decimal.Zero)
        {
            var user = await _dbContext.Users
              .Include(x => x.CurrentAccount)
              .Include(x => x.UserTariffPlans)
              .ThenInclude(x => x.TariffPlan)
              .FirstOrDefaultAsync(x => x.ExternalClientId == externalClientId);
            if (user == null)
            {
                throw new NotFoundException(nameof(User), externalClientId);
            }


            var currentUserClassificationId = await _bankClientsService.GetClientClassificationId(user.ExternalClientId.Value);

            var currentUserTariff = user.UserTariffPlans.FirstOrDefault(t => t.Status == TariffPlanStatus.Active || t.Status == TariffPlanStatus.Overdue);
            var newUserTariff = user.UserTariffPlans.FirstOrDefault(t => t.TariffPlan.ExternalClassificationId == currentUserClassificationId);

            var monthlyCharge = await _bankChargesService.GetActiveTariffCharge(user.CurrentAccount.ExternalId);
            if (monthlyCharge == null)
            {
                throw new NotFoundException($"{monthlyCharge}", newUserTariff.TariffPlan.ExternalChargeId);
            }

            if (currentUserTariff != newUserTariff || newUserTariff == null)
            {
                if (newUserTariff == null)
                {
                    var newTariffPlan = _dbContext.TariffPlans.FirstOrDefault(t => t.ExternalClassificationId == currentUserClassificationId);
                    newUserTariff = new UserTariffPlan { TariffPlanId = newTariffPlan.Id, UserId = user.Id, TariffPlan = newTariffPlan };
                    user.UserTariffPlans.Add(newUserTariff);
                }

                newUserTariff.Status = monthlyCharge.TotalDeferredChargeAmount == Decimal.Zero ? TariffPlanStatus.Active : TariffPlanStatus.Overdue;
                foreach (var ut in user.UserTariffPlans)
                {
                    if (ut.Id != newUserTariff.Id && ut.Status != TariffPlanStatus.Upcoming)
                    {
                        ut.Status = TariffPlanStatus.Inactive;
                    }
                }
            }
            else
            {
                if (deferredAmount != Decimal.Zero || monthlyCharge.TotalDeferredChargeAmount != Decimal.Zero)
                {
                    newUserTariff.Status = TariffPlanStatus.Overdue;
                }
            }

            newUserTariff.NextPaymentDate = monthlyCharge.DueDate ?? DateTime.Now.AddMonths(1);

            await _dbContext.SaveChangesAsync(CancellationToken.None);
        }

        public async Task<TariffPlan> GetTariffByClassificationId(int externalId, CancellationToken token)
        {
            return await _dbContext
                .TariffPlans
                .AsNoTracking()
                .FirstOrDefaultAsync(
                    tp => tp.ExternalClassificationId == externalId,
                    token);
        }

        public async Task<TariffPlan> GetCurrentTariff(Guid userId, CancellationToken token)
        {
            var tariffs = await GetCurrentAndUpcomingTariffs(userId, token);
            return tariffs.Current;
        }

        public async Task<IUserTariffModel> GetCurrentTariffModel(Guid userId, CancellationToken token)
        {
            var activeTariff = await GetCurrentTariff(userId, token);
            if (activeTariff == null)
            {
                return null;
            }

            return new UserTariffModel(activeTariff, _partnerProvider);
        }


        public async Task<(TariffPlan Current, DateTime? CyclePaymentDate, TariffPlan Upcoming, DateTime? SwitchDate)> GetCurrentAndUpcomingTariffs(
            Guid userId, CancellationToken token)
        {
            var clientId = (await _dbContext.Users
                    .SingleAsync(x => x.Id == userId, token))
                .ExternalClientId;

            if (clientId == null)
            {
                return (null, null, null, null);
            }

            var classifications = await _classificationsService.GetUserClassifications(clientId.Value, token);

            var current = await GetTariffByClassificationId(classifications.Current.ExternalId, token);
            var upcoming = classifications.Upcoming == null
                ? null
                : await GetTariffByClassificationId(classifications.Upcoming.Value.ExternalId, token);

            return NeedToCollapseFreeTariff(current, upcoming)
                ? (upcoming, classifications.Upcoming?.PaymentDate, null, null)
                : (current, classifications.Current.PaymentDate, upcoming, classifications.Upcoming?.PaymentDate);

            static bool NeedToCollapseFreeTariff(TariffPlan current, TariffPlan upcoming)
            {
                if (current == null)
                {
                    return false;
                }
                return current.IsDisabled && upcoming != null && upcoming.Type == TariffPlanFamily.Advance;
            }
        }

        public async Task<TariffPlanFamily> GetCurrentTariffPlanFamily(Guid userId, CancellationToken token)
        {
            var tariff = await GetCurrentTariff(userId, token);
            return tariff?.Type ?? TariffPlanFamily.Advance;
        }

        private async Task<TariffPlan> FetchLegitimateTariffPlan(Guid userId, Guid newTariffPlanId)
        {
            var newTariff = await _dbContext.TariffPlans.FirstOrDefaultAsync(tp => tp.Id == newTariffPlanId);
            if (newTariff == null)
            {
                throw new NotFoundException(nameof(TariffPlan), userId);
            }

            if (newTariff.IsDisabled)
            {
                throw new InvalidOperationException($"Cannot switch to a disabled tariff {newTariffPlanId}.");
            }

            return newTariff;
        }

        private async Task SwitchTariffPlanDelayed(Guid userId, Guid newTariffPlanId, int monthsDelay)
        {
            var user = await _dbContext.Users
                .Include(x => x.CurrentAccount)
                .Include(u => u.UserTariffPlans)
                .ThenInclude(t => t.TariffPlan)
                .Where(x => x.Id == userId)
                .FirstOrDefaultAsync();
            if (user == null)
            {
                throw new NotFoundException(nameof(User), userId);
            }

            var newTariff = await FetchLegitimateTariffPlan(userId, newTariffPlanId);

            var newUserTariff = user.UserTariffPlans.FirstOrDefault(t => t.TariffPlanId == newTariffPlanId);
            var nextPaymentDate = DateTime.Now.AddMonths(monthsDelay);
            if (newUserTariff == null)
            {
                newUserTariff = new UserTariffPlan
                {
                    TariffPlanId = newTariff.Id,
                    UserId = user.Id,
                    Status = TariffPlanStatus.Upcoming,
                    NextPaymentDate = nextPaymentDate,
                };
                user.UserTariffPlans.Add(newUserTariff);
            }

            var resourceId = await _bankClientsService.UpdateClientClassification(
                user.ExternalClientId!.Value,
                newTariff.ExternalClassificationId,
                nextPaymentDate
            );
            if (resourceId == -1)
            {
                throw new InvalidOperationException(
                    $"Failed to switch the classification to {newTariff.ExternalClassificationId}");
            }

            await _dbContext.SaveChangesAsync(CancellationToken.None);
        }

    }
}
