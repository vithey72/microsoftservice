﻿namespace BNine.Application.Helpers;

using BNine.Common;
using System.Text.RegularExpressions;
using Constants;
using Enums.Transfers;

public static class TransfersMappingHelper
{
    private const string IncomingTransactionColor = "009C3D";
    private const string OutgoingTransactionColor = "14273A";

    // removes irrelevant details
    public static string ShortenMerchant(string transactionNote) => transactionNote?.Length >= 23 ? transactionNote[..23].TrimEnd() : transactionNote;

    public static string InferReturnedChargeName(string reference)
    {
        var dummy = "Reversed Fee";
        if (string.IsNullOrEmpty(reference))
        {
            return dummy;
        }

        var separatorIndex = reference.IndexOf(" with Transaction", StringComparison.Ordinal);
        if (separatorIndex == -1)
        {
            return dummy;
        }

        var chargeInReference = reference.Substring(0, separatorIndex);

        // mostly same on every environment
        var readableName = chargeInReference switch
        {
            "Physical card embossing fee" or "Physical card replacement fee" => "Physical Card Delivery Fee",
            "Premium support fee" => "Premium Support Fee",
            "Monthly Membership Fee Default" or "Basic_Charge" or "Monthly Membership Default OT" => "Monthly Card and Account Fee",
            "External Card Fee" => "Card Transfer Fee",
            "MCC Fee" => "External Wallet Fee",
            _ when chargeInReference.Contains("Advance Boost fee") => "Advance Boost Fee",
            _ when chargeInReference.Contains("After Advance Tip") => "Tip",
            _ when chargeInReference.StartsWith("Advance unfreeze fee") => "Advance Unfreeze Fee",
            _ => chargeInReference,
        };

        if (chargeInReference.StartsWith("Monthly Membership Fee"))
        {
            return "Monthly Fee Reimbursement";
        }

        return "Returned " + readableName;
    }

    public static string GetTransactionHistoryAmountStringColor(TransferDirectionSimplified? amountDirection)
    {
        return amountDirection == TransferDirectionSimplified.Incoming
            ? IncomingTransactionColor
            : OutgoingTransactionColor;
    }

    /// <summary>
    /// We rely heavily on following the charges naming notation here.
    /// If there were a better way, I would implement it that way.
    /// </summary>
    public static string GetMonthlyFeeTextWithPeriod(string reference, bool isDetailsScreenFormat, string periodicFeeName, int monthsDuration)
    {
        if (reference == null)
        {
            return null;
        }

        try
        {
            var match = Regex.Match(reference, DateHelper.DateDDMMMYYYYRegex);
            if (!match.Success || !match.Groups.ContainsKey("date"))
            {
                return null;
            }

            var parseSuccess = DateTime.TryParse(match.Groups["date"].Value, out var date);
            if (!parseSuccess)
            {
                return null;
            }

            var monthsMatch = Regex.Match(reference, "^.*(?'duration'[0-9]{1,2}) (month).*$");
            if (monthsMatch.Success && monthsMatch.Groups.ContainsKey("duration"))
            {
                if (int.TryParse(monthsMatch.Groups["duration"].Value, out var monthsParsed))
                {
                    monthsDuration = monthsParsed;
                }
            }

            return periodicFeeName
                   + (isDetailsScreenFormat ? "\n" : " ")
                   + "for the period"
                   + (!isDetailsScreenFormat ? "\n" : " ")
                   + date.ToString(DateFormat.MonthFirst)
                   + " — "
                   + date.AddMonths(monthsDuration).ToString(DateFormat.MonthFirst);
        }
        catch
        {
            return null;
        }
    }

    public static bool TryGetLoanAccountExternalId(string advanceRepaymentTransferReference, out int loanId)
    {
        loanId = 0;

        //example - "Loan Repayment - Loan Account No (000009443)"
        const string loanIdGroupName = "loanid";
        var match = Regex.Match(advanceRepaymentTransferReference, @"\bLoan Account No\s*\((?'loanid'[0-9]+)\)");
        if (!match.Success || !match.Groups.ContainsKey(loanIdGroupName))
        {
            return false;
        }

        return int.TryParse(match.Groups[loanIdGroupName].Value, out loanId);
    }
}
