﻿namespace BNine.Application.Extensions;

using Enums.Transfers;
using Enums.Transfers.Controller;

public static class TransferDisplayTypeExtensions
{
    public static BankTransferIconOption InferIconOption(this TransferDisplayType displayType)
    {
        return (displayType) switch
        {
            TransferDisplayType.ApprovedCardAuth => BankTransferIconOption.DefaultCardOk,
            TransferDisplayType.Declined or TransferDisplayType.Returned or
                TransferDisplayType.PushToExternalCard or TransferDisplayType.PullFromExternalCard => BankTransferIconOption.CardWarning,
            TransferDisplayType.Atm => BankTransferIconOption.Atm,
            TransferDisplayType.MonthlyB9Fee or Enums.Transfers.TransferDisplayType.AnnualFee or TransferDisplayType.VipSupportFee or TransferDisplayType.AdvanceUnfreezeFee
                or TransferDisplayType.PhysicalCardDeliveryFee or TransferDisplayType.AdvanceBoostFee
                or TransferDisplayType.CreditScoreFee => BankTransferIconOption.Membership,
            TransferDisplayType.AtmFee or TransferDisplayType.ForeignTransactionFee or
                TransferDisplayType.ExternalWalletFee or TransferDisplayType.DefaultFee or
                TransferDisplayType.CardTransferFee or TransferDisplayType.ReturnedPaycharge => BankTransferIconOption.Fee,
            TransferDisplayType.AdvanceRepayment or TransferDisplayType.AdvanceIssued => BankTransferIconOption.Advance,
            TransferDisplayType.AchReceived or TransferDisplayType.AchSent or TransferDisplayType.CheckCashing or TransferDisplayType.Rewards => BankTransferIconOption.Bank,
            TransferDisplayType.ReceivedFromB9Client or TransferDisplayType.SentToB9Client => BankTransferIconOption.Person,
            TransferDisplayType.B9Bonus or TransferDisplayType.B9Cashback or TransferDisplayType.AfterAdvanceTip => BankTransferIconOption.MoneyBag,
            _ => BankTransferIconOption.CardWarning,
        };
    }
}
