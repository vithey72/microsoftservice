﻿namespace BNine.Application.Services;

using Aggregates.Cards.Models;
using Common;
using Domain.Entities;
using Enums.Transfers.Controller;
using Exceptions;
using Helpers;
using Interfaces;
using Interfaces.Bank.Client;
using Interfaces.TransactionHistory;
using Microsoft.EntityFrameworkCore;
using Models.MBanq;
using Models.Transfer;

public class TransfersMappingService : ITransfersMappingService
{
    private readonly ITransferIconsAndNamesProviderService _iconsAndNamesProvider;
    private readonly IBankSavingAccountsService _accountsService;
    private readonly IBankInternalCardsService _cardsService;
    private readonly IBNineDbContext _bNineDbContext;
    private readonly IPartnerProviderService _partnerProvider;

    public TransfersMappingService(
        ITransferIconsAndNamesProviderService iconsAndNamesProvider,
        IBankSavingAccountsService accountsService,
        IBankInternalCardsService cardsService,
        IBNineDbContext bNineDbContext,
        IPartnerProviderService partnerProvider
        )
    {
        _iconsAndNamesProvider = iconsAndNamesProvider;
        _accountsService = accountsService;
        _cardsService = cardsService;
        _bNineDbContext = bNineDbContext;
        _partnerProvider = partnerProvider;
    }

    public async Task<string> InferIconUrl(BankTransferIconOption transferIconOption, int? mccType, string counterparty = null)
    {
        string pictureUrl = null;
        if (transferIconOption == BankTransferIconOption.Person)
        {
            var externalId = counterparty?.Split(':').LastOrDefault();

            var user = await _bNineDbContext.Users.Include(x => x.CurrentAccount)
                .FirstOrDefaultAsync(x => x.CurrentAccount.ExternalId.ToString() == externalId);

            pictureUrl = user?.ProfilePhotoUrl;
        }

        return pictureUrl
               ?? _iconsAndNamesProvider.FindMccIconUrl(mccType)
               ?? _iconsAndNamesProvider.FindIconUrl(transferIconOption);
    }

    public (string Account, string Routing) ExtractAchRecipientNumbers(SavingsTransactionItem savingsTransaction)
    {
        var numbers = savingsTransaction.Counterparty?.Split('/');
        var recipientAccountNumber = numbers?.LastOrDefault();
        var recipientRoutingNumber = numbers?.ElementAtOrDefault(2);
        return (recipientAccountNumber, recipientRoutingNumber);
    }

    public async Task<(string FullName, string Phone)> ExtractB9ClientPhoneAndName(string counterpartyIdentifer)
    {
        var externalId = counterpartyIdentifer?.Split(':').LastOrDefault();
        if (externalId == null)
        {
            return (null, null);
        }
        var userDetails = await _bNineDbContext.Users
            .Include(x => x.CurrentAccount)
            .Where(x => x.CurrentAccount.ExternalId.ToString() == externalId)
            .Select(x => new { x.Phone, x.FirstName, x.LastName })
            .FirstOrDefaultAsync();
        if (userDetails == null)
        {
            return (null, null);
        }
        return (userDetails.FirstName + " " + userDetails.LastName, userDetails.Phone);
    }

    public MerchantDescriptionModel InferMerchant(string reference)
    {
        return _iconsAndNamesProvider.FindMerchant(reference);
    }

    public async Task<(string card, string account)> GetAccountAndCardNumberTexts(
        int accountExternalId, DebitCard debitCard, string mbanqToken, string mbanqIp)
    {
        var acc = await _accountsService.GetSavingAccountInfo(accountExternalId, mbanqToken, mbanqIp);
        CardInfoModel card = null;
        if (debitCard != null)
        {
            card = await _cardsService.GetCardInfo(mbanqToken, mbanqIp, debitCard.ExternalId);
        }

        var cardLastDigits = SecretNumberHelper.GetLastFourDigits(card?.PrimaryAccountNumber);
        var accountLastDigitsText = "Account *" + SecretNumberHelper.GetLastFourDigits(acc.AccountNumber);
        var cardLastDigitsText = cardLastDigits != null ? $"{StringProvider.GetPartnerName(_partnerProvider)} Card *" + cardLastDigits : accountLastDigitsText;

        return (cardLastDigitsText, accountLastDigitsText);
    }

    public async Task<string> GetAccountLastDigits(Guid userId, string mbanqToken, string mbanqIp)
    {
        var user = await _bNineDbContext.Users
            .Include(x => x.CurrentAccount)
            .FirstOrDefaultAsync(x => x.Id == userId);

        if (user?.CurrentAccount is null)
        {
            throw new NotFoundException("Account not found");
        }

        var account = await _accountsService.GetSavingAccountInfo(user.CurrentAccount.ExternalId, mbanqToken, mbanqIp);

        var accountLastDigits = SecretNumberHelper.GetLastFourDigits(account.AccountNumber);
        return accountLastDigits;
    }

}
