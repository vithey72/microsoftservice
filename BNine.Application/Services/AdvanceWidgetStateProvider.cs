﻿namespace BNine.Application.Services;

using BNine.Application.Interfaces;
using BNine.Domain.Entities.User;
using BNine.Enums.Advance;

public class AdvanceWidgetStateProvider : IAdvanceWidgetStateProvider
{
    public const decimal AvailableLimitThresholdToAvailableAdvance = 10m;

    private readonly ILoanAccountProvider _loanAccountProvider;

    public AdvanceWidgetStateProvider(
        ILoanAccountProvider loanAccountProvider
        )
    {
        _loanAccountProvider = loanAccountProvider;
    }

    public async Task<AdvanceWidgetState> GetWidgetState(User user)
    {
        var advanceStats = user.AdvanceStats ?? new AdvanceStats();
        var externalLoanAccount = await _loanAccountProvider.TryGetActiveLoanAccount(user);

        if (externalLoanAccount != null)
        {
            if (externalLoanAccount.MaturityDate < DateTime.UtcNow.Date)
            {
                return AdvanceWidgetState.RepaymentOverdue;
            }

            return AdvanceWidgetState.AdvanceDisbursed;
        }

        if (advanceStats.DelayedBoostSettings is { ReadyToDisburse: false } &&
           advanceStats.DelayedBoostSettings.ActivationDate > DateTime.Now)
        {
            return AdvanceWidgetState.DelayedBoost;
        }

        if (advanceStats.AvailableLimit > AvailableLimitThresholdToAvailableAdvance)
        {
            return advanceStats.HasBoostLimit
                ? AdvanceWidgetState.BoostAvailable
                : AdvanceWidgetState.AdvanceAvailable;
        }

        if (advanceStats.IsEligibleForExtension())
        {
            return AdvanceWidgetState.ExtensionAvailable;
        }

        return AdvanceWidgetState.NoAdvance;
    }
}
