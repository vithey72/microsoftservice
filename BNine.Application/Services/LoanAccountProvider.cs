﻿namespace BNine.Application.Services;

using System.Threading.Tasks;
using BNine.Application.Interfaces;
using BNine.Application.Interfaces.Bank.Administration;
using BNine.Application.Models.MBanq;
using BNine.Domain.Entities.User;
using BNine.Enums;

public class LoanAccountProvider : ILoanAccountProvider
{
    private readonly IBankLoanAccountsService _loanAccountsService;

    public LoanAccountProvider(
        IBankLoanAccountsService loanAccountsService
        )
    {
        _loanAccountsService = loanAccountsService;
    }

    public async Task<LoanSummary> TryGetActiveLoanAccount(User user)
    {
        var activeLoan = user
            .LoanAccounts
            .FirstOrDefault(x => x.Status == LoanAccountStatus.Active);

        if (activeLoan is null)
        {
            return null;
        }

        return await _loanAccountsService.GetLoan(activeLoan.ExternalId);
    }
}
