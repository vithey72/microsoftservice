﻿namespace BNine.Application.Services
{
    using System.Text.RegularExpressions;
    using BNine.Application.Exceptions.ApiResponse;
    using BNine.Application.Interfaces;
    using BNine.Application.Models.MBanq;
    using BNine.Constants;
    using BNine.ResourceLibrary;
    using Microsoft.Extensions.Localization;

    public class PushPullFromExternalCardErrorHandlingService : IPushPullFromExternalCardErrorHandlingService
    {
        private const string INSUFFICIENT_ACCOUNT_BALANCE = "error.msg.savingsaccount.transaction.insufficient.account.balance";
        private const string TRANSFER_REACH_LIMIT_CODE = "error.msg.transfer.reach.limit";
        private const string INTERNAL_SERVER_ERROR = "internal.server.error";
        private const string TRANSFER_EXTERNAL_CARD_FAIL = "error.msg.transfer.externalCard.fail";
        private const string VALIDATION_ERRORS_EXIST = "validation.msg.validation.errors.exist";
        private const string SAVING_ACCOUNT_BLOCKED = "error.msg.saving.account.blocked.transaction.not.allowed";

        private const string AmountLimit = "amount limit";
        private const string NetworkRC = "networkRC";
        private const string CreditTransfer = "CREDIT transfer";
        private const string Daily = "daily";
        private const string Weekly = "weekly";
        private const string Monthly = "monthly";

        private IStringLocalizer<SharedResource> SharedLocalizer
        {
            get;
        }

        public PushPullFromExternalCardErrorHandlingService(IStringLocalizer<SharedResource> sharedLocalizer)
        {
            SharedLocalizer = sharedLocalizer;
        }

        public void HandlePushPullFromExternalCardErrors(IEnumerable<Error> errors)
        {
            if (errors.Any(x =>
                    INSUFFICIENT_ACCOUNT_BALANCE.Equals(x.Extensions?.GlobalisationMessageCode, StringComparison.OrdinalIgnoreCase)))
            {
                throw new ApiResponseException(ApiResponseErrorCodes.InsufficientFunds, SharedLocalizer["insufficientFundsErrorMessage"]);
            }

            if (errors.Any(x =>
                    TRANSFER_REACH_LIMIT_CODE.Equals(x.Extensions?.GlobalisationMessageCode, StringComparison.OrdinalIgnoreCase)))
            {
                if (errors.Any(x => x.Extensions.DefaultUserMessage.Contains(Daily)))
                {
                    if (errors.Any(x => x.Extensions.DefaultUserMessage.Contains(AmountLimit)))
                    {
                        throw new ApiResponseException(ApiResponseErrorCodes.DailyAmountLimitExceeded, SharedLocalizer["dailyAmountLimitExceededErrorMessage"]);
                    }
                    else
                    {
                        throw new ApiResponseException(ApiResponseErrorCodes.DailyCountLimitExceeded, SharedLocalizer["dailyCountLimitExceededErrorMessage"]);
                    }
                }

                if (errors.Any(x => x.Extensions.DefaultUserMessage.Contains(Weekly)))
                {
                    if (errors.Any(x => x.Extensions.DefaultUserMessage.Contains(AmountLimit)))
                    {
                        throw new ApiResponseException(ApiResponseErrorCodes.WeeklyAmountLimitExceeded, SharedLocalizer["weeklyAmountLimitExceededErrorMessage"]);
                    }
                    else
                    {
                        throw new ApiResponseException(ApiResponseErrorCodes.WeeklyCountLimitExceeded, SharedLocalizer["weeklyCountLimitExceededErrorMessage"]);
                    }
                }

                if (errors.Any(x => x.Extensions.DefaultUserMessage.Contains(Monthly)))
                {
                    if (errors.Any(x => x.Extensions.DefaultUserMessage.Contains(AmountLimit)))
                    {
                        throw new ApiResponseException(ApiResponseErrorCodes.MonthlyAmountLimitExceeded, SharedLocalizer["monthlyAmountLimitExceededErrorMessage"]);
                    }
                    else
                    {
                        throw new ApiResponseException(ApiResponseErrorCodes.MonthlyCountLimitExceeded, SharedLocalizer["monthlyCountLimitExceededErrorMessage"]);
                    }
                }
            }

            if (errors.Any(x =>
                VALIDATION_ERRORS_EXIST.Equals(x.Extensions?.GlobalisationMessageCode, StringComparison.OrdinalIgnoreCase)))
            {
                throw new ApiResponseException(ApiResponseErrorCodes.ErrorValidation, SharedLocalizer["validationErrorMessage"]);
            }

            if (errors.Any(x =>
                SAVING_ACCOUNT_BLOCKED.Equals(x.Extensions?.GlobalisationMessageCode, StringComparison.OrdinalIgnoreCase)))
            {
                throw new ApiResponseException(ApiResponseErrorCodes.ErrorAccountBlocked, SharedLocalizer["accountBlockedErrorMessage"]);
            }

            if (errors.Any(x => x.Extensions?.GlobalisationMessageCode is null && x.Message.Contains(CreditTransfer)))
            {
                throw new ApiResponseException(ApiResponseErrorCodes.ExternalcardCreditTransfer, SharedLocalizer["externalCardCreditTransferErrorMessage"]);
            }

            if (errors.Any(x =>
                INTERNAL_SERVER_ERROR.Equals(x.Extensions?.GlobalisationMessageCode, StringComparison.OrdinalIgnoreCase)))
            {
                throw new ApiResponseException(ApiResponseErrorCodes.ErrorServer, SharedLocalizer["errorServerMessage"]);
            }

            if (errors.Any(x =>
                TRANSFER_EXTERNAL_CARD_FAIL.Equals(x.Extensions?.GlobalisationMessageCode, StringComparison.OrdinalIgnoreCase)))
            {
                var networkError = errors.FirstOrDefault(x => x.Extensions.DefaultUserMessage.Contains(NetworkRC));
                if (networkError != null)
                {
                    var regex = new Regex(@"\d+");
                    var errorCode = regex.Match(networkError.Extensions.DefaultUserMessage);
                    var errorMessage = ExternalCardErrors.Errors[errorCode.Value];

                    throw new ApiResponseException(ApiResponseErrorCodes.ErrorCardVendor, errorMessage);
                }
                else
                {
                    throw new ApiResponseException(ApiResponseErrorCodes.OnetimeTranLimitExceeded, SharedLocalizer["onetimeTranLimitExceededErrorMessage"]);
                }
            }
        }
    }
}
