﻿namespace BNine.Application.Services;

using System.Collections.Immutable;
using BNine.Application.Models.Transfer;
using BNine.Domain.Entities.Settings;
using BNine.Enums.Transfers.Controller;
using Enums;
using Interfaces;

public class TransferIconsAndNamesProviderService : ITransferIconsAndNamesProviderService
{

    // Key = merchant description, Value = merchant code and readable name
    private readonly Dictionary<string, MerchantDescriptionModel> _merchantDescriptionsDictionary;
    private readonly ImmutableList<TransferIconSettings> _transferIconSettings;
    private readonly ImmutableList<MerchantCategoryCodeIconSettings> _mccIconSettings;
    private readonly IPartnerProviderService _partnerProviderService;

    public TransferIconsAndNamesProviderService(
        IBNineDbContext dbContext,
        IPartnerProviderService partnerProviderService
    )
    {
        _partnerProviderService = partnerProviderService;
        var matchesSortedByPriority = dbContext
            .TransferMerchantsSettings
            .AsQueryable()
            .ToList()
            .SelectMany(e => e.MerchantDescriptionKeywords,
                (settings, description) =>
                    new
                    {
                        description,
                        settings.MerchantName,
                        settings.MerchantDisplayName,
                        settings.PictureUrl,
                        settings.Priority
                    })
            .OrderByDescending(x => x.Priority);

        _merchantDescriptionsDictionary = matchesSortedByPriority
            .ToDictionary(
                x => x.description,
                y => new MerchantDescriptionModel
                {
                    Icon = y.MerchantName,
                    Name = y.MerchantDisplayName,
                    PictureUrl = y.PictureUrl,
                });

        _transferIconSettings = dbContext.TransferIconSettings.ToImmutableList();
        _mccIconSettings = dbContext.MerchantCategoryCodeIconSettings.ToImmutableList();
    }

    public string FindIconUrl(BankTransferIconOption transferIconOption)
    {
        // https://bninecom.atlassian.net/browse/B9-4741
        if (_partnerProviderService.GetPartner() == PartnerApp.MBanqApp)
        {
            return "https://b9devaccount.blob.core.windows.net/static-documents-container/TransactionIcons/05_person_of_mbanq.png";
        }

        return _transferIconSettings.FirstOrDefault(t => t.Id == transferIconOption)?.IconUrl;
    }

    public string FindMccIconUrl(int? mccType) =>
        _mccIconSettings.FirstOrDefault(ms => ms.Code == mccType)?.IconUrl;

    public MerchantDescriptionModel FindMerchant(string merchantDescription)
    {
        if (string.IsNullOrEmpty(merchantDescription))
        {
            return new MerchantDescriptionModel();
        }

        foreach (var kvPair in _merchantDescriptionsDictionary)
        {
            if (merchantDescription.StartsWith(kvPair.Key))
            {
                return kvPair.Value;
            }
        }

        return new MerchantDescriptionModel();
    }
}
