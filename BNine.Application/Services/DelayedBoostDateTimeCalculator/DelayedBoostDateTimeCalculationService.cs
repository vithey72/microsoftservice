﻿namespace BNine.Application.Services.DelayedBoostDateTimeCalculator;

using Interfaces;

public class DelayedBoostDateTimeCalculationService : IDateTimeCalculationService
{
    private readonly IDateTimeUtcProviderService _dateTimeProviderService;
    private readonly Dictionary<int, DateTimeItem> _dateTimeItems = new Dictionary<int, DateTimeItem>();
    private const int DayHours = 24;

    public DelayedBoostDateTimeCalculationService(IDateTimeUtcProviderService dateTimeProviderService)
    {
        _dateTimeProviderService = dateTimeProviderService;
    }

    public void Initialize((int BoostId, int HoursToDelay)[] hoursToDelayEntries)
    {
        if (!IsInitialized())
        {
            var startDate = _dateTimeProviderService.UtcNow();

            foreach (var (boostId, hoursToDelay) in hoursToDelayEntries)
            {

                var targetDate = CalculateTargetDateByBusinessDays(startDate, hoursToDelay);
                var daysToPayOff = CalculateBusinessDaysToPayOff(startDate, targetDate);

                _dateTimeItems.Add(
                    boostId,
                    new DateTimeItem(
                    startDate: startDate,
                    targetDate: targetDate,
                    businessDaysToPayOff: daysToPayOff,
                    hoursToDelay: hoursToDelay
                ));
            }
        }
    }


    public int GetDaysToPayOff(int boostId)
    {
        if (!IsInitialized())
        {
            throw new InvalidOperationException("DelayedBoostDateTimeCalculationService is not initialized");
        }

        return _dateTimeItems[boostId].BusinessDaysToPayOff;


    }

    public DateTime GetTargetDate(int boostId)
    {
        if (!IsInitialized())
        {
            throw new InvalidOperationException("DelayedBoostDateTimeCalculationService is not initialized");
        }

        return _dateTimeItems[boostId].TargetDate;
    }

    public string GetTargetDateFormatted(int boostId) => GetTargetDate(boostId).ToString("MM.dd.yyyy");


    public (int DaysToPayOff, DateTime TargetDate) GetDaysToPayOffAndTargetDate(int boostId)
    {
        if (!IsInitialized())
        {
            throw new InvalidOperationException("DelayedBoostDateTimeCalculationService is not initialized");
        }

        return (_dateTimeItems[boostId].BusinessDaysToPayOff, _dateTimeItems[boostId].TargetDate);
    }


    public static int CalculateBusinessDaysToPayOff(DateTime startDate, DateTime targetDate)
    {

        var rangeTotalHours = (int)(targetDate - startDate).TotalHours;
        var weekendsCount = CountWeekends(startDate, targetDate);
        // if we subtract from all hours in range weekends hours we will get business hours
        var delayedBoostBusinessHours = rangeTotalHours - (weekendsCount * DayHours);
        var daysToPayOff = CalculateDelayDays(delayedBoostBusinessHours);
        return daysToPayOff ?? 1;
    }


    private static int? CalculateDelayDays(double boostLimitHoursToDelay)
    {

        if (boostLimitHoursToDelay > 0)
        {
            var daysCount = (int)Math.Ceiling(boostLimitHoursToDelay / 24);
            return daysCount;
        }

        return null;
    }

    private static DateTime FindNearestPastDay(DateTime inputDate, DayOfWeek targetDayOfWeek)
    {
        DayOfWeek dayOfWeek = inputDate.DayOfWeek;

        var daysToSubtract = 0;
        if (dayOfWeek != targetDayOfWeek)
        {
            // Calculate the number of days to subtract to get to the nearest past Friday
            daysToSubtract = (dayOfWeek - DayOfWeek.Friday + 7) % 7;
        }

        DateTime nearestPastDay = inputDate.AddDays(-daysToSubtract);
        return nearestPastDay;
    }

    private static DateTime FindNearestFutureDay(DateTime inputDate, DayOfWeek targetDayOfWeek)
    {
        DayOfWeek dayOfWeek = inputDate.DayOfWeek;

        var daysToAdd = 0;
        if (dayOfWeek != targetDayOfWeek)
        {
            // Calculate the number of days to add to get to the nearest future Monday
            daysToAdd = (DayOfWeek.Monday - dayOfWeek + 7) % 7;
        }

        DateTime nearestFutureDay = inputDate.AddDays(daysToAdd);
        return nearestFutureDay;
    }

    private static DateTime SetClockToTheEndOfTheDay(DateTime date) =>
        SetClockToTime(date, TimeSpan.FromHours(23) + TimeSpan.FromMinutes(59));

    private static DateTime SetClockToTheStartOfTheDay(DateTime date) =>
        SetClockToTime(date, TimeSpan.FromHours(0) + TimeSpan.FromMinutes(0));


    private static DateTime SetClockToTime(DateTime inputDateTime, TimeSpan timeToSet)
    {
        DateTime adjustedDateTime = inputDateTime;

        if (inputDateTime.TimeOfDay != timeToSet)
        {
            TimeSpan timeDifference = timeToSet - inputDateTime.TimeOfDay;
            adjustedDateTime = inputDateTime.Add(timeDifference);
        }

        return adjustedDateTime;
    }


    private static int CountWeekends(DateTime startTime, DateTime endTime)
    {
        var weekendCount = 0;
        DateTime currentDate = startTime;

        while (currentDate <= endTime)
        {
            if (currentDate.DayOfWeek == DayOfWeek.Saturday || currentDate.DayOfWeek == DayOfWeek.Sunday)
            {
                weekendCount++;
            }

            currentDate = currentDate.AddDays(1);
        }

        return weekendCount;
    }

    private bool IsInitialized() => _dateTimeItems.Count > 0;

    private DateTime CalculateTargetDateByBusinessDays(DateTime startDate, int hoursToDelay)
    {

        DateTime targetDate = startDate.AddHours(hoursToDelay);


        if (targetDate.DayOfWeek is DayOfWeek.Saturday or DayOfWeek.Sunday)
        {
            var nearestFriday = SetClockToTheEndOfTheDay(FindNearestPastDay(targetDate, DayOfWeek.Friday));
            var hoursDiff = (nearestFriday - startDate).TotalHours;
            var hoursFromDataNotApplied = (int)Math.Ceiling(hoursToDelay - hoursDiff);
            var nearestMonday = SetClockToTheStartOfTheDay(FindNearestFutureDay(targetDate, DayOfWeek.Monday));
            targetDate = nearestMonday + TimeSpan.FromHours(hoursFromDataNotApplied);

        }
        else
        {
            var weekendsCount = CountWeekends(startDate, targetDate);
            targetDate += TimeSpan.FromDays(weekendsCount);
        }

        return targetDate;

    }



}

public class DateTimeItem
{
    public DateTimeItem(DateTime startDate, DateTime targetDate, int businessDaysToPayOff, int hoursToDelay)
    {
        StartDate = startDate;
        TargetDate = targetDate;
        BusinessDaysToPayOff = businessDaysToPayOff;
        HoursToDelay = hoursToDelay;
    }

    public int HoursToDelay
    {
        get;
        set;
    }

    public DateTime StartDate
    {
        get;
        set;
    }

    public DateTime TargetDate
    {
        get;

    }

    public int BusinessDaysToPayOff
    {
        get;
    }
}


