﻿namespace BNine.Application.Services
{
    using System.Threading.Tasks;
    using AutoMapper;
    using Interfaces;
    using Interfaces.Bank.Administration;

    public class ActivateClientsService : IActivateClientsService
    {
        private IBankClientsService BankClientsService
        {
            get;
        }

        private IBNineDbContext DbContext
        {
            get;
        }

        private IBankSavingAccountsService BankSavingAccountsService
        {
            get;
        }

        private IBankLoanAccountsService LoanAccountsService
        {
            get;
        }

        private IBankLoanProductsService LoanProductsService
        {
            get;
        }

        private IMapper Mapper
        {
            get;
        }

        private IBankInternalCardsService BankInternalCardsService
        {
            get;
        }

        public ActivateClientsService(
            IBNineDbContext dbContext,
            IBankClientsService bankClientsService,
            IBankSavingAccountsService bankSavingAccountsService,
            IBankLoanAccountsService loanAccountsService,
            IBankLoanProductsService loanProductsService,
            IBankInternalCardsService bankInternalCardsService,
            IMapper mapper
            )
        {
            BankClientsService = bankClientsService;
            BankSavingAccountsService = bankSavingAccountsService;
            LoanAccountsService = loanAccountsService;
            DbContext = dbContext;
            LoanProductsService = loanProductsService;
            BankInternalCardsService = bankInternalCardsService;
            Mapper = mapper;
        }

        public Task ActivateClient(int clientId)
        {
            //var externalClient = await DbContext.ExternalClients
            //    .Where(x => x.ExternalId == clientId)
            //    .Include(x => x.User)
            //    .FirstOrDefaultAsync();

            //if (externalClient == null)
            //{
            //    throw new NotFoundException(nameof(User));
            //}

            throw new System.NotImplementedException();
            //if (externalClient.User.VerificationStatus == VerificationStatus.APPROVED && externalClient.User.Status == UserStatus.PendingApproval)
            {
                //await BankClientsService.ActivateClient(externalClient.ExternalId);

                //await BankClientsService.LinkClientToUser(externalClient.ExternalId, externalClient.ExternalUserId);

                //var externalSavingsAccountId = await AddSavingAccount(externalClient);

                //await AddCard(externalClient.User, externalClient, externalSavingsAccountId);

                //externalClient.User.Status = UserStatus.Active;

                //DbContext.SaveChanges();
            }
        }

        //private async Task<int> AddSavingAccount(ExternalClient externalClient)
        //{
        //    var externalSavingsAccountId = await BankSavingAccountsService.CreateSavingAccount(externalClient.ExternalId, 1);

        //    var externalSavingAccount = new ExternalSavingAccount
        //    {
        //        ExternalId = externalSavingsAccountId,
        //        UserId = externalClient.UserId,
        //        ExternalClientId = externalClient.ExternalId
        //    };
        //    DbContext.ExternalSavingAccounts.Add(externalSavingAccount);

        //    await BankSavingAccountsService.ApproveSavingAccount(externalSavingsAccountId);

        //    await BankSavingAccountsService.ActivateSavingAccount(externalSavingsAccountId);

        //    await BankSavingAccountsService.EnableDebitTransfersForAccount(externalSavingsAccountId);

        //    return externalSavingsAccountId;
        //}

        //private async Task AddCard(User user, ExternalClient externalClient, int externalSavingAccountId)
        //{
        //    var cardExternalId = await BankInternalCardsService.CreateInternalCardREST(
        //        externalClient.ExternalId, externalSavingAccountId, 1);

        //    await BankInternalCardsService.EnableOnlinePayments(cardExternalId);

        //    user.DebitCards.Add(new DebitCard
        //    {
        //        Status = CardStatus.ACTIVE,
        //        ShippingStatus = CardShippingStatus.IN_TRANSIT,
        //        ExternalId = cardExternalId
        //    });
        //}

        // private void SetLoanDefaults(LoanAccount loanAccount, User user)
        // {
        //     loanAccount.ClientId = user.ExternalClientId.Value;
        //
        //     loanAccount.LinkAccountId = user.ExternalSavingsAccountId.Value;
        //
        //     loanAccount.Principal = 5000;
        //
        //     loanAccount.LoanTermFrequency = 12;
        //
        //     loanAccount.ExpectedDisbursementDate = DateTime.UtcNow.AddYears(1).ToString(DateFormat.Default);
        //
        //     loanAccount.LoanTermFrequencyType = TermFrequencyType.Months;
        // }
    }
}
