﻿namespace BNine.Application.Services;

using Aggregates.Configuration.Queries.GetConfiguration;
using Constants;
using Domain.Entities.User;
using Interfaces;
using Interfaces.Bank.Administration;
using MediatR;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Options;
using Settings;
using System;
using Microsoft.EntityFrameworkCore;

public class BankCurrentAccountsService : IBankCurrentAccountsService
{
    private readonly IConfiguration _configuration;
    private readonly IBNineDbContext _dbContext;
    private readonly IMediator _mediator;
    private readonly CurrentAdditionalAccountsSettings _currentAdditionalAccountsSettings;
    private readonly Interfaces.Bank.Administration.IBankSavingAccountsService _bankSavingAccountsService;

    private int CurrentAccountProductId
    {
        get
        {
            var parsedValue =
                int.TryParse(_configuration["CurrentAccountProductId"], out var currentAccountProductId)
                ? currentAccountProductId
                : 1;

            return parsedValue == 0
                ? 1
                : parsedValue;
        }
    }

    public BankCurrentAccountsService(
        IConfiguration configuration,
        IOptions<CurrentAdditionalAccountsSettings> currentAdditionalAccountsSettings,
        IBNineDbContext dbContext,
        IMediator mediator,
        IBankSavingAccountsService bankSavingAccountsService)
    {
        _configuration = configuration;
        _dbContext = dbContext;
        _mediator = mediator;
        _bankSavingAccountsService = bankSavingAccountsService;
        _currentAdditionalAccountsSettings = currentAdditionalAccountsSettings.Value;
    }

    public async Task AddCurrentBankAccountsIfNeeded(Guid userId, CancellationToken cancellationToken)
    {

        var user = _dbContext.Users
            .Include(u => u.CurrentAccount)
            .Include(u => u.CurrentAdditionalAccounts)
            .FirstOrDefault(u => u.Id == userId);

        var configuration = await _mediator.Send(new GetConfigurationQuery(), cancellationToken);
        var isFeatureEnabled = configuration.HasFeatureEnabled(FeaturesNames.Features.CurrentAdditionalAccountsInternal);

        await AddMainCurrentAccountIfNotExists(user, cancellationToken);

        if(isFeatureEnabled)
        {
            await AddAdditionalCurrentAccountsIfNotExists(user, cancellationToken);
        }
    }

    private async Task AddMainCurrentAccountIfNotExists(User user, CancellationToken cancellationToken)
    {
        await AddSavingAccountsInner(user, new []{ CurrentAccountProductId }, isMain: true, cancellationToken);
    }

    private async Task AddAdditionalCurrentAccountsIfNotExists(User user, CancellationToken cancellationToken)
    {
        var productIds = _currentAdditionalAccountsSettings.AccountProducts.Select(p => p.Id).ToArray();
        await AddSavingAccountsInner(user, productIds, isMain: false, cancellationToken);
    }

     private async Task AddSavingAccountsInner(User user, IReadOnlyCollection<int> productAccountIds, bool isMain,
            CancellationToken cancellationToken)
        {

            var createAccountsTask = isMain switch
            {
                true => CreateAndSaveCurrentMainAccountIfNotExists(user, productAccountIds, cancellationToken),
                false => CreateAndSaveCurrentAdditionalAccountsIfNotExists(user, productAccountIds, cancellationToken)
            };

            var createdAccounts = await createAccountsTask;

            foreach (var (_, externalId) in createdAccounts)
            {
                await _bankSavingAccountsService.ApproveSavingAccount(externalId);

                await _bankSavingAccountsService.ActivateSavingAccount(externalId);

                await _bankSavingAccountsService.EnableDebitTransfersForAccount(externalId);
            }

        }

        private async Task<CreatedBankAccount[]> CreateAndSaveCurrentAdditionalAccountsIfNotExists(User user,
            IReadOnlyCollection<int> productAccountIds, CancellationToken cancellationToken)
        {

            var accProducts = _currentAdditionalAccountsSettings.AccountProducts;
            var createdAccounts = new List<CreatedBankAccount>();

            foreach (var productId in productAccountIds)
            {

                var existing =
                    user.CurrentAdditionalAccounts.FirstOrDefault(a => a.ProductId == productId);

                if (existing != null)
                {
                   continue;
                }

                var externalId = await _bankSavingAccountsService.CreateSavingAccount(user.ExternalClientId.Value, productId);


                user.CurrentAdditionalAccounts.Add(new Domain.Entities.BankAccount.CurrentAdditionalAccount
                {
                    ProductId = productId,
                    ExternalId = externalId,
                    AccountType = accProducts.First(p => p.Id == productId).AccountType
                });

                createdAccounts.Add(new(productId,externalId));
            }

            await _dbContext.SaveChangesAsync(cancellationToken);
            return createdAccounts.ToArray();
        }

        private async Task<CreatedBankAccount[]> CreateAndSaveCurrentMainAccountIfNotExists(User user, IReadOnlyCollection<int> productAccountIds, CancellationToken cancellationToken)
        {

            if (productAccountIds.Count > 1)
            {
                throw new ArgumentOutOfRangeException(nameof(productAccountIds), "There can be only one product for main account");
            }

            if(user.CurrentAccount != null)
            {
                return Array.Empty<CreatedBankAccount>();
            }

            var productId = productAccountIds.Single();
            var externalId = await _bankSavingAccountsService.CreateSavingAccount(user.ExternalClientId.Value, productId);

            user.CurrentAccount = new Domain.Entities.BankAccount.CurrentAccount
            {
                ProductId = productId,
                ExternalId = externalId
            };
            await _dbContext.SaveChangesAsync(cancellationToken);

            return new CreatedBankAccount[] { new(productId,externalId) };
        }

        private record CreatedBankAccount(int ProductId, int ExternalId);



}
