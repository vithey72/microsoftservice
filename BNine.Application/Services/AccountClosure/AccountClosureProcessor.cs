﻿namespace BNine.Application.Services.AccountClosure;

using System;
using System.Threading;
using System.Threading.Tasks;
using BNine.Application.Aggregates.Users.AccountStateMachine;
using BNine.Application.Interfaces;
using Microsoft.Extensions.Logging;

public class AccountClosureProcessor : IAccountClosureProcessor
{
    private readonly ILogger<AccountClosureProcessor> _logger;
    private readonly IAccountClosureScheduler _accountClosureScheduler;
    private readonly IClientAccountSettingsProvider _clientAccountSettingsProvider;
    private readonly IClientAccountBlockedStateProvider _accountStateProvider;

    public AccountClosureProcessor(
        ILogger<AccountClosureProcessor> logger,
        IAccountClosureScheduler accountClosureScheduler,
        IClientAccountSettingsProvider clientAccountSettingsProvider,
        IClientAccountBlockedStateProvider accountStateProvider
        )
    {
        _logger = logger;
        _accountClosureScheduler = accountClosureScheduler;
        _clientAccountSettingsProvider = clientAccountSettingsProvider;
        _accountStateProvider = accountStateProvider;
    }

    public async Task<TimeSpan> ProcessDueClosuresAndGetNextCallDelay(CancellationToken cancellationToken)
    {
        await ProcessClosures(cancellationToken);

        var waitSpan = await GetWaitSpan(cancellationToken);

        return waitSpan;
    }

    private async Task ProcessClosures(CancellationToken cancellationToken)
    {
        var userIds = await _accountClosureScheduler.GetUsersWhoseTimeHasCome(cancellationToken);

        foreach (var userId in userIds)
        {
            await ProcessClosureForUser(userId, cancellationToken);
        }
    }

    private async Task ProcessClosureForUser(Guid userId, CancellationToken cancellationToken)
    {
        try
        {
            var pendingClosureState = await _accountStateProvider.TryGetCustomAccountPendingClosureBlockedState(userId, cancellationToken);
            if (pendingClosureState is null)
            {
                await RemoveScheduleAndLogWarning(userId, cancellationToken);
                return;
            }

            await pendingClosureState.InvokeScheduledClosure(cancellationToken);
        }
        catch (Exception ex)
        {
            _logger.LogError(ex.Message);
        }
    }

    private async Task<TimeSpan> GetWaitSpan(CancellationToken cancellationToken)
    {
        var settings = await _clientAccountSettingsProvider.GetClientAccountSettings(cancellationToken);

        return settings.PendingClosureCheckGap;
    }

    private async Task RemoveScheduleAndLogWarning(Guid userId, CancellationToken cancellationToken)
    {
        await _accountClosureScheduler.RemoveAccountClosureSchedule(userId, cancellationToken);

        _logger.LogWarning($"Pending closure schedule remains for user {userId}");
    }

}
