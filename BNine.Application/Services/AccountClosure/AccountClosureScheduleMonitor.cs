﻿namespace BNine.Application.Services.AccountClosure;

using System;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;

public class AccountClosureScheduleMonitor : BackgroundService
{
    private readonly ILogger<AccountClosureScheduleMonitor> _logger;
    private readonly IServiceProvider _serviceProvider;

    public AccountClosureScheduleMonitor(
        ILogger<AccountClosureScheduleMonitor> logger,
        IServiceProvider serviceProvider
        )
    {
        _logger = logger;
        _serviceProvider = serviceProvider;
    }

    protected override async Task ExecuteAsync(CancellationToken cancellationToken)
    {
        while (!cancellationToken.IsCancellationRequested)
        {
            var waitSpan = await ProcessDueClosuresAndGetNextCallDelay(cancellationToken);

            await Task.Delay(waitSpan, cancellationToken);
        }
    }

    private async Task<TimeSpan> ProcessDueClosuresAndGetNextCallDelay(CancellationToken cancellationToken)
    {
        var waitSpan = TimeSpan.FromHours(1);

        try
        {
            using var serviceScope = _serviceProvider.CreateScope();
            var accountClosureProcessor = serviceScope.ServiceProvider.GetRequiredService<IAccountClosureProcessor>();

            waitSpan = await accountClosureProcessor.ProcessDueClosuresAndGetNextCallDelay(cancellationToken);
        }
        catch (Exception ex)
        {
            _logger.LogError(ex.Message);
        }

        return waitSpan;
    }

    public override async Task StartAsync(CancellationToken cancellationToken)
    {
        await base.StartAsync(cancellationToken);

        _logger.LogInformation("AccountClosureScheduleMonitor is started");
    }

    public override Task StopAsync(CancellationToken cancellationToken)
    {
        _logger.LogInformation("AccountClosureScheduleMonitor is stopping");

        return base.StopAsync(cancellationToken);
    }
}
