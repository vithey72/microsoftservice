﻿namespace BNine.Application.Services.AccountClosure;

public interface IAccountClosureProcessor
{
    Task<TimeSpan> ProcessDueClosuresAndGetNextCallDelay(CancellationToken cancellationToken);
}
