﻿namespace BNine.Application.Services;

using Enums.Advance;
using Exceptions;
using Interfaces;
using Interfaces.Bank.Administration;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;

public class AdvancesCompletedCheckupService : IAdvancesCompletedCheckupService
{
    private readonly IConfiguration _configuration;
    private readonly IBNineDbContext _dbContext;
    private readonly IAdvanceWidgetStateProvider _advanceWidgetStateProvider;
    private readonly IBankExternalCardsService _bankExternalCardsService;

    public AdvancesCompletedCheckupService(
        IConfiguration configuration,
        IBNineDbContext dbContext,
        IAdvanceWidgetStateProvider advanceWidgetStateProvider,
        IBankExternalCardsService bankExternalCardsService
        )
    {
        _configuration = configuration;
        _dbContext = dbContext;
        _advanceWidgetStateProvider = advanceWidgetStateProvider;
        _bankExternalCardsService = bankExternalCardsService;
    }

    public async Task<bool> IsModalWindowRequired(Guid userId) => !(await IsUserTrustedEnough(userId));

    public async Task<bool> IsUserTrustedEnough(Guid userId)
    {
        var userWithStats =
            _dbContext.Users
                .Include(u => u.UserSyncedStats)
                .Include(u => u.AdvanceStats)
                .FirstOrDefault(u => u.Id == userId);

        if (userWithStats == null)
        {
            throw new NotFoundException($"User with Id {userId} is not found");
        }

        var advancesTakenCount = userWithStats.UserSyncedStats?.AdvancesTakenCount ?? 0;
        var widgetState = await _advanceWidgetStateProvider.GetWidgetState(userWithStats);

        return await IsUserTrustedEnoughInternal(advancesTakenCount, widgetState, userWithStats.ExternalClientId);
    }

    private async Task<bool> IsUserTrustedEnoughInternal(int advancesTakenCount, AdvanceWidgetState state, int? externalClientId)
    {
        var isExternalCardLinked = await IsExternalCardLinked(externalClientId);
        var isEnoughCompletedAdvances = IsEnoughCompletedAdvances(advancesTakenCount, state);

        return isEnoughCompletedAdvances || isExternalCardLinked;
    }


    private bool IsEnoughCompletedAdvances(int advancesTakenCount, AdvanceWidgetState state)
    {

        var advancesCompleted = state switch
        {
            AdvanceWidgetState.AdvanceDisbursed => SubtractOne(advancesTakenCount),
            _ => advancesTakenCount
        };

        // set this config value to 0 so the checkup will be skipped
        if (AdvancesCompletedRequiredThreshold == 0)
        {
            return true;
        }

        return advancesCompleted >= AdvancesCompletedRequiredThreshold;
    }

    private async Task<bool> IsExternalCardLinked(int? externalClientId)
    {
        // card can't be linked if there is no external client id
        if (!externalClientId.HasValue)
        {
            return false;
        }
        var activeExternalCards =
            (await _bankExternalCardsService.GetExternalCards(externalClientId.Value))
            .Where(c => !c.IsDeleted).ToList();
        return activeExternalCards.Count > 0;
    }

    private static int SubtractOne(int input) => input == 0 ? input : input - 1;

    private int AdvancesCompletedRequiredThreshold
    {
        get
        {
            return int.TryParse(_configuration["AdvancesCompletedRequiredThreshold"], out var advancesThreshold)
                ? advancesThreshold
                : 1;
        }
    }
}
