﻿namespace BNine.Application.Services;

using Enums;
using Interfaces;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Options;
using Settings;

public class PartnerProviderService : IPartnerProviderService
{
    public const string UserAgentHeader = "User-Agent";
    private readonly IHttpContextAccessor _httpContext;
    private readonly PartnerSettings _settings;

    public PartnerProviderService(IHttpContextAccessor httpContext, IOptions<PartnerSettings> options)
    {
        _httpContext = httpContext;
        _settings = options.Value;
    }

    public PartnerApp GetPartner()
    {
        if (_httpContext.HttpContext != null && _settings.OverrideByHttpHeader)
        {
            var userAgentValue = _httpContext
                .HttpContext
                .Request
                .Headers
                .FirstOrDefault(x => x.Key == UserAgentHeader)
                .Value;

            // https://bninecom.atlassian.net/browse/B9-5016
            if (userAgentValue.ToString().StartsWith("mbanq", StringComparison.InvariantCultureIgnoreCase))
            {
                return PartnerApp.MBanqApp;
            }
            if (userAgentValue.ToString().StartsWith("eazy", StringComparison.InvariantCultureIgnoreCase))
            {
                return PartnerApp.Eazy;
            }
            if (userAgentValue.ToString().StartsWith("poetryy", StringComparison.InvariantCultureIgnoreCase))
            {
                return PartnerApp.Poetryy;
            }
            if(userAgentValue.ToString().StartsWith("usnational", StringComparison.InvariantCultureIgnoreCase))
            {
                return PartnerApp.USNational;
            }
            if (userAgentValue.ToString().StartsWith("p2p", StringComparison.InvariantCultureIgnoreCase))
            {
                return PartnerApp.P2P;
            }
            if (userAgentValue.ToString().StartsWith("payhammer", StringComparison.InvariantCultureIgnoreCase))
            {
                return PartnerApp.PayHammer;
            }
        }

        return _settings.AppVariant;
    }
}
