﻿namespace BNine.Application.Services;

using Aggregates.Banners.Models;
using Aggregates.ReferalProgramm.Commands.DeleteActivationEligibilityBlock;
using Aggregates.ReferalProgramm.Commands.DeleteReceivedBonusBlock;
using Aggregates.Users.Queries.GetUserGroups;
using AutoMapper;
using BNine.Application.Interfaces;
using Constants;
using Domain.Entities.Banners;
using MediatR;
using Microsoft.EntityFrameworkCore;

public class MarketingBannersService : IMarketingBannersService
{
    private readonly IBNineDbContext _dbContext;
    private readonly ICurrentUserService _currentUser;
    private readonly IDateTimeProviderService _dateTimeProvider;
    private readonly IMapper _mapper;
    private readonly IMediator _mediator;

    public MarketingBannersService(
        IBNineDbContext dbContext,
        ICurrentUserService currentUser,
        IDateTimeProviderService dateTimeProvider,
        IMapper mapper,
        IMediator mediator
    )
    {
        _dbContext = dbContext;
        _currentUser = currentUser;
        _dateTimeProvider = dateTimeProvider;
        _mapper = mapper;
        _mediator = mediator;
    }

    public async Task<List<TViewModel>> GetWalletScreenBanners<TBanner, TViewModel>()
        where TBanner : BaseMarketingBanner
    {
        var groupsFromFeatures = await _mediator.Send(new GetUserGroupsQuery(_currentUser.UserId!.Value));
        var groupIdsFromDb = await _dbContext.UserGroups
            .Include(ug => ug.Group)
            .Where(ug => ug.UserId == _currentUser.UserId)
            .Select(x => x.Group.Id)
            .Distinct()
            .ToArrayAsync();

        // entities fetched from features omit groups that aren't connected to any features,
        // hence we need to add those groups manually
        var allGroups = groupIdsFromDb
            .Concat(
                groupsFromFeatures
                    .Select(g => g.Id)
                    .Distinct())
            .Distinct();

        var userWithBanners = await _dbContext.Users
            .Include(u => u.MarketingBannersSeen)
            .ThenInclude(sb => sb.Banner)
            .AsNoTracking()
            .FirstOrDefaultAsync(u => u.Id == _currentUser.UserId);

        if (userWithBanners == null)
        {
            throw new KeyNotFoundException("User is not present in DB.");
        }

        var now = _dateTimeProvider.Now();
        var banners = await _dbContext.AllMarketingBanners
            .AsQueryable()
            .Where(bs => bs.IsEnabled
                         && (bs.ShowFrom == null || bs.ShowFrom < now)
                         && (bs.ShowTil == null || bs.ShowTil > now)
                         && (bs.MinUserRegistrationDate == null || bs.MinUserRegistrationDate < userWithBanners.CreatedAt))
            .OfType<TBanner>()
            .OrderByDescending(bs => bs.Priority)
            .ToArrayAsync();

        var bannersToNotShow = userWithBanners
            .MarketingBannersSeen
            .Where(sb => sb.NextDisplayTime > DateTime.Now)
            .Select(sb => sb.Banner.Id)
            .ToArray();

        var bannersToShow = banners
            .Where(b => !bannersToNotShow.Contains(b.Id))
            .ToArray();

        bannersToShow = bannersToShow.Where(b =>
                b.Groups is null ||
                b.Groups.Length == 0 ||
                allGroups.Intersect(b.Groups).Any())
            .ToArray();

        return _mapper.Map<List<TViewModel>>(bannersToShow);
    }

    /// <summary>
    /// TODO: handle not adding entry if banner cannot be closed.
    /// </summary>
    public async Task MarkBannerAsSeen(string name, DateTime? customDate = null)
    {
        switch (name)
        {
            case BannersNames.EligibleForBonusBanner:
                await _mediator.Send(new DeleteActivationEligibilityBlockCommand());
                return;
            case BannersNames.SendNewReferralCodeBanner:
                await _mediator.Send(new DeleteReceivedBonusBlockCommand());
                return;
        }

        var banner = await GetMarketingBannerEntity(name);
        if (banner == null)
        {
            return;
        }

        var existingSeenEntity = await GetBannerSeenEntity(banner.Id);
        if (existingSeenEntity == null)
        {
            _dbContext.MarketingBannersSeen.Add(
                new SeenMarketingBanner
                {
                    UserId = _currentUser.UserId!.Value,
                    BannerId = banner.Id,
                    BannerName = banner.Name,
                    NextDisplayTime = CalculateDisplayTime(banner, customDate),
                    SeenTimes = 1,
                });
            await _dbContext.SaveChangesAsync(CancellationToken.None);
        }
        else
        {
            if (existingSeenEntity.NextDisplayTime > DateTime.Now)
            {
                return;
            }

            existingSeenEntity.NextDisplayTime = CalculateDisplayTime(banner, customDate, existingSeenEntity);
            existingSeenEntity.SeenTimes += 1;
            await _dbContext.SaveChangesAsync(CancellationToken.None);
        }

    }

    public Task<GetWalletScreenTopBannerResponse.BannerInstance> GetBannerByName(string name)
    {
        throw new NotImplementedException();
    }

    public Task<GetWalletScreenTopBannerResponse.BannerInstance> GetBannerByGroup(string name, Guid[] groups)
    {
        throw new NotImplementedException();
    }

    private async Task<BaseMarketingBanner> GetMarketingBannerEntity(string name)
    {
        return await _dbContext
            .AllMarketingBanners
            .FirstOrDefaultAsync(b => b.Name.ToUpper() == name.ToUpper()
                && b.IsEnabled);
    }

    private async Task<SeenMarketingBanner> GetBannerSeenEntity(Guid bannerId)
    {
        return await _dbContext.MarketingBannersSeen.FirstOrDefaultAsync(sb =>
            sb.UserId == _currentUser.UserId
            && sb.BannerId == bannerId);
    }

    private DateTime CalculateDisplayTime(BaseMarketingBanner banner, DateTime? customTime = null,
        SeenMarketingBanner seenInstance = null)
    {
        var maxValue = TimeSpan.FromDays(140000);
        if (customTime != null)
        {
            return customTime.Value;
        }
        if (banner.ShowCooldownProgressive != null && banner.ShowCooldownProgressive.Length > 0)
        {
            var delay = seenInstance == null
                ? banner.ShowCooldownProgressive[0]
                : seenInstance.SeenTimes < banner.ShowCooldownProgressive.Length
                    ? banner.ShowCooldownProgressive[seenInstance.SeenTimes]
                    : maxValue;

            return _dateTimeProvider.Now().Add(delay);
        }
        if (banner.ShowCooldown == null)
        {
            return _dateTimeProvider.Now().Add(maxValue);
        }

        return _dateTimeProvider.Now().Add(banner.ShowCooldown.Value);
    }

}
