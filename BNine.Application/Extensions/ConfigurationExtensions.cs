﻿namespace BNine.Application.Extensions
{
    using System.Collections.Generic;
    using System.Linq;
    using Aggregates.Configuration.Models;

    public static class ConfigurationExtensions
    {
        /// <summary>Return documents as key-value. </summary>
        /// <param name="configuration"><see cref="Configuration"/>. </param>
        /// <returns>Documents as key-value. </returns>
        public static Dictionary<string, string> ToKeyValueDocuments(this Configuration configuration) => configuration.Documents.ToDictionary(k => k.Key, v => v.StoragePath);
    }
}
