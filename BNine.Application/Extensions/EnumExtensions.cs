﻿namespace BNine.Application.Extensions
{
    using System;
    using System.ComponentModel;
    using System.Linq;
    using System.Reflection;
    using System.Runtime.Serialization;

    public static class EnumExtensions
    {
        public static string ToLowerCamelCase<T>(this T value) where T : Enum
        {
            var stringValue = value.ToString();

            var result = char.ToLowerInvariant(stringValue[0]) + stringValue[1..];

            return result;
        }

        public static bool TryParseByEnumMemberAttribute<T>(string value, out T result) where T : Enum
        {
            try
            {
                result = ParseByEnumMemberAttribute<T>(value);

                return true;

            }
            catch
            {
                result = default;

                return false;
            }
        }

        public static T ParseByEnumMemberAttribute<T>(string value) where T : Enum
        {
            var enumMember = typeof(T).GetMembers()
                    .First(x => x.GetCustomAttributes(false).OfType<EnumMemberAttribute>().Any(c => value.Equals(c.Value, StringComparison.OrdinalIgnoreCase)));


            return (T)Enum.Parse(typeof(T), enumMember.Name);
        }

        public static string? GetEnumMemberValue<T>(this T value)
            where T : Enum
        {
            return typeof(T)
                .GetTypeInfo()
                .DeclaredMembers
                .FirstOrDefault(x => x.Name == value.ToString())
                ?.GetCustomAttribute<EnumMemberAttribute>(false)
                ?.Value;
        }

        public static string? GetEnumDescriptionValue<T>(this T value)
            where T : Enum
        {
            return typeof(T)
                .GetTypeInfo()
                .DeclaredMembers
                .FirstOrDefault(x => x.Name == value.ToString())
                ?.GetCustomAttribute<DescriptionAttribute>(false)
                ?.Description;
        }
    }
}
