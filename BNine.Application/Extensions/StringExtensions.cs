﻿namespace BNine.Application.Extensions
{
    internal static class StringExtensions
    {
        public static string ToLowerStart(this string self)
        {
            return self == null || self.Length == 0 ? self : (self.Substring(0, 1).ToLower() + self.Substring(1));
        }
    }
}
