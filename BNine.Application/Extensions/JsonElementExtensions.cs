﻿namespace BNine.Application.Extensions
{
    using System.Text.Json;
    using Newtonsoft.Json;

    public static class JsonElementExtensions
    {
        public static T ToObject<T>(this JsonElement element)
        {
            try
            {
                var json = element.GetRawText();
                return JsonConvert.DeserializeObject<T>(json);
            }
            catch
            {
                return default;
            }
        }
    }
}
