﻿namespace BNine.Application.Extensions;

using System;
using BNine.Application.Interfaces;

public static class UserExtension
{
    public static Guid UserId(this ICurrentUserService service)
    {
        if (!service.UserId.HasValue)
        {
            throw new InvalidOperationException();
        }

        return service.UserId.Value;
    }
}
