﻿namespace BNine.Application.Extensions
{
    using System;
    using System.Collections.Generic;

    public static class DateTimeExtensions
    {
        public static DateTime AddWorkDays(this DateTime date, int workingDays)
        {
            var direction = workingDays < 0 ? -1 : 1;
            DateTime newDate = date;
            while (workingDays != 0)
            {
                newDate = newDate.AddDays(direction);
                if (newDate.DayOfWeek != DayOfWeek.Saturday &&
                    newDate.DayOfWeek != DayOfWeek.Sunday &&
                    !newDate.IsHoliday())
                {
                    workingDays -= direction;
                }
            }
            return newDate;
        }

        public static DateTime RemoveSeconds(this DateTime dt)
        {
            return new DateTime(dt.Year, dt.Month, dt.Day, dt.Hour, dt.Minute, 0, 0, dt.Kind);
        }

        private static bool IsHoliday(this DateTime date)
        {
            var holidays = new List<DateTime>
            {
                //new DateTime(2010,12,27),
                //new DateTime(2010,12,28),
                //new DateTime(2011,01,03),
                //new DateTime(2011,01,12),
                //new DateTime(2011,01,13)
            };

            return holidays.Contains(date.Date);
        }
    }
}
