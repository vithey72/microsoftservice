﻿namespace BNine.Application.Extensions;

using System;
using System.Linq;
using Domain.Entities.BankAccount;
using Microsoft.EntityFrameworkCore;

public static class EfLinqTransferPayrollExtensions
{
    public static async Task<decimal> GetPayrollSum(this DbSet<ACHCreditTransfer> transfers, Guid userId, string[] achKeywords, TimeSpan timeSpan)
    {
        var minDate = DateTime.UtcNow.Add(-timeSpan);
        return (await transfers.Where(x => x.UserId == userId)
            .Where(x => x.CreatedAt > minDate)
            .ToListAsync())
            .GroupBy(x => x.ExternalId)
            .Select(x => x.First())
            .Where(ach =>
                achKeywords.Any(kw =>
                    ach.Reference.Contains(kw, StringComparison.OrdinalIgnoreCase)))
            .Select(x => x.Amount)
            .ToList()
            .Sum();
    }

    public static async Task<List<int>> GetPayrollExternalTransactionIds(this DbSet<ACHCreditTransfer> transfers,
        Guid userId, string[] achKeywords)
    {
        var achCreditTransfers = await transfers.Where(x => x.UserId == userId)
            .ToListAsync();

        var transactionIds = achCreditTransfers
            .GroupBy(x => x.ExternalId)
            .Select(x => x.First())
            .Where(ach =>
                achKeywords.Any(kw =>
                    ach.Reference.Contains(kw, StringComparison.OrdinalIgnoreCase)))
            .Select(x => x.SavingsAccountTransferExternalId)
            .ToList();

        return transactionIds;
    }

    public static async Task<bool> HasAnyPayroll(this DbSet<ACHCreditTransfer> transfers, Guid userId, string[] achKeywords)
    {
        return (await transfers
            .Where(x => x.UserId == userId)
            .Where(x => x.Amount > 0)
            .ToListAsync())
            .Any(x => achKeywords.Any(c => x.Reference.Contains(c, StringComparison.OrdinalIgnoreCase)));
    }
}
