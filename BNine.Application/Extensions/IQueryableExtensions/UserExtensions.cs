﻿namespace BNine.Application.Extensions.IQueryableExtensions
{
    using System.Collections.Generic;
    using System.Linq;
    using BNine.Domain.Entities.User;
    using BNine.Enums;

    public static class UserExtensions
    {
        public static IQueryable<User> FilterByStatuses(this IQueryable<User> query, IEnumerable<UserStatus> statuses)
        {
            if (statuses == null || !statuses.Any())
            {
                return query;
            }

            return query.Where(x => statuses.Contains(x.Status) || statuses.Contains(UserStatus.Blocked) && x.IsBlocked);
        }

        public static IQueryable<User> FilterBySearchString(this IQueryable<User> query, string search)
        {
            if (string.IsNullOrWhiteSpace(search))
            {
                return query;
            }

            return query.Where(x =>
                (x.FirstName + " " + x.LastName).Contains(search) ||
                (x.LastName + " " + x.FirstName).Contains(search) ||
                x.Phone.Contains(search) ||
                x.Email.Contains(search) ||
                x.Id.ToString() == search.ToUpper() ||
                x.Document.Number.Contains(search) ||
                x.Identity.Value.Contains(search)
            );
        }
    }
}
