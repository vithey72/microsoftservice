﻿namespace BNine.Application.Extensions.IQueryableExtensions
{
    using System.Linq;
    using BNine.Domain.Interfaces;

    public static class ISoftDeleteExtensions
    {
        public static IQueryable<T> NotRemoved<T>(this IQueryable<T> query) where T : ISoftDelete
        {
            return query.Where(x => !x.DeletedAt.HasValue);
        }
    }
}
