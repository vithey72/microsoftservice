﻿namespace BNine.Application.Models
{
    using Newtonsoft.Json;

    public class PaginatedFilteredList<T1, T2> : PaginatedList<T1>
    {
        public PaginatedFilteredList(IEnumerable<T1> items, T2 filter, long count, int pageNumber, int pageSize)
            : base(items.ToList(), count, pageNumber, pageSize)
        {
            Filter = filter;
        }

        [JsonProperty("filter")]
        public T2 Filter
        {
            get;
            set;
        }
    }
}
