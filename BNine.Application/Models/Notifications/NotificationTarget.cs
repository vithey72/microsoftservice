﻿namespace BNine.Application.Models.Notifications
{
    using System;
    using BNine.Enums;

    public class NotificationTarget
    {
        public NotificationTarget(string value, MobilePlatform mobilePlatform)
        {
            Value = value;
            MobilePlatform = mobilePlatform;
        }

        public NotificationTarget(Guid value, MobilePlatform mobilePlatform)
        {
            Value = value.ToString();
            MobilePlatform = mobilePlatform;
        }

        public string Value
        {
            get; set;
        }

        public MobilePlatform MobilePlatform
        {
            get; set;
        }
    }
}
