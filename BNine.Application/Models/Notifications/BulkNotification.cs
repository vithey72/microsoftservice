﻿namespace BNine.Application.Models.Notifications
{
    using System.Collections.Generic;

    public class BulkNotification
    {
        public BulkNotification(string body, IEnumerable<string> tags, string deeplink, string appsflyerEventType, string title, string category)
        {
            Tags = tags;
            Deeplink = deeplink;
            Body = body;
            AppsflyerEventType = appsflyerEventType;
            Title = title;
            Category = category;
        }

        public string Title
        {
            get;
        }

        public string Category
        {
            get;
        }

        public string Body
        {
            get;
        }

        public string Deeplink
        {
            get;
        }

        public string AppsflyerEventType
        {
            get;
        }

        public IEnumerable<string> Tags
        {
            get;
        } = new List<string>();
    }
}
