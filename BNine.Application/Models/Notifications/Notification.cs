﻿namespace BNine.Application.Models.Notifications
{
    using System.Collections.Generic;

    public class Notification
    {
        public Notification(string body, IList<NotificationTarget> notificationTargets = null,
            string deeplink = null, string appsflyerEventType = null, string title = null,
            string sub1 = null, bool isSilent = false)
        {
            NotificationTargets = notificationTargets;
            Deeplink = deeplink;
            Body = body;
            AppsflyerEventType = appsflyerEventType;
            Title = title;
            Sub1 = sub1;
            IsSilent = isSilent;
        }

        public string Body
        {
            get; set;
        }

        public string Title
        {
            get; set;
        }

        public string Deeplink
        {
            get; set;
        }

        public string AppsflyerEventType
        {
            get; set;
        }

        public IList<NotificationTarget> NotificationTargets
        {
            get; protected set;
        } = new List<NotificationTarget>();

        public string Sub1
        {
            get; set;
        }

        public bool IsSilent
        {
            get; set;
        }
    }
}
