﻿namespace BNine.Application.Models.Notifications
{
    using System;
    using System.Collections.Generic;
    using BNine.Domain.Entities;
    using BNine.Enums;

    public class SendNotificationListItem
    {
        public Guid UserId
        {
            get; set;
        }

        public string NotificationText
        {
            get; set;
        }

        public string NotificationCategory
        {
            get; set;
        }

        public string NotificationTitle
        {
            get; set;
        }

        public bool IsNotificationsEnabled
        {
            get; set;
        }

        public bool IsImportant
        {
            get; set;
        }

        public NotificationTypeEnum Type
        {
            get; set;
        }

        public IEnumerable<Device> Devices
        {
            get; set;
        }

        public string Deeplink
        {
            get; set;
        }

        public string AppsflyerEventType
        {
            get; set;
        }
    }
}
