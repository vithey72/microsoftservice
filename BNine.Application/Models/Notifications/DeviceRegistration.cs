﻿namespace BNine.Application.Models.Notifications
{
    using BNine.Enums;
    using Newtonsoft.Json;
    using Newtonsoft.Json.Converters;

    public class DeviceRegistration
    {
        [JsonConverter(typeof(StringEnumConverter))]
        public MobilePlatform Platform
        {
            get; set;
        }

        public string Handle
        {
            get; set;
        }

        public string[] Tags
        {
            get; set;
        }
    }
}
