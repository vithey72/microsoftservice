﻿namespace BNine.Application.Models.Plaid
{
    public class BalanceDto
    {
        public string Currency
        {
            get; set;
        }

        public decimal? Available
        {
            get; set;
        }

        public decimal? Current
        {
            get; set;
        }
    }
}
