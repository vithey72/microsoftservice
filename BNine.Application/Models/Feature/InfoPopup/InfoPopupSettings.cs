﻿namespace BNine.Application.Models.Feature.InfoPopup
{
    public class InfoPopupSettings
    {
        public Dictionary<string, InfoPopup> Popup
        {
            get; set;
        }

        public bool IsEnabled
        {
            get; set;
        }
    }
}
