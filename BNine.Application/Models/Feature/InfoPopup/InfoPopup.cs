﻿namespace BNine.Application.Models.Feature.InfoPopup
{
    using System.Text.Json.Serialization;

    public class InfoPopup
    {
        [JsonIgnore]
        public string Title
        {
            get; set;
        }

        [JsonIgnore]
        public string Subtitle
        {
            get; set;
        }

        [JsonIgnore]
        public string ButtonText
        {
            get; set;
        }

        [JsonIgnore]
        public string Deeplink
        {
            get; set;
        }

        [JsonIgnore]
        public string ExternalLink
        {
            get; set;
        }

        [JsonIgnore]
        public string PictureUrl
        {
            get; set;
        }

        [JsonIgnore]
        public string Action
        {
            get; set;
        }

        [JsonIgnore]
        public int Priority
        {
            get; set;
        }

        [JsonIgnore]
        public int ShowAfter
        {
            get; set;
        }

        /// <summary>
        /// An array of ShowAfter values. If empty or null, ignored. The value to add on every iteration is determined by <see cref="CyclesPassed"/>.
        /// </summary>
        [JsonIgnore]
        public int[] ShowAfterProgressive
        {
            get; set;
        }

        public string ValidFrom
        {
            get; set;
        }

        /// <summary>
        /// How many times this banner has been shown.
        /// </summary>
        public int CyclesPassed
        {
            get; set;
        }
    }
}
