﻿namespace BNine.Application.Models.Feature.MarketingBanners
{
    using Newtonsoft.Json.Linq;

    public record ActiveBanner
    {
        public ActiveBanner(string id, JObject translations, string deeplink)
        {
            Id = id;
            Translations = translations;
            Deeplink = deeplink;
        }

        public string Id
        {
            get; set;
        }

        public JObject Translations
        {
            get; set;
        }

        public string Deeplink
        {
            get; set;
        }
    }
}
