﻿namespace BNine.Application.Models
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using BNine.Application.Interfaces;
    using Microsoft.EntityFrameworkCore;
    using Newtonsoft.Json;

    public class PaginatedList<T>
    {
        [JsonProperty("items")]
        public List<T> Items
        {
            get; set;
        }

        [JsonProperty("pageNumber")]
        public int PageNumber
        {
            get;
        }

        [JsonProperty("totalPages")]
        public int TotalPages
        {
            get;
        }

        [JsonProperty("totalCount")]
        public long TotalCount
        {
            get;
        }

        [JsonProperty("pageSize")]
        public int PageSize
        {
            get;
        }

        public PaginatedList(List<T> items, long count, int pageNumber, int pageSize)
        {
            PageSize = pageSize;
            PageNumber = pageNumber;
            TotalPages = (int)Math.Ceiling(count / (double)pageSize);
            TotalCount = count;
            Items = items;
        }

        public PaginatedList(IQueryable<T> source, int pageIndex, int pageSize) : this(source.Skip((pageIndex - 1) * pageSize).Take(pageSize).ToList(), source.Count(), pageIndex, pageSize)
        {
        }

        public PaginatedList()
        {
        }

        [JsonProperty("hasPreviousPage")]
        public bool HasPreviousPage => PageNumber > 1;

        [JsonProperty("hasNextPage")]
        public bool HasNextPage => PageNumber < TotalPages;

        public static async Task<PaginatedList<T>> CreateAsync(IQueryable<T> source, int pageIndex, int pageSize)
        {
            var count = await source.CountAsync();
            var items = await source.Skip((pageIndex - 1) * pageSize).Take(pageSize).ToListAsync();

            return new PaginatedList<T>(items, count, pageIndex, pageSize);
        }
    }

    public static class PaginatedListExtensions
    {
        public static PaginatedList<T> AddOrder<T>(this PaginatedList<T> list) where T : IOrderedListItem
        {
            list.Items = Enumerable.Range(1, list.Items.Count).Select(x =>
            {
                list.Items[x - 1].Order = (list.PageNumber - 1) * list.PageSize + x;
                return list.Items[x - 1];
            }).ToList();
            return list;
        }
    }
}
