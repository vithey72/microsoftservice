﻿namespace BNine.Application.Models.Dto;

using Newtonsoft.Json;

public class AdminApiUser
{
    public Guid Id
    {
        get; set;
    }

    public DateTime CreatedAt
    {
        get; set;
    }

    public string Email
    {
        get; set;
    }

    [JsonIgnore]
    public string PasswordHash
    {
        get; set;
    }

    public string FirstName
    {
        get; set;
    }

    public string LastName
    {
        get; set;
    }

    public string Role
    {
        get;
        set;
    }
}
