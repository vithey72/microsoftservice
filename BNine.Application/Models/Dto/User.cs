﻿namespace BNine.Application.Models.Dto
{
    using System;
    using BNine.Enums;

    public class User
    {
        public Guid Id
        {
            get; set;
        }

        public string PasswordHash
        {
            get; set;
        }

        public UserStatus Status
        {
            get; set;
        }

        public Guid[] Groups
        {
            get; set;
        } = new Guid[0];
    }
}
