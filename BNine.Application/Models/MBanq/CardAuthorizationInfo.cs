﻿namespace BNine.Application.Models.MBanq
{
    using Newtonsoft.Json;

    public class CardAuthorizationInfo
    {
        [JsonProperty("id")]
        public long Id
        {
            get; set;
        }

        [JsonProperty("status")]
        public string Status
        {
            get; set;
        }

        [JsonProperty("card")]
        public CardDetails Card
        {
            get; set;
        }
    }

    public class CardDetails
    {
        [JsonProperty("id")]
        public long Id
        {
            get; set;
        }

        [JsonProperty("primaryAccountNumber")]
        public string PrimaryAccountNumber
        {
            get; set;
        }
    }
}
