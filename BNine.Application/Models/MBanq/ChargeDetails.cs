﻿namespace BNine.Application.Models.MBanq
{
    public class ChargeDetails
    {
        public long Id
        {
            get; set;
        }

        public decimal Amount
        {
            get; set;
        }

        public decimal? MinCap
        {
            get; set;
        }

        public string Name
        {
            get; set;
        }
    }
}
