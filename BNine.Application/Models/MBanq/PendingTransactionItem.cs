﻿namespace BNine.Application.Models.MBanq;

using Constants;
using Enums.Transfers;
using Newtonsoft.Json;
using Transfers;

public record PendingTransactionItem : TransactionItemBase
{
    [JsonProperty("merchant")]
    public string Merchant
    {
        get;
        set;
    }

    [JsonIgnore]
    public string AccountNumber
    {
        get;
        set;
    }

    public Creditor Creditor
    {
        get;
        set;
    }

    public Debtor Debtor
    {
        get;
        set;
    }

    public string RawPaymentType
    {
        get;
        set;
    }

    public string Reference
    {
        get;
        set;
    }

    public PendingTransferDisplayType PaymentType => RawPaymentType switch
    {
        TransactionTypes.ACH => PendingTransferDisplayType.Ach,
        TransactionTypes.INTERNAL => PendingTransferDisplayType.Internal,
        TransactionTypes.ATM => PendingTransferDisplayType.Atm,
        "AUTH" => PendingTransferDisplayType.CardAuthorization,
        "RDC" => PendingTransferDisplayType.CheckCashing,
        "CARD" => PendingTransferDisplayType.PushToExternal,
        _ => PendingTransferDisplayType.Unknown,
    };

    public TransferDisplayType TransferDisplayType => InferTransferDisplayType();

    private TransferDisplayType InferTransferDisplayType()
        {
            return (PaymentType) switch
            {
                (PendingTransferDisplayType.Atm) => TransferDisplayType.Atm,
                (PendingTransferDisplayType.Ach) => TransferDisplayType.AchSent,
                (PendingTransferDisplayType.PushToExternal) => TransferDisplayType.PushToExternalCard,
                (PendingTransferDisplayType.Internal) => TransferDisplayType.SentToB9Client,

                _ => TransferDisplayType.OtherDefaultTransaction,
            };
        }
}
