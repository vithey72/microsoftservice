﻿namespace BNine.Application.Models.MBanq
{
    using System.Collections.Generic;
    using Newtonsoft.Json;

    // TODO: refactor this
    public class GraphQLResponse<T>
    {
        [JsonProperty("data")]
        public T Data
        {
            get; set;
        }

        [JsonProperty("errors")]
        public IEnumerable<Error> Errors
        {
            get; set;
        } = new List<Error>();
    }
}
