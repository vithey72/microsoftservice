﻿namespace BNine.Application.Models.MBanq
{
    public class ExternalCardListItem
    {
        public long ExternalId
        {
            get; set;
        }

        public string Last4Digits
        {
            get; set;
        }

        public string NetworkName
        {
            get; set;
        }
    }
}
