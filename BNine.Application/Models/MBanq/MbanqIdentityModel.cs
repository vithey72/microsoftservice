﻿namespace BNine.Application.Models.MBanq
{
    using Newtonsoft.Json;

    public class MbanqIdentityModel
    {
        [JsonProperty("id")]
        public int Id
        {
            get;
            set;
        }

        [JsonProperty("clientId")]
        public int ClientId
        {
            get;
            set;
        }

        [JsonProperty("documentType")]
        public DocType DocumentType
        {
            get;
            set;
        }

        /// <summary>
        /// SSN or ITIN number.
        /// </summary>
        [JsonProperty("documentKey")]
        public string DocumentKey
        {
            get;
            set;
        }

        [JsonProperty("status")]
        public string Status
        {
            get;
            set;
        }

        public class DocType
        {
            [JsonProperty("id")]
            public int Id
            {
                get;
                set;
            }

            [JsonProperty("name")]
            public string Name
            {
                get;
                set;
            }
        }
    }
}
