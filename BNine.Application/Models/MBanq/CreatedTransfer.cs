﻿namespace BNine.Application.Models.MBanq
{
    using Newtonsoft.Json;

    public class CreatedTransfer
    {
        [JsonProperty("id")]
        public int Id
        {
            get; set;
        }
    }
}
