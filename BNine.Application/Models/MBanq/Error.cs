﻿namespace BNine.Application.Models.MBanq
{
    using Newtonsoft.Json;

    public class Error
    {
        [JsonProperty("message")]
        public string Message
        {
            get; set;
        }

        [JsonProperty("extensions")]
        public Extensions Extensions
        {
            get; set;
        }
    }
}
