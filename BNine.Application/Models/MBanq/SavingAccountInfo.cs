﻿namespace BNine.Application.Models.MBanq
{
    public class SavingAccountInfo
    {
        public int Id
        {
            get; set;
        }

        public string AccountNumber
        {
            get; set;
        }

        public string Status
        {
            get; set;
        }

        public string Currency
        {
            get; set;
        }

        public decimal AvailableBalance
        {
            get; set;
        }

        public decimal AccountBalance
        {
            get; set;
        }

        public bool IsBlocked
        {
            get; set;
        }

        public double SavingsAmountOnHold
        {
            get;
            set;
        }

        public bool IsCardOrderingRestricted
        {
            get; set;
        }

        public decimal TotalDeposits
        {
            get;
            set;
        }

    }
}
