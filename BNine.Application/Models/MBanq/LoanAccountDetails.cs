﻿namespace BNine.Application.Models.MBanq
{
    public class LoanAccountDetails
    {
        public decimal DisbursedAmount
        {
            get; set;
        }

        public decimal OutstandingAmount
        {
            get; set;
        }

        public int ExternalId
        {
            get; set;
        }

        public bool Closed
        {
            get; set;
        }
    }
}
