﻿namespace BNine.Application.Models.MBanq
{
    using System.Collections.Generic;
    using BNine.Enums.Loans;

    public class LoanAccount
    {
        public int ClientId
        {
            get; set;
        }

        public int ProductId
        {
            get; set;
        }

        public double Principal
        {
            get; set;
        }

        public int LoanTermFrequency
        {
            get; set;
        }

        public TermFrequencyType LoanTermFrequencyType
        {
            get; set;
        }

        public int NumberOfRepayments
        {
            get; set;
        }

        public int RepaymentEvery
        {
            get; set;
        }

        public int RepaymentFrequencyType
        {
            get; set;
        }

        public double InterestRatePerPeriod
        {
            get; set;
        }

        public int AmortizationType
        {
            get; set;
        }

        public bool IsEqualAmortization
        {
            get; set;
        }

        public int InterestType
        {
            get; set;
        }

        public int InterestCalculationPeriodType
        {
            get; set;
        }

        public bool AllowPartialPeriodInterestCalculation
        {
            get; set;
        }

        public int TransactionProcessingStrategyId
        {
            get; set;
        }

        public string ExpectedDisbursementDate
        {
            get; set;
        }

        public int LinkAccountId
        {
            get; set;
        }

        public IEnumerable<DisbursementItemData> DisbursementData
        {
            get; set;
        }

        public int MaxOutstandingLoanBalance
        {
            get; set;
        }

        public bool CreateStandingInstructionAtDisbursement
        {
            get; set;
        }
    }

    public class DisbursementItemData
    {
        public string ExpectedDisbursementDate
        {
            get; set;
        }

        public int Principal
        {
            get; set;
        }
    }
}
