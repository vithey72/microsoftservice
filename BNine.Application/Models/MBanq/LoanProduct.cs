﻿namespace BNine.Application.Models.MBanq
{
    using AutoMapper;
    using BNine.Application.Mappings;
    using BNine.Common.Classes;

    public class LoanProduct : IMapTo<LoanAccount>
    {
        public int Id
        {
            get; set;
        }

        public string Status
        {
            get; set;
        }

        public double Principal
        {
            get; set;
        }

        public int NumberOfRepayments
        {
            get; set;
        }

        public int RepaymentEvery
        {
            get; set;
        }

        public IdValue RepaymentFrequencyType
        {
            get; set;
        }

        public double InterestRatePerPeriod
        {
            get; set;
        }

        public IdValue InterestRateFrequencyType
        {
            get; set;
        }

        public double AnnualInterestRate
        {
            get; set;
        }

        public IdValue AmortizationType
        {
            get; set;
        }

        public IdValue InterestType
        {
            get; set;
        }

        public IdValue InterestCalculationPeriodType
        {
            get; set;
        }

        public int TransactionProcessingStrategyId
        {
            get; set;
        }

        public void Mapping(Profile profile)
        {

            profile.CreateMap<LoanProduct, LoanAccount>()
                .ForMember(dst => dst.ProductId, opt => opt.MapFrom(src => src.Id))
                .ForMember(dst => dst.ProductId, opt => opt.MapFrom(src => src.Id))
                .ForMember(dst => dst.InterestCalculationPeriodType, opt => opt.MapFrom(src => src.InterestCalculationPeriodType.Id))
                .ForMember(dst => dst.InterestRatePerPeriod, opt => opt.MapFrom(src => src.InterestRatePerPeriod))
                .ForMember(dst => dst.InterestType, opt => opt.MapFrom(src => src.InterestType.Id))
                .ForMember(dst => dst.RepaymentFrequencyType, opt => opt.MapFrom(src => src.RepaymentFrequencyType.Id))
                .ForMember(dst => dst.RepaymentEvery, opt => opt.MapFrom(src => src.RepaymentEvery))
                .ForMember(dst => dst.NumberOfRepayments, opt => opt.MapFrom(src => src.NumberOfRepayments))
                .ForMember(dst => dst.TransactionProcessingStrategyId, opt => opt.MapFrom(src => src.TransactionProcessingStrategyId))
                .ForMember(dst => dst.AmortizationType, opt => opt.MapFrom(src => src.AmortizationType.Id))
                ;
        }
    }
}
