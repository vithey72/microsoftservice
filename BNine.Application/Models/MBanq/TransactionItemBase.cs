﻿namespace BNine.Application.Models.MBanq;

using Newtonsoft.Json;

public record TransactionItemBase
{
    [JsonProperty("id")]
    public int Id
    {
        get; set;
    }

    [JsonIgnore]
    public int TypeId
    {
        get; set;
    }

    [JsonProperty("type")]
    public string Type
    {
        get; set;
    }

    [JsonProperty("amount")]
    public decimal Amount
    {
        get;
        set;
    }

    [JsonProperty("date")]
    public DateTime CreatedAt
    {
        get; set;
    }

    [JsonIgnore]
    public int? MccCode
    {
        get;
        set;
    }

    [JsonIgnore]
    public string CardDigits
    {
        get;
        set;
    }
}
