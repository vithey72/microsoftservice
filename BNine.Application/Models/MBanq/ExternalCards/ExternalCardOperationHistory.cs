﻿namespace BNine.Application.Models.MBanq;

public class ExternalCardOperationHistory
{
    public int Id
    {
        get;
        set;
    }

    public string Message
    {
        get;
        set;
    }

    public string Action
    {
        get;
        set;
    }

    public DateTime CreatedAt
    {
        get;
        set;
    }

    /// <summary>
    /// Json string
    /// </summary>
    public string Request
    {
        get;
        set;
    }

    /// <summary>
    /// Json string
    /// </summary>
    public string Response
    {
        get;
        set;
    }

    public List<string> DuplicateCards
    {
        get;
        set;
    }
}
