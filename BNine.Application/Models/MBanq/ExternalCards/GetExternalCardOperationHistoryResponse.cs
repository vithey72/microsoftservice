﻿namespace BNine.Application.Models.MBanq;

public class GetExternalCardOperationHistoryResponse
{
    public int TotalFilteredRecords
    {
        get;
        set;
    }

    public List<ExternalCardOperationHistory> PageItems
    {
        get;
        set;
    }
}
