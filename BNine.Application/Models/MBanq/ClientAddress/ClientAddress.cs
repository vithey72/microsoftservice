﻿namespace BNine.Application.Models.MBanq;

public class ClientAddress
{
    public bool IsActive
    {
        get;
        set;
    }

    public int AddressId
    {
        get;
        set;
    }
}
