﻿namespace BNine.Application.Models.MBanq
{
    public class DocumentListItem
    {
        public string Id
        {
            get; set;
        }

        public string FileName
        {
            get; set;
        }

        public string Type
        {
            get; set;
        }
    }
}
