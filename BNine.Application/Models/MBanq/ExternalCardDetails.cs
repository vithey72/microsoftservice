﻿namespace BNine.Application.Models.MBanq
{
    public class ExternalCardDetails
    {
        public int Id
        {
            get; set;
        }

        public string LastDigits
        {
            get; set;
        }

        public string ExpiryDate
        {
            get; set;
        }

        public string Network
        {
            get; set;
        }

        public bool IsDeleted
        {
            get;
            set;
        }
    }
}
