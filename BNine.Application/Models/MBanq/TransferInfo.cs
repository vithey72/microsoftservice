﻿namespace BNine.Application.Models.MBanq
{
    using System;

    public class TransferInfo
    {
        public int Id
        {
            get; set;
        }

        public string Status
        {
            get; set;
        }

        public DateTime DateTime
        {
            get; set;
        }
    }
}
