﻿namespace BNine.Application.Models.MBanq
{
    public class SavingAccountTransactionInfo
    {
        public int Id
        {
            get; set;
        }

        public decimal Amount
        {
            get; set;
        }

        public decimal RunningBalance
        {
            get; set;
        }

        public int AccountId
        {
            get; set;
        }
    }
}
