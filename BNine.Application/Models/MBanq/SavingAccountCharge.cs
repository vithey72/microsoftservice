﻿namespace BNine.Application.Models.MBanq
{
    public class SavingAccountCharge
    {
        public int Id
        {
            get; set;
        }

        /// <summary>
        /// Id of a charge type entity that this charge is an instance of.
        /// </summary>
        public int ChargeId
        {
            get; set;
        }

        /// <summary>
        /// Date when the charge period ends and a new charge is collected if it's a reoccurring type.
        /// </summary>
        public DateTime? DueDate
        {
            get; set;
        }

        public decimal Amount
        {
            get; set;
        }

        public decimal AmountPaid
        {
            get; set;
        }

        public bool IsActive
        {
            get; set;
        }

        public decimal TotalDeferredChargeAmount
        {
            get; set;
        }

        public string ChargeTypeName
        {
            get; set;
        }
    }

}
