﻿namespace BNine.Application.Models.MBanq.Transfers
{
    using System;
    using System.Collections.Generic;
    using Enums.CheckCashing;
    using Newtonsoft.Json;
    using Newtonsoft.Json.Serialization;

    [JsonObject(NamingStrategyType = typeof(CamelCaseNamingStrategy))]
    public class CheckTransfer : Transfer
    {
        public string ExternalId
        {
            get;
            set;
        }

        public decimal InstantFunds
        {
            get;
            set;
        }

        public new List<string> Reference
        {
            get; set;
        }

        public CheckTransferDetails TransferDetails
        {
            get;
            set;
        }

        [JsonObject(NamingStrategyType = typeof(CamelCaseNamingStrategy))]
        public class CheckTransferDetails
        {
            public string CheckFrontImage
            {
                get;
                internal set;
            }
            public string CheckBackImage
            {
                get;
                internal set;
            }
        }
        public object RawPaymentDetails
        {
            get;
            set;
        }

        [JsonObject(NamingStrategyType = typeof(CamelCaseNamingStrategy))]
        public class CheckTransferRawPaymentDetails
        {
            public decimal Amount
            {
                get;
                set;
            }
            public string RawMicr
            {
                get;
                set;
            }
            public string PayeeName
            {
                get;
                set;
            }
            public string PayerName
            {
                get;
                set;
            }
            public string PayerAddress
            {
                get;
                set;
            }
            public DateTime CheckDate
            {
                get;
                set;
            }
            public string CheckNumber
            {
                get;
                set;
            }
            public string FrontProcessedImage
            {
                get;
                set;
            }
            public object RearProcessedImage
            {
                get;
                set;
            }
            public object Status
            {
                get;
                set;
            }
            public object Error
            {
                get;
                set;
            }
            public CheckType Kind
            {
                get;
                set;
            }
            public MicrType Micr
            {
                get;
                set;
            }
            public object Maker
            {
                get;
                set;
            }

            [JsonObject(NamingStrategyType = typeof(CamelCaseNamingStrategy))]
            public class MicrType
            {
                public string RawMicr
                {
                    get;
                    set;
                }
                public string ParseError
                {
                    get;
                    set;
                }
                public string StatusMessage
                {
                    get;
                    set;
                }
                public bool ContainsUnrecognizedSymbols
                {
                    get;
                    set;
                }
                public string CheckType
                {
                    get;
                    set;
                }
                public string AuxOnus
                {
                    get;
                    set;
                }
                public string Onus
                {
                    get;
                    set;
                }
                public string TransitNumber
                {
                    get;
                    set;
                }
                public object AccountNumber
                {
                    get;
                    set;
                }
                public string CheckNumber
                {
                    get;
                    set;
                }
                public decimal Amount
                {
                    get;
                    set;
                }
                public bool Special
                {
                    get;
                    set;
                }
            }
        }

        [JsonObject(NamingStrategyType = typeof(CamelCaseNamingStrategy))]
        public class Maker
        {
            public string Id
            {
                get;
                set;
            }
            public string AbaNumber
            {
                get;
                set;
            }
            public string AccountNumber
            {
                get;
                set;
            }
            public string Status
            {
                get;
                set;
            }
            public CheckType Kind
            {
                get;
                set;
            }
            public string Name
            {
                get;
                set;
            }
            public string Address1
            {
                get;
                set;
            }
            public string Address2
            {
                get;
                set;
            }
            public string City
            {
                get;
                set;
            }
            public string State
            {
                get;
                set;
            }
            public string Zip
            {
                get;
                set;
            }
            public string Phone
            {
                get;
                set;
            }
            public string AltPhone
            {
                get;
                set;
            }
            public string[] Notes
            {
                get;
                set;
            }
        }
    }
}
