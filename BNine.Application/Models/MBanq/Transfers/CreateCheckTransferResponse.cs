﻿namespace BNine.Application.Models.MBanq.Transfers
{
    using Newtonsoft.Json;

    public class CreateCheckTransferResponse
    {
        [JsonProperty("savingsId")]
        public string SavingsId
        {
            get; set;
        }

        [JsonProperty("resourceId")]
        public string ResourceId
        {
            get; set;
        }

        /// <summary>
        /// Databse Id of transaction that corresponds to the created transfer.
        /// </summary>
        [JsonProperty("resourceIdentifier")]
        public string ResourceIdentifier
        {
            get; set;
        }
    }
}
