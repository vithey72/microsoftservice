﻿namespace BNine.Application.Models.MBanq.Transfers
{
    using Newtonsoft.Json;

    public class Debtor
    {
        [JsonProperty("identifier")]
        public string Identifier
        {
            get; set;
        }

        [JsonProperty("name")]
        public string Name
        {
            get; set;
        }
    }
}
