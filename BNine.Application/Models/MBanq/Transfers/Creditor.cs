﻿namespace BNine.Application.Models.MBanq.Transfers
{
    using System.Collections.Generic;
    using Newtonsoft.Json;

    public class Creditor
    {
        [JsonProperty("identifier")]
        public string Identifier
        {
            get; set;
        }

        [JsonProperty("name")]
        public string Name
        {
            get; set;
        }

        [JsonProperty("address")]
        public IEnumerable<string> Address
        {
            get; set;
        }

        [JsonProperty("country")]
        public string Country
        {
            get; set;
        }

        [JsonProperty("city")]
        public string City
        {
            get; set;
        }

        [JsonProperty("postalCode")]
        public string PostalCode
        {
            get; set;
        }
    }
}
