﻿namespace BNine.Application.Models.MBanq.Transfers
{
    using System;
    using System.Collections.Generic;
    using Enums.Transfers;
    using Newtonsoft.Json;

    public class TransferData
    {
        [JsonProperty("id")]
        public int Id
        {
            get;
            set;
        }

        [JsonProperty("clientId")]
        public int? ClientExternalId
        {
            get;
            set;
        }

        [JsonProperty("creditorAccountId")]
        public int? CreditorAccountId
        {
            get; set;
        }

        [JsonProperty("inOrOut")]
        public string InOrOut
        {
            get; set;
        }

        [JsonProperty("amount")]
        public decimal Amount
        {
            get;
            set;
        }

        [JsonProperty("note")]
        public string Note
        {
            get;
            set;
        }

        [JsonProperty("status")]
        public string Status
        {
            get;
            set;
        }

        [JsonProperty("reference")]
        public IEnumerable<string> Reference
        {
            get;
            set;
        }

        [JsonProperty("created")]
        public DateTime Created
        {
            get;
            set;
        }

        [JsonProperty("transferType")]
        public TransferType TransferType
        {
            get;
            set;
        }

        [JsonProperty("direction")]
        public TransferDirection Direction
        {
            get;
            set;
        }

        [JsonProperty("debtor")]
        public Debtor Debtor
        {
            get;
            set;
        }

        [JsonProperty("creditor")]
        public Creditor Creditor
        {
            get;
            set;
        }

        [JsonProperty("correlationId")]
        public Guid CorrelationId
        {
            get;
            set;
        }

        [JsonProperty("savingsAccountTransferExternalId")]
        public int SavingsAccountTransferExternalId
        {
            get;
            set;
        }
    }
}
