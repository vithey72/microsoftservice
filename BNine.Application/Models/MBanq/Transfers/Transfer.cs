﻿namespace BNine.Application.Models.MBanq.Transfers
{
    using Enums;
    using Enums.Transfers;
    using Newtonsoft.Json;
    using Newtonsoft.Json.Serialization;

    [JsonObject(NamingStrategyType = typeof(CamelCaseNamingStrategy))]
    public class Transfer
    {
        public TransferDirection Type
        {
            get; set;
        }

        public TransferType PaymentType
        {
            get; set;
        }

        public AchAccountType? ReceiverAccountType
        {
            get; set;
        }

        public string Currency
        {
            get; set;
        } = "USD"; // TODO: hardcode. while we support only USD transfers


        public decimal Amount
        {
            get; set;
        }

        public string Reference
        {
            get; set;
        }

        public string DateFormat
        {
            get; set;
        } = "YYYY-MM-DD"; // TODO: hardcode. needs to use the same format everywhere and move this constant to specific constants library.

        public Counterparty Debtor // TODO: model
        {
            get; set;
        }

        public Counterparty Creditor // TODO: model
        {
            get; set;
        }

    }
}
