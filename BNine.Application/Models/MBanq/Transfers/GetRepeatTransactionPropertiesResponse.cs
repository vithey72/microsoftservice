﻿namespace BNine.Application.Models.MBanq.Transfers;

using System.Runtime.Serialization;
using Newtonsoft.Json;

public class GetRepeatTransactionPropertiesResponse
{
    [JsonProperty("type")]
    public RepeatTransactionType Type
    {
        get;
        set;
    }

    [JsonProperty("ach")]
    public AchRepeatTransferDetails Ach
    {
        get;
        set;
    }

    [JsonProperty("internal")]
    public InternalRepeatTransferDetails Internal
    {
        get;
        set;
    }

    [JsonProperty("pushToExternalCard")]
    public PushToExternalCardTransferDetails PushToExternalCard
    {
        get;
        set;
    }

    [JsonProperty("amount")]
    public decimal Amount
    {
        get;
        set;
    }

    public enum RepeatTransactionType
    {
        /// <summary>
        /// Default value.
        /// </summary>
        Unknown = 0,

        /// <summary>
        /// ACH outgoing transfer.
        /// </summary>
        [EnumMember(Value = "ach")]
        Ach = 1,

        /// <summary>
        /// Transfer between B9 members.
        /// </summary>
        [EnumMember(Value = "internal")]
        Internal = 2,

        /// <summary>
        /// Transfer from B9 card to external card.
        /// </summary>
        [EnumMember(Value = "pushToExternalCard")]
        PushToExternalCard = 3,
    }

    public class InternalRepeatTransferDetails
    {
        [JsonProperty("recipientFullName")]
        public string RecipientFullName
        {
            get;
            set;
        }

        [JsonProperty("recipientPhoneNumber")]
        public string RecipientPhoneNumber
        {
            get;
            set;
        }
    }

    public class AchRepeatTransferDetails
    {
        [JsonProperty("routingNumber")]
        public string RoutingNumber
        {
            get;
            set;
        }

        [JsonProperty("accountNumber")]
        public string AccountNumber
        {
            get;
            set;
        }

        [JsonProperty("recipientFullName")]
        public string RecipientFullName
        {
            get;
            set;
        }

        [JsonProperty("address")]
        public string Address
        {
            get;
            set;
        }
    }

    public class PushToExternalCardTransferDetails
    {
        [JsonProperty("externalCardId")]
        public long ExternalCardId
        {
            get; set;
        }
    }
}
