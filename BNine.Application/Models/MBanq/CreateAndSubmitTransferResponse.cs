﻿namespace BNine.Application.Models.MBanq
{
    using Newtonsoft.Json;

    public class CreateAndSubmitTransferResponse
    {
        [JsonProperty("createAndSubmitTransfer")]
        public CreatedTransfer Transfer
        {
            get; set;
        }
    }
}
