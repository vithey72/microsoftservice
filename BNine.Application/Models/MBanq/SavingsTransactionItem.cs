﻿namespace BNine.Application.Models.MBanq
{
    using BNine.Enums.Transfers;
    using Constants;
    using Transfers;

    public record SavingsTransactionItem : TransactionItemBase
    {
        public bool Reversed
        {
            get; set;
        }

        public TransferDirection Direction
        {
            get; set;
        }

        public decimal RunningBalance
        {
            get; set;
        }

        public string Counterparty
        {
            get; set;
        }

        public string Note
        {
            get;
            set;
        }

        public string UserNote
        {
            get;
            set;
        }

        public Creditor Creditor
        {
            get;
            set;
        }

        public Debtor Debtor
        {
            get;
            set;
        }

        public int? RawType
        {
            get; set;
        }

        public int? RawSubType
        {
            get; set;
        }

        public string Reference
        {
            get; set;
        }

        public string AccountDigits
        {
            get;
            set;
        }

        /// <summary>
        /// Time of card authorization, or other operations that take time to complete.
        /// </summary>
        public DateTime InitiatedAt
        {
            get; set;
        }

        public TransferDisplayType TransferDisplayType => InferTransferDisplayType();

        public int GetExternalCardId() => Convert.ToInt32(Creditor?.Identifier?.Split(':').LastOrDefault() ?? "-1");

        private TransferDisplayType InferTransferDisplayType()
        {
            var type = (MbanqTransferType?)RawType;
            var subType = (MbanqTransferSubtype?)RawSubType;

            return (type, subType, Type) switch
            {
                (MbanqTransferType.DEPOSIT, MbanqTransferSubtype.LOAN_DISBURSEMENT, _) => TransferDisplayType.AdvanceIssued,
                (MbanqTransferType.DEPOSIT, MbanqTransferSubtype.CARD_AUTHORIZE_PAYMENT, _) => TransferDisplayType.ReceivedTransferToCard,
                (MbanqTransferType.DEPOSIT, MbanqTransferSubtype.SETTLEMENT_RETURN_CREDIT, _) => TransferDisplayType.Returned,
                (MbanqTransferType.DEPOSIT, _, "IDK") => TransferDisplayType.CheckCashing, // TODO: not sure how to infer it
                (MbanqTransferType.DEPOSIT, _, TransactionTypes.ACH) => TransferDisplayType.AchReceived,
                (MbanqTransferType.DEPOSIT, _, TransactionTypes.INTERNAL)
                    when (Note is not null && Note.Contains(TransactionReferenceTexts.CashbackReferenceText))
                    || (UserNote is not null && UserNote.Contains(TransactionReferenceTexts.CashbackReferenceText))
                    => TransferDisplayType.B9Cashback,
                (MbanqTransferType.DEPOSIT, _, TransactionTypes.INTERNAL)
                    when Note is not null && (Note.Contains(TransactionReferenceTexts.ReferralReferenceText) ||
                                              Note.Contains(TransactionReferenceTexts.ReferralBonusText)) => TransferDisplayType.B9Bonus,
                (MbanqTransferType.DEPOSIT, _, TransactionTypes.INTERNAL) when Debtor?.Name == Creditor?.Name => TransferDisplayType.Rewards,
                (MbanqTransferType.DEPOSIT, _, TransactionTypes.INTERNAL) => TransferDisplayType.ReceivedFromB9Client,
                (MbanqTransferType.DEPOSIT, _, TransactionTypes.EXTERNAL_CARD) => TransferDisplayType.PullFromExternalCard,
                (MbanqTransferType.DEPOSIT, _, _) => TransferDisplayType.DefaultDeposit,

                (MbanqTransferType.WITHDRAWAL, MbanqTransferSubtype.CARD_TRANSACTION, TransactionTypes.ATM) => TransferDisplayType.Atm,
                (MbanqTransferType.WITHDRAWAL, MbanqTransferSubtype.CARD_TRANSACTION, _) => TransferDisplayType.ApprovedCardAuth,
                (MbanqTransferType.WITHDRAWAL, MbanqTransferSubtype.LOAN_REPAYMENT, _) => TransferDisplayType.AdvanceRepayment,
                (MbanqTransferType.WITHDRAWAL, _, TransactionTypes.ATM) => TransferDisplayType.Atm,
                (MbanqTransferType.WITHDRAWAL, _, TransactionTypes.ACH) => TransferDisplayType.AchSent,
                (MbanqTransferType.WITHDRAWAL, _, TransactionTypes.INTERNAL) => TransferDisplayType.SentToB9Client,
                (MbanqTransferType.WITHDRAWAL, _, TransactionTypes.EXTERNAL_CARD) => TransferDisplayType.PushToExternalCard,
                (MbanqTransferType.WITHDRAWAL, _, _) => TransferDisplayType.DefaultWithdrawal,

                (MbanqTransferType.PAY_CHARGE, MbanqTransferSubtype.DOMESTIC_ATM_WITHDRAWAL_FEE or
                    MbanqTransferSubtype.INTERNATIONAL_ATM_WITHDRAWAL_FEE, _) => TransferDisplayType.AtmFee,
                (MbanqTransferType.PAY_CHARGE, MbanqTransferSubtype.INTERNATIONAL_TRANSACTION_FEE, _) => TransferDisplayType.ForeignTransactionFee,
                (MbanqTransferType.PAY_CHARGE, MbanqTransferSubtype.EXTERNAL_CARD_PUSH_TRANSACTION_FEE or
                    MbanqTransferSubtype.EXTERNAL_CARD_PULL_TRANSACTION_FEE, _) => TransferDisplayType.CardTransferFee,
                (MbanqTransferType.PAY_CHARGE, MbanqTransferSubtype.MCC_CHARGE, _) => TransferDisplayType.ExternalWalletFee,
                (MbanqTransferType.PAY_CHARGE, null, TransactionTypes.PAY_CHARGE) => TransferDisplayType.MonthlyB9Fee,
                (MbanqTransferType.ANNUAL_FEE, null, TransactionTypes.PAY_CHARGE) => TransferDisplayType.AnnualFee,
                (MbanqTransferType.PAY_CHARGE, null, TransactionTypes.VIP_SUPPORT) => TransferDisplayType.VipSupportFee,
                (MbanqTransferType.PAY_CHARGE, null, TransactionTypes.ADVANCE_EXTENSION_FEE) => TransferDisplayType.AdvanceUnfreezeFee,
                (MbanqTransferType.PAY_CHARGE, null, TransactionTypes.ADVANCE_BOOST_FEE) => TransferDisplayType.AdvanceBoostFee,
                (MbanqTransferType.PAY_CHARGE, null, TransactionTypes.AFTER_ADVANCE_TIP) => TransferDisplayType.AfterAdvanceTip,
                (MbanqTransferType.PAY_CHARGE, null, TransactionTypes.CREDIT_SCORE_FEE) => TransferDisplayType.CreditScoreFee,
                (MbanqTransferType.PAY_CHARGE, null, TransactionTypes.PHYSICAL_CARD_EMBOSSING_FEE or TransactionTypes.PHYSICAL_CARD_REPLACEMENT_FEE)
                    => TransferDisplayType.PhysicalCardDeliveryFee,
                (MbanqTransferType.PAY_CHARGE, _, _) => TransferDisplayType.DefaultFee,
                (MbanqTransferType.PAY_CHARGE_REVERSAL, _, _) => TransferDisplayType.ReturnedPaycharge,

                (MbanqTransferType.INVALID, MbanqTransferSubtype.NONE, _) => TransferDisplayType.OtherCardTransaction,
                (null, null, TransactionTypes.REJECTED) => TransferDisplayType.Declined,

                _ => TransferDisplayType.OtherDefaultTransaction,
            };
        }
    }
}
