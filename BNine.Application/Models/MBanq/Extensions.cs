﻿namespace BNine.Application.Models.MBanq
{
    using Newtonsoft.Json;

    public class Extensions
    {
        [JsonProperty("code")]
        public string Code
        {
            get; set;
        }

        [JsonProperty("globalisationMessageCode")]
        public string GlobalisationMessageCode
        {
            get; set;
        }

        [JsonProperty("defaultUserMessage")]
        public string DefaultUserMessage
        {
            get; set;
        }
    }
}
