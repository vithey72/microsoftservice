﻿namespace BNine.Application.Models.MBanq
{
    using Newtonsoft.Json;
    using Newtonsoft.Json.Serialization;

    [JsonObject(NamingStrategyType = typeof(CamelCaseNamingStrategy))]
    public class Counterparty
    {
        public string Identifier
        {
            get; set;
        }

        public string Name
        {
            get; set;
        }

        public string Address
        {
            get; set;
        }

        /// <summary>
        /// Counterparty country 2 digits code.
        /// </summary>
        public string Country
        {
            get; set;
        }

        public string City
        {
            get; set;
        }

        public string PostalCode
        {
            get; set;
        }
    }
}
