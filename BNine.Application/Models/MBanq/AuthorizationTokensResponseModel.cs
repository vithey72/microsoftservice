﻿namespace BNine.Application.Models.MBanq
{
    using Newtonsoft.Json;

    public class AuthorizationTokensResponseModel
    {
        [JsonProperty("access_token")]
        public string AccessToken
        {
            get; set;
        }

        [JsonProperty("refresh_token")]
        public string RefreshToken
        {
            get; set;
        }
    }
}
