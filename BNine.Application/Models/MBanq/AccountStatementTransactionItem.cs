﻿namespace BNine.Application.Models.MBanq;

public record AccountStatementTransactionItem : SavingsTransactionItem;
