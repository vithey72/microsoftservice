﻿namespace BNine.Application.Models.MBanq
{
    public class ClientIdentifierType
    {
        public int ExternalId
        {
            get; set;
        }

        public string Name
        {
            get; set;
        }
    }
}
