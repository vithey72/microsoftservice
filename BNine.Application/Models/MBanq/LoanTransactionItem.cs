﻿namespace BNine.Application.Models.MBanq
{
    using System;

    public class LoanTransactionItem
    {
        public int Id
        {
            get; set;
        }

        public string Type
        {
            get; set;
        }

        public double Amount
        {
            get; set;
        }

        public DateTime Date
        {
            get; set;
        }
    }
}
