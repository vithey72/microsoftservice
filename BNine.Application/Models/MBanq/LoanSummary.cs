﻿namespace BNine.Application.Models.MBanq
{
    using System;

    public class LoanSummary
    {
        public int Id
        {
            get; set;
        }

        public double? Available
        {
            get; set;
        }

        public double? Total
        {
            get; set;
        }

        public double InterestRate
        {
            get; set;
        }

        public double? InterestPaid
        {
            get; set;
        }

        public double? InterestCharged
        {
            get; set;
        }

        public bool ClosedObligationsMet
        {
            get; set;
        }

        public string Status
        {
            get; set;
        }

        public int StatusId
        {
            get; set;
        }

        public double ApprovedPrincipal
        {
            get; set;
        }

        public DateTime? MaturityDate
        {
            get; set;
        }

        public double PrincipalOutstandingAmount
        {
            get;
            set;
        }

        public decimal FeeAmount
        {
            get;
            set;
        }

        public decimal FeeOutstandingAmount
        {
            get;
            set;
        }

        [Obsolete("Use IsClosedById() instead.")]
        public bool IsClosed() => Status.Trim().StartsWith("closed", StringComparison.OrdinalIgnoreCase);
        public bool IsClosedById() => StatusId is 600 or 601;
    }
}
