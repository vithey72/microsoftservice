﻿namespace BNine.Application.Models
{
    public class FileBytes
    {
        public FileBytes(byte[] bytes, string name, string contentType)
        {
            Bytes = bytes;
            FileName = name;
            ContentType = contentType;
        }

        public byte[] Bytes
        {
            get; set;
        }

        public string FileName
        {
            get; set;
        }

        public string ContentType
        {
            get; set;
        }
    }
}
