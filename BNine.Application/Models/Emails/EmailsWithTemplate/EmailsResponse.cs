﻿namespace BNine.Application.Models.Emails.EmailsWithTemplate
{
    using System.Collections.Generic;

    public class EmailsResponse
    {
        public bool IsSucceeded { get; set; } = true;

        public List<string> FailedFor { get; set; } = new List<string>();
    }
}
