﻿namespace BNine.Application.Models.Emails.EmailsWithTemplate.InviteFriends
{
    using Newtonsoft.Json;

    public class InviteFriendsTemplateData : BaseTemplateData
    {
        [JsonProperty("name")]
        public string Name
        {
            get; set;
        }

        [JsonProperty("code")]
        public string Code
        {
            get; set;
        }
    }
}
