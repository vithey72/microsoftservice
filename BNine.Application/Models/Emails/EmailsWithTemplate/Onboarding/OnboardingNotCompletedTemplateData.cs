﻿namespace BNine.Application.Models.Emails.EmailsWithTemplate.Onboarding
{
    using Newtonsoft.Json;

    public class OnboardingNotCompletedTemplateData : BaseTemplateData
    {
        [JsonProperty("name")]
        public string Name
        {
            get; set;
        }

        [JsonProperty("link")]
        public string Link
        {
            get; set;
        }
    }
}
