﻿namespace BNine.Application.Models.Emails.EmailsWithTemplate.Registration
{
    using Newtonsoft.Json;

    public class RegistrationDeniedTemplateData : BaseTemplateData
    {
        [JsonProperty("name")]
        public string Name
        {
            get; set;
        }
    }
}
