﻿namespace BNine.Application.Models.Emails.EmailsWithTemplate.Registration
{
    using Newtonsoft.Json;

    public class RegistrationFinishedTemplateData : BaseTemplateData
    {
        [JsonProperty("name")]
        public string Name
        {
            get; set;
        }

        [JsonProperty("link")]
        public string Link
        {
            get; set;
        }
    }
}
