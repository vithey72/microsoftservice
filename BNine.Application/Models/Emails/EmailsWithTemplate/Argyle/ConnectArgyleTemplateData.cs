﻿namespace BNine.Application.Models.Emails.EmailsWithTemplate.Argyle
{
    using Newtonsoft.Json;

    public class ConnectArgyleTemplateData : BaseTemplateData
    {
        [JsonProperty("name")]
        public string Name
        {
            get; set;
        }

        [JsonProperty("link")]
        public string Link
        {
            get; set;
        }
    }
}
