﻿namespace BNine.Application.Models.Emails.EmailsWithTemplate.User
{
    using Newtonsoft.Json;

    public class AddressUpdatedTemplateData : BaseTemplateData
    {
        [JsonProperty("name")]
        public string Name
        {
            get;
            set;
        }
    }
}
