﻿namespace BNine.Application.Models.Emails.EmailsWithTemplate.User
{
    using Newtonsoft.Json;

    public class ConfirmEmailUpdateTemplateData : BaseTemplateData
    {
        [JsonProperty("link")]
        public string Link
        {
            get;
            set;
        }

        [JsonProperty("name")]
        public string Name
        {
            get;
            set;
        }
    }
}
