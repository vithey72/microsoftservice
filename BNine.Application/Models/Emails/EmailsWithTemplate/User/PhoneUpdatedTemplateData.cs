﻿namespace BNine.Application.Models.Emails.EmailsWithTemplate.User
{
    using Newtonsoft.Json;

    public class PhoneUpdatedTemplateData : BaseTemplateData
    {
        [JsonProperty("name")]
        public string Name
        {
            get;
            set;
        }
    }
}
