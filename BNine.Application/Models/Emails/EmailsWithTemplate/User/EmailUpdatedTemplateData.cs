﻿namespace BNine.Application.Models.Emails.EmailsWithTemplate.User
{
    using Newtonsoft.Json;

    public class EmailUpdatedTemplateData : BaseTemplateData
    {
        [JsonProperty("name")]
        public string Name
        {
            get;
            set;
        }
    }
}
