﻿namespace BNine.Application.Models.Emails.EmailsWithTemplate.User
{
    using Newtonsoft.Json;

    public class AccountUnblockedTemplateData : BaseTemplateData
    {
        [JsonProperty("name")]
        public string Name
        {
            get;
            set;
        }

        [JsonProperty("accountNumber")]
        public string AccountNumber
        {
            get;
            set;
        }
    }
}
