﻿namespace BNine.Application.Models.Emails.EmailsWithTemplate
{
    using Newtonsoft.Json;

    public class EmailAttachmentData
    {
        public EmailAttachmentData(string name, byte[] data)
        {
            Name = name;
            Data = data;
        }

        [JsonProperty("Name")]
        public string Name
        {
            get;
            set;
        }

        [JsonProperty("Data")]
        public byte[] Data
        {
            get;
            set;
        }
    }
}
