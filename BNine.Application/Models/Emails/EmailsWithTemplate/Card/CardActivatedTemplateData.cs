﻿namespace BNine.Application.Models.Emails.EmailsWithTemplate.Card
{
    using Newtonsoft.Json;

    public class CardActivatedTemplateData : BaseTemplateData
    {
        [JsonProperty("name")]
        public string Name
        {
            get; set;
        }
    }
}
