﻿namespace BNine.Application.Models.Emails.EmailsWithTemplate.Card
{
    using Newtonsoft.Json;

    public class CardShippedTemplateData : BaseTemplateData
    {
        [JsonProperty("first_name")]
        public string FirstName
        {
            get;
            set;
        }
    }
}
