﻿namespace BNine.Application.Models.Emails.EmailsWithTemplate.Card
{
    using Newtonsoft.Json;

    public class CardBlockedTemplateData : BaseTemplateData
    {
        [JsonProperty("name")]
        public string Name
        {
            get;
            set;
        }

        [JsonProperty("cardNumber")]
        public string CardNumber
        {
            get;
            set;
        }
    }
}
