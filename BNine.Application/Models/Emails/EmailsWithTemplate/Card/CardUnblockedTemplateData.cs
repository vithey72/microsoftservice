﻿namespace BNine.Application.Models.Emails.EmailsWithTemplate.Card
{
    using Newtonsoft.Json;

    public class CardUnblockedTemplateData : BaseTemplateData
    {
        [JsonProperty("name")]
        public string Name
        {
            get;
            set;
        }

        [JsonProperty("cardNumber")]
        public string CardNumber
        {
            get;
            set;
        }
    }
}
