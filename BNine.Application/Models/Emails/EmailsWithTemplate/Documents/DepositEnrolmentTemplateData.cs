﻿namespace BNine.Application.Models.Emails.EmailsWithTemplate.Documents
{
    using Newtonsoft.Json;

    public class DepositEnrolmentTemplateData : BaseTemplateData
    {
        [JsonProperty("name")]
        public string Name
        {
            get;
            set;
        }
    }
}
