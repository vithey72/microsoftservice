﻿namespace BNine.Application.Models.Emails.EmailsWithTemplate.Documents
{
    using Newtonsoft.Json;

    public class SpendingAccountStatementData : BaseTemplateData
    {
        [JsonProperty("name")]
        public string Name
        {
            get;
            set;
        }
    }
}
