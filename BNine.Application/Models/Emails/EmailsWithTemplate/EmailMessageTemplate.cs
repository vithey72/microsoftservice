﻿namespace BNine.Application.Models.Emails.EmailsWithTemplate
{
    using System.Collections.Generic;
    using Newtonsoft.Json;

    public class EmailMessageTemplate
    {
        [JsonProperty("templateData")]
        public BaseTemplateData TemplateData
        {
            get; set;
        }

        [JsonProperty("templateId")]
        public string TemplateId
        {
            get; set;
        }

        [JsonProperty("recipientEmail")]
        public string RecipientEmail
        {
            get; set;
        }

        [JsonProperty("asmGroupId")]
        public int AsmGroupId
        {
            get; set;
        }

        public IEnumerable<EmailAttachmentData> Attachments
        {
            get;
            set;
        } = new List<EmailAttachmentData>();
    }
}
