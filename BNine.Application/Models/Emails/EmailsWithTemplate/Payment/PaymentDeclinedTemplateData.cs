﻿namespace BNine.Application.Models.Emails.EmailsWithTemplate.Payment
{
    using Newtonsoft.Json;

    public class PaymentDeclinedTemplateData : BaseTemplateData
    {
        [JsonProperty("billing_date")]
        public string BillingDate
        {
            get;
            set;
        }

        [JsonProperty("fee_size")]
        public string FeeSize
        {
            get;
            set;
        }
    }
}
