﻿namespace BNine.Application.Models.DwhIntegration.LogAdminAction
{
    public class AdminActionLogEntryChange
    {
        public string OldValue
        {
            get; set;
        }

        public string NewValue
        {
            get; set;
        }

        public string ColumnName
        {
            get; set;
        }
    }
}
