﻿namespace BNine.Application.Models.Argyle;

using Newtonsoft.Json;

/// <summary>
/// Response from /v1/link-items.
/// </summary>
public class ArgyleLinkItemsArray
{
    [JsonProperty("results")]
    public ArgyleLinkItem[] Items
    {
        get;
        set;
    }
}
