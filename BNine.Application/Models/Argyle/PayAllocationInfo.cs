﻿namespace BNine.Application.Models.Argyle
{
    using System;

    public class PayAllocationInfo
    {
        public string Status
        {
            get; set;
        }

        public string AllocationType
        {
            get; set;
        }

        public string AllocationValue
        {
            get; set;
        }

        public Guid ArgyleUserId
        {
            get; set;
        }

        public string AccountNumber
        {
            get; set;
        }

        public string RoutingNumber
        {
            get; set;
        }
    }
}
