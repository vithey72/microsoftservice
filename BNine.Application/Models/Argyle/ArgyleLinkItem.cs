﻿namespace BNine.Application.Models.Argyle;

using System;
using Newtonsoft.Json;

public class ArgyleLinkItem
{
    public string Id
    {
        get;
        set;
    }

    [JsonProperty("item_id")]
    public Guid ItemId
    {
        get;
        set;
    }

    public string Kind
    {
        get;
        set;
    }

    public string Name
    {
        get;
        set;
    }

    [JsonProperty("logo_url")]
    public string LogoUrl
    {
        get;
        set;
    }
}
