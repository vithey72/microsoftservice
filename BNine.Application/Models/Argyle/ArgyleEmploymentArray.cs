﻿namespace BNine.Application.Models.Argyle;

using Newtonsoft.Json;

/// <summary>
/// Response from /v1/employments model.
/// </summary>
public class ArgyleEmploymentArray
{
    [JsonProperty("results")]
    public ArgyleEmployment[] Employments
    {
        get;
        set;
    }
}
