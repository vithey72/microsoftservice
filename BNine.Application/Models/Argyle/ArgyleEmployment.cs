﻿namespace BNine.Application.Models.Argyle;

using System;
using Enums;
using Newtonsoft.Json;

/// <summary>
/// A projection of an Employment entity in Argyle.
/// See example value here https://docs.argyle.com/guides/reference/employments
/// </summary>
public class ArgyleEmployment
{
    public Guid Id
    {
        get;
        set;
    }

    public ArgyleStatus Status
    {
        get;
        set;
    }

    public string Employer
    {
        get;
        set;
    }

    [JsonProperty("base_pay")]
    public BasePay Salary
    {
        get;
        set;
    }

    [JsonProperty("termination_datetime")]
    public DateTime? TerminationDatetime
    {
        get;
        set;
    }

    public class BasePay
    {
        public decimal? Amount
        {
            get;
            set;
        }

        public string Period
        {
            get;
            set;
        }

        public string Currency
        {
            get;
            set;
        }
    }
}
