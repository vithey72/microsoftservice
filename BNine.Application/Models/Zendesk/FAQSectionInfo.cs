﻿namespace BNine.Application.Models.Zendesk
{
    using Newtonsoft.Json;

    public class FAQSectionInfo
    {
        [JsonProperty("id")]
        public long Id
        {
            get; set;
        }

        [JsonProperty("name")]
        public string Name
        {
            get; set;
        }

        [JsonProperty("questions")]
        public List<FAQCategoryInfo> Questions
        {
            get; set;
        }
    }
}
