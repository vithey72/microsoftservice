﻿namespace BNine.Application.Models.Zendesk
{
    using Newtonsoft.Json;

    public class FAQSections
    {
        [JsonProperty("sections")]
        public List<FAQSectionInfo> Sections
        {
            get; set;
        }
    }
}
