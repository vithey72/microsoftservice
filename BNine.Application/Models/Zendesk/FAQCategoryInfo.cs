﻿namespace BNine.Application.Models.Zendesk
{
    using Newtonsoft.Json;

    public class FAQCategoryInfo
    {
        [JsonProperty("id")]
        public long Id
        {
            get; set;
        }

        [JsonProperty("title")]
        public string Title
        {
            get; set;
        }

        [JsonProperty("answerPageUrl")]
        public string AnswerPageUrl
        {
            get; set;
        }
    }
}
