﻿namespace BNine.Application.Models.Zendesk
{
    [Obsolete]
    public class FAQCategory
    {
        public string Title
        {
            get; set;
        }

        public string Body
        {
            get; set;
        }
    }
}
