﻿namespace BNine.Application.Models.BonusCodes
{
    using System;
    using AutoMapper;
    using BNine.Application.Mappings;
    using BNine.Domain.Entities.BonusCodes;

    public class BonusCodeActivationModel : IMapFrom<BonusCodeActivation>
    {
        public BonusCodeActivationModel()
        {
            UpdatedAt = DateTime.UtcNow;
        }

        public Guid Id
        {
            get;
            set;
        }

        public Guid BonusCodeId
        {
            get;
            set;
        }

        public Guid ReceiverId
        {
            get;
            set;
        }

        public DateTime CreatedAt
        {
            get;
            set;
        }

        public DateTime? UpdatedAt
        {
            get;
            set;
        }

        public bool ReceiverTransferSuccess
        {
            get;
            set;
        }

        public bool SenderTransferSuccess
        {
            get;
            set;
        }

        public void Mapping(Profile profile) => profile.CreateMap<BonusCodeActivation, BonusCodeActivationModel>()
            .ForMember(d => d.Id, o => o.MapFrom(s => s.Id))
            .ForMember(d => d.BonusCodeId, o => o.MapFrom(s => s.BonusCodeId))
            .ForMember(d => d.UpdatedAt, o => o.MapFrom(s => s.UpdatedAt))
            .ForMember(d => d.CreatedAt, o => o.MapFrom(s => s.CreatedAt))
            .ForMember(d => d.ReceiverTransferSuccess, o => o.MapFrom(s => s.ReceiverTransferSuccess))
            .ForMember(d => d.ReceiverId, o => o.MapFrom(s => s.ReceiverId))
            .ForMember(d => d.SenderTransferSuccess, o => o.MapFrom(s => s.SenderTransferSuccess));
    }
}
