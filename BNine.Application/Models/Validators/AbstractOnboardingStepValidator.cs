﻿namespace BNine.Application.Models.Validators
{
    using System.Linq;
    using BNine.Application.Interfaces;
    using BNine.Domain.Events.Users;
    using BNine.Enums;
    using FluentValidation;
    using FluentValidation.Results;
    using Newtonsoft.Json;

    public abstract class AbstractOnboardingStepValidator<T> : AbstractValidator<T>
    {
        protected AbstractOnboardingStepValidator(IBNineDbContext dbContext, ICurrentUserService currentUserService)
        {
            DbContext = dbContext;
            CurrentUserService = currentUserService;
        }

        protected IBNineDbContext DbContext
        {
            get;
        }

        protected ICurrentUserService CurrentUserService
        {
            get;
        }

        protected UserStatus Step
        {
            get; set;
        }

        protected UserStatusDetailed StepDetailed
        {
            get; set;
        }

        public override ValidationResult Validate(ValidationContext<T> context)
        {
            var result = base.Validate(context);

            if (!result.IsValid)
            {
                var user = DbContext.Users
                    .FirstOrDefault(x => x.Id == CurrentUserService.UserId);

                var errors = result.Errors
                    .Select(x => $"Request: {JsonConvert.SerializeObject(context.InstanceToValidate)}, PropertyName: {x.PropertyName}, ErrorMessage: {x.ErrorMessage}");

                user.DomainEvents.Add(new OnboardingStepFailedEvent(user, Step, StepDetailed, errors));

                DbContext.SaveChanges();
            }

            return result;
        }
    }
}
