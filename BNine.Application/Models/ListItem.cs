﻿namespace BNine.Application.Models
{
    using System.Collections.Generic;
    using AutoMapper;
    using BNine.Application.Interfaces;
    using BNine.Application.Mappings;
    using BNine.Domain.Infrastructure;
    using Domain.Entities.Common;
    using Newtonsoft.Json;

    public abstract class ListItem<TDal, TApp> : IMapFrom<TDal>, IListItem
        where TDal : BaseListItem
        where TApp : IListItem
    {
        [JsonProperty("key")]
        public string Key
        {
            get; set;
        }

        [JsonProperty("value")]
        public ICollection<LocalizedText> Value
        {
            get; set;
        }

        public void Mapping(Profile profile)
        {
            profile.CreateMap<TDal, TApp>()
                .ForMember(dst => dst.Key, opt => opt.MapFrom(src => src.Key))
                .ForMember(dst => dst.Value, opt => opt.MapFrom(src => src.Value));
        }
    }
}
