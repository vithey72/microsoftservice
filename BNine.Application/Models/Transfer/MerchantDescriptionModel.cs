﻿namespace BNine.Application.Models.Transfer
{
    public class MerchantDescriptionModel
    {
        public string Icon
        {
            get; set;
        }

        public string Name
        {
            get; set;
        }

        public string PictureUrl
        {
            get; set;
        }
    }
}
