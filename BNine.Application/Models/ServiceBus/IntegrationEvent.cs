﻿namespace BNine.Application.Models.ServiceBus
{
    using System.Collections.Generic;

    public class IntegrationEvent
    {
        public string TopicName
        {
            get; set;
        }

        public string EventName
        {
            get; set;
        }

        public string Body
        {
            get; set;
        }

        public IDictionary<string, object> ApplicationProperties
        {
            get; set;
        } = new Dictionary<string, object>();
    }
}
