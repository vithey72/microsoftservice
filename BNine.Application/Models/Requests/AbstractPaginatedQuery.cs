﻿namespace BNine.Application.Models.Requests
{
    using MediatR;

    public class AbstractPaginatedQuery<T> : IRequest<PaginatedList<T>>
    {
        public int PageIndex
        {
            get; protected set;
        }

        public int PageSize
        {
            get; protected set;
        }

        public int Skip
        {
            get
            {
                return PageSize * (PageIndex - 1);
            }
        }

        protected AbstractPaginatedQuery(int? pageIndex, int? pageSize)
        {
            PageIndex = GetValue(pageIndex, 1);
            PageSize = GetValue(pageSize, 30);
        }

        private int GetValue(int? value, int defaultValue)
        {
            return value == null || value < 1 ? defaultValue : value.Value;
        }
    }
}
