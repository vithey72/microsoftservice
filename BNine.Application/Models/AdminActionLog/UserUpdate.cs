﻿namespace BNine.Application.Models.AdminActionLog;

public class UserUpdate
{
    public Guid Id
    {
        get;
        set;
    }

    public string UpdateFrom
    {
        get;
        set;
    }

    public string UpdateTo
    {
        get;
        set;
    }

    public string UpdateBy
    {
        get;
        set;
    }

    public DateTime UpdateAt
    {
        get;
        set;
    }

    public string Notes
    {
        get;
        set;
    }

    public string Type
    {
        get;
        set;
    }
}
