﻿namespace BNine.Application.Interfaces;

public interface IClientAccountSettings
{
    int VoluntaryClosureDelayInDays
    {
        get;
    }

    int BlockClosureOffsetFromActivationInDays
    {
        get;
    }

    TimeSpan PendingClosureCheckGap
    {
        get;
    }

    int TruvDistributionShareInPercents
    {
        get;
    }
}
