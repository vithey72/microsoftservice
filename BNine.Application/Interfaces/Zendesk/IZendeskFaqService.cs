﻿namespace BNine.Application.Interfaces.Zendesk
{
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using BNine.Application.Models.Zendesk;
    using BNine.Enums;

    public interface IZendeskFaqService
    {
        Task<IEnumerable<FAQCategory>> GetFaqArticles(Language language);
        Task<FAQSections> GetFaqSections();
    }
}
