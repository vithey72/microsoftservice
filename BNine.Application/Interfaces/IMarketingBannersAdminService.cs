﻿namespace BNine.Application.Interfaces;

using Aggregates.Banners.Models;
using Domain.Entities.Banners;

/// <summary>
/// Service that provides data for displaying various marketing banners.
/// </summary>
public interface IMarketingBannersAdminService
{
    /// <summary>
    /// Get all banners of type <see cref="TBanner"/>.
    /// </summary>
    /// <returns>DB entries mapped to <see cref="TViewModel"/>.</returns>
    Task<List<TViewModel>> GetWalletScreenBanners<TBanner, TViewModel>()
        where TBanner : BaseMarketingBanner;

    Task<T> CreateBanner<T>(T banner, CancellationToken cancellationToken)
        where T : InfoPopupBannerInstanceSettings;

    Task DeleteBanner<T>(Guid bannerId, CancellationToken cancellationToken)
        where T: BaseMarketingBanner;

    Task<T> UpdateBanner<T>(T banner, CancellationToken token)
        where T : InfoPopupBannerInstanceSettings;
}
