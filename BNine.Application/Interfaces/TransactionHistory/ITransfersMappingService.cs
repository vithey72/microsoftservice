﻿namespace BNine.Application.Interfaces.TransactionHistory;

using Domain.Entities;
using Enums.Transfers.Controller;
using Models.MBanq;
using Models.Transfer;

public interface ITransfersMappingService
{
    Task<string> InferIconUrl(BankTransferIconOption transferIconOption, int? mccType, string counterparty = null);

    (string Account, string Routing) ExtractAchRecipientNumbers(SavingsTransactionItem savingsTransaction);

    Task<(string FullName, string Phone)> ExtractB9ClientPhoneAndName(string counterpartyIdentifer);

    MerchantDescriptionModel InferMerchant(string reference);

    Task<(string card, string account)> GetAccountAndCardNumberTexts(int accountExternalId, DebitCard debitCard, string mbanqToken, string mbanqIp);

    Task<string> GetAccountLastDigits(Guid userId, string mbanqToken, string mbanqIp);
}
