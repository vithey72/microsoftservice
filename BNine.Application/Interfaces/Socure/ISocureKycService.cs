﻿namespace BNine.Application.Interfaces.Socure
{
    using System.Threading.Tasks;
    using Aggregates.Evaluation.Commands;
    using Aggregates.Evaluation.Models;

    public interface ISocureKycService
    {
        /// <summary>
        /// Send all user data collected on onboarding to Socure and get a verdict in response.
        /// </summary>
        Task<SocureTransactionResponse> PerformKycTransaction(SocureTransactionRequest request, CancellationToken token);

        Task<Dictionary<SocureClientDocumentModel, byte[]>> DownloadAllDocumentSides(Guid scanId, CancellationToken token);
    }
}
