﻿namespace BNine.Application.Interfaces
{
    using System.Threading.Tasks;

    public interface ICreditProductService
    {
        Task<bool> IsCreditAllowed();

        Task<int> CalculateCreditLimit();
    }
}
