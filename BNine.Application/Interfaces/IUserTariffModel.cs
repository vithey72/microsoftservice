﻿namespace BNine.Application.Interfaces;

using BNine.Domain.Entities.TariffPlan;

public interface IUserTariffModel
{
    TariffPlan Entity
    {
        get;
    }

    string GetTariffImageUrl();

    string GetTariffPriceText();

    string GetTariffTitle();
}
