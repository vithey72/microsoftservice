﻿namespace BNine.Application.Interfaces;

using System.Threading;
using BNine.Domain.Entities.TariffPlan;
using Enums.TariffPlan;

public interface ITariffPlanService
{
    Task SwitchTariffPlan(Guid userId, Guid tariffPlanId, bool skipBalanceCheck = false);

    /// <summary>
    /// Initial activation of tariffs, the tariffs created differ by partners.
    /// </summary>
    Task ActivateStarterTariffPlan(Guid userId, CancellationToken cancellationToken);

    Task UpdateUserTariffPlan(long externalClientId, decimal deferredAmount = decimal.Zero);

    Task UpdateUserTariffPurchase(decimal deferredAmount = decimal.Zero);

    Task<TariffPlan> GetTariffByClassificationId(int externalId, CancellationToken token);

    Task<(TariffPlan Current, DateTime? CyclePaymentDate, TariffPlan Upcoming, DateTime? SwitchDate)> GetCurrentAndUpcomingTariffs(
        Guid userId, CancellationToken token);

    Task<TariffPlan> GetCurrentTariff(Guid userId, CancellationToken token);

    Task<IUserTariffModel> GetCurrentTariffModel(Guid userId, CancellationToken token);

    Task<TariffPlanFamily> GetCurrentTariffPlanFamily(Guid userId, CancellationToken token);
}
