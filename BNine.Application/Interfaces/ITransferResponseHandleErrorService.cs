﻿namespace BNine.Application.Interfaces
{
    using BNine.Application.Models.MBanq;

    public interface IPushPullFromExternalCardErrorHandlingService
    {
        void HandlePushPullFromExternalCardErrors(IEnumerable<Error> errors);
    }
}
