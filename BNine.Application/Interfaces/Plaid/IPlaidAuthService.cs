namespace BNine.Application.Interfaces.Plaid
{
    using System.Threading.Tasks;
    using Aggregates.Plaid.Models;

    public interface IPlaidAuthService
    {
        Task<string> GetLinkToken(string userId, string androidPackageName = null);

        Task<AccessTokenResponseModel> GetAccessToken(string publicToken);

        Task<AccountDataResponseModel> GetAccountData(string accessToken);

        Task DisposeAccessToken(string accessToken);
    }
}
