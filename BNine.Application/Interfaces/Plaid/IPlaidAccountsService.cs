﻿namespace BNine.Application.Interfaces.Plaid
{
    using System.Threading.Tasks;
    using BNine.Application.Models.Plaid;

    public interface IPlaidAccountsService
    {
        Task<BalanceDto> GetBalance(string accessToken, string accountId);
    }
}
