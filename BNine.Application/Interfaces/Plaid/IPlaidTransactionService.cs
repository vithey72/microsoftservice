﻿namespace BNine.Application.Interfaces.Plaid
{
    using System.Collections.Generic;
    using System.Threading.Tasks;

    public interface IPlaidTransactionService
    {
        Task<string> GetRawTransactionsData(string accessToken, IEnumerable<string> accountIds);
    }
}
