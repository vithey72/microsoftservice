﻿namespace BNine.Application.Interfaces.Argyle
{
    using System;
    using System.Threading.Tasks;

    public interface IArgyleUsersService
    {
        Task DeleteUser(Guid userId);

        Task<object> GetUser(Guid userId);
    }
}
