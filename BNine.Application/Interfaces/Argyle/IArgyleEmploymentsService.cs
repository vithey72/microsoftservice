﻿namespace BNine.Application.Interfaces.Argyle;

using System;
using System.Threading.Tasks;
using Models.Argyle;

/// <summary>
/// Communicates with this list of endpoints https://docs.argyle.com/guides/reference/employments.
/// </summary>
public interface IArgyleEmploymentsService
{
    Task<object> GetEmployment(Guid id);

    Task<ArgyleEmploymentArray> GetEmployments(Guid userId, CancellationToken cancellationToken);

    public Task<string[]> GetEmploymentsLinkIds(Guid userId, CancellationToken cancellationToken);
}
