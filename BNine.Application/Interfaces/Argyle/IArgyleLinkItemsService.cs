﻿namespace BNine.Application.Interfaces.Argyle;

using System.Threading.Tasks;
using Models.Argyle;

/// <summary>
/// Communicates with this list of endpoints https://docs.argyle.com/guides/reference/link-items.
/// </summary>
public interface IArgyleLinkItemsService
{
    /// <summary>
    /// Get the Link entities with corresponding Ids.
    /// </summary>
    /// <param name="ids">Either a string or guid Id of entity.</param>
    /// <param name="cancellationToken">Self explanatory.</param>
    /// <returns>Model containing found Link items.</returns>
    Task<ArgyleLinkItemsArray> GetLinkItems(string[] ids, CancellationToken cancellationToken);
}
