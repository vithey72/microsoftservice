﻿namespace BNine.Application.Interfaces.Argyle
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;

    public interface IPayoutsService
    {
        Task<IEnumerable<object>> GetPayouts(Guid accountId);
    }
}
