﻿namespace BNine.Application.Interfaces
{
    using System.Threading.Tasks;
    using System;

    public interface IArgyleService
    {
        Task<string> GetEncryptedConfig(
            string accountNumber,
            decimal defaultAmountAllocation,
            decimal minAmountAllocation,
            decimal maxAmountAllocation,
            decimal defaultPercentAllocation,
            decimal minPercentAllocation,
            decimal maxPercentAllocation);

        Task<string> UpdateToken(Guid userId);

        Task<object> GetAccount(Guid id);

        Task<(string token, Guid externalUserId)> CreateAccount(Guid userId);
    }
}
