﻿namespace BNine.Application.Interfaces.Argyle
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using BNine.Application.Models.Argyle;

    public interface IPayAllocationsService
    {
        Task<IEnumerable<PayAllocationInfo>> GetPayAllocations(Guid argyleUserId);

        Task<object> GetPayAllocation(Guid id);
    }
}
