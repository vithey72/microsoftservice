﻿namespace BNine.Application.Interfaces;

using BNine.Domain.Entities.User;
using BNine.Enums.Advance;

public interface IAdvanceWidgetStateProvider
{
    Task<AdvanceWidgetState> GetWidgetState(User user);
}
