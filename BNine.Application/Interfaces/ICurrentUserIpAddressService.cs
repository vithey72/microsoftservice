﻿namespace BNine.Application.Interfaces;

public interface ICurrentUserIpAddressService
{
    public string CurrentIp
    {
        get;
    }
}
