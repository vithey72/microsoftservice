﻿namespace BNine.Application.Interfaces
{
    public interface IOrderedListItem
    {
        long Order
        {
            get; set;
        }
    }
}
