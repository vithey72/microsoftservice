﻿namespace BNine.Application.Interfaces.LegalDocuments;

using Aggregates.StaticDocuments.Commands;
using Domain.Entities.Settings;

public interface ILegalDocumentsStorageService
{
    Task<string> UploadLegalDocument(StaticDocument existingDocument,
        UpdateLegalDocumentCommand request,
        CancellationToken ctx = default);
}
