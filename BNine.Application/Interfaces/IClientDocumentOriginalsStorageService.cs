﻿namespace BNine.Application.Interfaces;

using Enums;

public interface IClientDocumentOriginalsStorageService
{
    /// <summary>
    /// Returns file name and created date list
    /// </summary>
    Task<List<KeyValuePair<string, DateTimeOffset?>>> GetClientDocumentsMetadata(Guid clientId, CancellationToken cancellationToken = default);

    Task UploadClientDocument(Guid clientId, string documentTypeKey, DocumentSide documentSide,
        byte[] fileData, string contentType, string fileExtension, CancellationToken cancellationToken = default);

    Task<byte[]> DownloadClientDocumentFile(string fileName, CancellationToken cancellationToken = default);
}
