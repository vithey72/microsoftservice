﻿namespace BNine.Application.Interfaces
{
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Models.Emails.EmailsWithTemplate;

    public interface IEmailService
    {
        Task<EmailsResponse> SendEmail(EmailMessageTemplate templateMessage);

        Task<EmailsResponse> SendEmails(List<EmailMessageTemplate> templateMessages);
    }
}
