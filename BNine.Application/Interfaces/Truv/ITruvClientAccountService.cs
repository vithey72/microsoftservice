﻿namespace BNine.Application.Interfaces.Truv;

using BNine.Application.Models.Truv;

public interface ITruvClientAccountService
{
    Task<TruvClientAccount> CreateAccount(Guid userId, CancellationToken cancellationToken);

    Task DeleteAccount(Guid truvUserId, CancellationToken cancellationToken);
}
