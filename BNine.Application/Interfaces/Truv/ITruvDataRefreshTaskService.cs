﻿namespace BNine.Application.Interfaces.Truv;

#nullable enable

public interface ITruvDataRefreshTaskService
{
    Task<TruvTaskId> BeginDataRefresh(string accessToken, CancellationToken cancellationToken);
    Task<TruvTaskStatus> GetTaskStatus(TruvTaskId taskId, CancellationToken cancellationToken);
    Task<TruvTaskStatusUpdate?> ParseTaskStatusUpdateEvent(Stream requestBody);
}

public record TruvTaskStatusUpdate(TruvTaskId TaskId, Guid TruvUserId, string LinkId, TruvTaskStatus Status, DateTime UpdatedAt);

public record TruvTaskId(string Value);

public enum TruvTaskStatus
{
    Pending,
    Failed,
    Succeeded
}
