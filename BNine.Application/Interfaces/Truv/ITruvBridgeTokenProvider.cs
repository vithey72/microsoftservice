﻿namespace BNine.Application.Interfaces.Truv;

public interface ITruvBridgeTokenProvider
{
    Task<TruvBridgeToken> GetToken(
        Guid userId,
        string bankAccountNumber,
        CancellationToken cancellationToken);
}

public record TruvBridgeToken(string Value, DateTime ValidTo);
