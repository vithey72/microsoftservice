﻿namespace BNine.Application.Interfaces.Truv;

public interface ITruvEmploymentProvider
{
    Task<TruvEmployment[]> ProvideEmployments(string linkId, CancellationToken cancellationToken);
}

public record TruvEmployment(string EmployerTitle, string EmployerIconUrl);
