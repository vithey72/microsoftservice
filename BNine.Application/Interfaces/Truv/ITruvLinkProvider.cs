﻿namespace BNine.Application.Interfaces.Truv;

public interface ITruvLinkProvider
{
    Task<TruvLinkToken> ProvideLinkToken(string publicToken, CancellationToken cancellationToken);
}

public record TruvLinkToken(string LinkId, string AccessToken);
