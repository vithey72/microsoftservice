﻿namespace BNine.Application.Interfaces.Truv;

public static class ContractExtensions
{
    public static string GetTruvUserIdString(this Guid truvUserId) => truvUserId.ToString("N");
}
