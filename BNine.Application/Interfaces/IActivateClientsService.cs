﻿namespace BNine.Application.Interfaces
{
    using System.Threading.Tasks;

    public interface IActivateClientsService
    {
        Task ActivateClient(int clientId);
    }
}
