﻿namespace BNine.Application.Interfaces;

public interface IAdvancesCompletedCheckupService
{
    Task<bool> IsUserTrustedEnough(Guid userId);
    Task<bool> IsModalWindowRequired(Guid userId);
}
