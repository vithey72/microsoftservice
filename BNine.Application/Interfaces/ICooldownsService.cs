﻿namespace BNine.Application.Interfaces;

/// <summary>
/// Service designed to track time periods of some reoccurring activities that require cooldown.
/// </summary>
public interface IUserActivitiesCooldownService
{
    /// <summary>
    /// Checks if there is currently a cooldown for this activity.
    /// </summary>
    /// <returns>True if cooldown is active.</returns>
    Task<bool> IsOnCooldown(Guid userId, string activityName, CancellationToken cancellationToken);

    /// <summary>
    /// Start a cooldown for this activity. If cooldown is currently active, it will not update the time (!).
    /// </summary>
    /// <returns>Flag of success.</returns>
    Task<bool> AddCooldown(Guid userId, string activityName, TimeSpan duration, CancellationToken cancellationToken);

    /// <summary>
    /// Same as <see cref="AddCooldown"/>, but time for active cooldown will be updated.
    /// </summary>
    Task AddOrUpdateCooldown(Guid userId, string activityName, TimeSpan duration, CancellationToken cancellationToken);
}
