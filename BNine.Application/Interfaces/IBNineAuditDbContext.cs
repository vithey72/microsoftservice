﻿namespace BNine.Application.Interfaces;

using Domain.Entities.EventAudit;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;

public interface IBNineAuditDbContext
{
    public DbSet<EventAudit> EventAuditEntries { get; set; }

    EntityEntry<TEntity> Entry<TEntity>(TEntity entity) where TEntity : class;


    Task<int> SaveChangesAsync(CancellationToken cancellationToken);
    int SaveChanges();

}
