﻿namespace BNine.Application.Interfaces;

public interface ICurrentAppVersionService
{
    Version AppVersion
    {
        get;
    }
}
