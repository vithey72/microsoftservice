﻿namespace BNine.Application.Interfaces;

public interface IDateTimeProviderService
{
    DateTime Now();
}
