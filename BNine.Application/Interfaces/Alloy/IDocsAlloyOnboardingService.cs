﻿namespace BNine.Application.Interfaces.Alloy
{
    using System.Threading.Tasks;
    using Aggregates.Evaluation.Commands;
    using Aggregates.Evaluation.Models;

    [Obsolete]
    public interface IDocsAlloyOnboardingService
    {
        Task<string> GetEvaluationParameters();

        Task<EvaluationSuccessResponse> EvaluateUser(EvaluateUserCommand command, string entityToken);
    }
}
