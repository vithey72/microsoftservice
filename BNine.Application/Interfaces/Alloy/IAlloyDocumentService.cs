﻿namespace BNine.Application.Interfaces.Alloy
{
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Aggregates.Users.Models;

    public interface IAlloyDocumentService
    {
        Task<byte[]> GetDocument(string entityToken, string documentToken);

        Task<IEnumerable<AlloyDocumentDetails>> GetDocuments(string entityToken);
    }
}
