﻿namespace BNine.Application.Interfaces;

public interface ILoanSettingsProvider
{
    Task<ILoanSettings> GetLoanSettings(CancellationToken cancellationToken);
}
