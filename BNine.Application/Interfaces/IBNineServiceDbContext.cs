﻿namespace BNine.Application.Interfaces
{
    using System.Threading.Tasks;
    using System;

    public interface IBNineServiceDbContext
    {
        Task<IAsyncDisposable> CreateTransactionalAppLockAsync(string resourceId);
    }
}
