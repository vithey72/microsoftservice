﻿namespace BNine.Application.Interfaces;

using Enums;

public interface IClientSelfieDocumentsService
{
    Task UploadDocument(Guid clientId,
        byte[] fileData, string contentType, string fileExtension, CancellationToken cancellationToken = default);

    Task UploadDocument(Guid clientId,
        byte[] fileData, string contentType, string fileExtension, SelfieInitiatingFlow selfieInitiatingFlow,
        CancellationToken cancellationToken = default);
}
