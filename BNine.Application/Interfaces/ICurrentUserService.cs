﻿namespace BNine.Application.Interfaces
{
    using System;
    using System.IdentityModel.Tokens.Jwt;
    using Enums;

    public interface ICurrentUserService
    {
        Guid? UserId
        {
            get;
        }

        string? UserMbanqToken
        {
            get;
        }

        Guid[] GroupIds
        {
            get;
        }

        JwtSecurityToken AccessToken
        {
            get;
        }

        MobilePlatform Platform
        {
            get;
        }
    }
}
