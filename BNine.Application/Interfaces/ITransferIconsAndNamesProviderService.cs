﻿namespace BNine.Application.Interfaces
{
    using BNine.Application.Models.Transfer;
    using BNine.Enums.Transfers.Controller;

    public interface ITransferIconsAndNamesProviderService
    {
        /// <summary>
        /// Find merchant icon and name by description.
        /// </summary>
        /// <returns></returns>
        public MerchantDescriptionModel FindMerchant(string merchantDescription);

        string FindIconUrl(BankTransferIconOption transferIconOption);

        string FindMccIconUrl(int? mccType);
    }
}
