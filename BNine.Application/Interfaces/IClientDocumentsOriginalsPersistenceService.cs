﻿namespace BNine.Application.Interfaces;

using Aggregates.Users.Models;
using Enums;

public interface IClientDocumentsOriginalsPersistenceService
{
    Task<List<ClientDocument>> GetClientDocuments(Guid clientId, CancellationToken cancellationToken = default);

    /// <summary>
    /// Returns blob file in Base64 string format
    /// </summary>
    /// <param name="fileName"></param>
    /// <returns></returns>
    Task<string> DownloadClientDocumentFile(string fileName, CancellationToken cancellationToken = default);

    Task UploadClientDocumentFromAlloy(Guid clientId, string documentTypeKey, string fileName, string contentType,
        byte[] fileData, string fileExtension, CancellationToken cancellationToken = default);

    Task UploadClientDocumentFromSocure(Guid clientId, string documentTypeKey, string fileName, string contentType,
        DocumentSide documentSide, byte[] fileData, string fileExtension,
        CancellationToken cancellationToken = default);

    Task MarkClientDocumentAsUploaded(Guid clientId, CancellationToken cancellationToken);
}
