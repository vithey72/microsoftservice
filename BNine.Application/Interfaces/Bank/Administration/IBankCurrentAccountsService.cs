﻿namespace BNine.Application.Interfaces.Bank.Administration;

public interface IBankCurrentAccountsService
{
    public Task AddCurrentBankAccountsIfNeeded(Guid userId, CancellationToken cancellationToken);
}
