﻿namespace BNine.Application.Interfaces.Bank.Administration
{
    using System.Threading.Tasks;
    using BNine.Application.Models.MBanq;

    public interface IBankTransactionsService
    {
        Task<SavingAccountTransactionInfo> GetSavingAccountTransaction(int id);
    }
}
