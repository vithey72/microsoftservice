﻿namespace BNine.Application.Interfaces.Bank.Administration
{
    using System.Threading.Tasks;

    public interface IBankUsersService
    {
        Task<int> CreateUser(string userName, string firstName, string lastName, string email, string password, int officeId = 1);

        Task<int> UpdateUser(int id, string userName, string firstName, string lastName, string email);

        Task<int> UpdateUser(int id, string firstName, string newPassword, string passwordConfirmation);

        Task<int> UpdateUserEmail(int id, string email);

        Task DeleteUser(int id);
    }
}
