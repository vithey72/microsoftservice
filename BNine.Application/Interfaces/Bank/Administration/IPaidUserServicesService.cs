﻿namespace BNine.Application.Interfaces.Bank.Administration;

using System;
using System.Threading.Tasks;
using Domain.Entities.PaidUserServices;

/// <summary>
/// Service for enabling paid services for our clients outside of tariff paradigm.
/// TODO: a better name
/// </summary>
public interface IPaidUserServicesService
{
    Task<UsedPaidUserService> EnableService(Guid paidServiceId, string idempotencyKey = null, Guid? userId = null);

    Task<bool> ServiceIsAlreadyPurchased(Guid paidServiceId, Guid? userId = null);

    Task<UsedPaidUserService> GetEnabledService(Guid paidServiceId, Guid? userId = null);

    Task<decimal> GetServiceCost(Guid paidServiceId);

    Task MarkServiceAsUsed(Guid paidServiceId, Guid? userId = null);
}
