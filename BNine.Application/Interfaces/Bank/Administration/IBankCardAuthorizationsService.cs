﻿namespace BNine.Application.Interfaces.Bank.Administration
{
    using System.Threading.Tasks;
    using BNine.Application.Models.MBanq;

    public interface IBankCardAuthorizationsService
    {
        Task<CardAuthorizationInfo> GetCardAuthorization(int id);
    }
}
