﻿namespace BNine.Application.Interfaces.Bank.Administration
{
    using System.Threading.Tasks;
    using BNine.Application.Models.MBanq;

    /// <summary>
    /// Charge is a bank entity that corresponds to a usually regular billing of a user.
    /// Charge is a property of Savings Account.
    /// </summary>
    public interface IBankChargesService
    {
        Task<ChargeDetails> GetChargeDetails(long id);

        Task<SavingAccountCharge> GetActiveTariffCharge(int savingsAccountId);

        Task<SavingAccountCharge> GetActiveCharge(int savingsAccountId, string[] chargesTypes = null);

        Task<List<SavingAccountCharge>> GetSavingAccountCharges(int savingId);

        Task<SavingAccountCharge> CreateAndCollectAccountCharge(int accountId, int chargeId, DateTime dueDate, string idempotencyKey = null);


        /// <summary>
        /// Mark charge as not requiring collection.
        /// </summary>
        Task WaiveCharge(int accountId, int chargeId);
    }
}
