﻿namespace BNine.Application.Interfaces.Bank.Administration
{
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Models.MBanq;

    public interface IBankDocumentsService
    {
        Task<string> UploadDocument(byte[] file, string fileName, string contentType, string type, int clientId);

        Task<IEnumerable<DocumentListItem>> GetDocuments(int clientIdentifierId);

        Task<string> DeleteDocument(int clientIdentifierId, string documentId);
    }
}
