﻿namespace BNine.Application.Interfaces.Bank.Administration;

public interface IBankCreditCardsService
{
    Task<(Guid id, int externalId)> CreateCreditCard(int clientId, int currentAccountId, int productId, string idempotencyKey = null);

    Task<(string cardNumber, decimal balance)> GetBalance(int cardId);
}
