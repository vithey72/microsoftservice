﻿namespace BNine.Application.Interfaces.Bank.Administration;

using System.Threading.Tasks;
using Aggregates.TariffPlans.Models;

/// <summary>
/// Fetch users' classifications (aka tariffs), current and upcoming.
/// </summary>
public interface IBankClassificationsService
{
    Task<UserClassificationsList> GetUserClassifications(int externalUserId, CancellationToken token);
}
