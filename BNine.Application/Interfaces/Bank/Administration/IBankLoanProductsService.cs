﻿namespace BNine.Application.Interfaces.Bank.Administration
{
    using System.Threading.Tasks;
    using Models.MBanq;

    public interface IBankLoanProductsService
    {
        Task<LoanProduct> GetLoanProduct(int productId);
    }
}
