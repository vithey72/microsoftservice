﻿namespace BNine.Application.Interfaces.Bank.Administration
{
    using System.Threading.Tasks;
    using Models.MBanq;

    public interface IBankSavingAccountsService
    {
        Task<int> CreateSavingAccount(int clientId, int productId);

        Task<int> ActivateSavingAccount(int savingId);

        Task<int> ApproveSavingAccount(int savingId);

        Task<SavingAccountInfo> GetSavingAccountInfo(int savingId);

        Task EnableDebitTransfersForAccount(int savingId);

        Task CloseSavingAccount(int savingAccountId, string note);

        Task BlockSavingAccount(int savingAccountId, string reason);

        Task UnblockSavingAccount(int savingAccountId);
    }
}
