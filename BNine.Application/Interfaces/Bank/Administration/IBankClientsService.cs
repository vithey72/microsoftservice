﻿namespace BNine.Application.Interfaces.Bank.Administration
{
    using System;
    using System.Threading.Tasks;

    public interface IBankClientsService
    {
        Task<int> CreateClient(string firstName, string lastName, string phone,
            string email, DateTime dateOfBirth, string idempotencyKey = null);

        Task<int> GetClientClassificationId(int clientId);

        Task<int> LinkClientToUser(int clientId, int userId);

        Task<int> ActivateClient(int clientId);

        Task<int> UpdateClient(int clientId, string firstName, string lastName, string phone, string email, DateTime dateOfBirth);

        Task<int> UpdateClientPhoneNumber(int id, string phoneNumber);

        Task<int> UpdateClientEmail(int id, string email);

        Task<int> UpdateClientFullName(int id, string firstName, string lastName);

        Task<int> UpdateClientClassification(int clientId, int classificationId, DateTime? activationDate = null);

        Task<string> GetVerificationStatus(int clientId);

        Task ResetClientUniqueData(int clientId);

        Task CloseClient(int clientId);

        Task DeleteClient(int clientId);
    }
}
