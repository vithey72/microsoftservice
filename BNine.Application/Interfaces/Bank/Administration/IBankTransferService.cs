﻿namespace BNine.Application.Interfaces.Bank.Administration
{
    using System.Threading.Tasks;
    using Models.MBanq.Transfers;

    public interface IBankTransferService
    {
        Task<TransferData> GetTransfer(int id);
    }
}
