﻿namespace BNine.Application.Interfaces.Bank.Administration
{
    using System.Threading.Tasks;
    using Models.MBanq;

    public interface IBankIdentityService
    {
        Task<int> CreateIdentity(int clientId, int type, string number, string issuedBy);

        Task<int> UpdateIdentity(int clientId, int identityId, int type, string number, string issuedBy);

        Task DeleteIdentity(int clientId, int identityId);

        Task<MbanqIdentityModel> FetchIdentity(int clientId, int identityId);
    }
}
