﻿namespace BNine.Application.Interfaces.Bank.Administration
{
    using System.Threading.Tasks;
    using Aggregates.Cards.Commands.UpdateCardLimits;
    using Aggregates.Cards.Models;

    public interface IBankInternalCardsService
    {
        Task<int> CreateInternalCardREST(int clientId, int savingsAccountId, int productId, string idempotentKey = null);

        Task<int> EnableOnlinePayments(int cardId);

        Task<CardLimitsResponseModel> GetCardLimits(int cardId);

        Task<int> SetCardLimits(int cardId, UpdateCardLimitsCommand request);

        Task TerminateClientCard(int mbanqCardId);

        Task<CardInfoModel[]> GetAllCards(int externalUserId);
    }
}
