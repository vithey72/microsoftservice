﻿namespace BNine.Application.Interfaces.Bank.Administration
{
    using System.Threading.Tasks;
    using BNine.Application.Aggregates.Users.Models;

    public interface IBankTemplatesService
    {
        Task<AddressParametersOptions> GetAddressParametersOptions();
    }
}
