﻿namespace BNine.Application.Interfaces.Bank.Administration
{
    using System.Threading.Tasks;
    using BNine.Application.Models.MBanq;

    public interface IBankExternalCardsService
    {
        Task<string> GetPublicKey();

        Task<ExternalCardDetails> GetExternalCard(int clientId, int cardId);

        Task DeleteCard(int clientId, long externalCardId);

        Task<List<ExternalCardDetails>> GetExternalCards(int clientId);

        Task<List<ExternalCardOperationHistory>> GetExternalCardsHistory(int externalClientId);
    }
}
