﻿namespace BNine.Application.Interfaces.Bank.Administration;

using System.Threading.Tasks;
using Models.MBanq;

public interface IBankLoanAccountsService
{
    /// <summary>
    /// Step 1 of taking an Advance. Create a Loan Account entity.
    /// </summary>
        Task<int> CreateLoanAccount(
            int clientId,
            int currentAccountExternalId,
            int loanAmount,
            decimal loanBoostExpressFeeAmount,
            CancellationToken cancellationToken);

    /// <summary>
    /// Step 2. Approve the loan.
    /// </summary>
    Task<int> ApproveLoanAccount(int loanId);

    /// <summary>
    /// Step 3. Disburse the loan and deposit the funds into client's Current Account.
    /// </summary>
    Task<int> DisburseLoanAccountToSaving(int loanId, double amount);

    Task<LoanSummary> GetLoan(int loanId);
}
