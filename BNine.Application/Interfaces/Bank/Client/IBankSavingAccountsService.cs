﻿#nullable enable
namespace BNine.Application.Interfaces.Bank.Client
{
    using System.Threading.Tasks;
    using Models.MBanq;

    public interface IBankSavingAccountsService
    {
        Task<SavingAccountInfo?> GetSavingAccountInfoSafe(int savingId, string mbanqToken, string ip);
        Task<SavingAccountInfo> GetSavingAccountInfo(int savingId, string mbanqToken, string ip);

        Task<SavingAccountInfo> GetSavingAccountInfoAsAdmin(int savingId);

        Task<SavingAccountInfo[]> GetSavingAccounts(string[] savingIds);
    }
}
