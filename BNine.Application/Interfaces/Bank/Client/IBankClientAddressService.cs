﻿namespace BNine.Application.Interfaces.Bank.Client
{
    using System.Threading.Tasks;
    using Models.MBanq;

    public interface IBankClientAddressService
    {
        Task<int> SetAddress(int clientId, string city, int stateId, string address, string zipCode, string unit, int countryId, int addressTypeId);

        Task<int> UpdateAddress(int addressId, string city, int stateId, string address, string zipCode, string unit, int countryId);

        Task<List<ClientAddress>> GetAddresses(int clientId);

        Task<List<ClientAddress>> GetAddresses(int clientId, int addressTypeId);
    }
}
