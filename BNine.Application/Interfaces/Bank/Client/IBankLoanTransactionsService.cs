﻿namespace BNine.Application.Interfaces.Bank.Client
{
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Models.MBanq;

    public interface IBankLoanTransactionsService
    {
        Task<IEnumerable<LoanTransactionItem>> GetTransactions(int accountId, int start = 1, int limit = 10);
    }
}
