﻿namespace BNine.Application.Interfaces.Bank.Client
{
    using System.Threading.Tasks;
    using Aggregates.Cards.Models;

    public interface IBankInternalCardsService
    {
        Task<string> GetCardImageUrl(string mbanqToken, string ip, int cardId);

        Task<ShippingHistoryModel> GetCardShippingHistory(string mbanqToken, string ip, int cardId);

        Task<string> ActivatePhysicalCard(string mbanqToken, string ip, int cardId);

        Task<int> ReplaceCard(string mbanqToken, string ip, int cardId, string reason);

        Task<CardInfoModel> GetCardInfo(string mbanqToken, string ip, int cardId);

        Task<CardInfoModel[]> GetAllCards(string mbanqToken, string ip, int externalUserId);

        Task<int> ChangeCardStatus(string mbanqToken, string ip, int cardId, string command);

        Task<string> GetChangeCardPinUrl(string mbanqToken, string ip, int cardId);

        /// <summary>
        /// Get a number of times user's physical card was embossed and shipped due to replacement.
        /// </summary>
        Task<int> CountReplacements(string mbanqToken, string ip, int externalUserId);
    }
}
