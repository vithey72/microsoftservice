﻿namespace BNine.Application.Interfaces.Bank.Client
{
    using System.Threading.Tasks;
    using BNine.Application.Models.MBanq;

    public interface IBankAuthorizationService
    {
        Task<AuthorizationTokensResponseModel> GetTokens(string userName, string password);

        Task<AuthorizationTokensResponseModel> GetTokensByRefreshToken(string mbanqRefreshToken, string userId);
    }
}
