﻿namespace BNine.Application.Interfaces.Bank.Client
{
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using BNine.Application.Models.MBanq;

    public interface IBankClientExternalCardsService
    {
        Task<IEnumerable<ExternalCardListItem>> GetCards(string mbanqToken, string ip, IEnumerable<int> ids);

        Task<IEnumerable<ExternalCardListItem>> GetAllUserCards(string mbanqToken, string ip);

        Task DeleteCard(long externalId, int value, string ip, string mbanqAccessToken);
    }
}
