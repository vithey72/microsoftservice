﻿namespace BNine.Application.Interfaces.Bank.Client
{
    using System.Threading.Tasks;
    using BNine.Application.Models.MBanq;
    using Models.MBanq.Transfers;

    public interface IBankTransfersService
    {
        Task<GraphQLResponse<CreateAndSubmitTransferResponse>> CreateAndSubmitTransfer(string accessToken, string ip, Transfer transfer);

        Task<TransferInfo> GetTransfer(int transferId, string accessToken, string ip);

        Task<CreateCheckTransferResponse> CreateCheckTransfer(string accessToken, string ip, CheckTransfer transfer);
    }
}
