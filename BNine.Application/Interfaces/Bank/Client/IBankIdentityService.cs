﻿namespace BNine.Application.Interfaces.Bank.Client
{
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Models.MBanq;

    public interface IBankIdentityService
    {
        Task<List<ClientIdentifierType>> GetClientIdentifierDocumentTypes();
    }
}
