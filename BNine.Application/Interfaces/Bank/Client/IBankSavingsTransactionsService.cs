﻿namespace BNine.Application.Interfaces.Bank.Client
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Aggregates.CheckingAccounts.Models;
    using Enums.Transfers;
    using Models;
    using Models.MBanq;

    public interface IBankSavingsTransactionsService
    {
        Task<PaginatedList<SavingsTransactionItem>> GetTransactions(int accountId, int start = 1, int limit = 10,
            DateTime? from = null, DateTime? to = null, IEnumerable<TransactionType> transactionTypes = null,
            string mbanqToken = null, string ip = null, GetFilteredTransfersExpandedFilter newFilter = null);

        Task<PaginatedList<PendingTransactionItem>> GetPendingTransactions(int accountId, string mbanqToken,
            int start = 1, int limit = 10, DateTime? from = null, DateTime? to = null, bool? succeeded = null,
            int? paymentTypeId = null, string ip = null,
            GetFilteredTransfersExpandedFilter newFilter = null);

        Task<PaginatedList<SavingsTransactionItem>> GetRejectedTransactions(int accountId,
            int start = 1, int limit = 10, DateTime? from = null, DateTime? to = null, string mbanqToken = null,
            string ip = null,
            GetFilteredTransfersExpandedFilter newFilter = null);

        Task<PaginatedList<AccountStatementTransactionItem>> GetAccountStatementTransactions(int accountId,
            int start, int limit, DateTime from, DateTime to, string mbanqToken, string ip, bool asAdmin = false);

        Task<SavingsTransactionItem> GetTransferByCorrelationId(int accountId, Guid correlationId);

        Task<SavingsTransactionItem> GetSavingsAccountTransfer(int transferId, string accessToken, string ip);

        Task<SavingsTransactionItem[]> GetSavingsAccountTransfersArray(int[] transferIds, string accessToken, string ip);

        Task<PendingTransactionItem> GetPendingTransfer(int transferId, string accessToken, string ip);

        Task<SavingsTransactionItem> GetRejectedTransfer(int transferId, string accessToken, string ip);
    }
}
