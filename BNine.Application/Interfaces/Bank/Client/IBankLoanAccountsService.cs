﻿namespace BNine.Application.Interfaces.Bank.Client
{
    using System.Threading.Tasks;
    using BNine.Application.Models.MBanq;

    public interface IBankLoanAccountsService
    {
        Task MakeRepaymentLoanAccount(string accessToken, string ip, int loanId, decimal amount);

        Task<LoanAccountDetails> GetLoan(string accessToken, string ip, int loanId);
    }
}
