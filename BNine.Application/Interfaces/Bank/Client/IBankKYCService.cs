﻿namespace BNine.Application.Interfaces.Bank.Client
{
    using System.Threading.Tasks;

    public interface IBankKYCService
    {
        Task<string> StartKYC(int clientId);
    }
}
