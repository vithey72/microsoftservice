﻿namespace BNine.Application.Interfaces.Bank.Client
{
    using System.Threading.Tasks;
    using Aggregates.ATMs.Models;
    using Models;

    public interface IBankATMsService
    {
        Task<PaginatedList<ATMInfoModel>> GetATMs(double longitude, double latitude, long radius, int skip = 0, int take = 20);
    }
}
