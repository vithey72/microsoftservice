﻿// ReSharper disable InconsistentNaming
namespace BNine.Application.Interfaces
{
    using System.Threading;
    using System.Threading.Tasks;
    using BNine.Domain.Entities.Administrator;
    using BNine.Domain.Entities.Settings;
    using BNine.Domain.Entities.User;
    using BNine.Domain.Entities.BankAccount;
    using BNine.Domain.Entities.User.UpdateInfoProcessing;
    using Domain.Entities;
    using Domain.Entities.CheckCashing;
    using Domain.Entities.User.EarlySalary;
    using Domain.Entities.User.UserDetails.Settings;
    using Domain.ListItems;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.EntityFrameworkCore.Infrastructure;
    using BNine.Domain.Entities.ExternalCards;
    using BNine.Domain.Entities.Features;
    using BNine.Domain.Entities.MBanq;
    using BNine.Domain.Entities.BonusCodes;
    using Domain.Entities.UserQuestionnaire;
    using BNine.Domain.Entities.Notification;
    using BNine.Domain.Entities.TariffPlan;
    using BNine.Domain.Entities.Transfers;
    using Domain.Entities.Banners;
    using Domain.Entities.CashbackProgram;
    using Domain.Entities.PaidUserServices;
    using BNine.Domain.Entities.AdminActionLogEntry;
    using Domain.Entities.TransfersSecurityOTPs;

    public interface IBNineDbContext
    {
        DbSet<User> Users
        {
            get; set;
        }

        DbSet<Administrator> Administrators
        {
            get; set;
        }

        DbSet<UserSettings> UserSettings
        {
            get;
            set;
        }

        DbSet<EarlySalaryWidgetConfiguration> EarlySalaryWidgetConfigurations
        {
            get; set;
        }

        DbSet<Device> Devices
        {
            get; set;
        }

        DbSet<Notification> Notifications
        {
            get; set;
        }

        DbSet<OTP> OTPs
        {
            get; set;
        }

        [Obsolete]
        DbSet<FrequencyOfPayments> FrequenciesOfPayments
        {
            get; set;
        }

        [Obsolete]
        DbSet<PayMethod> PayMethods
        {
            get; set;
        }

        DbSet<DocumentType> DocumentTypes
        {
            get; set;
        }

        [Obsolete("Plaid-specific table, thus unused")]
        DbSet<ExternalBankAccount> BankAccounts
        {
            get; set;
        }

        /// <summary>
        /// Invite-a-friend codes, created for clients.
        /// </summary>
        DbSet<BonusCode> BonusCodes
        {
            get; set;
        }

        /// <summary>
        /// Used invite-a-friend codes, one code from the same initiator can be used by different invitees.
        /// </summary>
        DbSet<BonusCodeActivation> BonusCodeActivations
        {
            get; set;
        }

        /// <summary>
        /// A projection of MBanq internal cards.
        /// </summary>
        DbSet<DebitCard> DebitCards
        {
            get; set;
        }

        /// <summary>
        /// A projection of MBanq external cards.
        /// </summary>
        DbSet<ExternalCard> ExternalCards
        {
            get;
            set;
        }

        /// <summary>
        /// A projection of MBanq Loan Accounts.
        /// </summary>
        DbSet<LoanAccount> LoanAccounts
        {
            get; set;
        }

        DbSet<SavingAccountSettings> SavingAccountSettings
        {
            get; set;
        }

        /// <summary>
        /// Results of evaluations of users in the process of onboarding.
        /// </summary>
        DbSet<KYCVerificationResult> KYCVerificationResults
        {
            get; set;
        }

        DbSet<AnonymousAppsflyerId> AnonymousAppsflyerIds
        {
            get; set;
        }

        DbSet<UpdateUserInfoRequest> UpdateUserInfoRequests
        {
            get; set;
        }

        [Obsolete("Plaid is unused")]
        DbSet<Domain.Entities.Plaid> PlaidData
        {
            get; set;
        }

        [Obsolete("Plaid-specific table, thus unused")]
        DbSet<ExternalBankAccountBalance> ExternalBankAccountBalances
        {
            get; set;
        }

        [Obsolete("Plaid-specific table, thus unused")]
        DbSet<ExternalBankAccountTransfer> ExternalBankAccountTransfers
        {
            get; set;
        }

        /// <summary>
        /// Logs of every success and failure of every step of onboarding the client.
        /// </summary>
        DbSet<OnboardingEvent> OnboardingEvents
        {
            get; set;
        }

        DbSet<ChangeBlockStatusEvent> ChangeBlockStatusEvents
        {
            get; set;
        }

        [Obsolete("Offers are stored in the Users.UserSyncedStats and Users.AdvanceStats objects/tables")]
        DbSet<LoanSpecialOffer> LoanSpecialOffers
        {
            get; set;
        }

        /// <summary>
        /// A single-row table containing global configuration for the Advance product.
        /// </summary>
        DbSet<LoanSettings> LoanSettings
        {
            get; set;
        }

        /// <summary>
        /// A single-row table containing global configuration for user accounts.
        /// </summary>
        DbSet<CommonAccountSettings> CommonAccountSettings
        {
            get; set;
        }

        /// <summary>
        /// A single-row table containing wallet lower block description.
        /// </summary>
        DbSet<WalletBlockCollectionSettings> WalletBlockCollectionSettings
        {
            get; set;
        }

        DbSet<AccountClosureSchedule> AccountClosureSchedules
        {
            get; set;
        }

        DbSet<ACHCreditTransfer> ACHCreditTransfers
        {
            get; set;
        }

        [Obsolete("Offers are stored in the Users.UserSyncedStats and Users.AdvanceStats objects/tables")]
        DbSet<LoanCalculatedLimit> LoanCalculatedLimits
        {
            get; set;
        }

        /// <summary>
        /// Dictionary table listing possible reasons for blocking/closing client's account.
        /// </summary>
        DbSet<BlockUserReason> UserBlockReasons
        {
            get; set;
        }

        DbSet<ZendeskCategory> ZendeskCategories
        {
            get; set;
        }

        DbSet<ArgyleSettings> ArgyleSettings
        {
            get; set;
        }

        DbSet<Domain.Entities.Settings.EarlySalaryManualSettings> EarlySalaryManualSettings
        {
            get; set;
        }

        /// <summary>
        /// PDF documents that cover our legal base.
        /// </summary>
        DbSet<StaticDocument> StaticDocuments
        {
            get; set;
        }

        DbSet<AlltrustCustomerSettings> AlltrustCustomerSettings
        {
            get;
            set;
        }

        DbSet<CheckCashingTransaction> CheckCashingTransactions
        {
            get;
            set;
        }

        DbSet<CheckMaker> CheckMakers
        {
            get;
            set;
        }

        DbSet<EarlySalaryManualRequest> EarlySalaryManualRequests
        {
            get;
            set;
        }

        DbSet<MBanqChargeProduct> MBanqChargeProducts
        {
            get;
            set;
        }

        public DbSet<Feature> Features
        {
            get;
            set;
        }

        public DbSet<Group> Groups
        {
            get;
            set;
        }

        public DbSet<FeatureGroup> FeatureGroups
        {
            get;
            set;
        }

        public DbSet<UserGroup> UserGroups
        {
            get;
            set;
        }

        public DbSet<Question> Questions
        {
            get;
            set;
        }

        public DbSet<QuestionResponse> QuestionResponses
        {
            get;
            set;
        }

        public DbSet<QuestionSet> QuestionSets
        {
            get;
            set;
        }

        public DbSet<QuestionQuestionSet> QuestionQuestionSets
        {
            get;
            set;
        }

        public DbSet<SubmittedUserQuestionnaire> CompletedUserQuestionnaires
        {
            get;
            set;
        }

        public DbSet<SubmittedQuestionResponse> SubmittedQuestionResponses
        {
            get;
            set;
        }

        public DbSet<SubmittedQuestionResponseQuestionResponse> SubmittedQuestionResponseQuestionResponses
        {
            get;
            set;
        }

        public DbSet<TariffPlan> TariffPlans
        {
            get;
            set;
        }

        [Obsolete("Not a reliable source of the state")]
        public DbSet<UserTariffPlan> UserTariffPlans
        {
            get;
            set;
        }

        public DbSet<TransferMerchantsSettings> TransferMerchantsSettings
        {
            get;
            set;
        }

        public DbSet<TransferIconSettings> TransferIconSettings
        {
            get;
            set;
        }

        public DbSet<MerchantCategoryCodeIconSettings> MerchantCategoryCodeIconSettings
        {
            get;
            set;
        }

        public DbSet<UsedCashback> UsedCashbacks
        {
            get;
            set;
        }

        public DbSet<UserCashbackStatistic> UserCashbackStatistics
        {
            get;
            set;
        }

        public DbSet<CashbackCategory> CashbackCategories
        {
            get;
            set;
        }

        public DbSet<FavoriteUserTransfer> FavoriteUserTransfers
        {
            get;
            set;
        }

        public DbSet<CashbackProgramSettings> CashbackProgramSettings
        {
            get;
            set;
        }

        public DbSet<BaseMarketingBanner> AllMarketingBanners
        {
            get;
            set;
        }

        /// <summary>
        /// List of info popups. TPH model.
        /// </summary>
        public DbSet<InfoPopupBanner> InfoPopupMarketingBanners
        {
            get;
            set;
        }

        /// <summary>
        /// Records of banners that users have already seen.
        /// </summary>
        public DbSet<SeenMarketingBanner> MarketingBannersSeen
        {
            get;
            set;
        }

        public DbSet<PaidUserService> PaidUserServices
        {
            get;
            set;
        }

        public DbSet<UsedPaidUserService> UsedPaidUserServices
        {
            get;
            set;
        }

        public DbSet<AdminActionLogEntry> AdminActionLogEntry
        {
            get; set;
        }

        public DbSet<TransfersSecurityOTP> TransfersSecurityOTPs
        {
            get;
            set;
        }

        public DbSet<AdminApiUser> AdminUsers
        {
            get;
            set;
        }

        public DbSet<UserPlaceholderText> UserPlaceholderTexts
        {
            get;
            set;
        }

        public DbSet<CardProduct> CardProducts
        {
            get;
            set;
        }

        public DbSet<PotentialAchIncomeTransfer> PotentialAchIncomeTransfers
        {
            get;
            set;
        }


        public DbSet<TransactionAnalyticsCategory> TransactionCategories
        {
            get;
            set;
        }

        public DbSet<TransferErrorLog> TransferErrorLogs
        {
            get;
            set;
        }

        /// <summary>
        /// Records of cooldowns of some cyclical activities relating to clients.
        /// </summary>
        public DbSet<UserActivityCooldown> UserActivityCooldowns
        {
            get;
            set;
        }

        DatabaseFacade Database
        {
            get;
        }

        Task<int> SaveChangesAsync(CancellationToken cancellationToken);

        int SaveChanges();

        public void AddGeneric<T>(T newItem) where T : class;

        public void RemoveGeneric<T>(T itemToRemove) where T : class;
    }
}
