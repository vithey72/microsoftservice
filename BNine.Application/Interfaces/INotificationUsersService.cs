﻿namespace BNine.Application.Interfaces
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Aggregates.CommonModels;
    using BNine.Application.Models.Notifications;
    using BNine.Enums;
    using Domain.Entities;
    using Domain.Entities.User;

    public interface INotificationUsersService
    {

        Task SendNotification(Guid userId, string notificationText, bool isNotificationsEnabled, NotificationTypeEnum type, bool isImportant, IEnumerable<Device> devices, string deeplink, string appsflyerEventType, string category, string title = null, string sub1 = null);

        /// <summary>
        /// Send standard notification with placeholders.
        /// When calling, make sure that User entity has Devices and Settings included.
        /// </summary>
        Task SendNotification(User user, GenericPushNotification notification, bool isImportant);

        Task SendNotifications(IEnumerable<SendNotificationListItem> notifications);

        Task SendPINChangedNotification(int clientId);

        Task SendAdvanceIssued(Guid userId, decimal amount, decimal balance, string date);

        Task SendInternalTransferNotifications(string debtorUserSavingsId, string creditorUserSavingsId, decimal amount, decimal debtorBalance, decimal creditorBalance, string note);

        Task SendPhysicalCardActivated(int clientId);

        Task SendACHDebitNotification(Guid userId, decimal availableBalance, decimal amount, string name);

        Task SendPushToExternalNotification(Guid userId, string accountNumber, decimal requestAmount, decimal accountBalance);

        Task SendCardReorderedNotification(int cardExternalId, string dataLastDigits);

        /// <summary>
        /// Sending notification without saving into db so will not be shown in app's Notifications list.
        /// </summary>
        Task SendStealthyPushNotification(User user, Device device, string notificationText, string deeplink, string sub);

        /// <summary>
        /// Send a notification without saving to DB and without showing it in the phone's Notification center.
        /// </summary>
        Task SendSilentPushNotification(User user, string deeplink, string sub = null);
    }
}
