﻿namespace BNine.Application.Interfaces;

using Aggregates.Banners.Models;
using Domain.Entities.Banners;

/// <summary>
/// Service that provides data for displaying various marketing banners.
/// </summary>
public interface IMarketingBannersService
{
    /// <summary>
    /// Get all banners of type <see cref="TBanner"/> eligible to be shown to user.
    /// </summary>
    /// <returns>DB entries mapped to <see cref="TViewModel"/>.</returns>
    Task<List<TViewModel>> GetWalletScreenBanners<TBanner, TViewModel>()
        where TBanner : BaseMarketingBanner;

    /// <summary>
    /// Make it so that banner is hidden and sent on cooldown.
    /// </summary>
    Task MarkBannerAsSeen(string name, DateTime? customDate = null);

    /// <summary>
    /// Get banner by name. Only returns first active one. Returns null if the name does not exist.
    /// </summary>
    Task<GetWalletScreenTopBannerResponse.BannerInstance> GetBannerByName(string name);

    /// <summary>
    /// Determine which version of the banner should be shown depending on user's Groups.
    /// </summary>
    Task<GetWalletScreenTopBannerResponse.BannerInstance> GetBannerByGroup(string name, Guid[] groups);
}
