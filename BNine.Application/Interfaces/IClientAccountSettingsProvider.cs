﻿namespace BNine.Application.Interfaces;

public interface IClientAccountSettingsProvider
{
    Task<IClientAccountSettings> GetClientAccountSettings(CancellationToken cancellationToken);
}
