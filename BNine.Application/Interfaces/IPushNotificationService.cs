﻿namespace BNine.Application.Interfaces
{
    using System.Threading.Tasks;
    using BNine.Application.Models.Notifications;
    using BNine.Enums;

    public interface IPushNotificationService
    {
        Task<string> CreateRegistrationId();

        Task DeleteRegistration(string registrationId);

        Task RegisterForPushNotifications(string id, DeviceRegistration deviceUpdate);

        Task SendNotification(Notification notification);

        Task SendBulkNotificationToSelectedPlatform(BulkNotification notification, MobilePlatform platform);
    }
}
