﻿namespace BNine.Application.Interfaces
{
    using System;
    using System.Threading;
    using System.Threading.Tasks;
    using BNine.Domain.Entities.Features;

    public interface IFeatureConfigurationService
    {
        Task<FeatureGroup> GetPersonalFeatureConfigAsync(Guid userId, string featureName, string defaultSettingsValue, CancellationToken cancellationToken);

        Task AddUserToGroup(Guid userId, string groupName, CancellationToken cancellationToken);

        Task RemoveUserFromGroup(Guid userId, string groupName, CancellationToken cancellationToken);
    }
}
