﻿namespace BNine.Application.Interfaces
{
    using Domain.Entities.Common;

    public interface IListItem
    {
        string Key
        {
            get; set;
        }

        ICollection<LocalizedText> Value
        {
            get; set;
        }
    }
}
