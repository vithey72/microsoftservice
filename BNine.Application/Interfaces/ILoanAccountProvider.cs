﻿namespace BNine.Application.Interfaces;

using BNine.Application.Models.MBanq;
using BNine.Domain.Entities.User;

public interface ILoanAccountProvider
{
    Task<LoanSummary> TryGetActiveLoanAccount(User user);
}
