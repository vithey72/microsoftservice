﻿namespace BNine.Application.Interfaces;

public interface ICallService
{
    public int? MaxCallsLongLimit
    {
        get;
    }
    public int? MaxCallsShortLimit
    {
        get;
    }

    public Task CallAsync(string to, string body);
}
