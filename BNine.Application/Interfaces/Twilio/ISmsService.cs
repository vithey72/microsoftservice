﻿namespace BNine.Application.Interfaces
{
    using System.Threading.Tasks;

    public interface ISmsService
    {
        Task SendMessageAsync(string to, string body);
    }
}
