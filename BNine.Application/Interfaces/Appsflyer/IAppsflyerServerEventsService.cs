﻿namespace BNine.Application.Interfaces.Appsflyer
{
    using System;
    using System.Threading.Tasks;
    using Enums;

    public interface IAppsflyerServerEventsService
    {
        Task SendEvent(string appsflyerId, Guid anonymousAppsflyerId, string eventName, MobilePlatform platform);
    }
}
