﻿namespace BNine.Application.Interfaces
{
    using System.Threading.Tasks;
    using BNine.Domain.Infrastructure;

    public interface IDomainEventsService
    {
        Task Publish(DomainEvent domainEvent);
    }
}
