﻿namespace BNine.Application.Interfaces
{
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Http;
    using Models.Blob;

    public interface IBlobStorageService
    {
        bool Enabled
        {
            get;
        }

        Task<byte[]> GetFile(string containerName, string url, CancellationToken cancellationToken = default);

        Task<Dictionary<string, byte[]>> GetFiles(string containerName, IEnumerable<string> urls, CancellationToken cancellationToken = default);

        Task<string> UploadFile(string containerName, IFormFile file, string fileId = null, CancellationToken cancellationToken = default);

        Task<IEnumerable<UploadedFileInfo>> UploadFiles(string containerName, IFormFile[] files, string fileId = null, CancellationToken cancellationToken = default);

        Task RemoveFiles(string containerName, IEnumerable<string> urls, CancellationToken cancellationToken = default);

        Task<string> WriteJson(string containerName, string jsonToUpload, string fileName);

        Task RemoveFile(string containerName, string url, CancellationToken cancellationToken = default);

        Task<IEnumerable<UploadedFileInfo>> UploadFile(string containerName, byte[] file, string fileName,
            string contentType, CancellationToken cancellationToken = default);

        /// <summary>
        /// Returns file name and created date list
        /// </summary>
        Task<List<KeyValuePair<string, DateTimeOffset?>>> GetFilesListByFileNamePrefix(string containerName,
            string fileNamePrefix, CancellationToken cancellationToken = default);

        Task<byte[]> DownloadBlob(string containerName,
            string fileName, CancellationToken cancellationToken = default);
    }
}
