﻿namespace BNine.Application.Interfaces.DwhIntegration
{
    using Aggregates.Users.Models.CardLimits;
    using Aggregates.Users.Models.UserAdvancesHistory;
    using Aggregates.Users.Models.UserCashback;
    using BNine.Application.Aggregates.Users.Models.TransactionLimits;
    using BNine.Application.Models.DwhIntegration.LogAdminAction;

    public interface IDwhIntegrationService
    {
        Task LogAdminAction(AdminActionLogEntry logEntry);

        Task<UserTransactionLimitsViewModel> GetUserTransactionLimits(Guid userId);

        Task<UserCardLimitsViewModel> GetUserCardLimits(Guid userId);

        Task<UserCashbackHistoryViewModel> GetUserCashbackInfo(Guid userId);

        Task<AdvancesHistoryViewModel> GetAdvancesHistory(Guid userId);
    }
}
