﻿namespace BNine.Application.Interfaces.DwhIntegration;

using Aggregates.Users.Models.AdvanceSimulation;

public interface IAdvanceSimulationService
{
    Task<DwhAdvanceSimulationViewModel> GetAdvanceSimulation(Guid userId, DwhAdvanceSimulationRequestParameters requestParameters = null);
}
