﻿namespace BNine.Application.Interfaces;

using System;

using System.Threading.Tasks;

public interface IAprilTaxService
{
    Task<string> GetAccessToken(Guid userId, string userEmail, CancellationToken token);

    Task<string> GetLoginUrl(string accessToken, CancellationToken token);
}
