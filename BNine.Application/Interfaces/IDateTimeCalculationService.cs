﻿namespace BNine.Application.Interfaces;

public interface IDateTimeCalculationService
{
    void Initialize((int BoostId, int HoursToDelay)[] hoursToDelayEntries);
    int GetDaysToPayOff(int boostId);
    string GetTargetDateFormatted(int boostId);
    DateTime GetTargetDate(int boostId);
}
