﻿namespace BNine.Application.Interfaces.ServiceBus
{
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using BNine.Application.Models.ServiceBus;

    public interface IServiceBus
    {
        Task Publish(IntegrationEvent @event, CancellationToken cancellationToken = default);

        Task Publish(IList<IntegrationEvent> events, CancellationToken cancellationToken = default);
    }
}
