﻿namespace BNine.Application.Interfaces.Firebase
{
    public interface IDynamicLinkService
    {
        string GetDynamicShortLink(object obj, string linkSuffix);
    }
}
