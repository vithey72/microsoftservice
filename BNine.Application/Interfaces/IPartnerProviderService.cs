﻿namespace BNine.Application.Interfaces;

using Enums;

/// <summary>
/// Service that provides which incarnation of the app is currently running.
/// </summary>
public interface IPartnerProviderService
{
    PartnerApp GetPartner();
}
