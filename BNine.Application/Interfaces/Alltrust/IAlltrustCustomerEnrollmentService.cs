﻿namespace BNine.Application.Interfaces.Alltrust
{
    using System.Threading;
    using System.Threading.Tasks;
    using BNine.Domain.Entities.User;

    public interface IAlltrustCustomerEnrollmentService
    {
        Task EnrollCustomer(User user, CancellationToken cancellationToken);
    }
}
