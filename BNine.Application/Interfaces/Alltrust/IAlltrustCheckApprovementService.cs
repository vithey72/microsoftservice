﻿namespace BNine.Application.Interfaces.Alltrust
{
    using System;
    using System.Threading;
    using System.Threading.Tasks;
    using Aggregates.CheckCashing.Models.Transaction;
    using BNine.Application.Aggregates.CheckCashing.Models;
    using Enums.CheckCashing;

    public interface IAlltrustCheckApprovementService
    {
        /// <summary>
        /// Post transaction to Alltrust and save it locally.
        /// </summary>
        Task<AlltrustTransaction> InitializeTransaction(AlltrustTransaction transaction, CancellationToken ct);

        /// <summary>
        /// Get transaction from Alltrust.
        /// </summary>
        Task<AlltrustTransaction> GetTransaction(string alltrustTransactionId, CancellationToken ct);

        /// <summary>
        /// Cancel transaction locally and in Alltrust.
        /// </summary>
        Task CancelTransaction(string alltrustTransactionId, CancellationToken ct);

        /// <summary>
        /// Update check fields and maker.
        /// </summary>
        Task<AlltrustTransaction> UpdateCheck(AlltrustTransaction transaction, bool updateMaker, CancellationToken ct);

        /// <summary>
        /// Add redemption images of a voided check to transaction, and accept transaction if ok.
        /// </summary>
        Task RedeemCheck(string transactionId, CheckRedemptionRequest request, CancellationToken ct);

        /// <summary>
        /// Set the result of transaction.
        /// </summary>
        Task SetTransactionResult(string transactionId, CheckReviewResult result, CancellationToken ct);

        /// <summary>
        /// Parse check details. Only requires 'processed' (Check21 compliant) front and back images.
        /// </summary>
        Task<CheckOcrResult> OcrCheck(CheckScans scans, CancellationToken ct);

        /// <summary>
        /// Parse MICR and get normalized check routing properties.
        /// </summary>
        Task<MicrParseResult> ParseMicr(string micr, CancellationToken ct);

        /// <summary>
        /// Find check maker details by routing + account number pair.
        /// there might be several makers created with same aba + account number pair
        /// </summary>
        Task<Maker[]> GetMakers(string aba, string accountNumber, CancellationToken ct);

        /// <summary>
        /// Check if this check was already submitted.
        /// </summary>
        Task<CheckIsDuplicateResponse> FindCheckDuplicates(string aba, string accountNumber, string checkNumber, DateTime date, CancellationToken ct);

        /// <summary>
        /// Get a decision on whether to accept the check.
        /// </summary>
        Task<CheckDecisionResponse> GetCheckDecision(CheckDecisionRequest request, CancellationToken ct);

        /// <summary>
        /// Get Check21-compliant tiff images of check.
        /// </summary>
        Task<CheckImages> GetDepositCheckImages(string checkId, CancellationToken ct);
    }
}
