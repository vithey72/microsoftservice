﻿namespace BNine.Application.Interfaces.Alltrust
{
    using System.Threading.Tasks;
    using Aggregates.CheckCashing.Models;
    using Aggregates.CheckCashing.Models.Transaction;
    using Aggregates.CheckCashing.Queries;

    public interface ICheckProblemsLoggingService
    {
        public Task<CheckCashingInitializationFailedResponse> DenyAsMicrParseFail(InitializeCheckCashingTransactionQuery request,
            CheckOcrResult ocrResult, MicrParseResult micrParseResult);

        public Task<CheckCashingInitializationFailedResponse> DenyAsOcrFail(
            InitializeCheckCashingTransactionQuery request, CheckOcrResult ocrResult);

        public Task<CheckCashingInitializationFailedResponse> DenyAsAmountRecognitionFail(
            InitializeCheckCashingTransactionQuery request, CheckOcrResult ocrResult);

        public Task<CheckCashingInitializationFailedResponse> DenyAsMicrRecognitionFail(
            InitializeCheckCashingTransactionQuery request, CheckOcrResult ocrResult);

        public Task<string> SaveOperationDataToStorage<T>(string prefix, string message, T data, bool success = false);

        public Task<CheckCashingDeniedResponse> DenyByAlltrustDecisioning(AlltrustTransaction transaction, CheckDecisionResponse decision);

        public Task<CheckCashingDeniedResponse> DenyAsDuplicate(AlltrustTransaction transaction, CheckIsDuplicateResponse duplicatesCheckResult);
    }
}
