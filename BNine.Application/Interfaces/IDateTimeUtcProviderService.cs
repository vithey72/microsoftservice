﻿namespace BNine.Application.Interfaces;

public interface IDateTimeUtcProviderService
{
    DateTime UtcNow();
}
