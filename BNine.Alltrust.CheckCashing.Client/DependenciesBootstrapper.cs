﻿namespace BNine.Alltrust.Client
{
    using System;
    using BNine.Constants;
    using Microsoft.Extensions.DependencyInjection;
    using Microsoft.Extensions.Configuration;
    using BNine.Application.Interfaces.Alltrust;
    using BNine.Alltrust.Client.Services;

    public static class DependenciesBootstrapper
    {
        public static IServiceCollection AddAlltrustCheckCashingClient(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddTransient<IAlltrustCustomerEnrollmentService, AlltrustCustomerEnrollmentService>();
            services.AddTransient<IAlltrustCheckApprovementService, AlltrustCheckApprovementService>();
            services.AddTransient<ICheckProblemsLoggingService, CheckProblemsLoggingService>();
            services.AddHttpClient(HttpClientName.Alltrust, client =>
            {
                client.BaseAddress = new Uri(configuration["AlltrustSettings:ApiUrl"]);
            });

            return services;
        }
    }
}
