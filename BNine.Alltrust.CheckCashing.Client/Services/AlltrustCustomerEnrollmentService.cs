﻿namespace BNine.Alltrust.Client.Services
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Net;
    using System.Net.Http;
    using System.Threading;
    using System.Threading.Tasks;
    using BNine.Alltrust.Client.Models.CustomerEnrollment;
    using BNine.Alltrust.Client.Models.CustomerEnrollment.Customer;
    using BNine.Application.Interfaces.Alltrust;
    using BNine.Common;
    using BNine.Constants;
    using BNine.Domain.Entities.User;
    using BNine.Settings;
    using Microsoft.Extensions.Logging;
    using Microsoft.Extensions.Options;

    public class AlltrustCustomerEnrollmentService : AlltrustBaseService, IAlltrustCustomerEnrollmentService
    {
        public AlltrustCustomerEnrollmentService(
            IOptions<AlltrustSettings> options,
            IHttpClientFactory clientFactory,
            ILogger<AlltrustCustomerEnrollmentService> logger)
            : base(options, clientFactory, logger)
        {
        }

        public async Task EnrollCustomer(User user, CancellationToken cancellationToken)
        {
            var client = base.GetAuthorizedClient();
            var body = GetEnrollmentRequest(user);
            var content = base.CreateRequestContent(body);

            var response = await client.PostAsync("/v1/customers", content, cancellationToken);
            if (response.IsSuccessStatusCode)
            {
                var enrollmentResponse = await SerializationHelper.GetResponseContent<EnrollCustomerResponse>(response);
                var now = DateTime.Now;
                user.AlltrustCustomerSettings = new AlltrustCustomerSettings
                {
                    AlltrustCustomerId = enrollmentResponse.Id,
                    CreatedAt = now,
                    UpdatedAt = now,
                };
            }
            else
            {
                if (response.StatusCode == HttpStatusCode.Conflict)
                {
                    // Customer already exists - no need to do anything.
                    // TODO: sync customer changes
                }
                else
                {
                    await HandleErrors(response);
                }
            }
        }

        private EnrollCustomerRequest GetEnrollmentRequest(User user)
        {
            return new EnrollCustomerRequest
            {
                LocationId = Settings.LocationId,
                FirstName = user.FirstName,
                LastName = user.LastName,
                DateOfBirth = user.DateOfBirth,
                CellPhone = user.Phone.Trim('+'),
                Address = new CustomerAddress
                {
                    Address1 = user.Address.AddressLine,
                    City = user.Address.City,
                    State = user.Address.State,
                    Zip = user.Address.ZipCode,
                },
                CustomerIdentification = ConvertIdentification(user.Document),
                Status = "neutral",
                RecMarketingMessages = false,
                MobileCard = new MobileCard
                {
                    CardAllowed = true,
                    MobileAllowed = true,
                },
                IdVerificationInfo = new IdVerificationInfo { Verified = true },
                NoDuplicateCheck = Settings.DisableCustomerDuplicatesCheck,
                Settings = new EnrollmentSettings { BypassIdValidation = true }
            };
        }

        private CustomerIdentification ConvertIdentification(Document document)
        {
            var b9ToAlltrustIdMap = new Dictionary<string, string>
            {
                { ClientIdentifierDocumentTypes.Passport, AlltrustIdentificationDocuments.Passport },
                { ClientIdentifierDocumentTypes.DriverLicense, AlltrustIdentificationDocuments.DriverLicense },
                { ClientIdentifierDocumentTypes.StateIdentityCard, AlltrustIdentificationDocuments.IdCard },
                { ClientIdentifierDocumentTypes.ConsularId, AlltrustIdentificationDocuments.ConsularCard },
                { ClientIdentifierDocumentTypes.ForeignPassport, AlltrustIdentificationDocuments.NationalId },
                { ClientIdentifierDocumentTypes.ForeignDriverLicense, AlltrustIdentificationDocuments.ForeignDriversLicense },
                { ClientIdentifierDocumentTypes.Other, AlltrustIdentificationDocuments.Other },
            };
            var alltrustDocumentType = b9ToAlltrustIdMap.ContainsKey(document.TypeKey) ?
                b9ToAlltrustIdMap[document.TypeKey] :
                document.TypeKey;

            var idTypesWithCountryIssuer = new[]
            {
                AlltrustIdentificationDocuments.Passport,
                AlltrustIdentificationDocuments.ConsularCard,
                AlltrustIdentificationDocuments.NationalId,
                AlltrustIdentificationDocuments.ForeignDriversLicense,
            };
            var issuer = idTypesWithCountryIssuer.Contains(alltrustDocumentType) ?
                document.IssuingCountry :
                document.IssuingState;

            return new CustomerIdentification
            {
                IdType = alltrustDocumentType,
                IdNumber = document.Number,
                Issuer = issuer,
            };

        }

        protected async Task HandleErrors(HttpResponseMessage response)
        {
            var result = await response.Content.ReadAsStringAsync();
            if (response.StatusCode == HttpStatusCode.Unauthorized)
            {
                throw new UnauthorizedAccessException(result);
            }
            else if (response.StatusCode == HttpStatusCode.BadRequest)
            {
                throw new FormatException(result);
            }
            else if (response.StatusCode == HttpStatusCode.Conflict)
            {
                return;
            }
            throw new Exception(result);
        }
    }
}
