﻿namespace BNine.Alltrust.Client.Services
{
    using System;
    using System.Threading.Tasks;
    using Application.Aggregates.CheckCashing.Models;
    using Application.Aggregates.CheckCashing.Models.Transaction;
    using Application.Aggregates.CheckCashing.Queries;
    using Application.Interfaces;
    using Application.Interfaces.Alltrust;
    using Constants;
    using Enums.CheckCashing;
    using Microsoft.Extensions.Logging;
    using Newtonsoft.Json;

    public class CheckProblemsLoggingService : ICheckProblemsLoggingService
    {
        private readonly ILogger<CheckProblemsLoggingService> _logger;
        private readonly IBlobStorageService _blobStorageService;
        private readonly ICurrentUserService _currentUser;

        public CheckProblemsLoggingService(
            ILogger<CheckProblemsLoggingService> logger,
            IBlobStorageService blobStorageService,
            ICurrentUserService currentUser)
        {
            _logger = logger;
            _blobStorageService = blobStorageService;
            _currentUser = currentUser;
        }

        public async Task<CheckCashingInitializationFailedResponse> DenyAsMicrParseFail(InitializeCheckCashingTransactionQuery request, CheckOcrResult ocrResult,
            MicrParseResult micrParseResult)
        {
            var problemMessage =
                $"MICR parse {nameof(CheckDataErrorType.AlltrustMicrParseFail)} for user {_currentUser.UserId}. " +
                $"Check date: {ocrResult.CheckDate}, Check amount: {ocrResult.Amount}, " +
                $"Raw MICR: {micrParseResult.RawMicr}, Status message: {micrParseResult.StatusMessage}, " +
                $"IsParseError: {micrParseResult.ParseError}, HasUnrecognizedChars: {micrParseResult.ContainsUnrecognizedSymbols}. ";
            var problemMessageWithBlobName = await SaveOperationDataToStorage("cde-micr", problemMessage, request);
            _logger.LogInformation(problemMessageWithBlobName);
            return new CheckCashingInitializationFailedResponse(CheckDataErrorType.AlltrustMicrParseFail);
        }

        public async Task<CheckCashingInitializationFailedResponse> DenyAsMicrRecognitionFail(InitializeCheckCashingTransactionQuery request, CheckOcrResult ocrResult)
        {
            var problemMessage =
                $"{nameof(CheckDataErrorType.MicrUnrecognized)}: OCR returned empty MICR for user {_currentUser.UserId}. " +
                $"Check date: {ocrResult.CheckDate}, Check amount: {ocrResult.Amount}";
            var problemMessageWithBlobName = await SaveOperationDataToStorage("cde-missing-micr", problemMessage, request);
            _logger.LogInformation(problemMessageWithBlobName);
            return new CheckCashingInitializationFailedResponse(CheckDataErrorType.MicrUnrecognized);
        }

        public async Task<CheckCashingInitializationFailedResponse> DenyAsAmountRecognitionFail(InitializeCheckCashingTransactionQuery request, CheckOcrResult ocrResult)
        {
            var problemMessage =
                $"{nameof(CheckDataErrorType.AmountUnrecognized)}: OCR returned amount $0 for user {_currentUser.UserId}. " +
                $"Check date: {ocrResult.CheckDate}, Check amount: {ocrResult.Amount}, Check MICR: {ocrResult.Micr}";
            var problemMessageWithBlobName = await SaveOperationDataToStorage("cde-amount", problemMessage, request);
            _logger.LogInformation(problemMessageWithBlobName);
            return new CheckCashingInitializationFailedResponse(CheckDataErrorType.AmountUnrecognized);
        }

        public async Task<CheckCashingInitializationFailedResponse> DenyAsOcrFail(InitializeCheckCashingTransactionQuery request, CheckOcrResult ocrResult)
        {
            var problemMessage = $"OCR {nameof(CheckDataErrorType.AlltrustOcrFail)} for user {_currentUser.UserId}. " +
                                 $"Check date: {ocrResult.CheckDate}, Check amount: {ocrResult.Amount}, Check MICR: {ocrResult.Micr} " +
                                 $"Reasons: {string.Join("; ", ocrResult.UsabilityFailReasons)}.";
            var problemMessageWithBlobName = await SaveOperationDataToStorage("cde-ocr", problemMessage, request);
            _logger.LogInformation(problemMessageWithBlobName);
            return new CheckCashingInitializationFailedResponse(CheckDataErrorType.AlltrustOcrFail);
        }

        public async Task<CheckCashingDeniedResponse> DenyByAlltrustDecisioning(AlltrustTransaction transaction, CheckDecisionResponse decision)
        {
            var problemMessage =
                $"Check was rejected by Alltrust for user {_currentUser.UserId}. Check ABA: {transaction.Check.Maker.AbaNumber}, " +
                $"account number: {transaction.Check.Maker.AccountNumber}, check number: {transaction.Check.CheckNumber}, date: {transaction.Check.CheckDate}. " +
                $"Score: {decision.Score}, Required for check of type {transaction.Check.Kind}: {decision.LowRange}-{decision.HighRange}. " +
                $"Decision url: /v1/decisioning/decision/{decision.DecisionId}. ";
            var problemMessageWithBlobName = await SaveOperationDataToStorage("reject", problemMessage, transaction);
            _logger.LogInformation(problemMessageWithBlobName);
            return new CheckCashingDeniedResponse { Reason = CheckCashingDenyReason.CheckRejected };
        }

        public async Task<CheckCashingDeniedResponse> DenyAsDuplicate(AlltrustTransaction transaction, CheckIsDuplicateResponse duplicatesCheckResult)
        {
            var problemMessage =
                $"Found check duplicate for user {_currentUser.UserId}. Check ABA: {transaction.Check.Maker.AbaNumber}, " +
                $"account number: {transaction.Check.Maker.AccountNumber}, check number: {transaction.Check.CheckNumber}, date: {transaction.Check.CheckDate}. " +
                $"IsMultiple: {duplicatesCheckResult.MultipleDuplicates}. ";
            var problemMessageWithBlobName = await SaveOperationDataToStorage("dupe", problemMessage, transaction);
            _logger.LogInformation(problemMessageWithBlobName);
            return new CheckCashingDeniedResponse { Reason = CheckCashingDenyReason.CheckDuplicate };
        }

        public async Task<string> SaveOperationDataToStorage<T>(string prefix, string message, T data, bool success = false)
        {
            if (!_blobStorageService.Enabled)
            {
                return message + " Blob not saved.";
            }

            var blobGuid = prefix + "-" + Guid.NewGuid();
            message += $"Blob with request: {blobGuid}";
            var name = success ? ContainerNames.SuccessfulChecks : ContainerNames.FailedChecks;
            await _blobStorageService.WriteJson(
                name,
                JsonConvert.SerializeObject(new
                {
                    Message = message,
                    Request = data
                }),
                blobGuid);
            return blobGuid;
        }
    }
}
