﻿namespace BNine.Alltrust.Client.Services
{
    using System;
    using System.Collections.Generic;
    using System.Net;
    using System.Net.Http;
    using System.Threading;
    using System.Threading.Tasks;
    using Application.Aggregates.CheckCashing.Models.Transaction;
    using BNine.Application.Aggregates.CheckCashing.Models;
    using BNine.Application.Interfaces.Alltrust;
    using BNine.Common;
    using BNine.Settings;
    using Enums.CheckCashing;
    using Microsoft.Extensions.Logging;
    using Microsoft.Extensions.Options;

    public class AlltrustCheckApprovementService : AlltrustBaseService, IAlltrustCheckApprovementService
    {
        public AlltrustCheckApprovementService(
            IOptions<AlltrustSettings> options,
            IHttpClientFactory clientFactory,
            ILogger<AlltrustCheckApprovementService> logger)
            : base(options, clientFactory, logger)
        {
        }

        /// <inheritdoc />
        public async Task<AlltrustTransaction> InitializeTransaction(AlltrustTransaction transaction, CancellationToken ct)
        {
            var client = base.GetAuthorizedClient();
            var content = base.CreateRequestContent(transaction);

            var response = await client.PostAsync("/v1/transactions", content, ct);
            if (!response.IsSuccessStatusCode)
            {
                if (response.StatusCode == HttpStatusCode.Conflict) { return null; }

                throw await HandleErrors(response);
            }

            return await SerializationHelper.GetResponseContent<AlltrustTransaction>(response);
        }

        /// <inheritdoc />
        public async Task<AlltrustTransaction> GetTransaction(string alltrustTransactionId, CancellationToken ct)
        {
            var client = base.GetAuthorizedClient();

            var response = await client.GetAsync($"/v1/transactions/{alltrustTransactionId}", ct);

            if (!response.IsSuccessStatusCode)
            {
                var result = await response.Content.ReadAsStringAsync();
                if (result.Contains("transaction not found")) { return null; }
                throw await HandleErrors(response, alltrustTransactionId);
            }

            return await SerializationHelper.GetResponseContent<AlltrustTransaction>(response);
        }

        /// <inheritdoc />
        public async Task CancelTransaction(string alltrustTransactionId, CancellationToken ct)
        {
            var client = base.GetAuthorizedClient();

            var response = await client.PutAsync($"/v1/transactions/cancel/{alltrustTransactionId}", null, ct);

            if (!response.IsSuccessStatusCode)
            {
                throw await HandleErrors(response, alltrustTransactionId);
            }
        }

        /// <inheritdoc />
        public async Task<AlltrustTransaction> UpdateCheck(AlltrustTransaction transaction, bool updateMaker, CancellationToken ct)
        {
            var client = base.GetAuthorizedClient();
            if (updateMaker)
            {
                var makerContent = base.CreateRequestContent(transaction.Check.Maker);
                var makerResponse = await client.PutAsync($"/v1/makers/{Settings.BusinessId}", makerContent, ct);
                if (!makerResponse.IsSuccessStatusCode)
                {
                    throw await HandleErrors(makerResponse, null);
                }

                transaction.Check.Maker = await SerializationHelper.GetResponseContent<Maker>(makerResponse);
            }

            var checkContent = base.CreateRequestContent(transaction.Check);
            var response = await client.PutAsync("/v1/checks", checkContent, ct);
            if (!response.IsSuccessStatusCode)
            {
                throw await HandleErrors(response, transaction.Id);
            }

            var check = await SerializationHelper.GetResponseContent<Check>(response);
            transaction.Check = check;
            return transaction;
        }

        public async Task RedeemCheck(string transactionId, CheckRedemptionRequest request, CancellationToken ct)
        {
            var client = base.GetAuthorizedClient();
            var content = base.CreateRequestContent(request);

            var response = await client.PutAsync($"/v1/transactions/redemption/images/{transactionId}", content, ct);
            if (!response.IsSuccessStatusCode)
            {
                throw await HandleErrors(response, transactionId);
            }
        }

        public async Task SetTransactionResult(string transactionId, CheckReviewResult result, CancellationToken ct)
        {
            var client = base.GetAuthorizedClient();
            var resultLowercase = result.ToString().ToLower();

            var response = await client.PutAsync($"/v1/transactions/{transactionId}/{resultLowercase}", null, ct);
            if (!response.IsSuccessStatusCode)
            {
                throw await HandleErrors(response, transactionId);
            }
        }

        /// <inheritdoc />
        public async Task<CheckOcrResult> OcrCheck(CheckScans scans, CancellationToken ct)
        {
            var client = base.GetAuthorizedClient();
            var content = base.CreateRequestContent(new { front_image = scans.FrontImageProcessed, back_image = scans.BackImageProcessed });

            var response = await client.PostAsync($"/v2/checks/ocr/{Settings.LocationId}", content, ct);
            if (!response.IsSuccessStatusCode)
            {
                throw await HandleErrors(response);
            }

            return await SerializationHelper.GetResponseContent<CheckOcrResult>(response);

        }

        /// <inheritdoc />
        public async Task<MicrParseResult> ParseMicr(string micr, CancellationToken ct)
        {
            var client = base.GetAuthorizedClient();
            var response = await client.GetAsync($"/v1/checks/micr-parse/{micr}", ct);
            if (!response.IsSuccessStatusCode)
            {
                throw await HandleErrors(response);
            }

            return await SerializationHelper.GetResponseContent<MicrParseResult>(response);
        }

        public async Task<Maker[]> GetMakers(string aba, string accountNumber, CancellationToken ct)
        {
            var client = base.GetAuthorizedClient();
            var response = await client.GetAsync($"/v1/makers/{aba}/{accountNumber}/{Settings.BusinessId} ", ct);
            if (!response.IsSuccessStatusCode)
            {
                throw await HandleErrors(response);
            }

            return await SerializationHelper.GetResponseContent<Maker[]>(response);
        }

        public async Task<CheckIsDuplicateResponse> FindCheckDuplicates(string aba, string accountNumber, string checkNumber, DateTime date, CancellationToken ct)
        {
            var client = base.GetAuthorizedClient();
            var content = base.CreateRequestContent(new
            {
                aba_number = aba,
                account_number = accountNumber,
                check_number = checkNumber,
                check_date = date,
            });
            var response = await client.PostAsync("/v1/checks/is-duplicate", content, ct);
            if (!response.IsSuccessStatusCode && response.StatusCode != HttpStatusCode.Conflict)
            {
                throw await HandleErrors(response);
            }

            return await SerializationHelper.GetResponseContent<CheckIsDuplicateResponse>(response);
        }

        /// <inheritdoc />
        public async Task<CheckDecisionResponse> GetCheckDecision(CheckDecisionRequest request, CancellationToken ct)
        {
            var client = base.GetAuthorizedClient();
            var content = base.CreateRequestContent(request);
            var response = await client.PostAsync("/v1/decisioning/score", content, ct);
            if (!response.IsSuccessStatusCode)
            {
                throw await HandleErrors(response);
            }

            return await SerializationHelper.GetResponseContent<CheckDecisionResponse>(response);
        }

        public async Task<CheckImages> GetDepositCheckImages(string checkId, CancellationToken ct)
        {
            var client = base.GetAuthorizedClient();
            var response = await client.GetAsync($"/v1/deposit/images/{checkId}", ct);
            if (!response.IsSuccessStatusCode)
            {
                throw await HandleErrors(response);
            }

            return await SerializationHelper.GetResponseContent<CheckImages>(response);
        }

        private async Task<Exception> HandleErrors(HttpResponseMessage response, string alltrustTransactionId = null)
        {
            var result = await response.Content.ReadAsStringAsync();
            if (response.StatusCode == HttpStatusCode.Unauthorized)
            {
                throw new UnauthorizedAccessException(result);
            }
            else if (response.StatusCode == HttpStatusCode.BadRequest)
            {
                throw new FormatException(result);
            }
            else if (response.StatusCode == HttpStatusCode.NotFound)
            {
                if (result.Contains("transaction not found"))
                {
                    throw new KeyNotFoundException(result + "; transactionId=" + alltrustTransactionId);
                }
                throw new ArgumentException(result, nameof(AlltrustSettings.ApiUrl));
            }
            throw new Exception(result);
        }
    }
}
