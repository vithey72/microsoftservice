﻿namespace BNine.Alltrust.Client.Services
{
    using System;
    using System.Net.Http;
    using System.Net.Http.Headers;
    using System.Text;
    using BNine.Common;
    using BNine.Constants;
    using BNine.Settings;
    using Microsoft.Extensions.Logging;
    using Microsoft.Extensions.Options;
    using Newtonsoft.Json;

    public class AlltrustBaseService
    {
        protected AlltrustSettings Settings
        {
            get;
        }

        protected IHttpClientFactory ClientFactory
        {
            get;
        }

        protected ILogger Logger
        {
            get;
        }

        protected AlltrustBaseService(IOptions<AlltrustSettings> options, IHttpClientFactory clientFactory, ILogger logger)
        {
            Settings = options.Value;
            ClientFactory = clientFactory;
            Logger = logger;
        }

        protected HttpClient GetAuthorizedClient()
        {
            var client = ClientFactory.CreateClient(HttpClientName.Alltrust);
            var byteArray = Encoding.ASCII.GetBytes(Settings.ApiKey + ":" + Settings.Password);
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Convert.ToBase64String(byteArray));
            return client;
        }

        protected StringContent CreateRequestContent(object body)
        {
            var dateFormatSettings = new JsonSerializerSettings { DateFormatString = "yyyy-MM-dd" };
            var content = SerializationHelper.GetRequestContent(body, dateFormatSettings);
            return content;
        }
    }
}
