﻿namespace BNine.Alltrust.Client.Models.CustomerEnrollment
{
    using System;
    using BNine.Alltrust.Client.Models.CustomerEnrollment.Customer;
    using Newtonsoft.Json;

    internal class EnrollCustomerRequest
    {
        [JsonProperty("location_id")]
        public string LocationId
        {
            get; set;
        }

        [JsonProperty("first_name")]
        public string FirstName
        {
            get; set;
        }

        [JsonProperty("last_name")]
        public string LastName
        {
            get; set;
        }

        [JsonProperty("date_of_birth")]
        public DateTime? DateOfBirth
        {
            get; set;
        }

        [JsonProperty("address")]
        public CustomerAddress Address
        {
            get; set;
        }

        /// <summary>
        /// Cell phone number without the "+".
        /// </summary>
        [JsonProperty("cell_phone")]
        public string CellPhone
        {
            get; set;
        }

        [JsonProperty("status")]
        public string Status
        {
            get; set;
        }

        [JsonProperty("rec_marketing_messages")]
        public bool RecMarketingMessages
        {
            get; set;
        }

        /// <summary>
        /// Passport or SSN details.
        /// </summary>
        [JsonProperty("customer_identification")]
        public CustomerIdentification CustomerIdentification
        {
            get; set;
        }

        [JsonProperty("no_duplicate_check")]
        public bool NoDuplicateCheck
        {
            get; set;
        }

        [JsonProperty("id_verification_info")]
        public IdVerificationInfo IdVerificationInfo
        {
            get; set;
        }

        [JsonProperty("mobile_card")]
        public MobileCard MobileCard
        {
            get; set;
        }

        [JsonProperty("settings")]
        public EnrollmentSettings Settings
        {
            get; set;
        }
    }
}
