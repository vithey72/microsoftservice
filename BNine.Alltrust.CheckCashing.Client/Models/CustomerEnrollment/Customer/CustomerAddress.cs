﻿namespace BNine.Alltrust.Client.Models.CustomerEnrollment.Customer
{
    using Newtonsoft.Json;

    internal class CustomerAddress
    {
        [JsonProperty("address1")]
        public string Address1
        {
            get; set;
        }

        [JsonProperty("city")]
        public string City
        {
            get; set;
        }

        [JsonProperty("state")]
        public string State
        {
            get; set;
        }

        [JsonProperty("zip")]
        public string Zip
        {
            get; set;
        }
    }
}
