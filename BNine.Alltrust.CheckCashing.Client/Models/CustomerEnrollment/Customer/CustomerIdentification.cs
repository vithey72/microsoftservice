﻿namespace BNine.Alltrust.Client.Models.CustomerEnrollment.Customer
{
    using System;
    using Newtonsoft.Json;

    internal class CustomerIdentification
    {
        [JsonProperty("id_type")]
        public string IdType
        {
            get; set;
        }

        [JsonProperty("id_number")]
        public string IdNumber
        {
            get; set;
        }

        [JsonProperty("issuer")]
        public string Issuer
        {
            get; set;
        }

        [JsonProperty("expiration_date")]
        public DateTime? ExpirationDate
        {
            get; set;
        }
    }
}
