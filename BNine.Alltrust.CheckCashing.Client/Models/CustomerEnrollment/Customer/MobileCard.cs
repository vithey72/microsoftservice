﻿namespace BNine.Alltrust.Client.Models.CustomerEnrollment.Customer
{
    using Newtonsoft.Json;

    internal class MobileCard
    {
        [JsonProperty("mobile_allowed")]
        public bool MobileAllowed
        {
            get; set;
        }

        [JsonProperty("card_allowed")]
        public bool CardAllowed
        {
            get; set;
        }
    }
}
