﻿namespace BNine.Alltrust.Client.Models.CustomerEnrollment.Customer
{
    using Newtonsoft.Json;

    internal class EnrollmentSettings
    {
        [JsonProperty("bypass_id_validation")]
        public bool BypassIdValidation
        {
            get; set;
        }
    }
}
