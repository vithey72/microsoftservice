﻿namespace BNine.Alltrust.Client.Models.CustomerEnrollment.Customer
{
    using Newtonsoft.Json;

    internal class IdVerificationInfo
    {

        [JsonProperty("verified")]
        public bool Verified
        {
            get; set;
        }
    }
}
