﻿namespace BNine.Alltrust.Client.Models.CustomerEnrollment
{
    using Newtonsoft.Json;

    internal class EnrollCustomerResponse : EnrollCustomerRequest
    {
        [JsonProperty("id")]
        public string Id
        {
            get; set;
        }
    }
}
