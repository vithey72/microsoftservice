﻿namespace BNine.BlobStorage.Services;

using Application.Interfaces;
using Constants;
using Enums;

public class ClientSelfieDocumentsService : IClientSelfieDocumentsService
{
    private readonly IBlobStorageService _blobStorageService;
    public ClientSelfieDocumentsService(IBlobStorageService blobStorageService)
    {
        _blobStorageService = blobStorageService;
    }

    public async Task UploadDocument(Guid clientId,
        byte[] fileData, string contentType, string fileExtension, CancellationToken cancellationToken = default)
    {
        // Format in which client selfies stored in Azure Blob Storage
        var fileName = clientId + fileExtension;

        await _blobStorageService.UploadFile(ContainerNames.ClientSelfieContainer,
            fileData, fileName, contentType, cancellationToken);
    }

    public async Task UploadDocument(Guid clientId,
        byte[] fileData, string contentType, string fileExtension, SelfieInitiatingFlow selfieInitiatingFlow,
        CancellationToken cancellationToken = default)
    {
        // Format in which client selfies stored in Azure Blob Storage
        var fileName = Path.Combine(selfieInitiatingFlow.ToString(), string.Concat(clientId, fileExtension));

        await _blobStorageService.UploadFile(ContainerNames.ClientSelfieContainerWithType,
            fileData, fileName, contentType, cancellationToken);
    }
}
