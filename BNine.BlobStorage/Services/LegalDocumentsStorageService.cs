﻿namespace BNine.BlobStorage.Services;

using System.Web;
using Application.Aggregates.StaticDocuments.Commands;
using Application.Interfaces;
using Application.Interfaces.LegalDocuments;
using Constants;
using Domain.Entities.Settings;

public class LegalDocumentsStorageService : ILegalDocumentsStorageService
{
    private readonly IBlobStorageService _blobStorageService;

    public LegalDocumentsStorageService(IBlobStorageService blobStorageService)
    {
        this._blobStorageService = blobStorageService;
    }

    public async Task<string> UploadLegalDocument(StaticDocument existingDocument,
        UpdateLegalDocumentCommand request,
        CancellationToken ctx = default)
    {
        var extension = request.FileExtension;
        var contentType = request.ContentType;

        // upload file to dynamic path for mobile clients
        var dynamicDocumentName = ToDynamicBlobStorageName(existingDocument, extension);
        var uploadedDynamicFileInfo = (
            await _blobStorageService.UploadFile(
            ContainerNames.SharedContainer,
            request.FileData,
            dynamicDocumentName,
            contentType,
            ctx)).FirstOrDefault();

        if (uploadedDynamicFileInfo == null)
        {
            throw new ArgumentException($"{nameof(uploadedDynamicFileInfo)}");
        }
        // upload file to static path for Zendesk
        var staticDocumentName = ToStaticBlobStorageName(existingDocument, extension);
        var uploadedStaticFileInfo = (
            await _blobStorageService.UploadFile(
                containerName: ContainerNames.SharedContainer,
                file: request.FileData,
                fileName: staticDocumentName,
                contentType,
                ctx
                )).FirstOrDefault();

        if (uploadedStaticFileInfo == null)
        {
            throw new ArgumentException($"{nameof(uploadedStaticFileInfo)}");
        }
        return HttpUtility.UrlDecode(uploadedDynamicFileInfo.Url);
    }

    private string ToDynamicBlobStorageName(StaticDocument document, string extension)
    {
        return ToBlobStorageName(document, extension, LegalDocumentLinkType.Dynamic);
    }

    private string ToStaticBlobStorageName(StaticDocument document, string extension)
    {
        return ToBlobStorageName(document, extension, LegalDocumentLinkType.Static);
    }
    private string ToBlobStorageName(StaticDocument document, string extension, LegalDocumentLinkType legalDocumentLinkType)
    {
       return legalDocumentLinkType switch
        {
            LegalDocumentLinkType.Dynamic =>
                $"{document.Title} {DateTimeOffset.UtcNow:dd-MM-yyyy.hh-mm}{extension}",
            LegalDocumentLinkType.Static =>
                string.Join("/", document.StaticStoragePath.Split("/")[^2..]),
            _ => throw new ArgumentOutOfRangeException(nameof(legalDocumentLinkType), legalDocumentLinkType, null)
        };

    }


    private enum LegalDocumentLinkType
    {
        Dynamic,
        Static
    }

}
