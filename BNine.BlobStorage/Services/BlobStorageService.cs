﻿namespace BNine.BlobStorage.Services
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using Application.Interfaces;
    using Application.Models.Blob;
    using Azure.Storage.Blobs;
    using Azure.Storage.Blobs.Models;
    using Microsoft.AspNetCore.Http;
    using Microsoft.Extensions.Options;
    using Settings.Blob;

    public class BlobStorageService : IBlobStorageService
    {
        public bool Enabled => !string.IsNullOrEmpty(_configuration.ConnectionString);

        private readonly BlobStorageSettings _configuration;

        public BlobStorageService(IOptions<BlobStorageSettings> configuration)
        {
            _configuration = configuration.Value;
        }

        public async Task<IEnumerable<UploadedFileInfo>> UploadFiles(string containerName, IFormFile[] files,
            string fileId = null, CancellationToken cancellationToken = default)
        {
            var blobServiceClient = GetBlobClient(containerName);
            BlobContainerClient containerClient;

            try
            {
                containerClient = await blobServiceClient.CreateBlobContainerAsync(containerName, cancellationToken: cancellationToken);
            }
            catch
            {
                containerClient = blobServiceClient.GetBlobContainerClient(containerName);
            }

            var urls = new List<UploadedFileInfo>();
            foreach (var file in files)
            {
                var fileName = fileId ?? Guid.NewGuid() + file.FileName;
                var blobClient = containerClient.GetBlobClient(fileName);
                var url = await UploadFile(blobClient, file);
                urls.Add(new UploadedFileInfo { Url = url, ContentType = file.ContentType, FileName = file.FileName });
            }

            return urls;
        }

        public async Task<string> UploadFile(string containerName, IFormFile file, string fileId = null, CancellationToken cancellationToken = default)
        {
            var urls = await UploadFiles(containerName, new[] { file }, fileId, cancellationToken);
            return urls.First().Url;
        }

        public async Task<Dictionary<string, byte[]>> GetFiles(string containerName, IEnumerable<string> urls,
            CancellationToken cancellationToken = default)
        {
            var blobServiceClient = GetBlobClient(containerName);
            var blobContainer = blobServiceClient.GetBlobContainerClient(containerName);

            var files = new Dictionary<string, byte[]>();
            foreach (var url in urls)
            {
                var urlData = url.Split("/");
                var blobClient = blobContainer.GetBlobClient(urlData[^1]);

                BlobDownloadInfo download = await blobClient.DownloadAsync(cancellationToken);

                var memoryStream = new MemoryStream();
                await download.Content.CopyToAsync(memoryStream);

                files.Add(url, memoryStream.ToArray());
            }

            return files;
        }

        public async Task<byte[]> GetFile(string containerName, string url, CancellationToken cancellationToken = default)
        {
            var file = await GetFiles(containerName, new[] { url }, cancellationToken);
            return file.First().Value;
        }

        public async Task RemoveFiles(string containerName, IEnumerable<string> urls, CancellationToken cancellationToken = default)
        {
            var blobServiceClient = GetBlobClient(containerName);
            var blobContainer = blobServiceClient.GetBlobContainerClient(containerName);

            foreach (var url in urls)
            {
                var cleanUrl = System.Web.HttpUtility.UrlDecode(url);
                var urlData = cleanUrl.Split("/");
                var blobClient = blobContainer.GetBlobClient(urlData[^1]);

                await blobClient.DeleteAsync(cancellationToken: cancellationToken);
            }
        }

        public Task<string> WriteJson(string containerName, string jsonToUpload, string fileName,
            CancellationToken cancellationToken = default)
        {
            throw new NotImplementedException();
        }

        public async Task<string> WriteJson(string containerName, string jsonToUpload, string fileName)
        {
            var blobServiceClient = GetBlobClient(containerName);
            var blobContainer = blobServiceClient.GetBlobContainerClient(containerName);
            BlobClient blob = blobContainer.GetBlobClient(fileName);

            await using MemoryStream ms = new MemoryStream(Encoding.UTF8.GetBytes(jsonToUpload));
            await blob.UploadAsync(ms);
            return blob.Uri.AbsoluteUri;
        }

        public async Task RemoveFile(string containerName, string url, CancellationToken cancellationToken = default)
        {
            await RemoveFiles(containerName, new[] { url }, cancellationToken);
        }

        public async Task<IEnumerable<UploadedFileInfo>> UploadFile(string containerName, byte[] file, string fileName,
            string contentType, CancellationToken cancellationToken = default)
        {
            var blobServiceClient = GetBlobClient(containerName);
            BlobContainerClient containerClient;

            try
            {
                containerClient = await blobServiceClient.CreateBlobContainerAsync(containerName);
            }
            catch
            {
                containerClient = blobServiceClient.GetBlobContainerClient(containerName);
            }

            var urls = new List<UploadedFileInfo>();

            var blobClient = containerClient.GetBlobClient(fileName);

            var url = await UploadFile(blobClient, file, contentType, cancellationToken);

            urls.Add(new UploadedFileInfo { Url = url, ContentType = contentType, FileName = fileName });

            return urls;
        }

        public async Task<List<KeyValuePair<string, DateTimeOffset?>>> GetFilesListByFileNamePrefix(
            string containerName,
            string fileNamePrefix, CancellationToken cancellationToken = default)
        {
            var blobServiceClient = GetBlobClient(containerName);
            var blobContainer = blobServiceClient.GetBlobContainerClient(containerName);

            var files = new Dictionary<string, byte[]>();

            var blobItems = blobContainer.GetBlobs(BlobTraits.None, BlobStates.None, fileNamePrefix, cancellationToken)
                .ToList();

            return blobItems.Select(x => new KeyValuePair<string, DateTimeOffset?>(x.Name, x.Properties.CreatedOn))
                .ToList();
        }

        public async Task<byte[]> DownloadBlob(string containerName,
            string fileName, CancellationToken cancellationToken = default)
        {
            var blobServiceClient = GetBlobClient(containerName);
            var blobContainer = blobServiceClient.GetBlobContainerClient(containerName);

            var blobItems = blobContainer.GetBlobs(BlobTraits.None, BlobStates.None, fileName, cancellationToken);

            var blobClient = blobContainer.GetBlobClient(fileName);

            BlobDownloadInfo download = await blobClient.DownloadAsync(cancellationToken);

            var memoryStream = new MemoryStream();
            await download.Content.CopyToAsync(memoryStream);

            return memoryStream.ToArray();
        }

        private BlobServiceClient GetBlobClient(string containerName)
        {
            var blobServiceClient = new BlobServiceClient(_configuration.ConnectionString);
            return blobServiceClient;
        }

        private async Task<string> UploadFile(BlobClient blobClient, IFormFile file,
            CancellationToken cancellationToken = default)
        {
            var streamReader = new StreamReader(file.OpenReadStream());

            await blobClient.UploadAsync(streamReader.BaseStream,
                new BlobHttpHeaders { ContentType = file.ContentType }, cancellationToken: cancellationToken);

            return blobClient.Uri.AbsoluteUri;
        }

        private async Task<string> UploadFile(BlobClient blobClient, byte[] file, string contentType,
            CancellationToken cancellationToken = default)
        {
            var streamReader = new StreamReader(new MemoryStream(file));

            await blobClient.UploadAsync(streamReader.BaseStream, new BlobHttpHeaders { ContentType = contentType },
                cancellationToken: cancellationToken);

            return blobClient.Uri.AbsoluteUri;
        }
    }
}
