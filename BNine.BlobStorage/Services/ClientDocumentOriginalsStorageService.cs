﻿namespace BNine.BlobStorage.Services;

using Application.Extensions;
using Application.Interfaces;
using Enums;
using Microsoft.Extensions.Options;
using Settings.Blob;

public class ClientDocumentOriginalsStorageService : IClientDocumentOriginalsStorageService
{
    private readonly IBlobStorageService _blobStorageService;
    private readonly IOptions<BlobStorageSettings> _blobStorageConfiguration;

    public ClientDocumentOriginalsStorageService(IBlobStorageService blobStorageService,
        IOptions<BlobStorageSettings> blobStorageConfiguration)
    {
        _blobStorageService = blobStorageService;
        _blobStorageConfiguration = blobStorageConfiguration;
    }

    public async Task<List<KeyValuePair<string, DateTimeOffset?>>> GetClientDocumentsMetadata(Guid clientId, CancellationToken cancellationToken = default)
    {
        var documents =
            await _blobStorageService.GetFilesListByFileNamePrefix(_blobStorageConfiguration.Value.ClientDocumentsContainer.Name,
                clientId.ToString(), cancellationToken);

        return documents;
    }

    public async Task<byte[]> DownloadClientDocumentFile(string fileName, CancellationToken cancellationToken = default)
    {
        var documentFile =
            await _blobStorageService.DownloadBlob(_blobStorageConfiguration.Value.ClientDocumentsContainer.Name,
                fileName, cancellationToken);

        return documentFile;
    }

    public async Task UploadClientDocument(Guid clientId, string documentTypeKey, DocumentSide documentSide,
        byte[] fileData, string contentType, string fileExtension, CancellationToken cancellationToken = default)
    {
        var fileName = ToClientDocumentName(clientId.ToString(), documentTypeKey,
            documentSide.GetEnumMemberValue()) + fileExtension;

        var url = await _blobStorageService.UploadFile(_blobStorageConfiguration.Value.ClientDocumentsContainer.Name,
            fileData, fileName, contentType, cancellationToken);
    }

    /// <summary>
    /// Format in which client files stored in Azure Blob Storage
    /// </summary>
    /// <returns></returns>
    private static string ToClientDocumentName(string clientId, string documentTypeKey, string documentSide)
    {
        return $"{clientId}_{documentTypeKey}_{documentSide}";
    }
}
