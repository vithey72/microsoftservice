﻿namespace BNine.BlobStorage
{
    using Application.Interfaces.LegalDocuments;
    using BNine.Application.Interfaces;
    using BNine.BlobStorage.Services;
    using Microsoft.Extensions.DependencyInjection;

    public static class DependenciesBootstrapper
    {
        public static IServiceCollection AddBlobStorage(this IServiceCollection services)
        {
            services.AddTransient<IBlobStorageService, BlobStorageService>();
            services.AddTransient<IClientDocumentOriginalsStorageService, ClientDocumentOriginalsStorageService>();
            services.AddTransient<IClientSelfieDocumentsService, ClientSelfieDocumentsService>();
            services.AddTransient<ILegalDocumentsStorageService, LegalDocumentsStorageService>();

            return services;
        }
    }
}
