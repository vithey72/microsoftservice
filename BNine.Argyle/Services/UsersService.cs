﻿namespace BNine.Argyle.Services
{
    using System;
    using System.Net;
    using System.Net.Http;
    using System.Threading.Tasks;
    using BNine.Application.Interfaces.Argyle;
    using BNine.Settings;
    using Microsoft.Extensions.Options;
    using Newtonsoft.Json;

    public class UsersService : BaseArgyleService, IArgyleUsersService
    {
        public UsersService(IOptions<ArgyleSettings> options, IHttpClientFactory clientFactory) : base(options, clientFactory)
        {
        }

        public async Task DeleteUser(Guid userId)
        {
            var client = await CreateAuthorizedClient();

            var uri = $"/v1/users/{userId}";

            var request = new HttpRequestMessage(HttpMethod.Delete, uri);

            var response = await client.SendAsync(request);
            response.EnsureSuccessStatusCode();
        }

        //TODO: Replace with model
        public async Task<object> GetUser(Guid userId)
        {
            var client = await CreateAuthorizedClient();

            var uri = $"/v1/users/{userId}";

            var request = new HttpRequestMessage(HttpMethod.Get, uri);

            var response = await client.SendAsync(request);

            // No ensuring success code as user can be already deleted and we're ok with it
            if (response.StatusCode == HttpStatusCode.NotFound)
            {
                return null;
            }

            var stringResponse = await response.Content.ReadAsStringAsync();

            var result = JsonConvert.DeserializeObject<object>(stringResponse);

            return result;
        }
    }
}
