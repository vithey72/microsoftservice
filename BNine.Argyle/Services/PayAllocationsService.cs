﻿namespace BNine.Argyle.Services
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Net.Http;
    using System.Threading.Tasks;
    using BNine.Application.Interfaces.Argyle;
    using BNine.Application.Models.Argyle;
    using BNine.Argyle.Models.Responses;
    using BNine.Settings;
    using Microsoft.Extensions.Options;
    using Newtonsoft.Json;

    public class PayAllocationsService : BaseArgyleService, IPayAllocationsService
    {
        public PayAllocationsService(IOptions<ArgyleSettings> options, IHttpClientFactory clientFactory) : base(options, clientFactory)
        {
        }

        public async Task<IEnumerable<PayAllocationInfo>> GetPayAllocations(Guid argyleUserId)
        {
            var client = await CreateAuthorizedClient();

            var list = new List<PayAllocationResponseListItem>();

            var url = $"/v1/pay-allocations?user={argyleUserId}&limit=200";

            while (!string.IsNullOrEmpty(url))
            {
                var response = await client.GetAsync(url);

                response.EnsureSuccessStatusCode();

                var result = await response.Content.ReadAsStringAsync();

                var data = JsonConvert.DeserializeObject<ListResponse<PayAllocationResponseListItem>>(result);

                list.AddRange(data.Results);

                url = string.IsNullOrEmpty(data.Next) ? null : data.Next[data.Next.IndexOf("/v1/pay-allocations")..];
            }

            return list.Select(x => new PayAllocationInfo
            {
                AllocationType = x.AllocationType,
                AllocationValue = x.AllocationValue,
                Status = x.Status,
                ArgyleUserId = argyleUserId,
                AccountNumber = x.BankAccount.AccountNumber,
                RoutingNumber = x.BankAccount.RoutingNumber,
            });
        }

        public async Task<object> GetPayAllocation(Guid id)
        {
            return await GetArgyleEnityInfo("pay-allocations", id);
        }
    }
}
