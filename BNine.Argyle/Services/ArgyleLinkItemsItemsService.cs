﻿namespace BNine.Argyle.Services;

using System;
using System.Net.Http;
using System.Threading.Tasks;
using Application.Models.Argyle;
using BNine.Application.Interfaces.Argyle;
using BNine.Settings;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;

/// <inheritdoc cref="IArgyleLinkItemsService" />
public class ArgyleLinkItemsItemsService
    : BaseArgyleService
        , IArgyleLinkItemsService
{
    /// <inheritdoc />
    public ArgyleLinkItemsItemsService(
        IOptions<ArgyleSettings> options,
        IHttpClientFactory clientFactory
    )
        : base(options, clientFactory)
    {
    }

    /// <inheritdoc />
    public async Task<ArgyleLinkItemsArray> GetLinkItems(string[] ids, CancellationToken cancellationToken)
    {
        var client = await CreateAuthorizedClient();
        if (ids == null || ids.Length == 0)
        {
            return new ArgyleLinkItemsArray { Items = Array.Empty<ArgyleLinkItem>() };
        }
        var response = await client.GetAsync($"/v1/link-items?id={string.Join(',', ids)}", cancellationToken);
        response.EnsureSuccessStatusCode();

        var stringResponse = await response.Content.ReadAsStringAsync(cancellationToken);
        var result = JsonConvert.DeserializeObject<ArgyleLinkItemsArray>(stringResponse);

        return result;
    }
}
