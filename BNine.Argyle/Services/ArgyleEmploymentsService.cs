﻿namespace BNine.Argyle.Services
{
    using System;
    using System.Net.Http;
    using System.Threading.Tasks;
    using Application.Models.Argyle;
    using BNine.Application.Interfaces.Argyle;
    using BNine.Settings;
    using Microsoft.Extensions.Options;
    using Models.Responses;
    using Newtonsoft.Json;

    public class ArgyleEmploymentsService
        : BaseArgyleService
        , IArgyleEmploymentsService
    {
        public ArgyleEmploymentsService(
            IOptions<ArgyleSettings> options,
            IHttpClientFactory clientFactory
            )
            : base(options, clientFactory)
        {
        }

        public async Task<object> GetEmployment(Guid id)
        {
            return await GetArgyleEnityInfo("employments", id);
        }

        public async Task<ArgyleEmploymentArray> GetEmployments(Guid userId, CancellationToken cancellationToken)
        {
            var client = await CreateAuthorizedClient();

            var response = await client.GetAsync($"/v1/employments?user={userId}&limit=50", cancellationToken);
            response.EnsureSuccessStatusCode();

            var stringResponse = await response.Content.ReadAsStringAsync(cancellationToken);
            var result = JsonConvert.DeserializeObject<ArgyleEmploymentArray>(stringResponse);

            return result;
        }

        public async Task<string[]> GetEmploymentsLinkIds(Guid userId, CancellationToken cancellationToken)
        {
            var url = $"/v1/accounts?user={userId}";
            var client = await CreateAuthorizedClient();

            var response = await client.GetAsync(url, cancellationToken);
            response.EnsureSuccessStatusCode();

            var stringResponse = await response.Content.ReadAsStringAsync(cancellationToken);
            var result = JsonConvert.DeserializeObject<ArgyleUserAccounts>(stringResponse);

            return result?.Results?
                .Where(x => x.WasConnected)
                .Select(x => x.LinkItem)
                .ToArray() ?? Array.Empty<string>();
        }
    }
}
