﻿namespace BNine.Argyle.Services
{
    using System;
    using System.Collections.Generic;
    using System.Net.Http;
    using System.Threading.Tasks;
    using BNine.Application.Interfaces.Argyle;
    using BNine.Argyle.Models.Responses;
    using BNine.Settings;
    using Microsoft.Extensions.Options;
    using Newtonsoft.Json;

    public class PayoutsService : BaseArgyleService, IPayoutsService
    {
        public PayoutsService(IOptions<ArgyleSettings> options, IHttpClientFactory clientFactory) : base(options, clientFactory)
        {
        }

        public async Task<object> GetPayout(Guid id)
        {
            return await GetArgyleEnityInfo("payouts", id);
        }

        public async Task<IEnumerable<object>> GetPayouts(Guid accountId)
        {
            var client = await CreateAuthorizedClient();

            var list = new List<object>();

            var url = $"/v1/payouts?account={accountId}&limit=200";

            while (!string.IsNullOrEmpty(url))
            {
                var response = await client.GetAsync(url);

                response.EnsureSuccessStatusCode();

                var result = await response.Content.ReadAsStringAsync();

                var data = JsonConvert.DeserializeObject<ListResponse<object>>(result);

                list.AddRange(data.Results);

                url = string.IsNullOrEmpty(data.Next) ? null : data.Next[data.Next.IndexOf("/v1/payouts")..];
            }

            return list;
        }
    }
}
