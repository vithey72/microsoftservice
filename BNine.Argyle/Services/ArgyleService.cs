﻿namespace BNine.Argyle.Services
{
    using System;
    using System.Net.Http;
    using System.Threading.Tasks;
    using Application.Interfaces;
    using BNine.Settings;
    using Common;
    using Microsoft.Extensions.Options;
    using Models.Requests;
    using Models.Responses;
    using Newtonsoft.Json;

    public class ArgyleService : BaseArgyleService, IArgyleService
    {
        public ArgyleService(IOptions<ArgyleSettings> options, IHttpClientFactory clientFactory) : base(options, clientFactory)
        {
        }

        public async Task<object> GetAccount(Guid id)
        {
            return await GetArgyleEnityInfo("accounts", id);
        }

        public async Task<(string token, Guid externalUserId)> CreateAccount(Guid userId)
        {
            var uri = "/v2/users";
            var client = await CreateAuthorizedClient();

            var content = SerializationHelper.GetRequestContent(new { external_id = userId.ToString(), });

            var request = new HttpRequestMessage(HttpMethod.Post, uri) { Content = content };

            var response = await client.SendAsync(request);
            response.EnsureSuccessStatusCode();

            var stringResponse = await response.Content.ReadAsStringAsync();

            var result = JsonConvert.DeserializeObject<UserRegistrationResponse>(stringResponse);
            if (result?.Token == null)
            {
                throw new InvalidOperationException($"Cannot parse token from Argyle response: {stringResponse}");
            }

            return (result.Token, result.UserExternalId);
        }

        public async Task<string> GetEncryptedConfig(
            string accountNumber,
            decimal defaultAmountAllocation,
            decimal minAmountAllocation,
            decimal maxAmountAllocation,
            decimal defaultPercentAllocation,
            decimal minPercentAllocation,
            decimal maxPercentAllocation)
        {
            var client = await CreateAuthorizedClient();

            var uri = "/v1/pay-distribution-config/encrypt";

            var content = SerializationHelper.GetRequestContent(new EncryptedConfigRequestModel
            {
                BankAccount = new Models.Requests.BankAccount
                {
                    BankName = "B9",
                    AccountType = "checking",
                    RoutingNumber = Settings.RoutingNumber,
                    AccountNumber = accountNumber
                },
                DefaultAllocationType = "amount",
                PercentAllocation = new Allocation
                {
                    Value = defaultPercentAllocation.ToString("0"),
                    MinValue = minPercentAllocation.ToString("0"),
                    MaxValue = maxPercentAllocation.ToString("0")
                },
                AmountAllocation = new Allocation
                {
                    MinValue = minAmountAllocation.ToString("0"),
                    Value = defaultAmountAllocation.ToString("0"),
                    MaxValue = maxAmountAllocation.ToString("0")
                }
            });

            var request = new HttpRequestMessage(HttpMethod.Post, uri) { Content = content };

            var response = await client.SendAsync(request);

            var stringResponse = await response.Content.ReadAsStringAsync();

            response.EnsureSuccessStatusCode();

            var result = JsonConvert.DeserializeObject<EncryptedConfigResponseModel>(stringResponse);

            return result.EncryptedConfig;
        }

        public async Task<string> UpdateToken(Guid userId)
        {
            var client = await CreateAuthorizedClient();

            var uri = "/v1/user-tokens";

            var content = SerializationHelper.GetRequestContent(new UserTokenRequest { UserId = userId.ToString() });
            var request = new HttpRequestMessage(HttpMethod.Post, uri) { Content = content };

            var response = await client.SendAsync(request);
            response.EnsureSuccessStatusCode();

            var stringResponse = await response.Content.ReadAsStringAsync();
            var result = JsonConvert.DeserializeObject<UserTokenResponse>(stringResponse);
            return result.Access;
        }
    }
}
