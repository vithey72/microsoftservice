﻿namespace BNine.Argyle.Services
{
    using System;
    using System.Net.Http;
    using System.Net.Http.Headers;
    using System.Text;
    using System.Threading.Tasks;
    using BNine.Constants;
    using BNine.Settings;
    using Microsoft.Extensions.Options;
    using Newtonsoft.Json;

    public class BaseArgyleService
    {
        protected ArgyleSettings Settings
        {
            get;
        }

        private IHttpClientFactory ClientFactory
        {
            get;
        }

        protected BaseArgyleService(IOptions<ArgyleSettings> options, IHttpClientFactory clientFactory)
        {
            Settings = options.Value;
            ClientFactory = clientFactory;
        }

        protected Task<HttpClient> CreateAuthorizedClient()
        {
            var client = ClientFactory.CreateClient(HttpClientName.Argyle);

            client.DefaultRequestHeaders.Authorization =
                new AuthenticationHeaderValue("Basic",
                    Convert.ToBase64String(Encoding.ASCII
                        .GetBytes(Settings.ClientId + ":" + Settings.ClientSecret)));

            return Task.FromResult(client);
        }

        protected async Task<object> GetArgyleEnityInfo(string path, Guid id)
        {
            var client = await CreateAuthorizedClient();

            var response = await client.GetAsync($"/v1/{path}/{id}");

            response.EnsureSuccessStatusCode();

            var stringResponse = await response.Content.ReadAsStringAsync();

            var result = JsonConvert.DeserializeObject<object>(stringResponse);

            return result;
        }
    }
}
