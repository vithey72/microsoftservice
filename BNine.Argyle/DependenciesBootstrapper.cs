﻿namespace BNine.Argyle
{
    using BNine.Application.Interfaces;
    using BNine.Application.Interfaces.Argyle;
    using BNine.Argyle.Services;
    using Microsoft.Extensions.DependencyInjection;
    using Polly;
    using Microsoft.Extensions.Configuration;
    using System;
    using BNine.Constants;

    public static class DependenciesBootstrapper
    {
        public static IServiceCollection AddArgyle(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddTransient<IArgyleService, ArgyleService>();
            services.AddTransient<IPayAllocationsService, PayAllocationsService>();
            services.AddTransient<IPayoutsService, PayoutsService>();
            services.AddTransient<IArgyleEmploymentsService, ArgyleEmploymentsService>();
            services.AddTransient<IArgyleLinkItemsService, ArgyleLinkItemsItemsService>();
            services.AddTransient<IArgyleUsersService, UsersService>();

            services.AddHttpClient(HttpClientName.Argyle, client =>
            {
                client.BaseAddress = new Uri(configuration["ArgyleSettings:ApiUrl"]);
            })
            .AddTransientHttpErrorPolicy(p => p.WaitAndRetryAsync(3, (x) => TimeSpan.FromSeconds(x)));

            return services;
        }
    }
}
