﻿namespace BNine.Argyle.Models.Requests
{
    using Newtonsoft.Json;

    public class UserTokenRequest
    {
        [JsonProperty("user")]
        public string UserId
        {
            get;
            set;
        }
    }
}
