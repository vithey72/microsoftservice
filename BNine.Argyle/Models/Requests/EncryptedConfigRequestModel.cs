﻿namespace BNine.Argyle.Models.Requests
{
    using Newtonsoft.Json;

    internal class EncryptedConfigRequestModel
    {
        [JsonProperty("bank_account")]
        internal BankAccount BankAccount
        {
            get; set;
        }

        [JsonProperty("default_allocation_type")]
        internal string DefaultAllocationType
        {
            get; set;
        }

        [JsonProperty("percent_allocation")]
        internal Allocation PercentAllocation
        {
            get; set;
        }

        [JsonProperty("amount_allocation")]
        internal Allocation AmountAllocation
        {
            get; set;
        }
    }

    internal class BankAccount
    {
        [JsonProperty("bank_name")]
        internal string BankName
        {
            get; set;
        }

        [JsonProperty("account_type")]
        internal string AccountType
        {
            get; set;
        }

        [JsonProperty("routing_number")]
        internal string RoutingNumber
        {
            get; set;
        }

        [JsonProperty("account_number")]
        internal string AccountNumber
        {
            get; set;
        }
    }

    internal class Allocation
    {
        [JsonProperty("value")]
        internal string Value
        {
            get; set;
        }

        [JsonProperty("min_value")]
        internal string MinValue
        {
            get; set;
        }

        [JsonProperty("max_value")]
        internal string MaxValue
        {
            get; set;
        }
    }
}
