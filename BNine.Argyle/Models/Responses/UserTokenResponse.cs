﻿namespace BNine.Argyle.Models.Responses
{
    using Newtonsoft.Json;

    public class UserTokenResponse
    {
        [JsonProperty("access")]
        public string Access
        {
            get;
            set;
        }
    }
}
