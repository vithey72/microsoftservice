﻿namespace BNine.Argyle.Models.Responses;

using Newtonsoft.Json;

public class UserRegistrationResponse
{
    [JsonProperty("id")]
    public Guid UserExternalId
    {
        get;
        set;
    }

    [JsonProperty("user_token")]
    public string Token
    {
        get;
        set;
    }
}
