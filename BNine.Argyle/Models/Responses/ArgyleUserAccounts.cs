﻿namespace BNine.Argyle.Models.Responses;

using Newtonsoft.Json;

internal class ArgyleUserAccounts
{
    public ArgyleUserAccountsEntry[] Results
    {
        get;
        set;
    }

    internal class ArgyleUserAccountsEntry
    {
        public string[] Employers
        {
            get;
            set;
        }

        [JsonProperty("link_item")]
        public string LinkItem
        {
            get;
            set;
        }

        [JsonProperty("data_partner")]
        public string DataPartner
        {
            get;
            set;
        }

        [JsonProperty("was_connected")]
        public bool WasConnected
        {
            get;
            set;
        }
    }
}
