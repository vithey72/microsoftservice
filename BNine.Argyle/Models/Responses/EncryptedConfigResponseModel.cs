﻿namespace BNine.Argyle.Models.Responses
{
    using Newtonsoft.Json;

    internal class EncryptedConfigResponseModel
    {
        [JsonProperty("encrypted_config")]
        internal string EncryptedConfig
        {
            get; set;
        }
    }
}
