﻿namespace BNine.Argyle.Models.Responses
{
    using System;
    using Newtonsoft.Json;

    public class PayAllocationResponseListItem
    {
        [JsonProperty("id")]
        public string Id
        {
            get; set;
        }

        [JsonProperty("account")]
        public string Account
        {
            get; set;
        }

        [JsonProperty("bank_account")]
        public BankAccount BankAccount
        {
            get; set;
        }

        [JsonProperty("metadata")]
        public Metadata Metadata
        {
            get; set;
        }

        [JsonProperty("created_at")]
        public DateTime CreatedAt
        {
            get; set;
        }

        [JsonProperty("updated_at")]
        public DateTime UpdatedAt
        {
            get; set;
        }

        [JsonProperty("status")]
        public string Status
        {
            get; set;
        }

        [JsonProperty("allocation_type")]
        public string AllocationType
        {
            get; set;
        }

        [JsonProperty("allocation_value")]
        public string AllocationValue
        {
            get; set;
        }

        [JsonProperty("employer")]
        public object Employer
        {
            get; set;
        }
    }

    public class BankAccount
    {
        [JsonProperty("routing_number")]
        public string RoutingNumber
        {
            get; set;
        }

        [JsonProperty("account_number")]
        public string AccountNumber
        {
            get; set;
        }

        [JsonProperty("account_type")]
        public string AccountType
        {
            get; set;
        }
    }

    public class Metadata
    {
    }
}
