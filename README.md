﻿# BNine

## Get started

Refer to nested READMEs to get more details about:

[Admin API](./BNine.AdminAPI/README.md)
[API](./BNine.API/README.md)
[Identity Server](./BNine.IdentityServer/README.md)
[Webhook](./BNine.Webhook/README.md)

## Code quality

We maintain consistent code style in a codebase by defining code style rule options in the `.editorconfig` file. These rules are surfaced by various development IDEs.

## Contribute

Please reference articles inside of [Git flow, branch and commit naming](https://bninecom.atlassian.net/wiki/spaces/MAYF/pages/945815553/Development+workflow#Git-flow%2C-branch-and-commit-naming) to find out more about our Git experience.
